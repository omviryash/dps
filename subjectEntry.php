<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$subjectMasterId = 0;
	$subjectName     = "";
	$subjectCode     = "";
	$startClass      = 0;
	$endClass        = 0;
	$subjectType     = 0;
	$subjectDescription = '';
	$class   = "";
	$isEdit  = 0;
	$classArray    = array();
	
  if(isset($_POST['Submit']))
  {
  	$subjectName     = isset($_POST['subjectName']) ? $_POST['subjectName'] : "";
  	$subjectCode     = isset($_POST['subjectCode']) ? $_POST['subjectCode'] : "";
  	$startClass      = isset($_POST['startClass']) ? $_POST['startClass'] : "";
  	$endClass        = isset($_POST['endClass']) ? $_POST['endClass'] : "";
  	$subjectType     = isset($_POST['subjectType']) ? $_POST['subjectType'] : "";
  	$subjectDescription = isset($_POST['subjectDescription']) ? $_POST['subjectDescription'] : "";
  	$subjectMasterId = isset($_POST['subjectMasterId']) ? $_POST['subjectMasterId'] : 0;
  	
  	if($subjectMasterId == 0)
  	{
  	  $insertClass = "INSERT INTO subjectmaster (subjectName,subjectCode,startClass,endClass,subjectType,subjectDescription)
  	                  VALUES ('".$subjectName."','".$subjectCode."','".$startClass."','".$endClass."','".$subjectType."','".$subjectDescription."')";
  	  $insertClassRes = om_query($insertClass);
  	  if(!$insertClassRes)
  	  {
  	  	echo "Insert Fail";
  	  }
  	  else
  	  {
  	  	header("Location:subjectEntry.php?done=1");
  	  }
  	}
  	else
  	{
  	  $isEdit = 1;
  	  $updateClass = "UPDATE subjectmaster
  	                     SET subjectName = '".$subjectName."',
  	                         subjectCode = '".$subjectCode."',
  	                         startClass = '".$startClass."',
  	                         endClass = '".$endClass."',
  	                         subjectDescription = '".$subjectDescription."',
  	                         subjectType = '".$subjectType."'
  	                   WHERE subjectMasterId = ".$_REQUEST['subjectMasterId'];
      $updateClassRes = om_query($updateClass);
      if(!$updateClassRes)
      {
      	echo "Update Fail";
      }
      else
      {
      	header("Location:subjectEntry.php?done=1");
      }
  	}
  }
  
  if(isset($_REQUEST['subjectMasterId']) > 0)
  {
    $selectClass = "SELECT subjectmaster.subjectMasterId,subjectmaster.startClass,subjectmaster.endClass,subjectmaster.subjectType,
                           subjectmaster.subjectDescription,subjectmaster.subjectName,subjectmaster.subjectCode
                      FROM subjectmaster
                     WHERE subjectMasterId = ".$_REQUEST['subjectMasterId'];
    $selectClassRes = mysql_query($selectClass);
    if($classRow = mysql_fetch_array($selectClassRes))
    {
    	$subjectMasterId = $classRow['subjectMasterId'];
    	$subjectName     = $classRow['subjectName'];
    	$subjectCode     = $classRow['subjectCode'];
    	$startClass      = $classRow['startClass'];
    	$endClass        = $classRow['endClass'];
    	$subjectType     = $classRow['subjectType'];
    	$subjectDescription = $classRow['subjectDescription'];
    }
  }
  
	$c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  $secArrOut[0] = 'Main';
  $secArrOut[1] = 'Optional';
  $secArrOut[2] = 'Club';
  $secArrOut[3] = 'Co - Scholastic';
  
  $secArrOut1[0] = '2A';
  $secArrOut1[1] = '2B';
  $secArrOut1[2] = '2C';
  $secArrOut1[3] = '2D';
  $secArrOut1[4] = '3A';
  $secArrOut1[5] = '3B';
  
  include("./bottom.php");
  $smarty->assign('subjectMasterId',$subjectMasterId);
  $smarty->assign('startClass',$startClass);
  $smarty->assign('secArrOut1',$secArrOut1);
  $smarty->assign('endClass',$endClass);
  $smarty->assign('isEdit',$isEdit);
  $smarty->assign('cArray',$cArray);
  $smarty->assign('secArrOut',$secArrOut);
  $smarty->assign('classArray',$classArray);
  $smarty->assign('subjectName',$subjectName);
  $smarty->assign('subjectCode',$subjectCode);
  $smarty->assign('subjectType',$subjectType);
  $smarty->assign('subjectDescription',$subjectDescription);
  $smarty->display('subjectEntry.tpl');  
}
?>
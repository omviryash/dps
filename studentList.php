<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] != 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$stdArray = array();
  $selectStd = "SELECT studentName,fatherName,mothersName,grNo,dateOfBirth,joinedInClass,joiningDate,gender,bloodGroup,houseName,
                       smsMobile,currentAddress,currentState,currentAddressPin,residencePhone1,residencePhone2,studentEmail,fatherPhone,
                       fatherEmail,fatherMobile,fatherOccupation,fathersDesignation,fathersOrganization,mothersPhone,mothersEmailid,
                       mothersMobile,motherOccupation,mothersDesignation,mothersOrganization,permanentAddress,permanentState,
                       permanentPIN,religion,previousSchool,studentImage,fatherImage,motherImage
                  FROM studentmaster
                  LEFT JOIN house ON house.houseId = studentmaster.houseId
                 WHERE studentLoginId = '".$_SESSION['s_activName']."' 
                    OR parentLoginId = '".$_SESSION['s_activName']."'";
  $selectStdRes = mysql_query($selectStd);
  while($stdRow = mysql_fetch_array($selectStdRes))
  {
  	$stdArray['studentName']         = $stdRow['studentName'];
  	$stdArray['fatherName']          = $stdRow['fatherName'];
  	$stdArray['mothersName']         = $stdRow['mothersName'];
  	$stdArray['grNo']                = $stdRow['grNo'];
  	$stdArray['dateOfBirth']         = $stdRow['dateOfBirth'];
  	$stdArray['joinedInClass']       = $stdRow['joinedInClass'];
  	$stdArray['joiningDate']         = $stdRow['joiningDate'];
  	$stdArray['gender']              = $stdRow['gender'];
  	$stdArray['houseName']           = $stdRow['houseName'];
    $stdArray['bloodGroup']          = $stdRow['bloodGroup'];
    $stdArray['smsMobile']           = $stdRow['smsMobile'];
  	$stdArray['currentAddress']      = $stdRow['currentAddress'].'  '.$stdRow['currentState'].'  '.$stdRow['currentAddressPin'];
  	$stdArray['residencePhone1']     = $stdRow['residencePhone1'];
  	$stdArray['residencePhone2']     = $stdRow['residencePhone2'];
  	$stdArray['studentEmail']        = $stdRow['studentEmail'];
  	$stdArray['fatherPhone']         = $stdRow['fatherPhone'];
  	$stdArray['fatherEmail']         = $stdRow['fatherEmail'];
  	$stdArray['fatherMobile']        = $stdRow['fatherMobile'];
  	$stdArray['fatherOccupation']    = $stdRow['fatherOccupation'];
  	$stdArray['fathersDesignation']  = $stdRow['fathersDesignation'];
  	$stdArray['fathersOrganization'] = $stdRow['fathersOrganization'];
  	$stdArray['mothersPhone']        = $stdRow['mothersPhone'];
  	$stdArray['mothersEmailid']      = $stdRow['mothersEmailid'];
  	$stdArray['mothersMobile']       = $stdRow['mothersMobile'];
  	$stdArray['motherOccupation']    = $stdRow['motherOccupation'];
  	$stdArray['mothersDesignation']  = $stdRow['mothersDesignation'];
  	$stdArray['mothersOrganization'] = $stdRow['mothersOrganization'];
  	$stdArray['permanentAddress']    = $stdRow['permanentAddress'].'  '.$stdRow['permanentState'].'  '.$stdRow['permanentPIN'];
  	$stdArray['religion']            = $stdRow['religion'];
  	$stdArray['previousSchool']      = $stdRow['previousSchool'];
  	$stdArray['studentImage']        = $stdRow['studentImage'];
  	$stdArray['fatherImage']         = $stdRow['fatherImage'];
  	$stdArray['motherImage']         = $stdRow['motherImage'];
  }
  include("./bottom.php");
  $smarty->assign('stdArray',$stdArray);
  $smarty->display('studentList.tpl');  
}
?>
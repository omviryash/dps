<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$employeeMasterId = 0;
	$subjectMasterId  = 0;
	$weekDay          = '';
	$class            = '';
	$section          = '';
	$periodNo         = 0;
	$timeTableId      = "";
	$isEdit           = 0;
	$todayAcademic    = date('m-d');
	
	if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
	{
  	$academicStartYear = date('Y');
	}
	else
	{
		$academicStartYear = date('Y') - 1;
	}
	
  if(isset($_POST['Submit']))
  {
  	$nextYear          = $_POST['startYear'] + 1;
    $academicStartYear = $_POST['startYear']."-04-01";
    $academicEndYear   = $nextYear."-03-31";
  	$employeeMasterId  = isset($_POST['employeeMasterId']) ? $_POST['employeeMasterId'] : 0;
  	$subjectMasterId   = isset($_POST['subjectMasterId']) ? $_POST['subjectMasterId'] : 0;
  	$weekDay           = isset($_POST['weekDay']) ? $_POST['weekDay'] : '';
  	$class             = isset($_POST['class']) ? $_POST['class'] : '';
  	$section           = isset($_POST['section']) ? $_POST['section'] : '';
  	$periodNo          = isset($_POST['periodNo']) ? $_POST['periodNo'] : 0;
  	$timeTableId       = isset($_POST['timeTableId']) ? $_POST['timeTableId'] : 0;
  	
  	if($timeTableId == 0)
  	{
  	  $insertTimeTable = "INSERT INTO timetable (academicStartYear,academicEndYear,employeeMasterId,subjectMasterId,weekDay,class,section,periodNo)
  	                      VALUES('".$academicStartYear."','".$academicEndYear."',".$employeeMasterId.",".$subjectMasterId.",'".$weekDay."','".$class."','".$section."',".$periodNo.")";
  	  $insertTimeTableRes = om_query($insertTimeTable);
  	  if(!$insertTimeTableRes)
  	  {
  	  	echo "Insert Fail";
  	  }
  	  else
  	  {
  	  	header("Location:timeTableEntry.php?done=1");
  	  }
  	}
  	else
  	{
  	  $isEdit = 1;
  	  $updateTimeTable = "UPDATE timetable
	                           SET academicStartYear = '".$academicStartYear."',
	                               academicEndYear = '".$academicEndYear."',
	                               employeeMasterId = ".$employeeMasterId.",
	                               subjectMasterId = ".$subjectMasterId.",
	                               weekDay = '".$weekDay."',
	                               class = '".$class."',
	                               section = '".$section."',
	                               periodNo = ".$periodNo."
	                         WHERE timeTableId = ".$_REQUEST['timeTableId'];
      $updateTimeTableRes = om_query($updateTimeTable);
      if(!$updateTimeTableRes)
      {
      	echo "Update Fail";
      }
      else
      {
      	header("Location:timeTableEntry.php?done=1");
      }
  	}
  }
  
  if(isset($_REQUEST['timeTableId']) > 0)
  {
    $selectTimeTable = "SELECT timeTableId,academicStartYear,academicEndYear,employeeMasterId,subjectMasterId,weekDay,
                               class,section,periodNo
                          FROM timetable
                         WHERE timeTableId = ".$_REQUEST['timeTableId'];
    $selectTimeTableRes = mysql_query($selectTimeTable);
    if($timeTableRow = mysql_fetch_array($selectTimeTableRes))
    {
    	$timeTableId      = $timeTableRow['timeTableId'];
    	$employeeMasterId = $timeTableRow['employeeMasterId'];
    	$subjectMasterId  = $timeTableRow['subjectMasterId'];
    	$weekDay          = $timeTableRow['weekDay'];
    	$class            = $timeTableRow['class'];
    	$section          = $timeTableRow['section'];
    	$periodNo         = $timeTableRow['periodNo'];
    }
  }
  
  $tArr = array();
  $t = 0;
  $selectT = "SELECT employeeMasterId,name
                FROM employeemaster
               WHERE userType = 'Teacher'";
  $selectTRes = mysql_query($selectT);
  while($tRow = mysql_fetch_array($selectTRes))
  {
  	$tArr['employeeMasterId'][$t] = $tRow['employeeMasterId'];
  	$tArr['name'][$t]             = $tRow['name'];
  	$t++;
  }
  
  $sArr = array();
  $s = 0;
  $selectS = "SELECT subjectMasterId,subjectName
                FROM subjectmaster";
  $selectSRes = mysql_query($selectS);
  while($sRow = mysql_fetch_array($selectSRes))
  {
  	$sArr['subjectMasterId'][$s] = $sRow['subjectMasterId'];
  	$sArr['subjectName'][$s]     = $sRow['subjectName'];
  	$s++;
  }
  
  $weekOut[0] = 'Monday';
  $weekOut[1] = 'Tuesday';
  $weekOut[2] = 'Wednesday';
  $weekOut[3] = 'Thursday';
  $weekOut[4] = 'Friday';
  $weekOut[5] = 'Saturday';
  $weekOut[6] = 'Sunday';
  
  $periodOut[0] = '1';
  $periodOut[1] = '2';
  $periodOut[2] = '3';
  $periodOut[3] = '4';
  $periodOut[4] = '5';
  $periodOut[5] = '6';
  $periodOut[6] = '7';
  
  $c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  $secArrOut[0] = 'A';
  $secArrOut[1] = 'B';
  $secArrOut[2] = 'C';
  $secArrOut[3] = 'D';
  
  include("./bottom.php");
  $smarty->assign('cArray',$cArray);
  $smarty->assign('secArrOut',$secArrOut);
  $smarty->assign('timeTableId',$timeTableId);
  $smarty->assign('employeeMasterId',$employeeMasterId);
  $smarty->assign('subjectMasterId',$subjectMasterId);
  $smarty->assign('weekDay',$weekDay);
  $smarty->assign('class',$class);
  $smarty->assign('section',$section);
  $smarty->assign('periodNo',$periodNo);
  $smarty->assign('isEdit',$isEdit);
  $smarty->assign('academicStartYear',$academicStartYear);
  $smarty->assign('tArr',$tArr);
  $smarty->assign('sArr',$sArr);
  $smarty->assign('weekOut',$weekOut);
  $smarty->assign('periodOut',$periodOut);
  $smarty->display('timeTableEntry.tpl');  
}
?>
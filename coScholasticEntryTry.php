<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$class      = '';
  $count      = 0;
  $today      = date('Y-m-d');
	$section    = '';
	$examMarksPerTryId = 0;
	
	if(isset($_REQUEST['startYear']))
  {
  	$academicStartYear = $_REQUEST['startYear']."-04-01";
  	$nextYear          = $_REQUEST['startYear'] + 1;
  	$academicEndYear   = $nextYear."-03-31";
  	$academicStartYearSelected = $_REQUEST['startYear'];
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y')."-04-01";
	  	$nextYear          = date('Y') + 1;
	  	$academicEndYear   = $nextYear."-03-31";
	  	$academicStartYearSelected = date('Y');
		}
		else
		{
			$prevYear          = date('Y') - 1;
			$academicStartYear = $prevYear."-04-01";
	  	$academicEndYear   = date('Y')."-03-31";
	  	$academicStartYearSelected = $prevYear;
		}
	}
	
	$class   = isset($_GET['class']) && $_GET['class'] != '' ? $_GET['class'] : 0;
	$section = isset($_GET['section']) && $_GET['section'] != '' ? $_GET['section'] : 0;
	
	$selectId = "SELECT employeeMasterId
                 FROM employeemaster
                WHERE loginId = '".$_SESSION['s_activName']."'";
  $selectIdRes = mysql_query($selectId);
  if($idRow = mysql_fetch_array($selectIdRes))
  {
  	$selectClass = "SELECT class,section
	                    FROM classteacherallotment
	                   WHERE employeeMasterId = '".$idRow['employeeMasterId']."'
	                     AND academicStartYear = '".$academicStartYear."'
                       AND academicEndYear = '".$academicEndYear."'";
	  $selectClassRes = mysql_query($selectClass);
	  if($classRow = mysql_fetch_array($selectClassRes))
	  {
	  	if($class == 0)
	  	{
		  	$class   = $classRow['class'];
		  	$section = $classRow['section'];
		  }
		}
		
		$dtlArr = array();
		$g = 0;
	  $selectStdIn = "SELECT exammarkspertry.examMarksPerTryId,exammarkspertry.grNo,studentmaster.gender,studentmaster.activated,
	                         exammarkspertry.academicStartYear,exammarkspertry.academicEndYear,studentmaster.studentName,
	                         2A1,2A2,2A3,2B1,2C1,2D1,2D2,2D3,2D4,3A1,3A2,3B1,3B2
	                    FROM exammarkspertry
	               LEFT JOIN studentmaster ON studentmaster.grNo = exammarkspertry.grNo
	                   WHERE exammarkspertry.class = '".$class."'
	                     AND exammarkspertry.section = '".$section."'
	                     AND exammarkspertry.academicStartYear = '".$academicStartYear."'
                       AND exammarkspertry.academicEndYear = '".$academicEndYear."'
                       AND studentmaster.activated = 'Y'";
	  $selectStdInRes = mysql_query($selectStdIn);
	  $count = mysql_num_rows($selectStdInRes);
	  while($stdInRow = mysql_fetch_array($selectStdInRes))
	  {
	  	$selectRoll = "SELECT rollNo
	                     FROM nominalroll
	                    WHERE grNo = '".$stdInRow['grNo']."'";
	    $selectRollRes = mysql_query($selectRoll);
	    while($rollRow = mysql_fetch_array($selectRollRes))
	    {
	    	$dtlArr[$g]['rollNo'] = $rollRow['rollNo'];
	    }
	  	$dtlArr[$g]['examMarksPerTryId'] = $stdInRow['examMarksPerTryId'];
	  	$dtlArr[$g]['grNo']          = $stdInRow['grNo'];
	  	$dtlArr[$g]['studentName']   = $stdInRow['studentName'];
	  	
	  	$dtlArr[$g]['2A1'] = $stdInRow['2A1'];
	  	$dtlArr[$g]['2A2'] = $stdInRow['2A2'];
	  	$dtlArr[$g]['2A3'] = $stdInRow['2A3'];
	  	$dtlArr[$g]['2B1'] = $stdInRow['2B1'];
	  	$dtlArr[$g]['2C1'] = $stdInRow['2C1'];
	  	$dtlArr[$g]['2D1'] = $stdInRow['2D1'];
	  	$dtlArr[$g]['2D2'] = $stdInRow['2D2'];
	  	$dtlArr[$g]['2D3'] = $stdInRow['2D3'];
	  	$dtlArr[$g]['2D4'] = $stdInRow['2D4'];
	  	$dtlArr[$g]['3A1'] = $stdInRow['3A1'];
	  	$dtlArr[$g]['3A2'] = $stdInRow['3A2'];
	  	$dtlArr[$g]['3B1'] = $stdInRow['3B1'];
	  	$dtlArr[$g]['3B2'] = $stdInRow['3B2'];
	  	
	  	$g++;
	  }
  }
  
  if(isset($_POST['submitTaken']))
  {
  	$loopCount = 0;
  	while($loopCount < count($_POST['examMarksPerTryId']))
  	{
  		$examMarksPerTryId = ($_POST['examMarksPerTryId'][$loopCount] != '') ? $_POST['examMarksPerTryId'][$loopCount] : 0;
  		$co2A1 = ($_POST['2A1'][$loopCount] != '') ? $_POST['2A1'][$loopCount] : 0;
  		$co2A2 = ($_POST['2A2'][$loopCount] != '') ? $_POST['2A2'][$loopCount] : 0;
  		$co2A3 = ($_POST['2A3'][$loopCount] != '') ? $_POST['2A3'][$loopCount] : 0;
  		$co2B1 = ($_POST['2B1'][$loopCount] != '') ? $_POST['2B1'][$loopCount] : 0;
  		$co2C1 = ($_POST['2C1'][$loopCount] != '') ? $_POST['2C1'][$loopCount] : 0;
  		$co2D1 = ($_POST['2D1'][$loopCount] != '') ? $_POST['2D1'][$loopCount] : 0;
  		$co2D2 = ($_POST['2D2'][$loopCount] != '') ? $_POST['2D2'][$loopCount] : 0;
  		$co2D3 = ($_POST['2D3'][$loopCount] != '') ? $_POST['2D3'][$loopCount] : 0;
  		$co2D4 = ($_POST['2D4'][$loopCount] != '') ? $_POST['2D4'][$loopCount] : 0;
  		$co3A1 = ($_POST['3A1'][$loopCount] != '') ? $_POST['3A1'][$loopCount] : 0;
  		$co3A2 = ($_POST['3A2'][$loopCount] != '') ? $_POST['3A2'][$loopCount] : 0;
  		$co3B1 = ($_POST['3B1'][$loopCount] != '') ? $_POST['3B1'][$loopCount] : 0;
  		$co3B2 = ($_POST['3B2'][$loopCount] != '') ? $_POST['3B2'][$loopCount] : 0;
  		
  		
  		if($_POST['examMarksPerTryId'][$loopCount] != '' && $_POST['examMarksPerTryId'][$loopCount] > 0)
  		{
				$updateClass = "UPDATE exammarkspertry
		  	                   SET 2A1 = ".$co2A1.",
		  	                       2A2 = ".$co2A2.",
		  	                       2A3 = ".$co2A3.",
		  	                       2B1 = ".$co2B1.",
		  	                       2C1 = ".$co2C1.",
		  	                       2D1 = ".$co2D1.",
		  	                       2D2 = ".$co2D2.",
		  	                       2D3 = ".$co2D3.",
		  	                       2D4 = ".$co2D4.",
		  	                       3A1 = ".$co3A1.",
		  	                       3A2 = ".$co3A2.",
		  	                       3B1 = ".$co3B1.",
		  	                       3B2 = ".$co3B2."
		  	                 WHERE examMarksPerTryId = ".$examMarksPerTryId."";
			  $updateClassRes = om_query($updateClass);
			  if(!$updateClassRes)
			  {
			  	echo "Update Fail";
			  }
			  else
			  {
			  	header("Location:coScholasticEntryTry.php?startYear=".$academicStartYearSelected."&go=go");
			  }
			}
			$loopCount++;
		}
  }
  
  $c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster
                   WHERE className > 5
                     AND className <= 10";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  $secArrOut[0] = 'A';
  $secArrOut[1] = 'B';
  $secArrOut[2] = 'C';
  $secArrOut[3] = 'D';
  
  
  include("./bottom.php");
  $smarty->assign('academicStartYearSelected',$academicStartYearSelected);
  $smarty->assign('dtlArr',$dtlArr);
  $smarty->assign('secArrOut',$secArrOut);
  $smarty->assign('class',$class);
  $smarty->assign('section',$section);
  $smarty->assign('count',$count);
  $smarty->assign('cArray',$cArray);
  $smarty->assign('examMarksPerTryId',$examMarksPerTryId);
  $smarty->display('coScholasticEntryTry.tpl');  
}
?>
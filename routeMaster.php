<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$isEdit    = 0;
	$routeName = '';
	$startTime = '';
	$endTime   = '';
	$arrival   = '';
	$routeMasterId = 0;
	
	$classArray      = array();
	
  if(isset($_POST['Submit']))
  {
  	$routeName     = isset($_POST['routeName']) ? $_POST['routeName'] : 0;
  	$startTime     = $_POST['startTimeHour'].':'.$_POST['startTimeMinute'].':00';
  	$endTime       = $_POST['endTimeHour'].':'.$_POST['endTimeMinute'].':00';
  	$arrival       = isset($_POST['arrival']) ? $_POST['arrival'] : '';
  	$routeMasterId = isset($_POST['routeMasterId']) ? $_POST['routeMasterId'] : 0;
  	
  	if($routeMasterId == 0)
  	{
  	  $insertClass = "INSERT INTO routemaster (routeName,startTime,endTime,arrival)
  	                  VALUES ('".$routeName."','".$startTime."','".$endTime."','".$arrival."')";
  	  $insertClassRes = om_query($insertClass);
  	  if(!$insertClassRes)
  	  {
  	  	echo "Insert Fail";
  	  }
  	  else
  	  {
  	  	header("Location:routeMaster.php?done=1");
  	  }
  	}
  	else
  	{
  	  $isEdit = 1;
  	  $updateClass = "UPDATE routemaster
  	                     SET routeName   = '".$routeName."',
  	                         startTime = '".$startTime."',
  	                         endTime    = '".$endTime."',
  	                         arrival = '".$arrival."'
  	                   WHERE routeMasterId = ".$_REQUEST['routeMasterId'];
      $updateClassRes = om_query($updateClass);
      if(!$updateClassRes)
      {
      	echo "Update Fail";
      }
      else
      {
      	header("Location:routeMaster.php?done=1");
      }
  	}
  }
  
  $i = 0;
  $selectClass = "SELECT routemaster.routeMasterId,routemaster.routeName,routemaster.startTime,routemaster.endTime,
                         routemaster.arrival
                    FROM routemaster";
  $selectClassRes = mysql_query($selectClass);
  while($classRow = mysql_fetch_array($selectClassRes))
  {
  	$classArray[$i]['routeMasterId'] = $classRow['routeMasterId'];
  	$classArray[$i]['routeName']     = $classRow['routeName'];
  	$classArray[$i]['startTime']     = $classRow['startTime'];
  	$classArray[$i]['endTime']       = $classRow['endTime'];
  	$classArray[$i]['arrival']       = $classRow['arrival'];
  	$i++;
  }
  
  if(isset($_REQUEST['routeMasterId']) > 0)
  {
    $selectClass = "SELECT routemaster.routeMasterId,routemaster.routeName,
                           routemaster.startTime,routemaster.endTime,routemaster.arrival
	                    FROM routemaster
                     WHERE routeMasterId = ".$_REQUEST['routeMasterId']."";
    $selectClassRes = mysql_query($selectClass);
    if($classRow = mysql_fetch_array($selectClassRes))
    {
    	$routeMasterId = $classRow['routeMasterId'];
    	$routeName        = $classRow['routeName'];
    	$startTime = $classRow['startTime'];
    	$endTime       = $classRow['endTime'];
    	$arrival         = $classRow['arrival'];
    }
  }
  
  $typeArr[0] = 'Arrival';
  $typeArr[1] = 'Departure';
  
  include("./bottom.php");
  $smarty->assign('routeMasterId',$routeMasterId);
  $smarty->assign('isEdit',$isEdit);
  $smarty->assign('classArray',$classArray);
  $smarty->assign('routeName',$routeName);
  $smarty->assign('startTime',$startTime);
  $smarty->assign('endTime',$endTime);
  $smarty->assign('arrival',$arrival);
  $smarty->assign('typeArr',$typeArr);
  $smarty->display('routeMaster.tpl'); 
}
?>
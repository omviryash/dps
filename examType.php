<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$examTypeArray = array();
  if(isset($_POST['submit']))
  {
  	$examType   = isset($_POST['examType']) ? $_POST['examType'] : "";
  	$examTypeId = isset($_POST['examTypeId']) ? $_POST['examTypeId'] : 0;
  	if($examTypeId == 0)
  	{
  	  $insertExam = "INSERT INTO examtype (examType)
  	                  VALUES('".$examType."')";
  	  $insertExamRes = om_query($insertExam);
  	  if(!$insertExamRes)
  	  {
  	  	echo "Insert Fail";
  	  }
  	  else
  	  {
  	  	header("Location:examType.php?done=1");
  	  }
  	}
  	else
  	{
  	  $isEdit = 1;
  	  $updateExam = "UPDATE examtype
  	                    SET examType = '".$examType."'
  	                  WHERE examTypeId = ".$_REQUEST['examTypeId'];
      $updateExamRes = om_query($updateExam);
      if(!$updateExamRes)
      {
      	echo "Update Fail";
      }
      else
      {
      	header("Location:examType.php?done=1");
      }
  	}
  }
  
  $i = 0;
  $selectExam = "SELECT examTypeId,examType
                   FROM examtype
               ORDER BY examType";
  $selectExamRes = mysql_query($selectExam);
  while($examRow = mysql_fetch_array($selectExamRes))
  {
  	$examTypeArray[$i]['examTypeId'] = $examRow['examTypeId'];
  	$examTypeArray[$i]['examType']   = $examRow['examType'];
  	$i++;
  }
   
  include("./bottom.php");
  $smarty->assign('examTypeArray',$examTypeArray);
  $smarty->display('examType.tpl');  
}
?>
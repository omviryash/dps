<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$isEdit               = 0;
	$checktype            = '';
	$employeeCode         = '';
	$grNo                 = '';
	$studentName          = '';
	$bookAccessionNo      = '';
	$bookTitle            = '';
	$toName               = '';
	$libraryTransactionId = 0;
	$issueDate            = date('Y-m-d');
	$returnableDate       = '';
	$fine                 = 0;
	$finePay              = 0;
	
	$classArray      = array();
	$checktype = isset($_REQUEST['checktype']) ? $_REQUEST['checktype'] : '';
	$libraryTransactionId = isset($_REQUEST['libraryTransactionId']) ? $_REQUEST['libraryTransactionId'] : '';
	$employeeCode = isset($_REQUEST['employeeCode']) ? $_REQUEST['employeeCode'] : '';
	
	$selectEmp = "SELECT name
                  FROM employeemaster
                 WHERE employeeCode = '".$employeeCode."'";
	$empRes = mysql_query($selectEmp);
	if($empRow = mysql_fetch_array($empRes))
	{
	  $toName = $empRow['name'];
	}
  if(isset($_POST['Submit']))
  {
  	$employeeCode         = isset($_POST['employeeCode']) && $_POST['employeeCode'] != '' ? $_POST['employeeCode'] : 0;
		$issueDate            = $_POST['issueDateYear']."-".$_POST['issueDateMonth']."-".$_POST['issueDateDay'];
		$returnableDate       = $_POST['returnableDateYear']."-".$_POST['returnableDateMonth']."-".$_POST['returnableDateDay'];
  	$checktype            = isset($_POST['checktype']) && $_POST['checktype'] != '' ? $_POST['checktype'] : 'S';
  	$grNo                 = isset($_POST['grNo']) && $_POST['grNo'] != '' ? $_POST['grNo'] : 0;
  	$fine                 = isset($_POST['fine']) && $_POST['fine'] != '' ? $_POST['fine'] : 0;
  	$bookAccessionNo      = isset($_POST['bookAccessionNo']) && $_POST['bookAccessionNo'] != '' ? $_POST['bookAccessionNo'] : 0;
  	$libraryTransactionId = isset($_POST['libraryTransactionId']) && $_POST['libraryTransactionId'] != '' ? $_POST['libraryTransactionId'] : 0;
  	
  	if($fine == 0)
		{
		  $finePay            = 0;
		}
		else
		{
			$finePay            = ($_POST['finePay'] == 'on') ? 'Paid' : 'Panding';
		}
  	
  	if($employeeCode > 0 || $grNo > 0)
  	{
	  	if($libraryTransactionId == 0)
	  	{
	  	  $insertClass = "INSERT INTO librarytransaction (checktype,employeeCode,grNo,bookAccessionNo,returnableDate,issueDate)
	  	                  VALUES ('".$checktype."',".$employeeCode.",".$grNo.",".$bookAccessionNo.",'0000-00-00','".$issueDate."')";
	  	  $insertClassRes = om_query($insertClass);
	  	  if(!$insertClassRes)
	  	  {
	  	  	echo "Insert Fail";
	  	  }
	  	  else
	  	  {
	  	  	header("Location:libraryTransaction.php?done=1");
	  	  }
	  	}
	  	else
	  	{
	  	  $isEdit = 1;
	  	  $updateClass = "UPDATE librarytransaction
	  	                     SET checktype = '".$checktype."',
	  	                         employeeCode = ".$employeeCode.",
	  	                         grNo = ".$grNo.",
	  	                         bookAccessionNo = ".$bookAccessionNo.",
	  	                         issueDate = '".$issueDate."',
	  	                         returnableDate = '".$returnableDate."',
	  	                         fine = '".$fine."',
	  	                         finePay = '".$finePay."'
	  	                   WHERE libraryTransactionId = ".$_REQUEST['libraryTransactionId'];
	      $updateClassRes = om_query($updateClass);
	      if(!$updateClassRes)
	      {
	      	echo "Update Fail";
	      }
	      else
	      {
	      	header("Location:libraryTransaction.php?done=1");
	      }
	  	}
	  }
  }
  
  $i = 0;
  $selectClass = "SELECT librarytransaction.libraryTransactionId,librarytransaction.checktype,librarytransaction.employeeCode,
                         librarytransaction.grNo,librarytransaction.bookAccessionNo,librarytransaction.issueDate
                    FROM librarytransaction
               LEFT JOIN bookmaster ON bookmaster.bookAccessionNo = librarytransaction.bookAccessionNo
                ORDER BY librarytransaction.issueDate DESC";
  $selectClassRes = mysql_query($selectClass);
  while($classRow = mysql_fetch_array($selectClassRes))
  {
  	$classArray[$i]['libraryTransactionId'] = $classRow['libraryTransactionId'];
  	$classArray[$i]['checktype']            = $classRow['checktype'];
  	$classArray[$i]['employeeCode']         = $classRow['employeeCode'];
  	$classArray[$i]['grNo']                 = $classRow['grNo'];
  	$classArray[$i]['bookAccessionNo']      = $classRow['bookAccessionNo'];
  	$classArray[$i]['issueDate']            = $classRow['issueDate'];
  	$i++;
  }
  
  if(isset($_REQUEST['libraryTransactionId']) > 0)
  {
    $selectClass = "SELECT librarytransaction.libraryTransactionId,librarytransaction.checktype,librarytransaction.employeeCode,
                           librarytransaction.grNo,librarytransaction.bookAccessionNo,librarytransaction.issueDate,librarytransaction.finePay,
                           librarytransaction.returnableDate,studentmaster.studentName,bookmaster.bookTitle,librarytransaction.fine
	                    FROM librarytransaction
	               LEFT JOIN studentmaster ON studentmaster.grNo = librarytransaction.grNo
	               LEFT JOIN bookmaster ON bookmaster.bookAccessionNo = librarytransaction.bookAccessionNo
                     WHERE libraryTransactionId = ".$_REQUEST['libraryTransactionId']."";
    $selectClassRes = mysql_query($selectClass);
    if($classRow = mysql_fetch_array($selectClassRes))
    {
    	$libraryTransactionId = $classRow['libraryTransactionId'];
    	$checktype            = $classRow['checktype'];
    	$employeeCode         = $classRow['employeeCode'];
    	$grNo                 = $classRow['grNo'];
    	$studentName          = $classRow['studentName'];
    	$bookAccessionNo      = $classRow['bookAccessionNo'];
    	$bookTitle            = $classRow['bookTitle'];
    	$issueDate            = $classRow['issueDate'];
    	$returnableDate       = $classRow['returnableDate'];
    	$fine                 = $classRow['fine'];
    	$finePay              = $classRow['finePay'];
    }
  }

  $c=0;
	$cArray = array();
	$selectClass = "SELECT studentName
                    FROM studentmaster";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['studentName'][$c]    = $classRow['studentName'];
	  $c++;
	}
	
  $d=0;
	$dArray = array();
	$selectClass1 = "SELECT name
                     FROM employeemaster";
	$selectClass1Res = mysql_query($selectClass1);
	while($class1Row = mysql_fetch_array($selectClass1Res))
	{
	  $dArray['name'][$d]    = $class1Row['name'];
	  $d++;
	}
  
  include("./bottom.php");
  $smarty->assign('libraryTransactionId',$libraryTransactionId);
  $smarty->assign('isEdit',$isEdit);
  $smarty->assign('classArray',$classArray);
  $smarty->assign('issueDate',$issueDate);
  $smarty->assign('checktype',$checktype);
  $smarty->assign('employeeCode',$employeeCode);
  $smarty->assign('grNo',$grNo);
  $smarty->assign('studentName',$studentName);
  $smarty->assign('bookAccessionNo',$bookAccessionNo);
  $smarty->assign('bookTitle',$bookTitle);
  $smarty->assign('cArray',$cArray);
  $smarty->assign('dArray',$dArray);
  $smarty->assign('checktype',$checktype);
  $smarty->assign('toName',$toName);
  $smarty->assign('fine',$fine);
  $smarty->assign('finePay',$finePay);
  $smarty->assign('returnableDate',$returnableDate);
  
  $smarty->display('libraryTransaction.tpl');  
  
}
?>
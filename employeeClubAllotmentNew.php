<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$nominalArr = array();
	if(isset($_REQUEST['startYear']))
  {
  	$academicStartYear = $_REQUEST['startYear']."-04-01";
  	$nextYear          = $_REQUEST['startYear'] + 1;
  	$academicEndYear   = $nextYear."-03-31";
  	$academicStartYearSelected = $_REQUEST['startYear'];
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y')."-04-01";
	  	$nextYear          = date('Y') + 1;
	  	$academicEndYear   = $nextYear."-03-31";
	  	$academicStartYearSelected = date('Y');
		}
		else
		{
			$prevYear          = date('Y') - 1;
			$academicStartYear = $prevYear."-04-01";
	  	$academicEndYear   = date('Y')."-03-31";
	  	$academicStartYearSelected = $prevYear;
		}
	}
	
	$class   = isset($_REQUEST['class']) ? $_REQUEST['class'] : 0;
	$section = isset($_REQUEST['classSection']) ? $_REQUEST['classSection'] : 0;
	
	$selectId = "SELECT employeeMasterId
                 FROM employeemaster
                WHERE loginId = '".$_SESSION['s_activName']."'";
  $selectIdRes = mysql_query($selectId);
  if($idRow = mysql_fetch_array($selectIdRes))
  {
  	$selectClass = "SELECT class,section
	                    FROM classteacherallotment
	                   WHERE employeeMasterId = '".$idRow['employeeMasterId']."'
	                     AND academicStartYear = '".$academicStartYear."'
	                     AND academicEndYear = '".$academicEndYear."'";
	  $selectClassRes = mysql_query($selectClass);
	  if($classRow = mysql_fetch_array($selectClassRes))
	  {
	  	if($class == 0)
	  	{
		  	$class   = $classRow['class'];
		  	$section = $classRow['section'];
		  }
	  }
	  $i = 0;
	  $selectNominal = "SELECT nominalRollId,nominalroll.grNo,academicStartYear,academicEndYear,nominalroll.class,
	                           nominalroll.section,rollNo,feeGroup,libraryMemberType,libraryMemberType,studentmaster.studentName,
	                           busRoute,busStop,subjectGroup,coScholasticGroup,clubTerm1,clubTerm2,
	                           boardRegistrionId,boardRollNo,activated,studentmaster.studentLoginId,studentmaster.parentLoginId,
	                           nominalroll.optionSubject1,nominalroll.optionSubject2,nominalroll.3a1,nominalroll.3a2,nominalroll.3b1,nominalroll.3b2
	                      FROM nominalroll
	                 LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
	                     WHERE studentmaster.activated = 'Y'
	                       AND nominalroll.class = '".$class."'
	                       AND nominalroll.section = '".$section."'
	                       AND academicStartYear = '".$academicStartYear."'
	                       AND academicEndYear = '".$academicEndYear."'
	                  ORDER BY nominalroll.rollNo";
	  $selectNominalRes = mysql_query($selectNominal);
	  while($nominalRow = mysql_fetch_array($selectNominalRes))
	  {
	    $nominalArr[$i]['nominalRollId']      = $nominalRow['nominalRollId'];
	    $nominalArr[$i]['grNo']               = $nominalRow['grNo'];
	    $nominalArr[$i]['academicYear']       = substr($nominalRow['academicStartYear'],0,4).'-'.substr($nominalRow['academicEndYear'],0,4);
	    $nominalArr[$i]['class']              = $nominalRow['class'];
	    $nominalArr[$i]['section']            = $nominalRow['section'];
	    $nominalArr[$i]['rollNo']             = $nominalRow['rollNo'];
	    $nominalArr[$i]['studentName']        = $nominalRow['studentName'];
	    $nominalArr[$i]['activated']          = $nominalRow['activated'];
	    $nominalArr[$i]['clubTerm1']          = $nominalRow['clubTerm1'];
	    $nominalArr[$i]['clubTerm2']          = $nominalRow['clubTerm2'];
	    $i++;
	  }
	}
  
  $clubArr  = array();
  $clubArr2 = array();
	$k = 0;
	$selectClub1 = "SELECT subjectMasterId,subjectName,subjectType
	                  FROM subjectmaster
	                 WHERE subjectType = 'club'";
	$selectClub1Res = mysql_query($selectClub1);
	while($club1Row = mysql_fetch_array($selectClub1Res))
	{
		$clubArr['subjectMasterId'][$k]   = $club1Row['subjectMasterId'];
		$clubArr['subjectName'][$k]       = $club1Row['subjectName'];
		$k++;
	}
	
	if(isset($_POST['submit']))
	{
		$loopCount1 = 0;
	  while($loopCount1 < count($_POST['nominalRollId']))
	  {
	    $nominalRollId = isset($_POST['nominalRollId'][$loopCount1]) && $_POST['nominalRollId'][$loopCount1] != '' ? $_POST['nominalRollId'][$loopCount1] : 0;
	    $clubTerm1     = isset($_POST['clubTerm1'][$loopCount1]) && $_POST['clubTerm1'][$loopCount1] != '' ? $_POST['clubTerm1'][$loopCount1] : '';
		  $clubTerm2     = isset($_POST['clubTerm2'][$loopCount1]) && $_POST['clubTerm2'][$loopCount1] != '' ? $_POST['clubTerm2'][$loopCount1] : '';
		  
	    if($clubTerm1 != '' || $clubTerm2 != '')
	    {
	      $nominalEntry = "UPDATE nominalroll 
			  										SET clubTerm1 = '".$clubTerm1."',
			  											  clubTerm2 = '".$clubTerm2."'
											  	WHERE nominalRollId  = ".$nominalRollId."";
			  $nominalEntryRes = om_query($nominalEntry);
			  if(!$nominalEntryRes)
			  {
			  	echo "Update Fail";
			  }
			  else
			  {
			    header("Location:employeeClubAllotmentNew.php?done=1");
			  }
	    }
	    $loopCount1++;
	  }
  }
  
  $c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  $secArrOut[0] = 'A';
  $secArrOut[1] = 'B';
  $secArrOut[2] = 'C';
  $secArrOut[3] = 'D';
  
  include("./bottom.php");
  $smarty->assign('class',$class);
  $smarty->assign('section',$section);
  $smarty->assign('cArray',$cArray);
  $smarty->assign('secArrOut',$secArrOut);
  $smarty->assign('clubArr',$clubArr);
  $smarty->assign('clubArr2',$clubArr2);
  $smarty->assign('nominalArr',$nominalArr);
  $smarty->assign('academicStartYear',$academicStartYear);
  $smarty->assign('academicEndYear',$academicEndYear);
  $smarty->assign('academicStartYearSelected',$academicStartYearSelected);
  $smarty->display('employeeClubAllotmentNew.tpl');  
}
?>
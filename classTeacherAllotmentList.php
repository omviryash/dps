<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$classTeacherAllotmentId = 0;
	$employeeMasterId        = 0;
	$academicStartYear       = '0000';
	$academicEndYear         = '0000';
	if(isset($_REQUEST['startYear']))
  {
	  $academicStartYear = $_REQUEST['startYear'];
	  $academicEndYear   = $_REQUEST['startYear'] + 1;
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y');
	  	$nextYear          = date('Y') + 1;
	  	$academicEndYear   = $nextYear;
		}
		else
		{
			$prevYear          = date('Y') - 1;
			$academicStartYear = $prevYear;
	  	$academicEndYear   = date('Y');
		}
	}
	$class   = "";
	$section = "";
	$isEdit  = 0;
	
	$classArray    = array();
	
  $i = 0;
  $selectClass = "SELECT DISTINCT classteacherallotment.classTeacherAllotmentId,
                         classteacherallotment.academicStartYear,classteacherallotment.academicEndYear,
                         classteacherallotment.class,classteacherallotment.section,
                         employeemaster.name
                    FROM classteacherallotment
               LEFT JOIN employeemaster ON employeemaster.employeeMasterId = classteacherallotment.employeeMasterId
               LEFT JOIN classmaster ON classmaster.className = classteacherallotment.class
                   WHERE classteacherallotment.academicStartYear = '".$academicStartYear."-04-01'
                     AND classteacherallotment.academicEndYear = '".$academicEndYear."-03-31'
                ORDER BY classmaster.priority,classteacherallotment.section";
  $selectClassRes = mysql_query($selectClass);
  while($classRow = mysql_fetch_array($selectClassRes))
  {
  	$classArray[$i]['classTeacherAllotmentId'] = $classRow['classTeacherAllotmentId'];
  	$classArray[$i]['name']                    = $classRow['name'];
  	$classArray[$i]['academicStartYear']       = substr($classRow['academicStartYear'],0,4);
  	$classArray[$i]['academicEndYear']         = substr($classRow['academicEndYear'],0,4);
  	
  	$classArray[$i]['class']   = $classRow['class'];
  	$classArray[$i]['section'] = $classRow['section'];
  	$i++;
  }
  
  include("./bottom.php");
  $smarty->assign('classTeacherAllotmentId',$classTeacherAllotmentId);
  $smarty->assign('employeeMasterId',$employeeMasterId);
  $smarty->assign('academicStartYear',$academicStartYear);
  $smarty->assign('academicEndYear',$academicEndYear);
  $smarty->assign('isEdit',$isEdit);
  $smarty->assign('classArray',$classArray);
  $smarty->display('classTeacherAllotmentList.tpl');  
}
?>
<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$class      = '';
  $count      = 0;
  $today      = date('Y-m-d');
	$stdArray   = array();
	$section    = '';
	
	if(isset($_REQUEST['startYear']))
  {
  	$academicStartYear = $_REQUEST['startYear']."-04-01";
  	$nextYear          = $_REQUEST['startYear'] + 1;
  	$academicEndYear   = $nextYear."-03-31";
  	$academicStartYearSelected = $_REQUEST['startYear'];
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y')."-04-01";
	  	$nextYear          = date('Y') + 1;
	  	$academicEndYear   = $nextYear."-03-31";
	  	$academicStartYearSelected = date('Y');
		}
		else
		{
			$prevYear          = date('Y') - 1;
			$academicStartYear = $prevYear."-04-01";
	  	$academicEndYear   = date('Y')."-03-31";
	  	$academicStartYearSelected = $prevYear;
		}
	}
	
	$class   = isset($_GET['class']) && $_GET['class'] != '' ? $_GET['class'] : 0;
	$section = isset($_GET['section']) && $_GET['section'] != '' ? $_GET['section'] : 0;
	
	$selectId = "SELECT employeeMasterId
                 FROM employeemaster
                WHERE loginId = '".$_SESSION['s_activName']."'";
  $selectIdRes = mysql_query($selectId);
  if($idRow = mysql_fetch_array($selectIdRes))
  {
  	$selectClass = "SELECT class,section
	                    FROM classteacherallotment
	                   WHERE employeeMasterId = '".$idRow['employeeMasterId']."'
	                     AND academicStartYear = '".$academicStartYear."'
	                     AND academicEndYear = '".$academicEndYear."'";
	  $selectClassRes = mysql_query($selectClass);
	  if($classRow = mysql_fetch_array($selectClassRes))
	  {
	  	if($class == 0)
	  	{
		  	$class   = $classRow['class'];
		  	$section = $classRow['section'];
		  }
		}
		
		
		if(isset($_GET['submit']))
	  {
		  $selectStdIn = "SELECT nominalroll.grNo,nominalroll.rollNo,studentmaster.gender,studentmaster.activated,nominalroll.academicStartYear,nominalroll.academicEndYear
		                    FROM nominalroll
		               LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
		                   WHERE nominalroll.class = '".$class."'
		                     AND nominalroll.section = '".$section."'
		                     AND nominalroll.academicStartYear = '".$academicStartYear."'
	                       AND nominalroll.academicEndYear = '".$academicEndYear."'
	                       AND studentmaster.activated = 'Y'";
		  $selectStdInRes = mysql_query($selectStdIn);
		  while($stdInRow = mysql_fetch_array($selectStdInRes))
		  {
		  	$grNo        = $stdInRow['grNo'];
		  	
		  	$selectCheck = "SELECT grNo
			                    FROM exammarkspertry
			                   WHERE class = '".$class."'
			                     AND section = '".$section."'
			                     AND academicStartYear = '".$academicStartYear."'
		                       AND academicEndYear = '".$academicEndYear."'
		                       AND grNo = '".$grNo."'";
			  $selectCheckRes = mysql_query($selectCheck);
			  $genCount = mysql_num_rows($selectCheckRes);
			  
			  if($genCount == 0)
				{
			  	$insertClass = "INSERT INTO exammarkspertry (academicStartYear,academicEndYear,grNo,class,section)
		  	                  VALUES('".$academicStartYear."','".$academicEndYear."','".$grNo."','".$class."','".$section."')";
		  	  $insertClassRes = om_query($insertClass);
		  	  if(!$insertClassRes)
		  	  {
		  	  	echo "Insert Fail";
		  	  }
		  	  else
		  	  {
		  	  	header("Location:reGenerateMarksEntryPerformaTry.php?startYear=".$academicStartYearSelected."&class=".$class."&section=".$section."&go=go");
	  	    }
	  	  }
		  }
	  }
  
		$i = 0;
	  $selectStd = "SELECT exammarkspertry.grNo,studentmaster.gender,studentmaster.studentName
	                  FROM exammarkspertry
	             LEFT JOIN studentmaster ON studentmaster.grNo = exammarkspertry.grNo
	                 WHERE exammarkspertry.class = '".$class."'
	                   AND exammarkspertry.section = '".$section."'
	                   AND exammarkspertry.academicStartYear = '".$academicStartYear."'
	                   AND exammarkspertry.academicEndYear = '".$academicEndYear."'
	                   AND studentmaster.activated = 'Y'";
	  $selectStdRes = mysql_query($selectStd);
	  $count = mysql_num_rows($selectStdRes);
	  while($stdRow = mysql_fetch_array($selectStdRes))
	  {
	  	$selectRoll = "SELECT rollNo
	                     FROM nominalroll
	                    WHERE grNo = '".$stdRow['grNo']."'";
	    $selectRollRes = mysql_query($selectRoll);
	    while($rollRow = mysql_fetch_array($selectRollRes))
	    {
	    	$stdArray[$i]['rollNo'] = $rollRow['rollNo'];
	    }
	  	$stdArray[$i]['grNo']            = $stdRow['grNo'];
	  	$stdArray[$i]['gender']          = $stdRow['gender'];
	  	$stdArray[$i]['studentName']     = $stdRow['studentName'];
	  	$i++;
	  }
  }
  
  $c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster
                   WHERE className > 5
                     AND className <= 10";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  $secArrOut[0] = 'A';
  $secArrOut[1] = 'B';
  $secArrOut[2] = 'C';
  $secArrOut[3] = 'D';
  
  include("./bottom.php");
  $smarty->assign('academicStartYearSelected',$academicStartYearSelected);
  $smarty->assign('secArrOut',$secArrOut);
  $smarty->assign('class',$class);
  $smarty->assign('section',$section);
  $smarty->assign('stdArray',$stdArray);
  $smarty->assign('count',$count);
  $smarty->assign('cArray',$cArray);
  $smarty->display('reGenerateMarksEntryPerformaTry.tpl');  
}
?>
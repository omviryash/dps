<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	if(isset($_REQUEST['startYear']))
  {
	  $academicStartYear = $_REQUEST['startYear'];
	  $academicEndYear   = $_REQUEST['startYear'] + 1;
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y');
	  	$nextYear            = date('Y') + 1;
	  	$academicEndYear   = $nextYear;
		}
		else
		{
			$prevYear            = date('Y') - 1;
			$academicStartYear = $prevYear;
	  	$academicEndYear   = date('Y');
		}
	}
	
	$term            = isset($_REQUEST['term']) ? $_REQUEST['term'] : 0;
	$subjectMasterId = isset($_REQUEST['subjectMasterId']) ? $_REQUEST['subjectMasterId'] : 0;
	
	if($term == 'term1' && $subjectMasterId != '0')
	{
		$clubQuery = " AND nominalroll.clubTerm1 = '".$subjectMasterId."'";
	}
	elseif($term == 'term2' && $subjectMasterId != '0')
	{
		$clubQuery = " AND nominalroll.clubTerm2 = '".$subjectMasterId."'";
	}
	else
	{
		$clubQuery = "AND 1 = 1";
	}
	
	$nominalArr = array();
  $i = 0;
  $selectNominal = "SELECT DISTINCT nominalRollId,nominalroll.grNo,academicStartYear,academicEndYear,class,
                           section,rollNo,feeGroup,libraryMemberType,libraryMemberType,studentmaster.studentName,
                           busRoute,busStop,subjectGroup,coScholasticGroup,clubTerm1,clubTerm2,
                           boardRegistrionId,boardRollNo,activated,nominalroll.clubTerm1,nominalroll.clubTerm2
                      FROM nominalroll
                 LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
                 LEFT JOIN classmaster ON classmaster.className = nominalroll.class
                     WHERE 1 = 1
                       AND nominalroll.academicStartYear = '".$academicStartYear."-04-01'
                       AND nominalroll.academicEndYear = '".$academicEndYear."-03-31'
                       ".$clubQuery."
                       AND studentmaster.activated = 'Y'
                  ORDER BY classmaster.priority,nominalroll.section,nominalroll.rollNo";
  $selectNominalRes = mysql_query($selectNominal);
  while($nominalRow = mysql_fetch_array($selectNominalRes))
  {
    $nominalArr[$i]['nominalRollId']      = $nominalRow['nominalRollId'];
    $nominalArr[$i]['grNo']               = $nominalRow['grNo'];
    $nominalArr[$i]['academicYear']       = substr($nominalRow['academicStartYear'],0,4).'-'.substr($nominalRow['academicEndYear'],0,4);
    $nominalArr[$i]['class']              = $nominalRow['class'];
    $nominalArr[$i]['section']            = $nominalRow['section'];
    $nominalArr[$i]['rollNo']             = $nominalRow['rollNo'];
    $nominalArr[$i]['feeGroup']           = $nominalRow['feeGroup'];
    $nominalArr[$i]['libraryMemberType']  = $nominalRow['libraryMemberType'];
    $nominalArr[$i]['libraryMemberType']  = $nominalRow['libraryMemberType'];
    $nominalArr[$i]['studentName']        = $nominalRow['studentName'];
    $nominalArr[$i]['busRoute']           = $nominalRow['busRoute'];
    $nominalArr[$i]['busStop']            = $nominalRow['busStop'];
    $nominalArr[$i]['subjectGroup']       = $nominalRow['subjectGroup'];
    $nominalArr[$i]['coScholasticGroup']  = $nominalRow['coScholasticGroup'];
    $nominalArr[$i]['clubTerm1']          = $nominalRow['clubTerm1'];
    $nominalArr[$i]['clubTerm2']          = $nominalRow['clubTerm2'];
    $nominalArr[$i]['boardRegistrionId']  = $nominalRow['boardRegistrionId'];
    $nominalArr[$i]['boardRollNo']        = $nominalRow['boardRollNo'];
    $nominalArr[$i]['activated']          = $nominalRow['activated'];
    $i++;
  }
  
  $termArrOut[0] = 'term1';
  $termArrOut[1] = 'term2';
  
  $clubArray = array();
  $t = 0;
  $selectClass = "SELECT subjectMasterId,subjectName
                    FROM subjectmaster
                   WHERE subjectType = 'Club'
                ORDER BY subjectName";
  $selectClassRes = mysql_query($selectClass);
  while($classRow = mysql_fetch_array($selectClassRes))
  {
  	$clubArray['subjectMasterId'][$t] = $classRow['subjectMasterId'];
  	$clubArray['clubName'][$t]        = $classRow['subjectName'];
  	$t++;
  }
  
  include("./bottom.php");
  $smarty->assign('nominalArr',$nominalArr);
  $smarty->assign('termArrOut',$termArrOut);
  $smarty->assign('clubArray',$clubArray);
  $smarty->assign('academicStartYear',$academicStartYear);
  $smarty->assign('academicEndYear',$academicEndYear);
  $smarty->assign('term',$term);
  $smarty->assign('subjectMasterId',$subjectMasterId);
  $smarty->display('clubReportAll.tpl');  
}
?>
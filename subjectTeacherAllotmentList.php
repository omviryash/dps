<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$classTeacherAllotmentId = 0;
	$employeeMasterId        = 0;
	$academicStartYear       = '0000';
	$academicEndYear         = '0000';
	if(isset($_REQUEST['startYear']))
  {
	  $academicStartYear = $_REQUEST['startYear'];
	  $academicEndYear   = $_REQUEST['startYear'] + 1;
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y');
	  	$nextYear          = date('Y') + 1;
	  	$academicEndYear   = $nextYear;
		}
		else
		{
			$prevYear          = date('Y') - 1;
			$academicStartYear = $prevYear;
	  	$academicEndYear   = date('Y');
		}
	}
	$class   = "";
	$section = "";
	$period  = 0;
	$subjectMasterId = "";
	$isEdit  = 0;
	$subjectTeacherAllotmentId  = 0;
	
	$classArray    = array();
	
  $i = 0;
  $selectClass = "SELECT DISTINCT subjectteacherallotment.subjectTeacherAllotmentId,
                         subjectteacherallotment.academicStartYear,subjectteacherallotment.academicEndYear,
                         subjectteacherallotment.class,subjectteacherallotment.section,
                         employeemaster.name,subjectmaster.subjectName,subjectteacherallotment.period
                    FROM subjectteacherallotment
               LEFT JOIN employeemaster ON employeemaster.employeeMasterId = subjectteacherallotment.employeeMasterId
               LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = subjectteacherallotment.subjectMasterId
               LEFT JOIN classmaster ON classmaster.className = subjectteacherallotment.class
                   WHERE subjectteacherallotment.academicStartYear = '".$academicStartYear."-04-01'
                     AND subjectteacherallotment.academicEndYear = '".$academicEndYear."-03-31'
                ORDER BY academicStartYear DESC,classmaster.priority,subjectteacherallotment.section,subjectmaster.subjectName";
  $selectClassRes = mysql_query($selectClass);
  while($classRow = mysql_fetch_array($selectClassRes))
  {
  	$classArray[$i]['subjectTeacherAllotmentId'] = $classRow['subjectTeacherAllotmentId'];
  	$classArray[$i]['name']                      = $classRow['name'];
  	$classArray[$i]['academicStartYear']         = substr($classRow['academicStartYear'],0,4);
  	$classArray[$i]['academicEndYear']           = substr($classRow['academicEndYear'],0,4);
  	
  	$classArray[$i]['class']       = $classRow['class'];
  	$classArray[$i]['section']     = $classRow['section'];
  	$classArray[$i]['subjectName'] = $classRow['subjectName'];
  	$classArray[$i]['period']      = $classRow['period'];
  	$i++;
  }
  
  include("./bottom.php");
  $smarty->assign('subjectTeacherAllotmentId',$subjectTeacherAllotmentId);
  $smarty->assign('employeeMasterId',$employeeMasterId);
  $smarty->assign('academicStartYear',$academicStartYear);
  $smarty->assign('academicEndYear',$academicEndYear);
  $smarty->assign('class',$class);
  $smarty->assign('isEdit',$isEdit);
  $smarty->assign('section',$section);
  $smarty->assign('subjectMasterId',$subjectMasterId);
  $smarty->assign('classArray',$classArray);
  $smarty->assign('period',$period);
  $smarty->display('subjectTeacherAllotmentList.tpl');  
}
?>
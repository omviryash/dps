<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$academicStartYear = date('Y');
	$currentSelctedYear = isset($_REQUEST['startYear']) ? $_REQUEST['startYear'] : date('Y');
	$result = "";
	$subArray = array();
	$i=0;
	
	if(isset($_POST['startYear']))
  {
	  $academicStartYear = $_REQUEST['startYear'];
	  $nextYear          = $_REQUEST['startYear'] + 1;
	  $academicEndYear   = $nextYear; 
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	    $academicStartYear = date('Y');
	  	$nextYear          = date('Y') + 1;
	  	$academicEndYear   = $nextYear;
		}
		else
		{
			$prevYear            = date('Y') - 1;
			$academicStartYear = $prevYear;
	  	$academicEndYear   = date('Y');
		}
	}
	
  $selectSubject = "SELECT subjectId,subjectName
                      FROM subjectmaster 
                    WHERE subjectType = 'Main'";
  $selectSubjectRes = mysql_query($selectSubject);
  while($subjectRow = mysql_fetch_array($selectSubjectRes))
  {
  	$subArray['subjectId'][$i]   = $subjectRow['subjectId'];
  	$subArray['subjectName'][$i] = $subjectRow['subjectName'];
  	$i++;
  }
  
  $o=0;
  $selectGrade = "SELECT gradeId,grade
                    FROM grade
                  WHERE priority <= 9";
  $selectGradeRes = mysql_query($selectGrade);
  while($gradeRow = mysql_fetch_array($selectGradeRes))
  {
    $gradeArray['gradeId'][$o] = $gradeRow['gradeId'];
    $gradeArray['grade'][$o]   = $gradeRow['grade'];
    $o++;
  }
  
  $termValue[0]  = 1;
  $termOutput[0] = 'Term 1';
  $termValue[1]  = 2;
  $termOutput[1] = 'Term 2';
  
  $subjectValue[0] = 1;
  $subjectOutput[0] = 'Hindi';
  $subjectValue[1] = 2;
  $subjectOutput[1] = 'English';
  $subjectValue[2] = 3;
  $subjectOutput[2] = 'Environmental Science';
  $subjectValue[3] = 4;
  $subjectOutput[3] = 'Persnalioty Devlopment';
  $subjectValue[4] = 5;
  $subjectOutput[4] = 'Cocuricular Activities';
  $subjectValue[5] = 6;
  $subjectOutput[5] = 'Computer';
  $subjectValue[6] = 7;
  $subjectOutput[6] = 'Health';
  $subjectValue[7] = 8;
  $subjectOutput[7] = 'Mathematics';
  $subjectValue[8] = 9;
  $subjectOutput[8] = 'Other';
  
  $selectId = "SELECT employeeMasterId
                 FROM employeemaster
                WHERE loginId = '".$_SESSION['s_activName']."'";
  $selectIdRes = mysql_query($selectId);
  if($idRow = mysql_fetch_array($selectIdRes))
  {
    $selectClass = "SELECT class,section
	                    FROM classteacherallotment
	                   WHERE employeeMasterId = ".$idRow['employeeMasterId']."
					   AND  academicStartYear  LIKE '".$academicStartYear."%'";
	  $selectClassRes = mysql_query($selectClass);
	  while($classRow = mysql_fetch_array($selectClassRes))
	  {
	  	$class   = $classRow['class'];
	  	$section = $classRow['section'];
	  	if($class <=12)
	  	{
	  		$result = 0;
	  	}
	  	else
	  	{
	  		$result = 1;
	  		header("Location:index.php");
	  	}
	  }
  }
  include("./bottom.php");
  $smarty->assign('result',$result);
  $smarty->assign('gradeArray',$gradeArray);
  $smarty->assign('termValue',$termValue);
  $smarty->assign('termOutput',$termOutput);
  $smarty->assign('subjectOutput',$subjectOutput);
  $smarty->assign('subjectValue',$subjectValue);
  $smarty->assign('dateArrVal',$dateArrVal);
  $smarty->assign('dateArrOut',$dateArrOut);
  $smarty->assign("currentSelctedYear",$currentSelctedYear);
  $smarty->assign('subArray',$subArray);
  $smarty->display('gradeEntry.tpl');
}
?>
<?PHP
//Source...http://www.strangerstudios.com/sandbox/pagination/diggstyle.php
//Customize the css...http://www.mis-algoritmos.com/2007/03/16/some-styles-for-your-pagination/
//Help to impliment...http://www.phpeasystep.com/phptu/29.html

//include('db.php');    // include your code to connect to DB.
$tbl_name="nominalroll";             //****EDIT HERE**** your table name     
$adjacents = 2;                 //****EDIT HERE**** How many adjacent pages should be shown on each side of active page?
/* 
   First get total number of rows in data table. 
   If you have a WHERE clause in your query, make sure you mirror it here.
*/
if(isset($_REQUEST['startYear']))
{
  $academicStartYear = $_REQUEST['startYear'];
  $academicEndYear   = $_REQUEST['startYear'] + 1;
}
else
{
	$todayAcademic = date('m-d');
	if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
	{
  	$academicStartYear = date('Y');
  	$nextYear            = date('Y') + 1;
  	$academicEndYear   = $nextYear;
	}
	else
	{
		$prevYear            = date('Y') - 1;
		$academicStartYear = $prevYear;
  	$academicEndYear   = date('Y');
	}
}


$CurTime = date('Y-m-d H:i:s');
$query = "SELECT COUNT(*) as num FROM $tbl_name WHERE academicStartYear = '".$academicStartYear."-04-01' AND academicEndYear = '".$academicEndYear."-03-31'";
$total_pages = mysql_fetch_array(mysql_query($query));
$total_pages = $total_pages['num'];


/* Setup vars for query. */
$targetpage = "nominalRollListAll.php";                               //****EDIT HERE**** your file name  (the name of the page referring to this file)
$limit = isset($_GET['limit']) && $_GET['limit'] != '' ? $_GET['limit'] : 100;                                                     //****EDIT HERE**** how many items to show per page
$page = (isset($_GET['page']) ? $_GET['page'] : 0);
if($page) 
  $start = ($page - 1) * $limit;                  //first item to display on this page
else
  $start = 0;                                                             //if no page var is given, set start to 0

$CurTime = date('Y-m-d H:i:s');
/* Get data. */
//echo "<br> P ".$sql = "SELECT * FROM $tbl_name WHERE status='A' AND startTime < '".$CurTime."' AND endTime > '".$CurTime."' ORDER BY `businessId` DESC LIMIT $start, $limit";  //****EDIT HERE**** you need to edit... WHERE `active` = 1 ORDER BY `date` DESC
//$result = mysql_query($sql);

/* Setup page vars for display. */
if ($page == 0) $page = 1;                                      //if no page var is given, default to 1.
$prev = $page - 1;                                                      //previous page is page - 1
$next = $page + 1;                                                      //next page is page + 1
$lastpage = ceil($total_pages/$limit);          //lastpage is = total pages / items per page, rounded up.
$lpm1 = $lastpage - 1;                                          //last page minus 1

/* 
        Now we apply our rules and draw the pagination object. 
        We're actually saving the code to a variable in case we want to draw it more than once.
*/
$pagination = "";
if($lastpage > 1)
{       
  $pagination .= "<div class=\"pagination\">";
  //previous button
  if ($page > 1) 
    $pagination.= "<a href=\"$targetpage?page=$prev\"><< previous</a>";
  else
    $pagination.= "<span class=\"disabled\"><< previous</span>";    
  
  //pages 
  if ($lastpage < 7 + ($adjacents * 2))   //not enough pages to bother breaking it up
  {       
    for ($counter = 1; $counter <= $lastpage; $counter++)
    {
      if ($counter == $page)
        $pagination.= "<span class=\"current\">$counter</span>";
      else
        $pagination.= "<a href=\"$targetpage?page=$counter&limit=$limit&startYear=$academicStartYear\">$counter</a>";                                     
    }
  }
  elseif($lastpage > 5 + ($adjacents * 2))        //enough pages to hide some
  {
    //close to beginning; only hide later pages
    if($page < 1 + ($adjacents * 2))                
    {
      for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
      {
        if ($counter == $page)
          $pagination.= "<span class=\"current\">$counter</span>";
        else
          $pagination.= "<a href=\"$targetpage?page=$counter&limit=$limit&startYear=$academicStartYear\">$counter</a>";                                     
      }
      $pagination.= "...";
      $pagination.= "<a href=\"$targetpage?page=$lpm1\">$lpm1</a>";
      $pagination.= "<a href=\"$targetpage?page=$lastpage\">$lastpage</a>";           
    }
    //in middle; hide some front and some back
    elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
    {
      $pagination.= "<a href=\"$targetpage?page=1\">1</a>";
      $pagination.= "<a href=\"$targetpage?page=2\">2</a>";
      $pagination.= "...";
      for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
      {
        if ($counter == $page)
          $pagination.= "<span class=\"current\">$counter</span>";
        else
          $pagination.= "<a href=\"$targetpage?page=$counter&limit=$limit&startYear=$academicStartYear\">$counter</a>";                                     
      }
      $pagination.= "...";
      $pagination.= "<a href=\"$targetpage?page=$lpm1\">$lpm1</a>";
      $pagination.= "<a href=\"$targetpage?page=$lastpage\">$lastpage</a>";           
    }
    //close to end; only hide early pages
    else
    {
      $pagination.= "<a href=\"$targetpage?page=1\">1</a>";
      $pagination.= "<a href=\"$targetpage?page=2\">2</a>";
      $pagination.= "...";
      for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
      {
        if ($counter == $page)
          $pagination.= "<span class=\"current\">$counter</span>";
        else
          $pagination.= "<a href=\"$targetpage?page=$counter&limit=$limit&startYear=$academicStartYear\">$counter</a>";                                     
      }
    }
  }
  //next button
  if ($page < $counter - 1) 
     $pagination.= "<a href=\"$targetpage?page=$next\">next >></a>";
  else
     $pagination.= "<span class=\"disabled\">next >></span>";
     $pagination.= "</div>\n";               
}
?>
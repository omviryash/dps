<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$feeTypeId      = 0;
	$amount         = '';
	$remarks        = '';
	$feeStructureId = 0;
	$isEdit         = 0;
	$feeStructureArray = array();
	$todayAcademic = date('m-d');
	if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
	{
  	$academicStartYear = date('Y');
	}
	else
	{
		$academicStartYear = date('Y') - 1;
	}
	
  if(isset($_POST['Submit']))
  {
  	$nextYear            = $_POST['startYear'] + 1;
    $academicStartYear   = $_POST['startYear']."-04-01";
    $academicEndYear     = $nextYear."-03-31";
  	$feeTypeId      = isset($_POST['feeTypeId']) ? $_POST['feeTypeId'] : 0;
  	$amount         = isset($_POST['amount']) ? $_POST['amount'] : 0;
  	$remarks        = isset($_POST['remarks']) ? $_POST['remarks'] : '';
  	$feeStructureId = isset($_POST['feeStructureId']) ? $_POST['feeStructureId'] : 0;
  	if($feeStructureId == 0)
  	{
  	  $insertFeeStructure = "INSERT INTO feestructure (feeTypeId,academicStartYear,academicEndYear,amount,remarks)
  	                     VALUES('".$feeTypeId."','".$academicStartYear."','".$academicEndYear."',".$amount.",'".$remarks."')";
  	  $insertFeeStructureRes = om_query($insertFeeStructure);
  	  if(!$insertFeeStructureRes)
  	  {
  	  	echo "Insert Fail";
  	  }
  	  else
  	  {
  	  	header("Location:feeStructure.php?done=1");
  	  }
  	}
  	else
  	{
  	  $isEdit = 1;
  	  $updateFeeStructure = "UPDATE feestructure
	                         SET feeTypeId = ".$feeTypeId.",
	                             academicStartYear = '".$academicStartYear."',
	                             academicEndYear = '".$academicEndYear."',
	                             amount = ".$amount.",
	                             remarks = '".$remarks."'
	                       WHERE feeStructureId = ".$_REQUEST['feeStructureId'];
      $updateFeeStructureRes = om_query($updateFeeStructure);
      if(!$updateFeeStructureRes)
      {
      	echo "Update Fail";
      }
      else
      {
      	header("Location:feeStructure.php?done=1");
      }
  	}
  }
  
  if(isset($_REQUEST['feeStructureId']) > 0)
  {
    $selectFeeStructure = "SELECT feeStructureId,feeTypeId,academicStartYear,academicEndYear,amount,remarks
                        FROM feestructure
                       WHERE feeStructureId = ".$_REQUEST['feeStructureId'];
    $selectFeeStructureRes = mysql_query($selectFeeStructure);
    if($feeStructureRow = mysql_fetch_array($selectFeeStructureRes))
    {
    	$feeStructureId = $feeStructureRow['feeStructureId'];
    	$feeTypeId      = $feeStructureRow['feeTypeId'];
    	$amount         = $feeStructureRow['amount'];
    	$remarks        = $feeStructureRow['remarks'];
    }
  }
  
  $k = 0;
  $typeArr = array();
  $selectClub = "SELECT feeTypeId,feeType
                   FROM feetype";
  $selectClubRes = mysql_query($selectClub);
  while($clubRow = mysql_fetch_array($selectClubRes))
  {
  	$typeArr['feeTypeId'][$k]   = $clubRow['feeTypeId'];
  	$typeArr['feeType'][$k]     = $clubRow['feeType'];
  	$k++;
  }
  
  include("./bottom.php");
  $smarty->assign('typeArr',$typeArr);
  $smarty->assign('feeTypeId',$feeTypeId);
  $smarty->assign('feeStructureId',$feeStructureId);
  $smarty->assign('amount',$amount);
  $smarty->assign('remarks',$remarks);
  $smarty->assign('isEdit',$isEdit);
  $smarty->assign('feeStructureArray',$feeStructureArray);
  $smarty->assign('academicStartYear',$academicStartYear);
  $smarty->display('feeStructure.tpl');  
}
?>
<?php
include "include/config.inc.php";
include("amountToWords.php");

if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
  $nominalRollId = isset($_REQUEST['nominalRollId']) ? $_REQUEST['nominalRollId'] : 0;
  $selectNominal = "SELECT nominalroll.grNo,nominalroll.academicStartYear,nominalroll.academicEndYear,nominalroll.class,
                           nominalroll.section,nominalroll.rollNo,studentmaster.activated,studentmaster.studentName,
                           studentmaster.studentName,studentmaster.fatherName,studentmaster.gender,
                           DATE_FORMAT(nominalroll.bonafideDate, '%d-%m-%Y') AS bonafideDate,
                           DATE_FORMAT(studentmaster.joiningDate, '%d-%m-%Y') AS wefDate,
                           DATE_FORMAT(studentmaster.dateOfBirth, '%d-%m-%Y') AS dateOfBirth
                      FROM nominalroll
                 LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
                     WHERE nominalroll.nominalRollId = ".$nominalRollId."";
  $selectNominalRes = mysql_query($selectNominal);
  while($nominalRow = mysql_fetch_array($selectNominalRes))
  {
    $grNo               = $nominalRow['grNo'];
    $studentName        = ucwords(strtolower($nominalRow['studentName']));
    $fatherName         = ucwords(strtolower($nominalRow['fatherName']));
    $class              = $nominalRow['class'];
    $academicYear       = substr($nominalRow['academicStartYear'],0,4).'-'.substr($nominalRow['academicEndYear'],0,4);
    
    $gender             = $nominalRow['gender'];
    $bonafideDate       = $nominalRow['bonafideDate'];
    $wefDate            = $nominalRow['wefDate'];
    $dateOfBirth        = $nominalRow['dateOfBirth'];
    $d = date_parse_from_format("d-m-YY", $dateOfBirth);
    $dfirsttwo = words(substr($d['year'], 0, 2));
    $dlasttwo = words(substr($d['year'], 2, 2));
    if($d['month'] == 1)
	  {
	  	$dmonth = 'January';
	  }
	  if($d['month'] == 2)
	  {
	  	$classWordJoin = 'February';
	  }
	  if($d['month'] == 3)
	  {
	  	$dmonth = 'March';
	  }
	  if($d['month'] == 4)
	  {
	  	$dmonth = 'April';
	  }
	  if($d['month'] == 5)
	  {
	  	$dmonth = 'May';
	  }
	  if($d['month'] == 6)
	  {
	  	$dmonth = 'June';
	  }
	  if($d['month'] == 7)
	  {
	  	$dmonth = 'July';
	  }
	  if($d['month'] == 8)
	  {
	  	$dmonth = 'August';
	  }
	  if($d['month'] == 9)
	  {
	  	$dmonth = 'September';
	  }
	  if($d['month'] == 10)
	  {
	  	$dmonth = 'October';
	  }
          if($d['month'] == 11)
	  {
	  	$dmonth = 'November';
	  }
	  if($d['month'] == 12)
	  {
	  	$dmonth = 'December';
	  }
          
          if($d['day'] == 1)
	  {
	  	$dday = 'First';
	  }
	  if($d['day'] == 2)
	  {
	  	$dday = 'Second';
	  }
	  if($d['day'] == 3)
	  {
	  	$dday = 'Third';
	  }
	  if($d['day'] == 4)
	  {
	  	$dday = 'Fourth';
	  }
	  if($d['day'] == 5)
	  {
	  	$dday = 'Fifth';
	  }
	  if($d['day'] == 6)
	  {
	  	$dday = 'Sixth';
	  }
	  if($d['day'] == 7)
	  {
	  	$dday = 'Seventh';
	  }
	  if($d['day'] == 8)
	  {
	  	$dday = 'Eighth';
	  }
	  if($d['day'] == 9)
	  {
	  	$dday = 'Ninth';
	  }
	  if($d['day'] == 10)
	  {
	  	$dday = 'Tenth';
	  }
          if($d['day'] == 11)
	  {
	  	$dday = 'Eleventh';
	  }
	  if($d['day'] == 12)
	  {
	  	$dday = 'Twelfth';
	  }
          if($d['day'] == 13)
	  {
	  	$dday = 'Thirteenth';
	  }
          if($d['day'] == 14)
	  {
	  	$dday = 'Fourteenth';
	  }
	  if($d['day'] == 15)
	  {
	  	$dday = 'Fifteenth';
	  }
          if($d['day'] == 16)
	  {
	  	$dday = 'Sixteenth';
	  }
          if($d['day'] == 17)
	  {
	  	$dday = 'Seventeenth';
	  }
	  if($d['day'] == 18)
	  {
	  	$dday = 'Eighteenth';
	  }
          if($d['day'] == 19)
	  {
	  	$dday = 'Nineteenth';
	  }
          if($d['day'] == 20)
	  {
	  	$dday = 'Twentieth';
	  }
	  if($d['day'] == 21)
	  {
	  	$dday = 'Twenty first';
	  }
          if($d['day'] == 22)
	  {
	  	$dday = 'Twenty second';
	  }
	  if($d['day'] == 23)
	  {
	  	$dday = 'Twenty third';
	  }
          if($d['day'] == 24)
	  {
	  	$dday = 'Twenty fourth';
	  }
          if($d['day'] == 25)
	  {
	  	$dday = 'Twenty fifth';
	  }
	  if($d['day'] == 26)
	  {
	  	$dday = 'Twenty sixth';
	  }
          if($d['day'] == 27)
	  {
	  	$dday = 'Twenty seventh';
	  }
          if($d['day'] == 28)
	  {
	  	$dday = 'Twenty eighth';
	  }
	  if($d['day'] == 29)
	  {
	  	$dday = 'Twenty ninth';
	  }
          if($d['day'] == 30)
	  {
	  	$dday = 'Thirtieth';
	  }
          if($d['day'] == 31)
	  {
	  	$dday = 'Thirty first';
	  }
	  if($d['day'] == 32)
	  {
	  	$dday = 'Thirty second';
	  }
          
          $dateOfBirthWord = "$dday $dmonth, ".ucwords(strtolower(words(substr($dateOfBirth, 6, 4))));
    //$mydate = strtoTime($dateOfBirth);
    //$dateOfBirthWord = ucwords(strtolower(words(substr($dateOfBirth, 0, 2))))." '.date('F', $mydate).', '.ucwords(strtolower(words(substr($dateOfBirth, 6, 4))));
  }
  
  if($class == 1)
  {
  	$classRoma = 'I';
  	$classWord = 'First';
  }
  if($class == 2)
  {
  	$classRoma = 'II';
  	$classWord = 'Second';
  }
  if($class == 3)
  {
  	$classRoma = 'III';
  	$classWord = 'Third';
  }
  if($class == 4)
  {
  	$classRoma = 'IV';
  	$classWord = 'Fourth';
  }
  if($class == 5)
  {
  	$classRoma = 'V';
  	$classWord = 'Fifth';
  }
  if($class == 6)
  {
  	$classRoma = 'VI';
  	$classWord = 'Sixth';
  }
  if($class == 7)
  {
  	$classRoma = 'VII';
  	$classWord = 'Seventh';
  }
  if($class == 8)
  {
  	$classRoma = 'VIII';
  	$classWord = 'Eighth';
  }
  if($class == 9)
  {
  	$classRoma = 'IX';
  	$classWord = 'Ninth';
  }
  if($class == 10)
  {
  	$classRoma = 'X';
  	$classWord = 'Tenth';
  }
  if($class == 11)
  {
  	$classRoma = 'XI';
  	$classWord = 'Eleventh';
  }
  if($class == 12)
  {
  	$classRoma = 'XII';
  	$classWord = 'Twelveth';
  }
  if($class == 'Pre-Nursery')
  {
  	$classRoma = 'Pre-Nursery';
  	$classWord = '';
  }
  if($class == 'Nursery')
  {
  	$classRoma = 'Nursery';
  	$classWord = '';
  }
  if($class == 'Prep' || $class == 'PREP'  )
  {
  	$classRoma = 'Prep';
  	$classWord = '';
  }
  
  include("./bottom.php");
  $smarty->assign('studentName',$studentName);  
  $smarty->assign('fatherName',$fatherName);  
  $smarty->assign('classRoma',$classRoma);  
  $smarty->assign('classWord',$classWord);  
  $smarty->assign('academicYear',$academicYear);  
  $smarty->assign('bonafideDate',$bonafideDate);  
  $smarty->assign('gender',$gender);  
  $smarty->assign('wefDate',$wefDate);  
  $smarty->assign('dateOfBirth',$dateOfBirth);  
  $smarty->assign('dateOfBirthWord',$dateOfBirthWord);  
  $smarty->display('bonafidePrintSimple.tpl');  
}
?>
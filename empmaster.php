<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$filePath = '';
  $fileName = '';
  
  $employeeId                 = 0;
  $userType                   = '';
  $loginId                    = '';
  $password                   = '';
  $activated                  = '';
  $staffId                    = '';
  $name                       = '';
  $shortName                  = '';
  $dateOfBirth                = '';
  $gender                     = '';
  $joiningDate                = '';
  $retireDate                 = '';
  $currentAddress             = '';
  $currentAddressPin          = '';
  $correspondenceAddress      = '';
  $permanentAddress           = '';
  $residenceTelephone         = '';
  $mobile                     = '';
  $phone1                     = '';
  $phone2                     = '';
  $email                      = '';
  $fathersName                = '';
  $mothersName                = '';
  $marital                    = '';
  $spouseName                 = '';
  $spousePhone                = '';
  $pfNo                       = '';
  $accountNo                  = '';
  $panNo                      = '';
  $createdBy                  = '';
  $createdOn                  = '';
  $qualification              = '';
  $experience                 = '';
  $category                   = '';
  $designation                = '';
  $bloodGroup                 = '';
  
  
  if(isset($_POST['submitBtn']))
  {
    $uploaddir = dirname($_POST['filePath']);
  
    $uploadfile = $uploaddir . basename($_FILES['fileName']['name']);
    
    $target_path = 'data';
    $target_path = $target_path ."/". basename( $_FILES['fileName']['name']);
    $_FILES['fileName']['tmp_name']; // temp file
    
    $oldfile =  basename($_FILES['fileName']['name']);
  
    // getting the extention
  
    $pos = strpos($oldfile,".",0);
    $ext = trim(substr($oldfile,$pos+1,strlen($oldfile))," ");
    
    if(move_uploaded_file($_FILES['fileName']['tmp_name'], $target_path)) 
    {
    	$row = 0;
      $handle = fopen($target_path, "r");
      while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
      {
        if($row > 0)
        {
	        $employeeId             = $data[0];
	        $userType               = $data[2];
	        $loginId                = $data[3];
	        $password               = $data[4];
	        $activated              = $data[6];
	        $staffId                = $data[7];
	        $name                   = $data[8];
	        $shortName              = $data[9];
	        if($data[10] != '')
	        {
	          $dateOfBirth          = substr($data[10], 6, 4).'-'.substr($data[10], 3, 2).'-'.substr($data[10], 0, 2);
	        }
	        else
	        {
	        	$dateOfBirth          = '0000-00-00';
	        }
	        
	        $gender                 = $data[11];
	        if($data[12] != '')
	        {
	          $joiningDate          = substr($data[12], 6, 4)."-".substr($data[12], 3, 2)."-".substr($data[12], 0, 2);
	        }
	        else
	        {
	        	$joiningDate          = '0000-00-00';
	        }
	        if($data[15] != '')
	        {
	          $retireDate           = substr($data[15], 6, 4)."-".substr($data[15], 3, 2)."-".substr($data[15], 0, 2);
	        }
	        else
	        {
	        	$retireDate           = '0000-00-00';
	        }
	        $currentAddress         = $data[16];
	        $currentAddressPin      = $data[17];
	        $correspondenceAddress  = $data[18];
	        $permanentAddress       = $data[19];
	        $residenceTelephone     = $data[20];
	        $mobile                 = $data[21];
	        $phone1                 = $data[22];
	        $phone2                 = $data[23];
	        $email                  = $data[24];
	        $fathersName            = $data[25];
	        $mothersName            = $data[26];
	        $marital                = $data[27];
	        $spouseName             = $data[28];
	        $spousePhone            = $data[29];
	        $pfNo                   = $data[30];
	        $accountNo              = $data[31];
	        $panNo                  = $data[32];
	        $createdBy              = $data[33];
	        if($data[34] != '')
	        {
	          $createdOn            = substr($data[34], 6, 4)."-".substr($data[34], 3, 2)."-".substr($data[34], 0, 2);
	        }
	        else
	        {
	        	$createdOn            = '0000-00-00';
	        }
	        $qualification          = $data[36];
	        $experience             = $data[37];
	        $category               = $data[38];
	        $designation            = $data[39];
	        if (!isset($data[40]))
					{
					  $bloodGroup    = '';
				  }
				  else
				  {
				  	$bloodGroup    = $data[40];
				  }

         
          $insertempMaster = "INSERT INTO employeemaster 
                                          (employeeId,userType,loginId,password,
         	                                 activated,staffId,name,shortName,dateOfBirth,gender,joiningDate,
         	                                 retireDate,currentAddress,currentAddressPin,correspondenceAddress,
         	                                 permanentAddress,residenceTelephone,mobile,phone1,phone2,email,
         	                                 fathersName,mothersName,marital,spouseName,spousePhone,pfNo,
         	                                 accountNo,panNo,createdBy,createdOn,qualification,experience,
         	                                 category,designation,bloodGroup)
        	                         VALUES (".$employeeId.",'".$userType."','".$loginId."','".$password."',
        	                                 '".$activated."','".$staffId."','".$name."','".$shortName."',
        	                                 '".$dateOfBirth."','".$gender."','".$joiningDate."','".$retireDate."',
        	                                 '".addslashes($currentAddress)."','".$currentAddressPin."','".addslashes($correspondenceAddress)."',
        	                                 '".addslashes($permanentAddress)."','".$residenceTelephone."','".$mobile."',
        	                                 '".$phone1."','".$phone2."','".$email."','".$fathersName."',
        	                                 '".$mothersName."','".$marital."','".$spouseName."','".$spousePhone."',
        	                                 '".$pfNo."','".$accountNo."','".$panNo."','".$createdBy."','".$createdOn."',
         	                                 '".$qualification."','".$experience."','".$category."','".$designation."','".$bloodGroup."')";
          om_query($insertempMaster);
        }
        $row++;
      }
    } 
    else
    {
      echo "There was an error uploading the file, please try again!";
    }
  }
}
?>
<BODY>
<H1><CENTER><FONT color="red">Employee Profile</FONT></CENTER></H1><HR>
<CENTER>
<FORM enctype="multipart/form-data" action="" name="empMaster" method="POST"> 
	<a href="index.php">Home</a>
	<br /><br />
  <INPUT size="60" type="hidden" name="filePath"><BR>
  File Name:
  <INPUT name="fileName" type="file" onChange="document.empMaster.filePath.value=document.empMaster.fileName.value;">
  <INPUT type="submit" name="submitBtn" value="Submit File"> 
</FORM>
</CENTER>
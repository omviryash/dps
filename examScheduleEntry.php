<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$examTypeId      = 0;
	$class           = "";
	$maxMarks        = "";
	$minMarks        = "";
	$studentView     = 0;
	$subjectMasterId = "";
	$isEdit          = 0;
	$examScheduleId  = 0;
	$scheduleDate    = date('Y-m-d');
	
	$classArray      = array();
	
  if(isset($_POST['Submit']))
  {
  	$examTypeId      = isset($_POST['examTypeId']) ? $_POST['examTypeId'] : 0;
  	$scheduleDate    = $_POST['scheduleDateYear']."-".$_POST['scheduleDateMonth']."-".$_POST['scheduleDateDay'];
  	$maxMarks        = isset($_POST['maxMarks']) && $_POST['maxMarks'] != '' ? $_POST['maxMarks'] : 0;
  	$minMarks        = isset($_POST['minMarks']) && $_POST['minMarks'] != '' ? $_POST['minMarks'] : 0;
  	$subjectMasterId = isset($_POST['subjectMasterId']) ? $_POST['subjectMasterId'] : 0;
  	$class           = isset($_POST['class']) ? $_POST['class'] : '';
  	$studentView     = isset($_POST['studentView']) && $_POST['studentView'] != '' ? $_POST['studentView'] : '';
  	
  	$examScheduleId  = isset($_POST['examScheduleId']) ? $_POST['examScheduleId'] : 0;
  	
  	if($examScheduleId == 0)
  	{
  	  $insertClass = "INSERT INTO examschedule (examTypeId,scheduleDate,maxMarks,minMarks,subjectMasterId,class,studentView)
  	                  VALUES (".$examTypeId.",'".$scheduleDate."','".$maxMarks."','".$minMarks."','".$subjectMasterId."','".$class."','".$studentView."')";
  	  $insertClassRes = om_query($insertClass);
  	  if(!$insertClassRes)
  	  {
  	  	echo "Insert Fail";
  	  }
  	  else
  	  {
  	  	header("Location:examScheduleEntry.php?done=1");
  	  }
  	}
  	else
  	{
  	  $isEdit = 1;
  	  $updateClass = "UPDATE examschedule
  	                     SET examTypeId      = ".$examTypeId.",
  	                         scheduleDate    = '".$scheduleDate."',
  	                         maxMarks        = '".$maxMarks."',
  	                         minMarks        = '".$minMarks."',
  	                         subjectMasterId = '".$subjectMasterId."',
  	                         class           = '".$class."',
  	                         studentView     = '".$studentView."'
  	                   WHERE examScheduleId = ".$_REQUEST['examScheduleId'];
      $updateClassRes = om_query($updateClass);
      if(!$updateClassRes)
      {
      	echo "Update Fail";
      }
      else
      {
      	header("Location:examScheduleEntry.php?done=1");
      }
  	}
  }
  
  $i = 0;
  $selectClass = "SELECT examschedule.examScheduleId,examschedule.scheduleDate,
                         examschedule.maxMarks,examschedule.minMarks,examschedule.class,
                         examtype.examType,subjectmaster.subjectName,examschedule.studentView
                    FROM examschedule
               LEFT JOIN examtype ON examtype.examTypeId = examschedule.examTypeId
               LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = examschedule.subjectMasterId
                ORDER BY examschedule.scheduleDate DESC";
  $selectClassRes = mysql_query($selectClass);
  while($classRow = mysql_fetch_array($selectClassRes))
  {
  	$classArray[$i]['examScheduleId'] = $classRow['examScheduleId'];
  	$classArray[$i]['examType']       = $classRow['examType'];
  	$classArray[$i]['scheduleDate']   = $classRow['scheduleDate'];
  	$classArray[$i]['maxMarks']       = $classRow['maxMarks'];
  	$classArray[$i]['minMarks']       = $classRow['minMarks'];
  	$classArray[$i]['class']          = $classRow['class'];
  	$classArray[$i]['subjectName']    = $classRow['subjectName'];
  	$classArray[$i]['studentView']    = $classRow['studentView'];
  	$i++;
  }
  
  if(isset($_REQUEST['examScheduleId']) > 0)
  {
    $selectClass = "SELECT examschedule.examScheduleId,examschedule.scheduleDate,
	                         examschedule.maxMarks,examschedule.minMarks,examschedule.class,
	                         examtype.examTypeId,subjectmaster.subjectMasterId,examschedule.studentView
	                    FROM examschedule
	               LEFT JOIN examtype ON examtype.examTypeId = examschedule.examTypeId
	               LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = examschedule.subjectMasterId
                     WHERE examScheduleId = ".$_REQUEST['examScheduleId']."";
    $selectClassRes = mysql_query($selectClass);
    if($classRow = mysql_fetch_array($selectClassRes))
    {
    	$examScheduleId  = $classRow['examScheduleId'];
    	$examTypeId      = $classRow['examTypeId'];
    	$scheduleDate    = $classRow['scheduleDate'];
    	$class           = $classRow['class'];
    	$maxMarks        = $classRow['maxMarks'];
    	$minMarks        = $classRow['minMarks'];
    	$subjectMasterId = $classRow['subjectMasterId'];
    	$studentView     = $classRow['studentView'];
    }
  }
  
  $p=0;
	$exArray = array();
	$selectExamType = "SELECT examTypeId,examType
                       FROM examtype";
	$selectExamTypeRes = mysql_query($selectExamType);
	while($exRow = mysql_fetch_array($selectExamTypeRes))
	{
	  $exArray['examTypeId'][$p] = $exRow['examTypeId'];
	  $exArray['examType'][$p]   = $exRow['examType'];
	  $p++;
	}
	
	$c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  $k = 0;
  $selectClub = "SELECT subjectMasterId,subjectName
                   FROM subjectmaster
                  WHERE subjectType = 'Main' OR subjectType = 'Optional'";
  $selectClubRes = mysql_query($selectClub);
  while($clubRow = mysql_fetch_array($selectClubRes))
  {
  	$clubArr['subjectMasterId'][$k]   = $clubRow['subjectMasterId'];
  	$clubArr['subjectName'][$k]       = $clubRow['subjectName'];
  	$k++;
  }
	
  $yesNoArrOut[0] = 'Yes';
  $yesNoArrOut[1] = 'No';
  
  include("./bottom.php");
  $smarty->assign('examScheduleId',$examScheduleId);
  $smarty->assign('class',$class);
  $smarty->assign('isEdit',$isEdit);
  $smarty->assign('subjectMasterId',$subjectMasterId);
  $smarty->assign('cArray',$cArray);
  $smarty->assign('yesNoArrOut',$yesNoArrOut);
  $smarty->assign('classArray',$classArray);
  $smarty->assign('exArray',$exArray);
  $smarty->assign('clubArr',$clubArr);
  $smarty->assign('scheduleDate',$scheduleDate);
  $smarty->assign('maxMarks',$maxMarks);
  $smarty->assign('minMarks',$minMarks);
  $smarty->assign('studentView',$studentView);
  $smarty->assign('examTypeId',$examTypeId);
  $smarty->display('examScheduleEntry.tpl');  
}
?>
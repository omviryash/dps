<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$class    = '';
  $section  = '';
  $count    = 0;
  $today    = date('Y-m-d');
	$stdArray = array();
	
	if(isset($_REQUEST['goDateYear']))
	{
		if(isset($_REQUEST['goDateMonth']) && isset($_REQUEST['goDateDay']))
		{
			$todayAcademic = $_REQUEST['goDateMonth']."-".$_REQUEST['goDateDay'];
		}
		else
		{
		  $todayAcademic = date('m-d');
		}
		
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = $_REQUEST['goDateYear']."-04-01";
	  	$nextYear          = $_REQUEST['goDateYear'] + 1;
	  	$academicEndYear   = $nextYear."-03-31";
		}
		else
		{
			$prevYear          = $_REQUEST['goDateYear'] - 1;
			$academicStartYear = $prevYear."-04-01";
	  	$academicEndYear   = $_REQUEST['goDateYear']."-03-31";
		}
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y')."-04-01";
	  	$nextYear            = date('Y') + 1;
	  	$academicEndYear   = $nextYear."-03-31";
		}
		else
		{
			$prevYear          = date('Y') - 1;
			$academicStartYear = $prevYear."-04-01";
	  	$academicEndYear   = date('Y')."-03-31";
		}
	}
	
	if(isset($_REQUEST['goDateYear']))
	{
	  $goDate = $_REQUEST['goDateYear']."-".$_REQUEST['goDateMonth']."-".$_REQUEST['goDateDay'];
	}
  else
  {
    $goDate = $today;
  }
	
	$class   = isset($_GET['class']) && $_GET['class'] != '' ? $_GET['class'] : 0;
	$section = isset($_GET['section']) && $_GET['section'] != '' ? $_GET['section'] : 0;
	
	$yearDay  = substr($goDate, 0, 4);
	$monthDay = substr($goDate, 5, 2);
	$dateDay  = substr($goDate, 8, 2);
	$jd=cal_to_jd(CAL_GREGORIAN,date($monthDay),date($dateDay),date($yearDay));
  $weekDay = (jddayofweek($jd,1));

	$selectId = "SELECT employeeMasterId
                 FROM employeemaster
                WHERE loginId = '".$_SESSION['s_activName']."'";
  $selectIdRes = mysql_query($selectId);
  if($idRow = mysql_fetch_array($selectIdRes))
  {
  	$selectClass = "SELECT class,section
	                    FROM classteacherallotment
	                   WHERE employeeMasterId = '".$idRow['employeeMasterId']."'
	                     AND academicStartYear = '".$academicStartYear."'
	                     AND academicEndYear = '".$academicEndYear."'";
	  $selectClassRes = mysql_query($selectClass);
	  if($classRow = mysql_fetch_array($selectClassRes))
	  {
	  	if($class == 0)
	  	{
		  	$class   = $classRow['class'];
		  	$section = $classRow['section'];
		  }
		}
  	if(isset($_GET['submit']))
    {
		  $selectStdIn = "SELECT nominalroll.grNo,nominalroll.rollNo,studentmaster.gender,studentmaster.activated,
		                         nominalroll.academicStartYear,nominalroll.academicEndYear,studentmaster.joiningDate
		                    FROM nominalroll
		               LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
		                   WHERE class = '".$class."'
		                     AND section = '".$section."'
		                     AND studentmaster.joiningDate <= '".$goDate."'
		                     AND nominalroll.academicStartYear = '".$academicStartYear."'
	                       AND nominalroll.academicEndYear = '".$academicEndYear."'
	                       AND activated = 'Y'";
		  $selectStdInRes = mysql_query($selectStdIn);
		  while($stdInRow = mysql_fetch_array($selectStdInRes))
		  {
		  	$rollNo      = $stdInRow['rollNo'];
		  	$grNo        = $stdInRow['grNo'];
		  	$gender      = $stdInRow['gender'];
		  	
		  	$selectCheck = "SELECT grNo
			                    FROM attendence
			                   WHERE class = '".$class."'
			                     AND section = '".$section."'
			                     AND date = '".$goDate."'
		                       AND grNo = '".$grNo."'";
			  $selectCheckRes = mysql_query($selectCheck);
			  $genCount = mysql_num_rows($selectCheckRes);
			  
			  if($genCount == 0)
			  {
			  	$insertClass = "INSERT INTO attendence (grNo,rollNo,date,class,section,gender)
		  	                  VALUES('".$grNo."','".$rollNo."','".$goDate."','".$class."','".$section."','".$gender."')";
		  	  $insertClassRes = om_query($insertClass);
		  	  if(!$insertClassRes)
		  	  {
		  	  	echo "Insert Fail";
		  	  }
		  	  else
		  	  {
		  	  	header("Location:reGenerateAttendence.php?goDateYear=".$_GET['goDateYear']."&goDateMonth=".$_GET['goDateMonth']."&goDateDay=".$_GET['goDateDay']."");
		  	  }
		  	}
		  }
		}
		if(isset($_GET['submitTaken']))
    {
    	$selectStdIn = "SELECT attendenceId
		                    FROM attendence
		                   WHERE class = '".$class."'
		                     AND section = '".$section."'
		                     AND date = '".$goDate."'";
		  $selectStdInRes = mysql_query($selectStdIn);
		  while($stdInRow = mysql_fetch_array($selectStdInRes))
		  {
		  	$attendenceId = $stdInRow['attendenceId'];
		  	
		  	$updateClass = "UPDATE attendence
		  	                   SET taken = 'AT'
		  	                 WHERE attendenceId = '".$attendenceId."'";
	  	  $updateClassRes = om_query($updateClass);
	  	  if(!$updateClassRes)
	  	  {
	  	  	echo "Update Fail";
	  	  }
	  	  else
	  	  {
	  	  	header("Location:reGenerateAttendence.php?goDateYear=".$_GET['goDateYear']."&goDateMonth=".$_GET['goDateMonth']."&goDateDay=".$_GET['goDateDay']."");
	  	  }
		  }
    }
		$i = 0;
	  $selectStd = "SELECT attendence.grNo,attendence.rollNo,studentmaster.gender,studentmaster.studentName,attendence.attendences
	                  FROM attendence
	             LEFT JOIN studentmaster ON studentmaster.grNo = attendence.grNo
	                 WHERE attendence.class = '".$class."'
	                   AND attendence.section = '".$section."'
	                   AND attendence.date = '".$goDate."'
	                   AND activated = 'Y'
	              ORDER BY rollNo";
	  $selectStdRes = mysql_query($selectStd);
	  $count = mysql_num_rows($selectStdRes);
	  while($stdRow = mysql_fetch_array($selectStdRes))
	  {
	  	$stdArray[$i]['grNo']        = $stdRow['grNo'];
	  	$stdArray[$i]['rollNo']      = $stdRow['rollNo'];
	  	$stdArray[$i]['gender']      = $stdRow['gender'];
	  	$stdArray[$i]['studentName'] = $stdRow['studentName'];
	  	$stdArray[$i]['attendences'] = $stdRow['attendences'];
	  	$i++;
	  }
	}
	
	$c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  $secArrOut[0] = 'A';
  $secArrOut[1] = 'B';
  $secArrOut[2] = 'C';
  $secArrOut[3] = 'D';
  
  include("./bottom.php");
  $smarty->assign('cArray',$cArray);
  $smarty->assign('secArrOut',$secArrOut);
  $smarty->assign('class',$class);
  $smarty->assign('section',$section);
  $smarty->assign('stdArray',$stdArray);
  $smarty->assign('count',$count);
  $smarty->assign('goDate',$goDate);
  $smarty->assign('weekDay',$weekDay);
  $smarty->display('reGenerateAttendence.tpl');  
}
?>
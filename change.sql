ALTER TABLE  `nominalroll` ADD  `resultRemark1` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER  `busStop` ,
ADD  `resultRemark2` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER  `resultRemark1` ,
ADD  `enEducation` CHAR( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER  `resultRemark2` ,
ADD  `phEducation` CHAR( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER  `enEducation`;

ALTER TABLE  `nominalroll` ADD  `enEducation2` CHAR( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER  `phEducation` ,
ADD  `phEducation2` CHAR( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER  `enEducation2`;
<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$nominalArr = array();
	if(isset($_REQUEST['startYear']))
  {
  	$academicStartYear = $_REQUEST['startYear']."-04-01";
  	$nextYear          = $_REQUEST['startYear'] + 1;
  	$academicEndYear   = $nextYear."-03-31";
  	$academicStartYearSelected = $_REQUEST['startYear'];
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y')."-04-01";
	  	$nextYear          = date('Y') + 1;
	  	$academicEndYear   = $nextYear."-03-31";
	  	$academicStartYearSelected = date('Y');
		}
		else
		{
			$prevYear          = date('Y') - 1;
			$academicStartYear = $prevYear."-04-01";
	  	$academicEndYear   = date('Y')."-03-31";
	  	$academicStartYearSelected = $prevYear;
		}
	}
	
	$subjectAltId = isset($_REQUEST['subjectAltId']) && $_REQUEST['subjectAltId'] != '' ? $_REQUEST['subjectAltId'] : '0_0_0';
	
	$subjectAltIdExp = explode("_",$subjectAltId);
	$subjectAltIdExp[0];
	$subjectAltIdExp[1];
	$subjectAltIdExp[2];
	
	if(isset($_REQUEST['class']))
	{
	  $class           = isset($_REQUEST['class']) && $_REQUEST['class'] != '' ? $_REQUEST['class'] : 0;
		$subjectMasterId = isset($_REQUEST['subjectMasterId']) && $_REQUEST['subjectMasterId'] != '' ? $_REQUEST['subjectMasterId'] : 0;
	}
	else
	{
		$class           = $subjectAltIdExp[0];
		$subjectMasterId = $subjectAltIdExp[2];
	}
	
	$selectId = "SELECT employeeMasterId
                 FROM employeemaster
                WHERE loginId = '".$_SESSION['s_activName']."'";
  $selectIdRes = mysql_query($selectId);
  if($idRow = mysql_fetch_array($selectIdRes))
  {
  	$selectClass = "SELECT class
	                    FROM classteacherallotment
	                   WHERE employeeMasterId = '".$idRow['employeeMasterId']."'
	                     AND academicStartYear = '".$academicStartYear."'
	                     AND academicEndYear = '".$academicEndYear."'";
	  $selectClassRes = mysql_query($selectClass);
	  if($classRow = mysql_fetch_array($selectClassRes))
	  {
	  	if($class == 0)
	  	{
		  	$class   = $classRow['class'];
		  }
	  }
	  $i = 0;
	  $selectNominal = "SELECT onlinetest.onlineTestId,onlinetest.class,subjectmaster.subjectName,
	                           onlinetest.subjectMasterId,onlinetest.question,onlinetest.qNo, 
	                           onlinetest.option1,onlinetest.option2,onlinetest.option3,onlinetest.option4
	                      FROM onlinetest
	                 LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = onlinetest.subjectMasterId
	                     WHERE onlinetest.class = '".$class."'
	                       AND onlinetest.subjectMasterId = '".$subjectMasterId."'
	                  ORDER BY onlinetest.onlineTestId desc";
	  $selectNominalRes = mysql_query($selectNominal);
	  while($nominalRow = mysql_fetch_array($selectNominalRes))
	  {
	    $nominalArr[$i]['onlineTestId']    = $nominalRow['onlineTestId'];
	    $nominalArr[$i]['class']           = $nominalRow['class'];
	    $nominalArr[$i]['subjectMasterId'] = $nominalRow['subjectMasterId'];
	    $nominalArr[$i]['subjectName']     = $nominalRow['subjectName'];
	    $nominalArr[$i]['qNo']             = $nominalRow['qNo'];
	    $nominalArr[$i]['question']        = $nominalRow['question'];
	    $nominalArr[$i]['option1']        = $nominalRow['option1'];
	    $nominalArr[$i]['option2']        = $nominalRow['option2'];
	    $nominalArr[$i]['option3']        = $nominalRow['option3'];
	    $nominalArr[$i]['option4']        = $nominalRow['option4'];
	    $i++;
	  }
	}
  
  $clubArr  = array();
  $clubArr2 = array();
	$k = 0;
	$selectClub1 = "SELECT subjectMasterId,subjectName,subjectType
	                  FROM subjectmaster
	                 WHERE subjectType = 'Main' OR subjectType = 'Optional'
	              ORDER BY subjectName";
	$selectClub1Res = mysql_query($selectClub1);
	while($club1Row = mysql_fetch_array($selectClub1Res))
	{
		$clubArr['subjectMasterId'][$k]   = $club1Row['subjectMasterId'];
		$clubArr['subjectName'][$k]       = $club1Row['subjectName'];
		$k++;
	}
	
	if(isset($_POST['submit']))
	{
		if(isset($_POST['classNew']))
		{
		  $classNew = isset($_POST['classNew']) && $_POST['classNew'] != '' ? $_POST['classNew'] : 0;
		  $subjectMasterId = isset($_POST['subjectMasterId']) && $_POST['subjectMasterId'] != '' ? $_POST['subjectMasterId'] : 0;
		}
		else
		{
		  $classNew = 0;
		  $subjectMasterId = 0;
		}
		$scheduleDate = $_POST['scheduleDateYear'].'-'.$_POST['scheduleDateMonth'].'-'.$_POST['scheduleDateDay'];
		
		$scheduleEntry = "INSERT INTO onlineschedule (scheduleDate,class,subjectMasterId)
	  										 VALUES ('".$scheduleDate."','".$classNew."',".$subjectMasterId.")";
	  $scheduleEntryRes = om_query($scheduleEntry);
	  $scheduleMasterId = mysql_insert_id();
		$loopCount1 = 0;
	  while($loopCount1 < count($_POST['onlineTestId']))
	  {
	    $onlineTestId  = isset($_POST['onlineTestId'][$loopCount1]) && $_POST['onlineTestId'][$loopCount1] != '' ? $_POST['onlineTestId'][$loopCount1] : 0;
	    $scheduled     = isset($_POST['scheduled'][$loopCount1]) && $_POST['scheduled'][$loopCount1] != '' ? $_POST['scheduled'][$loopCount1] : 'No';
		  
	    if($scheduled == 'Yes')
	    {
	      $nominalEntry = "INSERT INTO onlinescheduledtl (scheduleMasterId,onlineTestId)
			  										 VALUES (".$scheduleMasterId.",".$onlineTestId.")";
			  $nominalEntryRes = om_query($nominalEntry);
			  if(!$nominalEntryRes)
			  {
			  	echo "Update Fail";
			  }
			  else
			  {
			    header("Location:onlineTestSchedule.php?done=1");
			  }
	    }
	    $loopCount1++;
	  }
  }
  
  $scdArray = array();
  $f = 0;
	$selectId = "SELECT employeeMasterId
                 FROM employeemaster
                WHERE loginId = '".$_SESSION['s_activName']."'";
  $selectIdRes = mysql_query($selectId);
  if($subRow = mysql_fetch_array($selectIdRes))
  {
  	$selectSub = "SELECT subjectteacherallotment.class,subjectteacherallotment.section,subjectteacherallotment.subjectMasterId,subjectmaster.subjectName
                    FROM subjectteacherallotment
               LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = subjectteacherallotment.subjectMasterId
                   WHERE subjectteacherallotment.employeeMasterId = '".$subRow['employeeMasterId']."'
                     AND subjectteacherallotment.academicStartYear = '".$academicStartYear."'
                     AND subjectteacherallotment.academicEndYear = '".$academicEndYear."'";
	  $selectSubRes = mysql_query($selectSub);
	  while($subRow = mysql_fetch_array($selectSubRes))
	  {
	  	$classTeacherSubjectClass    = $subRow['class'];
	  	$classTeacherSubjectsection  = $subRow['section'];
	  	$classTeachersubjectMasterId = $subRow['subjectMasterId'];
	  	$subjectName = $subRow['subjectName'];
		  
	  	$scdArray['subjectAltId'][$f] = $subRow['class'].'_'.$subRow['section'].'_'.$subRow['subjectMasterId'];
	  	$scdArray['subjectDtl'][$f]   = $subRow['class'].'_'.$subRow['section'].'_'.$subRow['subjectName'];
	  	$f++;
	  }
  }
  
  $c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  include("./bottom.php");
  $smarty->assign('class',$class);
  $smarty->assign('cArray',$cArray);
  $smarty->assign('clubArr',$clubArr);
  $smarty->assign('clubArr2',$clubArr2);
  $smarty->assign('nominalArr',$nominalArr);
  $smarty->assign('academicStartYear',$academicStartYear);
  $smarty->assign('academicEndYear',$academicEndYear);
  $smarty->assign('academicStartYearSelected',$academicStartYearSelected);
  $smarty->assign('subjectMasterId',$subjectMasterId);
  $smarty->assign('scdArray',$scdArray);
  $smarty->assign('subjectAltId',$subjectAltId);
  $smarty->display('onlineTestSchedule.tpl');  
}
?>
<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$houseArray          = array();
  $studentMasterId     = isset($_REQUEST['studentMasterId']) ? $_REQUEST['studentMasterId'] : 0;
  $studentName         = "";
  $fatherName          = "";
  $grNo                = "";
  $dateOfBirth         = "";
  $joinedInClass       = "";
  $joiningDate         = "";
  $studentLoginId      = "";
	$studentPassword     = "";
	$parentLoginId       = "";
	$parentPassword      = "";
  $gender              = "";
  $bloodGroup          = "";
  $currentAddress      = "";
  $currentAddressPin   = "";
  $residencePhone1     = "";
  $residencePhone2     = "";
  $smsMobile		   = "";
  $permanentAddress    = "";
  $permanentState      = "";
  $permanentPIN        = "";
  $religion            = "";
  $previousSchool      = "";
  $studentEmail        = "";
  $fatherMobile        = "";
  $fatherPhone         = "";
  $urban               = "";
  $currentState        = "";
  $fatherEmail         = "";
  $fatherOccupation    = "";
  $fathersDesignation  = "";
  $fathersOrganization = "";
  $mothersName         = "";
  $mothersPhone        = "";
  $mothersEmailid      = "";
  $mothersMobile       = "";
  $motherOccupation    = "";
  $mothersDesignation  = "";
  $createdOn           = "";
  $mothersOrganization = "";
  $houseId             = "";
  $activated           = "";
  $studentImage        = "";
  $fatherImage         = "";
  $motherImage         = "";
  $aadhaar         = "";
  
  if(isset($_REQUEST['startYear']))
	{
  	$academicStartYear = $_REQUEST['startYear']."-04-01";
  	$nextYear          = $_REQUEST['startYear'] + 1;
  	$academicEndYear   = $nextYear."-03-31";
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYearselected = date('Y');
	  	$academicStartYear = date('Y')."-04-01";
	  	$nextYear          = date('Y') + 1;
	  	$academicEndYear   = $nextYear."-03-31";
		}
		else
		{
			$prevYear          = date('Y') - 1;
			$academicStartYear = $prevYear."-04-01";
	  	$academicEndYear   = date('Y')."-03-31";
	  	$academicStartYearselected = $prevYear;
		}
	}

	if(isset($_POST['submit']))
	{
	  $studentId           = isset($_REQUEST['studentId']) && $_REQUEST['studentId'] != '' ? $_REQUEST['studentId'] : 0;
	  $grNo                = isset($_REQUEST['grNo']) && $_REQUEST['grNo'] != '' ? $_REQUEST['grNo'] : 0;
	  $userType            = 'Student';
	  $studentLoginId      = isset($_REQUEST['studentLoginId']) && $_REQUEST['studentLoginId'] != '' ? $_REQUEST['studentLoginId'] : 's/'.$grNo;
	  $studentPassword     = isset($_REQUEST['studentPassword']) && $_REQUEST['studentPassword'] != '' ? $_REQUEST['studentPassword'] : 'dps'.$grNo;
	  $parentLoginId       = isset($_REQUEST['parentLoginId']) && $_REQUEST['parentLoginId'] != '' ? $_REQUEST['parentLoginId'] : 'p/'.$grNo;
	  $parentPassword      = isset($_REQUEST['parentPassword']) && $_REQUEST['parentPassword'] != '' ? $_REQUEST['parentPassword'] : 'dps'.$grNo;
	  $activated           = isset($_REQUEST['activated']) && $_REQUEST['activated'] != '' ? $_REQUEST['activated'] : "";
	  $joinedInClass       = isset($_REQUEST['joinedInClass']) && $_REQUEST['joinedInClass'] != '' ? $_REQUEST['joinedInClass'] : "";
	  $joiningDate         = $_POST['joiningYear']."-".$_POST['joiningMonth']."-".$_POST['joiningDay'];
	  $studentName         = isset($_REQUEST['studentName']) && $_REQUEST['studentName'] != '' ? $_REQUEST['studentName'] : "";
	  $dateOfBirth         = $_POST['dobYear']."-".$_POST['dobMonth']."-".$_POST['dobDay'];
	  $gender              = isset($_REQUEST['gender']) && $_REQUEST['gender'] != '' ? $_REQUEST['gender'] : "";
	  $bloodGroup          = isset($_REQUEST['bloodGroup']) && $_REQUEST['bloodGroup'] != '' ? $_REQUEST['bloodGroup'] : "";
	  $currentAddress      = isset($_REQUEST['currentAddress']) && $_REQUEST['currentAddress'] != '' ? $_REQUEST['currentAddress'] : "";
	  $currentState        = isset($_REQUEST['currentState']) && $_REQUEST['currentState'] != '' ? $_REQUEST['currentState'] : "";
	  $currentAddressPin   = isset($_REQUEST['currentAddressPin']) && $_REQUEST['currentAddressPin'] != '' ? $_REQUEST['currentAddressPin'] : "";
	  $residencePhone1     = isset($_REQUEST['residencePhone1']) && $_REQUEST['residencePhone1'] != '' ? $_REQUEST['residencePhone1'] : "";
	  $residencePhone2     = isset($_REQUEST['residencePhone2']) && $_REQUEST['residencePhone2'] != '' ? $_REQUEST['residencePhone2'] : "";
	  $smsMobile           = isset($_REQUEST['smsMobile']) && $_REQUEST['smsMobile'] != '' ? $_REQUEST['smsMobile'] : "";
	  $studentEmail        = isset($_REQUEST['studentEmail']) && $_REQUEST['studentEmail'] != '' ? $_REQUEST['studentEmail'] : "";
	  $fatherName          = isset($_REQUEST['fatherName']) && $_REQUEST['fatherName'] != '' ? $_REQUEST['fatherName'] : "";
	  $fatherPhone         = isset($_REQUEST['fatherPhone']) && $_REQUEST['fatherPhone'] != '' ? $_REQUEST['fatherPhone'] : "";
	  $fatherEmail         = isset($_REQUEST['fatherEmail']) && $_REQUEST['fatherEmail'] != '' ? $_REQUEST['fatherEmail'] : "";
	  $fatherMobile        = isset($_REQUEST['fatherMobile']) && $_REQUEST['fatherMobile'] != '' ? $_REQUEST['fatherMobile'] : "";
	  $fatherOccupation    = isset($_REQUEST['fatherOccupation']) && $_REQUEST['fatherOccupation'] != '' ? $_REQUEST['fatherOccupation'] : "";
	  $fathersDesignation  = isset($_REQUEST['fathersDesignation']) && $_REQUEST['fathersDesignation'] != '' ? $_REQUEST['fathersDesignation'] : "";
	  $fathersOrganization = isset($_REQUEST['fathersOrganization']) && $_REQUEST['fathersOrganization'] != '' ? $_REQUEST['fathersOrganization'] : "";
	  $mothersName         = isset($_REQUEST['mothersName']) && $_REQUEST['mothersName'] != '' ? $_REQUEST['mothersName'] : "";
	  $mothersPhone        = isset($_REQUEST['mothersPhone']) && $_REQUEST['mothersPhone'] != '' ? $_REQUEST['mothersPhone'] : "";
	  $mothersEmailid      = isset($_REQUEST['mothersEmailid']) && $_REQUEST['mothersEmailid'] != '' ? $_REQUEST['mothersEmailid'] : "";
	  $mothersMobile       = isset($_REQUEST['mothersMobile']) && $_REQUEST['mothersMobile'] != '' ? $_REQUEST['mothersMobile'] : "";
	  $motherOccupation    = isset($_REQUEST['motherOccupation']) && $_REQUEST['motherOccupation'] != '' ? $_REQUEST['motherOccupation'] : "";
	  $mothersDesignation  = isset($_REQUEST['mothersDesignation']) && $_REQUEST['mothersDesignation'] != '' ? $_REQUEST['mothersDesignation'] : "";
	  $mothersOrganization = isset($_REQUEST['mothersOrganization']) && $_REQUEST['mothersOrganization'] != '' ? $_REQUEST['mothersOrganization'] : "";
	  $permanentAddress    = isset($_REQUEST['permanentAddress']) && $_REQUEST['permanentAddress'] != '' ? $_REQUEST['permanentAddress'] : "";
	  $permanentState      = isset($_REQUEST['permanentState']) && $_REQUEST['permanentState'] != '' ? $_REQUEST['permanentState'] : "";
	  $permanentPIN        = isset($_REQUEST['permanentPIN']) && $_REQUEST['permanentPIN'] != '' ? $_REQUEST['permanentPIN'] : "";
	  $urban               = isset($_REQUEST['urban']) && $_REQUEST['urban'] != '' ? $_REQUEST['urban'] : "";
	  $religion            = isset($_REQUEST['religion']) && $_REQUEST['religion'] != '' ? $_REQUEST['religion'] : "";
	  $createdOn           = $_POST['createdonYear']."-".$_POST['createdonMonth']."-".$_POST['createdonDay'];
	  $previousSchool      = isset($_REQUEST['previousSchool']) && $_REQUEST['previousSchool'] != '' ? $_REQUEST['previousSchool'] : "";
	  $houseId             = isset($_REQUEST['houseId']) && $_REQUEST['houseId'] != '' ? $_REQUEST['houseId'] : 0;
    $studentImageIn      = '<img height="150" src="studentImage/'.$grNo.'_P.jpg" alt="'.$grNo.'_P.jpg" class="studentImage"><p id="large"></p>';
		$fatherImageIn       = '<img height="150" src="studentImage/'.$grNo.'_F.jpg" alt="'.$grNo.'_F.jpg" class="studentImage"><p id="large"></p>';
		$motherImageIn       = '<img height="150" src="studentImage/'.$grNo.'_M.jpg" alt="'.$grNo.'_M.jpg" class="studentImage"><p id="large"></p>';
		$aadhaar      = isset($_REQUEST['aadhaar']) && $_REQUEST['aadhaar'] != '' ? $_REQUEST['aadhaar'] : "";
    
    if(isset($_POST['studentMasterId']) && $_POST['studentMasterId'] == 0)
    {
		  $studentEntry = "INSERT INTO studentmaster 
		                                 (studentId,
		  																grNo,
		  																userType,
		  																studentLoginId,
		  																studentPassword,
		  																parentLoginId,
		  																parentPassword,
		  																activated,
		  																joinedInClass,
		  																joiningDate,
		  																studentName,
		  																dateOfBirth,
		  																gender,
		  																bloodGroup,
		  																houseId,
		  																currentAddress,
		  																currentState,
		  																currentAddressPin,
		  																residencePhone1,
		  																residencePhone2,
		  																smsMobile,
		  																studentEmail,
		  																fatherName,
		  																fatherPhone,
		  																fatherEmail,
		  																fatherMobile,
		  																fatherOccupation,
		  																fathersDesignation,
		  																fathersOrganization,
		  																mothersName,
		  																mothersPhone,
		  																mothersEmailid,
		  																mothersMobile,
		  																motherOccupation,
		  																mothersDesignation,
		  																mothersOrganization,
		  																permanentAddress,
		  																permanentState,
		  																permanentPIN,
		  																urban,
		  																religion,
		  																createdOn,
		  																previousSchool,
		  																studentImage,
		  																fatherImage,
		  																motherImage,
		  																aadhaar)
		                          VALUES ('".$studentId."',
		                                  ".$grNo.",
		                                  '".$userType."',
		                                  '".$studentLoginId."',
		                                  '".$studentPassword."',
		                                  '".$parentLoginId."',
		                                  '".$parentPassword."',
		                                  '".$activated."',
		                                  '".$joinedInClass."',
		                                  '".$joiningDate."',
		                                  '".$studentName."',
		                                  '".$dateOfBirth."',
		                                  '".$gender."',
		                                  '".$bloodGroup."',
		                                  ".$houseId.",
		                                  '".addslashes($currentAddress)."',
		                                  '".$currentState."',
		                                  '".addslashes($currentAddressPin)."',
		                                  '".$residencePhone1."',
		                                  '".$residencePhone2."',
		                                  '".$smsMobile."',
		                                  '".$studentEmail."',
		                                  '".$fatherName."',
		                                  '".$fatherPhone."',
		                                  '".$fatherEmail."',
		                                  '".$fatherMobile."',
		                                  '".$fatherOccupation."',
		                                  '".addslashes($fathersDesignation)."',
		                                  '".addslashes($fathersOrganization)."',
		                                  '".$mothersName."',
		                                  '".$mothersPhone."',
		                                  '".addslashes($mothersEmailid)."',
		                                  '".$mothersMobile."',
		                                  '".addslashes($motherOccupation)."',
		                                  '".addslashes($mothersDesignation)."',
		                                  '".addslashes($mothersOrganization)."',
		                                  '".addslashes($permanentAddress)."',
		                                  '".$permanentState."',
		                                  '".$permanentPIN."',
		                                  '".$urban."',
		                                  '".$religion."',
		                                  '".$createdOn."',
		                                  '".addslashes($previousSchool)."',
		                                  '".$studentImageIn."',
		                                  '".$fatherImageIn."',
		                                  '".$motherImageIn."',
		                                  '".$aadhaar."')";
		  $studentEntryRes = om_query($studentEntry);
		  if(isset($_FILES["imagePupil"]["name"]))
	   	{
  			$filename1  =  pathinfo($_FILES["imagePupil"]["name"]);
  			$imageP = $grNo."_P.jpg";
  			$fileP = "studentImage/".$imageP; 
  			if (move_uploaded_file($_FILES['imagePupil']['tmp_name'], $fileP))
  			{
  				  @copy("studentImage/".$imageP);
  			} 
  			else 
  			{
  				//echo "pupil error"; For now we add comment over here, as upload image is not mandatory.
  			}
	  	}
		  if(isset($_FILES["imageFather"]["name"]))
	   	{
  			$filename2  =  pathinfo($_FILES["imageFather"]["name"]);
  			$imageF = $grNo."_F.jpg";
  			$fileF = "studentImage/".$imageF;
  			if (move_uploaded_file($_FILES['imageFather']['tmp_name'], $fileF))
  			{
  				  @copy("studentImage/".$imageF);
  			} 
  			else 
  			{
  				//echo "Father error"; For now we add comment over here, as upload image is not mandatory.
  			}
	  	}
		  if(isset($_FILES["imageMother"]["name"]))
	   	{
  			$filename3  =  pathinfo($_FILES["imageMother"]["name"]);
  			$imageM = $grNo."_M.jpg";
  			$fileM = "studentImage/".$imageM;
  			if (move_uploaded_file($_FILES['imageMother']['tmp_name'], $fileM))
  			{
  				  @copy("studentImage/".$imageM);
  			} 
  			else 
  			{
  				//echo "Mother error"; For now we add comment over here, as upload image is not mandatory.
  			}
	  	}
		  if(!$studentEntryRes)
		  {
		    echo "Student Entry Fail";
		  }
		  else
		  {
//		    $insertNominal = "INSERT INTO nominalroll(grNo,rollNo,academicStartYear,academicEndYear,class,section)
//		  	                         VALUES(".$grNo.",0,'".$academicStartYear."','".$academicEndYear."',
//		  	                               '".$joinedInClass."','A');";
//		    $insertNominalRes = om_query($insertNominal);
//		    if(!$insertNominalRes)
//		    {
//		    	echo "New Admission Nominal Insert Fail";
//		    }
		    
		    header("Location:studentEntry.php?done=1");
		  }
		}
	
	  if(isset($_POST['studentMasterId']) && $_POST['studentMasterId'] > 0)
	  {
	    $updateEntry = "UPDATE studentmaster 
	                          SET studentId            = '".$studentId."',                                                         
			  											  grNo                 = ".$grNo.",                             
			  											  userType             = '".$userType."',                       
			  											  studentLoginId       = '".$studentLoginId."',                 
			  											  studentPassword      = '".$studentPassword."',                
			  											  parentLoginId        = '".$parentLoginId."',                  
			  											  parentPassword       = '".$parentPassword."',                 
			  											  activated            = '".$activated."',                      
			  											  joinedInClass        = '".$joinedInClass."',                  
			  											  joiningDate          = '".$joiningDate."',                    
			  											  studentName          = '".$studentName."',                    
			  											  dateOfBirth          = '".$dateOfBirth."',                    
			  											  gender               = '".$gender."',                         
			  											  bloodGroup           = '".$bloodGroup."',                     
			  											  houseId              =  ".$houseId.",                      
			  											  currentAddress       = '".addslashes($currentAddress)."',     
			  											  currentState         = '".$currentState."',                   
			  											  currentAddressPin    = '".addslashes($currentAddressPin)."',  
			  											  residencePhone1      = '".$residencePhone1."',                
			  											  residencePhone2      = '".$residencePhone2."',                
			  											  smsMobile            = '".$smsMobile."',                      
			  											  studentEmail         = '".$studentEmail."',                   
			  											  fatherName           = '".$fatherName."',                     
			  											  fatherPhone          = '".$fatherPhone."',                    
			  											  fatherEmail          = '".$fatherEmail."',                    
			  											  fatherMobile         = '".$fatherMobile."',                   
			  											  fatherOccupation     = '".$fatherOccupation."',               
			  											  fathersDesignation   = '".addslashes($fathersDesignation)."', 
			  											  fathersOrganization  = '".addslashes($fathersOrganization)."',
			  											  mothersName          = '".$mothersName."',                    
			  											  mothersPhone         = '".$mothersPhone."',                   
			  											  mothersEmailid       = '".addslashes($mothersEmailid)."',     
			  											  mothersMobile        = '".$mothersMobile."',                  
			  											  motherOccupation     = '".addslashes($motherOccupation)."',   
			  											  mothersDesignation   = '".addslashes($mothersDesignation)."', 
			  											  mothersOrganization  = '".addslashes($mothersOrganization)."',
			  											  permanentAddress     = '".addslashes($permanentAddress)."',   
			  											  permanentState       = '".$permanentState."',                 
			  											  permanentPIN         = '".$permanentPIN."',                   
			  											  urban                = '".$urban."',                          
			  											  religion             = '".$religion."',                       
			  											  createdOn            = '".$createdOn."',                      
			  											  previousSchool       = '".addslashes($previousSchool)."',
			  											  studentImage         = '".$studentImageIn."',
			  											  fatherImage          = '".$fatherImageIn."',
			  											  motherImage          = '".$motherImageIn."',
			  											  aadhaar          = '".$aadhaar."'
	  									WHERE studentMasterId      = ".$_POST['studentMasterId'];
	    $updateEntryRes = om_query($updateEntry);
	    if(isset($_FILES["imagePupil"]["name"]))
	   	{
  			$filename1  =  pathinfo($_FILES["imagePupil"]["name"]);
  			$imageP = $grNo."_P.jpg";
  			$fileP = "studentImage/".$imageP; 
  			if (move_uploaded_file($_FILES['imagePupil']['tmp_name'], $fileP))
  			{
  				  @copy("studentImage/".$imageP);
  			} 
  			else 
  			{
  				echo "pupil error";
  			}
	  	}
		  if(isset($_FILES["imageFather"]["name"]))
	   	{
  			$filename2  =  pathinfo($_FILES["imageFather"]["name"]);
  			$imageF = $grNo."_F.jpg";
  			$fileF = "studentImage/".$imageF;
  			if (move_uploaded_file($_FILES['imageFather']['tmp_name'], $fileF))
  			{
  				  @copy("studentImage/".$imageF);
  			} 
  			else 
  			{
  				echo "Father error";
  			}
	  	}
		  if(isset($_FILES["imageMother"]["name"]))
	   	{
  			$filename3  =  pathinfo($_FILES["imageMother"]["name"]);
  			$imageM = $grNo."_M.jpg";
  			$fileM = "studentImage/".$imageM;
  			if (move_uploaded_file($_FILES['imageMother']['tmp_name'], $fileM))
  			{
  				  @copy("studentImage/".$imageM);
  			} 
  			else 
  			{
  				echo "Mother error";
  			}
	  	}
	    if(!$updateEntryRes)
	    {
	    	echo "Update Fail";
	    }
	    else
	    {
//	    	$insertNominal = "INSERT INTO  nominalroll(grNo,academicStartYear,academicEndYear,class,section)
//		  	                         VALUES(".$grNo.",'".$academicStartYear."','".$academicEndYear."',
//		  	                               '".$joinedInClass."','A');";
//		    $insertNominalRes = om_query($insertNominal);
//		    if(!$insertNominalRes)
//		    {
//		    	echo "New Admission Nominal Insert Fail";
//		    }
//		    else
//		    {
//		      header("Location:studentEntry.php?done=1");
//		    }
		    header("Location:studentEntry.php?done=1");
	    }
	  }
	}
  
  if($studentMasterId > 0)
	{
	  $selectStd = "SELECT studentMasterId,studentName,fatherName,grNo,dateOfBirth,joinedInClass,joiningDate,gender,bloodGroup,
	                       studentLoginId,studentPassword,parentLoginId,parentPassword,
	                       currentAddress,currentState,currentAddressPin,residencePhone1,residencePhone2,smsMobile,studentEmail,
	                       permanentAddress,permanentState,permanentPIN,religion,previousSchool,religion,urban,
	                       currentState,fatherMobile,fatherEmail,fatherOccupation,fathersDesignation,fathersOrganization,
	                       mothersName,mothersPhone,mothersEmailid,mothersMobile,motherOccupation,fatherPhone,
	                       mothersDesignation,mothersOrganization,createdOn,studentmaster.houseId,activated,
	                       studentImage,fatherImage,motherImage,aadhaar
	                  FROM studentmaster
	             LEFT JOIN house ON house.houseId = studentmaster.houseId
	                 WHERE studentMasterId = ".$studentMasterId."";
	  $selectStdRes = mysql_query($selectStd);
	  while($stdRow = mysql_fetch_array($selectStdRes))
	  {
	  	$studentMasterId       = $stdRow['studentMasterId'];
	  	$studentName           = $stdRow['studentName'];
	  	$fatherName            = $stdRow['fatherName'];
	  	$grNo                  = $stdRow['grNo'];
	  	$dateOfBirth           = $stdRow['dateOfBirth'];
	  	$joinedInClass         = $stdRow['joinedInClass'];
	  	$joiningDate           = $stdRow['joiningDate'];
	  	$studentLoginId        = $stdRow['studentLoginId'];
	  	$studentPassword       = $stdRow['studentPassword'];
	  	$parentLoginId         = $stdRow['parentLoginId'];
	  	$parentPassword        = $stdRow['parentPassword'];
	  	$gender                = $stdRow['gender'];
	  	$bloodGroup            = $stdRow['bloodGroup'];
	  	$currentAddress        = $stdRow['currentAddress'];
      $currentState          = $stdRow['currentState'];
      $currentAddressPin     = $stdRow['currentAddressPin'];
	  	$residencePhone1       = $stdRow['residencePhone1'];
	  	$residencePhone2       = $stdRow['residencePhone2'];
		$smsMobile			   = $stdRow['smsMobile'];
	  	$studentEmail          = $stdRow['studentEmail'];
	  	$permanentAddress      = $stdRow['permanentAddress'];
	  	$permanentState        = $stdRow['permanentState'];
      $permanentPIN          = $stdRow['permanentPIN'];
	  	$religion              = $stdRow['religion'];
	  	$previousSchool        = $stdRow['previousSchool'];
	  	$urban                 = $stdRow['urban'];
	  	$fatherMobile          = $stdRow['fatherMobile'];
	  	$fatherPhone           = $stdRow['fatherPhone'];
	  	$currentState          = $stdRow['currentState'];
	  	$fatherEmail           = $stdRow['fatherEmail'];
	  	$fatherOccupation      = $stdRow['fatherOccupation'];
	  	$fathersDesignation    = $stdRow['fathersDesignation'];
	  	$fathersOrganization   = $stdRow['fathersOrganization'];
	  	$mothersName           = $stdRow['mothersName'];
	  	$mothersPhone          = $stdRow['mothersPhone'];
	  	$mothersEmailid        = $stdRow['mothersEmailid'];
	  	$mothersMobile         = $stdRow['mothersMobile'];
	  	$motherOccupation      = $stdRow['motherOccupation'];
	  	$mothersDesignation    = $stdRow['mothersDesignation'];
	  	$createdOn             = $stdRow['createdOn'];
	  	$mothersOrganization   = $stdRow['mothersOrganization'];
	  	$houseId               = $stdRow['houseId'];
	  	$activated             = $stdRow['activated'];
	  	$studentImage          = $stdRow['studentImage'];
	  	$fatherImage           = $stdRow['fatherImage'];
	  	$motherImage           = $stdRow['motherImage'];
	  	$aadhaar           = $stdRow['aadhaar'];
	  	
	  }
	}
	
  $k = 0;
  $selectHouse = "SELECT houseId,houseName
                   FROM house";
  $selectHouseRes = mysql_query($selectHouse);
  while($houseRow = mysql_fetch_array($selectHouseRes))
  {
  	$houseArray['houseId'][$k]   = $houseRow['houseId'];
  	$houseArray['houseName'][$k] = $houseRow['houseName'];
  	$k++;
  }

  $secArrOut[0] = 'YES';
  $secArrValue[0] = 'Y';
  $secArrOut[1] = 'NO';
  $secArrValue[1] = 'N';  
  
  $maleArrValue[0] = 'Male';
  $maleArrValue[1] = 'Female';
  
  $c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  include("./bottom.php");
  $smarty->assign('maleArrValue',$maleArrValue);
  $smarty->assign('studentImage',$studentImage);
  $smarty->assign('fatherImage',$fatherImage);
  $smarty->assign('motherImage',$motherImage);
  $smarty->assign('cArray',$cArray);
  $smarty->assign("secArrOut",$secArrOut);
  $smarty->assign("secArrValue",$secArrValue);
  $smarty->assign("houseId",$houseId);
  $smarty->assign("houseArray",$houseArray);
  $smarty->assign("studentMasterId",$studentMasterId);
  $smarty->assign("studentName",$studentName);
  $smarty->assign("fatherName",$fatherName);
  $smarty->assign("grNo",$grNo);
  $smarty->assign("dateOfBirth",$dateOfBirth);
  $smarty->assign("joinedInClass",$joinedInClass);
  $smarty->assign("joiningDate",$joiningDate);
  $smarty->assign("studentLoginId",$studentLoginId);
  $smarty->assign("studentPassword",$studentPassword);
  $smarty->assign("parentLoginId",$parentLoginId);
  $smarty->assign("parentPassword",$parentPassword);
  $smarty->assign("gender",$gender);
  $smarty->assign("bloodGroup",$bloodGroup);
  $smarty->assign("currentAddress",$currentAddress);
  $smarty->assign("currentAddressPin",$currentAddressPin);
  $smarty->assign("residencePhone1",$residencePhone1);
  $smarty->assign("residencePhone2",$residencePhone2);
  $smarty->assign("smsMobile",$smsMobile);
  $smarty->assign("createdOn",$createdOn);
  $smarty->assign("motherOccupation",$motherOccupation);
  $smarty->assign("mothersDesignation",$mothersDesignation);
  $smarty->assign("studentEmail",$studentEmail);
  $smarty->assign("mothersMobile",$mothersMobile);
  $smarty->assign("mothersOrganization",$mothersOrganization);
  $smarty->assign("mothersEmailid",$mothersEmailid);
  $smarty->assign("mothersName",$mothersName);
  $smarty->assign("mothersPhone",$mothersPhone);
  $smarty->assign("fathersOrganization",$fathersOrganization);
  $smarty->assign("permanentAddress",$permanentAddress);
  $smarty->assign("permanentState",$permanentState);
  $smarty->assign("fatherMobile",$fatherMobile);
  $smarty->assign("fatherPhone",$fatherPhone);
  $smarty->assign("permanentPIN",$permanentPIN);
  $smarty->assign("fatherEmail",$fatherEmail);
  $smarty->assign("urban",$urban);
  $smarty->assign("currentState",$currentState);
  $smarty->assign("religion",$religion);
  $smarty->assign("fathersDesignation",$fathersDesignation);
  $smarty->assign("activated",$activated);
  $smarty->assign("fatherOccupation",$fatherOccupation);
  $smarty->assign("previousSchool",$previousSchool);
  $smarty->assign("academicStartYearselected",$academicStartYearselected);
  $smarty->assign("aadhaar",$aadhaar);
  $smarty->assign('bloodGroups', array('A+','A-','B+','B-','O+','O-','AB+','AB-'));
  $smarty->display("studentEntry.tpl");
}
?>

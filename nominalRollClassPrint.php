<?php
define('FPDF_FONTPATH', 'font/');
require('./font/fpdf.php');
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
  $pdf=new FPDF('P','mm','A4');   //Create new pdf file
  $pdf->Open();     //Open file
  $pdf->SetAutoPageBreak(false);  //Disable automatic page break
  $pdf->AddPage();  //Add first page


  //header part start
  $pdf->Image('./images/logoHead.png',65,2,75,20);
  //header part end
  //table header part start  
  $pdf->SetFillColor(232, 232, 232);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->SetXY(5,25);
  $pdf->Cell(25, 5, 'Roll No', 1, 0, 'C', 1);
  $pdf->Cell(25, 5, 'Gr No.',  1, 0, 'C', 1);
  $pdf->Cell(140, 5, 'Name',   1, 0, 'C', 1);
  $pdf->Cell(10, 5, '~',       1, 0, 'C', 1);

  
  $i         = 0; 
  $rowHeight = 5;
  $yAxis     = 25;

  $yAxis = $yAxis + $rowHeight;
  
  //table header part end 
    
  $todayAcademic = date('m-d');
	if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
	{
  	$academicStartYear = date('Y')."-04-01";
  	$nextYear          = date('Y') + 1;
  	$academicEndYear   = $nextYear."-03-31";
	}
	else
	{
		$prevYear          = date('Y') - 1;
		$academicStartYear = $prevYear."-04-01";
  	$academicEndYear   = date('Y')."-03-31";
	}
	$class   = isset($_REQUEST['class']) ? $_REQUEST['class'] : 0;
	$section = isset($_REQUEST['section']) ? $_REQUEST['section'] : 0;
	
	//print column titles for the actual page
  $pdf->SetFillColor(232, 232, 232);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->SetXY(5,20);
  $pdf->Cell(50, 5, 'Class : '.$class.' - '.$section, 0, 0, 'L', 0);
  $pdf->Cell(95, 5, '', 0, 0, 'C', 0);
  $pdf->Cell(55, 5, 'Academic Year : '.substr($academicStartYear,0,4).' - '.substr($academicEndYear,0,4), 0, 0, 'R', 0);
  
  $selectNominal = "SELECT nominalroll.grNo,nominalroll.academicStartYear,nominalroll.academicEndYear,nominalroll.class,
                           nominalroll.section,nominalroll.rollNo,studentmaster.activated,studentmaster.studentName
                      FROM nominalroll
                 LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
                     WHERE nominalroll.class = '".$class."'
                       AND nominalroll.section = '".$section."'
                       AND nominalroll.academicStartYear = '".$academicStartYear."'
                       AND nominalroll.academicEndYear = '".$academicEndYear."'
                       AND studentmaster.activated = 'Y'
                  ORDER BY nominalroll.rollNo";
  $selectNominalRes = mysql_query($selectNominal);
  while($nominalRow = mysql_fetch_array($selectNominalRes))
  {
    $grNo               = $nominalRow['grNo'];
    $studentName        = $nominalRow['studentName'];
    $academicYear       = substr($nominalRow['academicStartYear'],0,4).'-'.substr($nominalRow['academicEndYear'],0,4);
    $rollNo             = $nominalRow['rollNo'];

    $pdf->SetFillColor(232, 232, 232);
    $pdf->SetXY(5,$yAxis);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(25, 5, $rollNo,       1,0,'L');
    $pdf->Cell(25, 5, $grNo,         1,0,'L');
    $pdf->Cell(140, 5, $studentName, 1,0,'L');
    $pdf->Cell(10, 5, '',  1,0,'L');
    
    $yAxis = $yAxis + $rowHeight;
    $i = $i + 1;
  }
  
  $pdf->Output();
}
?>
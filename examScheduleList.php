<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	if(isset($_REQUEST['startYear']))
  {
  	$attendenceStartDateSelected = $_REQUEST['startYear'];
	  $attendenceStartDate = $_REQUEST['startYear'];
	  $attendenceEndDate   = $_REQUEST['startYear'] + 1;
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
			$attendenceStartDateSelected = date('Y');
	  	$attendenceStartDate = date('Y');
	  	$nextYear            = date('Y') + 1;
	  	$attendenceEndDate   = $nextYear;
		}
		else
		{
			$prevYear            = date('Y') - 1;
			$attendenceStartDate = $prevYear;
	  	$attendenceEndDate   = date('Y');
	  	$attendenceStartDateSelected = $prevYear;
		}
	}
	
	$empArray = array();
  $i = 0;
  $selectEmp = "SELECT DISTINCT examschedule.examScheduleId,examschedule.scheduleDate,examschedule.maxMarks,examschedule.minMarks,examschedule.class,
                       subjectmaster.subjectName,examschedule.studentView,examtype.examType
                  FROM examschedule
             LEFT JOIN examtype ON examtype.examTypeId = examschedule.examTypeId
             LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = examschedule.subjectMasterId
             LEFT JOIN classmaster ON classmaster.className = examschedule.class
                 WHERE examschedule.scheduleDate >= '".$attendenceStartDate."-04-01'
                   AND examschedule.scheduleDate <= '".$attendenceEndDate."-03-31'
              ORDER BY examschedule.scheduleDate ASC,classmaster.priority,examtype.examType,subjectmaster.subjectName,examschedule.maxMarks,examschedule.minMarks";
  $selectEmpRes = mysql_query($selectEmp);
  while($empRow = mysql_fetch_array($selectEmpRes))
  {
  	$empArray[$i]['examScheduleId'] = $empRow['examScheduleId'];
  	$empArray[$i]['scheduleDate']   = $empRow['scheduleDate'];
  	$empArray[$i]['examType']       = $empRow['examType'];
  	$empArray[$i]['maxMarks']       = $empRow['maxMarks'];
  	$empArray[$i]['minMarks']       = $empRow['minMarks'];
  	$empArray[$i]['subjectName']    = $empRow['subjectName'];
  	$empArray[$i]['class']          = $empRow['class'];
  	$empArray[$i]['studentView']    = $empRow['studentView'];
  	$i++;
  }
  include("./bottom.php");
  
  $smarty->assign('empArray',$empArray);
  $smarty->assign('attendenceStartDateSelected',$attendenceStartDateSelected);
  $smarty->display('examScheduleList.tpl');  
}
?>
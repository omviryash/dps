<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$filePath = '';
  $fileName = '';
  
  $employeeCode = 0;
  $empAtnDate   = date('Y-m-d');
  $startTime    = '';
  $endTime      = '';
  $attendence   = '';
  $late         = '';
  $oldDateNewDate  = '';
  $oldDateNewMonth = '';
  $oldDateNewYear  = '';
  
  if(isset($_POST['submitBtn']))
  {
    $uploaddir = dirname($_POST['filePath']);
  
    $uploadfile = $uploaddir . basename($_FILES['fileName']['name']);
    
    $target_path = 'data';
    $target_path = $target_path ."/". basename( $_FILES['fileName']['name']);
    $_FILES['fileName']['tmp_name']; // temp file
    
    $oldfile =  basename($_FILES['fileName']['name']);
  
    // getting the extention
  
    $pos = strpos($oldfile,".",0);
    $ext = trim(substr($oldfile,$pos+1,strlen($oldfile))," ");
    
    if(move_uploaded_file($_FILES['fileName']['tmp_name'], $target_path)) 
    {
    	$row = 0;
      $handle = fopen($target_path, "r");
      while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
      {
        if($row > 0)
        {
        	$oldDate = substr($data[3],0,2);
        	
        	if($oldDate > 0)
          {
            $oldDateNewDate  = $oldDate;
            $oldDateNewMonth = substr($data[3],3,3);
            $oldDateNewYear  = substr($data[3],7,4);
            if($oldDateNewMonth == 'Jan')
            {
              $oldDateNewMonth  = '01';
            }
            elseif($oldDateNewMonth == 'Feb')
            {
              $oldDateNewMonth  = '02';
            }
            elseif($oldDateNewMonth == 'Mar')
            {
              $oldDateNewMonth  = '03';
            }
            elseif($oldDateNewMonth == 'Apr')
            {
              $oldDateNewMonth  = '04';
            }
            elseif($oldDateNewMonth == 'May')
            {
              $oldDateNewMonth  = '05';
            }
            elseif($oldDateNewMonth == 'Jun')
            {
              $oldDateNewMonth  = '06';
            }
            elseif($oldDateNewMonth == 'Jul')
            {
              $oldDateNewMonth  = '07';
            }
            elseif($oldDateNewMonth == 'Aug')
            {
              $oldDateNewMonth  = '08';
            }
            elseif($oldDateNewMonth == 'Sep')
            {
              $oldDateNewMonth  = '09';
            }
            elseif($oldDateNewMonth == 'Oct')
            {
              $oldDateNewMonth  = '10';
            }
            elseif($oldDateNewMonth == 'Nov')
            {
              $oldDateNewMonth  = '11';
            }
            elseif($oldDateNewMonth == 'Dec')
            {
              $oldDateNewMonth  = '12';
            }
          }
          $employeeCode = $data[1];
          $empAtnDate   = $oldDateNewYear.'-'.$oldDateNewMonth.'-'.$oldDateNewDate;
          $startTime    = $data[4].':00';
          if($data[9] == 'Present')
          {
            $attendence   = 'P';
          }
          else
          {
          	$attendence   = 'A';
          }
          $late = 'No';
          if($employeeCode > 0)
          {
	          $insertsubMaster = "INSERT INTO employeeattend(employeeCode,date,startTime,attendences,late)
	        	                         VALUES ('".$employeeCode."','".$empAtnDate."','".$startTime."','".$attendence."','".$late."')";
	        	om_query($insertsubMaster);
	        }
  	    }
        $row++;
      }
    } 
    else
    {
      echo "There was an error uploading the file, please try again!";
    }
  }
}
?>
<BODY>
  <H1><CENTER><FONT color="red">Take Staff Attendance </FONT></CENTER></H1><HR>
<CENTER>
<FORM enctype="multipart/form-data" action="" name="employeeattend" method="POST"> 
  <a href="index.php">Home</a>
	<br /><br />
  <INPUT size="60" type="hidden" name="filePath"><BR>
  File Name:
  <INPUT name="fileName" type="file" onChange="document.employeeattend.filePath.value=document.employeeattend.fileName.value;">
  <INPUT type="submit" name="submitBtn" value="Submit File"> 
</FORM>
</CENTER>
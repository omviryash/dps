<?php
define('FPDF_FONTPATH', 'font/');
require('./font/fpdf.php');
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
  $pdf=new FPDF('P','mm','A4');   //Create new pdf file
  $pdf->Open();     //Open file
  $pdf->SetAutoPageBreak(false);  //Disable automatic page break
  $pdf->AddPage();  //Add first page


  //header part start
  $pdf->Image('./images/logoHead.png',65,2,75,20);
  //header part end
  //table header part start  
  $pdf->SetFillColor(232, 232, 232);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->SetXY(5,25);
  $pdf->Cell(200, 5, 'Haripar, Survey No.12, B/H N.R.I Bungalows, Kalawad  Road ,  Rajkot - 360 005.', 0, 0, 'C', 0);

  $pdf->SetFont('Arial', '', 10);
  $pdf->SetXY(5,35);
  $pdf->Cell(20, 5, 'Phone No.', 0,0,'L');
  $pdf->Cell(5,  5, ':', 0,0,'L');
  $pdf->Cell(45, 5, '+91 9375070921/22', 0,0,'L');
  
  $pdf->SetXY(5,40);
  $pdf->Cell(20, 5, 'Fax No.', 0,0,'L');
  $pdf->Cell(5,  5, ':', 0,0,'L');
  $pdf->Cell(45, 5, '+91 9375070923', 0,0,'L');
  
  $pdf->SetXY(5,50);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(100, 5, 'CBSE Affilition No. 430054', 0,0,'L');
  $pdf->Cell(100, 5, 'School Code: 13015', 0,0,'R');
  
  $pdf->SetXY(55,90);
  $pdf->SetFont('Times', 'B', 20);
  $pdf->Cell(100, 10, 'BONAFIDE CERTIFICATE', 1,0,'C');
  
  $pdf->SetXY(5,185);
  $pdf->SetFont('Arial', '', 12);
  $pdf->Cell(200, 10, 'Issuing Assistant: ______________', 0,0,'L');
  
  $pdf->SetXY(5,205);
  $pdf->Cell(200, 10, 'Principal: ________________', 0,0,'R');
  
  $pdf->Line(5,55,205,55);
  
  $i         = 0; 
  $rowHeight = 5;
  $yAxis     = 25;

  $yAxis = $yAxis + $rowHeight;
  $nominalRollId = isset($_REQUEST['nominalRollId']) ? $_REQUEST['nominalRollId'] : 0;
  //table header part end 
  $selectNominal = "SELECT nominalroll.grNo,nominalroll.academicStartYear,nominalroll.academicEndYear,nominalroll.class,
                           nominalroll.section,nominalroll.rollNo,studentmaster.activated,studentmaster.studentName,
                           studentmaster.studentName,fatherName,
                           DATE_FORMAT(nominalroll.bonafideDate, '%d-%m-%Y') AS bonafideDate,
                           DATE_FORMAT(studentmaster.joiningDate, '%d-%m-%Y') AS wefDate,
                           DATE_FORMAT(studentmaster.dateOfBirth, '%d-%m-%Y') AS dateOfBirth
                      FROM nominalroll
                 LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
                     WHERE nominalroll.nominalRollId = ".$nominalRollId."";
  $selectNominalRes = mysql_query($selectNominal);
  while($nominalRow = mysql_fetch_array($selectNominalRes))
  {
    $grNo               = $nominalRow['grNo'];
    $studentName        = ucfirst(strtolower($nominalRow['studentName']));
    $fatherName         = ucfirst(strtolower($nominalRow['fatherName']));
    $class              = $nominalRow['class'];
    $academicYear       = substr($nominalRow['academicStartYear'],0,4).'-'.substr($nominalRow['academicEndYear'],0,4);
    
    $bonafideDate       = $nominalRow['bonafideDate'];
    $wefDate            = $nominalRow['wefDate'];
    $dateOfBirth        = $nominalRow['dateOfBirth'];
    
    $mydate = strtoTime($dateOfBirth);
		$dateOfBirthWord = date('d F Y', $mydate);

    $pdf->SetXY(5,60);
	  $pdf->SetFont('Arial', 'B', 10);
	  $pdf->Cell(200, 5, 'Date : '.$bonafideDate, 0,0,'R');
	  
	  $var = 'This is to certify that <b>'.$studentName.'</b> s/o <b>'.$fatherName.
               '</b> is a bona fide student of our school w. e. f. <b>'.$wefDate.
               '</b> During the session <b>'.$academicYear.
               '</b> he is studying in Class <b>'.$class.
               '</b> As per school record his/her date of birth is : <b>'.$dateOfBirth.'</b> - ( <b>'.$dateOfBirthWord.'</b>)';
	  
    $pdf->SetFillColor(232, 232, 232);
    $pdf->SetXY(5,130);
    $pdf->SetFont('Arial', '', 12);
    $pdf->MultiCell(200, 15,  $var, 0,'L');
    $yAxis = $yAxis + $rowHeight;
    $i = $i + 1;
  }
  
  $pdf->Output();
}
?>
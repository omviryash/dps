<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$nominalArr = array();
	
  $i = 0;
  $selectNominal = "SELECT employeeMasterId,name,activated,busArrival,busDeparture,routeArrival,
                           routeDeparture,busStopMasterId
                      FROM employeemaster
                     WHERE activated = 'Yes'";
  $selectNominalRes = mysql_query($selectNominal);
  while($nominalRow = mysql_fetch_array($selectNominalRes))
  {
    $nominalArr[$i]['employeeMasterId'] = $nominalRow['employeeMasterId'];
    $nominalArr[$i]['name']             = $nominalRow['name'];
    $nominalArr[$i]['activated']        = $nominalRow['activated'];
    $nominalArr[$i]['busArrival']       = $nominalRow['busArrival'];
    $nominalArr[$i]['busDeparture']     = $nominalRow['busDeparture'];
    $nominalArr[$i]['routeArrival']     = $nominalRow['routeArrival'];
    $nominalArr[$i]['routeDeparture']   = $nominalRow['routeDeparture'];
    $nominalArr[$i]['busStopMasterId']  = $nominalRow['busStopMasterId'];
    $i++;
  }
  
	if(isset($_POST['submit']))
	{
		$loopCount1 = 0;
	  while($loopCount1 < count($_POST['employeeMasterId']))
	  {
	    $employeeMasterId   = isset($_POST['employeeMasterId'][$loopCount1]) && $_POST['employeeMasterId'][$loopCount1] != '' ? $_POST['employeeMasterId'][$loopCount1] : 0;
	    $busArrival      = isset($_POST['busArrival'][$loopCount1]) && $_POST['busArrival'][$loopCount1] != '' ? $_POST['busArrival'][$loopCount1] : 0;
		  $busDeparture    = isset($_POST['busDeparture'][$loopCount1]) && $_POST['busDeparture'][$loopCount1] != '' ? $_POST['busDeparture'][$loopCount1] : 0;
		  $routeArrival    = isset($_POST['routeArrival'][$loopCount1]) && $_POST['routeArrival'][$loopCount1] != '' ? $_POST['routeArrival'][$loopCount1] : 0;
		  $routeDeparture  = isset($_POST['routeDeparture'][$loopCount1]) && $_POST['routeDeparture'][$loopCount1] != '' ? $_POST['routeDeparture'][$loopCount1] : 0;
		  $busStopMasterId = isset($_POST['busStopMasterId'][$loopCount1]) && $_POST['busStopMasterId'][$loopCount1] != '' ? $_POST['busStopMasterId'][$loopCount1] : 0;
		  
	    if($busArrival > 0 || $busDeparture > 0 || $routeArrival > 0 || $routeDeparture > 0 || $busStopMasterId > 0)
	    {
	      $nominalEntry = "UPDATE employeemaster 
			  										SET busArrival = '".$busArrival."',
			  											  busDeparture = '".$busDeparture."',
			  											  routeArrival = '".$routeArrival."',
			  											  routeDeparture = '".$routeDeparture."',
			  											  busStopMasterId = '".$busStopMasterId."'
											  	WHERE employeeMasterId  = ".$employeeMasterId."";
			  $nominalEntryRes = om_query($nominalEntry);
			  if(!$nominalEntryRes)
			  {
			  	echo "Update Fail";
			  }
			  else
			  {
			    header("Location:staffBusAllocation.php?done=1");
			  }
	    }
	    $loopCount1++;
	  }
  }
  
	$typeArr         = array();
	$rArr            = array();
	$tArr            = array();
	$sArr            = array();
	
	$k = 0;
  $selectClub = "SELECT vehicleMasterId,vehicleNo
                   FROM vehiclemaster";
  $selectClubRes = mysql_query($selectClub);
  while($clubRow = mysql_fetch_array($selectClubRes))
  {
  	$typeArr['vehicleMasterId'][$k] = $clubRow['vehicleMasterId'];
  	$typeArr['vehicleNo'][$k]   = $clubRow['vehicleNo'];
  	$k++;
  }
  
  $r = 0;
  $selectR = "SELECT routeMasterId,routeName
                FROM routemaster
               WHERE arrival = 'Arrival'";
  $selectRRes = mysql_query($selectR);
  while($rRow = mysql_fetch_array($selectRRes))
  {
  	$rArr['routeMasterId'][$r] = $rRow['routeMasterId'];
  	$rArr['routeName'][$r]     = $rRow['routeName'];
  	$r++;
  }
  
  $t = 0;
  $selectT = "SELECT routeMasterId,routeName
                FROM routemaster
               WHERE arrival = 'Departure'";
  $selectTRes = mysql_query($selectT);
  while($tRow = mysql_fetch_array($selectTRes))
  {
  	$tArr['routeMasterId'][$t] = $tRow['routeMasterId'];
  	$tArr['routeName'][$t]     = $tRow['routeName'];
  	$t++;
  }
  
  $s = 0;
  $selectS = "SELECT busStopMasterId,busStop
                FROM busstopmaster";
  $selectSRes = mysql_query($selectS);
  while($sRow = mysql_fetch_array($selectSRes))
  {
  	$sArr['busStopMasterId'][$s] = $sRow['busStopMasterId'];
  	$sArr['busStop'][$s]         = $sRow['busStop'];
  	$s++;
  }
  
  include("./bottom.php");
  $smarty->assign('nominalArr',$nominalArr);
  $smarty->assign('typeArr',$typeArr);
  $smarty->assign('rArr',$rArr);
  $smarty->assign('tArr',$tArr);
  $smarty->assign('sArr',$sArr);
  $smarty->display('staffBusAllocation.tpl');  
}
?>
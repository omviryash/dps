<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$total      = 0;
	$totalMale  = 0;
	$totalGirls = 0;
	$male = '';
	$strengthArr = array();
  $i = 0;
  //$startDate = date('Y').'-04-01';
  //$endDate   = '2015-03-31';
  //echo $Date = date('Y') + 1.'-03-31';
  if(isset($_REQUEST['startYear']))
  {
	  $academicStartYear = $_REQUEST['startYear'];
	  $academicEndYear   = $_REQUEST['startYear'] + 1;
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y');
	  	$nextYear            = date('Y') + 1;
	  	$academicEndYear   = $nextYear;
		}
		else
		{
			$prevYear            = date('Y') - 1;
			$academicStartYear = $prevYear;
	  	$academicEndYear   = date('Y');
		}
	}
  
  $selectEmp = "SELECT DISTINCT classteacherallotment.class,classteacherallotment.section,employeemaster.name
                  FROM classteacherallotment
                  JOIN employeemaster ON employeemaster.employeeMasterId = classteacherallotment.employeeMasterId
                  LEFT JOIN classmaster ON classmaster.className = classteacherallotment.class
                 WHERE academicStartYear = '".$academicStartYear."-04-01'
  	               AND academicEndYear = '".$academicEndYear."-03-31'
  	               ORDER BY classmaster.priority,classteacherallotment.section";
  $selectEmpRes = mysql_query($selectEmp);
  while($empRow = mysql_fetch_array($selectEmpRes))
  {
  	$strengthArr[$i]['class']    = $empRow['class'];
  	$strengthArr[$i]['section']  = $empRow['section'];
  	$strengthArr[$i]['name']     = $empRow['name'];
  	$selectNominalRollMale = "SELECT studentmaster.gender,studentmaster.activated,academicStartYear,academicEndYear
      	                        FROM nominalroll
      	                   LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
  	                           WHERE nominalroll.class = '".$empRow['class']."'
  	                             AND nominalroll.section = '".$empRow['section']."'
  	                             AND gender = 'Male'
  	                             AND academicStartYear = '".$academicStartYear."-04-01'
  	                             AND academicEndYear = '".$academicEndYear."-03-31'
  	                             AND studentmaster.activated = 'Y'";
    $selectNominalRollMaleRes = mysql_query($selectNominalRollMale);
    $countMale = mysql_num_rows($selectNominalRollMaleRes);
    $strengthArr[$i]['countMale'] = $countMale;
    
    $selectNominalRollFemale = "SELECT studentmaster.gender,studentmaster.activated,academicStartYear,academicEndYear
      	                          FROM nominalroll
      	                     LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
  	                             WHERE nominalroll.class = '".$empRow['class']."'
  	                               AND nominalroll.section = '".$empRow['section']."'
  	                               AND gender = 'Female'
  	                               AND academicStartYear = '".$academicStartYear."-04-01'
  	                               AND academicEndYear = '".$academicEndYear."-03-31'
  	                               AND studentmaster.activated = 'Y'";
    $selectNominalRollFemaleRes = mysql_query($selectNominalRollFemale);
    $countFemale = mysql_num_rows($selectNominalRollFemaleRes);
    $strengthArr[$i]['countFemale'] = $countFemale;
    $strengthArr[$i]['countAll']    = $countFemale + $countMale;
    $totalGirls                     += $countFemale; 
    $totalMale                      += $countMale; 
    $total                          = $totalGirls  + $totalMale;
  	$i++;
  }
  include("./bottom.php");
  $smarty->assign('male',$male);
  $smarty->assign('total',$total);
  $smarty->assign('totalMale',$totalMale);
  $smarty->assign('totalGirls',$totalGirls);
  $smarty->assign('strengthArr',$strengthArr);
  $smarty->assign('academicStartYear',$academicStartYear);
  $smarty->assign('academicEndYear',$academicEndYear);
  $smarty->display('studentStrength.tpl');  
}
?>
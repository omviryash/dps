<?php
define('FPDF_FONTPATH', 'font/');
require('./font/fpdf.php');
include "include/config.inc.php";

if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
  $pdf=new FPDF('P','mm','A4');   //Create new pdf file
  $pdf->Open();     //Open file
  $pdf->SetAutoPageBreak(false);  //Disable automatic page break
  $pdf->AddPage();  //Add first page


  //header part start
  $pdf->Image('./images/logo.png',10,10,22,25);
  //header part end
  //table header part start  
  $pdf->SetFillColor(232, 232, 232);
  $pdf->SetFont('Arial', '', 22);
  $pdf->SetXY(5,7);
  $pdf->Cell(200, 7, 'Delhi Public School', 0,0,'C',0);
  $pdf->SetFont('Arial', '', 10);
  $pdf->SetXY(5,12);
  $pdf->Cell(200, 7, 'Haripar, Survey No. 12, Kalawad Road, Rajkot - 360 007', 0,0,'C',0);
  $pdf->SetXY(5,19);
  $pdf->Cell(200, 7, 'Ph: 7572890044, 7572890055 , Email : Info@dpsrajkot.org, Website: www.dpsrajkot.org', 0,0,'C',0);
  $pdf->SetXY(5,26);
  $pdf->Cell(200, 7, '(Affiliated to C B S E, New Delhi, Affiliation No.430054)', 0,0,'C',0);
  $pdf->SetFont('Arial', 'B', 12);
  $pdf->SetXY(5,33);
  $pdf->Cell(200, 7, 'Record of Academic Performance', 0,0,'C',0);
  
  
  $pdf->SetFillColor(232, 232, 232);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->SetXY(8,95);
  $pdf->Cell(40, 10, 'Scholastic Area', 1,0,'C',1);
  $pdf->Cell(160, 10, 'Term - 1', 1,0,'C',1);
  
  $pdf->SetFillColor(232, 232, 232);
  $pdf->SetFont('Arial', 'B', 8);
  $pdf->SetXY(8,105);
  $pdf->Cell(40, 10, 'Subject', 1,0,'C',1);
  $pdf->Cell(40, 10, 'FA1', 1,0,'C',1);
  $pdf->Cell(40, 10, 'FA2', 1,0,'C',1);
  $pdf->Cell(40, 10, 'SA1', 1,0,'C',1);
  $pdf->Cell(40, 10, 'Total', 1,0,'C',1);
  
  $i         = 0; 
  $rowHeight = 10;
  $yAxis     = 105;

  $yAxis = $yAxis + $rowHeight;
  $totalNumberWorkingDay = 1;

  $resultNote1 = '';
  $resultNote2 = '';
  //table header part end 
  
  if(isset($_REQUEST['academicYear']))
  {
  	$academicStartYear = substr($_REQUEST['academicYear'],0,4)."-04-01";
  	$nextYear          = substr($_REQUEST['academicYear'],0,4) + 1;
  	$academicEndYear   = $nextYear."-03-31";
  }
  else
  {
	  $todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y')."-04-01";
	  	$nextYear          = date('Y') + 1;
	  	$academicEndYear   = $nextYear."-03-31";
		}
		else
		{
			$prevYear          = date('Y') - 1;
			$academicStartYear = $prevYear."-04-01";
	  	$academicEndYear   = date('Y')."-03-31";
		}
	}
	
	if(isset($_REQUEST['grNo']))
	{
	  $myGrNo = "AND nominalroll.grNo = ".$_REQUEST['grNo']."";
	}
	else
	{
		$myGrNo = "AND studentmaster.studentLoginId = '".$_SESSION['s_activName']."' 
                   OR studentmaster.parentLoginId = '".$_SESSION['s_activName']."'";
	}
	//print column titles for the actual page
  $pdf->SetFillColor(232, 232, 232);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->SetXY(0,40);
  $pdf->Cell(210, 5, substr($academicStartYear,0,4).' - '.substr($academicEndYear,0,4), 0, 0, 'C', 0);
  
  $selectNominal = "SELECT nominalroll.grNo,nominalroll.academicStartYear,nominalroll.academicEndYear,nominalroll.class,
                           nominalroll.section,nominalroll.rollNo,studentmaster.activated,studentmaster.bloodGroup,studentmaster.studentName,
                           studentmaster.fatherName,studentmaster.mothersName,DATE_FORMAT(studentmaster.dateOfBirth, '%d-%m-%Y') AS dateOfBirth,studentmaster.currentAddress,
                           nominalroll.goals,nominalroll.strength,nominalroll.intHobbies,nominalroll.responsibilities,
                           nominalroll.height,nominalroll.weight,nominalroll.dentalHygience,nominalroll.3a1,
                           nominalroll.3a2,nominalroll.3b1,nominalroll.3b2
                      FROM nominalroll
                 LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
                     WHERE 1 = 1
                       ".$myGrNo."
                       AND nominalroll.academicStartYear = '".$academicStartYear."'
                       AND nominalroll.academicEndYear = '".$academicEndYear."'
                       AND studentmaster.activated = 'Y'
                  ORDER BY nominalroll.rollNo";
  $selectNominalRes = mysql_query($selectNominal);
  while($nominalRow = mysql_fetch_array($selectNominalRes))
  {
    $class              = $nominalRow['class'];
    $section            = $nominalRow['section'];
    $grNo               = $nominalRow['grNo'];
    $studentName        = $nominalRow['studentName'];
    $fatherName         = $nominalRow['fatherName'];
    $mothersName        = $nominalRow['mothersName'];
    $dateOfBirth        = $nominalRow['dateOfBirth'];
    $currentAddress     = $nominalRow['currentAddress'];
    $academicYear       = substr($nominalRow['academicStartYear'],0,4).'-'.substr($nominalRow['academicEndYear'],0,4);
    $rollNo             = $nominalRow['rollNo'];
    
    $pdf->SetFillColor(232, 232, 232);
	  $pdf->SetFont('Arial', 'B', 10);
	  $pdf->SetXY(8,45);
	  $pdf->Cell(35, 5, 'Registration No : ', 0, 0, 'L', 0);
	  $pdf->Cell(105,5, $grNo, 0, 0, 'L', 0);
	  $pdf->Cell(35, 5, 'Admission No : ', 0, 0, 'R', 0);
	  $pdf->Cell(25, 5, $grNo, 0, 0, 'R', 0);
	  
	  $pdf->SetFillColor(232, 232, 232);
	  $pdf->SetFont('Arial', 'B', 10);
	  $pdf->SetXY(8,55);
	  $pdf->Cell(35, 5, 'Name of Student : ', 0, 0, 'L', 0);
	  $pdf->Cell(105,5, $studentName, 0, 0, 'L', 0);
	  $pdf->Cell(35, 5, 'Roll No : ', 0, 0, 'R', 0);
	  $pdf->Cell(25, 5, $rollNo, 0, 0, 'R', 0);
	  
    $pdf->SetFillColor(232, 232, 232);
	  $pdf->SetFont('Arial', 'B', 10);
	  $pdf->SetXY(8,65);
	  $pdf->Cell(35, 5, 'Father`s Name : ', 0, 0, 'L', 0);
	  $pdf->Cell(105,5, $fatherName, 0, 0, 'L', 0);
	  $pdf->Cell(35, 5, 'Class : ', 0, 0, 'R', 0);
	  $pdf->Cell(25, 5, $class.' - '.$section, 0, 0, 'R', 0);
	  
	  $pdf->SetFillColor(232, 232, 232);
	  $pdf->SetFont('Arial', 'B', 10);
	  $pdf->SetXY(8,75);
	  $pdf->Cell(35, 5, 'Mother`s Name : ', 0, 0, 'L', 0);
	  $pdf->Cell(105,5, $mothersName, 0, 0, 'L', 0);
	  $pdf->Cell(35, 5, 'DOB : ', 0, 0, 'R', 0);
	  $pdf->Cell(25, 5, $dateOfBirth, 0, 0, 'R', 0);
	  
    $pdf->SetFillColor(232, 232, 232);
	  $pdf->SetFont('Arial', 'B', 10);
	  $pdf->SetXY(8,85);
	  $pdf->Cell(35, 5, 'Address : ', 0, 0, 'L', 0);
	  $pdf->MultiCell(165,5, $currentAddress, 0, 'L');
	  
	  $selectStd = "SELECT academicStartYear,academicEndYear,grNo,class,section,
			sub1Fa1,sub1Fa2,sub1Sa1,sub1Fa3,sub1Fa4,sub1Sa2,
			sub2Fa1,sub2Fa2,sub2Sa1,sub2Fa3,sub2Fa4,sub2Sa2,
			sub3Fa1,sub3Fa2,sub3Sa1,sub3Fa3,sub3Fa4,sub3Sa2,
			sub4Fa1,sub4Fa2,sub4Sa1,sub4Fa3,sub4Fa4,sub4Sa2,
			sub5Fa1,sub5Fa2,sub5Sa1,sub5Fa3,sub5Fa4,sub5Sa2,
			sub6Fa1,sub6Fa2,sub6Sa1,sub6Fa3,sub6Fa4,sub6Sa2,
			2A1,2A2,2A3,2B1,2C1,2D1,2D2,2D3,2D4,3A1,3A2,3B1,3B2,
			goals,intHobbies,strengths,responsibilities,dentalHygiene,
                         visionL,visionR,height,weight,resultNote1,resultNote2,
                         DATE_FORMAT(reportDate1, '%d-%m-%Y') AS reportDate1,
                         DATE_FORMAT(reportDate2, '%d-%m-%Y') AS reportDate2
	                  FROM exammarkspertry
	                 WHERE grNo = '".$grNo."'
	                   AND class = '".$class."'
	                   AND section = '".$section."'
	                   AND academicStartYear = '".$academicStartYear."'
	                   AND academicEndYear = '".$academicEndYear."'";
	  $selectStdRes = mysql_query($selectStd);
	  while($stdRow = mysql_fetch_array($selectStdRes))
	  {
	  	$resultNote1 = $stdRow['resultNote1'];
	  	$resultNote2 = $stdRow['resultNote2'];
	  	
	  	$reportDate1 = $stdRow['reportDate1'];
  	  $reportDate2 = $stdRow['reportDate2'];
  	  
	  	$sub1Fa1         = $stdRow['sub1Fa1'];
	  	$sub1Fa1Per      = number_format(($sub1Fa1 * 100/10),0,'.',' ');
      if($sub1Fa1Per >= 91 && $sub1Fa1Per <= 100)
			{
			  $sub1Fa1Per = 'A1';
			}
			elseif($sub1Fa1Per >= 81 && $sub1Fa1Per <= 90)
			{
				$sub1Fa1Per = 'A2';
			}
			elseif($sub1Fa1Per >= 71 && $sub1Fa1Per <= 80)
			{
				$sub1Fa1Per = 'B1';
			}
			elseif($sub1Fa1Per >= 61 && $sub1Fa1Per <= 70)
			{
				$sub1Fa1Per = 'B2';
			}
			elseif($sub1Fa1Per >= 51 && $sub1Fa1Per <= 60)
			{
				$sub1Fa1Per = 'C1';
			}
			elseif($sub1Fa1Per >= 41 && $sub1Fa1Per <= 50)
			{
				$sub1Fa1Per = 'C2';
			}
			elseif($sub1Fa1Per >= 33 && $sub1Fa1Per <= 40)
			{
				$sub1Fa1Per = 'D';
			}
			elseif($sub1Fa1Per >= 21 && $sub1Fa1Per <= 32)
			{
				$sub1Fa1Per = 'E1';
			}
			elseif($sub1Fa1Per <= 20)
			{
				$sub1Fa1Per = 'E2';
			}
			
	  	$sub1Fa2         = $stdRow['sub1Fa2'];
	  	$sub1Fa2Per = number_format(($sub1Fa2 * 100/10),0,'.',' ');
	  	if($sub1Fa2Per >= 91 && $sub1Fa2Per <= 100)
			{
			  $sub1Fa2Per = 'A1';
			}
			elseif($sub1Fa2Per >= 81 && $sub1Fa2Per <= 90)
			{
				$sub1Fa2Per = 'A2';
			}
			elseif($sub1Fa2Per >= 71 && $sub1Fa2Per <= 80)
			{
				$sub1Fa2Per = 'B1';
			}
			elseif($sub1Fa2Per >= 61 && $sub1Fa2Per <= 70)
			{
				$sub1Fa2Per = 'B2';
			}
			elseif($sub1Fa2Per >= 51 && $sub1Fa2Per <= 60)
			{
				$sub1Fa2Per = 'C1';
			}
			elseif($sub1Fa2Per >= 41 && $sub1Fa2Per <= 50)
			{
				$sub1Fa2Per = 'C2';
			}
			elseif($sub1Fa2Per >= 33 && $sub1Fa2Per <= 40)
			{
				$sub1Fa2Per = 'D';
			}
			elseif($sub1Fa2Per >= 21 && $sub1Fa2Per <= 32)
			{
				$sub1Fa2Per = 'E1';
			}
			elseif($sub1Fa2Per <= 20)
			{
				$sub1Fa2Per = 'E2';
			}
	  	
	  	$sub1Sa1         = $stdRow['sub1Sa1'];
	  	$sub1Sa1Per = number_format(($sub1Sa1 * 100/30),0,'.',' ');
	  	if($sub1Sa1Per >= 91 && $sub1Sa1Per <= 100)
			{
			  $sub1Sa1Per = 'A1';
			}
			elseif($sub1Sa1Per >= 81 && $sub1Sa1Per <= 90)
			{
				$sub1Sa1Per = 'A2';
			}
			elseif($sub1Sa1Per >= 71 && $sub1Sa1Per <= 80)
			{
				$sub1Sa1Per = 'B1';
			}
			elseif($sub1Sa1Per >= 61 && $sub1Sa1Per <= 70)
			{
				$sub1Sa1Per = 'B2';
			}
			elseif($sub1Sa1Per >= 51 && $sub1Sa1Per <= 60)
			{
				$sub1Sa1Per = 'C1';
			}
			elseif($sub1Sa1Per >= 41 && $sub1Sa1Per <= 50)
			{
				$sub1Sa1Per = 'C2';
			}
			elseif($sub1Sa1Per >= 33 && $sub1Sa1Per <= 40)
			{
				$sub1Sa1Per = 'D';
			}
			elseif($sub1Sa1Per >= 21 && $sub1Sa1Per <= 32)
			{
				$sub1Sa1Per = 'E1';
			}
			elseif($sub1Sa1Per <= 20)
			{
				$sub1Sa1Per = 'E2';
			}
	    
	    $sub1Sa1Total = $sub1Fa1 + $sub1Fa2 + $sub1Sa1;
	    $sub1Sa1TotalPer = number_format(($sub1Sa1Total * 100/50),0,'.',' ');
	    if($sub1Sa1TotalPer >= 91 && $sub1Sa1TotalPer <= 100)
			{
			  $sub1Sa1TotalPer = 'A1';
			}
			elseif($sub1Sa1TotalPer >= 81 && $sub1Sa1TotalPer <= 90)
			{
				$sub1Sa1TotalPer = 'A2';
			}
			elseif($sub1Sa1TotalPer >= 71 && $sub1Sa1TotalPer <= 80)
			{
				$sub1Sa1TotalPer = 'B1';
			}
			elseif($sub1Sa1TotalPer >= 61 && $sub1Sa1TotalPer <= 70)
			{
				$sub1Sa1TotalPer = 'B2';
			}
			elseif($sub1Sa1TotalPer >= 51 && $sub1Sa1TotalPer <= 60)
			{
				$sub1Sa1TotalPer = 'C1';
			}
			elseif($sub1Sa1TotalPer >= 41 && $sub1Sa1TotalPer <= 50)
			{
				$sub1Sa1TotalPer = 'C2';
			}
			elseif($sub1Sa1TotalPer >= 33 && $sub1Sa1TotalPer <= 40)
			{
				$sub1Sa1TotalPer = 'D';
			}
			elseif($sub1Sa1TotalPer >= 21 && $sub1Sa1TotalPer <= 32)
			{
				$sub1Sa1TotalPer = 'E1';
			}
			elseif($sub1Sa1TotalPer <= 20)
			{
				$sub1Sa1TotalPer = 'E2';
			}

	    
	  	$sub1Fa3 = $stdRow['sub1Fa3'];
	  	$sub1Fa3Per = number_format(($sub1Fa3 * 100/10),0,'.',' ');
	  	if($sub1Fa3Per >= 91 && $sub1Fa3Per <= 100)
			{
			  $sub1Fa3Per = 'A1';
			}
			elseif($sub1Fa3Per >= 81 && $sub1Fa3Per <= 90)
			{
				$sub1Fa3Per = 'A2';
			}
			elseif($sub1Fa3Per >= 71 && $sub1Fa3Per <= 80)
			{
				$sub1Fa3Per = 'B1';
			}
			elseif($sub1Fa3Per >= 61 && $sub1Fa3Per <= 70)
			{
				$sub1Fa3Per = 'B2';
			}
			elseif($sub1Fa3Per >= 51 && $sub1Fa3Per <= 60)
			{
				$sub1Fa3Per = 'C1';
			}
			elseif($sub1Fa3Per >= 41 && $sub1Fa3Per <= 50)
			{
				$sub1Fa3Per = 'C2';
			}
			elseif($sub1Fa3Per >= 33 && $sub1Fa3Per <= 40)
			{
				$sub1Fa3Per = 'D';
			}
			elseif($sub1Fa3Per >= 21 && $sub1Fa3Per <= 32)
			{
				$sub1Fa3Per = 'E1';
			}
			elseif($sub1Fa3Per <= 20)
			{
				$sub1Fa3Per = 'E2';
			}
			
	  	$sub1Fa4 = $stdRow['sub1Fa4'];
	  	$sub1Fa4Per = number_format(($sub1Fa4 * 100/10),0,'.',' ');
	  	if($sub1Fa4Per >= 91 && $sub1Fa4Per <= 100)
			{
			  $sub1Fa4Per = 'A1';
			}
			elseif($sub1Fa4Per >= 81 && $sub1Fa4Per <= 90)
			{
				$sub1Fa4Per = 'A2';
			}
			elseif($sub1Fa4Per >= 71 && $sub1Fa4Per <= 80)
			{
				$sub1Fa4Per = 'B1';
			}
			elseif($sub1Fa4Per >= 61 && $sub1Fa4Per <= 70)
			{
				$sub1Fa4Per = 'B2';
			}
			elseif($sub1Fa4Per >= 51 && $sub1Fa4Per <= 60)
			{
				$sub1Fa4Per = 'C1';
			}
			elseif($sub1Fa4Per >= 41 && $sub1Fa4Per <= 50)
			{
				$sub1Fa4Per = 'C2';
			}
			elseif($sub1Fa4Per >= 33 && $sub1Fa4Per <= 40)
			{
				$sub1Fa4Per = 'D';
			}
			elseif($sub1Fa4Per >= 21 && $sub1Fa4Per <= 32)
			{
				$sub1Fa4Per = 'E1';
			}
			elseif($sub1Fa4Per <= 20)
			{
				$sub1Fa4Per = 'E2';
			}
	  	
	  	$sub1Sa2         = $stdRow['sub1Sa2'];
	  	$sub1Sa2Per = number_format(($sub1Sa2 * 100/30),0,'.',' ');
	  	if($sub1Sa2Per >= 91 && $sub1Sa2Per <= 100)
			{
			  $sub1Sa2Per = 'A1';
			}
			elseif($sub1Sa2Per >= 81 && $sub1Sa2Per <= 90)
			{
				$sub1Sa2Per = 'A2';
			}
			elseif($sub1Sa2Per >= 71 && $sub1Sa2Per <= 80)
			{
				$sub1Sa2Per = 'B1';
			}
			elseif($sub1Sa2Per >= 61 && $sub1Sa2Per <= 70)
			{
				$sub1Sa2Per = 'B2';
			}
			elseif($sub1Sa2Per >= 51 && $sub1Sa2Per <= 60)
			{
				$sub1Sa2Per = 'C1';
			}
			elseif($sub1Sa2Per >= 41 && $sub1Sa2Per <= 50)
			{
				$sub1Sa2Per = 'C2';
			}
			elseif($sub1Sa2Per >= 33 && $sub1Sa2Per <= 40)
			{
				$sub1Sa2Per = 'D';
			}
			elseif($sub1Sa2Per >= 21 && $sub1Sa2Per <= 32)
			{
				$sub1Sa2Per = 'E1';
			}
			elseif($sub1Sa2Per <= 20)
			{
				$sub1Sa2Per = 'E2';
			}
	  	
	  	$sub1Sa2Total = $sub1Fa3 + $sub1Fa4 + $sub1Sa2;
	  	$sub1Sa2TotalPer = number_format(($sub1Sa2Total * 100/50),0,'.',' ');
	  	if($sub1Sa2TotalPer >= 91 && $sub1Sa2TotalPer <= 100)
			{
			  $sub1Sa2TotalPer = 'A1';
			}
			elseif($sub1Sa2TotalPer >= 81 && $sub1Sa2TotalPer <= 90)
			{
				$sub1Sa2TotalPer = 'A2';
			}
			elseif($sub1Sa2TotalPer >= 71 && $sub1Sa2TotalPer <= 80)
			{
				$sub1Sa2TotalPer = 'B1';
			}
			elseif($sub1Sa2TotalPer >= 61 && $sub1Sa2TotalPer <= 70)
			{
				$sub1Sa2TotalPer = 'B2';
			}
			elseif($sub1Sa2TotalPer >= 51 && $sub1Sa2TotalPer <= 60)
			{
				$sub1Sa2TotalPer = 'C1';
			}
			elseif($sub1Sa2TotalPer >= 41 && $sub1Sa2TotalPer <= 50)
			{
				$sub1Sa2TotalPer = 'C2';
			}
			elseif($sub1Sa2TotalPer >= 33 && $sub1Sa2TotalPer <= 40)
			{
				$sub1Sa2TotalPer = 'D';
			}
			elseif($sub1Sa2TotalPer >= 21 && $sub1Sa2TotalPer <= 32)
			{
				$sub1Sa2TotalPer = 'E1';
			}
			elseif($sub1Sa2TotalPer <= 20)
			{
				$sub1Sa2TotalPer = 'E2';
			}
			
	  	$sub1faTotal = $sub1Fa1 + $sub1Fa2 + $sub1Fa3 + $sub1Fa4;
	  	$sub1faTotalPer = number_format(($sub1faTotal * 100/40),0,'.',' ');
	  	if($sub1faTotalPer >= 91 && $sub1faTotalPer <= 100)
			{
			  $sub1faTotalPer = 'A1';
			}
			elseif($sub1faTotalPer >= 81 && $sub1faTotalPer <= 90)
			{
				$sub1faTotalPer = 'A2';
			}
			elseif($sub1faTotalPer >= 71 && $sub1faTotalPer <= 80)
			{
				$sub1faTotalPer = 'B1';
			}
			elseif($sub1faTotalPer >= 61 && $sub1faTotalPer <= 70)
			{
				$sub1faTotalPer = 'B2';
			}
			elseif($sub1faTotalPer >= 51 && $sub1faTotalPer <= 60)
			{
				$sub1faTotalPer = 'C1';
			}
			elseif($sub1faTotalPer >= 41 && $sub1faTotalPer <= 50)
			{
				$sub1faTotalPer = 'C2';
			}
			elseif($sub1faTotalPer >= 33 && $sub1faTotalPer <= 40)
			{
				$sub1faTotalPer = 'D';
			}
			elseif($sub1faTotalPer >= 21 && $sub1faTotalPer <= 32)
			{
				$sub1faTotalPer = 'E1';
			}
			elseif($sub1faTotalPer <= 20)
			{
				$sub1faTotalPer = 'E2';
			}
	  	
	  	$sub1saTotal = $sub1Sa1 + $sub1Sa2;
	  	$sub1saTotalPer = number_format(($sub1saTotal * 100/60),0,'.',' ');
	  	if($sub1saTotalPer >= 91 && $sub1saTotalPer <= 100)
			{
			  $sub1saTotalPer = 'A1';
			}
			elseif($sub1saTotalPer >= 81 && $sub1saTotalPer <= 90)
			{
				$sub1saTotalPer = 'A2';
			}
			elseif($sub1saTotalPer >= 71 && $sub1saTotalPer <= 80)
			{
				$sub1saTotalPer = 'B1';
			}
			elseif($sub1saTotalPer >= 61 && $sub1saTotalPer <= 70)
			{
				$sub1saTotalPer = 'B2';
			}
			elseif($sub1saTotalPer >= 51 && $sub1saTotalPer <= 60)
			{
				$sub1saTotalPer = 'C1';
			}
			elseif($sub1saTotalPer >= 41 && $sub1saTotalPer <= 50)
			{
				$sub1saTotalPer = 'C2';
			}
			elseif($sub1saTotalPer >= 33 && $sub1saTotalPer <= 40)
			{
				$sub1saTotalPer = 'D';
			}
			elseif($sub1saTotalPer >= 21 && $sub1saTotalPer <= 32)
			{
				$sub1saTotalPer = 'E1';
			}
			elseif($sub1saTotalPer <= 20)
			{
				$sub1saTotalPer = 'E2';
			}
			
			$sub1fasaTotalPer = $sub1faTotal + $sub1saTotal;
			$sub1fasaTotal = number_format((($sub1fasaTotalPer) * 100/100),0,'.',' ');
	  	if($sub1fasaTotal >= 91 && $sub1fasaTotal <= 100)
			{
			  $sub1fasaTotal = 'A1';
			}
			elseif($sub1fasaTotal >= 81 && $sub1fasaTotal <= 90)
			{
				$sub1fasaTotal = 'A2';
			}
			elseif($sub1fasaTotal >= 71 && $sub1fasaTotal <= 80)
			{
				$sub1fasaTotal = 'B1';
			}
			elseif($sub1fasaTotal >= 61 && $sub1fasaTotal <= 70)
			{
				$sub1fasaTotal = 'B2';
			}
			elseif($sub1fasaTotal >= 51 && $sub1fasaTotal <= 60)
			{
				$sub1fasaTotal = 'C1';
			}
			elseif($sub1fasaTotal >= 41 && $sub1fasaTotal <= 50)
			{
				$sub1fasaTotal = 'C2';
			}
			elseif($sub1fasaTotal >= 33 && $sub1fasaTotal <= 40)
			{
				$sub1fasaTotal = 'D';
			}
			elseif($sub1fasaTotal >= 21 && $sub1fasaTotal <= 32)
			{
				$sub1fasaTotal = 'E1';
			}
			elseif($sub1fasaTotal <= 20)
			{
				$sub1fasaTotal = 'E2';
			}
	  	
	  	
	  	$pdf->SetFillColor(232, 232, 232);
	    $pdf->SetXY(8,115);
	    $pdf->SetFont('Arial', '', 10);
	    $pdf->Cell(40, 7, 'English', 1,0,'L');
		  $pdf->Cell(40, 7, $sub1Fa1Per, 1,0,'C');
		  $pdf->Cell(40, 7, $sub1Fa2Per, 1,0,'C');
		  $pdf->Cell(40, 7, $sub1Sa1Per, 1,0,'C');
		  $pdf->Cell(40, 7, $sub1Sa1TotalPer, 1,0,'C');
		  
		  ///////// subject 2
		  $sub2Fa1         = $stdRow['sub2Fa1'];
	  	$sub2Fa1Per      = number_format(($sub2Fa1 * 100/10),0,'.',' ');
      if($sub2Fa1Per >= 91 && $sub2Fa1Per <= 100)
			{
			  $sub2Fa1Per = 'A1';
			}
			elseif($sub2Fa1Per >= 81 && $sub2Fa1Per <= 90)
			{
				$sub2Fa1Per = 'A2';
			}
			elseif($sub2Fa1Per >= 71 && $sub2Fa1Per <= 80)
			{
				$sub2Fa1Per = 'B1';
			}
			elseif($sub2Fa1Per >= 61 && $sub2Fa1Per <= 70)
			{
				$sub2Fa1Per = 'B2';
			}
			elseif($sub2Fa1Per >= 51 && $sub2Fa1Per <= 60)
			{
				$sub2Fa1Per = 'C1';
			}
			elseif($sub2Fa1Per >= 41 && $sub2Fa1Per <= 50)
			{
				$sub2Fa1Per = 'C2';
			}
			elseif($sub2Fa1Per >= 33 && $sub2Fa1Per <= 40)
			{
				$sub2Fa1Per = 'D';
			}
			elseif($sub2Fa1Per >= 21 && $sub2Fa1Per <= 32)
			{
				$sub2Fa1Per = 'E1';
			}
			elseif($sub2Fa1Per <= 20)
			{
				$sub2Fa1Per = 'E2';
			}
			
	  	$sub2Fa2         = $stdRow['sub2Fa2'];
	  	$sub2Fa2Per = number_format(($sub2Fa2 * 100/10),0,'.',' ');
	  	if($sub2Fa2Per >= 91 && $sub2Fa2Per <= 100)
			{
			  $sub2Fa2Per = 'A1';
			}
			elseif($sub2Fa2Per >= 81 && $sub2Fa2Per <= 90)
			{
				$sub2Fa2Per = 'A2';
			}
			elseif($sub2Fa2Per >= 71 && $sub2Fa2Per <= 80)
			{
				$sub2Fa2Per = 'B1';
			}
			elseif($sub2Fa2Per >= 61 && $sub2Fa2Per <= 70)
			{
				$sub2Fa2Per = 'B2';
			}
			elseif($sub2Fa2Per >= 51 && $sub2Fa2Per <= 60)
			{
				$sub2Fa2Per = 'C1';
			}
			elseif($sub2Fa2Per >= 41 && $sub2Fa2Per <= 50)
			{
				$sub2Fa2Per = 'C2';
			}
			elseif($sub2Fa2Per >= 33 && $sub2Fa2Per <= 40)
			{
				$sub2Fa2Per = 'D';
			}
			elseif($sub2Fa2Per >= 21 && $sub2Fa2Per <= 32)
			{
				$sub2Fa2Per = 'E1';
			}
			elseif($sub2Fa2Per <= 20)
			{
				$sub2Fa2Per = 'E2';
			}
	  	
	  	$sub2Sa1         = $stdRow['sub2Sa1'];
	  	$sub2Sa1Per = number_format(($sub2Sa1 * 100/30),0,'.',' ');
	  	if($sub2Sa1Per >= 91 && $sub2Sa1Per <= 100)
			{
			  $sub2Sa1Per = 'A1';
			}
			elseif($sub2Sa1Per >= 81 && $sub2Sa1Per <= 90)
			{
				$sub2Sa1Per = 'A2';
			}
			elseif($sub2Sa1Per >= 71 && $sub2Sa1Per <= 80)
			{
				$sub2Sa1Per = 'B1';
			}
			elseif($sub2Sa1Per >= 61 && $sub2Sa1Per <= 70)
			{
				$sub2Sa1Per = 'B2';
			}
			elseif($sub2Sa1Per >= 51 && $sub2Sa1Per <= 60)
			{
				$sub2Sa1Per = 'C1';
			}
			elseif($sub2Sa1Per >= 41 && $sub2Sa1Per <= 50)
			{
				$sub2Sa1Per = 'C2';
			}
			elseif($sub2Sa1Per >= 33 && $sub2Sa1Per <= 40)
			{
				$sub2Sa1Per = 'D';
			}
			elseif($sub2Sa1Per >= 21 && $sub2Sa1Per <= 32)
			{
				$sub2Sa1Per = 'E1';
			}
			elseif($sub2Sa1Per <= 20)
			{
				$sub2Sa1Per = 'E2';
			}
	    
	    $sub2Sa1Total = $sub2Fa1 + $sub2Fa2 + $sub2Sa1;
	    $sub2Sa1TotalPer = number_format(($sub2Sa1Total * 100/50),0,'.',' ');
	    if($sub2Sa1TotalPer >= 91 && $sub2Sa1TotalPer <= 100)
			{
			  $sub2Sa1TotalPer = 'A1';
			}
			elseif($sub2Sa1TotalPer >= 81 && $sub2Sa1TotalPer <= 90)
			{
				$sub2Sa1TotalPer = 'A2';
			}
			elseif($sub2Sa1TotalPer >= 71 && $sub2Sa1TotalPer <= 80)
			{
				$sub2Sa1TotalPer = 'B1';
			}
			elseif($sub2Sa1TotalPer >= 61 && $sub2Sa1TotalPer <= 70)
			{
				$sub2Sa1TotalPer = 'B2';
			}
			elseif($sub2Sa1TotalPer >= 51 && $sub2Sa1TotalPer <= 60)
			{
				$sub2Sa1TotalPer = 'C1';
			}
			elseif($sub2Sa1TotalPer >= 41 && $sub2Sa1TotalPer <= 50)
			{
				$sub2Sa1TotalPer = 'C2';
			}
			elseif($sub2Sa1TotalPer >= 33 && $sub2Sa1TotalPer <= 40)
			{
				$sub2Sa1TotalPer = 'D';
			}
			elseif($sub2Sa1TotalPer >= 21 && $sub2Sa1TotalPer <= 32)
			{
				$sub2Sa1TotalPer = 'E1';
			}
			elseif($sub2Sa1TotalPer <= 20)
			{
				$sub2Sa1TotalPer = 'E2';
			}

	    
	  	$sub2Fa3         = $stdRow['sub2Fa3'];
	  	$sub2Fa3Per = number_format(($sub2Fa3 * 100/10),0,'.',' ');
	  	if($sub2Fa3Per >= 91 && $sub2Fa3Per <= 100)
			{
			  $sub2Fa3Per = 'A1';
			}
			elseif($sub2Fa3Per >= 81 && $sub2Fa3Per <= 90)
			{
				$sub2Fa3Per = 'A2';
			}
			elseif($sub2Fa3Per >= 71 && $sub2Fa3Per <= 80)
			{
				$sub2Fa3Per = 'B1';
			}
			elseif($sub2Fa3Per >= 61 && $sub2Fa3Per <= 70)
			{
				$sub2Fa3Per = 'B2';
			}
			elseif($sub2Fa3Per >= 51 && $sub2Fa3Per <= 60)
			{
				$sub2Fa3Per = 'C1';
			}
			elseif($sub2Fa3Per >= 41 && $sub2Fa3Per <= 50)
			{
				$sub2Fa3Per = 'C2';
			}
			elseif($sub2Fa3Per >= 33 && $sub2Fa3Per <= 40)
			{
				$sub2Fa3Per = 'D';
			}
			elseif($sub2Fa3Per >= 21 && $sub2Fa3Per <= 32)
			{
				$sub2Fa3Per = 'E1';
			}
			elseif($sub2Fa3Per <= 20)
			{
				$sub2Fa3Per = 'E2';
			}
			
	  	$sub2Fa4         = $stdRow['sub2Fa4'];
	  	$sub2Fa4Per = number_format(($sub2Fa4 * 100/10),0,'.',' ');
	  	if($sub2Fa4Per >= 91 && $sub2Fa4Per <= 100)
			{
			  $sub2Fa4Per = 'A1';
			}
			elseif($sub2Fa4Per >= 81 && $sub2Fa4Per <= 90)
			{
				$sub2Fa4Per = 'A2';
			}
			elseif($sub2Fa4Per >= 71 && $sub2Fa4Per <= 80)
			{
				$sub2Fa4Per = 'B1';
			}
			elseif($sub2Fa4Per >= 61 && $sub2Fa4Per <= 70)
			{
				$sub2Fa4Per = 'B2';
			}
			elseif($sub2Fa4Per >= 51 && $sub2Fa4Per <= 60)
			{
				$sub2Fa4Per = 'C1';
			}
			elseif($sub2Fa4Per >= 41 && $sub2Fa4Per <= 50)
			{
				$sub2Fa4Per = 'C2';
			}
			elseif($sub2Fa4Per >= 33 && $sub2Fa4Per <= 40)
			{
				$sub2Fa4Per = 'D';
			}
			elseif($sub2Fa4Per >= 21 && $sub2Fa4Per <= 32)
			{
				$sub2Fa4Per = 'E1';
			}
			elseif($sub2Fa4Per <= 20)
			{
				$sub2Fa4Per = 'E2';
			}
	  	
	  	$sub2Sa2         = $stdRow['sub2Sa2'];
	  	$sub2Sa2Per = number_format(($sub2Sa2 * 100/30),0,'.',' ');
	  	if($sub2Sa2Per >= 91 && $sub2Sa2Per <= 100)
			{
			  $sub2Sa2Per = 'A1';
			}
			elseif($sub2Sa2Per >= 81 && $sub2Sa2Per <= 90)
			{
				$sub2Sa2Per = 'A2';
			}
			elseif($sub2Sa2Per >= 71 && $sub2Sa2Per <= 80)
			{
				$sub2Sa2Per = 'B1';
			}
			elseif($sub2Sa2Per >= 61 && $sub2Sa2Per <= 70)
			{
				$sub2Sa2Per = 'B2';
			}
			elseif($sub2Sa2Per >= 51 && $sub2Sa2Per <= 60)
			{
				$sub2Sa2Per = 'C1';
			}
			elseif($sub2Sa2Per >= 41 && $sub2Sa2Per <= 50)
			{
				$sub2Sa2Per = 'C2';
			}
			elseif($sub2Sa2Per >= 33 && $sub2Sa2Per <= 40)
			{
				$sub2Sa2Per = 'D';
			}
			elseif($sub2Sa2Per >= 21 && $sub2Sa2Per <= 32)
			{
				$sub2Sa2Per = 'E1';
			}
			elseif($sub2Sa2Per <= 20)
			{
				$sub2Sa2Per = 'E2';
			}
	  	
	  	$sub2Sa2Total = $sub2Fa3 + $sub2Fa4 + $sub2Sa2;
	  	$sub2Sa2TotalPer = number_format(($sub2Sa2Total * 100/50),0,'.',' ');
	  	if($sub2Sa2TotalPer >= 91 && $sub2Sa2TotalPer <= 100)
			{
			  $sub2Sa2TotalPer = 'A1';
			}
			elseif($sub2Sa2TotalPer >= 81 && $sub2Sa2TotalPer <= 90)
			{
				$sub2Sa2TotalPer = 'A2';
			}
			elseif($sub2Sa2TotalPer >= 71 && $sub2Sa2TotalPer <= 80)
			{
				$sub2Sa2TotalPer = 'B1';
			}
			elseif($sub2Sa2TotalPer >= 61 && $sub2Sa2TotalPer <= 70)
			{
				$sub2Sa2TotalPer = 'B2';
			}
			elseif($sub2Sa2TotalPer >= 51 && $sub2Sa2TotalPer <= 60)
			{
				$sub2Sa2TotalPer = 'C1';
			}
			elseif($sub2Sa2TotalPer >= 41 && $sub2Sa2TotalPer <= 50)
			{
				$sub2Sa2TotalPer = 'C2';
			}
			elseif($sub2Sa2TotalPer >= 33 && $sub2Sa2TotalPer <= 40)
			{
				$sub2Sa2TotalPer = 'D';
			}
			elseif($sub2Sa2TotalPer >= 21 && $sub2Sa2TotalPer <= 32)
			{
				$sub2Sa2TotalPer = 'E1';
			}
			elseif($sub2Sa2TotalPer <= 20)
			{
				$sub2Sa2TotalPer = 'E2';
			}
			
	  	$sub2faTotal = $sub2Fa1 + $sub2Fa2 + $sub2Fa3 + $sub2Fa4;
	  	$sub2faTotalPer = number_format(($sub2faTotal * 100/40),0,'.',' ');
	  	if($sub2faTotalPer >= 91 && $sub2faTotalPer <= 100)
			{
			  $sub2faTotalPer = 'A1';
			}
			elseif($sub2faTotalPer >= 81 && $sub2faTotalPer <= 90)
			{
				$sub2faTotalPer = 'A2';
			}
			elseif($sub2faTotalPer >= 71 && $sub2faTotalPer <= 80)
			{
				$sub2faTotalPer = 'B1';
			}
			elseif($sub2faTotalPer >= 61 && $sub2faTotalPer <= 70)
			{
				$sub2faTotalPer = 'B2';
			}
			elseif($sub2faTotalPer >= 51 && $sub2faTotalPer <= 60)
			{
				$sub2faTotalPer = 'C1';
			}
			elseif($sub2faTotalPer >= 41 && $sub2faTotalPer <= 50)
			{
				$sub2faTotalPer = 'C2';
			}
			elseif($sub2faTotalPer >= 33 && $sub2faTotalPer <= 40)
			{
				$sub2faTotalPer = 'D';
			}
			elseif($sub2faTotalPer >= 21 && $sub2faTotalPer <= 32)
			{
				$sub2faTotalPer = 'E1';
			}
			elseif($sub2faTotalPer <= 20)
			{
				$sub2faTotalPer = 'E2';
			}
	  	
	  	$sub2saTotal = $sub2Sa1 + $sub2Sa2;
	  	$sub2saTotalPer = number_format(($sub2saTotal * 100/60),0,'.',' ');
	  	if($sub2saTotalPer >= 91 && $sub2saTotalPer <= 100)
			{
			  $sub2saTotalPer = 'A1';
			}
			elseif($sub2saTotalPer >= 81 && $sub2saTotalPer <= 90)
			{
				$sub2saTotalPer = 'A2';
			}
			elseif($sub2saTotalPer >= 71 && $sub2saTotalPer <= 80)
			{
				$sub2saTotalPer = 'B1';
			}
			elseif($sub2saTotalPer >= 61 && $sub2saTotalPer <= 70)
			{
				$sub2saTotalPer = 'B2';
			}
			elseif($sub2saTotalPer >= 51 && $sub2saTotalPer <= 60)
			{
				$sub2saTotalPer = 'C1';
			}
			elseif($sub2saTotalPer >= 41 && $sub2saTotalPer <= 50)
			{
				$sub2saTotalPer = 'C2';
			}
			elseif($sub2saTotalPer >= 33 && $sub2saTotalPer <= 40)
			{
				$sub2saTotalPer = 'D';
			}
			elseif($sub2saTotalPer >= 21 && $sub2saTotalPer <= 32)
			{
				$sub2saTotalPer = 'E1';
			}
			elseif($sub2saTotalPer <= 20)
			{
				$sub2saTotalPer = 'E2';
			}
			
			$sub2fasaTotalPer = $sub2faTotal + $sub2saTotal;
			$sub2fasaTotal = number_format((($sub2fasaTotalPer) * 100/100),0,'.',' ');
	  	if($sub2fasaTotal >= 91 && $sub2fasaTotal <= 100)
			{
			  $sub2fasaTotal = 'A1';
			}
			elseif($sub2fasaTotal >= 81 && $sub2fasaTotal <= 90)
			{
				$sub2fasaTotal = 'A2';
			}
			elseif($sub2fasaTotal >= 71 && $sub2fasaTotal <= 80)
			{
				$sub2fasaTotal = 'B1';
			}
			elseif($sub2fasaTotal >= 61 && $sub2fasaTotal <= 70)
			{
				$sub2fasaTotal = 'B2';
			}
			elseif($sub2fasaTotal >= 51 && $sub2fasaTotal <= 60)
			{
				$sub2fasaTotal = 'C1';
			}
			elseif($sub2fasaTotal >= 41 && $sub2fasaTotal <= 50)
			{
				$sub2fasaTotal = 'C2';
			}
			elseif($sub2fasaTotal >= 33 && $sub2fasaTotal <= 40)
			{
				$sub2fasaTotal = 'D';
			}
			elseif($sub2fasaTotal >= 21 && $sub2fasaTotal <= 32)
			{
				$sub2fasaTotal = 'E1';
			}
			elseif($sub2fasaTotal <= 20)
			{
				$sub2fasaTotal = 'E2';
			}
	  	
	  	
	  	$pdf->SetFillColor(232, 232, 232);
	    $pdf->SetXY(8,122);
	    $pdf->SetFont('Arial', '', 10);
	    $pdf->Cell(40, 7, 'Hindi', 1,0,'L');
		  $pdf->Cell(40, 7, $sub2Fa1Per, 1,0,'C');
		  $pdf->Cell(40, 7, $sub2Fa2Per, 1,0,'C');
		  $pdf->Cell(40, 7, $sub2Sa1Per, 1,0,'C');
		  $pdf->Cell(40, 7, $sub2Sa1TotalPer, 1,0,'C');
		  
		  ///////// subject 3
		  $sub3Fa1         = $stdRow['sub3Fa1'];
	  	$sub3Fa1Per      = number_format(($sub3Fa1 * 100/10),0,'.',' ');
      if($sub3Fa1Per >= 91 && $sub3Fa1Per <= 100)
			{
			  $sub3Fa1Per = 'A1';
			}
			elseif($sub3Fa1Per >= 81 && $sub3Fa1Per <= 90)
			{
				$sub3Fa1Per = 'A2';
			}
			elseif($sub3Fa1Per >= 71 && $sub3Fa1Per <= 80)
			{
				$sub3Fa1Per = 'B1';
			}
			elseif($sub3Fa1Per >= 61 && $sub3Fa1Per <= 70)
			{
				$sub3Fa1Per = 'B2';
			}
			elseif($sub3Fa1Per >= 51 && $sub3Fa1Per <= 60)
			{
				$sub3Fa1Per = 'C1';
			}
			elseif($sub3Fa1Per >= 41 && $sub3Fa1Per <= 50)
			{
				$sub3Fa1Per = 'C2';
			}
			elseif($sub3Fa1Per >= 33 && $sub3Fa1Per <= 40)
			{
				$sub3Fa1Per = 'D';
			}
			elseif($sub3Fa1Per >= 21 && $sub3Fa1Per <= 32)
			{
				$sub3Fa1Per = 'E1';
			}
			elseif($sub3Fa1Per <= 20)
			{
				$sub3Fa1Per = 'E2';
			}
			
	  	$sub3Fa2         = $stdRow['sub3Fa2'];
	  	$sub3Fa2Per = number_format(($sub3Fa2 * 100/10),0,'.',' ');
	  	if($sub3Fa2Per >= 91 && $sub3Fa2Per <= 100)
			{
			  $sub3Fa2Per = 'A1';
			}
			elseif($sub3Fa2Per >= 81 && $sub3Fa2Per <= 90)
			{
				$sub3Fa2Per = 'A2';
			}
			elseif($sub3Fa2Per >= 71 && $sub3Fa2Per <= 80)
			{
				$sub3Fa2Per = 'B1';
			}
			elseif($sub3Fa2Per >= 61 && $sub3Fa2Per <= 70)
			{
				$sub3Fa2Per = 'B2';
			}
			elseif($sub3Fa2Per >= 51 && $sub3Fa2Per <= 60)
			{
				$sub3Fa2Per = 'C1';
			}
			elseif($sub3Fa2Per >= 41 && $sub3Fa2Per <= 50)
			{
				$sub3Fa2Per = 'C2';
			}
			elseif($sub3Fa2Per >= 33 && $sub3Fa2Per <= 40)
			{
				$sub3Fa2Per = 'D';
			}
			elseif($sub3Fa2Per >= 21 && $sub3Fa2Per <= 32)
			{
				$sub3Fa2Per = 'E1';
			}
			elseif($sub3Fa2Per <= 20)
			{
				$sub3Fa2Per = 'E2';
			}
	  	
	  	$sub3Sa1         = $stdRow['sub3Sa1'];
	  	$sub3Sa1Per = number_format(($sub3Sa1 * 100/30),0,'.',' ');
	  	if($sub3Sa1Per >= 91 && $sub3Sa1Per <= 100)
			{
			  $sub3Sa1Per = 'A1';
			}
			elseif($sub3Sa1Per >= 81 && $sub3Sa1Per <= 90)
			{
				$sub3Sa1Per = 'A2';
			}
			elseif($sub3Sa1Per >= 71 && $sub3Sa1Per <= 80)
			{
				$sub3Sa1Per = 'B1';
			}
			elseif($sub3Sa1Per >= 61 && $sub3Sa1Per <= 70)
			{
				$sub3Sa1Per = 'B2';
			}
			elseif($sub3Sa1Per >= 51 && $sub3Sa1Per <= 60)
			{
				$sub3Sa1Per = 'C1';
			}
			elseif($sub3Sa1Per >= 41 && $sub3Sa1Per <= 50)
			{
				$sub3Sa1Per = 'C2';
			}
			elseif($sub3Sa1Per >= 33 && $sub3Sa1Per <= 40)
			{
				$sub3Sa1Per = 'D';
			}
			elseif($sub3Sa1Per >= 21 && $sub3Sa1Per <= 32)
			{
				$sub3Sa1Per = 'E1';
			}
			elseif($sub3Sa1Per <= 20)
			{
				$sub3Sa1Per = 'E2';
			}
	    
	    $sub3Sa1Total = $sub3Fa1 + $sub3Fa2 + $sub3Sa1;
            $sub3Sa1TotalPer = number_format(($sub3Sa1Total * 100/50),0,'.',' ');
            
	    if($sub3Sa1TotalPer >= 91 && $sub3Sa1TotalPer <= 100)
			{
                          $sub3Sa1TotalPer = 'A1';
			}
			elseif($sub3Sa1TotalPer >= 81 && $sub3Sa1TotalPer <= 90)
			{
				$sub3Sa1TotalPer = 'A2';
			}
			elseif($sub3Sa1TotalPer >= 71 && $sub3Sa1TotalPer <= 80)
			{
				$sub3Sa1TotalPer = 'B1';
			}
			elseif($sub3Sa1TotalPer >= 61 && $sub3Sa1TotalPer <= 70)
			{
				$sub3Sa1TotalPer = 'B2';
			}
			elseif($sub3Sa1TotalPer >= 51 && $sub3Sa1TotalPer <= 60)
			{
				$sub3Sa1TotalPer = 'C1';
			}
			elseif($sub3Sa1TotalPer >= 41 && $sub3Sa1TotalPer <= 50)
			{
				$sub3Sa1TotalPer = 'C2';
			}
			elseif($sub3Sa1TotalPer >= 33 && $sub3Sa1TotalPer <= 40)
			{
				$sub3Sa1TotalPer = 'D';
			}
			elseif($sub3Sa1TotalPer >= 21 && $sub3Sa1TotalPer <= 32)
			{
				$sub3Sa1TotalPer = 'E1';
			}
			elseif($sub3Sa1TotalPer <= 20)
			{
				$sub3Sa1TotalPer = 'E2';
			}

	    
	  	$sub3Fa3         = $stdRow['sub3Fa3'];
	  	$sub3Fa3Per = number_format(($sub3Fa3 * 100/10),0,'.',' ');
	  	if($sub3Fa3Per >= 91 && $sub3Fa3Per <= 100)
			{
			  $sub3Fa3Per = 'A1';
			}
			elseif($sub3Fa3Per >= 81 && $sub3Fa3Per <= 90)
			{
				$sub3Fa3Per = 'A2';
			}
			elseif($sub3Fa3Per >= 71 && $sub3Fa3Per <= 80)
			{
				$sub3Fa3Per = 'B1';
			}
			elseif($sub3Fa3Per >= 61 && $sub3Fa3Per <= 70)
			{
				$sub3Fa3Per = 'B2';
			}
			elseif($sub3Fa3Per >= 51 && $sub3Fa3Per <= 60)
			{
				$sub3Fa3Per = 'C1';
			}
			elseif($sub3Fa3Per >= 41 && $sub3Fa3Per <= 50)
			{
				$sub3Fa3Per = 'C2';
			}
			elseif($sub3Fa3Per >= 33 && $sub3Fa3Per <= 40)
			{
				$sub3Fa3Per = 'D';
			}
			elseif($sub3Fa3Per >= 21 && $sub3Fa3Per <= 32)
			{
				$sub3Fa3Per = 'E1';
			}
			elseif($sub3Fa3Per <= 20)
			{
				$sub3Fa3Per = 'E2';
			}
			
	  	$sub3Fa4         = $stdRow['sub3Fa4'];
	  	$sub3Fa4Per = number_format(($sub3Fa4 * 100/10),0,'.',' ');
	  	if($sub3Fa4Per >= 91 && $sub3Fa4Per <= 100)
			{
			  $sub3Fa4Per = 'A1';
			}
			elseif($sub3Fa4Per >= 81 && $sub3Fa4Per <= 90)
			{
				$sub3Fa4Per = 'A2';
			}
			elseif($sub3Fa4Per >= 71 && $sub3Fa4Per <= 80)
			{
				$sub3Fa4Per = 'B1';
			}
			elseif($sub3Fa4Per >= 61 && $sub3Fa4Per <= 70)
			{
				$sub3Fa4Per = 'B2';
			}
			elseif($sub3Fa4Per >= 51 && $sub3Fa4Per <= 60)
			{
				$sub3Fa4Per = 'C1';
			}
			elseif($sub3Fa4Per >= 41 && $sub3Fa4Per <= 50)
			{
				$sub3Fa4Per = 'C2';
			}
			elseif($sub3Fa4Per >= 33 && $sub3Fa4Per <= 40)
			{
				$sub3Fa4Per = 'D';
			}
			elseif($sub3Fa4Per >= 21 && $sub3Fa4Per <= 32)
			{
				$sub3Fa4Per = 'E1';
			}
			elseif($sub3Fa4Per <= 20)
			{
				$sub3Fa4Per = 'E2';
			}
	  	
	  	$sub3Sa2         = $stdRow['sub3Sa2'];
	  	$sub3Sa2Per = number_format(($sub3Sa2 * 100/30),0,'.',' ');
	  	if($sub3Sa2Per >= 91 && $sub3Sa2Per <= 100)
			{
			  $sub3Sa2Per = 'A1';
			}
			elseif($sub3Sa2Per >= 81 && $sub3Sa2Per <= 90)
			{
				$sub3Sa2Per = 'A2';
			}
			elseif($sub3Sa2Per >= 71 && $sub3Sa2Per <= 80)
			{
				$sub3Sa2Per = 'B1';
			}
			elseif($sub3Sa2Per >= 61 && $sub3Sa2Per <= 70)
			{
				$sub3Sa2Per = 'B2';
			}
			elseif($sub3Sa2Per >= 51 && $sub3Sa2Per <= 60)
			{
				$sub3Sa2Per = 'C1';
			}
			elseif($sub3Sa2Per >= 41 && $sub3Sa2Per <= 50)
			{
				$sub3Sa2Per = 'C2';
			}
			elseif($sub3Sa2Per >= 33 && $sub3Sa2Per <= 40)
			{
				$sub3Sa2Per = 'D';
			}
			elseif($sub3Sa2Per >= 21 && $sub3Sa2Per <= 32)
			{
				$sub3Sa2Per = 'E1';
			}
			elseif($sub3Sa2Per <= 20)
			{
				$sub3Sa2Per = 'E2';
			}
	  	
	  	$sub3Sa2Total = $sub3Fa3 + $sub3Fa4 + $sub3Sa2;
	  	$sub3Sa2TotalPer = number_format(($sub3Sa2Total * 100/50),0,'.',' ');
	  	if($sub3Sa2TotalPer >= 91 && $sub3Sa2TotalPer <= 100)
			{
			  $sub3Sa2TotalPer = 'A1';
			}
			elseif($sub3Sa2TotalPer >= 81 && $sub3Sa2TotalPer <= 90)
			{
				$sub3Sa2TotalPer = 'A2';
			}
			elseif($sub3Sa2TotalPer >= 71 && $sub3Sa2TotalPer <= 80)
			{
				$sub3Sa2TotalPer = 'B1';
			}
			elseif($sub3Sa2TotalPer >= 61 && $sub3Sa2TotalPer <= 70)
			{
				$sub3Sa2TotalPer = 'B2';
			}
			elseif($sub3Sa2TotalPer >= 51 && $sub3Sa2TotalPer <= 60)
			{
				$sub3Sa2TotalPer = 'C1';
			}
			elseif($sub3Sa2TotalPer >= 41 && $sub3Sa2TotalPer <= 50)
			{
				$sub3Sa2TotalPer = 'C2';
			}
			elseif($sub3Sa2TotalPer >= 33 && $sub3Sa2TotalPer <= 40)
			{
				$sub3Sa2TotalPer = 'D';
			}
			elseif($sub3Sa2TotalPer >= 21 && $sub3Sa2TotalPer <= 32)
			{
				$sub3Sa2TotalPer = 'E1';
			}
			elseif($sub3Sa2TotalPer <= 20)
			{
				$sub3Sa2TotalPer = 'E2';
			}
			
	  	$sub3faTotal = $sub3Fa1 + $sub3Fa2 + $sub3Fa3 + $sub3Fa4;
	  	$sub3faTotalPer = number_format(($sub3faTotal * 100/40),0,'.',' ');
	  	if($sub3faTotalPer >= 91 && $sub3faTotalPer <= 100)
			{
			  $sub3faTotalPer = 'A1';
			}
			elseif($sub3faTotalPer >= 81 && $sub3faTotalPer <= 90)
			{
				$sub3faTotalPer = 'A2';
			}
			elseif($sub3faTotalPer >= 71 && $sub3faTotalPer <= 80)
			{
				$sub3faTotalPer = 'B1';
			}
			elseif($sub3faTotalPer >= 61 && $sub3faTotalPer <= 70)
			{
				$sub3faTotalPer = 'B2';
			}
			elseif($sub3faTotalPer >= 51 && $sub3faTotalPer <= 60)
			{
				$sub3faTotalPer = 'C1';
			}
			elseif($sub3faTotalPer >= 41 && $sub3faTotalPer <= 50)
			{
				$sub3faTotalPer = 'C2';
			}
			elseif($sub3faTotalPer >= 33 && $sub3faTotalPer <= 40)
			{
				$sub3faTotalPer = 'D';
			}
			elseif($sub3faTotalPer >= 21 && $sub3faTotalPer <= 32)
			{
				$sub3faTotalPer = 'E1';
			}
			elseif($sub3faTotalPer <= 20)
			{
				$sub3faTotalPer = 'E2';
			}
	  	
	  	$sub3saTotal = $sub3Sa1 + $sub3Sa2;
	  	$sub3saTotalPer = number_format(($sub3saTotal * 100/60),0,'.',' ');
	  	if($sub3saTotalPer >= 91 && $sub3saTotalPer <= 100)
			{
			  $sub3saTotalPer = 'A1';
			}
			elseif($sub3saTotalPer >= 81 && $sub3saTotalPer <= 90)
			{
				$sub3saTotalPer = 'A2';
			}
			elseif($sub3saTotalPer >= 71 && $sub3saTotalPer <= 80)
			{
				$sub3saTotalPer = 'B1';
			}
			elseif($sub3saTotalPer >= 61 && $sub3saTotalPer <= 70)
			{
				$sub3saTotalPer = 'B2';
			}
			elseif($sub3saTotalPer >= 51 && $sub3saTotalPer <= 60)
			{
				$sub3saTotalPer = 'C1';
			}
			elseif($sub3saTotalPer >= 41 && $sub3saTotalPer <= 50)
			{
				$sub3saTotalPer = 'C2';
			}
			elseif($sub3saTotalPer >= 33 && $sub3saTotalPer <= 40)
			{
				$sub3saTotalPer = 'D';
			}
			elseif($sub3saTotalPer >= 21 && $sub3saTotalPer <= 32)
			{
				$sub3saTotalPer = 'E1';
			}
			elseif($sub3saTotalPer <= 20)
			{
				$sub3saTotalPer = 'E2';
			}
			
			$sub3fasaTotalPer = $sub3faTotal + $sub3saTotal;
			$sub3fasaTotal = number_format((($sub3fasaTotalPer) * 100/100),0,'.',' ');
	  	if($sub3fasaTotal >= 91 && $sub3fasaTotal <= 100)
			{
			  $sub3fasaTotal = 'A1';
			}
			elseif($sub3fasaTotal >= 81 && $sub3fasaTotal <= 90)
			{
				$sub3fasaTotal = 'A2';
			}
			elseif($sub3fasaTotal >= 71 && $sub3fasaTotal <= 80)
			{
				$sub3fasaTotal = 'B1';
			}
			elseif($sub3fasaTotal >= 61 && $sub3fasaTotal <= 70)
			{
				$sub3fasaTotal = 'B2';
			}
			elseif($sub3fasaTotal >= 51 && $sub3fasaTotal <= 60)
			{
				$sub3fasaTotal = 'C1';
			}
			elseif($sub3fasaTotal >= 41 && $sub3fasaTotal <= 50)
			{
				$sub3fasaTotal = 'C2';
			}
			elseif($sub3fasaTotal >= 33 && $sub3fasaTotal <= 40)
			{
				$sub3fasaTotal = 'D';
			}
			elseif($sub3fasaTotal >= 21 && $sub3fasaTotal <= 32)
			{
				$sub3fasaTotal = 'E1';
			}
			elseif($sub3fasaTotal <= 20)
			{
				$sub3fasaTotal = 'E2';
			}
	  	
	  	
	  	$pdf->SetFillColor(232, 232, 232);
	    $pdf->SetXY(8,129);
	    $pdf->SetFont('Arial', '', 10);
	    $pdf->Cell(40, 7, 'Mathematics', 1,0,'L');
		  $pdf->Cell(40, 7, $sub3Fa1Per, 1,0,'C');
		  $pdf->Cell(40, 7, $sub3Fa2Per, 1,0,'C');
		  $pdf->Cell(40, 7, $sub3Sa1Per, 1,0,'C');
		  $pdf->Cell(40, 7, $sub3Sa1TotalPer, 1,0,'C');
		  
		  ///////// subject 4
		  $sub4Fa1         = $stdRow['sub4Fa1'];
	  	$sub4Fa1Per      = number_format(($sub4Fa1 * 100/10),0,'.',' ');
      if($sub4Fa1Per >= 91 && $sub4Fa1Per <= 100)
			{
			  $sub4Fa1Per = 'A1';
			}
			elseif($sub4Fa1Per >= 81 && $sub4Fa1Per <= 90)
			{
				$sub4Fa1Per = 'A2';
			}
			elseif($sub4Fa1Per >= 71 && $sub4Fa1Per <= 80)
			{
				$sub4Fa1Per = 'B1';
			}
			elseif($sub4Fa1Per >= 61 && $sub4Fa1Per <= 70)
			{
				$sub4Fa1Per = 'B2';
			}
			elseif($sub4Fa1Per >= 51 && $sub4Fa1Per <= 60)
			{
				$sub4Fa1Per = 'C1';
			}
			elseif($sub4Fa1Per >= 41 && $sub4Fa1Per <= 50)
			{
				$sub4Fa1Per = 'C2';
			}
			elseif($sub4Fa1Per >= 33 && $sub4Fa1Per <= 40)
			{
				$sub4Fa1Per = 'D';
			}
			elseif($sub4Fa1Per >= 21 && $sub4Fa1Per <= 32)
			{
				$sub4Fa1Per = 'E1';
			}
			elseif($sub4Fa1Per <= 20)
			{
				$sub4Fa1Per = 'E2';
			}
			
	  	$sub4Fa2         = $stdRow['sub4Fa2'];
	  	$sub4Fa2Per = number_format(($sub4Fa2 * 100/10),0,'.',' ');
	  	if($sub4Fa2Per >= 91 && $sub4Fa2Per <= 100)
			{
			  $sub4Fa2Per = 'A1';
			}
			elseif($sub4Fa2Per >= 81 && $sub4Fa2Per <= 90)
			{
				$sub4Fa2Per = 'A2';
			}
			elseif($sub4Fa2Per >= 71 && $sub4Fa2Per <= 80)
			{
				$sub4Fa2Per = 'B1';
			}
			elseif($sub4Fa2Per >= 61 && $sub4Fa2Per <= 70)
			{
				$sub4Fa2Per = 'B2';
			}
			elseif($sub4Fa2Per >= 51 && $sub4Fa2Per <= 60)
			{
				$sub4Fa2Per = 'C1';
			}
			elseif($sub4Fa2Per >= 41 && $sub4Fa2Per <= 50)
			{
				$sub4Fa2Per = 'C2';
			}
			elseif($sub4Fa2Per >= 33 && $sub4Fa2Per <= 40)
			{
				$sub4Fa2Per = 'D';
			}
			elseif($sub4Fa2Per >= 21 && $sub4Fa2Per <= 32)
			{
				$sub4Fa2Per = 'E1';
			}
			elseif($sub4Fa2Per <= 20)
			{
				$sub4Fa2Per = 'E2';
			}
	  	
	  	$sub4Sa1         = $stdRow['sub4Sa1'];
	  	$sub4Sa1Per = number_format(($sub4Sa1 * 100/30),0,'.',' ');
	  	if($sub4Sa1Per >= 91 && $sub4Sa1Per <= 100)
			{
			  $sub4Sa1Per = 'A1';
			}
			elseif($sub4Sa1Per >= 81 && $sub4Sa1Per <= 90)
			{
				$sub4Sa1Per = 'A2';
			}
			elseif($sub4Sa1Per >= 71 && $sub4Sa1Per <= 80)
			{
				$sub4Sa1Per = 'B1';
			}
			elseif($sub4Sa1Per >= 61 && $sub4Sa1Per <= 70)
			{
				$sub4Sa1Per = 'B2';
			}
			elseif($sub4Sa1Per >= 51 && $sub4Sa1Per <= 60)
			{
				$sub4Sa1Per = 'C1';
			}
			elseif($sub4Sa1Per >= 41 && $sub4Sa1Per <= 50)
			{
				$sub4Sa1Per = 'C2';
			}
			elseif($sub4Sa1Per >= 33 && $sub4Sa1Per <= 40)
			{
				$sub4Sa1Per = 'D';
			}
			elseif($sub4Sa1Per >= 21 && $sub4Sa1Per <= 32)
			{
				$sub4Sa1Per = 'E1';
			}
			elseif($sub4Sa1Per <= 20)
			{
				$sub4Sa1Per = 'E2';
			}
	    
	    $sub4Sa1Total = $sub4Fa1 + $sub4Fa2 + $sub4Sa1;
	    $sub4Sa1TotalPer = number_format(($sub4Sa1Total * 100/50),0,'.',' ');
	    if($sub4Sa1TotalPer >= 91 && $sub4Sa1TotalPer <= 100)
			{
			  $sub4Sa1TotalPer = 'A1';
			}
			elseif($sub4Sa1TotalPer >= 81 && $sub4Sa1TotalPer <= 90)
			{
				$sub4Sa1TotalPer = 'A2';
			}
			elseif($sub4Sa1TotalPer >= 71 && $sub4Sa1TotalPer <= 80)
			{
				$sub4Sa1TotalPer = 'B1';
			}
			elseif($sub4Sa1TotalPer >= 61 && $sub4Sa1TotalPer <= 70)
			{
				$sub4Sa1TotalPer = 'B2';
			}
			elseif($sub4Sa1TotalPer >= 51 && $sub4Sa1TotalPer <= 60)
			{
				$sub4Sa1TotalPer = 'C1';
			}
			elseif($sub4Sa1TotalPer >= 41 && $sub4Sa1TotalPer <= 50)
			{
				$sub4Sa1TotalPer = 'C2';
			}
			elseif($sub4Sa1TotalPer >= 33 && $sub4Sa1TotalPer <= 40)
			{
				$sub4Sa1TotalPer = 'D';
			}
			elseif($sub4Sa1TotalPer >= 21 && $sub4Sa1TotalPer <= 32)
			{
				$sub4Sa1TotalPer = 'E1';
			}
			elseif($sub4Sa1TotalPer <= 20)
			{
				$sub4Sa1TotalPer = 'E2';
			}

	    
	  	$sub4Fa3         = $stdRow['sub4Fa3'];
	  	$sub4Fa3Per = number_format(($sub4Fa3 * 100/10),0,'.',' ');
	  	if($sub4Fa3Per >= 91 && $sub4Fa3Per <= 100)
			{
			  $sub4Fa3Per = 'A1';
			}
			elseif($sub4Fa3Per >= 81 && $sub4Fa3Per <= 90)
			{
				$sub4Fa3Per = 'A2';
			}
			elseif($sub4Fa3Per >= 71 && $sub4Fa3Per <= 80)
			{
				$sub4Fa3Per = 'B1';
			}
			elseif($sub4Fa3Per >= 61 && $sub4Fa3Per <= 70)
			{
				$sub4Fa3Per = 'B2';
			}
			elseif($sub4Fa3Per >= 51 && $sub4Fa3Per <= 60)
			{
				$sub4Fa3Per = 'C1';
			}
			elseif($sub4Fa3Per >= 41 && $sub4Fa3Per <= 50)
			{
				$sub4Fa3Per = 'C2';
			}
			elseif($sub4Fa3Per >= 33 && $sub4Fa3Per <= 40)
			{
				$sub4Fa3Per = 'D';
			}
			elseif($sub4Fa3Per >= 21 && $sub4Fa3Per <= 32)
			{
				$sub4Fa3Per = 'E1';
			}
			elseif($sub4Fa3Per <= 20)
			{
				$sub4Fa3Per = 'E2';
			}
			
	  	$sub4Fa4         = $stdRow['sub4Fa4'];
	  	$sub4Fa4Per = number_format(($sub4Fa4 * 100/10),0,'.',' ');
	  	if($sub4Fa4Per >= 91 && $sub4Fa4Per <= 100)
			{
			  $sub4Fa4Per = 'A1';
			}
			elseif($sub4Fa4Per >= 81 && $sub4Fa4Per <= 90)
			{
				$sub4Fa4Per = 'A2';
			}
			elseif($sub4Fa4Per >= 71 && $sub4Fa4Per <= 80)
			{
				$sub4Fa4Per = 'B1';
			}
			elseif($sub4Fa4Per >= 61 && $sub4Fa4Per <= 70)
			{
				$sub4Fa4Per = 'B2';
			}
			elseif($sub4Fa4Per >= 51 && $sub4Fa4Per <= 60)
			{
				$sub4Fa4Per = 'C1';
			}
			elseif($sub4Fa4Per >= 41 && $sub4Fa4Per <= 50)
			{
				$sub4Fa4Per = 'C2';
			}
			elseif($sub4Fa4Per >= 33 && $sub4Fa4Per <= 40)
			{
				$sub4Fa4Per = 'D';
			}
			elseif($sub4Fa4Per >= 21 && $sub4Fa4Per <= 32)
			{
				$sub4Fa4Per = 'E1';
			}
			elseif($sub4Fa4Per <= 20)
			{
				$sub4Fa4Per = 'E2';
			}
	  	
	  	$sub4Sa2         = $stdRow['sub4Sa2'];
	  	$sub4Sa2Per = number_format(($sub4Sa2 * 100/30),0,'.',' ');
	  	if($sub4Sa2Per >= 91 && $sub4Sa2Per <= 100)
			{
			  $sub4Sa2Per = 'A1';
			}
			elseif($sub4Sa2Per >= 81 && $sub4Sa2Per <= 90)
			{
				$sub4Sa2Per = 'A2';
			}
			elseif($sub4Sa2Per >= 71 && $sub4Sa2Per <= 80)
			{
				$sub4Sa2Per = 'B1';
			}
			elseif($sub4Sa2Per >= 61 && $sub4Sa2Per <= 70)
			{
				$sub4Sa2Per = 'B2';
			}
			elseif($sub4Sa2Per >= 51 && $sub4Sa2Per <= 60)
			{
				$sub4Sa2Per = 'C1';
			}
			elseif($sub4Sa2Per >= 41 && $sub4Sa2Per <= 50)
			{
				$sub4Sa2Per = 'C2';
			}
			elseif($sub4Sa2Per >= 33 && $sub4Sa2Per <= 40)
			{
				$sub4Sa2Per = 'D';
			}
			elseif($sub4Sa2Per >= 21 && $sub4Sa2Per <= 32)
			{
				$sub4Sa2Per = 'E1';
			}
			elseif($sub4Sa2Per <= 20)
			{
				$sub4Sa2Per = 'E2';
			}
	  	
	  	$sub4Sa2Total = $sub4Fa3 + $sub4Fa4 + $sub4Sa2;
	  	$sub4Sa2TotalPer = number_format(($sub4Sa2Total * 100/50),0,'.',' ');
	  	if($sub4Sa2TotalPer >= 91 && $sub4Sa2TotalPer <= 100)
			{
			  $sub4Sa2TotalPer = 'A1';
			}
			elseif($sub4Sa2TotalPer >= 81 && $sub4Sa2TotalPer <= 90)
			{
				$sub4Sa2TotalPer = 'A2';
			}
			elseif($sub4Sa2TotalPer >= 71 && $sub4Sa2TotalPer <= 80)
			{
				$sub4Sa2TotalPer = 'B1';
			}
			elseif($sub4Sa2TotalPer >= 61 && $sub4Sa2TotalPer <= 70)
			{
				$sub4Sa2TotalPer = 'B2';
			}
			elseif($sub4Sa2TotalPer >= 51 && $sub4Sa2TotalPer <= 60)
			{
				$sub4Sa2TotalPer = 'C1';
			}
			elseif($sub4Sa2TotalPer >= 41 && $sub4Sa2TotalPer <= 50)
			{
				$sub4Sa2TotalPer = 'C2';
			}
			elseif($sub4Sa2TotalPer >= 33 && $sub4Sa2TotalPer <= 40)
			{
				$sub4Sa2TotalPer = 'D';
			}
			elseif($sub4Sa2TotalPer >= 21 && $sub4Sa2TotalPer <= 32)
			{
				$sub4Sa2TotalPer = 'E1';
			}
			elseif($sub4Sa2TotalPer <= 20)
			{
				$sub4Sa2TotalPer = 'E2';
			}
			
	  	$sub4faTotal = $sub4Fa1 + $sub4Fa2 + $sub4Fa3 + $sub4Fa4;
	  	$sub4faTotalPer = number_format(($sub4faTotal * 100/40),0,'.',' ');
	  	if($sub4faTotalPer >= 91 && $sub4faTotalPer <= 100)
			{
			  $sub4faTotalPer = 'A1';
			}
			elseif($sub4faTotalPer >= 81 && $sub4faTotalPer <= 90)
			{
				$sub4faTotalPer = 'A2';
			}
			elseif($sub4faTotalPer >= 71 && $sub4faTotalPer <= 80)
			{
				$sub4faTotalPer = 'B1';
			}
			elseif($sub4faTotalPer >= 61 && $sub4faTotalPer <= 70)
			{
				$sub4faTotalPer = 'B2';
			}
			elseif($sub4faTotalPer >= 51 && $sub4faTotalPer <= 60)
			{
				$sub4faTotalPer = 'C1';
			}
			elseif($sub4faTotalPer >= 41 && $sub4faTotalPer <= 50)
			{
				$sub4faTotalPer = 'C2';
			}
			elseif($sub4faTotalPer >= 33 && $sub4faTotalPer <= 40)
			{
				$sub4faTotalPer = 'D';
			}
			elseif($sub4faTotalPer >= 21 && $sub4faTotalPer <= 32)
			{
				$sub4faTotalPer = 'E1';
			}
			elseif($sub4faTotalPer <= 20)
			{
				$sub4faTotalPer = 'E2';
			}
	  	
	  	$sub4saTotal = $sub4Sa1 + $sub4Sa2;
	  	$sub4saTotalPer = number_format(($sub4saTotal * 100/60),0,'.',' ');
	  	if($sub4saTotalPer >= 91 && $sub4saTotalPer <= 100)
			{
			  $sub4saTotalPer = 'A1';
			}
			elseif($sub4saTotalPer >= 81 && $sub4saTotalPer <= 90)
			{
				$sub4saTotalPer = 'A2';
			}
			elseif($sub4saTotalPer >= 71 && $sub4saTotalPer <= 80)
			{
				$sub4saTotalPer = 'B1';
			}
			elseif($sub4saTotalPer >= 61 && $sub4saTotalPer <= 70)
			{
				$sub4saTotalPer = 'B2';
			}
			elseif($sub4saTotalPer >= 51 && $sub4saTotalPer <= 60)
			{
				$sub4saTotalPer = 'C1';
			}
			elseif($sub4saTotalPer >= 41 && $sub4saTotalPer <= 50)
			{
				$sub4saTotalPer = 'C2';
			}
			elseif($sub4saTotalPer >= 33 && $sub4saTotalPer <= 40)
			{
				$sub4saTotalPer = 'D';
			}
			elseif($sub4saTotalPer >= 21 && $sub4saTotalPer <= 32)
			{
				$sub4saTotalPer = 'E1';
			}
			elseif($sub4saTotalPer <= 20)
			{
				$sub4saTotalPer = 'E2';
			}
			
			$sub4fasaTotalPer = $sub4faTotal + $sub4saTotal;
			$sub4fasaTotal = number_format((($sub4fasaTotalPer) * 100/100),0,'.',' ');
	  	if($sub4fasaTotal >= 91 && $sub4fasaTotal <= 100)
			{
			  $sub4fasaTotal = 'A1';
			}
			elseif($sub4fasaTotal >= 81 && $sub4fasaTotal <= 90)
			{
				$sub4fasaTotal = 'A2';
			}
			elseif($sub4fasaTotal >= 71 && $sub4fasaTotal <= 80)
			{
				$sub4fasaTotal = 'B1';
			}
			elseif($sub4fasaTotal >= 61 && $sub4fasaTotal <= 70)
			{
				$sub4fasaTotal = 'B2';
			}
			elseif($sub4fasaTotal >= 51 && $sub4fasaTotal <= 60)
			{
				$sub4fasaTotal = 'C1';
			}
			elseif($sub4fasaTotal >= 41 && $sub4fasaTotal <= 50)
			{
				$sub4fasaTotal = 'C2';
			}
			elseif($sub4fasaTotal >= 33 && $sub4fasaTotal <= 40)
			{
				$sub4fasaTotal = 'D';
			}
			elseif($sub4fasaTotal >= 21 && $sub4fasaTotal <= 32)
			{
				$sub4fasaTotal = 'E1';
			}
			elseif($sub4fasaTotal <= 20)
			{
				$sub4fasaTotal = 'E2';
			}
	  	
	  	
	  	$pdf->SetFillColor(232, 232, 232);
	    $pdf->SetXY(8,136);
	    $pdf->SetFont('Arial', '', 10);
	    $pdf->Cell(40, 7, 'Science', 1,0,'L');
		  $pdf->Cell(40, 7, $sub4Fa1Per, 1,0,'C');
		  $pdf->Cell(40, 7, $sub4Fa2Per, 1,0,'C');
		  $pdf->Cell(40, 7, $sub4Sa1Per, 1,0,'C');
		  $pdf->Cell(40, 7, $sub4Sa1TotalPer, 1,0,'C');
		  
		  ///////// subject 5
		  $sub5Fa1         = $stdRow['sub5Fa1'];
	  	$sub5Fa1Per      = number_format(($sub5Fa1 * 100/10),0,'.',' ');
      if($sub5Fa1Per >= 91 && $sub5Fa1Per <= 100)
			{
			  $sub5Fa1Per = 'A1';
			}
			elseif($sub5Fa1Per >= 81 && $sub5Fa1Per <= 90)
			{
				$sub5Fa1Per = 'A2';
			}
			elseif($sub5Fa1Per >= 71 && $sub5Fa1Per <= 80)
			{
				$sub5Fa1Per = 'B1';
			}
			elseif($sub5Fa1Per >= 61 && $sub5Fa1Per <= 70)
			{
				$sub5Fa1Per = 'B2';
			}
			elseif($sub5Fa1Per >= 51 && $sub5Fa1Per <= 60)
			{
				$sub5Fa1Per = 'C1';
			}
			elseif($sub5Fa1Per >= 41 && $sub5Fa1Per <= 50)
			{
				$sub5Fa1Per = 'C2';
			}
			elseif($sub5Fa1Per >= 33 && $sub5Fa1Per <= 40)
			{
				$sub5Fa1Per = 'D';
			}
			elseif($sub5Fa1Per >= 21 && $sub5Fa1Per <= 32)
			{
				$sub5Fa1Per = 'E1';
			}
			elseif($sub5Fa1Per <= 20)
			{
				$sub5Fa1Per = 'E2';
			}
			
	  	$sub5Fa2         = $stdRow['sub5Fa2'];
	  	$sub5Fa2Per = number_format(($sub5Fa2 * 100/10),0,'.',' ');
	  	if($sub5Fa2Per >= 91 && $sub5Fa2Per <= 100)
			{
			  $sub5Fa2Per = 'A1';
			}
			elseif($sub5Fa2Per >= 81 && $sub5Fa2Per <= 90)
			{
				$sub5Fa2Per = 'A2';
			}
			elseif($sub5Fa2Per >= 71 && $sub5Fa2Per <= 80)
			{
				$sub5Fa2Per = 'B1';
			}
			elseif($sub5Fa2Per >= 61 && $sub5Fa2Per <= 70)
			{
				$sub5Fa2Per = 'B2';
			}
			elseif($sub5Fa2Per >= 51 && $sub5Fa2Per <= 60)
			{
				$sub5Fa2Per = 'C1';
			}
			elseif($sub5Fa2Per >= 41 && $sub5Fa2Per <= 50)
			{
				$sub5Fa2Per = 'C2';
			}
			elseif($sub5Fa2Per >= 33 && $sub5Fa2Per <= 40)
			{
				$sub5Fa2Per = 'D';
			}
			elseif($sub5Fa2Per >= 21 && $sub5Fa2Per <= 32)
			{
				$sub5Fa2Per = 'E1';
			}
			elseif($sub5Fa2Per <= 20)
			{
				$sub5Fa2Per = 'E2';
			}
	  	
	  	$sub5Sa1         = $stdRow['sub5Sa1'];
	  	$sub5Sa1Per = number_format(($sub5Sa1 * 100/30),0,'.',' ');
	  	if($sub5Sa1Per >= 91 && $sub5Sa1Per <= 100)
			{
			  $sub5Sa1Per = 'A1';
			}
			elseif($sub5Sa1Per >= 81 && $sub5Sa1Per <= 90)
			{
				$sub5Sa1Per = 'A2';
			}
			elseif($sub5Sa1Per >= 71 && $sub5Sa1Per <= 80)
			{
				$sub5Sa1Per = 'B1';
			}
			elseif($sub5Sa1Per >= 61 && $sub5Sa1Per <= 70)
			{
				$sub5Sa1Per = 'B2';
			}
			elseif($sub5Sa1Per >= 51 && $sub5Sa1Per <= 60)
			{
				$sub5Sa1Per = 'C1';
			}
			elseif($sub5Sa1Per >= 41 && $sub5Sa1Per <= 50)
			{
				$sub5Sa1Per = 'C2';
			}
			elseif($sub5Sa1Per >= 33 && $sub5Sa1Per <= 40)
			{
				$sub5Sa1Per = 'D';
			}
			elseif($sub5Sa1Per >= 21 && $sub5Sa1Per <= 32)
			{
				$sub5Sa1Per = 'E1';
			}
			elseif($sub5Sa1Per <= 20)
			{
				$sub5Sa1Per = 'E2';
			}
	    
	    $sub5Sa1Total = $sub5Fa1 + $sub5Fa2 + $sub5Sa1;
	    $sub5Sa1TotalPer = number_format(($sub5Sa1Total * 100/50),0,'.',' ');
	    if($sub5Sa1TotalPer >= 91 && $sub5Sa1TotalPer <= 100)
			{
			  $sub5Sa1TotalPer = 'A1';
			}
			elseif($sub5Sa1TotalPer >= 81 && $sub5Sa1TotalPer <= 90)
			{
				$sub5Sa1TotalPer = 'A2';
			}
			elseif($sub5Sa1TotalPer >= 71 && $sub5Sa1TotalPer <= 80)
			{
				$sub5Sa1TotalPer = 'B1';
			}
			elseif($sub5Sa1TotalPer >= 61 && $sub5Sa1TotalPer <= 70)
			{
				$sub5Sa1TotalPer = 'B2';
			}
			elseif($sub5Sa1TotalPer >= 51 && $sub5Sa1TotalPer <= 60)
			{
				$sub5Sa1TotalPer = 'C1';
			}
			elseif($sub5Sa1TotalPer >= 41 && $sub5Sa1TotalPer <= 50)
			{
				$sub5Sa1TotalPer = 'C2';
			}
			elseif($sub5Sa1TotalPer >= 33 && $sub5Sa1TotalPer <= 40)
			{
				$sub5Sa1TotalPer = 'D';
			}
			elseif($sub5Sa1TotalPer >= 21 && $sub5Sa1TotalPer <= 32)
			{
				$sub5Sa1TotalPer = 'E1';
			}
			elseif($sub5Sa1TotalPer <= 20)
			{
				$sub5Sa1TotalPer = 'E2';
			}

	    
	  	$sub5Fa3         = $stdRow['sub5Fa3'];
	  	$sub5Fa3Per = number_format(($sub5Fa3 * 100/10),0,'.',' ');
	  	if($sub5Fa3Per >= 91 && $sub5Fa3Per <= 100)
			{
			  $sub5Fa3Per = 'A1';
			}
			elseif($sub5Fa3Per >= 81 && $sub5Fa3Per <= 90)
			{
				$sub5Fa3Per = 'A2';
			}
			elseif($sub5Fa3Per >= 71 && $sub5Fa3Per <= 80)
			{
				$sub5Fa3Per = 'B1';
			}
			elseif($sub5Fa3Per >= 61 && $sub5Fa3Per <= 70)
			{
				$sub5Fa3Per = 'B2';
			}
			elseif($sub5Fa3Per >= 51 && $sub5Fa3Per <= 60)
			{
				$sub5Fa3Per = 'C1';
			}
			elseif($sub5Fa3Per >= 41 && $sub5Fa3Per <= 50)
			{
				$sub5Fa3Per = 'C2';
			}
			elseif($sub5Fa3Per >= 33 && $sub5Fa3Per <= 40)
			{
				$sub5Fa3Per = 'D';
			}
			elseif($sub5Fa3Per >= 21 && $sub5Fa3Per <= 32)
			{
				$sub5Fa3Per = 'E1';
			}
			elseif($sub5Fa3Per <= 20)
			{
				$sub5Fa3Per = 'E2';
			}
			
	  	$sub5Fa4         = $stdRow['sub5Fa4'];
	  	$sub5Fa4Per = number_format(($sub5Fa4 * 100/10),0,'.',' ');
	  	if($sub5Fa4Per >= 91 && $sub5Fa4Per <= 100)
			{
			  $sub5Fa4Per = 'A1';
			}
			elseif($sub5Fa4Per >= 81 && $sub5Fa4Per <= 90)
			{
				$sub5Fa4Per = 'A2';
			}
			elseif($sub5Fa4Per >= 71 && $sub5Fa4Per <= 80)
			{
				$sub5Fa4Per = 'B1';
			}
			elseif($sub5Fa4Per >= 61 && $sub5Fa4Per <= 70)
			{
				$sub5Fa4Per = 'B2';
			}
			elseif($sub5Fa4Per >= 51 && $sub5Fa4Per <= 60)
			{
				$sub5Fa4Per = 'C1';
			}
			elseif($sub5Fa4Per >= 41 && $sub5Fa4Per <= 50)
			{
				$sub5Fa4Per = 'C2';
			}
			elseif($sub5Fa4Per >= 33 && $sub5Fa4Per <= 40)
			{
				$sub5Fa4Per = 'D';
			}
			elseif($sub5Fa4Per >= 21 && $sub5Fa4Per <= 32)
			{
				$sub5Fa4Per = 'E1';
			}
			elseif($sub5Fa4Per <= 20)
			{
				$sub5Fa4Per = 'E2';
			}
	  	
	  	$sub5Sa2         = $stdRow['sub5Sa2'];
	  	$sub5Sa2Per = number_format(($sub5Sa2 * 100/30),0,'.',' ');
	  	if($sub5Sa2Per >= 91 && $sub5Sa2Per <= 100)
			{
			  $sub5Sa2Per = 'A1';
			}
			elseif($sub5Sa2Per >= 81 && $sub5Sa2Per <= 90)
			{
				$sub5Sa2Per = 'A2';
			}
			elseif($sub5Sa2Per >= 71 && $sub5Sa2Per <= 80)
			{
				$sub5Sa2Per = 'B1';
			}
			elseif($sub5Sa2Per >= 61 && $sub5Sa2Per <= 70)
			{
				$sub5Sa2Per = 'B2';
			}
			elseif($sub5Sa2Per >= 51 && $sub5Sa2Per <= 60)
			{
				$sub5Sa2Per = 'C1';
			}
			elseif($sub5Sa2Per >= 41 && $sub5Sa2Per <= 50)
			{
				$sub5Sa2Per = 'C2';
			}
			elseif($sub5Sa2Per >= 33 && $sub5Sa2Per <= 40)
			{
				$sub5Sa2Per = 'D';
			}
			elseif($sub5Sa2Per >= 21 && $sub5Sa2Per <= 32)
			{
				$sub5Sa2Per = 'E1';
			}
			elseif($sub5Sa2Per <= 20)
			{
				$sub5Sa2Per = 'E2';
			}
	  	
	  	$sub5Sa2Total = $sub5Fa3 + $sub5Fa4 + $sub5Sa2;
	  	$sub5Sa2TotalPer = number_format(($sub5Sa2Total * 100/50),0,'.',' ');
	  	if($sub5Sa2TotalPer >= 91 && $sub5Sa2TotalPer <= 100)
			{
			  $sub5Sa2TotalPer = 'A1';
			}
			elseif($sub5Sa2TotalPer >= 81 && $sub5Sa2TotalPer <= 90)
			{
				$sub5Sa2TotalPer = 'A2';
			}
			elseif($sub5Sa2TotalPer >= 71 && $sub5Sa2TotalPer <= 80)
			{
				$sub5Sa2TotalPer = 'B1';
			}
			elseif($sub5Sa2TotalPer >= 61 && $sub5Sa2TotalPer <= 70)
			{
				$sub5Sa2TotalPer = 'B2';
			}
			elseif($sub5Sa2TotalPer >= 51 && $sub5Sa2TotalPer <= 60)
			{
				$sub5Sa2TotalPer = 'C1';
			}
			elseif($sub5Sa2TotalPer >= 41 && $sub5Sa2TotalPer <= 50)
			{
				$sub5Sa2TotalPer = 'C2';
			}
			elseif($sub5Sa2TotalPer >= 33 && $sub5Sa2TotalPer <= 40)
			{
				$sub5Sa2TotalPer = 'D';
			}
			elseif($sub5Sa2TotalPer >= 21 && $sub5Sa2TotalPer <= 32)
			{
				$sub5Sa2TotalPer = 'E1';
			}
			elseif($sub5Sa2TotalPer <= 20)
			{
				$sub5Sa2TotalPer = 'E2';
			}
			
	  	$sub5faTotal = $sub5Fa1 + $sub5Fa2 + $sub5Fa3 + $sub5Fa4;
	  	$sub5faTotalPer = number_format(($sub5faTotal * 100/40),0,'.',' ');
	  	if($sub5faTotalPer >= 91 && $sub5faTotalPer <= 100)
			{
			  $sub5faTotalPer = 'A1';
			}
			elseif($sub5faTotalPer >= 81 && $sub5faTotalPer <= 90)
			{
				$sub5faTotalPer = 'A2';
			}
			elseif($sub5faTotalPer >= 71 && $sub5faTotalPer <= 80)
			{
				$sub5faTotalPer = 'B1';
			}
			elseif($sub5faTotalPer >= 61 && $sub5faTotalPer <= 70)
			{
				$sub5faTotalPer = 'B2';
			}
			elseif($sub5faTotalPer >= 51 && $sub5faTotalPer <= 60)
			{
				$sub5faTotalPer = 'C1';
			}
			elseif($sub5faTotalPer >= 41 && $sub5faTotalPer <= 50)
			{
				$sub5faTotalPer = 'C2';
			}
			elseif($sub5faTotalPer >= 33 && $sub5faTotalPer <= 40)
			{
				$sub5faTotalPer = 'D';
			}
			elseif($sub5faTotalPer >= 21 && $sub5faTotalPer <= 32)
			{
				$sub5faTotalPer = 'E1';
			}
			elseif($sub5faTotalPer <= 20)
			{
				$sub5faTotalPer = 'E2';
			}
	  	
	  	$sub5saTotal = $sub5Sa1 + $sub5Sa2;
	  	$sub5saTotalPer = number_format(($sub5saTotal * 100/60),0,'.',' ');
	  	if($sub5saTotalPer >= 91 && $sub5saTotalPer <= 100)
			{
			  $sub5saTotalPer = 'A1';
			}
			elseif($sub5saTotalPer >= 81 && $sub5saTotalPer <= 90)
			{
				$sub5saTotalPer = 'A2';
			}
			elseif($sub5saTotalPer >= 71 && $sub5saTotalPer <= 80)
			{
				$sub5saTotalPer = 'B1';
			}
			elseif($sub5saTotalPer >= 61 && $sub5saTotalPer <= 70)
			{
				$sub5saTotalPer = 'B2';
			}
			elseif($sub5saTotalPer >= 51 && $sub5saTotalPer <= 60)
			{
				$sub5saTotalPer = 'C1';
			}
			elseif($sub5saTotalPer >= 41 && $sub5saTotalPer <= 50)
			{
				$sub5saTotalPer = 'C2';
			}
			elseif($sub5saTotalPer >= 33 && $sub5saTotalPer <= 40)
			{
				$sub5saTotalPer = 'D';
			}
			elseif($sub5saTotalPer >= 21 && $sub5saTotalPer <= 32)
			{
				$sub5saTotalPer = 'E1';
			}
			elseif($sub5saTotalPer <= 20)
			{
				$sub5saTotalPer = 'E2';
			}
			
			$sub5fasaTotalPer = $sub5faTotal + $sub5saTotal;
			$sub5fasaTotal = number_format((($sub5fasaTotalPer) * 100/100),0,'.',' ');
	  	if($sub5fasaTotal >= 91 && $sub5fasaTotal <= 100)
			{
			  $sub5fasaTotal = 'A1';
			}
			elseif($sub5fasaTotal >= 81 && $sub5fasaTotal <= 90)
			{
				$sub5fasaTotal = 'A2';
			}
			elseif($sub5fasaTotal >= 71 && $sub5fasaTotal <= 80)
			{
				$sub5fasaTotal = 'B1';
			}
			elseif($sub5fasaTotal >= 61 && $sub5fasaTotal <= 70)
			{
				$sub5fasaTotal = 'B2';
			}
			elseif($sub5fasaTotal >= 51 && $sub5fasaTotal <= 60)
			{
				$sub5fasaTotal = 'C1';
			}
			elseif($sub5fasaTotal >= 41 && $sub5fasaTotal <= 50)
			{
				$sub5fasaTotal = 'C2';
			}
			elseif($sub5fasaTotal >= 33 && $sub5fasaTotal <= 40)
			{
				$sub5fasaTotal = 'D';
			}
			elseif($sub5fasaTotal >= 21 && $sub5fasaTotal <= 32)
			{
				$sub5fasaTotal = 'E1';
			}
			elseif($sub5fasaTotal <= 20)
			{
				$sub5fasaTotal = 'E2';
			}
	  	
	  	
	  	$pdf->SetFillColor(232, 232, 232);
	    $pdf->SetXY(8,143);
	    $pdf->SetFont('Arial', '', 10);
	    $pdf->Cell(40, 7, 'Social Science', 1,0,'L');
		  $pdf->Cell(40, 7, $sub5Fa1Per, 1,0,'C');
		  $pdf->Cell(40, 7, $sub5Fa2Per, 1,0,'C');
		  $pdf->Cell(40, 7, $sub5Sa1Per, 1,0,'C');
		  $pdf->Cell(40, 7, $sub5Sa1TotalPer, 1,0,'C');
		  
		  ///////// subject 6
		  $sub6Fa1         = $stdRow['sub6Fa1'];
	  	$sub6Fa1Per      = number_format(($sub6Fa1 * 100/10),0,'.',' ');
      if($sub6Fa1Per >= 91 && $sub6Fa1Per <= 100)
			{
			  $sub6Fa1Per = 'A1';
			}
			elseif($sub6Fa1Per >= 81 && $sub6Fa1Per <= 90)
			{
				$sub6Fa1Per = 'A2';
			}
			elseif($sub6Fa1Per >= 71 && $sub6Fa1Per <= 80)
			{
				$sub6Fa1Per = 'B1';
			}
			elseif($sub6Fa1Per >= 61 && $sub6Fa1Per <= 70)
			{
				$sub6Fa1Per = 'B2';
			}
			elseif($sub6Fa1Per >= 51 && $sub6Fa1Per <= 60)
			{
				$sub6Fa1Per = 'C1';
			}
			elseif($sub6Fa1Per >= 41 && $sub6Fa1Per <= 50)
			{
				$sub6Fa1Per = 'C2';
			}
			elseif($sub6Fa1Per >= 33 && $sub6Fa1Per <= 40)
			{
				$sub6Fa1Per = 'D';
			}
			elseif($sub6Fa1Per >= 21 && $sub6Fa1Per <= 32)
			{
				$sub6Fa1Per = 'E1';
			}
			elseif($sub6Fa1Per <= 20)
			{
				$sub6Fa1Per = 'E2';
			}
	  	$sub6Fa2         = $stdRow['sub6Fa2'];
	  	$sub6Fa2Per = number_format(($sub6Fa2 * 100/10),0,'.',' ');
	  	if($sub6Fa2Per >= 91 && $sub6Fa2Per <= 100)
			{
			  $sub6Fa2Per = 'A1';
			}
			elseif($sub6Fa2Per >= 81 && $sub6Fa2Per <= 90)
			{
				$sub6Fa2Per = 'A2';
			}
			elseif($sub6Fa2Per >= 71 && $sub6Fa2Per <= 80)
			{
				$sub6Fa2Per = 'B1';
			}
			elseif($sub6Fa2Per >= 61 && $sub6Fa2Per <= 70)
			{
				$sub6Fa2Per = 'B2';
			}
			elseif($sub6Fa2Per >= 51 && $sub6Fa2Per <= 60)
			{
				$sub6Fa2Per = 'C1';
			}
			elseif($sub6Fa2Per >= 41 && $sub6Fa2Per <= 50)
			{
				$sub6Fa2Per = 'C2';
			}
			elseif($sub6Fa2Per >= 33 && $sub6Fa2Per <= 40)
			{
				$sub6Fa2Per = 'D';
			}
			elseif($sub6Fa2Per >= 21 && $sub6Fa2Per <= 32)
			{
				$sub6Fa2Per = 'E1';
			}
			elseif($sub6Fa2Per <= 20)
			{
				$sub6Fa2Per = 'E2';
			}
	  	
		//echo "<pre>"; print_r($stdRow); exit;
	  	$sub6Sa1         = $stdRow['sub6Sa1'];
	  	$sub6Sa1Per = number_format(($sub6Sa1 * 100/30),0,'.',' ');
	  	if($sub6Sa1Per >= 91 && $sub6Sa1Per <= 100)
			{
			  $sub6Sa1Per = 'A1';
			}
			elseif($sub6Sa1Per >= 81 && $sub6Sa1Per <= 90)
			{
				$sub6Sa1Per = 'A2';
			}
			elseif($sub6Sa1Per >= 71 && $sub6Sa1Per <= 80)
			{
				$sub6Sa1Per = 'B1';
			}
			elseif($sub6Sa1Per >= 61 && $sub6Sa1Per <= 70)
			{
				$sub6Sa1Per = 'B2';
			}
			elseif($sub6Sa1Per >= 51 && $sub6Sa1Per <= 60)
			{
				$sub6Sa1Per = 'C1';
			}
			elseif($sub6Sa1Per >= 41 && $sub6Sa1Per <= 50)
			{
				$sub6Sa1Per = 'C2';
			}
			elseif($sub6Sa1Per >= 33 && $sub6Sa1Per <= 40)
			{
				$sub6Sa1Per = 'D';
			}
			elseif($sub6Sa1Per >= 21 && $sub6Sa1Per <= 32)
			{
				$sub6Sa1Per = 'E1';
			}
			elseif($sub6Sa1Per <= 20)
			{
				$sub6Sa1Per = 'E2';
			}
	    $sub6Sa1Total = $sub6Fa1 + $sub6Fa2 + $sub6Sa1;
	    $sub6Sa1TotalPer = number_format(($sub6Sa1Total * 100/50),0,'.',' ');
	    if($sub6Sa1TotalPer >= 91 && $sub6Sa1TotalPer <= 100)
			{
			  $sub6Sa1TotalPer = 'A1';
			}
			elseif($sub6Sa1TotalPer >= 81 && $sub6Sa1TotalPer <= 90)
			{
				$sub6Sa1TotalPer = 'A2';
			}
			elseif($sub6Sa1TotalPer >= 71 && $sub6Sa1TotalPer <= 80)
			{
				$sub6Sa1TotalPer = 'B1';
			}
			elseif($sub6Sa1TotalPer >= 61 && $sub6Sa1TotalPer <= 70)
			{
				$sub6Sa1TotalPer = 'B2';
			}
			elseif($sub6Sa1TotalPer >= 51 && $sub6Sa1TotalPer <= 60)
			{
				$sub6Sa1TotalPer = 'C1';
			}
			elseif($sub6Sa1TotalPer >= 41 && $sub6Sa1TotalPer <= 50)
			{
				$sub6Sa1TotalPer = 'C2';
			}
			elseif($sub6Sa1TotalPer >= 33 && $sub6Sa1TotalPer <= 40)
			{
				$sub6Sa1TotalPer = 'D';
			}
			elseif($sub6Sa1TotalPer >= 21 && $sub6Sa1TotalPer <= 32)
			{
				$sub6Sa1TotalPer = 'E1';
			}
			elseif($sub6Sa1TotalPer <= 20)
			{
				$sub6Sa1TotalPer = 'E2';
			}

	    
	  	$sub6Fa3         = $stdRow['sub6Fa3'];
	  	$sub6Fa3Per = number_format(($sub6Fa3 * 100/10),0,'.',' ');
	  	if($sub6Fa3Per >= 91 && $sub6Fa3Per <= 100)
			{
			  $sub6Fa3Per = 'A1';
			}
			elseif($sub6Fa3Per >= 81 && $sub6Fa3Per <= 90)
			{
				$sub6Fa3Per = 'A2';
			}
			elseif($sub6Fa3Per >= 71 && $sub6Fa3Per <= 80)
			{
				$sub6Fa3Per = 'B1';
			}
			elseif($sub6Fa3Per >= 61 && $sub6Fa3Per <= 70)
			{
				$sub6Fa3Per = 'B2';
			}
			elseif($sub6Fa3Per >= 51 && $sub6Fa3Per <= 60)
			{
				$sub6Fa3Per = 'C1';
			}
			elseif($sub6Fa3Per >= 41 && $sub6Fa3Per <= 50)
			{
				$sub6Fa3Per = 'C2';
			}
			elseif($sub6Fa3Per >= 33 && $sub6Fa3Per <= 40)
			{
				$sub6Fa3Per = 'D';
			}
			elseif($sub6Fa3Per >= 21 && $sub6Fa3Per <= 32)
			{
				$sub6Fa3Per = 'E1';
			}
			elseif($sub6Fa3Per <= 20)
			{
				$sub6Fa3Per = 'E2';
			}
			
	  	$sub6Fa4         = $stdRow['sub6Fa4'];
	  	$sub6Fa4Per = number_format(($sub6Fa4 * 100/10),0,'.',' ');
	  	if($sub6Fa4Per >= 91 && $sub6Fa4Per <= 100)
			{
			  $sub6Fa4Per = 'A1';
			}
			elseif($sub6Fa4Per >= 81 && $sub6Fa4Per <= 90)
			{
				$sub6Fa4Per = 'A2';
			}
			elseif($sub6Fa4Per >= 71 && $sub6Fa4Per <= 80)
			{
				$sub6Fa4Per = 'B1';
			}
			elseif($sub6Fa4Per >= 61 && $sub6Fa4Per <= 70)
			{
				$sub6Fa4Per = 'B2';
			}
			elseif($sub6Fa4Per >= 51 && $sub6Fa4Per <= 60)
			{
				$sub6Fa4Per = 'C1';
			}
			elseif($sub6Fa4Per >= 41 && $sub6Fa4Per <= 50)
			{
				$sub6Fa4Per = 'C2';
			}
			elseif($sub6Fa4Per >= 33 && $sub6Fa4Per <= 40)
			{
				$sub6Fa4Per = 'D';
			}
			elseif($sub6Fa4Per >= 21 && $sub6Fa4Per <= 32)
			{
				$sub6Fa4Per = 'E1';
			}
			elseif($sub6Fa4Per <= 20)
			{
				$sub6Fa4Per = 'E2';
			}
	  	
	  	$sub6Sa2         = $stdRow['sub6Sa2'];
	  	$sub6Sa2Per = number_format(($sub6Sa2 * 100/30),0,'.',' ');
	  	if($sub6Sa2Per >= 91 && $sub6Sa2Per <= 100)
			{
			  $sub6Sa2Per = 'A1';
			}
			elseif($sub6Sa2Per >= 81 && $sub6Sa2Per <= 90)
			{
				$sub6Sa2Per = 'A2';
			}
			elseif($sub6Sa2Per >= 71 && $sub6Sa2Per <= 80)
			{
				$sub6Sa2Per = 'B1';
			}
			elseif($sub6Sa2Per >= 61 && $sub6Sa2Per <= 70)
			{
				$sub6Sa2Per = 'B2';
			}
			elseif($sub6Sa2Per >= 51 && $sub6Sa2Per <= 60)
			{
				$sub6Sa2Per = 'C1';
			}
			elseif($sub6Sa2Per >= 41 && $sub6Sa2Per <= 50)
			{
				$sub6Sa2Per = 'C2';
			}
			elseif($sub6Sa2Per >= 33 && $sub6Sa2Per <= 40)
			{
				$sub6Sa2Per = 'D';
			}
			elseif($sub6Sa2Per >= 21 && $sub6Sa2Per <= 32)
			{
				$sub6Sa2Per = 'E1';
			}
			elseif($sub6Sa2Per <= 20)
			{
				$sub6Sa2Per = 'E2';
			}
	  	
	  	$sub6Sa2Total = $sub6Fa3 + $sub6Fa4 + $sub6Sa2;
	  	$sub6Sa2TotalPer = number_format(($sub6Sa2Total * 100/50),0,'.',' ');
	  	if($sub6Sa2TotalPer >= 91 && $sub6Sa2TotalPer <= 100)
			{
			  $sub6Sa2TotalPer = 'A1';
			}
			elseif($sub6Sa2TotalPer >= 81 && $sub6Sa2TotalPer <= 90)
			{
				$sub6Sa2TotalPer = 'A2';
			}
			elseif($sub6Sa2TotalPer >= 71 && $sub6Sa2TotalPer <= 80)
			{
				$sub6Sa2TotalPer = 'B1';
			}
			elseif($sub6Sa2TotalPer >= 61 && $sub6Sa2TotalPer <= 70)
			{
				$sub6Sa2TotalPer = 'B2';
			}
			elseif($sub6Sa2TotalPer >= 51 && $sub6Sa2TotalPer <= 60)
			{
				$sub6Sa2TotalPer = 'C1';
			}
			elseif($sub6Sa2TotalPer >= 41 && $sub6Sa2TotalPer <= 50)
			{
				$sub6Sa2TotalPer = 'C2';
			}
			elseif($sub6Sa2TotalPer >= 33 && $sub6Sa2TotalPer <= 40)
			{
				$sub6Sa2TotalPer = 'D';
			}
			elseif($sub6Sa2TotalPer >= 21 && $sub6Sa2TotalPer <= 32)
			{
				$sub6Sa2TotalPer = 'E1';
			}
			elseif($sub6Sa2TotalPer <= 20)
			{
				$sub6Sa2TotalPer = 'E2';
			}
			
	  	$sub6faTotal = $sub6Fa1 + $sub6Fa2 + $sub6Fa3 + $sub6Fa4;
	  	$sub6faTotalPer = number_format(($sub6faTotal * 100/40),0,'.',' ');
	  	if($sub6faTotalPer >= 91 && $sub6faTotalPer <= 100)
			{
			  $sub6faTotalPer = 'A1';
			}
			elseif($sub6faTotalPer >= 81 && $sub6faTotalPer <= 90)
			{
				$sub6faTotalPer = 'A2';
			}
			elseif($sub6faTotalPer >= 71 && $sub6faTotalPer <= 80)
			{
				$sub6faTotalPer = 'B1';
			}
			elseif($sub6faTotalPer >= 61 && $sub6faTotalPer <= 70)
			{
				$sub6faTotalPer = 'B2';
			}
			elseif($sub6faTotalPer >= 51 && $sub6faTotalPer <= 60)
			{
				$sub6faTotalPer = 'C1';
			}
			elseif($sub6faTotalPer >= 41 && $sub6faTotalPer <= 50)
			{
				$sub6faTotalPer = 'C2';
			}
			elseif($sub6faTotalPer >= 33 && $sub6faTotalPer <= 40)
			{
				$sub6faTotalPer = 'D';
			}
			elseif($sub6faTotalPer >= 21 && $sub6faTotalPer <= 32)
			{
				$sub6faTotalPer = 'E1';
			}
			elseif($sub6faTotalPer <= 20)
			{
				$sub6faTotalPer = 'E2';
			}
	  	
	  	$sub6saTotal = $sub6Sa1 + $sub6Sa2;
	  	$sub6saTotalPer = number_format(($sub6saTotal * 100/60),0,'.',' ');
	  	if($sub6saTotalPer >= 91 && $sub6saTotalPer <= 100)
			{
			  $sub6saTotalPer = 'A1';
			}
			elseif($sub6saTotalPer >= 81 && $sub6saTotalPer <= 90)
			{
				$sub6saTotalPer = 'A2';
			}
			elseif($sub6saTotalPer >= 71 && $sub6saTotalPer <= 80)
			{
				$sub6saTotalPer = 'B1';
			}
			elseif($sub6saTotalPer >= 61 && $sub6saTotalPer <= 70)
			{
				$sub6saTotalPer = 'B2';
			}
			elseif($sub6saTotalPer >= 51 && $sub6saTotalPer <= 60)
			{
				$sub6saTotalPer = 'C1';
			}
			elseif($sub6saTotalPer >= 41 && $sub6saTotalPer <= 50)
			{
				$sub6saTotalPer = 'C2';
			}
			elseif($sub6saTotalPer >= 33 && $sub6saTotalPer <= 40)
			{
				$sub6saTotalPer = 'D';
			}
			elseif($sub6saTotalPer >= 21 && $sub6saTotalPer <= 32)
			{
				$sub6saTotalPer = 'E1';
			}
			elseif($sub6saTotalPer <= 20)
			{
				$sub6saTotalPer = 'E2';
			}
			
			$sub6fasaTotalPer = $sub6faTotal + $sub6saTotal;
			$sub6fasaTotal = number_format((($sub6fasaTotalPer) * 100/100),0,'.',' ');
	  	if($sub6fasaTotal >= 91 && $sub6fasaTotal <= 100)
			{
			  $sub6fasaTotal = 'A1';
			}
			elseif($sub6fasaTotal >= 81 && $sub6fasaTotal <= 90)
			{
				$sub6fasaTotal = 'A2';
			}
			elseif($sub6fasaTotal >= 71 && $sub6fasaTotal <= 80)
			{
				$sub6fasaTotal = 'B1';
			}
			elseif($sub6fasaTotal >= 61 && $sub6fasaTotal <= 70)
			{
				$sub6fasaTotal = 'B2';
			}
			elseif($sub6fasaTotal >= 51 && $sub6fasaTotal <= 60)
			{
				$sub6fasaTotal = 'C1';
			}
			elseif($sub6fasaTotal >= 41 && $sub6fasaTotal <= 50)
			{
				$sub6fasaTotal = 'C2';
			}
			elseif($sub6fasaTotal >= 33 && $sub6fasaTotal <= 40)
			{
				$sub6fasaTotal = 'D';
			}
			elseif($sub6fasaTotal >= 21 && $sub6fasaTotal <= 32)
			{
				$sub6fasaTotal = 'E1';
			}
			elseif($sub6fasaTotal <= 20)
			{
				$sub6fasaTotal = 'E2';
			}
	  	
	  	
	  	$pdf->SetFillColor(232, 232, 232);
	    $pdf->SetXY(8,150);
	    $pdf->SetFont('Arial', '', 10);
	    $pdf->Cell(40, 7, 'Sanskrit/Gujarati', 1,0,'L');
		  $pdf->Cell(40, 7, $sub6Fa1Per, 1,0,'C');
		  $pdf->Cell(40, 7, $sub6Fa2Per, 1,0,'C');
		  $pdf->Cell(40, 7, $sub6Sa1Per, 1,0,'C');
		  $pdf->Cell(40, 7, $sub6Sa1TotalPer, 1,0,'C');
		  
		  
	  }
	  
	$selectAtnP = "SELECT attendences
                     FROM attendence
                    WHERE class = '".$class."'
                      AND section = '".$section."'
                      AND grNo = '".$grNo."'
                      AND date >= '".$academicStartYear."'
                      AND date <= '".date("Y-m-d", strtotime($reportDate1))."'
                      AND attendences = 'P'";
    $selectAtnPRes = mysql_query($selectAtnP);
    $countP = mysql_num_rows($selectAtnPRes);
    
    $selectAtnA = "SELECT attendences
                     FROM attendence
                    WHERE class = '".$class."'
                      AND section = '".$section."'
                      AND grNo = '".$grNo."'
                      AND date >= '".$academicStartYear."'
                      AND date <= '".date("Y-m-d", strtotime($reportDate1))."'
                      AND attendences = 'A'";
					  
	//echo $selectAtnP;
	//echo "<br>".$selectAtnA;
	//exit;			  
	
    $selectAtnARes = mysql_query($selectAtnA);
    $countA = mysql_num_rows($selectAtnARes);
    $totalNumberWorkingDay = $countA + $countP;
    
    if($totalNumberWorkingDay != 0)
    {
  	  $countPPer = $countP * 100/$totalNumberWorkingDay;
  	}
  	else
  	{
  		$countPPer = 0;
  	}
  	
    $pdf->SetFont('Arial', '', 8);
	  $pdf->SetXY(8,157);
	  $pdf->Cell(40, 10, 'Attendance', 1,0,'C',0);
	  $pdf->Cell(80, 10, $countP.'/'.$totalNumberWorkingDay, 1,0,'C');
	  $pdf->Cell(40, 10, sprintf("%.2f%%", $countPPer), 1,0,'C');
	  $pdf->Cell(40, 10, '', 1,0,'C');
	  
	  $subAllfasaTotalPerTotal = $sub1Sa1Total + $sub2Sa1Total + $sub3Sa1Total + $sub4Sa1Total + $sub5Sa1Total;
	  //echo $sub5Sa1Total; exit;
	  $subAllfasaTotalPer = $subAllfasaTotalPerTotal/5;
	  
	  $subAllfasaTotal = number_format((($subAllfasaTotalPer) * 100/50),0,'.',' ');
  	if($subAllfasaTotal >= 91 && $subAllfasaTotal <= 100)
		{
		  $subAllfasaTotal = 'A1';
		}
		elseif($subAllfasaTotal >= 81 && $subAllfasaTotal <= 90)
		{
			$subAllfasaTotal = 'A2';
		}
		elseif($subAllfasaTotal >= 71 && $subAllfasaTotal <= 80)
		{
			$subAllfasaTotal = 'B1';
		}
		elseif($subAllfasaTotal >= 61 && $subAllfasaTotal <= 70)
		{
			$subAllfasaTotal = 'B2';
		}
		elseif($subAllfasaTotal >= 51 && $subAllfasaTotal <= 60)
		{
			$subAllfasaTotal = 'C1';
		}
		elseif($subAllfasaTotal >= 41 && $subAllfasaTotal <= 50)
		{
			$subAllfasaTotal = 'C2';
		}
		elseif($subAllfasaTotal >= 33 && $subAllfasaTotal <= 40)
		{
			$subAllfasaTotal = 'D';
		}
		elseif($subAllfasaTotal >= 21 && $subAllfasaTotal <= 32)
		{
			$subAllfasaTotal = 'E1';
		}
		elseif($subAllfasaTotal <= 20)
		{
			$subAllfasaTotal = 'E2';
		}
	  
	  $pdf->SetXY(8,157+10);
	  $pdf->Cell(134, 5, 'Grading Scale : A1 = 91% - 100%; A2 = 81% - 90%; B1 = 71% - 80%; B2 = 61% - 70%; C1 = 51% - 60%;', 1,0,'L',0);
	  $pdf->Cell(66, 10, 'Overall Grade (Main Subj.) : '.$subAllfasaTotal, 1,0,'C',0);
	  
	  $pdf->SetXY(8,157+15);
	  $pdf->Cell(134, 5, 'C2 = 41% - 50%; D = 33% - 40%; E1 = 21% - 32%; E2 = 20% AND BELOW', 1,0,'L',0);
	  
	  
	  
	  $pdf->SetFillColor(232, 232, 232);
	  $pdf->SetFont('Arial', '', 10);
	  $pdf->SetXY(8,157+50);
	  $pdf->Cell(15, 5, 'Result : ', 0, 0, 'L', 0);
	  $pdf->Cell(160,5, $resultNote1, 0, 0, 'L', 0);
	  //$pdf->Cell(25, 5, $reportDate1, 0, 0, 'R', 0);
	 
	 
	  $pdf->Image('./studentImage/stamp1.jpg',135,225,30,25);
	  $pdf->Image('./studentImage/sign.png',170,240,30,10);
	   
	  $pdf->SetFillColor(232, 232, 232);
	  $pdf->SetFont('Arial', '', 8);
	  $pdf->SetXY(8,157+93);
	  $pdf->Cell(67, 5, 'Class Teacher', 0, 0, 'L', 0);
	  $pdf->Cell(66, 5, 'Parent', 0, 0, 'C', 0);
	  $pdf->Cell(67, 5, 'Principal', 0, 0, 'R', 0);
	  
	  $pdf->SetFillColor(232, 232, 232);
	  $pdf->SetFont('Arial', '', 7);
	  $pdf->SetXY(8,157+99);
	  $pdf->Cell(125, 3, 'Note: (1)Promotion is based on the day-to-day continous assessment throughout the year', 0, 0, 'L', 0);
	  $pdf->Cell(75,3, '', 0, 0, 'L', 0);
	  
//	  $pdf->SetXY(5,157+122);
//	  $pdf->Cell(125, 3, '(2)CGPA = Cumulative Grade Point Avarage (3)Subject wise/Overall indicative percentage of', 0, 0, 'L', 0);
//	  $pdf->Cell(75,3, '', 0, 0, 'L', 0);
//	  
//	  $pdf->SetXY(5,157+125);
//	  $pdf->Cell(125, 3, 'Marks = 9.5 X GP of the subject/CGPA.', 0, 0, 'L', 0);
//	  $pdf->Cell(75,3, '', 0, 0, 'L', 0);
	  
	  $pdf->line(8,157+50,208,157+50);
	  $pdf->line(8,157+55,208,157+55);
	  $pdf->line(8,157+98,208,157+98);
	  $pdf->line(8,157+135,208,157+135);
	  
	  $pdf->line(8,5,208,5);
	  $pdf->line(8,5,8,157+135);
	  $pdf->line(208,5,208,157+135);
  }
  $pdf->Output();
}
?>
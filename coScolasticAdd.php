<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$class       = '';
  $count       = 0;
  $today       = date('Y-m-d');
	$stdArray    = array();
	$section     = "";
	$subjectType = "";
	if(isset($_REQUEST['startYear']))
  {
  	$academicStartYear = $_REQUEST['startYear']."-04-01";
  	$nextYear          = $_REQUEST['startYear'] + 1;
  	$academicEndYear   = $nextYear."-03-31";
  	$academicStartYearSelected = $_REQUEST['startYear'];
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y')."-04-01";
	  	$nextYear          = date('Y') + 1;
	  	$academicEndYear   = $nextYear."-03-31";
	  	$academicStartYearSelected = date('Y');
		}
		else
		{
			$prevYear          = date('Y') - 1;
			$academicStartYear = $prevYear."-04-01";
	  	$academicEndYear   = date('Y')."-03-31";
	  	$academicStartYearSelected = $prevYear;
		}
	}
	
	$class   = isset($_GET['class']) && $_GET['class'] != '' ? $_GET['class'] : 0;
	$section = isset($_GET['section']) && $_GET['section'] != '' ? $_GET['section'] : 0;
	$subjectMasterId = isset($_GET['subjectMasterId']) && $_GET['subjectMasterId'] != '' ? $_GET['subjectMasterId'] : 0;
	
	$selectId = "SELECT employeeMasterId
                 FROM employeemaster
                WHERE loginId = '".$_SESSION['s_activName']."'";
  $selectIdRes = mysql_query($selectId);
  if($idRow = mysql_fetch_array($selectIdRes))
  {
  	$selectClass = "SELECT class,section
	                    FROM classteacherallotment
	                   WHERE employeeMasterId = '".$idRow['employeeMasterId']."'
	                     AND academicStartYear = '".$academicStartYear."'
	                     AND academicEndYear = '".$academicEndYear."'";
	  $selectClassRes = mysql_query($selectClass);
	  if($classRow = mysql_fetch_array($selectClassRes))
	  {
	  	if($class == 0)
	  	{
		  	$class   = $classRow['class'];
		  	$section = $classRow['section'];
		  }
		}
		
		$optionQuery = "AND 1 = 1";
		$selectOptSubject = "SELECT subjectDescription
	                         FROM subjectmaster 
	                        WHERE subjectMasterId = '".$subjectMasterId."'";
	  $selectOptSubjectRes = mysql_query($selectOptSubject);
	  if($optSubRow = mysql_fetch_array($selectOptSubjectRes))
	  {
      $subjectType     = $optSubRow['subjectDescription'];
		}
		
		if($subjectType == '3a1')
  	{
  		$optionQuery = "AND nominalroll.3a1 = ".$subjectMasterId."";
  	}
  	
  	if($subjectType == '3a2')
  	{
  		$optionQuery = "AND nominalroll.3a2 = ".$subjectMasterId."";
  	}
  	
  	if($subjectType == '3b1')
  	{
  		$optionQuery = "AND nominalroll.3b1 = ".$subjectMasterId."";
  	}
  	
  	if($subjectType == '3b2')
  	{
  		$optionQuery = "AND nominalroll.3b2 = ".$subjectMasterId."";
  	}
  	
		if(isset($_GET['submit']))
	  {
		  $selectStdIn = "SELECT nominalroll.grNo,nominalroll.rollNo,studentmaster.gender,studentmaster.activated,nominalroll.academicStartYear,nominalroll.academicEndYear
		                    FROM nominalroll
		               LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
		                   WHERE nominalroll.class = '".$class."'
		                     AND nominalroll.section = '".$section."'
		                     AND nominalroll.academicStartYear = '".$academicStartYear."'
	                       AND nominalroll.academicEndYear = '".$academicEndYear."'
	                       ".$optionQuery."
	                       AND studentmaster.activated = 'Y'";
		  $selectStdInRes = mysql_query($selectStdIn);
		  while($stdInRow = mysql_fetch_array($selectStdInRes))
		  {
		  	$rollNo      = $stdInRow['rollNo'];
		  	$grNo        = $stdInRow['grNo'];
		  	$gender      = $stdInRow['gender'];
		  	
		  	$insertClass = "INSERT INTO examscholastic (academicStartYear,academicEndYear,grNo,rollNo,class,section,subjectMasterId,gender)
	  	                  VALUES('".$academicStartYear."','".$academicEndYear."','".$grNo."','".$rollNo."','".$class."','".$section."',".$subjectMasterId.",'".$gender."')";
	  	  $insertClassRes = om_query($insertClass);
	  	  if(!$insertClassRes)
	  	  {
	  	  	echo "Insert Fail";
	  	  }
	  	  else
	  	  {
	  	  	header("Location:coScolasticAdd.php?startYear=".$academicStartYearSelected."&class=".$class."&section=".$section."&subjectMasterId=".$subjectMasterId."&go=go");
	  	  }
		  }
	  }
  
		$i = 0;
	  $selectStd = "SELECT examscholastic.examScholasticId,examscholastic.grNo,examscholastic.rollNo,studentmaster.gender,studentmaster.studentName,examscholastic.grade,
	                       examscholastic.descriptive,examscholastic.subjectMasterId,subjectmaster.subjectName
	                  FROM examscholastic
	             LEFT JOIN studentmaster ON studentmaster.grNo = examscholastic.grNo
	             LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = examscholastic.subjectMasterId
	                 WHERE examscholastic.class = '".$class."'
	                   AND examscholastic.section = '".$section."'
	                   AND examscholastic.subjectMasterId = '".$subjectMasterId."'
	                   AND examscholastic.academicStartYear = '".$academicStartYear."'
	                   AND examscholastic.academicEndYear = '".$academicEndYear."'
	                   AND studentmaster.activated = 'Y'
	              ORDER BY examscholastic.rollNo";
	  $selectStdRes = mysql_query($selectStd);
	  $count = mysql_num_rows($selectStdRes);
	  while($stdRow = mysql_fetch_array($selectStdRes))
	  {
	  	$stdArray[$i]['examScholasticId'] = $stdRow['examScholasticId'];
	  	$stdArray[$i]['grNo']             = $stdRow['grNo'];
	  	$stdArray[$i]['rollNo']           = $stdRow['rollNo'];
	  	$stdArray[$i]['gender']           = $stdRow['gender'];
	  	$stdArray[$i]['studentName']      = $stdRow['studentName'];
	  	$stdArray[$i]['subjectMasterId']  = $stdRow['subjectMasterId'];
	  	$stdArray[$i]['subjectName']      = $stdRow['subjectName'];
	  	$stdArray[$i]['grade']            = $stdRow['grade'];
	  	$stdArray[$i]['descriptive']      = $stdRow['descriptive'];
	  	$i++;
	  }
  }
  
  if(isset($_POST['submitTaken']))
  {
  	$loopCount = 0;
  	while($loopCount < count($_POST['examScholasticId']))
  	{
  		$examScholasticId = ($_POST['examScholasticId'][$loopCount] != '') ? $_POST['examScholasticId'][$loopCount] : 0;
  		$grade            = ($_POST['grade'][$loopCount] != '') ? $_POST['grade'][$loopCount] : '';
  		$descriptive      = ($_POST['descriptive'][$loopCount] != '') ? $_POST['descriptive'][$loopCount] : '';
  		if($_POST['grade'][$loopCount] != '' && $_POST['examScholasticId'][$loopCount] > 0)
  		{
				$updateSchol = "UPDATE examscholastic
		  	                   SET grade = '".$grade."',
		  	                       descriptive = '".$descriptive."'
		  	                 WHERE examScholasticId = ".$examScholasticId."";
			  $updateScholRes = om_query($updateSchol);
			  if(!$updateScholRes)
			  {
			  	echo "Update Fail";
			  }
			  else
			  {
			  	header("Location:coScolasticAdd.php");
			  }
			}
	    $loopCount++;
	  }
  }
  
  $c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster
                   WHERE className > 5
                     AND className <= 10";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
	$k = 0;
  $selectClub = "SELECT subjectMasterId,subjectName
                   FROM subjectmaster
                  WHERE subjectType = 'Co - Scholastic'";
  $selectClubRes = mysql_query($selectClub);
  while($clubRow = mysql_fetch_array($selectClubRes))
  {
  	$clubArr['subjectMasterId'][$k]   = $clubRow['subjectMasterId'];
  	$clubArr['subjectName'][$k]       = $clubRow['subjectName'];
  	$k++;
  }
  
  $secArrOut[0] = 'A';
  $secArrOut[1] = 'B';
  $secArrOut[2] = 'C';
  $secArrOut[3] = 'D';
  
  $grade[0] = 'A';
  $grade[1] = 'B';
  $grade[2] = 'C';
  $grade[3] = 'D';
  $grade[4] = 'E';
  
  $d=0;
	$dArray = array();
	$selectDescriptive = "SELECT descriptive
                          FROM scholarMaster";
	$selectDescriptiveRes = mysql_query($selectDescriptive);
	while($descriptiveRow = mysql_fetch_array($selectDescriptiveRes))
	{
	  $dArray['descriptive'][$d] = $descriptiveRow['descriptive'];
	  $d++;
	}
	
  include("./bottom.php");
  $smarty->assign('academicStartYearSelected',$academicStartYearSelected);
  $smarty->assign('secArrOut',$secArrOut);
  $smarty->assign('class',$class);
  $smarty->assign('grade',$grade);
  $smarty->assign('section',$section);
  $smarty->assign('stdArray',$stdArray);
  $smarty->assign('count',$count);
  $smarty->assign('subjectMasterId',$subjectMasterId);
  $smarty->assign('clubArr',$clubArr);
  $smarty->assign('cArray',$cArray);
  $smarty->assign('dArray',$dArray);
  $smarty->display('coScolasticAdd.tpl');  
}
?>
<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] != 'Teacher')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	if(isset($_REQUEST['startYear']))
  {
	  $academicStartYear = $_REQUEST['startYear'];
	  $academicEndYear   = $_REQUEST['startYear'] + 1;
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y');
	  	$nextYear          = date('Y') + 1;
	  	$academicEndYear   = $nextYear;
		}
		else
		{
			$prevYear          = date('Y') - 1;
			$academicStartYear = $prevYear;
	  	$academicEndYear   = date('Y');
		}
	}
	
	$classArray = array();
  $i = 0;
  $selectClass = "SELECT subjectteacherallotment.subjectTeacherAllotmentId,employeemaster.loginId,
                         subjectteacherallotment.academicStartYear,subjectteacherallotment.academicEndYear,
                         subjectteacherallotment.class,subjectteacherallotment.section,subjectteacherallotment.period,
                         employeemaster.name,subjectmaster.subjectName
                    FROM subjectteacherallotment
               LEFT JOIN employeemaster ON employeemaster.employeeMasterId = subjectteacherallotment.employeeMasterId
               LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = subjectteacherallotment.subjectMasterId
                   WHERE employeemaster.loginId = '".$_SESSION['s_activName']."'
                     AND subjectteacherallotment.academicStartYear = '".$academicStartYear."-04-01'
	                   AND subjectteacherallotment.academicEndYear = '".$academicEndYear."-03-31'";
  $selectClassRes = mysql_query($selectClass);
  while($classRow = mysql_fetch_array($selectClassRes))
  {
  	$classArray[$i]['subjectTeacherAllotmentId'] = $classRow['subjectTeacherAllotmentId'];
  	$classArray[$i]['name']                      = $classRow['name'];
  	$classArray[$i]['academicStartYear']         = substr($classRow['academicStartYear'],0,4);
  	$classArray[$i]['academicEndYear']           = substr($classRow['academicEndYear'],2,2);
    
  	$classArray[$i]['class']       = $classRow['class'];
  	$classArray[$i]['section']     = $classRow['section'];
  	$classArray[$i]['subjectName'] = $classRow['subjectName'];
  	$classArray[$i]['period']      = $classRow['period'];
  	$i++;
  }
  
  include("./bottom.php");
  $smarty->assign('classArray',$classArray);
  $smarty->assign('academicStartYear',$academicStartYear);
  $smarty->display('mySubjectTeacherShip.tpl');  
}
?>
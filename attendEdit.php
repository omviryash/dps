<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$attendDateDMY    = '';
	$employeeattendId = 0;
	$today = date('Y-m-d');
	
	if(isset($_REQUEST['attendenceYear']))
	{
	  $attendenceDate = $_REQUEST['attendenceYear']."-".$_REQUEST['attendenceMonth']."-".$_REQUEST['attendenceDay'];
	}
	else
	{
	   $attendenceDate = $today;
	}
	if(isset($_REQUEST['staff_type']))
	{
	  $staff_type= $_REQUEST['staff_type'];
	}
	else if(isset($_GET['staff_type']))
	{
	   $staff_type= $_GET['staff_type'];
	}
	else
	{
	   $staff_type= 'Teacher';
	}
	$attendDate       = isset($_REQUEST['attendDate']) && ($_REQUEST['attendDate'] != '') ? $_REQUEST['attendDate'] : 0;
	$attendenceArr    = array();
	$i = 0;
	
	if(isset($_REQUEST['attendenceYear']) || isset($_GET['staff_type'])){
		$selectAttendM= "SELECT employeeattend.employeeattendId,employeeattend.employeeCode,employeeattend.startTime,employeeattend.attendences,
		                          employeeattend.late,employeeattend.date,employeemaster.name
		                     FROM employeeattend
		                     LEFT JOIN employeemaster ON employeemaster.employeeCode = employeeattend.employeeCode
		                    WHERE employeeattend.date = '".$attendenceDate."' AND employeemaster.userType='".$staff_type."'  order by employeeattend.startTime DESC";
		$selectAttendMRes = mysql_query($selectAttendM);
	}
	else{
		$selectAttendM= "SELECT employeeattend.employeeattendId,employeeattend.employeeCode,employeeattend.startTime,employeeattend.attendences,
		                          employeeattend.late,employeeattend.date,employeemaster.name
		                     FROM employeeattend
		                     LEFT JOIN employeemaster ON employeemaster.employeeCode = employeeattend.employeeCode
		                    WHERE employeeattend.date = '".$attendDate."' AND employeemaster.userType='Teacher'  order by employeeattend.startTime DESC";
		$selectAttendMRes = mysql_query($selectAttendM);
	}
	while($maRow = mysql_fetch_array($selectAttendMRes))
	{
	  	$attendenceArr[$i]['employeeattendId'] = $maRow['employeeattendId'];
	  	$attendenceArr[$i]['date']         = $maRow['date'];
	  	$attendenceArr[$i]['employeeCode'] = $maRow['employeeCode'];
	  	$attendenceArr[$i]['name']         = $maRow['name'];
	  	$attendenceArr[$i]['startTime']    = $maRow['startTime'];
	  	$attendenceArr[$i]['attendences']  = $maRow['attendences'];
	  	$attendenceArr[$i]['late']         = $maRow['late'];
	  	$i++;
	}
  
  if(isset($_POST['submitTaken']))
  {
  	$loopCount = 0;
  	while($loopCount < count($_POST['employeeattendId']))
  	{
  		$employeeattendId = ($_POST['employeeattendId'][$loopCount] != '') ? $_POST['employeeattendId'][$loopCount] : 0;
  		$startTime        = ($_POST['startTime'][$loopCount] != '') ? $_POST['startTime'][$loopCount] : '00:00:00';
  		$attendences      = ($_POST['attendences'][$loopCount] != '') ? $_POST['attendences'][$loopCount] : 'A';
  		$late             = ($_POST['late'][$loopCount] != '') ? $_POST['late'][$loopCount] : 'No';
  		
  		
  		if($_POST['employeeattendId'][$loopCount] != '' && $_POST['employeeattendId'][$loopCount] > 0)
  		{
				$updateClass = "UPDATE employeeattend
		  	                   SET startTime = '".$startTime."',
		  	                       attendences = '".$attendences."',
		  	                       late = '".$late."'
		  	                 WHERE employeeattendId = ".$employeeattendId."";
			  $updateClassRes = om_query($updateClass);
			  if(!$updateClassRes)
			  {
			  	echo "Update Fail";
			  }
			  else
			  {
			  	header("Location:attendEdit.php?attendDate=".$attendDate."");
			  }
			}
			$loopCount++;
		}
  }
  
  $lateOut[0] = 'Yes';
  $lateOut[1] = 'No';
  
  $attendDateDMY = substr($attendDate,8,2).'-'.substr($attendDate,5,2).'-'.substr($attendDate,0,4);
  
  include("./bottom.php");
  
  // delete code
  
  if(isset($_POST['Delete']))
{
	if(!empty($_POST['check_list']))
	{
		foreach($_POST['check_list'] as $selected)
		{
			$deleteStanding = "DELETE FROM employeeattend 
                       WHERE employeeattendId=".$selected;
			$result=mysql_query($deleteStanding);
			if(!$result)
			{
				echo "No Rows Deleted";
			}
			else
				header("Location: attendEdit.php?attendDate=".$attendDate."");
		}
	}
}
  
  
  $smarty->assign('employeeattendId',$employeeattendId);
  $smarty->assign('attendenceArr',$attendenceArr);
  $smarty->assign('attendDate',$attendDate);
  $smarty->assign('attendDateDMY',$attendDateDMY);
  $smarty->assign('lateOut',$lateOut);
  $smarty->assign('attendenceDate',$attendenceDate);
  $smarty->assign('staff_type',$staff_type);
  $smarty->display('attendEdit.tpl');  
}
?>
<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$today = date('Y-m-d');
	$academicStartYearSecondNum = 0;
	$attendenceArr = '';
	
	if(isset($_REQUEST['startYear']))
  {
  	$academicStartYear = $_REQUEST['startYear']."-04-01";
  	$nextYear          = $_REQUEST['startYear'] + 1;
  	$academicEndYear   = $nextYear."-03-31";
  	$academicStartYearSelected = $_REQUEST['startYear'];
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y')."-04-01";
	  	$nextYear          = date('Y') + 1;
	  	$academicEndYear   = $nextYear."-03-31";
	  	$academicStartYearSelected = date('Y');
		}
		else
		{
			$prevYear          = date('Y') - 1;
			$academicStartYear = $prevYear."-04-01";
	  	$academicEndYear   = date('Y')."-03-31";
	  	$academicStartYearSelected = $prevYear;
		}
	}
	
	$class   = isset($_REQUEST['class']) ? $_REQUEST['class'] : 0;
	$section = isset($_REQUEST['classSection']) ? $_REQUEST['classSection'] : 0;
	
	if($class == 5 || $class == 6 || $class == 7 || $class == 8 || $class == 9 || $class == 10)
  {
  	$classWordJoin1 = '3';
  	$classWordJoin2 = '5';
  	$classWordJoin3 = '6';
  }
  elseif($class == 'Pre-Nursery' || $class == 'Nursery' || $class == 'Prep' || $class == 1 || $class == 2 || $class == 3 || $class == 4)
  {
  	$classWordJoin1 = '4';
  	$classWordJoin2 = '5';
  	$classWordJoin3 = '6';
  }
  elseif($class == '11 SCI.' || $class == '12 SCI.')
  {
  	$classWordJoin1 = '4';
  	$classWordJoin2 = '3';
  	$classWordJoin3 = '6';
  }
  elseif($class == '11 COM.' || $class == '12 COM.')
  {
  	$classWordJoin1 = '3';
  	$classWordJoin2 = '4';
  	$classWordJoin3 = '5';
  }
  else
  {
  	$classWordJoin1 = '3';
  	$classWordJoin2 = '5';
  	$classWordJoin3 = '6';
  }
  
  $grNo = isset($_REQUEST['grNo']) ? $_REQUEST['grNo'] : 0;
  
  if($grNo > 0)
  {
		$attendenceArr   = array();
		$i = 0;
	  $selectAttendM= "SELECT feeTypeId,feeType
	                     FROM feetype
	                    WHERE feeTypeId != '".$classWordJoin1."'
	                      AND feeTypeId != '".$classWordJoin2."'
	                      AND feeTypeId != '".$classWordJoin3."'
	                 ORDER BY feeTypeId DESC";
	  $selectAttendMRes = mysql_query($selectAttendM);
	  while($maRow = mysql_fetch_array($selectAttendMRes))
	  {
	  	$attendenceArr[$i]['feeType'] = $maRow['feeType'];
	  	
	  	$selectFeeStructure = "SELECT feeStructureId,amount,feeTypeId
		                          FROM feestructure
		                         WHERE feeTypeId = ".$maRow['feeTypeId']."
		                           AND mainStatus = '1'";
		  $selectFeeStructureRes = mysql_query($selectFeeStructure);
		  while($structureRow = mysql_fetch_array($selectFeeStructureRes))
		  $attendenceArr[$i]['amount'] = $structureRow['amount'];
		  $attendenceArr[$i]['amountTotal'] = 0;
		  $selectFeeCollection = "SELECT grNo,amount AS amountTotal
		                            FROM feecollection
		                           WHERE feeTypeId = ".$maRow['feeTypeId']."
		                             AND grNo = '".$grNo."'";
		  $selectFeeCollectionRes = mysql_query($selectFeeCollection);
		  while($collectionRow = mysql_fetch_array($selectFeeCollectionRes))
		  
		  $attendenceArr[$i]['amountTotal'] += $collectionRow['amountTotal'];
		  
		  $pendingFee = $attendenceArr[$i]['amount'] - $attendenceArr[$i]['amountTotal'];
		  if($attendenceArr[$i]['amountTotal'] > $attendenceArr[$i]['amount'])
		  {
		  	$attendenceArr[$i]['status'] = 'Pay Full Fee';
		  }
		  else
		  {
		  	$attendenceArr[$i]['status'] = 'Pending Fee '.$pendingFee.' Rs.';
		  }
	  	$i++;
	  }
  }
  $c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  $secArrOut[0] = 'A';
  $secArrOut[1] = 'B';
  $secArrOut[2] = 'C';
  $secArrOut[3] = 'D';
  
  $academicStartYearSecondNum = substr($academicStartYearSelected + 1,-2);
  include("./bottom.php");
  $smarty->assign('grNo',$grNo);
  $smarty->assign('cArray',$cArray);
  $smarty->assign('class',$class);
  $smarty->assign('section',$section);
  $smarty->assign('secArrOut',$secArrOut);
  $smarty->assign('attendenceArr',$attendenceArr);
  $smarty->assign('academicStartYear',$academicStartYear);
  $smarty->assign('academicStartYearSelected',$academicStartYearSelected);
  $smarty->assign('academicStartYearSecondNum',$academicStartYearSecondNum);
  $smarty->display('feePendingList.tpl');  
}
?>
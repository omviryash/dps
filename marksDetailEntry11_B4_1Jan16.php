<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$class      = '';
  $count      = 0;
  $today      = date('Y-m-d');
	$stdArray   = array();
	$section    = '';
	$nominalRollId = 0;
	
	if(isset($_REQUEST['startYear']))
  {
  	$academicStartYear = $_REQUEST['startYear']."-04-01";
  	$nextYear          = $_REQUEST['startYear'] + 1;
  	$academicEndYear   = $nextYear."-03-31";
  	$academicStartYearSelected = $_REQUEST['startYear'];
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y')."-04-01";
	  	$nextYear          = date('Y') + 1;
	  	$academicEndYear   = $nextYear."-03-31";
	  	$academicStartYearSelected = date('Y');
		}
		else
		{
			$prevYear          = date('Y') - 1;
			$academicStartYear = $prevYear."-04-01";
	  	$academicEndYear   = date('Y')."-03-31";
	  	$academicStartYearSelected = $prevYear;
		}
	}
	
	$class   = isset($_REQUEST['class']) && $_REQUEST['class'] != '' ? $_REQUEST['class'] : 0;
	$section = isset($_REQUEST['section']) && $_REQUEST['section'] != '' ? $_REQUEST['section'] : 0;
	
	$selectId = "SELECT employeeMasterId
                 FROM employeemaster
                WHERE loginId = '".$_SESSION['s_activName']."'";
  $selectIdRes = mysql_query($selectId);
  if($idRow = mysql_fetch_array($selectIdRes))
  {
  	$selectClass = "SELECT class,section
	                    FROM classteacherallotment
	                   WHERE employeeMasterId = '".$idRow['employeeMasterId']."'
	                     AND academicStartYear = '".$academicStartYear."'
                       AND academicEndYear = '".$academicEndYear."'";
	  $selectClassRes = mysql_query($selectClass);
	  if($classRow = mysql_fetch_array($selectClassRes))
	  {
	  	if($class == 0)
	  	{
		  	$class   = $classRow['class'];
		  	$section = $classRow['section'];
		  }
		}
		
		$dtlArr = array();
		$g = 0;
	  $selectStdIn = "SELECT nominalroll.nominalRollId,nominalroll.grNo,studentmaster.gender,studentmaster.activated,
	                         nominalroll.academicStartYear,nominalroll.academicEndYear,studentmaster.studentName,
	                         nominalroll.height,nominalroll.weight,
	                         nominalroll.enEducation,nominalroll.phEducation,nominalroll.enEducation2,nominalroll.phEducation2,
	                         nominalroll.resultRemark1,nominalroll.resultRemark2,nominalroll.rollNo
	                    FROM nominalroll
	               LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
	                   WHERE nominalroll.class = '".$class."'
	                     AND nominalroll.section = '".$section."'
	                     AND nominalroll.academicStartYear = '".$academicStartYear."'
                       AND nominalroll.academicEndYear = '".$academicEndYear."'
                       AND studentmaster.activated = 'Y'";
	  $selectStdInRes = mysql_query($selectStdIn);
	  $count = mysql_num_rows($selectStdInRes);
	  while($stdInRow = mysql_fetch_array($selectStdInRes))
	  {
	  	$dtlArr[$g]['nominalRollId'] = $stdInRow['nominalRollId'];
	  	$dtlArr[$g]['rollNo']        = $stdInRow['rollNo'];
	  	$dtlArr[$g]['grNo']          = $stdInRow['grNo'];
	  	$dtlArr[$g]['studentName']   = $stdInRow['studentName'];
	  	
	  	$dtlArr[$g]['height']        = $stdInRow['height'];
	  	$dtlArr[$g]['weight']        = $stdInRow['weight'];
	  	
	  	$dtlArr[$g]['resultRemark1'] = $stdInRow['resultRemark1'];
	  	$dtlArr[$g]['resultRemark2'] = $stdInRow['resultRemark2'];
	  	
	  	$dtlArr[$g]['enEducation']   = $stdInRow['enEducation'];
	  	$dtlArr[$g]['phEducation']   = $stdInRow['phEducation'];
	  	$dtlArr[$g]['enEducation2']  = $stdInRow['enEducation2'];
	  	$dtlArr[$g]['phEducation2']  = $stdInRow['phEducation2'];
	  	$g++;
	  }
  }
  
  if(isset($_POST['submitTaken']))
  {
  	$class   = isset($_POST['class']) && $_POST['class'] != '' ? $_POST['class'] : 0;
	  $section = isset($_POST['section']) && $_POST['section'] != '' ? $_POST['section'] : 0;
  	$loopCount = 0;
  	while($loopCount < count($_POST['nominalRollId']))
  	{
  		$nominalRollId = ($_POST['nominalRollId'][$loopCount] != '') ? $_POST['nominalRollId'][$loopCount] : 0;
  		$height        = ($_POST['height'][$loopCount] != '') ? $_POST['height'][$loopCount] : 0;
  		$weight        = ($_POST['weight'][$loopCount] != '') ? $_POST['weight'][$loopCount] : 0;
  		$resultRemark1 = ($_POST['resultRemark1'][$loopCount] != '') ? $_POST['resultRemark1'][$loopCount] : '';
  		$resultRemark2 = ($_POST['resultRemark2'][$loopCount] != '') ? $_POST['resultRemark2'][$loopCount] : '';
  		$enEducation   = ($_POST['enEducation'][$loopCount] != '') ? $_POST['enEducation'][$loopCount] : '';
  		$phEducation   = ($_POST['phEducation'][$loopCount] != '') ? $_POST['phEducation'][$loopCount] : '';
  		$enEducation2  = ($_POST['enEducation2'][$loopCount] != '') ? $_POST['enEducation2'][$loopCount] : '';
  		$phEducation2  = ($_POST['phEducation2'][$loopCount] != '') ? $_POST['phEducation2'][$loopCount] : '';
  		
  		if($_POST['nominalRollId'][$loopCount] != '' && $_POST['nominalRollId'][$loopCount] > 0)
  		{
				$updateClass = "UPDATE nominalroll
		  	                   SET height = ".$height.",
		  	                       weight = ".$weight.",
		  	                       resultRemark1 = '".$resultRemark1."',
		  	                       resultRemark2 = '".$resultRemark2."',
		  	                       enEducation = '".$enEducation."',
		  	                       phEducation = '".$phEducation."',
		  	                       enEducation2 = '".$enEducation2."',
		  	                       phEducation2 = '".$phEducation2."'
		  	                 WHERE nominalRollId = ".$nominalRollId."";
			  $updateClassRes = om_query($updateClass);
			  if(!$updateClassRes)
			  {
			  	echo "Update Fail";
			  }
			  else
			  {
			  	header("Location:marksDetailEntry11.php?startYear=".$academicStartYearSelected."&class=".$class."&section=".$section."&go=go");
			  }
			}
			$loopCount++;
		}
  }
    
  $c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster
                   WHERE className > 10";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  $secArrOut[0] = 'A';
  $secArrOut[1] = 'B';
  $secArrOut[2] = 'C';
  $secArrOut[3] = 'D';
  
  $grdArrOut[0] = 'A';
  $grdArrOut[1] = 'B';
  $grdArrOut[2] = 'C';
  $grdArrOut[3] = 'D';
  $grdArrOut[4] = 'E';
	
  include("./bottom.php");
  $smarty->assign('academicStartYearSelected',$academicStartYearSelected);
  $smarty->assign('dtlArr',$dtlArr);
  $smarty->assign('secArrOut',$secArrOut);
  $smarty->assign('grdArrOut',$grdArrOut);
  $smarty->assign('class',$class);
  $smarty->assign('section',$section);
  $smarty->assign('stdArray',$stdArray);
  $smarty->assign('count',$count);
  $smarty->assign('cArray',$cArray);
  $smarty->assign('nominalRollId',$nominalRollId);
  $smarty->display('marksDetailEntry11.tpl');  
}
?>
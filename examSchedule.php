<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$filePath = '';
  $fileName = '';
  
  $scheduleDate    = '';
  $maxMarks        = 0;
  $minMarks        = 0;
  $class           = '';
  $displayStudent  = '';
  
  if(isset($_POST['submitBtn']))
  {
    $uploaddir = dirname($_POST['filePath']);
  
    $uploadfile = $uploaddir . basename($_FILES['fileName']['name']);
    
    $target_path = 'data';
    $target_path = $target_path ."/". basename( $_FILES['fileName']['name']);
    $_FILES['fileName']['tmp_name']; // temp file
    
    $oldfile =  basename($_FILES['fileName']['name']);
  
    // getting the extention
  
    $pos = strpos($oldfile,".",0);
    $ext = trim(substr($oldfile,$pos+1,strlen($oldfile))," ");
    
    if(move_uploaded_file($_FILES['fileName']['tmp_name'], $target_path)) 
    {
    	$row = 0;
      $handle = fopen($target_path, "r");
      while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
      {
        if($row > 0)
        {
        	if($data[4] != '')
					{
					  echo $scheduleDate       = substr($data[4], 6, 4)."-".substr($data[4], 3, 2)."-".substr($data[4], 0, 2);
					}
          $maxMarks        = 0;
          $minMarks        = 0;
          
          if($data[6] == 'I')
          {
            $class = 1;
          }
          elseif($data[6] == 'II')
          {
          	$class = 2;
          }
          elseif($data[6] == 'III')
          {
          	$class = 3;
          }
          elseif($data[6] == 'IV')
          {
          	$class = 4;
          }
          elseif($data[6] == 'V')
          {
          	$class = 5;
          }
          elseif($data[6] == 'VI')
          {
          	$class = 6;
          }
          elseif($data[6] == 'VII')
          {
          	$class = 7;
          }
          elseif($data[6] == 'VIII')
          {
          	$class = 8;
          }
          elseif($data[6] == 'IX')
          {
          	$class = 9;
          }
          elseif($data[6] == 'X')
          {
          	$class = 10;
          }
          elseif($data[6] == 'XI Science')
          {
          	$class = 11 .' SCI.';
          }
          elseif($data[6] == 'XI Commerce')
          {
          	$class = 11 .' COM.';
          }
          elseif($data[6] == 'XII Science')
          {
          	$class = 12 .' SCI.';
          }
          elseif($data[6] == 'XII Commerce')
          {
          	$class = 12 .' COM.';
          }
          else
          {
          	$class = $data[6];
          }
          $displayStudent  = $data[14];
         
          $insertsubMaster = "INSERT INTO subjectmaster(scheduleDate,maxMarks,minMarks,class)
        	                         VALUES ('".$scheduleDate."',".$maxMarks.",".$minMarks.",'".$class."','".$displayStudent."')";
        	om_query($insertsubMaster);
  	    }
        $row++;
      }
    } 
    else
    {
      echo "There was an error uploading the file, please try again!";
    }
  }
}
?>
<BODY>
  <H1><CENTER><FONT color="red">Subject Master</FONT></CENTER></H1><HR>
<CENTER>
<FORM enctype="multipart/form-data" action="" name="examSchedule" method="POST"> 
  <a href="index.php">Home</a>
	<br /><br />
  <INPUT size="60" type="hidden" name="filePath"><BR>
  File Name:
  <INPUT name="fileName" type="file" onChange="document.examSchedule.filePath.value=document.examSchedule.fileName.value;">
  <INPUT type="submit" name="submitBtn" value="Submit File"> 
</FORM>
</CENTER>
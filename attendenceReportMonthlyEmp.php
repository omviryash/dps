<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$attendenceArr  = array();
	$atnArr         = array();
  $today          = date('Y-m');
  
  $academicStartYear = '';
  $academicEndYear   = '';
	//$totalNumberWorkingDay = 0;
	
	if(isset($_REQUEST['attendenceYear']))
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = $_REQUEST['attendenceYear']."-04-01";
	  	$nextYear          = $_REQUEST['attendenceYear'] + 1;
	  	$academicEndYear   = $nextYear."-03-31";
		}
		else
		{
			$prevYear          = $_REQUEST['attendenceYear'] - 1;
			$academicStartYear = $prevYear."-04-01";
	  	$academicEndYear   = $_REQUEST['attendenceYear']."-03-31";
		}
	}
	
	if(isset($_REQUEST['attendenceYear']))
	{
	  $attendenceDate = $_REQUEST['attendenceYear']."-".$_REQUEST['attendenceMonth'];
	}
	else
	{
		$attendenceDate = $today;
	}
  $teachingNonteaching = isset($_REQUEST['teachingNonteaching']) ? $_REQUEST['teachingNonteaching'] : 't';

  
  if($teachingNonteaching == 't')
  {
    $orderBy = 'DESC';
    $user_type = "userType='Teacher'";
  }
  else
  {
    $orderBy = 'ASC';
    $user_type = "userType!='Teacher'";
  }

  $teachingNonteachingCmbVal[0] = 't';
  $teachingNonteachingCmbVal[1] = 'nt';

  $teachingNonteachingCmbTxt[0] = 'Teacher';
  $teachingNonteachingCmbTxt[1] = 'Administrator';
  $teachingNonteachingCmbTxt[2] = 'Class IV';

  $i = 0;
  $selectAttendM= "SELECT employeeCode,name,activated,userType
                     FROM employeemaster
                    WHERE activated = 'Yes' AND $user_type AND employeeCode IS NOT NULL ORDER BY userType $orderBy";
  $selectAttendMRes = mysql_query($selectAttendM);
  while($maRow = mysql_fetch_array($selectAttendMRes))
  {
    $attendenceArr[$i]['SrNo'] = $i+1;
  	$attendenceArr[$i]['employeeCode'] = $maRow['employeeCode'];
  	$attendenceArr[$i]['name']         = $maRow['name'];
  	$attendenceArr[$i]['activated']    = $maRow['activated'];
  	
  	$selectAtnP = "SELECT attendences
                     FROM employeeattend
                    WHERE employeeCode = '".$maRow['employeeCode']."'
                      AND date >= '".$attendenceDate."-01'
                      AND date <= '".$attendenceDate."-31'
                      AND attendences = 'H'";
    $selectAtnPRes = mysql_query($selectAtnP);
    $countP = mysql_num_rows($selectAtnPRes);
    $countPHalf = $countP/2;
    
    $selectAtnA = "SELECT attendences
                     FROM employeeattend
                    WHERE employeeCode = '".$maRow['employeeCode']."'
                      AND date >= '".$attendenceDate."-01'
                      AND date <= '".$attendenceDate."-31'
                      AND attendences = 'A'";
    $selectAtnARes = mysql_query($selectAtnA);
    $countA = mysql_num_rows($selectAtnARes);
    $attendenceArr[$i]['countA'] = $countA + $countPHalf;
    //$attendenceArr[$i]['totalNumberWorkingDay'] = $countA + $countPHalf;
    //$totalNumberWorkingDay = $countA + $countP;
    
    $attendenceArr[$i]['attendences1'] = '';
    $attendenceArr[$i]['attendences2'] = '';
    $attendenceArr[$i]['attendences3'] = '';
    $attendenceArr[$i]['attendences4'] = '';
    $attendenceArr[$i]['attendences5'] = '';
    $attendenceArr[$i]['attendences6'] = '';
    $attendenceArr[$i]['attendences7'] = '';
    $attendenceArr[$i]['attendences8'] = '';
    $attendenceArr[$i]['attendences9'] = '';
    $attendenceArr[$i]['attendences10'] = '';
    $attendenceArr[$i]['attendences11'] = '';
    $attendenceArr[$i]['attendences12'] = '';
    $attendenceArr[$i]['attendences13'] = '';
    $attendenceArr[$i]['attendences14'] = '';
    $attendenceArr[$i]['attendences15'] = '';
    $attendenceArr[$i]['attendences16'] = '';
    $attendenceArr[$i]['attendences17'] = '';
    $attendenceArr[$i]['attendences18'] = '';
    $attendenceArr[$i]['attendences19'] = '';
    $attendenceArr[$i]['attendences20'] = '';
    $attendenceArr[$i]['attendences21'] = '';
    $attendenceArr[$i]['attendences22'] = '';
    $attendenceArr[$i]['attendences23'] = '';
    $attendenceArr[$i]['attendences24'] = '';
    $attendenceArr[$i]['attendences25'] = '';
    $attendenceArr[$i]['attendences26'] = '';
    $attendenceArr[$i]['attendences27'] = '';
    $attendenceArr[$i]['attendences28'] = '';
    $attendenceArr[$i]['attendences29'] = '';
    $attendenceArr[$i]['attendences30'] = '';
    $attendenceArr[$i]['attendences31'] = '';
    //Main Monthly Attendences Start
    $selectAtnP = "SELECT date,employeeCode,attendences
                     FROM employeeattend
                    WHERE employeeCode = '".$maRow['employeeCode']."'
                      AND date = '".$attendenceDate."-01'";
    $selectAtnPRes = mysql_query($selectAtnP);
    if($allRow = mysql_fetch_array($selectAtnPRes))
    {
	    $attendenceArr[$i]['attendences1'] = $allRow['attendences'];
    }
    
    $selectAtnP = "SELECT date,employeeCode,attendences
                     FROM employeeattend
                    WHERE employeeCode = '".$maRow['employeeCode']."'
                      AND date = '".$attendenceDate."-02'";
    $selectAtnPRes = mysql_query($selectAtnP);
    if($allRow = mysql_fetch_array($selectAtnPRes))
    {
	    $attendenceArr[$i]['attendences2'] = $allRow['attendences'];
    }
    
    $selectAtnP = "SELECT date,employeeCode,attendences
                     FROM employeeattend
                    WHERE employeeCode = '".$maRow['employeeCode']."'
                      AND date = '".$attendenceDate."-03'";
    $selectAtnPRes = mysql_query($selectAtnP);
    if($allRow = mysql_fetch_array($selectAtnPRes))
    {
	    $attendenceArr[$i]['attendences3'] = $allRow['attendences'];
    }
    
    $selectAtnP = "SELECT date,employeeCode,attendences
                     FROM employeeattend
                    WHERE employeeCode = '".$maRow['employeeCode']."'
                      AND date = '".$attendenceDate."-04'";
    $selectAtnPRes = mysql_query($selectAtnP);
    if($allRow = mysql_fetch_array($selectAtnPRes))
    {
	    $attendenceArr[$i]['attendences4'] = $allRow['attendences'];
    }
    
    $selectAtnP = "SELECT date,employeeCode,attendences
                     FROM employeeattend
                    WHERE employeeCode = '".$maRow['employeeCode']."'
                      AND date = '".$attendenceDate."-05'";
    $selectAtnPRes = mysql_query($selectAtnP);
    if($allRow = mysql_fetch_array($selectAtnPRes))
    {
	    $attendenceArr[$i]['attendences5'] = $allRow['attendences'];
    }
    
    $selectAtnP = "SELECT date,employeeCode,attendences
                     FROM employeeattend
                    WHERE employeeCode = '".$maRow['employeeCode']."'
                      AND date = '".$attendenceDate."-06'";
    $selectAtnPRes = mysql_query($selectAtnP);
    if($allRow = mysql_fetch_array($selectAtnPRes))
    {
	    $attendenceArr[$i]['attendences6'] = $allRow['attendences'];
    }
    
    $selectAtnP = "SELECT date,employeeCode,attendences
                     FROM employeeattend
                    WHERE employeeCode = '".$maRow['employeeCode']."'
                      AND date = '".$attendenceDate."-07'";
    $selectAtnPRes = mysql_query($selectAtnP);
    if($allRow = mysql_fetch_array($selectAtnPRes))
    {
	    $attendenceArr[$i]['attendences7'] = $allRow['attendences'];
    }
    
    $selectAtnP = "SELECT date,employeeCode,attendences
                     FROM employeeattend
                    WHERE employeeCode = '".$maRow['employeeCode']."'
                      AND date = '".$attendenceDate."-08'";
    $selectAtnPRes = mysql_query($selectAtnP);
    if($allRow = mysql_fetch_array($selectAtnPRes))
    {
	    $attendenceArr[$i]['attendences8'] = $allRow['attendences'];
    }
    
    $selectAtnP = "SELECT date,employeeCode,attendences
                     FROM employeeattend
                    WHERE employeeCode = '".$maRow['employeeCode']."'
                      AND date = '".$attendenceDate."-09'";
    $selectAtnPRes = mysql_query($selectAtnP);
    if($allRow = mysql_fetch_array($selectAtnPRes))
    {
	    $attendenceArr[$i]['attendences9'] = $allRow['attendences'];
    }
    
    $selectAtnP = "SELECT date,employeeCode,attendences
                     FROM employeeattend
                    WHERE employeeCode = '".$maRow['employeeCode']."'
                      AND date = '".$attendenceDate."-10'";
    $selectAtnPRes = mysql_query($selectAtnP);
    if($allRow = mysql_fetch_array($selectAtnPRes))
    {
	    $attendenceArr[$i]['attendences10'] = $allRow['attendences'];
    }
    
    $selectAtnP = "SELECT date,employeeCode,attendences
                     FROM employeeattend
                    WHERE employeeCode = '".$maRow['employeeCode']."'
                      AND date = '".$attendenceDate."-11'";
    $selectAtnPRes = mysql_query($selectAtnP);
    if($allRow = mysql_fetch_array($selectAtnPRes))
    {
	    $attendenceArr[$i]['attendences11'] = $allRow['attendences'];
    }
    
    $selectAtnP = "SELECT date,employeeCode,attendences
                     FROM employeeattend
                    WHERE employeeCode = '".$maRow['employeeCode']."'
                      AND date = '".$attendenceDate."-12'";
    $selectAtnPRes = mysql_query($selectAtnP);
    if($allRow = mysql_fetch_array($selectAtnPRes))
    {
	    $attendenceArr[$i]['attendences12'] = $allRow['attendences'];
    }
    
    $selectAtnP = "SELECT date,employeeCode,attendences
                     FROM employeeattend
                    WHERE employeeCode = '".$maRow['employeeCode']."'
                      AND date = '".$attendenceDate."-13'";
    $selectAtnPRes = mysql_query($selectAtnP);
    if($allRow = mysql_fetch_array($selectAtnPRes))
    {
	    $attendenceArr[$i]['attendences13'] = $allRow['attendences'];
    }
    
    $selectAtnP = "SELECT date,employeeCode,attendences
                     FROM employeeattend
                    WHERE employeeCode = '".$maRow['employeeCode']."'
                      AND date = '".$attendenceDate."-14'";
    $selectAtnPRes = mysql_query($selectAtnP);
    if($allRow = mysql_fetch_array($selectAtnPRes))
    {
	    $attendenceArr[$i]['attendences14'] = $allRow['attendences'];
    }
    
    $selectAtnP = "SELECT date,employeeCode,attendences
                     FROM employeeattend
                    WHERE employeeCode = '".$maRow['employeeCode']."'
                      AND date = '".$attendenceDate."-15'";
    $selectAtnPRes = mysql_query($selectAtnP);
    if($allRow = mysql_fetch_array($selectAtnPRes))
    {
	    $attendenceArr[$i]['attendences15'] = $allRow['attendences'];
    }
    
    $selectAtnP = "SELECT date,employeeCode,attendences
                     FROM employeeattend
                    WHERE employeeCode = '".$maRow['employeeCode']."'
                      AND date = '".$attendenceDate."-16'";
    $selectAtnPRes = mysql_query($selectAtnP);
    if($allRow = mysql_fetch_array($selectAtnPRes))
    {
	    $attendenceArr[$i]['attendences16'] = $allRow['attendences'];
    }
    
    $selectAtnP = "SELECT date,employeeCode,attendences
                     FROM employeeattend
                    WHERE employeeCode = '".$maRow['employeeCode']."'
                      AND date = '".$attendenceDate."-17'";
    $selectAtnPRes = mysql_query($selectAtnP);
    if($allRow = mysql_fetch_array($selectAtnPRes))
    {
	    $attendenceArr[$i]['attendences17'] = $allRow['attendences'];
    }
    
    $selectAtnP = "SELECT date,employeeCode,attendences
                     FROM employeeattend
                    WHERE employeeCode = '".$maRow['employeeCode']."'
                      AND date = '".$attendenceDate."-18'";
    $selectAtnPRes = mysql_query($selectAtnP);
    if($allRow = mysql_fetch_array($selectAtnPRes))
    {
	    $attendenceArr[$i]['attendences18'] = $allRow['attendences'];
    }
    
    $selectAtnP = "SELECT date,employeeCode,attendences
                     FROM employeeattend
                    WHERE employeeCode = '".$maRow['employeeCode']."'
                      AND date = '".$attendenceDate."-19'";
    $selectAtnPRes = mysql_query($selectAtnP);
    if($allRow = mysql_fetch_array($selectAtnPRes))
    {
	    $attendenceArr[$i]['attendences19'] = $allRow['attendences'];
    }
    
    $selectAtnP = "SELECT date,employeeCode,attendences
                     FROM employeeattend
                    WHERE employeeCode = '".$maRow['employeeCode']."'
                      AND date = '".$attendenceDate."-20'";
    $selectAtnPRes = mysql_query($selectAtnP);
    if($allRow = mysql_fetch_array($selectAtnPRes))
    {
	    $attendenceArr[$i]['attendences20'] = $allRow['attendences'];
    }
    
    $selectAtnP = "SELECT date,employeeCode,attendences
                     FROM employeeattend
                    WHERE employeeCode = '".$maRow['employeeCode']."'
                      AND date = '".$attendenceDate."-21'";
    $selectAtnPRes = mysql_query($selectAtnP);
    if($allRow = mysql_fetch_array($selectAtnPRes))
    {
	    $attendenceArr[$i]['attendences21'] = $allRow['attendences'];
    }
    
    $selectAtnP = "SELECT date,employeeCode,attendences
                     FROM employeeattend
                    WHERE employeeCode = '".$maRow['employeeCode']."'
                      AND date = '".$attendenceDate."-22'";
    $selectAtnPRes = mysql_query($selectAtnP);
    if($allRow = mysql_fetch_array($selectAtnPRes))
    {
	    $attendenceArr[$i]['attendences22'] = $allRow['attendences'];
    }
    
    $selectAtnP = "SELECT date,employeeCode,attendences
                     FROM employeeattend
                    WHERE employeeCode = '".$maRow['employeeCode']."'
                      AND date = '".$attendenceDate."-23'";
    $selectAtnPRes = mysql_query($selectAtnP);
    if($allRow = mysql_fetch_array($selectAtnPRes))
    {
	    $attendenceArr[$i]['attendences23'] = $allRow['attendences'];
    }
    
    $selectAtnP = "SELECT date,employeeCode,attendences
                     FROM employeeattend
                    WHERE employeeCode = '".$maRow['employeeCode']."'
                      AND date = '".$attendenceDate."-24'";
    $selectAtnPRes = mysql_query($selectAtnP);
    if($allRow = mysql_fetch_array($selectAtnPRes))
    {
	    $attendenceArr[$i]['attendences24'] = $allRow['attendences'];
    }
    
    $selectAtnP = "SELECT date,employeeCode,attendences
                     FROM employeeattend
                    WHERE employeeCode = '".$maRow['employeeCode']."'
                      AND date = '".$attendenceDate."-25'";
    $selectAtnPRes = mysql_query($selectAtnP);
    if($allRow = mysql_fetch_array($selectAtnPRes))
    {
	    $attendenceArr[$i]['attendences25'] = $allRow['attendences'];
    }
    
    $selectAtnP = "SELECT date,employeeCode,attendences
                     FROM employeeattend
                    WHERE employeeCode = '".$maRow['employeeCode']."'
                      AND date = '".$attendenceDate."-26'";
    $selectAtnPRes = mysql_query($selectAtnP);
    if($allRow = mysql_fetch_array($selectAtnPRes))
    {
	    $attendenceArr[$i]['attendences26'] = $allRow['attendences'];
    }
    
    $selectAtnP = "SELECT date,employeeCode,attendences
                     FROM employeeattend
                    WHERE employeeCode = '".$maRow['employeeCode']."'
                      AND date = '".$attendenceDate."-27'";
    $selectAtnPRes = mysql_query($selectAtnP);
    if($allRow = mysql_fetch_array($selectAtnPRes))
    {
	    $attendenceArr[$i]['attendences27'] = $allRow['attendences'];
    }
    
    $selectAtnP = "SELECT date,employeeCode,attendences
                     FROM employeeattend
                    WHERE employeeCode = '".$maRow['employeeCode']."'
                      AND date = '".$attendenceDate."-28'";
    $selectAtnPRes = mysql_query($selectAtnP);
    if($allRow = mysql_fetch_array($selectAtnPRes))
    {
	    $attendenceArr[$i]['attendences28'] = $allRow['attendences'];
    }
    
    $selectAtnP = "SELECT date,employeeCode,attendences
                     FROM employeeattend
                    WHERE employeeCode = '".$maRow['employeeCode']."'
                      AND date = '".$attendenceDate."-29'";
    $selectAtnPRes = mysql_query($selectAtnP);
    if($allRow = mysql_fetch_array($selectAtnPRes))
    {
	    $attendenceArr[$i]['attendences29'] = $allRow['attendences'];
    }
    
    $selectAtnP = "SELECT date,employeeCode,attendences
                     FROM employeeattend
                    WHERE employeeCode = '".$maRow['employeeCode']."'
                      AND date = '".$attendenceDate."-30'";
    $selectAtnPRes = mysql_query($selectAtnP);
    if($allRow = mysql_fetch_array($selectAtnPRes))
    {
	    $attendenceArr[$i]['attendences30'] = $allRow['attendences'];
    }
    
    $selectAtnP = "SELECT date,employeeCode,attendences
                     FROM employeeattend
                    WHERE employeeCode = '".$maRow['employeeCode']."'
                      AND date = '".$attendenceDate."-31'";
    $selectAtnPRes = mysql_query($selectAtnP);
    if($allRow = mysql_fetch_array($selectAtnPRes))
    {
	    $attendenceArr[$i]['attendences31'] = $allRow['attendences'];
    }
    
    //Main Monthly Attendences End
  	$i++;
  }
  
  
  include("./bottom.php");
  $smarty->assign('attendenceArr',$attendenceArr);
  $smarty->assign('atnArr',$atnArr);
  $smarty->assign('attendenceDate',$attendenceDate);
  $smarty->assign('loopEnd', $i-1);
  $smarty->assign('teachingNonteaching', $teachingNonteaching);
  $smarty->assign('teachingNonteachingCmbVal', $teachingNonteachingCmbVal);
  $smarty->assign('teachingNonteachingCmbTxt', $teachingNonteachingCmbTxt);
  //$smarty->assign('totalNumberWorkingDay',$totalNumberWorkingDay);
  $smarty->display('attendenceReportMonthlyEmp.tpl');
}
?>
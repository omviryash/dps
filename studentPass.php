<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	
	$grNo     = isset($_REQUEST['grNo']) && $_REQUEST['grNo'] != '' ? $_REQUEST['grNo'] : "";
	$classnew = isset($_REQUEST['classnew']) && $_REQUEST['classnew'] != '' ? $_REQUEST['classnew'] : "";
	$gender   = isset($_REQUEST['gender']) && $_REQUEST['gender'] != '' ? $_REQUEST['gender'] : "";
	$passFail = isset($_REQUEST['passFail']) && $_REQUEST['passFail'] != '' ? $_REQUEST['passFail'] : "";
	$section  = isset($_REQUEST['section']) && $_REQUEST['section'] != '' ? $_REQUEST['section'] : "";
	
	if(isset($_REQUEST['classnew']) && ($_REQUEST['classnew'] == 'Pre Nursery'))
	{
		$NewSec = 'Nursery';
	}
	elseif(isset($_REQUEST['classnew']) && ($_REQUEST['classnew'] == 'Nursery'))
	{
		$NewSec = 'PREP';
	}
	elseif(isset($_REQUEST['classnew']) && ($_REQUEST['classnew'] == 'PREP'))
	{
		$NewSec = 1;
	}
	elseif(isset($_REQUEST['classnew']) && ($_REQUEST['classnew'] == '10'))
	{
		$NewSec = '11 SCI.';
	}
	elseif(isset($_REQUEST['classnew']) && ($_REQUEST['classnew'] == '11 SCI.'))
	{
		$NewSec = '12 SCI.';
	}
	elseif(isset($_REQUEST['classnew']) && ($_REQUEST['classnew'] == '11 COM.'))
	{
		$NewSec = '12 COM.';
	}
	else
	{
		$NewSec   = $classnew + 1 ;
	}
	

  $selectGrno = "SELECT MAX(academicStartYear) AS academicStartYear,MAX(academicEndYear) AS academicEndYear
                   FROM nominalroll
                  WHERE grNo =".$grNo;
  $selectGrnoRes = mysql_query($selectGrno);
  if($dateRow = mysql_fetch_array($selectGrnoRes))
  {
  	$academicStartYear = $dateRow['academicStartYear'];
  	$academicEndYear   = $dateRow['academicEndYear'];
  }
  
  $academicStartNewYear = date('Y-m-d',strtotime('+1 year',strtotime($academicStartYear)));
  $academicEndNewYear   = date('Y-m-d',strtotime('+1 year',strtotime($academicEndYear)));
	
	if($_REQUEST['passFail'] == "Pass")
	{
		$selectCheck = "SELECT grNo
	                    FROM nominalroll
	                   WHERE academicStartYear = '".$academicStartNewYear."'
	                     AND academicEndYear = '".$academicEndNewYear."'
                       AND grNo = '".$grNo."'";
	  $selectCheckRes = mysql_query($selectCheck);
	  $genCount = mysql_num_rows($selectCheckRes);
	  
	  if($genCount == 0)
	  {
		  $insertNewRoll = "INSERT INTO nominalroll (grNo,academicStartYear,academicEndYear,class,section)
		                     VALUES ('".$grNo."','".$academicStartNewYear."','".$academicEndNewYear."',
		                             '".$NewSec."','".$section."')";
		  $insertNewRollRes = om_query($insertNewRoll);
		  if(!$insertNewRollRes)
		  {
		  	echo "insert fail";
		  }
		}
	}
	if($_REQUEST['passFail'] == "Fail")
	{
		$selectCheck = "SELECT grNo
	                    FROM nominalroll
	                   WHERE academicStartYear = '".$academicStartNewYear."'
	                     AND academicEndYear = '".$academicEndNewYear."'
                       AND grNo = '".$grNo."'";
	  $selectCheckRes = mysql_query($selectCheck);
	  $genCount = mysql_num_rows($selectCheckRes);
	  
	  if($genCount == 0)
	  {
		  $insertNewRoll = "INSERT INTO nominalroll (grNo,academicStartYear,academicEndYear,class,section)
		                     VALUES ('".$grNo."','".$academicStartNewYear."','".$academicEndNewYear."',
		                             '".$classnew."','".$section."')";
		  $insertNewRollRes = om_query($insertNewRoll);
		  if(!$insertNewRollRes)
		  {
		  	echo "insert fail";
		  }
		}
	}
	
//	$year              = date('Y');
//	$academicStartYear = "";
//	$i=0;
//	$selectMaxYear = "SELECT MAX(academicStartYear) AS academicStartYear,grNo
//	                    FROM nominalroll
//	                   WHERE grNo = ".$grNo;
//	$selectMaxYearRes = mysql_query($selectMaxYear);
//	while($maxYear = mysql_fetch_array($selectMaxYearRes))
//	{
//		$academicStartYear[$i] = substr($maxYear['academicStartYear'],0,4);
//		if($academicStartYear[$i] == $year)
//		{
//			echo $nominalDone = 1;
//		}
//		else
//		{
//			echo $nominalDone = 1;
//		}
//		$i++;
//	}
}
?>
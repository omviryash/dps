<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$filePath = '';
	$fileName = '';
	
	$grNo     = '';
	$class    = '';
	if(isset($_POST['submitBtn']))
	{
	  $uploaddir = dirname($_POST['filePath']);
	
	  $uploadfile = $uploaddir . basename($_FILES['fileName']['name']);
	  
	  $target_path = './uploadXls';
	  $target_path = $target_path ."/". basename( $_FILES['fileName']['name']);
	  $_FILES['fileName']['tmp_name']; // temp file
	  
	  $oldfile =  basename($_FILES['fileName']['name']);
	
	  // getting the extention
	
	  $pos = strpos($oldfile,".",0);
	  $ext = trim(substr($oldfile,$pos+1,strlen($oldfile))," ");
	  
	  if(move_uploaded_file($_FILES['fileName']['tmp_name'], $target_path)) 
	  {
	  	$row = 0;
	    $handle = fopen($target_path, "r");
	    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
	    {
	      if($row > 0)
	      {
	      	$selectGrNo = "SELECT grNo FROM studentmaster WHERE studentName = '".$data[6]."'";
	      	$selectGrNoRes = mysql_query($selectGrNo);
	      	if($grRow = mysql_fetch_array($selectGrNoRes))
	      	{
	      		$grNo = $grRow['grNo'];
	      	}
	      	
	      	if($data[4] == 'I')
          {
            $class = 1;
          }
          elseif($data[4] == 'II')
          {
          	$class = 2;
          }
          elseif($data[4] == 'III')
          {
          	$class = 3;
          }
          elseif($data[4] == 'IV')
          {
          	$class = 4;
          }
          elseif($data[4] == 'V')
          {
          	$class = 5;
          }
          elseif($data[4] == 'VI')
          {
          	$class = 6;
          }
          elseif($data[4] == 'VII')
          {
          	$class = 7;
          }
          elseif($data[4] == 'VIII')
          {
          	$class = 8;
          }
          elseif($data[4] == 'IX')
          {
          	$class = 9;
          }
          elseif($data[4] == 'X')
          {
          	$class = 10;
          }
          elseif($data[4] == 'XI Science')
          {
          	$class = 11 .' SCI.';
          }
          elseif($data[4] == 'XI Commerce')
          {
          	$class = 11 .' COM.';
          }
          elseif($data[4] == 'XII Science')
          {
          	$class = 12 .' SCI.';
          }
          elseif($data[4] == 'XII Commerce')
          {
          	$class = 12 .' COM.';
          }
          else
          {
          	$class = $data[4];
          }
          
					$section           = substr($data[5], -1);
					$rollNo            = $data[7];
					$feeGroup          = addslashes($data[8]);
					$libraryMemberType = addslashes($data[9]);
					$busRoute          = addslashes($data[10]);
					$busStop           = addslashes($data[11]);
					$subjectGroup      = addslashes($data[12]);
					$coScholasticGroup = addslashes($data[13]);
					$club              = addslashes($data[14]);
					$academicStartYear = substr($data[16], 0, 4).'-04-01';
					$academicEndYear   = substr($data[16], 5, 4).'-03-31';
					$boardRegistrionId = addslashes($data[19]);
					if (!isset($data[20]))
					{
					  $boardRollNo       = '';
				  }
				  else
				  {
				  	$boardRollNo       = addslashes($data[20]);
				  }
					
					$uploadStudentMaster = "INSERT INTO nominalroll
	                                       (grNo,academicStartYear,academicEndYear,class,section,rollNo,feeGroup,
	                                        libraryMemberType,busRoute,busStop,subjectGroup,coScholasticGroup,clubTerm1,boardRegistrionId,
	                                        boardRollNo)
	      	                        VALUES ('".$grNo."','".$academicStartYear."','".$academicEndYear."','".$class."','".$section."',
	      	                                ".$rollNo.",'".$feeGroup."','".$libraryMemberType."','".$busRoute."','".$busStop."','".$subjectGroup."',
	      	                                '".$coScholasticGroup."','".$club."','".$boardRegistrionId."','".$boardRollNo."')";
	      	om_query($uploadStudentMaster);
		    }
	      $row++;
	    }
	  } 
	  else
	  {
	    echo "There was an error uploading the file, please try again!";
	  }
	}
}
?>
<body>
<H1><CENTER><FONT color="red">Nominal Roll</FONT></CENTER></H1><HR>
<CENTER>
  <FORM enctype="multipart/form-data" action="" name="nominalRoll" method="POST"> 
	<a href="index.php">Home</a>
	<br /><br />
  <INPUT size="60" type="hidden" name="filePath"><BR>
  File Name: 
  <INPUT name="fileName" type="file" onChange="document.nominalRoll.filePath.value=document.nominalRoll.fileName.value;">
  <INPUT type="submit" name="submitBtn" value="Submit File"> 
</FORM>
</CENTER>
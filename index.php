<?php
include("./include/config.inc.php");
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
  include("./bottom.php");
  $smarty->display('index.tpl');
}
?>
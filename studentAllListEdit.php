<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$stdArray = array();
	$notFound = 'Record Not Found';
	$i = 0;
	if(isset($_REQUEST['grNo']))
	{
	  $selectStd = "SELECT studentMasterId,studentName,fatherName,grNo,activated,dateOfBirth,joinedInClass,joiningDate,gender,bloodGroup,house.houseName,
	                       currentAddress,currentState,currentAddressPin,residencePhone1,residencePhone2,smsMobile,studentEmail,
	                       permanentAddress,permanentState,permanentPIN,religion,previousSchool,aadhaar
	                  FROM studentmaster
	             LEFT JOIN house ON house.houseId = studentmaster.houseId
	                 WHERE grNo = ".$_REQUEST['grNo']."";
	  $selectStdRes = mysql_query($selectStd);
	  while($stdRow = mysql_fetch_array($selectStdRes))
	  {
	  	$stdArray[$i]['studentMasterId']   = $stdRow['studentMasterId'];
	  	$stdArray[$i]['studentName']       = $stdRow['studentName'];
	  	$stdArray[$i]['fatherName']        = $stdRow['fatherName'];
	  	$stdArray[$i]['grNo']              = $stdRow['grNo'];
	  	$stdArray[$i]['activated']         = $stdRow['activated'];
	  	$stdArray[$i]['dateOfBirth']       = $stdRow['dateOfBirth'];
	  	$stdArray[$i]['joinedInClass']     = $stdRow['joinedInClass'];
	  	$stdArray[$i]['joiningDate']       = $stdRow['joiningDate'];
	  	$stdArray[$i]['gender']            = $stdRow['gender'];
	  	$stdArray[$i]['bloodGroup']        = $stdRow['bloodGroup'];
	  	$stdArray[$i]['houseName']         = $stdRow['houseName'];
	  	$stdArray[$i]['currentAddress']    = $stdRow['currentAddress'];
      $stdArray[$i]['currentState']      = $stdRow['currentState'];
      $stdArray[$i]['currentAddressPin'] = $stdRow['currentAddressPin'];
	  	$stdArray[$i]['residencePhone1']   = $stdRow['residencePhone1'];
	  	$stdArray[$i]['residencePhone2']   = $stdRow['residencePhone2'];
		$stdArray[$i]['smsMobile']	       = $stdRow['smsMobile'];
	  	$stdArray[$i]['studentEmail']      = $stdRow['studentEmail'];
	  	$stdArray[$i]['permanentAddress']  = $stdRow['permanentAddress'];
	  	$stdArray[$i]['permanentState']    = $stdRow['permanentState'];
      $stdArray[$i]['permanentPIN']      = $stdRow['permanentPIN'];
	  	$stdArray[$i]['religion']          = $stdRow['religion'];
	  	$stdArray[$i]['previousSchool']    = $stdRow['previousSchool'];
	  	$stdArray[$i]['aadhaar']    = $stdRow['aadhaar'];
	  	$i++;
	  }
	}
	else
	{
		$notFound = 'Record Not Found';
	}
	include("./bottom.php");
	$smarty->assign('stdArray',$stdArray);
	$smarty->assign('notFound',$notFound);
  $smarty->display('studentAllListEdit.tpl');
}
?>

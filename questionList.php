<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	if(isset($_REQUEST['startYear']))
  {
	  $academicStartYear = $_REQUEST['startYear'];
	  $academicEndYear   = $_REQUEST['startYear'] + 1;
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y');
	  	$nextYear          = date('Y') + 1;
	  	$academicEndYear   = $nextYear;
		}
		else
		{
			$prevYear          = date('Y') - 1;
			$academicStartYear = $prevYear;
	  	$academicEndYear   = date('Y');
		}
	}
	
	$subjectAltId = isset($_REQUEST['subjectAltId']) && $_REQUEST['subjectAltId'] != '' ? $_REQUEST['subjectAltId'] : '0_0_0';
	
	$subjectAltIdExp = explode("_",$subjectAltId);
	$subjectAltIdExp[0];
	$subjectAltIdExp[1];
	$subjectAltIdExp[2];
	
	if(isset($_REQUEST['class']))
	{
	  $class           = isset($_REQUEST['class']) && $_REQUEST['class'] != '' ? $_REQUEST['class'] : 0;
		$subjectMasterId = isset($_REQUEST['subjectMasterId']) && $_REQUEST['subjectMasterId'] != '' ? $_REQUEST['subjectMasterId'] : 0;
	}
	else
	{
		$class           = $subjectAltIdExp[0];
		$subjectMasterId = $subjectAltIdExp[2];
	}
	
	$empArray = array();
  $j = 0;
  $classArray = array();
  $i = 0;
  $selectClass = "SELECT subjectteacherallotment.subjectTeacherAllotmentId,employeemaster.loginId,
                         subjectteacherallotment.academicStartYear,subjectteacherallotment.academicEndYear,subjectteacherallotment.subjectMasterId,
                         subjectteacherallotment.class,subjectteacherallotment.section,subjectteacherallotment.period,
                         employeemaster.name,subjectmaster.subjectName
                    FROM subjectteacherallotment
               LEFT JOIN employeemaster ON employeemaster.employeeMasterId = subjectteacherallotment.employeeMasterId
               LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = subjectteacherallotment.subjectMasterId
                   WHERE employeemaster.loginId = '".$_SESSION['s_activName']."'
                     AND subjectteacherallotment.academicStartYear = '".$academicStartYear."-04-01'
	                   AND subjectteacherallotment.academicEndYear = '".$academicEndYear."-03-31'";
  $selectClassRes = mysql_query($selectClass);
  $count = mysql_num_rows($selectClassRes);
  while($classRow = mysql_fetch_array($selectClassRes))
  {
  	$classArray[$i]['subjectTeacherAllotmentId'] = $classRow['subjectTeacherAllotmentId'];
  	$classArray[$i]['name']                      = $classRow['name'];
  	$classArray[$i]['academicStartYear']         = substr($classRow['academicStartYear'],0,4);
  	$classArray[$i]['academicEndYear']           = substr($classRow['academicEndYear'],2,2);
    
  	$classArray[$i]['class']           = $classRow['class'];
  	$classArray[$i]['section']         = $classRow['section'];
  	$classArray[$i]['subjectMasterId'] = $classRow['subjectMasterId'];
  	$classArray[$i]['subjectName']     = $classRow['subjectName'];
  	$classArray[$i]['period']          = $classRow['period'];
  	$i++;
  
	  if($class == 0 || $subjectMasterId == 0 || $subjectAltId == '0_0_0')
	  {
		  $selectEmp = "SELECT DISTINCT onlinetest.onlineTestId,onlinetest.qNo,onlinetest.class,subjectmaster.subjectName,
		                       onlinetest.question,onlinetest.option1,onlinetest.option2,onlinetest.subjectMasterId,
		                       onlinetest.option3,onlinetest.option4,onlinetest.answer
		                  FROM onlinetest
		             LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = onlinetest.subjectMasterId
		             LEFT JOIN classmaster ON classmaster.className = onlinetest.class
		                 WHERE 1 = 1
		                   AND onlinetest.class = '".$classRow['class']."'
			                 AND onlinetest.subjectMasterId = '".$classRow['subjectMasterId']."'
		              ORDER BY onlinetest.onlineTestId DESC,subjectmaster.subjectName";
		  $selectEmpRes = mysql_query($selectEmp);
		  while($empRow = mysql_fetch_array($selectEmpRes))
		  {
		  	$empArray[$j]['onlineTestId'] = $empRow['onlineTestId'];
		  	$empArray[$j]['qNo']          = $empRow['qNo'];
		  	$empArray[$j]['class']        = $empRow['class'];
		  	$empArray[$j]['subjectName']  = $empRow['subjectName'];
		  	$empArray[$j]['question']     = $empRow['question'];
		  	$empArray[$j]['option1']      = $empRow['option1'];
		  	$empArray[$j]['option2']      = $empRow['option2'];
		  	$empArray[$j]['option3']      = $empRow['option3'];
		  	$empArray[$j]['option4']      = $empRow['option4'];
		  	$empArray[$j]['answer']       = $empRow['answer'];
		  	$j++;
		  }
		}
	}
	
	if(isset($_REQUEST['subjectAltId']) &&  $subjectAltId != '0_0_0')
	{
		$selectEmp = "SELECT DISTINCT onlinetest.onlineTestId,onlinetest.qNo,onlinetest.class,subjectmaster.subjectName,
	                       onlinetest.question,onlinetest.option1,onlinetest.option2,onlinetest.subjectMasterId,
	                       onlinetest.option3,onlinetest.option4,onlinetest.answer
	                  FROM onlinetest
	             LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = onlinetest.subjectMasterId
	             LEFT JOIN classmaster ON classmaster.className = onlinetest.class
	                 WHERE 1 = 1
	                   AND onlinetest.class = '".$class."'
			               AND onlinetest.subjectMasterId = '".$subjectMasterId."'
	              ORDER BY onlinetest.onlineTestId DESC,subjectmaster.subjectName";
	  $selectEmpRes = mysql_query($selectEmp);
	  while($empRow = mysql_fetch_array($selectEmpRes))
	  {
	  	$empArray[$j]['onlineTestId'] = $empRow['onlineTestId'];
	  	$empArray[$j]['qNo']          = $empRow['qNo'];
	  	$empArray[$j]['class']        = $empRow['class'];
	  	$empArray[$j]['subjectName']  = $empRow['subjectName'];
	  	$empArray[$j]['question']     = $empRow['question'];
	  	$empArray[$j]['option1']      = $empRow['option1'];
	  	$empArray[$j]['option2']      = $empRow['option2'];
	  	$empArray[$j]['option3']      = $empRow['option3'];
	  	$empArray[$j]['option4']      = $empRow['option4'];
	  	$empArray[$j]['answer']       = $empRow['answer'];
	  	$j++;
	  }
	}
  
  if($count == 0)
  {
    if($class > 0 && $subjectMasterId == 0)
    {
      $newQuery = "AND onlinetest.class = '".$class."'";
    }
  	elseif($subjectMasterId > 0 && $class == 0)
		{
			$newQuery = "AND onlinetest.subjectMasterId = '".$subjectMasterId."'";
		}
  	elseif($class > 0 && $subjectMasterId > 0)
		{
			$newQuery = "AND onlinetest.class = '".$class."'
			             AND onlinetest.subjectMasterId = '".$subjectMasterId."'";
		}
		else
		{
			$newQuery = "AND 1 = 1";
		}
		
	  $selectEmp = "SELECT DISTINCT onlinetest.onlineTestId,onlinetest.qNo,onlinetest.class,subjectmaster.subjectName,
	                       onlinetest.question,onlinetest.option1,onlinetest.option2,onlinetest.subjectMasterId,
	                       onlinetest.option3,onlinetest.option4,onlinetest.answer
	                  FROM onlinetest
	             LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = onlinetest.subjectMasterId
	             LEFT JOIN classmaster ON classmaster.className = onlinetest.class
	                 WHERE 1 = 1
	                 ".$newQuery."
	              ORDER BY onlinetest.onlineTestId DESC,subjectmaster.subjectName";
	  $selectEmpRes = mysql_query($selectEmp);
	  while($empRow = mysql_fetch_array($selectEmpRes))
	  {
	  	$empArray[$j]['onlineTestId'] = $empRow['onlineTestId'];
	  	$empArray[$j]['qNo']          = $empRow['qNo'];
	  	$empArray[$j]['class']        = $empRow['class'];
	  	$empArray[$j]['subjectName']  = $empRow['subjectName'];
	  	$empArray[$j]['question']     = $empRow['question'];
	  	$empArray[$j]['option1']      = $empRow['option1'];
	  	$empArray[$j]['option2']      = $empRow['option2'];
	  	$empArray[$j]['option3']      = $empRow['option3'];
	  	$empArray[$j]['option4']      = $empRow['option4'];
	  	$empArray[$j]['answer']       = $empRow['answer'];
	  	$j++;
	  }
  }
  
  $scdArray = array();
  $f = 0;
	$selectId = "SELECT employeeMasterId
                 FROM employeemaster
                WHERE loginId = '".$_SESSION['s_activName']."'";
  $selectIdRes = mysql_query($selectId);
  if($subRow = mysql_fetch_array($selectIdRes))
  {
  	$selectSub = "SELECT subjectteacherallotment.class,subjectteacherallotment.section,subjectteacherallotment.subjectMasterId,subjectmaster.subjectName
                    FROM subjectteacherallotment
               LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = subjectteacherallotment.subjectMasterId
                   WHERE subjectteacherallotment.employeeMasterId = '".$subRow['employeeMasterId']."'
                     AND subjectteacherallotment.academicStartYear = '".$academicStartYear."-04-01'
                     AND subjectteacherallotment.academicEndYear = '".$academicEndYear."-03-31'";
	  $selectSubRes = mysql_query($selectSub);
	  while($subRow = mysql_fetch_array($selectSubRes))
	  {
	  	$classTeacherSubjectClass    = $subRow['class'];
	  	$classTeacherSubjectsection  = $subRow['section'];
	  	$classTeachersubjectMasterId = $subRow['subjectMasterId'];
	  	$subjectName = $subRow['subjectName'];
		  
	  	$scdArray['subjectAltId'][$f] = $subRow['class'].'_'.$subRow['section'].'_'.$subRow['subjectMasterId'];
	  	$scdArray['subjectDtl'][$f]   = $subRow['class'].'_'.$subRow['section'].'_'.$subRow['subjectName'];
	  	$f++;
	  }
  }
  
  $c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  $k = 0;
  $clubArr = array();
  $selectClub = "SELECT subjectMasterId,subjectName
                   FROM subjectmaster
                  WHERE subjectType = 'Main' OR subjectType = 'Optional'
               ORDER BY subjectName";
  $selectClubRes = mysql_query($selectClub);
  while($clubRow = mysql_fetch_array($selectClubRes))
  {
  	$clubArr['subjectMasterId'][$k]   = $clubRow['subjectMasterId'];
  	$clubArr['subjectName'][$k]       = $clubRow['subjectName'];
  	$k++;
  }
  
  include("./bottom.php");
  $smarty->assign('empArray',$empArray);
  $smarty->assign('clubArr',$clubArr);
  $smarty->assign('cArray',$cArray);
  $smarty->assign('class',$class);
  $smarty->assign('scdArray',$scdArray);
  $smarty->assign('subjectMasterId',$subjectMasterId);
  $smarty->assign('subjectAltId',$subjectAltId);
  $smarty->display('questionList.tpl');  
}
?>
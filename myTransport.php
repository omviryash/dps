<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] != 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	if(isset($_REQUEST['startYear']))
  {
	  $academicStartYear = $_REQUEST['startYear'];
	  $academicEndYear   = $_REQUEST['startYear'] + 1;
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y');
	  	$nextYear            = date('Y') + 1;
	  	$academicEndYear   = $nextYear;
		}
		else
		{
			$prevYear            = date('Y') - 1;
			$academicStartYear = $prevYear;
	  	$academicEndYear   = date('Y');
		}
	}
	
	$nominalArr = array();
  $i = 0;
  $selectNominal = "SELECT nominalRollId,nominalroll.grNo,nominalroll.academicStartYear,nominalroll.academicEndYear,class,
                           section,rollNo,feeGroup,libraryMemberType,libraryMemberType,studentmaster.studentName,
                           activated,studentmaster.studentLoginId,studentmaster.parentLoginId,
                           vehiclemaster.vehicleNo,vehiclemasterD.vehicleNo AS vehicleNoD,
                           routemaster.routeName,routemasterD.routeName AS routeNameD,busstopmaster.busStop
                      FROM nominalroll
                 LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
                 LEFT JOIN vehiclemaster ON vehiclemaster.vehicleMasterId = nominalroll.busArrival
                 LEFT JOIN vehiclemaster AS vehiclemasterD ON vehiclemasterD.vehicleMasterId = nominalroll.busDeparture
                 LEFT JOIN routemaster ON routemaster.routeMasterId = nominalroll.routeArrival
                 LEFT JOIN routemaster AS routemasterD ON routemasterD.routeMasterId = nominalroll.routeDeparture
                 LEFT JOIN busstopmaster ON busstopmaster.busStopMasterId = nominalroll.busStopMasterId
                     WHERE 1 = 1
                       AND academicStartYear = '".$academicStartYear."-04-01'
                       AND academicEndYear = '".$academicEndYear."-03-31'
                       AND studentmaster.studentLoginId = '".$_SESSION['s_activName']."' 
                           OR studentmaster.parentLoginId = '".$_SESSION['s_activName']."'
                       AND studentmaster.activated = 'Y'
                  ORDER BY nominalroll.rollNo";
  $selectNominalRes = mysql_query($selectNominal);
  while($nominalRow = mysql_fetch_array($selectNominalRes))
  {
    $nominalArr[$i]['nominalRollId']      = $nominalRow['nominalRollId'];
    $nominalArr[$i]['grNo']               = $nominalRow['grNo'];
    $nominalArr[$i]['academicYear']       = substr($nominalRow['academicStartYear'],0,4).'-'.substr($nominalRow['academicEndYear'],2,2);
    $nominalArr[$i]['class']              = $nominalRow['class'];
    $nominalArr[$i]['section']            = $nominalRow['section'];
    $nominalArr[$i]['rollNo']             = $nominalRow['rollNo'];
    $nominalArr[$i]['studentName']        = $nominalRow['studentName'];
    $nominalArr[$i]['activated']          = $nominalRow['activated'];
    $nominalArr[$i]['vehicleNo']          = $nominalRow['vehicleNo'];
    $nominalArr[$i]['vehicleNoD']         = $nominalRow['vehicleNoD'];
    $nominalArr[$i]['routeName']          = $nominalRow['routeName'];
    $nominalArr[$i]['routeNameD']         = $nominalRow['routeNameD'];
    $nominalArr[$i]['busStop']            = $nominalRow['busStop'];
    $i++;
  }
  
  include("./bottom.php");
  $smarty->assign('nominalArr',$nominalArr);
  $smarty->assign('academicStartYear',$academicStartYear);
  $smarty->assign('academicEndYear',$academicEndYear);
  $smarty->display('myTransport.tpl');  
}
?>
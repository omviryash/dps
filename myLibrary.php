<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] != 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$today = date('Y-m-d');
	$attendenceArr       = array();
	$markArr             = array();
	$attendenceEndDate   = "";
	$attendenceStartDate = "";
	$academicStartYear   = "";
	$academicEndYear     = "";
  
	$totalNumberWorkingDay = 0;
	
	if(isset($_REQUEST['startYear']))
  {
  	$academicStartYear = $_REQUEST['startYear']."-04-01";
  	$nextYear          = $_REQUEST['startYear'] + 1;
  	$academicEndYear   = $nextYear."-03-31";
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y')."-04-01";
	  	$nextYear          = date('Y') + 1;
	  	$academicEndYear   = $nextYear."-03-31";
		}
		else
		{
			$prevYear          = date('Y') - 1;
			$academicStartYear = $prevYear."-04-01";
	  	$academicEndYear   = date('Y')."-03-31";
		}
	}
	
	if(isset($_REQUEST['startYear']))
  {
	  $attendenceStartDate = $_REQUEST['startYear'];
	  $attendenceEndDate   = $_REQUEST['startYear'] + 1;
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$attendenceStartDate = date('Y')."-04-01";
	  	$nextYear            = date('Y') + 1;
	  	$attendenceEndDate   = $nextYear;
		}
		else
		{
			$prevYear            = date('Y') - 1;
			$attendenceStartDate = $prevYear."-04-01";
	  	$attendenceEndDate   = date('Y');
		}
	}
		
	$class   = isset($_REQUEST['class']) ? $_REQUEST['class'] : 0;
	$section = isset($_REQUEST['classSection']) ? $_REQUEST['classSection'] : 0;
	
  $i = 0;
  $selectNominal = "SELECT studentmaster.grNo,studentmaster.activated,
                           studentmaster.studentLoginId,studentmaster.parentLoginId
                      FROM studentmaster
                     WHERE 1 = 1
                       AND studentmaster.studentLoginId = '".$_SESSION['s_activName']."' 
                           OR studentmaster.parentLoginId = '".$_SESSION['s_activName']."'
                       AND studentmaster.activated = 'Y'";
  $selectNominalRes = mysql_query($selectNominal);
  while($nominalRow = mysql_fetch_array($selectNominalRes))
  {
    $g = 0;
    $selectAtnP = "SELECT librarytransaction.grNo,librarytransaction.bookAccessionNo,
                          DATE_FORMAT(librarytransaction.issueDate, '%d-%m-%Y') AS issueDate,
                          DATE_FORMAT(librarytransaction.returnableDate, '%d-%m-%Y') AS returnableDate,
                          librarytransaction.fine,librarytransaction.finePay,
                          librarytransaction.returnBook,bookmaster.bookTitle
                     FROM librarytransaction
                LEFT JOIN bookmaster ON bookmaster.bookAccessionNo = librarytransaction.bookAccessionNo
                    WHERE librarytransaction.grNo = '".$nominalRow['grNo']."'
                      AND librarytransaction.issueDate >= '".$attendenceStartDate."-04-01'
                      AND librarytransaction.issueDate <= '".$attendenceEndDate."-03-31'
                 ORDER BY librarytransaction.issueDate DESC";
    $selectAtnPRes = mysql_query($selectAtnP);
    while($markRow = mysql_fetch_array($selectAtnPRes))
    {
    	$markArr[$i]['bookAccessionNo'] = $markRow['bookAccessionNo'];
    	$markArr[$g]['issueDate']       = $markRow['issueDate'];
    	$markArr[$g]['returnableDate']  = $markRow['returnableDate'];
    	$markArr[$g]['returnBook']      = $markRow['returnBook'];
    	$markArr[$g]['bookTitle']       = $markRow['bookTitle'];
    	
    	$ts1 = strtotime($today);
			$ts2 = strtotime(date('Y-m-d',strtotime('+7 days',strtotime($markRow['issueDate']))));
			
			$seconds_diff = $ts1 - $ts2;
			
			$fine = floor($seconds_diff/3600/24);
			
			$newFine = $fine/7;
			
			$newFineAll = explode('.',$newFine);
			$newFineAll[0];
			if($newFineAll[0] > 0 && $markRow['returnBook'] == 'No')
			{
			  $markArr[$g]['fine'] = $newFineAll[0] * 7;
			}
			else
			{
				$markArr[$g]['fine'] = $maRow['fine'];
			}
			if($markArr[$g]['fine'] == 0)
			{
			  $markArr[$g]['finePay'] = '-';
			}
			elseif($markArr[$g]['returnBook'] == 'No')
			{
			  $markArr[$g]['finePay'] = 'Panding';
			}
			else
			{
				$markArr[$g]['finePay'] = $maRow['finePay'];
			}
    	$g++;
    }
  	$i++;
  }
  
  $c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  $secArrOut[0] = 'A';
  $secArrOut[1] = 'B';
  $secArrOut[2] = 'C';
  $secArrOut[3] = 'D';
  
  $studStartYear = substr($academicStartYear,0,4);
  $studEndYear   = substr($academicEndYear,2,2);
  include("./bottom.php");
  $smarty->assign('attendenceArr',$attendenceArr);
  $smarty->assign('studStartYear',$studStartYear);
  $smarty->assign('studEndYear',$studEndYear);
  $smarty->assign('markArr',$markArr);
  $smarty->assign('cArray',$cArray);
  $smarty->assign('class',$class);
  $smarty->assign('section',$section);
  $smarty->assign('secArrOut',$secArrOut);
  $smarty->assign('totalNumberWorkingDay',$totalNumberWorkingDay);
  $smarty->assign('attendenceStartDate',$attendenceStartDate);
  $smarty->assign('attendenceEndDate',$attendenceEndDate);
  $smarty->display('myLibrary.tpl');
}
?>
<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	if(isset($_REQUEST['startYear']))
  {
	  $academicStartYear = $_REQUEST['startYear'];
	  $academicEndYear   = $_REQUEST['startYear'] + 1;
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y');
	  	$nextYear            = date('Y') + 1;
	  	$academicEndYear   = $nextYear;
		}
		else
		{
			$prevYear            = date('Y') - 1;
			$academicStartYear = $prevYear;
	  	$academicEndYear   = date('Y');
		}
	}
	
	$class            = isset($_REQUEST['class']) ? $_REQUEST['class'] : 0;
	$section          = isset($_REQUEST['classSection']) ? $_REQUEST['classSection'] : 0;
	$scheduleMasterId = isset($_REQUEST['scheduleMasterId']) ? $_REQUEST['scheduleMasterId'] : 0;
	
	$nominalArr = array();
  $i = 0;
  $selectNominal = "SELECT nominalRollId,nominalroll.grNo,academicStartYear,academicEndYear,class,
                           section,rollNo,feeGroup,libraryMemberType,libraryMemberType,studentmaster.studentName,
                           busRoute,busStop,subjectGroup,coScholasticGroup,clubTerm1,clubTerm2,
                           boardRegistrionId,boardRollNo,activated,studentmaster.studentMasterId,
                           studentmaster.fatherMobile,studentmaster.studentMasterId,currentAddress
                      FROM nominalroll
                 LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
                     WHERE nominalroll.class = '".$class."'
                       AND nominalroll.section = '".$section."'
                       AND nominalroll.academicStartYear = '".$academicStartYear."-04-01'
                       AND nominalroll.academicEndYear = '".$academicEndYear."-03-31'
                       AND studentmaster.activated = 'Y'
                  ORDER BY nominalroll.rollNo";
  $selectNominalRes = mysql_query($selectNominal);
  while($nominalRow = mysql_fetch_array($selectNominalRes))
  {
  	$nominalArr[$i]['totalMarks'] = 0;
    $nominalArr[$i]['studentMasterId']    = $nominalRow['studentMasterId'];
    $nominalArr[$i]['nominalRollId']      = $nominalRow['nominalRollId'];
    $nominalArr[$i]['grNo']               = $nominalRow['grNo'];
    $nominalArr[$i]['currentAddress']     = $nominalRow['currentAddress'];
    $nominalArr[$i]['fatherMobile']       = $nominalRow['fatherMobile'];
    $nominalArr[$i]['academicYear']       = substr($nominalRow['academicStartYear'],0,4).'-'.substr($nominalRow['academicEndYear'],0,4);
    $nominalArr[$i]['class']              = $nominalRow['class'];
    $nominalArr[$i]['section']            = $nominalRow['section'];
    $nominalArr[$i]['rollNo']             = $nominalRow['rollNo'];
    $nominalArr[$i]['feeGroup']           = $nominalRow['feeGroup'];
    $nominalArr[$i]['libraryMemberType']  = $nominalRow['libraryMemberType'];
    $nominalArr[$i]['libraryMemberType']  = $nominalRow['libraryMemberType'];
    $nominalArr[$i]['studentName']        = $nominalRow['studentName'];
    $nominalArr[$i]['busRoute']           = $nominalRow['busRoute'];
    $nominalArr[$i]['busStop']            = $nominalRow['busStop'];
    $nominalArr[$i]['subjectGroup']       = $nominalRow['subjectGroup'];
    $nominalArr[$i]['coScholasticGroup']  = $nominalRow['coScholasticGroup'];
    $nominalArr[$i]['clubTerm1']          = $nominalRow['clubTerm1'];
    $nominalArr[$i]['clubTerm2']          = $nominalRow['clubTerm2'];
    $nominalArr[$i]['boardRegistrionId']  = $nominalRow['boardRegistrionId'];
    $nominalArr[$i]['boardRollNo']        = $nominalRow['boardRollNo'];
    $nominalArr[$i]['activated']          = $nominalRow['activated'];
    
    $selectClass = "SELECT myonlinetest.myOnlineTestId,onlinetest.question,onlinetest.option1,onlinetest.option2,onlinetest.class,
	                         onlinetest.option3,onlinetest.option4,onlinetest.answer,myonlinetest.answer AS myAnswer
	                    FROM myonlinetest
	               LEFT JOIN onlinetest ON onlinetest.onlineTestId = myonlinetest.onlineTestId
	                   WHERE myonlinetest.scheduleMasterId = ".$scheduleMasterId."
	                     AND myonlinetest.grNo = ".$nominalRow['grNo']."";
	  $selectClassRes = mysql_query($selectClass);
	  $totalQuestion = mysql_num_rows($selectClassRes);
	  while($classRow = mysql_fetch_array($selectClassRes))
	  {
	  	$nominalArr[$i]['myOnlineTestId'] = $classRow['myOnlineTestId'];
	  	$nominalArr[$i]['class']          = $classRow['class'];
	  	$nominalArr[$i]['question']       = $classRow['question'];
	  	$nominalArr[$i]['option1']        = $classRow['option1'];
	  	$nominalArr[$i]['option2']        = $classRow['option2'];
	  	$nominalArr[$i]['option3']        = $classRow['option3'];
	  	$nominalArr[$i]['option4']        = $classRow['option4'];
	  	$nominalArr[$i]['answer']         = $classRow['answer'];
	  	$nominalArr[$i]['myAnswer']       = $classRow['myAnswer'];
	  	if($classRow['myAnswer'] == $classRow['answer'])
	  	{
	  	  $nominalArr[$i]['marks']       = 1;
	  	}
	  	else
	  	{
	  		$nominalArr[$i]['marks']       = 0;
	  	}
	  	
	  	$nominalArr[$i]['totalMarks'] += $nominalArr[$i]['marks'];
	  }
    
    $i++;
  }
  
  $c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  $secArrOut[0] = 'A';
  $secArrOut[1] = 'B';
  $secArrOut[2] = 'C';
  $secArrOut[3] = 'D';
  
 $d=0;
	$sArray = array();
	$selectClass = "SELECT onlineschedule.scheduleMasterId,onlineschedule.class,subjectmaster.subjectName,
	                       DATE_FORMAT(onlineschedule.scheduleDate, '%d-%m-%Y') AS scheduleDate
                    FROM onlineschedule
               LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = onlineschedule.subjectMasterId
                   WHERE class = '".$class."'";
	$selectClassRes = mysql_query($selectClass);
	if(!$selectClassRes)
	{
		echo mysql_error();
		die;
	}
	while($scheRow = mysql_fetch_array($selectClassRes))
	{
	  $sArray['scheduleMasterId'][$d] = $scheRow['scheduleMasterId'];
	  $sArray['scheduleDate'][$d]     = $scheRow['scheduleDate'].'____'.$scheRow['subjectName'];
	  $d++;
	}
	
  include("./bottom.php");
  $smarty->assign('nominalArr',$nominalArr);
  $smarty->assign('cArray',$cArray);
  $smarty->assign('sArray',$sArray);
  $smarty->assign('class',$class);
  $smarty->assign('section',$section);
  $smarty->assign('secArrOut',$secArrOut);
  $smarty->assign('academicStartYear',$academicStartYear);
  $smarty->assign('academicEndYear',$academicEndYear);
  $smarty->assign('scheduleMasterId',$scheduleMasterId);
  $smarty->display('onlineTestResult.tpl');  
}
?>
<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$attendenceArr       = array();
	$atnArr              = array();
	$attendenceEndDate   = "";
	$attendenceStartDate = "";
	$attendenceStartDateSelected = "";
	$academicStartYear   = "";
	$academicEndYear     = "";
  
	//$totalNumberWorkingDay = 0;
	
	if(isset($_REQUEST['startYear']))
  {
  	$academicStartYear = $_REQUEST['startYear']."-04-01";
  	$nextYear          = $_REQUEST['startYear'] + 1;
  	$academicEndYear   = $nextYear."-03-31";
	}
	
	if(isset($_REQUEST['startYear']))
  {
	  $attendenceStartDateSelected = $_REQUEST['startYear'];
	  $attendenceStartDate = $_REQUEST['startYear'];
	  $attendenceEndDate   = $_REQUEST['startYear'] + 1;
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$attendenceStartDateSelected = date('Y');
	  	$attendenceStartDate = date('Y')."-04-01";
	  	$nextYear            = date('Y') + 1;
	  	$attendenceEndDate   = $nextYear;
		}
		else
		{
			$prevYear            = date('Y') - 1;
			$attendenceStartDateSelected = $prevYear;
			$attendenceStartDate = $prevYear."-04-01";
	  	$attendenceEndDate   = date('Y');
		}
	}
		
	$class   = isset($_REQUEST['class']) ? $_REQUEST['class'] : 0;
	$section = isset($_REQUEST['classSection']) ? $_REQUEST['classSection'] : 0;
	
	$selectId = "SELECT employeeMasterId
                 FROM employeemaster
                WHERE loginId = '".$_SESSION['s_activName']."'";
  $selectIdRes = mysql_query($selectId);
  if($idRow = mysql_fetch_array($selectIdRes))
  {
  	$selectClass = "SELECT class,section
	                    FROM classteacherallotment
	                   WHERE employeeMasterId = '".$idRow['employeeMasterId']."'
	                     AND academicStartYear = '".$academicStartYear."'
                       AND academicEndYear = '".$academicEndYear."'";
	  $selectClassRes = mysql_query($selectClass);
	  if($classRow = mysql_fetch_array($selectClassRes))
	  {
	  	if($class == 0)
	  	{
		  	$class   = $classRow['class'];
		  	$section = $classRow['section'];
		  }
		}
	
	  $i = 0;
	  $selectAttendM = "SELECT studentmaster.grNo,studentmaster.studentName,studentmaster.activated,nominalroll.class,
	                           nominalroll.section,nominalroll.rollNo
	                      FROM nominalroll
	                 LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
	                     WHERE nominalroll.class = '".$class."'
	                       AND nominalroll.section = '".$section."'
	                       AND nominalroll.academicStartYear = '".$academicStartYear."'
	                       AND nominalroll.academicEndYear = '".$academicEndYear."'
	                       AND studentmaster.activated = 'Y'";
	  $selectAttendMRes = mysql_query($selectAttendM);
	  while($maRow = mysql_fetch_array($selectAttendMRes))
	  {
	  	$attendenceArr[$i]['rollNo']      = $maRow['rollNo'];
	  	$attendenceArr[$i]['grNo']        = $maRow['grNo'];
	  	$attendenceArr[$i]['studentName'] = $maRow['studentName'];
	  	$attendenceArr[$i]['class']       = $maRow['class'];
	  	$attendenceArr[$i]['section']     = $maRow['section'];
	  	
	  	$selectAtnP = "SELECT attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date >= '".$attendenceStartDate."-04-01'
	                      AND date <= '".$attendenceEndDate."-03-31'
	                      AND attendences = 'P'";
	    $selectAtnPRes = mysql_query($selectAtnP);
	    $countP = mysql_num_rows($selectAtnPRes);
	    $attendenceArr[$i]['countP'] = $countP;
	    
	    $selectAtnA = "SELECT attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date >= '".$attendenceStartDate."-04-01'
	                      AND date <= '".$attendenceEndDate."-03-31'
	                      AND attendences = 'A'";
	    $selectAtnARes = mysql_query($selectAtnA);
	    $countA = mysql_num_rows($selectAtnARes);
	    $attendenceArr[$i]['countA'] = $countA;
	    $attendenceArr[$i]['totalNumberWorkingDay'] = $countA + $countP;
	    //$totalNumberWorkingDay = $countA + $countP;
	  	$i++;
	  }
	}
  
  $c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  $secArrOut[0] = 'A';
  $secArrOut[1] = 'B';
  $secArrOut[2] = 'C';
  $secArrOut[3] = 'D';
  
  include("./bottom.php");
  $smarty->assign('attendenceArr',$attendenceArr);
  $smarty->assign('atnArr',$atnArr);
  $smarty->assign('cArray',$cArray);
  $smarty->assign('class',$class);
  $smarty->assign('section',$section);
  $smarty->assign('secArrOut',$secArrOut);
  //$smarty->assign('totalNumberWorkingDay',$totalNumberWorkingDay);
  $smarty->assign('attendenceStartDate',$attendenceStartDate);
  $smarty->assign('attendenceStartDateSelected',$attendenceStartDateSelected);
  $smarty->assign('attendenceEndDate',$attendenceEndDate);
  $smarty->display('attendenceReportYearly.tpl');
}
?>
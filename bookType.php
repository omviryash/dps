<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$bookType      = "";
	$bookTypeId    = "";
	$isEdit        = 0;
	$bookTypeArray = array();
  if(isset($_POST['Submit']))
  {
  	$bookType      = isset($_POST['bookType']) ? $_POST['bookType'] : "";
  	$bookTypeId    = isset($_POST['bookTypeId']) ? $_POST['bookTypeId'] : 0;
  	if($bookTypeId == 0)
  	{
  	  $insertBookType = "INSERT INTO booktype (bookType)
  	                     VALUES('".$bookType."')";
  	  $insertBookTypeRes = om_query($insertBookType);
  	  if(!$insertBookTypeRes)
  	  {
  	  	echo "Insert Fail";
  	  }
  	  else
  	  {
  	  	header("Location:bookType.php?done=1");
  	  }
  	}
  	else
  	{
  	  $isEdit = 1;
  	  $updateBookType = "UPDATE booktype
  	                        SET bookType = '".$bookType."'
  	                      WHERE bookTypeId = ".$_REQUEST['bookTypeId'];
      $updateBookTypeRes = om_query($updateBookType);
      if(!$updateBookTypeRes)
      {
      	echo "Update Fail";
      }
      else
      {
      	header("Location:bookType.php?done=1");
      }
  	}
  }
  
  $i = 0;
  $selectBookType = "SELECT bookTypeId,bookType
                       FROM booktype";
  $selectBookTypeRes = mysql_query($selectBookType);
  while($bookTypeRow = mysql_fetch_array($selectBookTypeRes))
  {
  	$bookTypeArray[$i]['bookTypeId'] = $bookTypeRow['bookTypeId'];
  	$bookTypeArray[$i]['bookType']   = $bookTypeRow['bookType'];
  	$i++;
  }
  
  if(isset($_REQUEST['bookTypeId']) > 0)
  {
    $selectBookType = "SELECT bookTypeId,bookType
                         FROM booktype
                        WHERE bookTypeId = ".$_REQUEST['bookTypeId'];
    $selectBookTypeRes = mysql_query($selectBookType);
    if($bookTypeRow = mysql_fetch_array($selectBookTypeRes))
    {
    	$bookTypeId = $bookTypeRow['bookTypeId'];
    	$bookType   = $bookTypeRow['bookType'];
    }
  }
  include("./bottom.php");
  $smarty->assign('bookType',$bookType);
  $smarty->assign('bookTypeId',$bookTypeId);
  $smarty->assign('isEdit',$isEdit);
  $smarty->assign('bookTypeArray',$bookTypeArray);
  $smarty->display('bookType.tpl');  
}
?>
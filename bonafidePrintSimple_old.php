<?php
include "include/config.inc.php";
include("amountToWords.php");
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
  $nominalRollId = isset($_REQUEST['nominalRollId']) ? $_REQUEST['nominalRollId'] : 0;
  $selectNominal = "SELECT nominalroll.grNo,nominalroll.academicStartYear,nominalroll.academicEndYear,nominalroll.class,
                           nominalroll.section,nominalroll.rollNo,studentmaster.activated,studentmaster.studentName,
                           studentmaster.studentName,studentmaster.fatherName,studentmaster.gender,
                           DATE_FORMAT(nominalroll.bonafideDate, '%d-%m-%Y') AS bonafideDate,
                           DATE_FORMAT(studentmaster.joiningDate, '%d-%m-%Y') AS wefDate,
                           DATE_FORMAT(studentmaster.dateOfBirth, '%d-%m-%Y') AS dateOfBirth
                      FROM nominalroll
                 LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
                     WHERE nominalroll.nominalRollId = ".$nominalRollId."";
  $selectNominalRes = mysql_query($selectNominal);
  while($nominalRow = mysql_fetch_array($selectNominalRes))
  {
    $grNo               = $nominalRow['grNo'];
    $studentName        = ucwords(strtolower($nominalRow['studentName']));
    $fatherName         = ucwords(strtolower($nominalRow['fatherName']));
    $class              = $nominalRow['class'];
    $academicYear       = substr($nominalRow['academicStartYear'],0,4).'-'.substr($nominalRow['academicEndYear'],0,4);
    
    $gender             = $nominalRow['gender'];
    $bonafideDate       = $nominalRow['bonafideDate'];
    $wefDate            = $nominalRow['wefDate'];
    $dateOfBirth        = $nominalRow['dateOfBirth'];
    
    $mydate = strtoTime($dateOfBirth);
		$dateOfBirthWord = ucwords(strtolower(words(substr($dateOfBirth, 0, 2)))).' '.date('F', $mydate).', '.ucwords(strtolower(words(substr($dateOfBirth, 6, 4))));
  }
  
  if($class == 1)
  {
  	$classRoma = 'I';
  	$classWord = 'First';
  }
  if($class == 2)
  {
  	$classRoma = 'II';
  	$classWord = 'Second';
  }
  if($class == 3)
  {
  	$classRoma = 'III';
  	$classWord = 'Third';
  }
  if($class == 4)
  {
  	$classRoma = 'IV';
  	$classWord = 'Fourth';
  }
  if($class == 5)
  {
  	$classRoma = 'V';
  	$classWord = 'Fifth';
  }
  if($class == 6)
  {
  	$classRoma = 'VI';
  	$classWord = 'Sixth';
  }
  if($class == 7)
  {
  	$classRoma = 'VII';
  	$classWord = 'Seventh';
  }
  if($class == 8)
  {
  	$classRoma = 'VIII';
  	$classWord = 'Eightth';
  }
  if($class == 9)
  {
  	$classRoma = 'IX';
  	$classWord = 'Ninth';
  }
  if($class == 10)
  {
  	$classRoma = 'X';
  	$classWord = 'Tenth';
  }
  if($class == 11)
  {
  	$classRoma = 'XI';
  	$classWord = 'Eleventh';
  }
  if($class == 12)
  {
  	$classRoma = 'XII';
  	$classWord = 'Twelveth';
  }
  if($class == 'Pre-Nursery')
  {
  	$classRoma = 'Pre-Nursery';
  	$classWord = '';
  }
  if($class == 'Nursery')
  {
  	$classRoma = 'Nursery';
  	$classWord = '';
  }
  if($class == 'Prep')
  {
  	$classRoma = 'Prep';
  	$classWord = '';
  }
  
  include("./bottom.php");
  $smarty->assign('studentName',$studentName);  
  $smarty->assign('fatherName',$fatherName);  
  $smarty->assign('classRoma',$classRoma);  
  $smarty->assign('classWord',$classWord);  
  $smarty->assign('academicYear',$academicYear);  
  $smarty->assign('bonafideDate',$bonafideDate);  
  $smarty->assign('gender',$gender);  
  $smarty->assign('wefDate',$wefDate);  
  $smarty->assign('dateOfBirth',$dateOfBirth);  
  $smarty->assign('dateOfBirthWord',$dateOfBirthWord);  
  $smarty->display('bonafidePrintSimple.tpl');  
}
?>
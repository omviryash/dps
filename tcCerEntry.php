<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$houseArray       = array();
  $studentMasterId  = isset($_REQUEST['studentMasterId']) ? $_REQUEST['studentMasterId'] : 0;
  $studentName      = "";
  $fatherName       = "";
  $grNo             = "";
  $dateOfBirth      = "";
  $joinedInClass    = "";
  $joiningDate      = "";
  $bloodGroup       = "";
  $mothersName      = "";
  $houseId          = "";
  $activated        = "";
  $genConduct       = "";
	$reasons          = "";
	$remarks          = "";
	$tribe            = "";
	$tcSrNo           = "";
	$lastStud         = "";
	$takenResult      = "";
	$sameClass        = "";
	$higherClassYesNo = "";
	$higherClass      = "";
	$schoolDues       = "";
	$workingDays      = "";
	$presentDays      = "";
	$nccCadet         = "";
	$gamePlayed       = "";
	$subjectStudies   = '';
	if(isset($_POST['submit']))
	{
	  if(isset($_POST['studentMasterId']) && $_POST['studentMasterId'] > 0)
	  {
	  	$activated        = isset($_REQUEST['activated']) && $_REQUEST['activated'] != '' ? $_REQUEST['activated'] : "";
	  	$tribe            = isset($_REQUEST['tribe']) && $_REQUEST['tribe'] != '' ? $_REQUEST['tribe'] : "";
	  	$tcSrNo           = isset($_REQUEST['tcSrNo']) && $_REQUEST['tcSrNo'] != '' ? $_REQUEST['tcSrNo'] : "";
	  	$lastStud         = isset($_REQUEST['lastStud']) && $_REQUEST['lastStud'] != '' ? $_REQUEST['lastStud'] : "";
	  	$takenResult      = isset($_REQUEST['takenResult']) && $_REQUEST['takenResult'] != '' ? $_REQUEST['takenResult'] : "";
	  	$sameClass        = isset($_REQUEST['sameClass']) && $_REQUEST['sameClass'] != '' ? $_REQUEST['sameClass'] : "";
	  	$higherClassYesNo = isset($_REQUEST['higherClassYesNo']) && $_REQUEST['higherClassYesNo'] != '' ? $_REQUEST['higherClassYesNo'] : "";
	  	$higherClass      = isset($_REQUEST['higherClass']) && $_REQUEST['higherClass'] != '' ? $_REQUEST['higherClass'] : "";
	  	$schoolDues       = isset($_REQUEST['schoolDues']) && $_REQUEST['schoolDues'] != '' ? $_REQUEST['schoolDues'] : "";
	  	$workingDays      = isset($_REQUEST['workingDays']) && $_REQUEST['workingDays'] != '' ? $_REQUEST['workingDays'] : "";
	  	$presentDays      = isset($_REQUEST['presentDays']) && $_REQUEST['presentDays'] != '' ? $_REQUEST['presentDays'] : "";
	  	$nccCadet         = isset($_REQUEST['nccCadet']) && $_REQUEST['nccCadet'] != '' ? $_REQUEST['nccCadet'] : "";
	  	$gamePlayed       = isset($_REQUEST['gamePlayed']) && $_REQUEST['gamePlayed'] != '' ? $_REQUEST['gamePlayed'] : "";
	  	$genConduct       = isset($_REQUEST['genConduct']) && $_REQUEST['genConduct'] != '' ? $_REQUEST['genConduct'] : "";
	  	$reasons          = isset($_REQUEST['reasons']) && $_REQUEST['reasons'] != '' ? $_REQUEST['reasons'] : "";
	  	$remarks          = isset($_REQUEST['remarks']) && $_REQUEST['remarks'] != '' ? $_REQUEST['remarks'] : "";
	  	$subjectStudies   = isset($_REQUEST['subjectStudies']) && $_REQUEST['subjectStudies'] != '' ? $_REQUEST['subjectStudies'] : "";
	  	$appCer           = $_REQUEST['appCerYear']."-".$_REQUEST['appCerMonth']."-".$_REQUEST['appCerDay'];
  	  $issueCer         = $_REQUEST['issueCerYear']."-".$_REQUEST['issueCerMonth']."-".$_REQUEST['issueCerDay'];
  	  
	    $updateEntry = "UPDATE studentmaster 
	                          SET activated        = '".$activated."',
	                              tribe            = '".$tribe."',
	                              tcSrNo           = '".$tcSrNo."',
	                              lastStud         = '".$lastStud."',
	                              takenResult      = '".$takenResult."',
	                              sameClass        = '".$sameClass."',
	                              higherClassYesNo = '".$higherClassYesNo."',
	                              higherClass      = '".$higherClass."',
	                              schoolDues       = '".$schoolDues."',
	                              workingDays      = '".$workingDays."',
	                              presentDays      = '".$presentDays."',
	                              nccCadet         = '".$nccCadet."',
	                              gamePlayed       = '".$gamePlayed."',
	                              genConduct       = '".$genConduct."',
	                              appCer           = '".$appCer."',
	                              issueCer         = '".$issueCer."',
	                              reasons          = '".$reasons."',
	                              remarks          = '".$remarks."',
	                              subjectStudies   = '".$subjectStudies."'
	  									    WHERE studentMasterId = ".$_POST['studentMasterId'];
	    $updateEntryRes = om_query($updateEntry);
	    if(!$updateEntryRes)
	    {
	    	echo "Update Fail";
	    }
	    else
	    {
		    header("Location:tcCerEntry.php?studentMasterId=".$_POST['studentMasterId']."&done=1");
	    }
	  }
	}
  
  if($studentMasterId > 0)
	{
	  $selectStd = "SELECT studentMasterId,studentName,fatherName,grNo,dateOfBirth,joinedInClass,joiningDate,gender,bloodGroup,
	                       mothersName,genConduct,studentmaster.houseId,activated,appCer,issueCer,reasons,remarks,subjectStudies,
	                       tribe,tcSrNo,lastStud,takenResult,sameClass,higherClassYesNo,higherClass,schoolDues,workingDays,presentDays,nccCadet,gamePlayed
	                  FROM studentmaster
	             LEFT JOIN house ON house.houseId = studentmaster.houseId
	                 WHERE studentMasterId = ".$studentMasterId."";
	  $selectStdRes = mysql_query($selectStd);
	  while($stdRow = mysql_fetch_array($selectStdRes))
	  {
	  	$studentMasterId = $stdRow['studentMasterId'];
	  	$studentName     = $stdRow['studentName'];
	  	$fatherName      = $stdRow['fatherName'];
	  	$grNo            = $stdRow['grNo'];
	  	$dateOfBirth     = $stdRow['dateOfBirth'];
	  	$joinedInClass   = $stdRow['joinedInClass'];
	  	$joiningDate     = $stdRow['joiningDate'];
	  	$mothersName     = $stdRow['mothersName'];
	  	$houseId         = $stdRow['houseId'];
	  	$activated       = $stdRow['activated'];
	  	$genConduct      = $stdRow['genConduct'];
	  	$reasons         = $stdRow['reasons'];
	  	$remarks         = $stdRow['remarks'];
	  	$subjectStudies  = $stdRow['subjectStudies'];
	  	if($stdRow['appCer'] != '')
	  	{
	  	  $appCer = $stdRow['appCer'];
	  	}
	  	else
	  	{
	  	  $appCer = date('Y-m-d');
	  	}
	  	if($stdRow['issueCer'] != '')
	  	{
	  	  $issueCer = $stdRow['issueCer'];
	  	}
	  	else
	  	{
	  	  $issueCer = date('Y-m-d');
	  	}
	  	$tribe            = $stdRow['tribe'];
	  	$tcSrNo           = $stdRow['tcSrNo'];
	  	$lastStud         = $stdRow['lastStud'];
	  	$takenResult      = $stdRow['takenResult'];
	  	$sameClass        = $stdRow['sameClass'];
	  	$higherClassYesNo = $stdRow['higherClassYesNo'];
	  	$higherClass      = $stdRow['higherClass'];
	  	$schoolDues       = $stdRow['schoolDues'];
	  	$workingDays      = $stdRow['workingDays'];
	  	$presentDays      = $stdRow['presentDays'];
	  	$nccCadet         = $stdRow['nccCadet'];
	  	$gamePlayed       = $stdRow['gamePlayed'];
	  }
	}
	
  $k = 0;
  $selectHouse = "SELECT houseId,houseName
                   FROM house";
  $selectHouseRes = mysql_query($selectHouse);
  while($houseRow = mysql_fetch_array($selectHouseRes))
  {
  	$houseArray['houseId'][$k]   = $houseRow['houseId'];
  	$houseArray['houseName'][$k] = $houseRow['houseName'];
  	$k++;
  }

  $secArrOut[0] = 'NO';
  $secArrValue[0] = 'N';
  $secArrOut[1] = 'YES';
  $secArrValue[1] = 'Y';
  
  
  $c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  include("./bottom.php");
  $smarty->assign('cArray',$cArray);
  $smarty->assign("secArrOut",$secArrOut);
  $smarty->assign("secArrValue",$secArrValue);
  $smarty->assign("houseId",$houseId);
  $smarty->assign("houseArray",$houseArray);
  $smarty->assign("studentMasterId",$studentMasterId);
  $smarty->assign("studentName",$studentName);
  $smarty->assign("fatherName",$fatherName);
  $smarty->assign("grNo",$grNo);
  $smarty->assign("dateOfBirth",$dateOfBirth);
  $smarty->assign("joinedInClass",$joinedInClass);
  $smarty->assign("joiningDate",$joiningDate);
  $smarty->assign("mothersName",$mothersName);
  $smarty->assign("activated",$activated);
  $smarty->assign("genConduct",$genConduct);
  $smarty->assign("appCer",$appCer);
  $smarty->assign("issueCer",$issueCer);
  $smarty->assign("reasons",$reasons);
  $smarty->assign("remarks",$remarks);
  $smarty->assign("tribe",$tribe);
  $smarty->assign("tcSrNo",$tcSrNo);
  $smarty->assign("lastStud",$lastStud);
  $smarty->assign("takenResult",$takenResult);
  $smarty->assign("sameClass",$sameClass);
  $smarty->assign("higherClassYesNo",$higherClassYesNo);
  $smarty->assign("higherClass",$higherClass);
  $smarty->assign("schoolDues",$schoolDues);
  $smarty->assign("workingDays",$workingDays);
  $smarty->assign("presentDays",$presentDays);
  $smarty->assign("nccCadet",$nccCadet);
  $smarty->assign("gamePlayed",$gamePlayed);
  $smarty->assign("subjectStudies",$subjectStudies);
  $smarty->display("tcCerEntry.tpl");
}
?>
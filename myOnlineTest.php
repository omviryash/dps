<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] != 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$attendenceArr       = array();
	$markArr             = array();
	$attendenceEndDate   = "";
	$attendenceStartDate = "";
	$academicStartYear   = "";
	$academicEndYear     = "";
	$grNo                = "";
	$countTest           = "";
  
	if(isset($_REQUEST['startYear']))
  {
  	$academicStartYear = $_REQUEST['startYear']."-04-01";
  	$nextYear          = $_REQUEST['startYear'] + 1;
  	$academicEndYear   = $nextYear."-03-31";
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y')."-04-01";
	  	$nextYear          = date('Y') + 1;
	  	$academicEndYear   = $nextYear."-03-31";
		}
		else
		{
			$prevYear          = date('Y') - 1;
			$academicStartYear = $prevYear."-04-01";
	  	$academicEndYear   = date('Y')."-03-31";
		}
	}
	
	if(isset($_REQUEST['startYear']))
  {
	  $attendenceStartDate = $_REQUEST['startYear'];
	  $attendenceEndDate   = $_REQUEST['startYear'] + 1;
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$attendenceStartDate = date('Y');
	  	$nextYear            = date('Y') + 1;
	  	$attendenceEndDate   = $nextYear;
		}
		else
		{
			$prevYear            = date('Y') - 1;
			$attendenceStartDate = $prevYear;
	  	$attendenceEndDate   = date('Y');
		}
	}
		
	$class   = isset($_REQUEST['class']) ? $_REQUEST['class'] : 0;
	$section = isset($_REQUEST['classSection']) ? $_REQUEST['classSection'] : 0;
	
  $i = 0;
  $selectNominal = "SELECT studentmaster.grNo,nominalroll.class,studentmaster.activated,
                           studentmaster.studentLoginId,studentmaster.parentLoginId
                      FROM nominalroll
                 LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
                     WHERE 1 = 1
                       AND nominalroll.academicStartYear = '".$attendenceStartDate."-04-01'
                       AND nominalroll.academicEndYear = '".$attendenceEndDate."-03-31'
                       AND studentmaster.studentLoginId = '".$_SESSION['s_activName']."' 
                           OR studentmaster.parentLoginId = '".$_SESSION['s_activName']."'
                       AND studentmaster.activated = 'Y'";
  $selectNominalRes = mysql_query($selectNominal);
  while($nominalRow = mysql_fetch_array($selectNominalRes))
  {
  	$grNo = $nominalRow['grNo'];
    $g = 0;
    $selectAtnP = "SELECT onlineschedule.scheduleMasterId,onlineschedule.class,subjectmaster.subjectName,
                          onlineschedule.scheduleDate
                     FROM onlineschedule
                LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = onlineschedule.subjectMasterId
                    WHERE class = '".$nominalRow['class']."'
                      AND onlineschedule.scheduleDate >= '".$attendenceStartDate."-04-01'
                      AND onlineschedule.scheduleDate <= '".$attendenceEndDate."-03-31'
                 ORDER BY onlineschedule.scheduleDate DESC, onlineschedule.scheduleMasterId DESC";
    $selectAtnPRes = mysql_query($selectAtnP);
    while($markRow = mysql_fetch_array($selectAtnPRes))
    {
    	$markArr[$g]['totalMarks'] = '';
    	$markArr[$g]['scheduleMasterId'] = $markRow['scheduleMasterId'];
    	$markArr[$g]['class']            = $markRow['class'];
    	$markArr[$g]['scheduleDate']     = $markRow['scheduleDate'];
    	$markArr[$g]['subjectName']      = $markRow['subjectName'];
    	
    	$countTest = 0;
			$selectCount = "SELECT myonlinetest.myOnlineTestId,onlinetest.answer,myonlinetest.answer AS myAnswer
		                    FROM myonlinetest
		               LEFT JOIN onlinetest ON onlinetest.onlineTestId = myonlinetest.onlineTestId
		                   WHERE scheduleMasterId = ".$markRow['scheduleMasterId']."
		                     AND grNo = '".$grNo."'";
			$selectCountRes = mysql_query($selectCount);
			$markArr[$g]['countTest'] = mysql_num_rows($selectCountRes);
			while($classRow = mysql_fetch_array($selectCountRes))
      {
			  if($classRow['myAnswer'] == $classRow['answer'])
		  	{
		  	  $markArr[$g]['marks'] = 1;
		  	}
		  	else
		  	{
		  		$markArr[$g]['marks'] = 0;
		  	}
		  	$markArr[$g]['totalMarks'] += $markArr[$g]['marks'];
		  }
    	$g++;
    }
  	$i++;
  }
  
  $c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  $secArrOut[0] = 'A';
  $secArrOut[1] = 'B';
  $secArrOut[2] = 'C';
  $secArrOut[3] = 'D';
  
  $studStartYear = substr($academicStartYear,0,4);
  $studEndYear   = substr($academicEndYear,2,2);
  include("./bottom.php");
  $smarty->assign('attendenceArr',$attendenceArr);
  $smarty->assign('studStartYear',$studStartYear);
  $smarty->assign('studEndYear',$studEndYear);
  $smarty->assign('markArr',$markArr);
  $smarty->assign('cArray',$cArray);
  $smarty->assign('class',$class);
  $smarty->assign('section',$section);
  $smarty->assign('secArrOut',$secArrOut);
  $smarty->assign('attendenceStartDate',$attendenceStartDate);
  $smarty->assign('attendenceEndDate',$attendenceEndDate);
  $smarty->assign('countTest',$countTest);
  $smarty->display('myOnlineTest.tpl');
}
?>
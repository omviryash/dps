<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] != 'Teacher')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	if(isset($_REQUEST['startYear']))
  {
	  $academicStartYear = $_REQUEST['startYear'];
	  $academicEndYear   = $_REQUEST['startYear'] + 1;
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y');
	  	$nextYear          = date('Y') + 1;
	  	$academicEndYear   = $nextYear;
		}
		else
		{
			$prevYear          = date('Y') - 1;
			$academicStartYear = $prevYear;
	  	$academicEndYear   = date('Y');
		}
	}
	
	$nominalArr = array();
	
	$selectId = "SELECT employeeMasterId
                 FROM employeemaster
                WHERE loginId = '".$_SESSION['s_activName']."'";
  $selectIdRes = mysql_query($selectId);
  if($idRow = mysql_fetch_array($selectIdRes))
  {
  	$selectClass = "SELECT class,section
	                    FROM classteacherallotment
	                   WHERE employeeMasterId = '".$idRow['employeeMasterId']."'
	                     AND academicStartYear = '".$academicStartYear."-04-01'
	                     AND academicEndYear = '".$academicEndYear."-03-31'";
	  $selectClassRes = mysql_query($selectClass);
	  if($classRow = mysql_fetch_array($selectClassRes))
	  {
	  	$class   = $classRow['class'];
	  	$section = $classRow['section'];
	  	
		  $i = 0;
		  $selectNominal = "SELECT nominalRollId,nominalroll.grNo,academicStartYear,academicEndYear,nominalroll.class,
		                           nominalroll.section,rollNo,feeGroup,libraryMemberType,libraryMemberType,studentmaster.studentName,
		                           busRoute,busStop,subjectGroup,coScholasticGroup,clubTerm1,clubTerm2,
		                           boardRegistrionId,boardRollNo,activated,studentmaster.studentLoginId,studentmaster.parentLoginId,
		                           nominalroll.optionSubject1,nominalroll.optionSubject2
		                      FROM nominalroll
		                 LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
		                     WHERE studentmaster.activated = 'Y'
		                       AND nominalroll.class = '".$class."'
		                       AND nominalroll.section = '".$section."'
		                       AND academicStartYear = '".$academicStartYear."-04-01'
		                       AND academicEndYear = '".$academicEndYear."-03-31'
		                  ORDER BY nominalroll.rollNo";
		  $selectNominalRes = mysql_query($selectNominal);
		  while($nominalRow = mysql_fetch_array($selectNominalRes))
		  {
		    $nominalArr[$i]['nominalRollId']      = $nominalRow['nominalRollId'];
		    $nominalArr[$i]['grNo']               = $nominalRow['grNo'];
		    $nominalArr[$i]['academicYear']       = substr($nominalRow['academicStartYear'],0,4).'-'.substr($nominalRow['academicEndYear'],0,4);
		    $nominalArr[$i]['class']              = $nominalRow['class'];
		    $nominalArr[$i]['section']            = $nominalRow['section'];
		    $nominalArr[$i]['rollNo']             = $nominalRow['rollNo'];
		    $nominalArr[$i]['feeGroup']           = $nominalRow['feeGroup'];
		    $nominalArr[$i]['libraryMemberType']  = $nominalRow['libraryMemberType'];
		    $nominalArr[$i]['libraryMemberType']  = $nominalRow['libraryMemberType'];
		    $nominalArr[$i]['studentName']        = $nominalRow['studentName'];
		    $nominalArr[$i]['busRoute']           = $nominalRow['busRoute'];
		    $nominalArr[$i]['busStop']            = $nominalRow['busStop'];
		    $nominalArr[$i]['subjectGroup']       = $nominalRow['subjectGroup'];
		    $nominalArr[$i]['coScholasticGroup']  = $nominalRow['coScholasticGroup'];
		    $nominalArr[$i]['clubTerm1']          = $nominalRow['clubTerm1'];
		    $nominalArr[$i]['clubTerm2']          = $nominalRow['clubTerm2'];
		    $nominalArr[$i]['boardRegistrionId']  = $nominalRow['boardRegistrionId'];
		    $nominalArr[$i]['boardRollNo']        = $nominalRow['boardRollNo'];
		    $nominalArr[$i]['activated']          = $nominalRow['activated'];
		    $nominalArr[$i]['optionSubject1']     = $nominalRow['optionSubject1'];
		    $nominalArr[$i]['optionSubject2']     = $nominalRow['optionSubject2'];
		    $i++;
		  }
		}
	}
  
	$k = 0;
	$selectClub1 = "SELECT subjectMasterId,subjectName
	                  FROM subjectmaster
	                 WHERE subjectName = 'Sanskrit'
	                    OR subjectName = 'Gujarati'
	                    OR subjectName = 'Biology(044)'
	                    OR subjectName = 'Mathematics'";
	$selectClub1Res = mysql_query($selectClub1);
	while($club1Row = mysql_fetch_array($selectClub1Res))
	{
		$clubArr1['subjectMasterId'][$k]   = $club1Row['subjectMasterId'];
		$clubArr1['subjectName'][$k]       = $club1Row['subjectName'];
		$k++;
	}
	
	$h = 0;
	$selectClub2 = "SELECT subjectMasterId,subjectName
	                  FROM subjectmaster
	                 WHERE subjectName = 'Informatics Practices(065)'
	                    OR subjectName = 'Physical Education(048)'";
	$selectClub2Res = mysql_query($selectClub2);
	while($club2Row = mysql_fetch_array($selectClub2Res))
	{
		$clubArr2['subjectMasterId'][$h]   = $club2Row['subjectMasterId'];
		$clubArr2['subjectName'][$h]       = $club2Row['subjectName'];
		$h++;
	}
	
	if(isset($_POST['submit']))
	{
		$loopCount1 = 0;
	  while($loopCount1 < count($_POST['nominalRollId']))
	  {
	    $nominalRollId  = isset($_POST['nominalRollId'][$loopCount1]) && $_POST['nominalRollId'][$loopCount1] != '' ? $_POST['nominalRollId'][$loopCount1] : 0;
	    $optionSubject1 = isset($_POST['optionSubject1'][$loopCount1]) && $_POST['optionSubject1'][$loopCount1] != '' ? $_POST['optionSubject1'][$loopCount1] : 0;
		  $optionSubject2 = isset($_POST['optionSubject2'][$loopCount1]) && $_POST['optionSubject2'][$loopCount1] != '' ? $_POST['optionSubject2'][$loopCount1] : 0;
	    if($optionSubject1 > 0 || $optionSubject2 > 0)
	    {
	      $nominalEntry = "UPDATE nominalroll 
			  										SET optionSubject1 = ".$optionSubject1.",
			  											  optionSubject2 = ".$optionSubject2."
											  	WHERE nominalRollId  = ".$nominalRollId."";
			  $nominalEntryRes = om_query($nominalEntry);
			  if(!$nominalEntryRes)
			  {
			  	echo "Update Fail";
			  }
			  else
			  {
			    header("Location:nominalRollListEmployee.php?done=1");
			  }
	    }
	    $loopCount1++;
	  }
  }
  
  include("./bottom.php");
  $smarty->assign('clubArr1',$clubArr1);
  $smarty->assign('clubArr2',$clubArr2);
  $smarty->assign('nominalArr',$nominalArr);
  $smarty->assign('academicStartYear',$academicStartYear);
  $smarty->assign('academicEndYear',$academicEndYear);
  $smarty->display('nominalRollListEmployee.tpl');  
}
?>
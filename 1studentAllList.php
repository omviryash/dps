<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$activated = isset($_GET['activated']) && $_GET['activated'] !='' ? $_GET['activated'] : 'Y';
	$stdArray = array();
	$i = 0;

  $selectStd = "SELECT studentMasterId,studentName,fatherName,grNo,activated,dateOfBirth,joinedInClass,joiningDate,gender,bloodGroup,house.houseName,
                       currentAddress,currentState,currentAddressPin,residencePhone1,residencePhone2,studentEmail,
                       permanentAddress,permanentState,permanentPIN,religion,previousSchool
                  FROM studentmaster
             LEFT JOIN house ON house.houseId = studentmaster.houseId";
  $selectStdRes = mysql_query($selectStd);
  while($stdRow = mysql_fetch_array($selectStdRes))
  {
  	$stdArray[$i]['studentMasterId']   = $stdRow['studentMasterId'];
  	$stdArray[$i]['studentName']       = $stdRow['studentName'];
  	$stdArray[$i]['fatherName']        = $stdRow['fatherName'];
  	$stdArray[$i]['grNo']              = $stdRow['grNo'];
  	$stdArray[$i]['activated']         = $stdRow['activated'];
  	$stdArray[$i]['dateOfBirth']       = $stdRow['dateOfBirth'];
  	$stdArray[$i]['joinedInClass']     = $stdRow['joinedInClass'];
  	$stdArray[$i]['joiningDate']       = $stdRow['joiningDate'];
  	$stdArray[$i]['gender']            = $stdRow['gender'];
  	$stdArray[$i]['bloodGroup']        = $stdRow['bloodGroup'];
  	$stdArray[$i]['houseName']         = $stdRow['houseName'];
  	$stdArray[$i]['currentAddress']    = $stdRow['currentAddress'];
    $stdArray[$i]['currentState']      = $stdRow['currentState'];
    $stdArray[$i]['currentAddressPin'] = $stdRow['currentAddressPin'];
  	$stdArray[$i]['residencePhone1']   = $stdRow['residencePhone1'];
  	$stdArray[$i]['residencePhone2']   = $stdRow['residencePhone2'];
  	$stdArray[$i]['studentEmail']      = $stdRow['studentEmail'];
  	$stdArray[$i]['permanentAddress']  = $stdRow['permanentAddress'];
  	$stdArray[$i]['permanentState']    = $stdRow['permanentState'];
    $stdArray[$i]['permanentPIN']      = $stdRow['permanentPIN'];
  	$stdArray[$i]['religion']          = $stdRow['religion'];
  	$stdArray[$i]['previousSchool']    = $stdRow['previousSchool'];
  	$i++;
  }
  
  $activeValues[0] = 'Y';
  $activeValues[1] = 'N';
  $activeValues[2] = 'All';
  $activeOutPut[0] = 'Yes';
  $activeOutPut[1] = 'No';
  $activeOutPut[2] = 'Both';
  
	include("./bottom.php");
	$smarty->assign('stdArray',$stdArray);
	$smarty->assign('activeValues',$activeValues);
	$smarty->assign('activeOutPut',$activeOutPut);
	$smarty->assign('activated',$activated);
  $smarty->display('studentAllList.tpl');
}
?>
<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] != 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$today = date('Y-m');
	$countA = "";
	$countP = "";
	$totalNumberWorkingDay = "";
	if(isset($_REQUEST['attendenceYear']))
	{
	  $attendenceDate = $_REQUEST['attendenceYear']."-".$_REQUEST['attendenceMonth'];
	  $attendenceStartDate = $_REQUEST['attendenceYear']."-".$_REQUEST['attendenceMonth']."-01";
	  $attendenceEndDate = $_REQUEST['attendenceYear']."-".$_REQUEST['attendenceMonth']."-31";
	}
	else
	{
		$attendenceDate      = $today;
		$attendenceStartDate = $today."-01";
		$attendenceEndDate   = $today."-31";
	}
	
	$nominalArr = array();
  $i = 0;
  $selectNominal = "SELECT studentmaster.grNo,attendence.date,attendence.class,attendence.section,studentmaster.activated,
                           studentmaster.studentLoginId,studentmaster.parentLoginId,attendence.attendences
                      FROM attendence
                 LEFT JOIN studentmaster ON studentmaster.grNo = attendence.grNo
                     WHERE 1 = 1
                       AND attendence.date >= '".$attendenceStartDate."'
                       AND attendence.date <= '".$attendenceEndDate."'
                       AND studentmaster.studentLoginId = '".$_SESSION['s_activName']."' 
                           OR studentmaster.parentLoginId = '".$_SESSION['s_activName']."'
                       AND studentmaster.activated = 'Y'
                  ORDER BY attendence.date";
  $selectNominalRes = mysql_query($selectNominal);
  while($nominalRow = mysql_fetch_array($selectNominalRes))
  {
    $nominalArr[$i]['grNo']               = $nominalRow['grNo'];
    $nominalArr[$i]['date']               = $nominalRow['date'];
    $nominalArr[$i]['class']              = $nominalRow['class']." - ".$nominalRow['section'];
    $nominalArr[$i]['attendences']        = $nominalRow['attendences'];
    
    $selectAtnP = "SELECT attendences
                     FROM attendence
                    WHERE grNo = '".$nominalRow['grNo']."'
                      AND date >= '".$attendenceStartDate."-04-01'
                      AND date <= '".$attendenceEndDate."-03-31'
                      AND attendences = 'P'";
    $selectAtnPRes = mysql_query($selectAtnP);
    $countP = mysql_num_rows($selectAtnPRes);
    $countP;
    
    $selectAtnA = "SELECT attendences
                     FROM attendence
                    WHERE grNo = '".$nominalRow['grNo']."'
                      AND date >= '".$attendenceStartDate."-04-01'
                      AND date <= '".$attendenceEndDate."-03-31'
                      AND attendences = 'A'";
    $selectAtnARes = mysql_query($selectAtnA);
    $countA = mysql_num_rows($selectAtnARes);
    $countA;
    $totalNumberWorkingDay = $countA + $countP;
  	$i++;
  }
  
  include("./bottom.php");
  $smarty->assign('nominalArr',$nominalArr);
  $smarty->assign('attendenceDate',$attendenceDate);
  $smarty->assign('attendenceStartDate',$attendenceStartDate);
  $smarty->assign('attendenceEndDate',$attendenceEndDate);
  $smarty->assign('countA',$countA);
  $smarty->assign('countP',$countP);
  $smarty->assign('totalNumberWorkingDay',$totalNumberWorkingDay);
  $smarty->display('attendanceDailyStudent.tpl');  
}
?>
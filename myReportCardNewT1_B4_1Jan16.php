<?php
define('FPDF_FONTPATH', 'font/');
require('./font/fpdf.php');
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
  $pdf=new FPDF('P','mm','A4');   //Create new pdf file
  $pdf->Open();     //Open file
  $pdf->SetAutoPageBreak(false);  //Disable automatic page break
  $pdf->AddPage();  //Add first page


  //header part start
  $pdf->Image('./images/logo.png',10,10,22,25);
  $pdf->Image('./images/logoIndex.png',65,2,70,20);
  //header part end
  //table header part start  
  $pdf->SetFillColor(232, 232, 232);
  $pdf->SetFont('Arial', '', 8);
  $pdf->SetXY(5,20);
  $pdf->Cell(200, 5, 'Haripar, Survey No. 12, Behind NRI Bunglows, Rajkot - 360 007', 0,0,'C',0);
  $pdf->SetXY(5,25);
  $pdf->Cell(200, 5, 'Ph: 9375070921, 9375070922, Email : Info@dpsrajkot.org, Website: www.dpsrajkot.org', 0,0,'C',0);
  $pdf->SetXY(5,30);
  $pdf->Cell(200, 5, '(Affilited to C B S E, New Delhi, Affiliation No.430054)', 0,0,'C',0);
  $pdf->SetFont('Arial', 'B', 12);
  $pdf->SetXY(5,35);
  $pdf->Cell(200, 5, 'Record of Academic Performance', 0,0,'C',0);
  
  $pdf->SetFillColor(232, 232, 232);
  $pdf->SetFont('Arial', 'B', 12);
  $pdf->SetXY(5,95);
  $pdf->Cell(100, 10, 'Subject', 1,0,'C',1);
  $pdf->Cell(100, 10, 'Half Yearly Examination Out of 100', 1,0,'C',1);
  
  $i         = 0; 
  $rowHeight = 10;
  $yAxis     = 95;

  $yAxis = $yAxis + $rowHeight;
  
  //table header part end 
  
  $marksHalf = 0;
  $marksAnnu = 0;
  $marksMond = 0;
  $marksPrac = 0;
  $marksHalfAll = 0;
  
  if(isset($_REQUEST['academicYear']))
  {
  	$academicStartYear = substr($_REQUEST['academicYear'],0,4)."-04-01";
  	$nextYear          = substr($_REQUEST['academicYear'],0,4) + 1;
  	$academicEndYear   = $nextYear."-03-31";
  }
  else
  {
	  $todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y')."-04-01";
	  	$nextYear          = date('Y') + 1;
	  	$academicEndYear   = $nextYear."-03-31";
		}
		else
		{
			$prevYear          = date('Y') - 1;
			$academicStartYear = $prevYear."-04-01";
                        $academicEndYear   = date('Y')."-03-31";
		}
	}
	
	if(isset($_REQUEST['grNo']))
	{
	  $myGrNo = "AND nominalroll.grNo = ".$_REQUEST['grNo']."";
	}
	else
	{
		$myGrNo = "AND studentmaster.studentLoginId = '".$_SESSION['s_activName']."' 
                   OR studentmaster.parentLoginId = '".$_SESSION['s_activName']."'";
	}
	//print column titles for the actual page
  $pdf->SetFillColor(232, 232, 232);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->SetXY(0,40);
  $pdf->Cell(210, 5, substr($academicStartYear,0,4).' - '.substr($academicEndYear,0,4), 0, 0, 'C', 0);
  
  $selectNominal = "SELECT nominalroll.grNo,nominalroll.academicStartYear,nominalroll.academicEndYear,nominalroll.class,
                           nominalroll.section,nominalroll.rollNo,studentmaster.activated,studentmaster.bloodGroup,studentmaster.studentName,
                           studentmaster.fatherName,studentmaster.mothersName,DATE_FORMAT(studentmaster.dateOfBirth,'%d-%m-%Y') AS dateOfBirth,
                           studentmaster.currentAddress,house.houseName,nominalroll.height,nominalroll.weight,
                           nominalroll.resultRemark1,nominalroll.resultRemark2,nominalroll.enEducation,nominalroll.phEducation,nominalroll.enEducation2,nominalroll.phEducation2
                      FROM nominalroll
                 LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
                 LEFT JOIN house ON house.houseId = studentmaster.houseId
                     WHERE 1 = 1
                       ".$myGrNo."
                       AND nominalroll.academicStartYear = '".$academicStartYear."'
                       AND nominalroll.academicEndYear = '".$academicEndYear."'
                       AND studentmaster.activated = 'Y'
                  ORDER BY nominalroll.rollNo";
  
  $selectNominalRes = mysql_query($selectNominal);
  while($nominalRow = mysql_fetch_array($selectNominalRes))
  {
    $class              = $nominalRow['class'];
    $section            = $nominalRow['section'];
    $grNo               = $nominalRow['grNo'];
    $studentName        = $nominalRow['studentName'];
    $fatherName         = $nominalRow['fatherName'];
    $mothersName        = $nominalRow['mothersName'];
    $dateOfBirth        = $nominalRow['dateOfBirth'];
    $currentAddress     = $nominalRow['currentAddress'];
    $academicYear       = substr($nominalRow['academicStartYear'],0,4).'-'.substr($nominalRow['academicEndYear'],0,4);
    $rollNo             = $nominalRow['rollNo'];
    $house              = $nominalRow['houseName'];
    $height             = $nominalRow['height'];
    $weight             = $nominalRow['weight'];
    $resultRemark1      = $nominalRow['resultRemark1'];
    $resultRemark2      = $nominalRow['resultRemark2'];
    $enEducation        = $nominalRow['enEducation'];
    $phEducation        = $nominalRow['phEducation'];
    $enEducation2       = $nominalRow['enEducation2'];
    $phEducation2       = $nominalRow['phEducation2'];
    $bloodGroup         = $nominalRow['bloodGroup'];
    
    $pdf->SetFillColor(232, 232, 232);
	  $pdf->SetFont('Arial', 'B', 10);
	  $pdf->SetXY(5,45);
	  $pdf->Cell(35, 5, 'Name of Student : ', 0, 0, 'L', 0);
	  $pdf->Cell(105,5, $studentName, 0, 0, 'L', 0);
	  $pdf->Cell(35, 5, 'Gr No : ', 0, 0, 'R', 0);
	  $pdf->Cell(25, 5, $grNo, 0, 0, 'R', 0);
	  
    $pdf->SetFillColor(232, 232, 232);
	  $pdf->SetFont('Arial', 'B', 10);
	  $pdf->SetXY(5,55);
	  $pdf->Cell(35, 5, 'Fathes Name : ', 0, 0, 'L', 0);
	  $pdf->Cell(105,5, $fatherName, 0, 0, 'L', 0);
	  $pdf->Cell(35, 5, 'Class : ', 0, 0, 'R', 0);
	  $pdf->Cell(25, 5, $class.' - '.$section, 0, 0, 'R', 0);
	  
	  $pdf->SetFillColor(232, 232, 232);
	  $pdf->SetFont('Arial', 'B', 10);
	  $pdf->SetXY(5,65);
	  $pdf->Cell(35, 5, 'Mothers Name : ', 0, 0, 'L', 0);
	  $pdf->Cell(105,5, $mothersName, 0, 0, 'L', 0);
	  $pdf->Cell(35, 5, 'DOB : ', 0, 0, 'R', 0);
	  $pdf->Cell(25, 5, $dateOfBirth, 0, 0, 'R', 0);
	  
    $pdf->SetFillColor(232, 232, 232);
	  $pdf->SetFont('Arial', 'B', 10);
	  $pdf->SetXY(5,75);
	  $pdf->Cell(35, 5, 'Roll No : ', 0, 0, 'L', 0);
	  $pdf->Cell(105,5, $rollNo, 0, 0, 'L', 0);
	  $pdf->Cell(35, 5, 'House : ', 0, 0, 'R', 0);
	  $pdf->Cell(25, 5, $house, 0, 0, 'R', 0);
	  
//    $pdf->SetFillColor(232, 232, 232);
//	  $pdf->SetFont('Arial', 'B', 10);
//	  $pdf->SetXY(5,85);
//	  $pdf->Cell(35, 5, 'Address : ', 0, 0, 'L', 1);
//	  $pdf->Cell(165,5, $currentAddress, 0, 0, 'L', 1);
    // monday 
    /*$selectAtnP = "SELECT subjectmaster.subjectName,exammarks.marks,exammarks.class,exammarks.section,examtype.examType,
                          examschedule.scheduleDate
                     FROM exammarks
                LEFT JOIN examschedule ON examschedule.examScheduleId = exammarks.examScheduleId
                LEFT JOIN examtype ON examtype.examTypeId = examschedule.examTypeId
                LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = examschedule.subjectMasterId
                    WHERE grNo = '".$grNo."'
                      AND examschedule.scheduleDate >= '".$academicStartYear."'
                      AND examschedule.scheduleDate <= '".$academicEndYear."'
                 GROUP BY subjectmaster.subjectName";*/
          $selectAtnP = "SELECT subjectmaster.subjectName,exammarks.marks,exammarks.class,exammarks.section,examtype.examType,
                          examschedule.scheduleDate
                     FROM exammarks
                LEFT JOIN examschedule ON examschedule.examScheduleId = exammarks.examScheduleId
                LEFT JOIN examtype ON examtype.examTypeId = examschedule.examTypeId
                LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = examschedule.subjectMasterId
                    WHERE grNo = '" . $grNo . "'
                      AND exammarks.class = '" . $class . "'
                      AND examschedule.scheduleDate >= '" . $academicStartYear . "'
                      AND examschedule.scheduleDate <= '" . $academicEndYear . "'
                 GROUP BY subjectmaster.subjectName
                 ORDER BY subjectmaster.sequence";
    
                 //ORDER BY subjectmaster.sequence";
    $selectAtnPRes = mysql_query($selectAtnP);
    while($markRow = mysql_fetch_array($selectAtnPRes))
    {
      $mondaymarks = $markRow['marks'];  
      $subjectNameAll = $markRow['subjectName'];
      $subjectName = $markRow['subjectName'];
	  	$selectAtnP1 = "SELECT exammarks.marks,exammarks.class,exammarks.section,subjectmaster.subjectName,examtype.examType,
	                          examschedule.scheduleDate,exammarks.marksPractical
	                     FROM exammarks
	                LEFT JOIN examschedule ON examschedule.examScheduleId = exammarks.examScheduleId
	                LEFT JOIN examtype ON examtype.examTypeId = examschedule.examTypeId
	                LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = examschedule.subjectMasterId
	                    WHERE grNo = '".$grNo."'
	                      AND examschedule.scheduleDate >= '".$academicStartYear."'
	                      AND examschedule.scheduleDate <= '".$academicEndYear."'
	                      AND subjectmaster.subjectName = '".$subjectName."'";
            $selectAtnP1Res = mysql_query($selectAtnP1);
	    while($mark1Row = mysql_fetch_array($selectAtnP1Res))
	    {
                
	        $subjectName    = $mark1Row['subjectName'];
	        $examType       = $mark1Row['examType'];
	    	$marks          = $mark1Row['marks'];
	    	$marksPractical = $mark1Row['marksPractical'];
                
                
                $selectEval = "SELECT subjectName,totalMarkHalf,evalMarkHalf,totalMarkAnnu,evalMarkAnnu,totalMarkPrac,evalMarkPrac
	                       FROM evalution";
                
	      $selectEvalRes = mysql_query($selectEval);
              
	      while($evalRow = mysql_fetch_array($selectEvalRes))
	      {
	      	$subjectEval   = $evalRow['subjectName'];
	      	$totalMarkHalf = $evalRow['totalMarkHalf'];
	      	$evalMarkHalf  = $evalRow['evalMarkHalf'];
	      	$totalMarkAnnu = $evalRow['totalMarkAnnu'];
	      	$evalMarkAnnu  = $evalRow['evalMarkAnnu'];
	      	$totalMarkPrac = $evalRow['totalMarkPrac'];
	      	$evalMarkPrac  = $evalRow['evalMarkPrac'];
                        
			    $pdf->SetFillColor(232, 232, 232);
			    $pdf->SetFont('Arial', '', 10);
			    $pdf->SetXY(5,$yAxis);
                            $pdf->Cell(100, 10, $subjectName, 1,0,'L'); 
                            
                            //$marksHalf = $marks / $totalMarkHalf;
                            /*$pdf->SetXY(105,$yAxis);
                            $pdf->Cell(100, 10, number_format($marks, 0,'.',''), 1,0,'C');*/
			    /*if($subjectEval == $subjectNameAll)
			    {
			        $pdf->Cell(100, 10, $subjectNameAll, 1,0,'L');
			    }
			    else
			    {
			    	$pdf->Cell(100, 10, '', 1,0,'L');
			    }*/
                            /*if($examType == 'Half Yearly Examination')
			    {
			    	if($subjectEval == $subjectNameAll)
			    	{
                                        $marksHalf = $marks / $totalMarkHalf;
                                        $pdf->SetXY(105,$yAxis);
				        $pdf->Cell(100, 10, number_format($marksHalf, 0,'.',''), 1,0,'C');
				    }
				    else
				    {
				    	$pdf->SetXY(105,$yAxis);
				        $pdf->Cell(100, 10, '', 1,0,'C');
				    }
			    }
			    else
			    {
			    	$pdf->SetXY(105,$yAxis);
			        $pdf->Cell(100, 10, '', 1,0,'C');
			    }*/
			    
			    //$marksHalfAll += $totalMarkHalf;
//			    if($examType == 'Annual Examination')
//			    {
//			    	if($subjectEval == $subjectNameAll)
//			    	{
//			    		$marksAnnu = $marks * $evalMarkAnnu / $totalMarkAnnu;
//				    	$marksPrac = $marksPractical;
//				    	//$marksPrac = $marksPractical * $evalMarkPrac / $totalMarkPrac;
//				    	$pdf->SetXY(85,$yAxis);
//				      $pdf->Cell(30, 10, number_format($marksAnnu, 0,'.',''), 1,0,'C');
//				      $pdf->Cell(30, 10, '', 0,0,'C');
//				      $pdf->Cell(30, 10, number_format($marksPrac, 0,'.',''), 1,0,'C');
//				    }
//			    }
//			    else
//			    {
//			    	$pdf->SetXY(85,$yAxis);
//			      $pdf->Cell(30, 10, '', 1,0,'C');
//			      $pdf->Cell(30, 10, '', 0,0,'C');
//			      $pdf->Cell(30, 10, '', 1,0,'C');
//			    }
//			    if($examType == 'Monday Test')
//			    {
//			    	if($subjectEval == $subjectNameAll)
//			      {
//			      	$subjectEval;
//			      	
//			      	$selectAtnP12 = "SELECT MAX(exammarks.marks) AS marksMond,exammarks.class,exammarks.section,subjectmaster.subjectName,examtype.examType,
//						                          examschedule.scheduleDate,exammarks.marksPractical
//						                     FROM exammarks
//						                LEFT JOIN examschedule ON examschedule.examScheduleId = exammarks.examScheduleId
//						                LEFT JOIN examtype ON examtype.examTypeId = examschedule.examTypeId
//						                LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = examschedule.subjectMasterId
//						                    WHERE grNo = '".$grNo."'
//						                      AND examschedule.scheduleDate >= '".$academicStartYear."'
//						                      AND examschedule.scheduleDate <= '".$academicEndYear."'
//						                      AND subjectmaster.subjectName = '".$subjectEval."'
//						                      AND examType = 'Monday Test'";
//						  $selectAtnP12Res = mysql_query($selectAtnP12);
//						  if($mark12Row = mysql_fetch_array($selectAtnP12Res))
//						  {
//						    $marksMond = $mark12Row['marksMond'];
//						  }
//				    	
//				    	$pdf->SetXY(115,$yAxis);
//				      $pdf->Cell(30, 10, number_format($marksMond, 0,'.',''), 1,0,'C');
//				    }
//			    }
//			    else
//			    {
//			    	$pdf->SetXY(115,$yAxis);
//			      $pdf->Cell(30, 10, '', 1,0,'C');
//			    }
			  }
	    }
            $marksHalfAll += $marks;
            $pdf->SetXY(105,$yAxis);
            $pdf->Cell(100, 10, number_format($marks, 0,'.',''), 1,0,'C');
//      echo "<br> x1 ".$marksHalf;
//      echo "<br> x2 ".$marksAnnu;
//      echo "<br> x3 ".$marksPrac;
//      echo "<br> x4 ".$marksMond;

//      $marksTotal = $marksHalf + $marksAnnu + $marksPrac + $marksMond;
//	    $pdf->SetXY(175,$yAxis);
//	    $pdf->Cell(30, 10, number_format($marksTotal, 0,'.',''), 1,0,'C');

	    //echo $marksMond;
	    $yAxis = $yAxis + $rowHeight;
	    $i = $i + 1;
	  }
	  
	  $pdf->SetFillColor(232, 232, 232);
	  $pdf->SetFont('Arial', 'B', 12);
	  $pdf->SetXY(5,$yAxis);
	  $pdf->Cell(100, 10, 'Total', 1,0,'C',1);
	  $pdf->Cell(100, 10, number_format($marksHalfAll, 0,'.',''), 1,0,'C',1);
	  
	  $pdf->SetFont('Arial', '', 12);
	  $pdf->SetXY(5,$yAxis+10);
	  $pdf->Cell(100, 10, 'Environmental Education', 1,0,'L',0);
	  $pdf->Cell(100, 10, $enEducation, 1,0,'C',0);
	  
	  $pdf->SetFont('Arial', '', 12);
	  $pdf->SetXY(5,$yAxis+20);
	  $pdf->Cell(100, 10, 'Physical Education', 1,0,'L',0);
	  $pdf->Cell(100, 10, $phEducation, 1,0,'C',0);
	  
	  $selectAtnP = "SELECT attendences
                     FROM attendence
                    WHERE class = '".$class."'
                      AND section = '".$section."'
                      AND grNo = '".$grNo."'
                      AND date >= '".$academicStartYear."-04-01'
                      AND date <= '".$academicEndYear."-03-31'
                      AND attendences = 'P'";
    $selectAtnPRes = mysql_query($selectAtnP);
    $countP = mysql_num_rows($selectAtnPRes);
    
    $selectAtnA = "SELECT attendences
                     FROM attendence
                    WHERE class = '".$class."'
                      AND section = '".$section."'
                      AND grNo = '".$grNo."'
                      AND date >= '".$academicStartYear."-04-01'
                      AND date <= '".$academicEndYear."-03-31'
                      AND attendences = 'A'";
    $selectAtnARes = mysql_query($selectAtnA);
    $countA = mysql_num_rows($selectAtnARes);
    $totalNumberWorkingDay = $countA + $countP;
    
    $pdf->SetFont('Arial', '', 12);
	  $pdf->SetXY(5,$yAxis+40);
	  $pdf->Cell(40, 10, 'Attendance : '.$countP.'/'.$totalNumberWorkingDay, 0,0,'L',0);
	  $pdf->Cell(20, 10, '', 0,0,'C');
	  $pdf->Cell(30, 10, 'Height : '.$height, 0,0,'L');
	  $pdf->Cell(30, 10, 'Weight : '.$weight, 0,0,'L');
	  $pdf->Cell(80, 10, '', 0,0,'C');
	  
	  $pdf->SetFillColor(232, 232, 232);
	  $pdf->SetFont('Arial', '', 12);
	  $pdf->SetXY(5,$yAxis+50);
	  $pdf->Cell(50, 5, 'Class Teacher Remarks : ', 0, 0, 'L', 0);
	  $pdf->Cell(150,5, $resultRemark1, 0, 0, 'L', 0);
	  
	  $pdf->Image('./studentImage/stamp1.jpg',135,225,30,25);
	  $pdf->Image('./studentImage/sign.png',170,240,30,10);
	  
	  $pdf->SetFillColor(232, 232, 232);
	  $pdf->SetFont('Arial', '', 12);
	  $pdf->SetXY(5,$yAxis+90);
	  $pdf->Cell(100, 5, 'Class Teachers Signature', 0, 0, 'L', 0);
	  
	  $pdf->Cell(100, 5, 'Principals Signature & Seal', 0, 0, 'R', 0);
	  
	  $pdf->SetXY(5,$yAxis+105);
//	  $pdf->Cell(200, 5, 'Note : Students must compulsorily pass in theory and practical exams separately.', 0, 0, 'L', 0);
	  
	  $yAxis = $yAxis + $rowHeight;
    $i = $i + 1;
  }
  
  $pdf->Output();
}
?>
<?php

include "include/config.inc.php";
if (!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student') {
    $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
    header("Location:checkLogin.php");
} else {
    $class = '';
    $count = 0;
    $today = date('Y-m-d');
    $stdArray = array();
    $section = '';
    $subjectName = '';

    if (isset($_REQUEST['startYear'])) {
        $academicStartYear = $_REQUEST['startYear'] . "-04-01";
        $nextYear = $_REQUEST['startYear'] + 1;
        $academicEndYear = $nextYear . "-03-31";
        $academicStartYearSelected = $_REQUEST['startYear'];
    } else {
        $todayAcademic = date('m-d');
        if ($todayAcademic >= '04-01' && $todayAcademic <= '12-31') {
            $academicStartYear = date('Y') . "-04-01";
            $nextYear = date('Y') + 1;
            $academicEndYear = $nextYear . "-03-31";
            $academicStartYearSelected = date('Y');
        } else {
            $prevYear = date('Y') - 1;
            $academicStartYear = $prevYear . "-04-01";
            $academicEndYear = date('Y') . "-03-31";
            $academicStartYearSelected = $prevYear;
        }
    }

    $subjectAltId = isset($_REQUEST['subjectAltId']) && $_REQUEST['subjectAltId'] != '' ? $_REQUEST['subjectAltId'] : '0_0_0';

    $subjectAltIdExp = explode("_", $subjectAltId);
    $subjectAltIdExp[0];
    $subjectAltIdExp[1];
    $subjectAltIdExp[2];

    if (isset($_REQUEST['class'])) {
        $class = isset($_REQUEST['class']) && $_REQUEST['class'] != '' ? $_REQUEST['class'] : 0;
        $section = isset($_REQUEST['section']) && $_REQUEST['section'] != '' ? $_REQUEST['section'] : 0;
        $subjectMasterId = isset($_REQUEST['subjectMasterId']) && $_REQUEST['subjectMasterId'] != '' ? $_REQUEST['subjectMasterId'] : 0;
    } else {
        $class = $subjectAltIdExp[0];
        $section = $subjectAltIdExp[1];
        $subjectMasterId = $subjectAltIdExp[2];
    }

    if (isset($_POST['submitTaken'])) {
        if (isset($_POST['subjectMasterId']) && ($_POST['subjectMasterId'] == 2)) {
            $loopCount = 0;
            while ($loopCount < count($_POST['sub1Fa1'])) {
                $examMarksPerTryId = ($_POST['examMarksPerTryId'][$loopCount] != '') ? $_POST['examMarksPerTryId'][$loopCount] : 0;

                $sub1Fa1 = ($_POST['sub1Fa1'][$loopCount] != '') ? $_POST['sub1Fa1'][$loopCount] : 0;
                $sub1Fa2 = ($_POST['sub1Fa2'][$loopCount] != '') ? $_POST['sub1Fa2'][$loopCount] : 0;
                $sub1Sa1 = ($_POST['sub1Sa1'][$loopCount] != '') ? $_POST['sub1Sa1'][$loopCount] : 0;
                $sub1Fa3 = ($_POST['sub1Fa3'][$loopCount] != '') ? $_POST['sub1Fa3'][$loopCount] : 0;
                $sub1Fa4 = ($_POST['sub1Fa4'][$loopCount] != '') ? $_POST['sub1Fa4'][$loopCount] : 0;
                $sub1Sa2 = ($_POST['sub1Sa2'][$loopCount] != '') ? $_POST['sub1Sa2'][$loopCount] : 0;
                if ($_POST['sub1Fa1'][$loopCount] != '' && $_POST['examMarksPerTryId'][$loopCount] > 0) {
                    $updateClass = "UPDATE exammarkspertry
                                   SET sub1Fa1 = " . $sub1Fa1 . ",
                                       sub1Fa2 = " . $sub1Fa2 . ",
                                       sub1Sa1 = " . $sub1Sa1 . ",
                                       sub1Fa3 = " . $sub1Fa3 . ",
                                       sub1Fa4 = " . $sub1Fa4 . ",
                                       sub1Sa2 = " . $sub1Sa2 . "
                                 WHERE examMarksPerTryId = " . $examMarksPerTryId . "";
                    $updateClassRes = om_query($updateClass);
                    if (!$updateClassRes) {
                        echo "Update Fail";
                    } else {
                        header("Location:marksEntryPerformaTry.php?startYear=" . $academicStartYearSelected . "&subjectAltId=" . $subjectAltId . "&go=go");
                    }
                }
                $loopCount++;
            }
        }
        // start by mitesh //
        if (isset($_POST['subjectMasterId']) && ($_POST['subjectMasterId'] == 4)) {
            $loopCount = 0;
            while ($loopCount < count($_POST['sub4Fa1'])) {
                $examMarksPerTryId = ($_POST['examMarksPerTryId'][$loopCount] != '') ? $_POST['examMarksPerTryId'][$loopCount] : 0;

                $sub4Fa1 = ($_POST['sub4Fa1'][$loopCount] != '') ? $_POST['sub4Fa1'][$loopCount] : 0;
                $sub4Fa2 = ($_POST['sub4Fa2'][$loopCount] != '') ? $_POST['sub4Fa2'][$loopCount] : 0;
                $sub4Sa1 = ($_POST['sub4Sa1'][$loopCount] != '') ? $_POST['sub4Sa1'][$loopCount] : 0;
                $sub4Fa3 = ($_POST['sub4Fa3'][$loopCount] != '') ? $_POST['sub4Fa3'][$loopCount] : 0;
                $sub4Fa4 = ($_POST['sub4Fa4'][$loopCount] != '') ? $_POST['sub4Fa4'][$loopCount] : 0;
                $sub4Sa2 = ($_POST['sub4Sa2'][$loopCount] != '') ? $_POST['sub4Sa2'][$loopCount] : 0;
                if ($_POST['sub4Fa1'][$loopCount] != '' && $_POST['examMarksPerTryId'][$loopCount] > 0) {
                    $updateClass = "UPDATE exammarkspertry
                                   SET sub4Fa1 = " . $sub4Fa1 . ",
                                       sub4Fa2 = " . $sub4Fa2 . ",
                                       sub4Sa1 = " . $sub4Sa1 . ",
                                       sub4Fa3 = " . $sub4Fa3 . ",
                                       sub4Fa4 = " . $sub4Fa4 . ",
                                       sub4Sa2 = " . $sub4Sa2 . "
                                 WHERE examMarksPerTryId = " . $examMarksPerTryId . "";
                    $updateClassRes = om_query($updateClass);
                    if (!$updateClassRes) {
                        echo "Update Fail";
                    } else {
                        header("Location:marksEntryPerformaTry.php?startYear=" . $academicStartYearSelected . "&subjectAltId=" . $subjectAltId . "&go=go");
                    }
                }
                $loopCount++;
            }
        }
        if (isset($_POST['subjectMasterId']) && ($_POST['subjectMasterId'] == 5)) {
            $loopCount = 0;
            while ($loopCount < count($_POST['sub5Fa1'])) {
                $examMarksPerTryId = ($_POST['examMarksPerTryId'][$loopCount] != '') ? $_POST['examMarksPerTryId'][$loopCount] : 0;

                $sub5Fa1 = ($_POST['sub5Fa1'][$loopCount] != '') ? $_POST['sub5Fa1'][$loopCount] : 0;
                $sub5Fa2 = ($_POST['sub5Fa2'][$loopCount] != '') ? $_POST['sub5Fa2'][$loopCount] : 0;
                $sub5Sa1 = ($_POST['sub5Sa1'][$loopCount] != '') ? $_POST['sub5Sa1'][$loopCount] : 0;
                $sub5Fa3 = ($_POST['sub5Fa3'][$loopCount] != '') ? $_POST['sub5Fa3'][$loopCount] : 0;
                $sub5Fa4 = ($_POST['sub5Fa4'][$loopCount] != '') ? $_POST['sub5Fa4'][$loopCount] : 0;
                $sub5Sa2 = ($_POST['sub5Sa2'][$loopCount] != '') ? $_POST['sub5Sa2'][$loopCount] : 0;
                if ($_POST['sub5Fa1'][$loopCount] != '' && $_POST['examMarksPerTryId'][$loopCount] > 0) {
                    $updateClass = "UPDATE exammarkspertry
                                   SET sub5Fa1 = " . $sub5Fa1 . ",
                                       sub5Fa2 = " . $sub5Fa2 . ",
                                       sub5Sa1 = " . $sub5Sa1 . ",
                                       sub5Fa3 = " . $sub5Fa3 . ",
                                       sub5Fa4 = " . $sub5Fa4 . ",
                                       sub5Sa2 = " . $sub5Sa2 . "
                                 WHERE examMarksPerTryId = " . $examMarksPerTryId . "";
                    $updateClassRes = om_query($updateClass);
                    if (!$updateClassRes) {
                        echo "Update Fail";
                    } else {
                        header("Location:marksEntryPerformaTry.php?startYear=" . $academicStartYearSelected . "&subjectAltId=" . $subjectAltId . "&go=go");
                    }
                }
                $loopCount++;
            }
        }
        if (isset($_POST['subjectMasterId']) && ($_POST['subjectMasterId'] == 6)) {
            $loopCount = 0;
            while ($loopCount < count($_POST['sub6Fa1'])) {
                $examMarksPerTryId = ($_POST['examMarksPerTryId'][$loopCount] != '') ? $_POST['examMarksPerTryId'][$loopCount] : 0;

                $sub6Fa1 = ($_POST['sub6Fa1'][$loopCount] != '') ? $_POST['sub6Fa1'][$loopCount] : 0;
                $sub6Fa2 = ($_POST['sub6Fa2'][$loopCount] != '') ? $_POST['sub6Fa2'][$loopCount] : 0;
                $sub6Sa1 = ($_POST['sub6Sa1'][$loopCount] != '') ? $_POST['sub6Sa1'][$loopCount] : 0;
                $sub6Fa3 = ($_POST['sub6Fa3'][$loopCount] != '') ? $_POST['sub6Fa3'][$loopCount] : 0;
                $sub6Fa4 = ($_POST['sub6Fa4'][$loopCount] != '') ? $_POST['sub6Fa4'][$loopCount] : 0;
                $sub6Sa2 = ($_POST['sub6Sa2'][$loopCount] != '') ? $_POST['sub6Sa2'][$loopCount] : 0;
                if ($_POST['sub6Fa1'][$loopCount] != '' && $_POST['examMarksPerTryId'][$loopCount] > 0) {
                    $updateClass = "UPDATE exammarkspertry
                                   SET sub6Fa1 = " . $sub6Fa1 . ",
                                       sub6Fa2 = " . $sub6Fa2 . ",
                                       sub6Sa1 = " . $sub6Sa1 . ",
                                       sub6Fa3 = " . $sub6Fa3 . ",
                                       sub6Fa4 = " . $sub6Fa4 . ",
                                       sub6Sa2 = " . $sub6Sa2 . "
                                 WHERE examMarksPerTryId = " . $examMarksPerTryId . "";
                    $updateClassRes = om_query($updateClass);
                    if (!$updateClassRes) {
                        echo "Update Fail";
                    } else {
                        header("Location:marksEntryPerformaTry.php?startYear=" . $academicStartYearSelected . "&subjectAltId=" . $subjectAltId . "&go=go");
                    }
                }
                $loopCount++;
            }
        }
        if (isset($_POST['subjectMasterId']) && ($_POST['subjectMasterId'] == 7)) {
            $loopCount = 0;
            while ($loopCount < count($_POST['sub1Fa1'])) {
                $examMarksPerTryId = ($_POST['examMarksPerTryId'][$loopCount] != '') ? $_POST['examMarksPerTryId'][$loopCount] : 0;

                $sub1Fa1 = ($_POST['sub1Fa1'][$loopCount] != '') ? $_POST['sub1Fa1'][$loopCount] : 0;
                $sub1Fa2 = ($_POST['sub1Fa2'][$loopCount] != '') ? $_POST['sub1Fa2'][$loopCount] : 0;
                $sub1Sa1 = ($_POST['sub1Sa1'][$loopCount] != '') ? $_POST['sub1Sa1'][$loopCount] : 0;
                $sub1Fa3 = ($_POST['sub1Fa3'][$loopCount] != '') ? $_POST['sub1Fa3'][$loopCount] : 0;
                $sub1Fa4 = ($_POST['sub1Fa4'][$loopCount] != '') ? $_POST['sub1Fa4'][$loopCount] : 0;
                $sub1Sa2 = ($_POST['sub1Sa2'][$loopCount] != '') ? $_POST['sub1Sa2'][$loopCount] : 0;
                if ($_POST['sub1Fa1'][$loopCount] != '' && $_POST['examMarksPerTryId'][$loopCount] > 0) {
                    $updateClass = "UPDATE exammarkspertry
                                   SET sub1Fa1 = " . $sub1Fa1 . ",
                                       sub1Fa2 = " . $sub1Fa2 . ",
                                       sub1Sa1 = " . $sub1Sa1 . ",
                                       sub1Fa3 = " . $sub1Fa3 . ",
                                       sub1Fa4 = " . $sub1Fa4 . ",
                                       sub1Sa2 = " . $sub1Sa2 . "
                                 WHERE examMarksPerTryId = " . $examMarksPerTryId . "";
                    $updateClassRes = om_query($updateClass);
                    if (!$updateClassRes) {
                        echo "Update Fail";
                    } else {
                        header("Location:marksEntryPerformaTry.php?startYear=" . $academicStartYearSelected . "&subjectAltId=" . $subjectAltId . "&go=go");
                    }
                }
                $loopCount++;
            }
        }
        if (isset($_POST['subjectMasterId']) && ($_POST['subjectMasterId'] == 8)) {
            $loopCount = 0;
            while ($loopCount < count($_POST['sub1Fa1'])) {
                $examMarksPerTryId = ($_POST['examMarksPerTryId'][$loopCount] != '') ? $_POST['examMarksPerTryId'][$loopCount] : 0;

                $sub1Fa1 = ($_POST['sub1Fa1'][$loopCount] != '') ? $_POST['sub1Fa1'][$loopCount] : 0;
                $sub1Fa2 = ($_POST['sub1Fa2'][$loopCount] != '') ? $_POST['sub1Fa2'][$loopCount] : 0;
                $sub1Sa1 = ($_POST['sub1Sa1'][$loopCount] != '') ? $_POST['sub1Sa1'][$loopCount] : 0;
                $sub1Fa3 = ($_POST['sub1Fa3'][$loopCount] != '') ? $_POST['sub1Fa3'][$loopCount] : 0;
                $sub1Fa4 = ($_POST['sub1Fa4'][$loopCount] != '') ? $_POST['sub1Fa4'][$loopCount] : 0;
                $sub1Sa2 = ($_POST['sub1Sa2'][$loopCount] != '') ? $_POST['sub1Sa2'][$loopCount] : 0;
                if ($_POST['sub1Fa1'][$loopCount] != '' && $_POST['examMarksPerTryId'][$loopCount] > 0) {
                    $updateClass = "UPDATE exammarkspertry
                                   SET sub1Fa1 = " . $sub1Fa1 . ",
                                       sub1Fa2 = " . $sub1Fa2 . ",
                                       sub1Sa1 = " . $sub1Sa1 . ",
                                       sub1Fa3 = " . $sub1Fa3 . ",
                                       sub1Fa4 = " . $sub1Fa4 . ",
                                       sub1Sa2 = " . $sub1Sa2 . "
                                 WHERE examMarksPerTryId = " . $examMarksPerTryId . "";
                    $updateClassRes = om_query($updateClass);
                    if (!$updateClassRes) {
                        echo "Update Fail";
                    } else {
                        header("Location:marksEntryPerformaTry.php?startYear=" . $academicStartYearSelected . "&subjectAltId=" . $subjectAltId . "&go=go");
                    }
                }
                $loopCount++;
            }
        }
        if (isset($_POST['subjectMasterId']) && ($_POST['subjectMasterId'] == 9)) {
            $loopCount = 0;
            while ($loopCount < count($_POST['sub1Fa1'])) {
                $examMarksPerTryId = ($_POST['examMarksPerTryId'][$loopCount] != '') ? $_POST['examMarksPerTryId'][$loopCount] : 0;

                $sub1Fa1 = ($_POST['sub1Fa1'][$loopCount] != '') ? $_POST['sub1Fa1'][$loopCount] : 0;
                $sub1Fa2 = ($_POST['sub1Fa2'][$loopCount] != '') ? $_POST['sub1Fa2'][$loopCount] : 0;
                $sub1Sa1 = ($_POST['sub1Sa1'][$loopCount] != '') ? $_POST['sub1Sa1'][$loopCount] : 0;
                $sub1Fa3 = ($_POST['sub1Fa3'][$loopCount] != '') ? $_POST['sub1Fa3'][$loopCount] : 0;
                $sub1Fa4 = ($_POST['sub1Fa4'][$loopCount] != '') ? $_POST['sub1Fa4'][$loopCount] : 0;
                $sub1Sa2 = ($_POST['sub1Sa2'][$loopCount] != '') ? $_POST['sub1Sa2'][$loopCount] : 0;
                if ($_POST['sub1Fa1'][$loopCount] != '' && $_POST['examMarksPerTryId'][$loopCount] > 0) {
                    $updateClass = "UPDATE exammarkspertry
                                   SET sub1Fa1 = " . $sub1Fa1 . ",
                                       sub1Fa2 = " . $sub1Fa2 . ",
                                       sub1Sa1 = " . $sub1Sa1 . ",
                                       sub1Fa3 = " . $sub1Fa3 . ",
                                       sub1Fa4 = " . $sub1Fa4 . ",
                                       sub1Sa2 = " . $sub1Sa2 . "
                                 WHERE examMarksPerTryId = " . $examMarksPerTryId . "";
                    $updateClassRes = om_query($updateClass);
                    if (!$updateClassRes) {
                        echo "Update Fail";
                    } else {
                        header("Location:marksEntryPerformaTry.php?startYear=" . $academicStartYearSelected . "&subjectAltId=" . $subjectAltId . "&go=go");
                    }
                }
                $loopCount++;
            }
        }
        if (isset($_POST['subjectMasterId']) && ($_POST['subjectMasterId'] == 10)) {
            $loopCount = 0;
            while ($loopCount < count($_POST['sub1Fa1'])) {
                $examMarksPerTryId = ($_POST['examMarksPerTryId'][$loopCount] != '') ? $_POST['examMarksPerTryId'][$loopCount] : 0;

                $sub1Fa1 = ($_POST['sub1Fa1'][$loopCount] != '') ? $_POST['sub1Fa1'][$loopCount] : 0;
                $sub1Fa2 = ($_POST['sub1Fa2'][$loopCount] != '') ? $_POST['sub1Fa2'][$loopCount] : 0;
                $sub1Sa1 = ($_POST['sub1Sa1'][$loopCount] != '') ? $_POST['sub1Sa1'][$loopCount] : 0;
                $sub1Fa3 = ($_POST['sub1Fa3'][$loopCount] != '') ? $_POST['sub1Fa3'][$loopCount] : 0;
                $sub1Fa4 = ($_POST['sub1Fa4'][$loopCount] != '') ? $_POST['sub1Fa4'][$loopCount] : 0;
                $sub1Sa2 = ($_POST['sub1Sa2'][$loopCount] != '') ? $_POST['sub1Sa2'][$loopCount] : 0;
                if ($_POST['sub1Fa1'][$loopCount] != '' && $_POST['examMarksPerTryId'][$loopCount] > 0) {
                    $updateClass = "UPDATE exammarkspertry
                                   SET sub1Fa1 = " . $sub1Fa1 . ",
                                       sub1Fa2 = " . $sub1Fa2 . ",
                                       sub1Sa1 = " . $sub1Sa1 . ",
                                       sub1Fa3 = " . $sub1Fa3 . ",
                                       sub1Fa4 = " . $sub1Fa4 . ",
                                       sub1Sa2 = " . $sub1Sa2 . "
                                 WHERE examMarksPerTryId = " . $examMarksPerTryId . "";
                    $updateClassRes = om_query($updateClass);
                    if (!$updateClassRes) {
                        echo "Update Fail";
                    } else {
                        header("Location:marksEntryPerformaTry.php?startYear=" . $academicStartYearSelected . "&subjectAltId=" . $subjectAltId . "&go=go");
                    }
                }
                $loopCount++;
            }
        }
        if (isset($_POST['subjectMasterId']) && ($_POST['subjectMasterId'] == 18)) {
            $loopCount = 0;
            while ($loopCount < count($_POST['sub1Fa1'])) {
                $examMarksPerTryId = ($_POST['examMarksPerTryId'][$loopCount] != '') ? $_POST['examMarksPerTryId'][$loopCount] : 0;

                $sub1Fa1 = ($_POST['sub1Fa1'][$loopCount] != '') ? $_POST['sub1Fa1'][$loopCount] : 0;
                $sub1Fa2 = ($_POST['sub1Fa2'][$loopCount] != '') ? $_POST['sub1Fa2'][$loopCount] : 0;
                $sub1Sa1 = ($_POST['sub1Sa1'][$loopCount] != '') ? $_POST['sub1Sa1'][$loopCount] : 0;
                $sub1Fa3 = ($_POST['sub1Fa3'][$loopCount] != '') ? $_POST['sub1Fa3'][$loopCount] : 0;
                $sub1Fa4 = ($_POST['sub1Fa4'][$loopCount] != '') ? $_POST['sub1Fa4'][$loopCount] : 0;
                $sub1Sa2 = ($_POST['sub1Sa2'][$loopCount] != '') ? $_POST['sub1Sa2'][$loopCount] : 0;
                if ($_POST['sub1Fa1'][$loopCount] != '' && $_POST['examMarksPerTryId'][$loopCount] > 0) {
                    $updateClass = "UPDATE exammarkspertry
                                   SET sub1Fa1 = " . $sub1Fa1 . ",
                                       sub1Fa2 = " . $sub1Fa2 . ",
                                       sub1Sa1 = " . $sub1Sa1 . ",
                                       sub1Fa3 = " . $sub1Fa3 . ",
                                       sub1Fa4 = " . $sub1Fa4 . ",
                                       sub1Sa2 = " . $sub1Sa2 . "
                                 WHERE examMarksPerTryId = " . $examMarksPerTryId . "";
                    $updateClassRes = om_query($updateClass);
                    if (!$updateClassRes) {
                        echo "Update Fail";
                    } else {
                        header("Location:marksEntryPerformaTry.php?startYear=" . $academicStartYearSelected . "&subjectAltId=" . $subjectAltId . "&go=go");
                    }
                }
                $loopCount++;
            }
        }
        if (isset($_POST['subjectMasterId']) && ($_POST['subjectMasterId'] == 52)) {
            $loopCount = 0;
            while ($loopCount < count($_POST['sub1Fa1'])) {
                $examMarksPerTryId = ($_POST['examMarksPerTryId'][$loopCount] != '') ? $_POST['examMarksPerTryId'][$loopCount] : 0;

                $sub1Fa1 = ($_POST['sub1Fa1'][$loopCount] != '') ? $_POST['sub1Fa1'][$loopCount] : 0;
                $sub1Fa2 = ($_POST['sub1Fa2'][$loopCount] != '') ? $_POST['sub1Fa2'][$loopCount] : 0;
                $sub1Sa1 = ($_POST['sub1Sa1'][$loopCount] != '') ? $_POST['sub1Sa1'][$loopCount] : 0;
                $sub1Fa3 = ($_POST['sub1Fa3'][$loopCount] != '') ? $_POST['sub1Fa3'][$loopCount] : 0;
                $sub1Fa4 = ($_POST['sub1Fa4'][$loopCount] != '') ? $_POST['sub1Fa4'][$loopCount] : 0;
                $sub1Sa2 = ($_POST['sub1Sa2'][$loopCount] != '') ? $_POST['sub1Sa2'][$loopCount] : 0;
                if ($_POST['sub1Fa1'][$loopCount] != '' && $_POST['examMarksPerTryId'][$loopCount] > 0) {
                    $updateClass = "UPDATE exammarkspertry
                                   SET sub1Fa1 = " . $sub1Fa1 . ",
                                       sub1Fa2 = " . $sub1Fa2 . ",
                                       sub1Sa1 = " . $sub1Sa1 . ",
                                       sub1Fa3 = " . $sub1Fa3 . ",
                                       sub1Fa4 = " . $sub1Fa4 . ",
                                       sub1Sa2 = " . $sub1Sa2 . "
                                 WHERE examMarksPerTryId = " . $examMarksPerTryId . "";
                    $updateClassRes = om_query($updateClass);
                    if (!$updateClassRes) {
                        echo "Update Fail";
                    } else {
                        header("Location:marksEntryPerformaTry.php?startYear=" . $academicStartYearSelected . "&subjectAltId=" . $subjectAltId . "&go=go");
                    }
                }
                $loopCount++;
            }
        }
        if (isset($_POST['subjectMasterId']) && ($_POST['subjectMasterId'] == 53)) {
            $loopCount = 0;
            while ($loopCount < count($_POST['sub1Fa1'])) {
                $examMarksPerTryId = ($_POST['examMarksPerTryId'][$loopCount] != '') ? $_POST['examMarksPerTryId'][$loopCount] : 0;

                $sub1Fa1 = ($_POST['sub1Fa1'][$loopCount] != '') ? $_POST['sub1Fa1'][$loopCount] : 0;
                $sub1Fa2 = ($_POST['sub1Fa2'][$loopCount] != '') ? $_POST['sub1Fa2'][$loopCount] : 0;
                $sub1Sa1 = ($_POST['sub1Sa1'][$loopCount] != '') ? $_POST['sub1Sa1'][$loopCount] : 0;
                $sub1Fa3 = ($_POST['sub1Fa3'][$loopCount] != '') ? $_POST['sub1Fa3'][$loopCount] : 0;
                $sub1Fa4 = ($_POST['sub1Fa4'][$loopCount] != '') ? $_POST['sub1Fa4'][$loopCount] : 0;
                $sub1Sa2 = ($_POST['sub1Sa2'][$loopCount] != '') ? $_POST['sub1Sa2'][$loopCount] : 0;
                if ($_POST['sub1Fa1'][$loopCount] != '' && $_POST['examMarksPerTryId'][$loopCount] > 0) {
                    $updateClass = "UPDATE exammarkspertry
                                   SET sub1Fa1 = " . $sub1Fa1 . ",
                                       sub1Fa2 = " . $sub1Fa2 . ",
                                       sub1Sa1 = " . $sub1Sa1 . ",
                                       sub1Fa3 = " . $sub1Fa3 . ",
                                       sub1Fa4 = " . $sub1Fa4 . ",
                                       sub1Sa2 = " . $sub1Sa2 . "
                                 WHERE examMarksPerTryId = " . $examMarksPerTryId . "";
                    $updateClassRes = om_query($updateClass);
                    if (!$updateClassRes) {
                        echo "Update Fail";
                    } else {
                        header("Location:marksEntryPerformaTry.php?startYear=" . $academicStartYearSelected . "&subjectAltId=" . $subjectAltId . "&go=go");
                    }
                }
                $loopCount++;
            }
        }
        if (isset($_POST['subjectMasterId']) && ($_POST['subjectMasterId'] == 54)) {
            $loopCount = 0;
            while ($loopCount < count($_POST['sub1Fa1'])) {
                $examMarksPerTryId = ($_POST['examMarksPerTryId'][$loopCount] != '') ? $_POST['examMarksPerTryId'][$loopCount] : 0;

                $sub1Fa1 = ($_POST['sub1Fa1'][$loopCount] != '') ? $_POST['sub1Fa1'][$loopCount] : 0;
                $sub1Fa2 = ($_POST['sub1Fa2'][$loopCount] != '') ? $_POST['sub1Fa2'][$loopCount] : 0;
                $sub1Sa1 = ($_POST['sub1Sa1'][$loopCount] != '') ? $_POST['sub1Sa1'][$loopCount] : 0;
                $sub1Fa3 = ($_POST['sub1Fa3'][$loopCount] != '') ? $_POST['sub1Fa3'][$loopCount] : 0;
                $sub1Fa4 = ($_POST['sub1Fa4'][$loopCount] != '') ? $_POST['sub1Fa4'][$loopCount] : 0;
                $sub1Sa2 = ($_POST['sub1Sa2'][$loopCount] != '') ? $_POST['sub1Sa2'][$loopCount] : 0;
                if ($_POST['sub1Fa1'][$loopCount] != '' && $_POST['examMarksPerTryId'][$loopCount] > 0) {
                    $updateClass = "UPDATE exammarkspertry
                                   SET sub1Fa1 = " . $sub1Fa1 . ",
                                       sub1Fa2 = " . $sub1Fa2 . ",
                                       sub1Sa1 = " . $sub1Sa1 . ",
                                       sub1Fa3 = " . $sub1Fa3 . ",
                                       sub1Fa4 = " . $sub1Fa4 . ",
                                       sub1Sa2 = " . $sub1Sa2 . "
                                 WHERE examMarksPerTryId = " . $examMarksPerTryId . "";
                    $updateClassRes = om_query($updateClass);
                    if (!$updateClassRes) {
                        echo "Update Fail";
                    } else {
                        header("Location:marksEntryPerformaTry.php?startYear=" . $academicStartYearSelected . "&subjectAltId=" . $subjectAltId . "&go=go");
                    }
                }
                $loopCount++;
            }
        }
        if (isset($_POST['subjectMasterId']) && ($_POST['subjectMasterId'] == 56)) {
            $loopCount = 0;
            while ($loopCount < count($_POST['sub1Fa1'])) {
                $examMarksPerTryId = ($_POST['examMarksPerTryId'][$loopCount] != '') ? $_POST['examMarksPerTryId'][$loopCount] : 0;

                $sub1Fa1 = ($_POST['sub1Fa1'][$loopCount] != '') ? $_POST['sub1Fa1'][$loopCount] : 0;
                $sub1Fa2 = ($_POST['sub1Fa2'][$loopCount] != '') ? $_POST['sub1Fa2'][$loopCount] : 0;
                $sub1Sa1 = ($_POST['sub1Sa1'][$loopCount] != '') ? $_POST['sub1Sa1'][$loopCount] : 0;
                $sub1Fa3 = ($_POST['sub1Fa3'][$loopCount] != '') ? $_POST['sub1Fa3'][$loopCount] : 0;
                $sub1Fa4 = ($_POST['sub1Fa4'][$loopCount] != '') ? $_POST['sub1Fa4'][$loopCount] : 0;
                $sub1Sa2 = ($_POST['sub1Sa2'][$loopCount] != '') ? $_POST['sub1Sa2'][$loopCount] : 0;
                if ($_POST['sub1Fa1'][$loopCount] != '' && $_POST['examMarksPerTryId'][$loopCount] > 0) {
                    $updateClass = "UPDATE exammarkspertry
                                   SET sub1Fa1 = " . $sub1Fa1 . ",
                                       sub1Fa2 = " . $sub1Fa2 . ",
                                       sub1Sa1 = " . $sub1Sa1 . ",
                                       sub1Fa3 = " . $sub1Fa3 . ",
                                       sub1Fa4 = " . $sub1Fa4 . ",
                                       sub1Sa2 = " . $sub1Sa2 . "
                                 WHERE examMarksPerTryId = " . $examMarksPerTryId . "";
                    $updateClassRes = om_query($updateClass);
                    if (!$updateClassRes) {
                        echo "Update Fail";
                    } else {
                        header("Location:marksEntryPerformaTry.php?startYear=" . $academicStartYearSelected . "&subjectAltId=" . $subjectAltId . "&go=go");
                    }
                }
                $loopCount++;
            }
        }
        if (isset($_POST['subjectMasterId']) && ($_POST['subjectMasterId'] == 57)) {
            $loopCount = 0;
            while ($loopCount < count($_POST['sub1Fa1'])) {
                $examMarksPerTryId = ($_POST['examMarksPerTryId'][$loopCount] != '') ? $_POST['examMarksPerTryId'][$loopCount] : 0;

                $sub1Fa1 = ($_POST['sub1Fa1'][$loopCount] != '') ? $_POST['sub1Fa1'][$loopCount] : 0;
                $sub1Fa2 = ($_POST['sub1Fa2'][$loopCount] != '') ? $_POST['sub1Fa2'][$loopCount] : 0;
                $sub1Sa1 = ($_POST['sub1Sa1'][$loopCount] != '') ? $_POST['sub1Sa1'][$loopCount] : 0;
                $sub1Fa3 = ($_POST['sub1Fa3'][$loopCount] != '') ? $_POST['sub1Fa3'][$loopCount] : 0;
                $sub1Fa4 = ($_POST['sub1Fa4'][$loopCount] != '') ? $_POST['sub1Fa4'][$loopCount] : 0;
                $sub1Sa2 = ($_POST['sub1Sa2'][$loopCount] != '') ? $_POST['sub1Sa2'][$loopCount] : 0;
                if ($_POST['sub1Fa1'][$loopCount] != '' && $_POST['examMarksPerTryId'][$loopCount] > 0) {
                    $updateClass = "UPDATE exammarkspertry
                                   SET sub1Fa1 = " . $sub1Fa1 . ",
                                       sub1Fa2 = " . $sub1Fa2 . ",
                                       sub1Sa1 = " . $sub1Sa1 . ",
                                       sub1Fa3 = " . $sub1Fa3 . ",
                                       sub1Fa4 = " . $sub1Fa4 . ",
                                       sub1Sa2 = " . $sub1Sa2 . "
                                 WHERE examMarksPerTryId = " . $examMarksPerTryId . "";
                    $updateClassRes = om_query($updateClass);
                    if (!$updateClassRes) {
                        echo "Update Fail";
                    } else {
                        header("Location:marksEntryPerformaTry.php?startYear=" . $academicStartYearSelected . "&subjectAltId=" . $subjectAltId . "&go=go");
                    }
                }
                $loopCount++;
            }
        }
        if (isset($_POST['subjectMasterId']) && ($_POST['subjectMasterId'] == 58)) {
            $loopCount = 0;
            while ($loopCount < count($_POST['sub1Fa1'])) {
                $examMarksPerTryId = ($_POST['examMarksPerTryId'][$loopCount] != '') ? $_POST['examMarksPerTryId'][$loopCount] : 0;

                $sub1Fa1 = ($_POST['sub1Fa1'][$loopCount] != '') ? $_POST['sub1Fa1'][$loopCount] : 0;
                $sub1Fa2 = ($_POST['sub1Fa2'][$loopCount] != '') ? $_POST['sub1Fa2'][$loopCount] : 0;
                $sub1Sa1 = ($_POST['sub1Sa1'][$loopCount] != '') ? $_POST['sub1Sa1'][$loopCount] : 0;
                $sub1Fa3 = ($_POST['sub1Fa3'][$loopCount] != '') ? $_POST['sub1Fa3'][$loopCount] : 0;
                $sub1Fa4 = ($_POST['sub1Fa4'][$loopCount] != '') ? $_POST['sub1Fa4'][$loopCount] : 0;
                $sub1Sa2 = ($_POST['sub1Sa2'][$loopCount] != '') ? $_POST['sub1Sa2'][$loopCount] : 0;
                if ($_POST['sub1Fa1'][$loopCount] != '' && $_POST['examMarksPerTryId'][$loopCount] > 0) {
                    $updateClass = "UPDATE exammarkspertry
                                   SET sub1Fa1 = " . $sub1Fa1 . ",
                                       sub1Fa2 = " . $sub1Fa2 . ",
                                       sub1Sa1 = " . $sub1Sa1 . ",
                                       sub1Fa3 = " . $sub1Fa3 . ",
                                       sub1Fa4 = " . $sub1Fa4 . ",
                                       sub1Sa2 = " . $sub1Sa2 . "
                                 WHERE examMarksPerTryId = " . $examMarksPerTryId . "";
                    $updateClassRes = om_query($updateClass);
                    if (!$updateClassRes) {
                        echo "Update Fail";
                    } else {
                        header("Location:marksEntryPerformaTry.php?startYear=" . $academicStartYearSelected . "&subjectAltId=" . $subjectAltId . "&go=go");
                    }
                }
                $loopCount++;
            }
        }
        if (isset($_POST['subjectMasterId']) && ($_POST['subjectMasterId'] == 60)) {
            $loopCount = 0;
            while ($loopCount < count($_POST['sub1Fa1'])) {
                $examMarksPerTryId = ($_POST['examMarksPerTryId'][$loopCount] != '') ? $_POST['examMarksPerTryId'][$loopCount] : 0;

                $sub1Fa1 = ($_POST['sub1Fa1'][$loopCount] != '') ? $_POST['sub1Fa1'][$loopCount] : 0;
                $sub1Fa2 = ($_POST['sub1Fa2'][$loopCount] != '') ? $_POST['sub1Fa2'][$loopCount] : 0;
                $sub1Sa1 = ($_POST['sub1Sa1'][$loopCount] != '') ? $_POST['sub1Sa1'][$loopCount] : 0;
                $sub1Fa3 = ($_POST['sub1Fa3'][$loopCount] != '') ? $_POST['sub1Fa3'][$loopCount] : 0;
                $sub1Fa4 = ($_POST['sub1Fa4'][$loopCount] != '') ? $_POST['sub1Fa4'][$loopCount] : 0;
                $sub1Sa2 = ($_POST['sub1Sa2'][$loopCount] != '') ? $_POST['sub1Sa2'][$loopCount] : 0;
                if ($_POST['sub1Fa1'][$loopCount] != '' && $_POST['examMarksPerTryId'][$loopCount] > 0) {
                    $updateClass = "UPDATE exammarkspertry
                                   SET sub1Fa1 = " . $sub1Fa1 . ",
                                       sub1Fa2 = " . $sub1Fa2 . ",
                                       sub1Sa1 = " . $sub1Sa1 . ",
                                       sub1Fa3 = " . $sub1Fa3 . ",
                                       sub1Fa4 = " . $sub1Fa4 . ",
                                       sub1Sa2 = " . $sub1Sa2 . "
                                 WHERE examMarksPerTryId = " . $examMarksPerTryId . "";
                    $updateClassRes = om_query($updateClass);
                    if (!$updateClassRes) {
                        echo "Update Fail";
                    } else {
                        header("Location:marksEntryPerformaTry.php?startYear=" . $academicStartYearSelected . "&subjectAltId=" . $subjectAltId . "&go=go");
                    }
                }
                $loopCount++;
            }
        }
        // end by mitesh //
        if (isset($_POST['subjectMasterId']) && ($_POST['subjectMasterId'] == 3)) {
            $loopCount = 0;
            while ($loopCount < count($_POST['sub2Fa1'])) {
                $examMarksPerTryId = ($_POST['examMarksPerTryId'][$loopCount] != '') ? $_POST['examMarksPerTryId'][$loopCount] : 0;

                $sub2Fa1 = ($_POST['sub2Fa1'][$loopCount] != '') ? $_POST['sub2Fa1'][$loopCount] : 0;
                $sub2Fa2 = ($_POST['sub2Fa2'][$loopCount] != '') ? $_POST['sub2Fa2'][$loopCount] : 0;
                $sub2Sa1 = ($_POST['sub2Sa1'][$loopCount] != '') ? $_POST['sub2Sa1'][$loopCount] : 0;
                $sub2Fa3 = ($_POST['sub2Fa3'][$loopCount] != '') ? $_POST['sub2Fa3'][$loopCount] : 0;
                $sub2Fa4 = ($_POST['sub2Fa4'][$loopCount] != '') ? $_POST['sub2Fa4'][$loopCount] : 0;
                $sub2Sa2 = ($_POST['sub2Sa2'][$loopCount] != '') ? $_POST['sub2Sa2'][$loopCount] : 0;
                if ($_POST['sub2Fa1'][$loopCount] != '' && $_POST['examMarksPerTryId'][$loopCount] > 0) {
                    $updateClass = "UPDATE exammarkspertry
                                   SET sub2Fa1 = " . $sub2Fa1 . ",
                                       sub2Fa2 = " . $sub2Fa2 . ",
                                       sub2Sa1 = " . $sub2Sa1 . ",
                                       sub2Fa3 = " . $sub2Fa3 . ",
                                       sub2Fa4 = " . $sub2Fa4 . ",
                                       sub2Sa2 = " . $sub2Sa2 . "
                                 WHERE examMarksPerTryId = " . $examMarksPerTryId . "";
                    $updateClassRes = om_query($updateClass);
                    if (!$updateClassRes) {
                        echo "Update Fail";
                    } else {
                        header("Location:marksEntryPerformaTry.php?startYear=" . $academicStartYearSelected . "&subjectAltId=" . $subjectAltId . "&go=go");
                    }
                }
                $loopCount++;
            }
        }

        if (isset($_POST['subjectMasterId']) && ($_POST['subjectMasterId'] == 1)) {
            $loopCount = 0;
            while ($loopCount < count($_POST['sub3Fa1'])) {
                $examMarksPerTryId = ($_POST['examMarksPerTryId'][$loopCount] != '') ? $_POST['examMarksPerTryId'][$loopCount] : 0;

                $sub3Fa1 = ($_POST['sub3Fa1'][$loopCount] != '') ? $_POST['sub3Fa1'][$loopCount] : 0;
                $sub3Fa2 = ($_POST['sub3Fa2'][$loopCount] != '') ? $_POST['sub3Fa2'][$loopCount] : 0;
                $sub3Sa1 = ($_POST['sub3Sa1'][$loopCount] != '') ? $_POST['sub3Sa1'][$loopCount] : 0;
                $sub3Fa3 = ($_POST['sub3Fa3'][$loopCount] != '') ? $_POST['sub3Fa3'][$loopCount] : 0;
                $sub3Fa4 = ($_POST['sub3Fa4'][$loopCount] != '') ? $_POST['sub3Fa4'][$loopCount] : 0;
                $sub3Sa2 = ($_POST['sub3Sa2'][$loopCount] != '') ? $_POST['sub3Sa2'][$loopCount] : 0;
                if ($_POST['sub3Fa1'][$loopCount] != '' && $_POST['examMarksPerTryId'][$loopCount] > 0) {
                    $updateClass = "UPDATE exammarkspertry
                                   SET sub3Fa1 = " . $sub3Fa1 . ",
                                       sub3Fa2 = " . $sub3Fa2 . ",
                                       sub3Sa1 = " . $sub3Sa1 . ",
                                       sub3Fa3 = " . $sub3Fa3 . ",
                                       sub3Fa4 = " . $sub3Fa4 . ",
                                       sub3Sa2 = " . $sub3Sa2 . "
                                 WHERE examMarksPerTryId = " . $examMarksPerTryId . "";
                    $updateClassRes = om_query($updateClass);
                    if (!$updateClassRes) {
                        echo "Update Fail";
                    } else {
                        header("Location:marksEntryPerformaTry.php?startYear=" . $academicStartYearSelected . "&subjectAltId=" . $subjectAltId . "&go=go");
                    }
                }
                $loopCount++;
            }
        }

        if (isset($_POST['subjectMasterId']) && ($_POST['subjectMasterId'] == 85)) {
            $loopCount = 0;
            while ($loopCount < count($_POST['sub4Fa1'])) {
                $examMarksPerTryId = ($_POST['examMarksPerTryId'][$loopCount] != '') ? $_POST['examMarksPerTryId'][$loopCount] : 0;

                $sub4Fa1 = ($_POST['sub4Fa1'][$loopCount] != '') ? $_POST['sub4Fa1'][$loopCount] : 0;
                $sub4Fa2 = ($_POST['sub4Fa2'][$loopCount] != '') ? $_POST['sub4Fa2'][$loopCount] : 0;
                $sub4Sa1 = ($_POST['sub4Sa1'][$loopCount] != '') ? $_POST['sub4Sa1'][$loopCount] : 0;
                $sub4Fa3 = ($_POST['sub4Fa3'][$loopCount] != '') ? $_POST['sub4Fa3'][$loopCount] : 0;
                $sub4Fa4 = ($_POST['sub4Fa4'][$loopCount] != '') ? $_POST['sub4Fa4'][$loopCount] : 0;
                $sub4Sa2 = ($_POST['sub4Sa2'][$loopCount] != '') ? $_POST['sub4Sa2'][$loopCount] : 0;
                if ($_POST['sub4Fa1'][$loopCount] != '' && $_POST['examMarksPerTryId'][$loopCount] > 0) {
                    $updateClass = "UPDATE exammarkspertry
                                   SET sub4Fa1 = " . $sub4Fa1 . ",
                                       sub4Fa2 = " . $sub4Fa2 . ",
                                       sub4Sa1 = " . $sub4Sa1 . ",
                                       sub4Fa3 = " . $sub4Fa3 . ",
                                       sub4Fa4 = " . $sub4Fa4 . ",
                                       sub4Sa2 = " . $sub4Sa2 . "
                                 WHERE examMarksPerTryId = " . $examMarksPerTryId . "";
                    $updateClassRes = om_query($updateClass);
                    if (!$updateClassRes) {
                        echo "Update Fail";
                    } else {
                        header("Location:marksEntryPerformaTry.php?startYear=" . $academicStartYearSelected . "&subjectAltId=" . $subjectAltId . "&go=go");
                    }
                }
                $loopCount++;
            }
        }

        if (isset($_POST['subjectMasterId']) && ($_POST['subjectMasterId'] == 167)) {
            $loopCount = 0;
            while ($loopCount < count($_POST['sub5Fa1'])) {
                $examMarksPerTryId = ($_POST['examMarksPerTryId'][$loopCount] != '') ? $_POST['examMarksPerTryId'][$loopCount] : 0;

                $sub5Fa1 = ($_POST['sub5Fa1'][$loopCount] != '') ? $_POST['sub5Fa1'][$loopCount] : 0;
                $sub5Fa2 = ($_POST['sub5Fa2'][$loopCount] != '') ? $_POST['sub5Fa2'][$loopCount] : 0;
                $sub5Sa1 = ($_POST['sub5Sa1'][$loopCount] != '') ? $_POST['sub5Sa1'][$loopCount] : 0;
                $sub5Fa3 = ($_POST['sub5Fa3'][$loopCount] != '') ? $_POST['sub5Fa3'][$loopCount] : 0;
                $sub5Fa4 = ($_POST['sub5Fa4'][$loopCount] != '') ? $_POST['sub5Fa4'][$loopCount] : 0;
                $sub5Sa2 = ($_POST['sub5Sa2'][$loopCount] != '') ? $_POST['sub5Sa2'][$loopCount] : 0;
                if ($_POST['sub5Fa1'][$loopCount] != '' && $_POST['examMarksPerTryId'][$loopCount] > 0) {
                    $updateClass = "UPDATE exammarkspertry
                                   SET sub5Fa1 = " . $sub5Fa1 . ",
                                       sub5Fa2 = " . $sub5Fa2 . ",
                                       sub5Sa1 = " . $sub5Sa1 . ",
                                       sub5Fa3 = " . $sub5Fa3 . ",
                                       sub5Fa4 = " . $sub5Fa4 . ",
                                       sub5Sa2 = " . $sub5Sa2 . "
                                 WHERE examMarksPerTryId = " . $examMarksPerTryId . "";
                    $updateClassRes = om_query($updateClass);
                    if (!$updateClassRes) {
                        echo "Update Fail";
                    } else {
                        header("Location:marksEntryPerformaTry.php?startYear=" . $academicStartYearSelected . "&subjectAltId=" . $subjectAltId . "&go=go");
                    }
                }
                $loopCount++;
            }
        }

        if (isset($_POST['subjectMasterId']) && ($_POST['subjectMasterId'] == 168 || $_POST['subjectMasterId'] == 55)) {
            $loopCount = 0;
            while ($loopCount < count($_POST['sub6Fa1'])) {
                $examMarksPerTryId = ($_POST['examMarksPerTryId'][$loopCount] != '') ? $_POST['examMarksPerTryId'][$loopCount] : 0;

                $sub6Fa1 = ($_POST['sub6Fa1'][$loopCount] != '') ? $_POST['sub6Fa1'][$loopCount] : 0;
                $sub6Fa2 = ($_POST['sub6Fa2'][$loopCount] != '') ? $_POST['sub6Fa2'][$loopCount] : 0;
                $sub6Sa1 = ($_POST['sub6Sa1'][$loopCount] != '') ? $_POST['sub6Sa1'][$loopCount] : 0;
                $sub6Fa3 = ($_POST['sub6Fa3'][$loopCount] != '') ? $_POST['sub6Fa3'][$loopCount] : 0;
                $sub6Fa4 = ($_POST['sub6Fa4'][$loopCount] != '') ? $_POST['sub6Fa4'][$loopCount] : 0;
                $sub6Sa2 = ($_POST['sub6Sa2'][$loopCount] != '') ? $_POST['sub6Sa2'][$loopCount] : 0;
                if ($_POST['sub6Fa1'][$loopCount] != '' && $_POST['examMarksPerTryId'][$loopCount] > 0) {
                    $updateClass = "UPDATE exammarkspertry
                                   SET sub6Fa1 = " . $sub6Fa1 . ",
                                       sub6Fa2 = " . $sub6Fa2 . ",
                                       sub6Sa1 = " . $sub6Sa1 . ",
                                       sub6Fa3 = " . $sub6Fa3 . ",
                                       sub6Fa4 = " . $sub6Fa4 . ",
                                       sub6Sa2 = " . $sub6Sa2 . "
                                 WHERE examMarksPerTryId = " . $examMarksPerTryId . "";
                    $updateClassRes = om_query($updateClass);
                    if (!$updateClassRes) {
                        echo "Update Fail";
                    } else {
                        header("Location:marksEntryPerformaTry.php?startYear=" . $academicStartYearSelected . "&subjectAltId=" . $subjectAltId . "&go=go");
                    }
                }
                $loopCount++;
            }
        }
    }

    $i = 0;
    $selectStd = "SELECT exammarkspertry.examMarksPerTryId,exammarkspertry.grNo,studentmaster.gender,studentmaster.studentName,
                       exammarkspertry.class,exammarkspertry.section,
                       sub1Fa1,sub1Fa2,sub1Sa1,sub1Fa3,sub1Fa4,sub1Sa2,
                                             sub2Fa1,sub2Fa2,sub2Sa1,sub2Fa3,sub2Fa4,sub2Sa2,
                                             sub3Fa1,sub3Fa2,sub3Sa1,sub3Fa3,sub3Fa4,sub3Sa2,
                                             sub4Fa1,sub4Fa2,sub4Sa1,sub4Fa3,sub4Fa4,sub4Sa2,
                                             sub5Fa1,sub5Fa2,sub5Sa1,sub5Fa3,sub5Fa4,sub5Sa2,
                                             sub6Fa1,sub6Fa2,sub6Sa1,sub6Fa3,sub6Fa4,sub6Sa2
                  FROM exammarkspertry
             LEFT JOIN studentmaster ON studentmaster.grNo = exammarkspertry.grNo
                 WHERE exammarkspertry.class = '" . $class . "'
                   AND exammarkspertry.section = '" . $section . "'
                   AND exammarkspertry.academicStartYear = '" . $academicStartYear . "'
                   AND exammarkspertry.academicEndYear = '" . $academicEndYear . "'
                   AND studentmaster.activated = 'Y'";
    
    $selectStdRes = mysql_query($selectStd);
    $count = mysql_num_rows($selectStdRes);
    while ($stdRow = mysql_fetch_array($selectStdRes)) {
        $selectRoll = "SELECT rollNo,optionSubject1
                     FROM nominalroll
                    WHERE grNo = '" . $stdRow['grNo'] . "'"
                . "AND class = '".$class."'"
                . "AND section = '".$section."'";
        $selectRollRes = mysql_query($selectRoll);
        while ($rollRow = mysql_fetch_array($selectRollRes)) {
            $stdArray[$i]['rollNo'] = $rollRow['rollNo'];
            
            if (isset($rollRow['optionSubject1']) && $rollRow['optionSubject1'] > 0) {
                $optSubjectQuery = mysql_query('SELECT subjectName FROM subjectmaster WHERE subjectMasterId = ' . $rollRow['optionSubject1']);
                $optSubjectNameRow = mysql_fetch_array($optSubjectQuery);

                $stdArray[$i]['subjectName'] = $optSubjectNameRow['subjectName'];
            } else {
                $stdArray[$i]['subjectName'] = "";
            }
        }
        $stdArray[$i]['examMarksPerTryId'] = $stdRow['examMarksPerTryId'];
        $stdArray[$i]['class'] = $stdRow['class'];
        $stdArray[$i]['section'] = $stdRow['section'];
        $stdArray[$i]['grNo'] = $stdRow['grNo'];
        $stdArray[$i]['gender'] = $stdRow['gender'];
        $stdArray[$i]['studentName'] = $stdRow['studentName'];

        $stdArray[$i]['sub1Fa1'] = $stdRow['sub1Fa1'];
        $stdArray[$i]['sub1Fa2'] = $stdRow['sub1Fa2'];
        $stdArray[$i]['sub1Sa1'] = $stdRow['sub1Sa1'];
        $stdArray[$i]['sub1Fa3'] = $stdRow['sub1Fa3'];
        $stdArray[$i]['sub1Fa4'] = $stdRow['sub1Fa4'];
        $stdArray[$i]['sub1Sa2'] = $stdRow['sub1Sa2'];

        $stdArray[$i]['sub2Fa1'] = $stdRow['sub2Fa1'];
        $stdArray[$i]['sub2Fa2'] = $stdRow['sub2Fa2'];
        $stdArray[$i]['sub2Sa1'] = $stdRow['sub2Sa1'];
        $stdArray[$i]['sub2Fa3'] = $stdRow['sub2Fa3'];
        $stdArray[$i]['sub2Fa4'] = $stdRow['sub2Fa4'];
        $stdArray[$i]['sub2Sa2'] = $stdRow['sub2Sa2'];

        $stdArray[$i]['sub3Fa1'] = $stdRow['sub3Fa1'];
        $stdArray[$i]['sub3Fa2'] = $stdRow['sub3Fa2'];
        $stdArray[$i]['sub3Sa1'] = $stdRow['sub3Sa1'];
        $stdArray[$i]['sub3Fa3'] = $stdRow['sub3Fa3'];
        $stdArray[$i]['sub3Fa4'] = $stdRow['sub3Fa4'];
        $stdArray[$i]['sub3Sa2'] = $stdRow['sub3Sa2'];

        $stdArray[$i]['sub4Fa1'] = $stdRow['sub4Fa1'];
        $stdArray[$i]['sub4Fa2'] = $stdRow['sub4Fa2'];
        $stdArray[$i]['sub4Sa1'] = $stdRow['sub4Sa1'];
        $stdArray[$i]['sub4Fa3'] = $stdRow['sub4Fa3'];
        $stdArray[$i]['sub4Fa4'] = $stdRow['sub4Fa4'];
        $stdArray[$i]['sub4Sa2'] = $stdRow['sub4Sa2'];

        $stdArray[$i]['sub5Fa1'] = $stdRow['sub5Fa1'];
        $stdArray[$i]['sub5Fa2'] = $stdRow['sub5Fa2'];
        $stdArray[$i]['sub5Sa1'] = $stdRow['sub5Sa1'];
        $stdArray[$i]['sub5Fa3'] = $stdRow['sub5Fa3'];
        $stdArray[$i]['sub5Fa4'] = $stdRow['sub5Fa4'];
        $stdArray[$i]['sub5Sa2'] = $stdRow['sub5Sa2'];

        $stdArray[$i]['sub6Fa1'] = $stdRow['sub6Fa1'];
        $stdArray[$i]['sub6Fa2'] = $stdRow['sub6Fa2'];
        $stdArray[$i]['sub6Sa1'] = $stdRow['sub6Sa1'];
        $stdArray[$i]['sub6Fa3'] = $stdRow['sub6Fa3'];
        $stdArray[$i]['sub6Fa4'] = $stdRow['sub6Fa4'];
        $stdArray[$i]['sub6Sa2'] = $stdRow['sub6Sa2'];
        $i++;
    }

    $scdArray = array();
    $f = 0;
    $selectId = "SELECT employeeMasterId
                 FROM employeemaster
                WHERE loginId = '" . $_SESSION['s_activName'] . "'";
    $selectIdRes = mysql_query($selectId);
    if ($subRow = mysql_fetch_array($selectIdRes)) {
        $selectSub = "SELECT subjectteacherallotment.class,subjectteacherallotment.section,subjectteacherallotment.subjectMasterId,subjectmaster.subjectName
                    FROM subjectteacherallotment
               LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = subjectteacherallotment.subjectMasterId
                   WHERE subjectteacherallotment.employeeMasterId = '" . $subRow['employeeMasterId'] . "'
                     AND subjectteacherallotment.academicStartYear = '" . $academicStartYear . "'
                     AND subjectteacherallotment.academicEndYear = '" . $academicEndYear . "'
                     AND subjectteacherallotment.class > 5
                     AND subjectteacherallotment.class <= 10";
        
        $selectSubRes = mysql_query($selectSub);
        while ($subRow = mysql_fetch_array($selectSubRes)) {
            $classTeacherSubjectClass = $subRow['class'];
            $classTeacherSubjectsection = $subRow['section'];
            $classTeachersubjectMasterId = $subRow['subjectMasterId'];
            $subjectName = $subRow['subjectName'];

            $scdArray['subjectAltId'][$f] = $subRow['class'] . '_' . $subRow['section'] . '_' . $subRow['subjectMasterId'];
            $scdArray['subjectDtl'][$f] = $subRow['class'] . '_' . $subRow['section'] . '_' . $subRow['subjectName'];
            $f++;
        }
    }

    $c = 0;
    $cArray = array();
    $selectClass = "SELECT DISTINCT className
                    FROM classmaster
                   WHERE className > 5
                     AND className <= 10";
    $selectClassRes = mysql_query($selectClass);
    while ($classRow = mysql_fetch_array($selectClassRes)) {
        $cArray['className'][$c] = $classRow['className'];
        $c++;
    }

    $k = 0;
    $selectClub = "SELECT subjectMasterId,subjectName
                   FROM subjectmaster
                  WHERE subjectType = 'Main' or subjectType = 'Optional'";
    $selectClubRes = mysql_query($selectClub);
    while ($clubRow = mysql_fetch_array($selectClubRes)) {
        $clubArr['subjectMasterId'][$k] = $clubRow['subjectMasterId'];
        $clubArr['subjectName'][$k] = $clubRow['subjectName'];
        $k++;
    }

    $secArrOut[0] = 'A';
    $secArrOut[1] = 'B';
    $secArrOut[2] = 'C';
    $secArrOut[3] = 'D';
    include("./bottom.php");
    $smarty->assign('academicStartYearSelected', $academicStartYearSelected);
    $smarty->assign('scdArray', $scdArray);
    $smarty->assign('secArrOut', $secArrOut);
    $smarty->assign('class', $class);
    $smarty->assign('section', $section);
    $smarty->assign('stdArray', $stdArray);
    $smarty->assign('count', $count);
    $smarty->assign('cArray', $cArray);
    $smarty->assign('clubArr', $clubArr);
    $smarty->assign('subjectMasterId', $subjectMasterId);
    $smarty->assign('subjectAltId', $subjectAltId);
    $smarty->assign('subjectName', $subjectName);
    $smarty->display('marksEntryPerformaTry.tpl');
}
?>

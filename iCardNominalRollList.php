<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	if(isset($_REQUEST['startYear']))
  {
	  $academicStartYear = $_REQUEST['startYear'];
	  $academicEndYear   = $_REQUEST['startYear'] + 1;
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y');
	  	$nextYear            = date('Y') + 1;
	  	$academicEndYear   = $nextYear;
		}
		else
		{
			$prevYear            = date('Y') - 1;
			$academicStartYear = $prevYear;
	  	$academicEndYear   = date('Y');
		}
	}
	
	$class   = isset($_REQUEST['class']) ? $_REQUEST['class'] : 0;
	$section = isset($_REQUEST['classSection']) ? $_REQUEST['classSection'] : 0;
	
	$nominalArr = array();
  $i = 0;
  $selectNominal = "SELECT nominalRollId,nominalroll.grNo,academicStartYear,academicEndYear,class,
                           section,rollNo,feeGroup,libraryMemberType,libraryMemberType,studentmaster.studentName,
                           busRoute,busStop,subjectGroup,coScholasticGroup,clubTerm1,clubTerm2,
                           boardRegistrionId,boardRollNo,activated,studentmaster.studentMasterId,
                           studentmaster.fatherMobile,studentmaster.studentMasterId,studentmaster.currentAddress,
						   studentmaster.dateOfBirth,studentmaster.bloodGroup,studentmaster.houseId,
						   nominalroll.busStopMasterId
                      FROM nominalroll
                 LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
                     WHERE nominalroll.class = '".$class."'
                       AND nominalroll.section = '".$section."'
                       AND nominalroll.academicStartYear = '".$academicStartYear."-04-01'
                       AND nominalroll.academicEndYear = '".$academicEndYear."-03-31'
                       AND studentmaster.activated = 'Y'
                  ORDER BY nominalroll.rollNo";
  $selectNominalRes = mysql_query($selectNominal);
  
  while($nominalRow = mysql_fetch_array($selectNominalRes))
  {
    $nominalArr[$i]['studentMasterId']    = $nominalRow['studentMasterId'];
    $nominalArr[$i]['nominalRollId']      = $nominalRow['nominalRollId'];
    $nominalArr[$i]['grNo']               = $nominalRow['grNo'];
    $nominalArr[$i]['currentAddress']     = $nominalRow['currentAddress'];
    $nominalArr[$i]['fatherMobile']       = $nominalRow['fatherMobile'];
    $nominalArr[$i]['academicYear']       = substr($nominalRow['academicStartYear'],0,4).'-'.substr($nominalRow['academicEndYear'],0,4);
    $nominalArr[$i]['class']              = $nominalRow['class'];
    $nominalArr[$i]['section']            = $nominalRow['section'];
    $nominalArr[$i]['rollNo']             = $nominalRow['rollNo'];
    $nominalArr[$i]['feeGroup']           = $nominalRow['feeGroup'];
    $nominalArr[$i]['libraryMemberType']  = $nominalRow['libraryMemberType'];
    $nominalArr[$i]['libraryMemberType']  = $nominalRow['libraryMemberType'];
    $nominalArr[$i]['studentName']        = $nominalRow['studentName'];
    $nominalArr[$i]['busRoute']           = $nominalRow['busRoute'];
    $nominalArr[$i]['busStop']            = $nominalRow['busStop'];
    $nominalArr[$i]['subjectGroup']       = $nominalRow['subjectGroup'];
    $nominalArr[$i]['coScholasticGroup']  = $nominalRow['coScholasticGroup'];
    $nominalArr[$i]['clubTerm1']          = $nominalRow['clubTerm1'];
    $nominalArr[$i]['clubTerm2']          = $nominalRow['clubTerm2'];
    $nominalArr[$i]['boardRegistrionId']  = $nominalRow['boardRegistrionId'];
    $nominalArr[$i]['boardRollNo']        = $nominalRow['boardRollNo'];
    $nominalArr[$i]['activated']          = $nominalRow['activated'];
	$nominalArr[$i]['dateOfBirth'] = date("d-m-Y",$nominalRow['dateOfBirth']);
	$nominalArr[$i]['bloodGroup'] = $nominalRow['bloodGroup'];
	$q = "select houseName from house where houseId = ".$nominalRow['houseId'];
	$rs = mysql_query($q);
	$nominalArr[$i]['houseId'] = mysql_result($rs,0,0);
	$q = "select vehicleMasterId from busstopmaster where busStopMasterId = ".$nominalRow['busStopMasterId'];
	$rs = mysql_query($q);
	$t = mysql_result($rs,0,0);
	$q = "select vehicleNo from vehiclemaster where vehicleMasterId = ".$t;
	$rs = mysql_query($q);
	$nominalArr[$i]['busStopMasterId'] = mysql_result($rs,0,0);
    $i++;
  }
  
  $c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  $secArrOut[0] = 'A';
  $secArrOut[1] = 'B';
  $secArrOut[2] = 'C';
  $secArrOut[3] = 'D';
  
  include("./bottom.php");
  $smarty->assign('nominalArr',$nominalArr);
  $smarty->assign('cArray',$cArray);
  $smarty->assign('class',$class);
  $smarty->assign('section',$section);
  $smarty->assign('secArrOut',$secArrOut);
  $smarty->assign('academicStartYear',$academicStartYear);
  $smarty->assign('academicEndYear',$academicEndYear);
  $smarty->display('iCardNominalRollList.tpl');  
}
?>
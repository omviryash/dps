<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$subjectMasterId = 0;
	$subjectName     = "";
	$isEdit  = 0;
	
  if(isset($_POST['Submit']))
  {
  	$subjectName     = isset($_POST['subjectName']) ? $_POST['subjectName'] : "";
  	$subjectMasterId = isset($_POST['subjectMasterId']) ? $_POST['subjectMasterId'] : 0;
  	
  	if($subjectMasterId == 0)
  	{
  	  $insertClass = "INSERT INTO subjectmaster (subjectName)
  	                  VALUES ('".$subjectName."')";
  	  $insertClassRes = om_query($insertClass);
  	  if(!$insertClassRes)
  	  {
  	  	echo "Insert Fail";
  	  }
  	  else
  	  {
  	  	header("Location:clubMasterEntry.php?done=1");
  	  }
  	}
  	else
  	{
  	  $isEdit = 1;
  	  $updateClass = "UPDATE subjectmaster
  	                     SET subjectName = '".$subjectName."'
  	                   WHERE subjectMasterId = ".$_REQUEST['subjectMasterId'];
      $updateClassRes = om_query($updateClass);
      if(!$updateClassRes)
      {
      	echo "Update Fail";
      }
      else
      {
      	header("Location:clubMasterEntry.php?done=1");
      }
  	}
  }
  
  if(isset($_REQUEST['subjectMasterId']) > 0)
  {
    $selectClass = "SELECT subjectmaster.subjectMasterId,subjectmaster.subjectName
                      FROM subjectmaster
                     WHERE subjectMasterId = ".$_REQUEST['subjectMasterId'];
    $selectClassRes = mysql_query($selectClass);
    if($classRow = mysql_fetch_array($selectClassRes))
    {
    	$subjectMasterId = $classRow['subjectMasterId'];
    	$subjectName     = $classRow['subjectName'];
    }
  }
  
  $secArrOut[0] = 'Club';
  
  include("./bottom.php");
  $smarty->assign('subjectMasterId',$subjectMasterId);
  $smarty->assign('isEdit',$isEdit);
  $smarty->assign('secArrOut',$secArrOut);
  $smarty->assign('subjectName',$subjectName);
  $smarty->display('clubMasterEntry.tpl');  
}
?>
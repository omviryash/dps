<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$class      = '';
  $count      = 0;
  $today      = date('Y-m-d');
	$stdArray   = array();
	$classTeacherSubjectClass = '';
	
	$academicStartYearSecondNum = '';
	
	$scheduleDateYear  = '';
	$scheduleDateMonth = '';
	$scheduleDate      = '';
	
	$examScheduleId = isset($_REQUEST['examScheduleId']) && $_REQUEST['examScheduleId'] != '' ? $_REQUEST['examScheduleId'] : 0;
	
	$selectClass = "SELECT class,scheduleDate
                    FROM examschedule
                   WHERE examScheduleId = '".$examScheduleId."'";
  $selectClassRes = mysql_query($selectClass);
  if($classRow = mysql_fetch_array($selectClassRes))
  {
  	$class        = $classRow['class'];
  	$scheduleDate = $classRow['scheduleDate'];
  	
  	$scheduleDateYear  = substr($classRow['scheduleDate'],0,4);
  	
  	$scheduleDateMonth = substr($classRow['scheduleDate'],5,5);
  }
  
  if(isset($_REQUEST['startYear']))
  {
  	$academicStartYearCombo = $_REQUEST['startYear']."-04-01";
  	$nextYearCombo          = $_REQUEST['startYear'] + 1;
  	$academicEndYearCobmo   = $nextYearCombo."-03-31";
  	$academicStartYearSelected = $_REQUEST['startYear'];
	}
	else
	{
	  $todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYearSelected = date('Y');
	  	$academicStartYearCombo = date('Y')."-04-01";
	  	$nextYearCombo          = date('Y') + 1;
	  	$academicEndYearCobmo   = $nextYearCombo."-03-31";
		}
		else
		{
			$prevYearCobmo          = date('Y') - 1;
			$academicStartYearSelected = $prevYearCobmo;
			$academicStartYearCombo = $prevYearCobmo."-04-01";
	  	$academicEndYearCobmo   = date('Y')."-03-31";
		}
	}
		
	$scdArray = array();
  $f = 0;
	$selectId = "SELECT employeeMasterId
                 FROM employeemaster
                WHERE loginId = '".$_SESSION['s_activName']."'";
  $selectIdRes = mysql_query($selectId);
  if($subRow = mysql_fetch_array($selectIdRes))
  {
  	$selectSub = "SELECT class,section,subjectMasterId
                    FROM subjectteacherallotment
                   WHERE employeeMasterId = '".$subRow['employeeMasterId']."'
                     AND academicStartYear = '".$academicStartYearCombo."'
                     AND academicEndYear = '".$academicEndYearCobmo."'";
	  $selectSubRes = mysql_query($selectSub);
	  while($subRow = mysql_fetch_array($selectSubRes))
	  {
	  	$classTeacherSubjectClass = $subRow['class'];
	  	$classTeachersubjectMasterId = $subRow['subjectMasterId'];
	  }
	  if($classTeacherSubjectClass != '')
  	{
  		$classQuery = "AND examschedule.class = '".$classTeacherSubjectClass."'
	                   AND examschedule.subjectMasterId = ".$classTeachersubjectMasterId."";
  	}
  	else
  	{
  		$classQuery = "AND 1 = 1";
  	}
//	  $selectScd = "SELECT DISTINCT examschedule.examScheduleId,examschedule.scheduleDate,examschedule.maxMarks,examschedule.minMarks,examschedule.class,
//	                       subjectmaster.subjectName,examschedule.studentView,examtype.examType,subjectteacherallotment.section,
//	                       marks
//	                  FROM examschedule
//	             LEFT JOIN subjectteacherallotment ON subjectteacherallotment.class = examschedule.class
//	             LEFT JOIN examtype ON examtype.examTypeId = examschedule.examTypeId
//	             LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = examschedule.subjectMasterId
//	             LEFT JOIN classmaster ON classmaster.className = subjectteacherallotment.class
//	             LEFT JOIN examMarks ON examMarks.examScheduleId = examschedule.examScheduleId
//	                 WHERE 1 = 1
//	                   ".$classQuery."
//	                   AND examschedule.scheduleDate >= '".$academicStartYearCombo."'
//                     AND examschedule.scheduleDate <= '".$academicEndYearCobmo."'
//                ORDER BY examschedule.scheduleDate,classmaster.priority,subjectteacherallotment.section";
//	  $selectScdRes = mysql_query($selectScd);
//	  while($scdRow = mysql_fetch_array($selectScdRes))
//	  {
//	  	$scdArray[$f]['examScheduleId']   = $scdRow['examScheduleId'];
//	  	$scdArray[$f]['scheduleDate']     = $scdRow['scheduleDate'];
//	  	$scdArray[$f]['examType']         = $scdRow['examType'];
//	  	$scdArray[$f]['class']            = $scdRow['class'];
//	  	$scdArray[$f]['section']          = $scdRow['section'];
//	  	$scdArray[$f]['subjectName']      = $scdRow['subjectName'];
//	  	$scdArray[$f]['marks']            = $scdRow['marks'];
//	  	if($scdRow['marks'] != '')
//	  	{
//	  	  $scdArray[$f]['status'] = 'Updated';
//	  	}
//	  	else
//	  	{
//	  		$scdArray[$f]['status'] = 'Pending';
//	  	}
//	  	
//	  	$f++;
//	  }
  	//change
  	///// preparing array for current year class and section
  	$currYrClassSection = array();
  	
  	$selClassSection = 'SELECT class, section FROM nominalroll WHERE academicStartYear="'.$academicStartYearCombo.'" GROUP BY class, section ORDER BY class ASC';
  	$resClassSection = mysql_query($selClassSection);
  	while($rowClassSection = mysql_fetch_array($resClassSection))
  	{
  		$currYrClassSection[] = $rowClassSection['class'].'=='.$rowClassSection['section'];
  	}

  	/*echo "<BR>========== <BR>";
  	print_r($currYrClassSection);

  	exit;*/

  	$currentDate = date('Y/m/d');
	$selectScd = "SELECT DISTINCT examschedule.examScheduleId,examschedule.scheduleDate,examschedule.maxMarks,examschedule.minMarks,examschedule.class,
	                       subjectmaster.subjectName,examschedule.studentView,examtype.examType,subjectteacherallotment.section
	                  FROM examschedule
	             LEFT JOIN subjectteacherallotment ON subjectteacherallotment.class = examschedule.class
	             LEFT JOIN examtype ON examtype.examTypeId = examschedule.examTypeId
	             LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = examschedule.subjectMasterId
	             LEFT JOIN classmaster ON classmaster.className = subjectteacherallotment.class
	                 WHERE 1 = 1
	                   ".$classQuery."
	                   AND examschedule.scheduleDate >= '".$academicStartYearCombo."'
                     AND examschedule.scheduleDate <= '".$currentDate."'
                ORDER BY examschedule.scheduleDate,classmaster.priority,subjectteacherallotment.section";
	  $selectScdRes = mysql_query($selectScd);
	  while($scdRow = mysql_fetch_array($selectScdRes))
	  {
	  	$classNSection = $scdRow['class'].'=='.$scdRow['section'];
	  	
	  	if(in_array($classNSection, $currYrClassSection))
	  	{
		  	$scdArray[$f]['marks'] = '';
		  	$scdArray[$f]['examScheduleId']   = $scdRow['examScheduleId'];
		  	$scdArray[$f]['scheduleDate']     = date("d-m-Y",  strtotime($scdRow['scheduleDate']));
		  	$scdArray[$f]['examType']         = $scdRow['examType'];
		  	$scdArray[$f]['class']            = $scdRow['class'];
		  	$scdArray[$f]['section']          = $scdRow['section'];
		  	$scdArray[$f]['subjectName']      = $scdRow['subjectName'];
		  	
		  	$selectMarks = "SELECT marks
		                      FROM exammarks
		                     WHERE examScheduleId = '".$scdRow['examScheduleId']."'";
		    $selectMarksRes = mysql_query($selectMarks);
		    if($mrRow = mysql_fetch_array($selectMarksRes))
		    {
		    	$scdArray[$f]['marks'] = $mrRow['marks'];
		    }
		  	
		  	if($scdArray[$f]['marks'] != '')
		  	{
		  	  $scdArray[$f]['status'] = 'Updated';
		  	}
		  	else
		  	{
		  		$scdArray[$f]['status'] = 'Pending';
		  	}
		  	
		  	$f++;
		}
	  }

  }
  
  $academicStartYearSecondNum = substr($academicStartYearSelected + 1,-2);
  include("./bottom.php");
  $smarty->assign('scdArray',$scdArray);
  $smarty->assign('class',$class);
  $smarty->assign('stdArray',$stdArray);
  $smarty->assign('count',$count);
  $smarty->assign('examScheduleId',$examScheduleId);
  $smarty->assign('academicStartYearSelected',$academicStartYearSelected);
  $smarty->assign('academicStartYearSecondNum',$academicStartYearSecondNum);
  $smarty->display('marksEntryReport.tpl');  
}
?>
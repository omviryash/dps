<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$today = date('Y-m');
	$countA = "";
	$countP = "";
	$totalNumberWorkingDay = "";
	if(isset($_REQUEST['attendenceYear']))
	{
	  $attendenceDate = $_REQUEST['attendenceYear']."-".$_REQUEST['attendenceMonth'];
	  $attendenceStartDate = $_REQUEST['attendenceYear']."-".$_REQUEST['attendenceMonth']."-01";
	  $attendenceEndDate = $_REQUEST['attendenceYear']."-".$_REQUEST['attendenceMonth']."-31";
	}
	else
	{
		$attendenceDate      = $today;
		$attendenceStartDate = $today."-01";
		$attendenceEndDate   = $today."-31";
	}
	
	$nominalArr = array();
  $i = 0;
  $selectNominal = "SELECT employeemaster.employeeCode,DATE_FORMAT(employeeattend.date, '%d-%m-%Y') AS date,employeeattend.startTime,employeeattend.endTime,
                           employeemaster.loginId,employeeattend.attendences,employeeattend.late
                      FROM employeeattend
                 LEFT JOIN employeemaster ON employeemaster.employeeCode = employeeattend.employeeCode
                     WHERE 1 = 1
                       AND employeeattend.date >= '".$attendenceStartDate."'
                       AND employeeattend.date <= '".$attendenceEndDate."'
                       AND employeemaster.loginId = '".$_SESSION['s_activName']."'
                       AND employeemaster.activated = 'Yes'
                  ORDER BY employeeattend.date";
  $selectNominalRes = mysql_query($selectNominal);
  while($nominalRow = mysql_fetch_array($selectNominalRes))
  {
    $nominalArr[$i]['employeeCode'] = $nominalRow['employeeCode'];
    $nominalArr[$i]['date']         = $nominalRow['date'];
    $nominalArr[$i]['attendences']  = $nominalRow['attendences'];
    $nominalArr[$i]['startTime']    = $nominalRow['startTime'];
    $nominalArr[$i]['endTime']      = $nominalRow['endTime'];
    $nominalArr[$i]['late']         = $nominalRow['late'];
    
    $selectAtnP = "SELECT attendences,startTime,endTime,late
                     FROM employeeattend
                    WHERE employeeCode = '".$nominalRow['employeeCode']."'
                      AND date >= '".$attendenceStartDate."-04-01'
                      AND date <= '".$attendenceEndDate."-03-31'
                      AND attendences = 'P'";
    $selectAtnPRes = mysql_query($selectAtnP);
    $countP = mysql_num_rows($selectAtnPRes);
    $countP;
    
    $selectAtnA = "SELECT attendences,startTime,endTime,late
                     FROM employeeattend
                    WHERE employeeCode = '".$nominalRow['employeeCode']."'
                      AND date >= '".$attendenceStartDate."-04-01'
                      AND date <= '".$attendenceEndDate."-03-31'
                      AND attendences = 'A'";
    $selectAtnARes = mysql_query($selectAtnA);
    $countA = mysql_num_rows($selectAtnARes);
    $countA;
    $totalNumberWorkingDay = $countA + $countP;
  	$i++;
  }
  
  include("./bottom.php");
  $smarty->assign('nominalArr',$nominalArr);
  $smarty->assign('attendenceDate',$attendenceDate);
  $smarty->assign('attendenceStartDate',$attendenceStartDate);
  $smarty->assign('attendenceEndDate',$attendenceEndDate);
  $smarty->assign('countA',$countA);
  $smarty->assign('countP',$countP);
  $smarty->assign('totalNumberWorkingDay',$totalNumberWorkingDay);
  $smarty->display('myEmpAttend.tpl');  
}
?>
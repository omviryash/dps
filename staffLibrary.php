<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] != 'Teacher')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$classArray = array();
  $i = 0;
  $selectClass = "SELECT librarytransaction.libraryTransactionId,employeemaster.loginId,
                         librarytransaction.checktype,librarytransaction.bookAccessionNo,
                         DATE_FORMAT(librarytransaction.issueDate, '%d-%m-%Y')AS issueDate,
                         DATE_FORMAT(librarytransaction.returnableDate, '%d-%m-%Y')AS returnableDate,
                         librarytransaction.returnBook,employeemaster.name,bookmaster.bookTitle
                    FROM librarytransaction
               LEFT JOIN employeemaster ON employeemaster.employeeCode = librarytransaction.employeeCode
               LEFT JOIN bookmaster ON bookmaster.bookAccessionNo = librarytransaction.bookAccessionNo
                   WHERE employeemaster.loginId = '".$_SESSION['s_activName']."'";
  $selectClassRes = mysql_query($selectClass);
  while($classRow = mysql_fetch_array($selectClassRes))
  {
  	$classArray[$i]['libraryTransactionId'] = $classRow['libraryTransactionId'];
  	$classArray[$i]['name']                 = $classRow['name'];
  	$classArray[$i]['bookAccessionNo']      = $classRow['bookAccessionNo'];
  	$classArray[$i]['bookTitle']            = $classRow['bookTitle'];
  	$classArray[$i]['issueDate']            = $classRow['issueDate'];
  	$classArray[$i]['returnableDate']       = $classRow['returnableDate'];
  	$classArray[$i]['returnBook']           = $classRow['returnBook'];
  	$i++;
  }
  
  include("./bottom.php");
  $smarty->assign('classArray',$classArray);
  $smarty->display('staffLibrary.tpl');  
}
?>
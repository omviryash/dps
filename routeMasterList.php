<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$classArray = array();
  $i = 0;
  $selectClass = "SELECT routemaster.routeMasterId,routemaster.routeName,routemaster.startTime,
                         routemaster.endTime,routemaster.arrival
                    FROM routemaster";
  $selectClassRes = mysql_query($selectClass);
  while($classRow = mysql_fetch_array($selectClassRes))
  {
  	$classArray[$i]['routeMasterId'] = $classRow['routeMasterId'];
  	$classArray[$i]['routeName']     = $classRow['routeName'];
  	$classArray[$i]['startTime']     = $classRow['startTime'];
  	$classArray[$i]['endTime']       = $classRow['endTime'];
  	$classArray[$i]['arrival']       = $classRow['arrival'];
  	$i++;
  }
  
  include("./bottom.php");
  $smarty->assign('classArray',$classArray);
  $smarty->display('routeMasterList.tpl');  
}
?>
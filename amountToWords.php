<?php
//using amount in words
function words($no)
{   
  $words = array('0'=> '' ,'1'=> 'one' ,'2'=> 'two' ,'3' => 'three','4' => 'four','5' => 'five','6' => 'six','7' => 'seven','8' => 'eight','9' => 'nine','10' => 'ten','11' => 'eleven','12' => 'twelve','13' => 'thirteen','14' => 'fouteen','15' => 'fifteen','16' => 'sixteen','17' => 'seventeen','18' => 'eighteen','19' => 'nineteen','20' => 'twenty','30' => 'thirty','40' => 'fourty','50' => 'fifty','60' => 'sixty','70' => 'seventy','80' => 'eighty','90' => 'ninty','100' => 'hundred &','1000' => 'thousand','100000' => 'lakh','10000000' => 'crore');
  if($no == 0)
    return ' ';
  else
  {
    $novalue='';
    $highno=$no;
    $remainno=0;
    $value=100;
    $value1=1000;       
    while($no>=100)
    {
      if(($value <= $no) &&($no  < $value1))
      {
        $novalue=$words["$value"];
        $highno = (int)($no/$value);
        $remainno = $no % $value;
        break;
      }
      $value= $value1;
      $value1 = $value * 100;
    }       
    if(array_key_exists("$highno",$words))
      return $words["$highno"]." ".$novalue." ".words($remainno);
    else
    {
      $unit=$highno%10;
      $ten =(int)($highno/10)*10;
      return $words["$ten"]." ".$words["$unit"]." ".$novalue." ".words($remainno);
    }
  }
}
//money format using amount
function makecomma($input)
{
  // This function is written by some anonymous person - I got it from Google
  if(strlen($input)<=2)
  { return $input; }
  $length=substr($input,0,strlen($input)-2);
  $formatted_input = makecomma($length).",".substr($input,-2);
  return $formatted_input;
}
//money format using amount
function formatInIndianStyle($num)
{ // This is moneyFormat function
  $pos = strpos((string)$num, ".");
  if ($pos === false) { $decimalpart="00";}
  else { $decimalpart= substr($num, $pos+1, 2); $num = substr($num,0,$pos); }
  
  if(strlen($num)>3 & strlen($num) <= 12){
              $last3digits = substr($num, -3 );
              $numexceptlastdigits = substr($num, 0, -3 );
              $formatted = makecomma($numexceptlastdigits);
              $stringtoreturn = $formatted.",".$last3digits.".".$decimalpart ;
  }elseif(strlen($num)<=3){
              $stringtoreturn = $num.".".$decimalpart ;
  }elseif(strlen($num)>12){
              $stringtoreturn = number_format($num, 2);
  }
  if(substr($stringtoreturn,0,2)=="-,"){$stringtoreturn = "-".substr($stringtoreturn,2 );}
  return $stringtoreturn;
}
?>
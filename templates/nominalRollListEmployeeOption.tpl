{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[-1, 25, 50, 100, 500, 1000], ['All', 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 500,
		"bJQueryUI":true
  });
});
</script>
{/block}
{block name="body"}
</br></br>
<form name="formGet" method="GET" action="nominalRollListEmployeeOption.php">
<table align="center">
	<tr>
		{if $s_userType == 'Administrator'}
		<td class="table2 form01">
		  <select name="class" autofocus="autofocus" required >
		    <option value="">Select class</option>
		    {html_options values=$cArray.className output=$cArray.className selected=$class}
		  </select>
	  </td>
    <td class="table2 form01">
		  <select name="classSection">
		    <option value="0">Select Section</option>
		    {html_options values=$secArrOut output=$secArrOut selected=$section}
		  </select>
	  </td>
	  {/if}
	  <td class="table2 form01">
		  <select name="startYear">
		    {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYearSelected}
		  </select>
		</td>
    <td>
      <input type="submit" name="submit" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
</form>
<form name="form1" method="POST" action="nominalRollListEmployeeOption.php">
<table align="left" border="1" id="myDataTable" class="display">
	<div class="hd"><h2 align="center">Optional Subject Allotment</h2>
	<thead>
	<tr>
		<td align="left"><b>Sr No</b></td>
		<td align="left"><b>GR No</b></td>
		<td align="left"><b>Roll No</b></td>
		<td align="left"><b>Student Name</b></td>
		<td align="left"><b>Class</b></td>
		<td align="left"><b>Section</b></td>
		<td align="left"><b>Optional Subject 1</b></td>
		{if $class == 6 || $class == 7 || $class == 8 || $class == 9 || $class == 10}
		<td align="left"><b>Co-Scholestics 3(A) Activity - I</b></td>
		<td align="left"><b>Co-Scholestics 3(A) Activity - II</b></td>
		<td align="left"><b>Co-Scholestics 3(B) Activity - I</b></td>
		<td align="left"><b>Co-Scholestics 3(B) Activity - II</b></td>
		{else}
		<td align="left"><b>Optional Subject 2</b></td>
		{/if}
		<!--td align="left"><input type="submit" name="submit" class="newSubmitBtn" value="Save"></td-->
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$nominalArr}
  <tr>
  	<td align="left">{$smarty.section.sec.rownum}</td>
    <td align="left">{$nominalArr[sec].grNo}</td>
    <td align="left">{$nominalArr[sec].rollNo}</td>
    <td align="left">{$nominalArr[sec].studentName}</td>
    <td align="left">{$nominalArr[sec].class}</td>
    <td align="left">{$nominalArr[sec].section}</td>
    <td class="table2 form01">
      <input type="hidden" name="nominalRollId[]" value="{$nominalArr[sec].nominalRollId}">
      <select name="optionSubject1[]" >
      	<option value="">Select Option1</option>
        {html_options values=$clubArr.subjectMasterId output=$clubArr.subjectName selected=$nominalArr[sec].optionSubject1}
      </select>
	  </td>
	  {if $class == 6 || $class == 7 || $class == 8 || $class == 9 || $class == 10}
	  <td class="table2 form01">
      <select name="3a1[]" >
      	<option value="">Select 3(A) Activity - I</option>
        {html_options values=$clubArr2.subjectMasterIdCo output=$clubArr2.subjectNameCo selected=$nominalArr[sec].3a1}
      </select>
	  </td>
    <td class="table2 form01">
      <select name="3a2[]" >
      	<option value="">Select 3(A) Activity - II</option>
        {html_options values=$clubArr2.subjectMasterIdCo output=$clubArr2.subjectNameCo selected=$nominalArr[sec].3a2}
      </select>
	  </td>
	  <td class="table2 form01">
      <select name="3b1[]" >
      	<option value="">Select 3(B) Activity - I</option>
        {html_options values=$clubArr2u.subjectMasterIdCo output=$clubArr2u.subjectNameCo selected=$nominalArr[sec].3b1}
      </select>
	  </td>
    <td class="table2 form01">
      <select name="3b2[]" >
      	<option value="">Select 3(B) Activity - II</option>
        {html_options values=$clubArr2u.subjectMasterIdCo output=$clubArr2u.subjectNameCo selected=$nominalArr[sec].3b2}
      </select>
	  </td>
	  {else}
	  <td class="table2 form01">
      <select name="optionSubject2[]" >
      	<option value="">Select Option2</option>
        {html_options values=$clubArr.subjectMasterId output=$clubArr.subjectName selected=$nominalArr[sec].optionSubject2}
      </select>
	  </td>
	  {/if}
  </tr>
  {/section}
  </tbody>
  <tfoot>
	<tr>
		<td align="left"></td>
		<td align="left"></td>
		<td align="left"></td>
		<td align="left"></td>
		<td align="left"></td>
		<td align="left"></td>
		{if $class == 6 || $class == 7 || $class == 8 || $class == 9 || $class == 10}
		<td align="left"></td>
		<td align="left"></td>
		<td align="left"></td>
		<td align="left"></td>
		{else}
		<td align="left"></td>
		{/if}
		<td align="left"><input type="submit" name="submit" class="newSubmitBtn" value="Save"></td>
  </tr>
  </tfoot>
</table>
</form>
{/block}
{include file="./main.tpl"}
{block name=head}
<style type="text/css" media="screen">
@import "./media/css/demo_table_jui.css";
@import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
input
{ 
  border:1px solid #000;
}
</style>
<script src="./media1/js/jquery-1.4.4.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.jeditable.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
	$('#grNo').focus();
	$('#example').dataTable({
		"bJQueryUI":true
	});
});
</script>
{/block}
{block name="body"}
</br></br>
<form name="formGet" method="GET" action="tecCer.php">
<table align="center" border="1">
	<tr>
		<td>
      Student Gr No : - <input type="text" name="grNo" id="grNo" placeholder="Enter Gr No">
    </td>
    <td>
      <input type="submit" name="" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
<form>
</br></br>	
<table align="left" border="1" id="example" class="display">
	<div class="hd"><h2 align="center">Tc/LC Create</h2>
	<thead>
	<tr>
		<th align="left"><b>Create</b></th>
		<th align="left"><b>G. R. No.</b></th>
		<th align="left"><b>Activated</b></th>
		<th align="left"><b>Name</b></th>
		<th align="left"><b>Father Name</b></th>
		<th align="left"><b>Date Of Birth</b></th>
		<th align="left"><b>Joined In Class</b></th>
		<th align="left"><b>Joining Date</b></th>
		<th align="left"><b>House Name</b></th>
		<th align="left"><b>Print</b></th>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$stdArray}
  <tr>
  	<th align="left"><a href="tcCerEntry.php?studentMasterId={$stdArray[sec].studentMasterId}">Create</a></th>
  	<th align="left">{$stdArray[sec].grNo}</th>
  	<td align="left">{$stdArray[sec].activated}</td>
  	<td align="left">{$stdArray[sec].studentName}</td>
    <td align="left">{$stdArray[sec].fatherName}</td>
    <td align="left">{$stdArray[sec].dateOfBirth|date_format:'%d-%m-%Y'}</td>
    <td align="left">{$stdArray[sec].joinedInClass}</td>
    <td align="left">{$stdArray[sec].joiningDate|date_format:'%d-%m-%Y'}</td>
    <th align="left">{$stdArray[sec].houseName}</th>
    <th align="left"><a href="tcCerPrint.php?studentMasterId={$stdArray[sec].studentMasterId}">Print</a></th>
  </tr>
  {sectionelse}
  <tr>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
  </tr>
  {/section}
  </tbody>
</table>
{/block}
{include file="./main.tpl"}
{block name=head}
<script type="text/javascript">
$(document).ready(function()
{
	$("#selectAllCheck").click(function()
	{
	  if($("#selectAllCheck").is(":checked"))
	  {
	  	$("input[id=techCheck]").attr('checked', 'checked');
	  }
	  else
	  {
	  	$("input[id=techCheck]").removeAttr('checked', 'checked');
	  }
	});
});

function setTemp(obj)
{
	var row = $(obj).parents('.mainRow');
	var smsTemplateId  = row.find('.smsTemplateId').val();
	var datastring = 'smsTemplateId=' + smsTemplateId;
	$.ajax({	
		type: 'GET',
		url: 'getTemp.php',
	  data: datastring,
	  success:function(data)
	  {
	  	$('#msgText').val(data);
    }
  });
}
</script>
{/block}
{block name="body"}
<form action="{$smarty.server.PHP_SELF}" name='user' method='POST'>
<br><br><br>
<div class="hd"><h2 align="center">Send Sms</h2></div>
<br><br>
<table align="left" border="1" cellpadding='3' cellspacing='6' style="margin-left:20px;">
	<thead>
	<tr>
		<td  class="table1" align='center' >
			<input type="checkbox" name="selectAllCheck" id="selectAllCheck" onclick="checkAll();"/>
		</td>
		<th align="left" class="table1"><b>Name</b></th>
		<th align="left" class="table1"><b>SMS</b></th>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$stdArray}
  <tr>
  	<th align="left" class="table2" NOWRAP >
	    <input type="checkbox" name="mgtsmsmobileId[]" id="techCheck" value="{$stdArray[sec].mgtsmsmobileId}">
	  </th>
  	<td align="left" class="table2" NOWRAP >{$stdArray[sec].nameOfContact}</td>
    <td align="left" class="table2" NOWRAP >{$stdArray[sec].smsMobile}</td>
  </tr>
  {/section}
</table>
<table border='0' cellpadding='1' cellspacing='2' align='center'>
{section name="sec1" loop=$tempArray}
<tr class='mainRow'>
  <td class="table1" align="center"><input type='radio' name='smsTemplateId' class='smsTemplateId' value='{$tempArray[sec1].smsTemplateId}' onclick='setTemp(this);'></td>
  <td class="table1" align="center">{$tempArray[sec1].smsTemplateId}</td>
  <td class="table1" align="center">{$tempArray[sec1].template}</td>
</tr>
{/section}
</table>
</br>
<table border='0' cellpadding='1' cellspacing='2' align='center'>
<tr>
  <td class="table1" align="center" colspan="2"><font style="font-size: 18px"><b> Message </b></font></td>
</tr>
<tr>
  <td class="table1" align="center"><textarea name="msgText" id="msgText" rows="50" cols="150" style="font-size:12px"></textarea></td>
</tr>
<tr>
  <td class="table1" align="center"><input type="submit" name="techSub" id="techSub" value=" Submit "/></td>
</tr>
</table>
</form>
{/block}
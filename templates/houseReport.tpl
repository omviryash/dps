{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 10,
		"bJQueryUI":true
  });
});
</script>
{/block}
{block name="body"}
</br></br>
<table align="left" border="1" id="myDataTable" class="display">
	<div class="hd"><h2 align="center">House Report</h2></div>
	<thead>
	<tr>
		<td align="left"><b>House Name</b></td>
	  <td align="left"><b>Male</b></td>
	  <td align="left"><b>Female</b></td>
	  <td align="left"><b>Total</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$attendenceArr}
  <tr>
  	<td align="left"><a href='houseWise.php?houseId={$attendenceArr[sec].houseId}'>{$attendenceArr[sec].houseName}</a></td>
    <td align="left">{$attendenceArr[sec].countP}</td>
    <td align="left">{$attendenceArr[sec].countA}</td>
    <td align="left">{$attendenceArr[sec].totalNumberWorkingDay}</td>
  </tr>
 {/section}
 </tbody>
 <tfoot>
   <tr>
   	<th></th>
   	<th></th>
   	<th></th>
   	<th align="left">{$countTotal}</th>
   </tr>
 </tfoot>
</table>
{/block}
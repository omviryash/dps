{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 10,
		"bJQueryUI":true
  });
});

function buttonHide(btnObj)
{
	var row = $(btnObj).parents('.trRow');
	row.find('.hideMe').hide();
}
</script>
{/block}
{block name="body"}
</br></br>
<form name="formGet" method="GET" action="myOnlineTest.php">
<table align="center">
	<tr>
		<td class="table2 form01">
		  <select name="startYear">
		    {html_options values=$dateArrVal output=$dateArrOut selected=$studStartYear}
		  </select>
		</td>
    <td>
      <input type="submit" name="submit" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
</form>
<table align="center" border="1">
	<div class="hd"><h2 align="center">Online Test</h2></div>
	<thead>
	<tr>
		<td align="left" class="table1"><b>Academic YEar</b></td>
	  <td align="left" class="table1"><b>Class</b></td>
	  <td align="left" class="table1"><b>Date</b></td>
	  <td align="left" class="table1"><b>Subject</b></td>
	  <td align="left" class="table1"><b>Marks</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$markArr}
  <tr class='trRow'>
  	<td align="left" class="table2">{$studStartYear}-{$studEndYear}</td>
    <td align="left" class="table2">{$markArr[sec].class}</td>
    <td align="left" class="table2">{$markArr[sec].scheduleDate|date_format: '%d-%m-%Y'}</td>
    <td align="left" class="table2">{$markArr[sec].subjectName}</td>
    {if $markArr[sec].countTest == 0}
      <td align="left" class="table2">
      	<input type="button" class='newSaveBtn hideMe' value="Start Test" onclick="window.open('myOnlineTestGenerate.php?scheduleMasterId={$markArr[sec].scheduleMasterId}&submit=submit','PoP_Up','directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=1024,height=668'); buttonHide(this);" />
      </td>
    {else}
      <td align="left" class="table2">{$markArr[sec].totalMarks}</td>
    {/if}
  </tr>
 {/section}
 </tbody>
</table>
{/block}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="refresh" content="30">
<head>
<title>Delhi Public School Rajkot</title>
<link rel="shortcut icon" type="image/x-icon" href="images/icon.png" />
<script type="text/javascript" src="./js/jquery.min.js"></script>
<script src="./js/jquery.dataTables.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" href="./css/buttonStyle.css" type="text/css" />
<style>
.textAlignRight
{
	text-align:right;
}
</style>
{block name=head}{/block}
</head>
<body>
<div class="menu">
<img src="./images/logoHead.png" width="196" height="51">
<span style="padding-left:60%;"><b>Hello {$s_activName} </b> | <a href="logout.php">Logout</a>
</span>
<ul id="nav">
	
	{if $s_userType == 'idCrdUser'}
	<li><a href="nominalRollList.php">Class List</a></li>
	{/if}
	{if $s_userType == 'Mangement'}
		<li><a href="index.php">Home</a></li>
		<li><a href="#">Employee Management</a>
			<ul>
				<li><a href="empmaster.php">Employee Upload</a></li>
			  <li><a href="employeAllList.php">Employee List</a></li>
			</ul>
		</li>
	{/if}
	{if $s_userType == 'Library'}
		<li><a href="index.php">Home</a></li>
		<li>Profile</a>
			<ul>
				<li><a href="teacherProfile.php">My Profile</a>
				<li><a href="changePassword.php">Change Password</a></li>
			  <li><a href="#">Message</a>
			  	<ul>
			  		<li><a href="feedbackEntry.php">Compose New Message</a></li>
			  		<li><a href="feedbackInbox.php">Inbox</a></li>
			  		<li><a href="feedback.php">Sent Messages</a></li>
			    </ul>
			  </li>
			</ul>
		</li>
		<li><a href="#">Library</a>
		<ul>
		  <li><a href="nominalRollListLibrary.php">Class List</a></li>
		  <li><a href="bookType.php">Books Type</a></li>
		  <li><a href="bookEntry.php">Books Entry</a></li>
		  <li><a href="bookMasterList.php">Books Master List</a></li>
		  <li><a href="bookMasterListEdit.php">Books Master List Edit</a></li>
		  <!-- li><a href="bookMasterList.php">Periodical Master List</a></li -->
		  <li><a href="libraryTransaction.php">Issue Books</a></li>
		  <li><a href="libraryTransactionList.php">Book Return List</a></li>
		  <li><a href="libraryTransactionListEdit.php">Library Transaction List</a></li>
		</ul>
		</li>
	{/if}
	
	{if $s_userType == 'Admin Staff'}
	<!--li><a href="index.php">Home</a></li>
	
	<li><a href="teacherProfile.php">My Profile</a></li>
	<li><a href="#">Accounts</a>
	<ul>
	  <li><a href="feeType.php">Fee Types</a></li>
	  <li><a href="feeTypeList.php">Fee Type List</a></li>
	  <li><a href="feeCollection.php">Receive Fees</a></li>
	  <li><a href="feeTransactionList.php">Fees collection Report</a></li>
	  <li><a href="feePendingList.php">Fees Pending Report</a></li>
	</ul>
	</li -->
	{/if}
	
	{if $s_userType == 'Admin Staff' || $s_userType == 'Principal' }
		<li><a href="index.php">Home</a></li>
		<li><a href="principalDashboard.php">Principal Dashboard</a></li>
		
		<li><a href="#">Profile</a>
			<ul>
				<li><a href="teacherProfile.php">My Profile</a>
			    	<li><a href="myEmpAttend.php">My Attendance</a></li>
			    	<li><a href="staffLibrary.php">My Library Books</a></li>

			  <li><a href="#">My Messages</a>
			  	<ul>
			  		<li><a href="feedbackInbox.php">Inbox</a></li>
			  		<li><a href="feedbackEntry.php">Compose New Message</a></li>
			  		<li><a href="feedback.php">Sent Messages</a></li>
			    </ul>
			  </li>
  			<li><a href="changePassword.php">Change Password</a></li>
			</ul>

		</li>
		<li><a href="#">Student</a>
			<ul>			
			  <li><a href="#">Student Master</a>
			  <ul>
			  <li><a href="studentEntry.php">New Admission</a></li>
  			  <li><a href="studentAllList.php">Student Master List</a></li>
			  <li><a href="studentAllListEdit.php">Student Edit</a></li>
  			  <li><a href="studentAllListPhoto.php">Student Photo List</a></li>
    			  <li><a href="contactNo.php">Student Contact Numbers</a></li>
  			  <li><a href="tecCer.php">Create TC/LC</a></li>
  			  <li><a href="tcList.php">TC/LC List</a></li>
			  </ul>
			  </li>
			  
			  <li><a href="#">NominalRoll</a>
			<ul>
			  <li><a href="nominalRollEntry.php">New NominalRoll</a></li>
			  <li><a href="nominalRollListAll.php">Nominal Roll List</a></li>
			  <li><a href="nominalRollListAllEdit.php">Nominal Roll Edit</a></li>
			  <li><a href="quickNominalRollListAll.php">Quick Nominal Roll List</a></li>
			  <li><a href="studentStrength.php">Student Strength</a></li>
			  <li><a href="bonafide.php">Bonafide</a></li>
			  <li><a href="nominalRollPromote.php">Promote</a></li>
  			<li><a href="cbseRegDetail.php">CBSE REGISTRATION DETAIL</a></li>
			  </ul>
			</li>
						
		 
			</ul>
		</li>
		<li><a href="#">Class </a>
			<ul>
			<li><a href="classMaster.php">Class Master</a></li>			
			<li><a href="nominalRollList.php">Class List</a></li>
			<li><a href="classTeacherAllotment.php">Class Teachership Allotment</a></li>
			<li><a href="classTeacherAllotmentList.php">Class Teachership List</a></li>
			<li><a href="iCardNominalRollList.php">I Card Print</a></li>
			</ul>
			</li>
		<li><a href="#">Subject</a>
			<ul>
			<li><a href="subjectEntry.php">Subject Entry</a></li>	
			<li><a href="subjectMasterList.php">Subject Master</a></li>
			<li><a href="nominalRollListEmployeeOption.php">Optional Subject Allotment</a></li>
			<li><a href="subjectTeacherAllotment.php">Subject Teachership Allotment</a></li>		
			<li><a href="subjectTeacherAllotmentList.php">Subject Teachership List</a></li>		
			</ul>
			</li>
			
		<li><a href="clubMaster.php">Club</a>
			  <ul>
			  	<li><a href="clubReportCount.php">Club Report Count</a></li>
			  	<li><a href="clubMasterEntry.php">Club Entry</a></li>
		      <li><a href="clubMaster.php">Club List</a></li>
		      <li><a href="employeeClubAllotmentNew.php">Club Allotment</a></li>
		      <li><a href="clubReportAll.php">Club Wise Student</a></li>
		      <li><a href="clubReport.php">Club Report</a></li>
		    </ul>
		  </li>
		  
		   <li><a href="houseReport.php">House</a>
			  <ul>
		      <li><a href="houseReport.php">House Report Count</a></li>
		      <li><a href="houseWise.php">House Wise Student</a></li>
		      <li><a href="classWiseHouseReport.php">Class Wise House Report</a></li>
		      <li><a href="houseMaster.php">House Master</a></li>
		    </ul>
		  </li>
		  
		<li><a href="#">Attendance</a>
		<ul>
		  <li><a href="attendence.php">Take Student Attendance</a></li>
		  <li><a href="empAttendMaster.php">Take Staff Attendance</a></li>		  
		  <li><a href="#">Attendence Report</a>
			<ul>
			  <li><a href="attendenceReport.php">Attendence Taken Report</a>
			  <li><a href="attendenceReportMonthly.php">Attendence Report (Monthly)</a></li>
			  <li><a href="attendenceReportTermly.php">Attendence Report (Termwise)</a></li>
			  <li><a href="attendenceReportYearly.php">Attendence Report (Yearly)</a></li>				
			</ul>
			</li>
		  </ul>
		</li>
		<li><a href="#">Examination</a>
			<ul>
			  <li><a href="#">Examination Schedule</a>
			  	<ul>
			  		<li><a href="examScheduleEntry.php">Add Examination</a>
			      <li><a href="examScheduleList.php">Examination List</a></li>
			    </ul>
			  </li>
			  <li><a href="examType.php">Examination Types</a></li>		
			 <li><a href="#">Marks</a>
			  <ul>	  
			  <li><a href="marksEntry.php">Marks Entry</a>
	  		  <li><a href="marksEntryReport.php">Marks Entry Status Report</a></li>
			  	<!-- ul>
		    		<li><a href="reGenerateMarksEntry.php">Re Generate Marks Entry</a></li>
		      </ul -->
			  </li>
			  </ul>
			  </li>
			  <li><a href="gradeMaster.php">Grade Master</a></li>
			
			 
    
     <li><a href="#">Online Test</a>
		<ul>
		  <li><a href="onlineTestEntry.php">Question Entry</a></li>
		  <li><a href="questionList.php">Question List</a></li>
		  <li><a href="onlineTestSchedule.php">Schedule Online Test</a></li>
		  <li><a href="onlineTestScheduleList.php">Scheduled Online Test List</a></li>
		  <li><a href="onlineTestResult.php">Online Test Result</a></li>
		</ul>
		</li>
		</ul>
	</li>
	<li><a href="#">Report Card</a>
	<ul>
 <li><a href="#">Report Card (1 To 5)</a>
          		<ul>
		            <li><a href="resultOneToFive.php">Generate Reportcard</a></li> 
		            <li><a href="gradeEntry.php">Grade Entry</a></li> 
		            <li><a href="reportCard1To5.php">View Report Card</a></li>
		        </ul>
	      </li>
		  <li><a href="#">Report Card (6 To 10)</a>
				<ul>
			    <li><a href="reGenerateMarksEntryPerformaTry.php">Generate ReportCard</a></li>
			    <li><a href="marksEntryPerformaTry.php">Part 1 Scholastics</a></li> 
			    <li><a href="coScholasticEntryTry.php">Part 2 Co-Scholastics</a></li>
	    		  <li><a href="scholarMasterList.php">Co-Scholastics Area Code List</a></li>
			    <li><a href="generateMarksDetailEntryTry.php">Part 3 Other Detail</a></li>
			    <!-- <li><a href="viewReportCard.php">Report Card Visible</a></li> -->
			    <li><a href="nominalRollListReportCard.php">View Report Card</a></li>
				</ul>
		  </li>
      <li><a href="#">Report Card (11 To 12)</a>

      	<ul>
      		<li><a href="marksDetailEntry11.php">Grade And Other Detail Entry</a></li>
	      	<li><a href="nominalRollListReportCard12.php">Report Card (11 To 12)</a></li>
	      </ul>
      </li>
      	  <li><a href="viewReportCard.php">Report Card Visibility on/off</a></li>
	   </ul>
	</li>	
		<li><a href="#">System</a>
			<ul>
				<li><a href="">Sms</a>
				  <ul>
				  	<li><a href="sendSMSStudent.php">To Student</a></li>
				  	<li><a href="sendSMSEmp.php">To Employee</a></li>
				  	<li><a href="sendSMS.php">To Management</a></li>
				  	<li><a href="http://smsby.megasoftware.in/new/" target="blank">SMS Plan Url</a></li>
				  </ul>
				</li>
				<li><a href="#">Bulk Upload</a>
				<ul>
				<li><a href="studentMaster.php">Student Upload</a></li>
				<li><a href="subjectMaster.php">Subject Master</a></li>	
				<li><a href="nominalRollUpload.php">Nominal Roll Upload</a></li>							
				<li><a href="bookMaster.php">Books Master Upload</a></li>
				<li><a href="questionUpload.php">Question Upload</a></li>
				</ul>
				</li>
        <li><a href="marksUpload.php">Marks Upload</a></li>
        <li><a href="marksUploadTry.php">Marks Upload</a></li>
		<li><a href="uploadData.php">Upload Data</a></li>
			</ul>
		</li>
		<li><a href="#">Staff </a>
		<ul>
		  <li><a href="employeeEntry.php">New Staff Entry</a></li>
		  <li><a href="employeAllList.php">Staff Master</a></li>
		  <li><a href="employeAllListEdit.php">Staff Edit</a></li>
		  <li><a href="teacherList.php">Teacher List</a></li>
		  <li><a href="attendenceReportMonthlyEmp.php">Staff Attendance</a></li>
		</ul>
		</li>
		<li><a href="#">Fee</a>
		<ul>
			<li><a href="#">Fee Type</a>
		  <ul>
			  <li><a href="feeType.php">Fee Types</a></li>
			  <li><a href="feeTypeList.php">Fee Type List</a></li>
			</ul>
		  <li><a href="#">Fee Structure</a>
		  <ul>
			  <li><a href="feeStructure.php">Fee Structure</a></li>
			  <li><a href="feeStructureList.php">Fee Structure List</a></li>
			</ul>
		  </li>
		  <li><a href="feeCollection.php">Receive Fees</a></li>
		  <li><a href="feeTransactionList.php">Fees collection Report</a></li>
		  <li><a href="feePendingList.php">Fees Pending Report</a></li>
		</ul>
		</li>
		<li><a href="#">Transport</a>
		<ul>
			<li><a href="#">Vehical</a>
		  <ul>
			  <li><a href="vehicalMaster.php">Vehical Master</a></li>
			  <li><a href="vehicalMasterList.php">Vehical List</a></li>
			</ul>
		  </li>
		  <li><a href="#">Route</a>
		  <ul>
		    <li><a href="routeMaster.php">Route Master</a></li>
		    <li><a href="routeMasterList.php">Route List</a></li>
		  </ul>
		  </li>
		  <li><a href="#">Bus Stop</a>
		  <ul>
		    <li><a href="busStopMaster.php">Bus Stop Master</a></li>
		    <li><a href="busStopMasterList.php">Bus Stop List</a></li>
		  </ul>
		  </li>
		  
		  <li><a href="busAllotment.php">Student Bus Allocation</a></li>
		  <li><a href="busWiseStudent.php">Bus Wise Student Report</a></li>
		  <li><a href="staffBusAllocation.php">Staff Bus Allocation</a></li>
		  <li><a href="busWiseStaff.php">Staff Bus List</a></li>
		  <li><a href="http://track.vtvsindia.com/php/getpage.php" target="blank">GPS Tracking Link</a></li>
		</ul>
	  </li>
	  <li><a href="#">Library</a>
		<ul>
		  <li><a href="bookType.php">Books Type</a></li>
		  <li><a href="bookEntry.php">Books Entry</a></li>
		  <li><a href="bookMasterList.php">Books Master List</a></li>
		  <li><a href="bookMasterListEdit.php">Books Master List Edit</a></li>
		  <!-- li><a href="bookMasterList.php">Periodical Master List</a></li -->
		  <li><a href="libraryTransaction.php">Issue Books</a></li>
		  <li><a href="libraryTransactionList.php">Book Return & Edit</a></li>
		  <li><a href="libraryTransactionListEdit.php">Library Transaction List</a></li>
		</ul>
		</li>
	  <li><a href="#">Time Table</a>
		<ul>
		  <li><a href="timeTableEntry.php">Time Table Entry</a></li>
		</ul>
		</li>
	 
	{/if}
	{if $s_userType == 'Student'}
	  <li><a href="index.php">Home</a></li>
	  <li><a href="#">Profile</a>
		<ul>
		  <li><a href="studentList.php">My Student Master Details</a></li>
		  <li><a href="nominalRollListStudent.php">My Nominal Roll</a></li>
		  <li><a href="myClassTeacher.php">My Class Teacher</a></li>
		  <li><a href="mySubjectTeacher.php">My Subject Teachers</a></li>
      <li><a href="#">My Messages</a>
		  	<ul>
		  		<li><a href="feedbackInbox.php">Inbox</a></li>
		  		<li><a href="feedbackEntry.php">Compose New Message</a></li>
		  		<li><a href="feedback.php">Sent Messages</a></li>
		    </ul>
			</li>
      <li><a href="changePassword.php">Change Password</a></li>
		</ul>
		</li>
	  <li><a href="#">Attendence</a>
	  	<ul>
			  <li><a href="attendanceYearlyStudent.php">My Yearly Attendance</a></li>
			  <li><a href="attendanceDailyStudent.php">My Monthly/Daily Attendance</a></li>
			</ul>
	  </li>
	  <li><a href="#">Account</a>
	  	<ul>
	  		<li><a href="myFee.php">My Fees Transaction List</a></li>
	    </ul>
	  </li>
	  <li><a href="#">Library</a>
	  	<ul>
	  		<li><a href="bookMasterList.php">Book List</a></li>
	  		<li><a href="myLibrary.php">My Library Transaction</a></li>
	    </ul>
	  </li>
	  <li><a href="#">Transport</a>
	  	<ul>
	  		<li><a href="myTransport.php">My Bus Allocation</a></li>
	    </ul>
	  </li>
	  <li><a href="#">Examination</a>
	  	<ul>
			  <li><a href="myExamSchedule.php">My Exam Schedule</a></li>
			  <li><a href="myExamMarks.php">My Exam Marks</a></li>
			  <li><a href="myReportCardAll.php">My Report Card</a></li>
			  <li><a href="myOnlineTest.php">My Online Test</a></li>
			</ul>
	  </li>
	  </li>
	{/if}
	{if $s_userType == 'Teacher'}
	  <li><a href="index.php">Home</a>
	
		<li><a href="teacherProfile.php">Profile</a>
			<ul>
				<li><a href="teacherProfile.php">My Profile</a>
			    	<li><a href="myEmpAttend.php">My Attendance</a></li>
			    	<li><a href="staffLibrary.php">My Library Books</a></li>
				<li><a href="#">My Messages</a>
			  	<ul>
			  		<li><a href="feedbackInbox.php">Inbox</a></li>
			  		<li><a href="feedbackEntry.php">Compose New Message</a></li>
			  		<li><a href="feedback.php">Sent Messages</a></li>
			   	</ul>
			  </li>
			<li><a href="changePassword.php">Change Password</a></li>
			</ul>
		</li>
		  <li><a href="#">Attendance</a>
			<ul>
			  <li><a href="attendence.php">Take Attendance</a>
			  	<!-- ul>
			  		<li><a href="reGenerateAttendence.php">Re Generate Attendance</a></li>
			    </ul -->
			  </li>
			  <li><a href="attendenceReportMonthly.php">Attendence Report (Monthly)</a></li>
			  <li><a href="attendenceReportTermly.php">Attendence Report (Termwise)</a></li>
			  <li><a href="attendenceReportYearly.php">Attendence Report (Yearly)</a></li>
			</ul>
		</li>
		<li><a href="#">Academic</a>
			<ul>
			<li><a href="nominalRollListEmployeeOption.php">My Class Optional Subject Allotment</a></li>
			<li><a href="nominalRollListEmployee.php">My Class Nominal Roll</a></li>
			<li><a href="myClassTeacherShip.php">My Class Teachership</a></li>
	    		<li><a href="mySubjectTeacherShip.php">My Subject Teachership</a></li>
		    	<li><a href="employeeClubAllotmentNew.php">My Class Club Allotment</a></li>

			</ul>
		</li>
		<li><a href="#">Examination</a>
			<ul>
			  <li><a href="examScheduleList.php">Examination Schedule</a></li>
			  <li><a href="marksEntry.php">Marks Entry</a>
			  	<!-- ul>
		    		<li><a href="reGenerateMarksEntry.php">Re Generate Marks Entry</a></li>
		      </ul -->
			  </li>
			  <!-- li><a href="gradeMaster.php">Grade Master</a></li -->
			  <li><a href="#">Report Card (1 To 5)</a>
          		<ul>
		            <li><a href="resultOneToFive.php">Generate Reportcard</a></li> 
		            <li><a href="gradeEntry.php">Grade Entry</a></li> 
		            <li><a href="reportCard1To5.php">View Report Card</a></li>
		        </ul>
	      </li>
		<li><a href="#">Report Card (6 To 10)</a>
		<ul>
	    <li><a href="reGenerateMarksEntryPerformaTry.php">Generate ReportCard</a></li>
	    <li><a href="marksEntryPerformaTry.php">Part 1 Scholastics</a></li> 
	    <li><a href="coScholasticEntryTry.php">Part 2 co-Scholastics</a></li>
	    <li><a href="generateMarksDetailEntryTry.php">Part 3 Other Detail</a></li>
	    <!-- <li><a href="viewReportCard.php">Report Card Visible</a></li> -->
	    <li><a href="nominalRollListReportCard.php">View Report Card</a></li>
		</ul>
		</li>
     <li><a href="#">Report Card (11 To 12)</a>
      	<ul>
      		<li><a href="marksDetailEntry11.php">Grade And Other Detail Entry</a></li>
	      	<li><a href="nominalRollListReportCard12.php">Report Card (11 To 12)</a></li>
	      </ul>
      </li>
	<li><a href="viewReportCard.php">Report Card Visible</a></li>
    <li><a href="scholarMasterList.php">Scholar Master List</a></li>
  </ul>
</li>
<li><a href="#">Transport</a>
<ul>
  <li><a href="busAllotment.php">Student Bus Allocation</a></li>
  <li><a href="busWiseStudent.php">Bus Wise Student Report</a></li>
  <li><a href="busWiseStaff.php">My Bus Allocation</a></li>
</ul>
</li>
<li><a href="#">Online Test</a>
<ul>
  <li><a href="onlineTestEntry.php">Question Add</a></li>
  <li><a href="questionList.php">Question List</a></li>
  <li><a href="onlineTestSchedule.php">Schedule Test</a></li>
  <li><a href="onlineTestScheduleList.php">Schedule Test List</a></li>
</ul>
</li>
{/if}
</ul>
</div>
<br>
<br>
</body>
{block name=body}{/block}
</html>
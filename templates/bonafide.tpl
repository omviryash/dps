{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<link href="./css/stylePagination.css" rel="stylesheet" type="text/css">
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"iDisplayLength": -1,
		"bJQueryUI":true
  });
});
</script>
{/block}
{block name="body"}
</br></br>
<form name="formGet" method="GET" action="bonafide.php">
<table align="center" border="1">
	<tr>
		<td>
      Student Gr No : - <input type="text" name="grNo" id="grNo" placeholder="Enter Gr No">
    </td>
    <td>
      <input type="submit" name="" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
<form>
<div class="hd"><h2 align="center">Bonafide / Character Certificate</h2></div>
<table align="left" border="1" id="myDataTable" class="display">
	<thead>
	<tr>
		<td align="left"><b>Create</b></td>
		<td align="left"><b>Academic Year</b></td>
		<td align="left"><b>GR No</b></td>
		<td align="left"><b>Roll No</b></td>
		<td align="left"><b>Student Name</b></td>
		<td align="left"><b>Activated</b></td>
		<td align="left"><b>Class</b></td>
		<td align="left"><b>Section</b></td>
		<td align="left"><b>Bonafide Date</b></td>
		<td align="left"><b>W. E. F. Date</b></td>
		<td align="left"><b>Print</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$nominalArr}
  <tr>
  	<td><a href="bonafideEntry.php?nominalRollId={$nominalArr[sec].nominalRollId}">Create</a></td>
  	<td align="left">{$nominalArr[sec].academicYear}</td>
    <td align="left">{$nominalArr[sec].grNo}</td>
    <td align="left">{$nominalArr[sec].rollNo}</td>
    <td align="left">{$nominalArr[sec].studentName}</td>
    <td align="left">{$nominalArr[sec].activated}</td>
    <td align="left">{$nominalArr[sec].class}</td>
    <td align="left">{$nominalArr[sec].section}</td>
    <td align="left">{$nominalArr[sec].bonafideDate}</td>
    <td align="left">{$nominalArr[sec].wefDate}</td>
    <td align="left"><a href="bonafidePrintSimple.php?nominalRollId={$nominalArr[sec].nominalRollId}">Print</a></td>
  </tr>
  {/section}
  </tbody>
</table>
{/block}
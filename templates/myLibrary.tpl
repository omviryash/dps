{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 10,
		"bJQueryUI":true
  });
});
</script>
{/block}
{block name="body"}
</br></br>
<form name="formGet" method="GET" action="myLibrary.php">
<table align="center">
	<tr>
		<td class="table2 form01">
		  <select name="startYear">
		    {html_options values=$dateArrVal output=$dateArrOut selected=$studStartYear}
		  </select>
		</td>
    <td>
      <input type="submit" name="submit" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
</form>
<table align="center" border="1">
	<div class="hd"><h2 align="center">My Library Books Transaction List</h2></div>
	<thead>
	<tr>
		<td align="left" class="table1"><b>Academic Year</b></td>
	  <td align="left" class="table1"><b>Issue Date</b></td>
	  <td align="left" class="table1"><b>Book Accession No</b></td>
	  <td align="left" class="table1"><b>Book Title</b></td>
	  <td align="left" class="table1"><b>Returnable Date</b></td>
	  <td align="left" class="table1"><b>Fine</b></td>
	  <td align="left" class="table1"><b>Fine Pay</b></td>
	  <td align="left" class="table1"><b>Return Book</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$markArr}
  <tr>
  	<td align="left" class="table2">{$studStartYear}-{$studEndYear}</td>
    <td align="left" class="table2">{$markArr[sec].issueDate}</td>
    <td align="left" class="table2">{$markArr[sec].bookAccessionNo}</td>
    <td align="left" class="table2">{$markArr[sec].bookTitle}</td>
    <td align="left" class="table2">{$markArr[sec].returnableDate}</td>
    <td align="left" class="table2">{$markArr[sec].fine}</td>
    <td align="left" class="table2">{$markArr[sec].finePay}</td>
    <td align="left" class="table2">{$markArr[sec].returnBook}</td>
  </tr>
 {/section}
 </tbody>
</table>
{/block}
{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[-1, 10, 20, 30, 40, 50], ["All", 10, 20, 30, 40, 50]],
  	"iDisplayLength": 500,
		"bJQueryUI":true
  });
  $(".omAttend").change(function()
  {
  	$('.newGoBtn').click();
  });
});
</script>
{/block}
{block name="body"}
</br></br>
<form name="formGet" method="GET" action="attendenceReportMonthly.php">
<table align="center">
	<tr>
		{if $s_userType == 'Administrator'}
		<td class="table2 form01 omAttend">
		  <select name="class" autofocus="autofocus" >
		    <option value="">Select class</option>
		    {html_options values=$cArray.className output=$cArray.className selected=$class}
		  </select>
	  </td>
    <td class="table2 form01 omAttend">
		  <select name="classSection">
		    <option value="0">Select Section</option>
		    {html_options values=$secArrOut output=$secArrOut selected=$section}
		  </select>
	  </td>
	  {/if}
    <td>
      {html_select_date prefix="attendence" start_year="-1" end_year="+0" field_order="DMY" time=$attendenceDate day_value_format="%02d" display_days=false}
    </td>
    <td>
      <input type="submit" name="submit" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
<!--center>Total Number Working Day = {$totalNumberWorkingDay}</center-->
</form>
<table align="left" border="1" id="myDataTable" class="display">
	</br>
	<h1 align="center">{$class}/{$section}</h1>
  </br>
	<div class="hd"><h2 align="center">Attendance Report (Monthly)</h2></div>
	<!--thead>
	<tr>
		<td align="left"><b>Roll No</b></td>
	  <td align="left"><b>Name</b></td>
	  <td align="left"><b>AP</b></td>
	  <td align="left"><b>Present</b></td>
	  <td align="left"><b>Absent</b></td>
  </tr>
  </thead-->
  <thead>
	<tr>
		<td align="left"><b>Roll No</b></td>
	  <td align="left"><b>Name</b></td>
	  
	  <td width="15">01</td><td width="15">02</td><td width="15">03</td>
	  <td width="15">04</td><td width="15">05</td><td width="15">06</td>
	  <td width="15">07</td><td width="15">08</td><td width="15">09</td>
	  <td width="15">10</td><td width="15">11</td><td width="15">12</td>
	  <td width="15">13</td><td width="15">14</td><td width="15">15</td>
	  <td width="15">16</td><td width="15">17</td><td width="15">18</td>
	  <td width="15">19</td><td width="15">20</td><td width="15">21</td>
	  <td width="15">22</td><td width="15">23</td><td width="15">24</td>
	  <td width="15">25</td><td width="15">26</td><td width="15">27</td>
	  <td width="15">28</td><td width="15">29</td><td width="15">30</td>
	  <td width="15">31</td>
    
    <td align="left"><b>Working Day</b></td>
    <td align="left"><b>Present</b></td>
	  <td align="left"><b>Absent</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$attendenceArr}
  <tr>
    <td align="left">{$attendenceArr[sec].rollNo}</td>
    <td align="left">{$attendenceArr[sec].studentName}</td>
		<td width="15">{$attendenceArr[sec].attendences01}</td>
  		<td width="15">{$attendenceArr[sec].attendences02}</td>
  		<td width="15">{$attendenceArr[sec].attendences03}</td>
	  <td width="15">{$attendenceArr[sec].attendences04}</td>
	  <td width="15">{$attendenceArr[sec].attendences05}</td>
	  <td width="15">{$attendenceArr[sec].attendences06}</td>
	  <td width="15">{$attendenceArr[sec].attendences07}</td>
	  <td width="15">{$attendenceArr[sec].attendences08}</td>
	  <td width="15">{$attendenceArr[sec].attendences09}</td>
	  <td width="15">{$attendenceArr[sec].attendences10}</td>
	  <td width="15">{$attendenceArr[sec].attendences11}</td>
	  <td width="15">{$attendenceArr[sec].attendences12}</td>
	  <td width="15">{$attendenceArr[sec].attendences13}</td>
	  <td width="15">{$attendenceArr[sec].attendences14}</td>
	  <td width="15">{$attendenceArr[sec].attendences15}</td>
	  <td width="15">{$attendenceArr[sec].attendences16}</td>
	  <td width="15">{$attendenceArr[sec].attendences17}</td>
	  <td width="15">{$attendenceArr[sec].attendences18}</td>
	  <td width="15">{$attendenceArr[sec].attendences19}</td>
	  <td width="15">{$attendenceArr[sec].attendences20}</td>
	  <td width="15">{$attendenceArr[sec].attendences21}</td>
	  <td width="15">{$attendenceArr[sec].attendences22}</td>
	  <td width="15">{$attendenceArr[sec].attendences23}</td>
	  <td width="15">{$attendenceArr[sec].attendences24}</td>
	  <td width="15">{$attendenceArr[sec].attendences25}</td>
	  <td width="15">{$attendenceArr[sec].attendences26}</td>
	  <td width="15">{$attendenceArr[sec].attendences27}</td>
	  <td width="15">{$attendenceArr[sec].attendences28}</td>
	  <td width="15">{$attendenceArr[sec].attendences29}</td>
	  <td width="15">{$attendenceArr[sec].attendences30}</td>
	  <td width="15">{$attendenceArr[sec].attendences31}</td>
    <td align="left">{$attendenceArr[sec].totalNumberWorkingDay}</td>
    <td align="left">{$attendenceArr[sec].countP}</td>
    <td align="left">{$attendenceArr[sec].countA}</td>
  </tr>
 {/section}
 </tbody>
 <tfoot>
   <tr>
   	<th></th>
   	<th></th>
   	<th></th>
   	<th></th>
   </tr>
 </tfoot>
</table>
{/block}
{include file="./main.tpl"}
{block name="head"}
<link rel="stylesheet" href="./css/buttonStyle.css" type="text/css" />
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript">
$(document).ready(function(){
	$('#myDataTable').dataTable({
		"aLengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 100,
  	"bAutoWidth": false,
  	"aaSorting": [[ 0, "asc" ]],
		"bJQueryUI":true
  });
  
	$(".omAttend").change(function()
  {
  	$('.newGoBtn').click();
  });
});


function buttonHide()
{
	$('#hideMe').hide();
}

</script>
<style>
.selectCombo
{
	width:500px;
}
</style>
{/block}
{block name="body"}
</br>
<center>
<form name="formGet" id="formGet" method="GET" action="coScholasticEntryTry.php">
<table align="center" border="1">
	<div class="hd"><h2 align="center">Part 2 Co-Scholastic Entry</h2></div>
	<tr>
		<td class="table2 form01">
		  <select name="startYear" class="omAttend">
		    {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYearSelected}
		  </select>
		</td>
		{if $s_userType == 'Administrator'}
		<td class="table2 form01 omAttend">
		  <select name="class" autofocus="autofocus">
		    <option value="0">Select class</option>
		    {html_options values=$cArray.className output=$cArray.className selected=$class}
		  </select>
	  </td>
    <td class="table2 form01 omAttend">
		  <select name="section">
		    <option value="0">Select Section</option>
		    {html_options values=$secArrOut output=$secArrOut selected=$section}
		  </select>
	  </td>
	  {/if}
		<td>
	    <input type="submit" name="go" class="newGoBtn" value="go">
	  </td>
	</tr>
</table>
	  <a href="scholarMasterList.php" target=_blank> (Co-Scholastics Area Code List)</a>
</form>
</center>
{if $count != 0}
<form name="form2" method="POST" action="coScholasticEntryTry.php">
<table align="left" border="1" id="myDataTable" class="display">
	<input type="hidden" name="startYear" value="{$academicStartYearSelected}">
  </br>
	<h1 align="center">Student List - {$class}-{$section}</h1>
	
  </br>
 
    <thead>
    
 
  
  <tr>
    <td class="table1" colspan="3"> </td>
    <td class="table1" colspan="3">Part 2(A) Life Skills</td>
    <td class="table1">Part 2(B)</td>
    <td class="table1">Part 2(C)</td>
    <td class="table1" colspan="4">Part 2(D) Attitudes and Values </td>
    <td class="table1" colspan="2">3A</td>
    <td class="table1" colspan="2">3B</td>
  </tr>
  
	
	<tr>
		<td align="left" class="table1"><b>Roll. No.</b></td>
		<td align="left" class="table1"><b>G. R. No.</b></td>
		<td align="left" class="table1"><b>Name</b></td>
		<td align="left" class="table1"><b>Thinking Skills</b></td>
		<td align="left" class="table1"><b>Social Skills</b></td>
		<td align="left" class="table1"><b>Emotional Skills</b></td>
		<td align="left" class="table1"><b>Work Education</b></td>
		<td align="left" class="table1"><b>Visual and Performaing Arts</b></td>
		<td align="left" class="table1"><b>Teachers</b></td>
		<td align="left" class="table1"><b>Schoolmates</b></td>
		<td align="left" class="table1"><b>Programmes and Environment</b></td>
		<td align="left" class="table1"><b>Value Systems</b></td>
		<td align="left" class="table1"><b>3A Activity 1</b></td>
		<td align="left" class="table1"><b>3A Activity 2</b></td>
		<td align="left" class="table1"><b>3B Activity 1</b></td>
		<td align="left" class="table1"><b>3B Activity 2</b></td>
  </tr>
 </thead>
  <tbody>
  {section name="sec" loop=$dtlArr}
  <tr class="gradeRow">
  	<td align="left" class="table2">{$dtlArr[sec].rollNo}</td>
		<td align="left" class="table2">
    	{$dtlArr[sec].grNo}
    	<input type="hidden" name="examMarksPerTryId[]" value="{$dtlArr[sec].examMarksPerTryId}">
    </td>
    <td align="left" class="table2">{$dtlArr[sec].studentName}</td>
    
    <td align="left" class="table2"><input type="text" name="2A1[]" value="{$dtlArr[sec].2A1}" size="5" /></td>
    <td align="left" class="table2"><input type="text" name="2A2[]" value="{$dtlArr[sec].2A2}" size="5" /></td>
    <td align="left" class="table2"><input type="text" name="2A3[]" value="{$dtlArr[sec].2A3}" size="5" /></td>
    <td align="left" class="table2"><input type="text" name="2B1[]" value="{$dtlArr[sec].2B1}" size="5" /></td>
    <td align="left" class="table2"><input type="text" name="2C1[]" value="{$dtlArr[sec].2C1}" size="5" /></td>
    <td align="left" class="table2"><input type="text" name="2D1[]" value="{$dtlArr[sec].2D1}" size="5" /></td>
    <td align="left" class="table2"><input type="text" name="2D2[]" value="{$dtlArr[sec].2D2}" size="5" /></td>
    <td align="left" class="table2"><input type="text" name="2D3[]" value="{$dtlArr[sec].2D3}" size="5" /></td>
    <td align="left" class="table2"><input type="text" name="2D4[]" value="{$dtlArr[sec].2D4}" size="5" /></td>
    <td align="left" class="table2"><input type="text" name="3A1[]" value="{$dtlArr[sec].3A1}" size="5" /></td>
    <td align="left" class="table2"><input type="text" name="3A2[]" value="{$dtlArr[sec].3A2}" size="5" /></td>
    <td align="left" class="table2"><input type="text" name="3B1[]" value="{$dtlArr[sec].3B1}" size="5" /></td>
    <td align="left" class="table2"><input type="text" name="3B2[]" value="{$dtlArr[sec].3B2}" size="5" /></td>

  </tr>
  {/section}
  </tbody>
  <tfoot>
  <tr>
    <td align="center" colspan="16" class="table2"><input type="submit" name="submitTaken" class="newSubmitBtn" value="Save"></td>
  </tr>
  </tfoot>
</table>
</form>
{/if}
{/block}
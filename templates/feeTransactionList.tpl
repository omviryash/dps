{include file="./main.tpl"}
{block name="head"}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script src="./media1/js/jquery-1.4.4.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.jeditable.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[-1, 10, 20, 30, 40, 50], ["All", 10, 20, 30, 40, 50]],
  	"iDisplayLength": 500,
  	"aaSorting": [[2, 'desc']],
		"bJQueryUI":true
  });
  $(".omAttend").change(function()
  {
  	$('.newGoBtnClick').click();
  });
  
  /* Editable Start*/
//	var oTable = $('#myDataTable').dataTable();
//     
//  /* Apply the jEditable handlers to the table */
//  oTable.$('th').editable( './updateRollNoData.php', {
//      "submitdata": function ( value, settings ) {
//          return {
//              "row_id": this.parentNode.getAttribute('th.id'),
//              "column": oTable.fnGetPosition( this )[2]
//          };
//      },
//      "height": "20px",
//      "width": "100"
//  });
  
  /* Editable End*/
  
  $("#startDateYear").change(function()
  {
    var yearExtra  = $('#startDateYear').val();
		var endDate    = yearExtra;
		var datastring = 'endDate=' + endDate;
		$.ajax({	
			type: 'GET',
			url: 'endYear.php',
		  data: datastring,
		  success:function(data)
		  {
		  	$('#endDateYear').val(data);
	    }
	  });
  });
});
</script>
{/block}
{block name="body"}
</br></br>
<link rel="stylesheet" href="./css/buttonStyle.css" type="text/css" />
<form name="formGet" method="GET" action="feeTransactionList.php">
<table align="center">
	<tr>
		<td class="table2">Fee Type</td>
	  <td class="table2 form01">
	  	<select name="feeTypeId" id="feeType" Autofocus >
	  		<option value="">Select Fee Type</option>
	      {html_options values=$typeArr.feeTypeId output=$typeArr.feeType selected=$feeTypeId}
	    </select>
		</td>
		<td class="table2 form01">
		  <select name="class" autofocus="autofocus" class='omAttend'>
		    <option value="">Select Class</option>
		    {html_options values=$cArray.className output=$cArray.className selected=$class}
		  </select>
	  </td>
    <td class="table2 form01">
		  <select name="classSection" class='omAttend'>
		    <option value="0">Select Section</option>
		    {html_options values=$secArrOut output=$secArrOut selected=$section}
		  </select>
	  </td>
	  <td class="table2 form01">
		  <select name="startYear">
		    {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYearSelected}
		  </select>
		</td>
    <td>
      <input type="submit" name="submit" class="newGoBtn newGoBtnClick" value="Go">
    </td>
  </tr>
</table>
</form>
<form name="formGet" method="GET" action="feeTransactionList.php">
<table align="center">
	<tr>
		<td class="table2 form01">
	    {html_select_date prefix="attendence" start_year="-25" end_year="+25" field_order="DMY" time=$attendDate day_value_format="%02d" display_days=true}
	  </td>
    <td>
      <input type="submit" name="submitNew" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
</form>
<form name="form2" method="POST" action="feeTransactionList.php">
<input type="hidden" name="attendDate" value="{$attendDate}">
<table align="center" border="1" id="myDataTable" class="display">  
  </br>
	<div class="hd"><h2 align="center">Fee Collection Report</h2></div>
	</br>
	<thead>
	<tr>
		<td align="left" class="table1"><b>Edit</b></td>
		<td align="left" class="table1"><b>Academic Year</b></td>
		<td align="left" class="table1"><b>Date</b></td>
		<td align="left" class="table1"><b>R No</b></td>
		<td align="left" class="table1"><b>GrNo</b></td>
		<td align="left" class="table1"><b>Class</b></td>
		<td align="left" class="table1"><b>Student Name</b></td>
		<td align="left" class="table1"><b>Fee Type</b></td>
		<td align="left" class="table1"><b>Duration</b></td>
		<td align="left" class="table1"><b>Mode of Pay</b></td>
		<td align="left" class="table1"><b>Ch No.</b></td>
		<td align="left" class="table1"><b>Discount</b></td>
		<td align="left" class="table1"><b>Amount</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$attendenceArr}
  <tr class="gradeRow">
  	<td><a href="feeCollection.php?feeCollectionId={$attendenceArr[sec].feeCollectionId}&rNo={$attendenceArr[sec].rNo}&feeDate={$attendenceArr[sec].feeDate|Date_format:"%Y-%m-%d"}">Edit</a></td>
  	<td align="left" class="table2">{$academicStartYearSelected}-{$academicStartYearSecondNum}</td>
  	<td align="left" class="table2" NOWRAP >{$attendenceArr[sec].feeDate}</td>
  	<td align="left" class="table2">{$attendenceArr[sec].rNo}</td>
    <td align="left" class="table2">{$attendenceArr[sec].grNo}</td>
    <td align="left" class="table2">{$attendenceArr[sec].class} - {$attendenceArr[sec].section}</td>
    <td align="left" class="table2">{$attendenceArr[sec].studentName}</td>
    <td align="left" class="table2">{$attendenceArr[sec].feeType}</td>
    <td align="left" class="table2">{$attendenceArr[sec].feeName}</td>
    <td align="left" class="table2">{$attendenceArr[sec].modeOfPay}</td>
    <td align="left" class="table2">{$attendenceArr[sec].chNo}</td>
    <td align="left" class="table2">{$attendenceArr[sec].discount}</td>
    <td align="left" class="table2">{$attendenceArr[sec].amount}</td>
  </tr>
  {/section}
  </tbody>
</table>
</form>
{/block}
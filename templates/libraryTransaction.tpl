{include file="./main.tpl"}
{block name="head"}
<script type="text/javascript">
$(document).ready(function()
{
	if($("#checktype").val() == 'S')
	{
	  $('#empNameBest').hide();
	  $('#empCodeBest').hide();
	}
	if($("#checktype").val() == 'E')
	{
		$('#grNoBest').hide();
	}
});
function hideStudent()
{
	$('#empNameBest').show();
	$('#empCodeBest').show();
	$('#grNoBest').hide();
	$('#stdNameBest').hide();
}
function hideEmp()
{
	$('#empNameBest').hide();
	$('#empCodeBest').hide();
	$('#grNoBest').show();
	$('#stdNameBest').show();
}
function checkGrno()
{
	var grNo = $("#grNo").val() != '' ? $("#grNo").val() : 0
	var dataString = "grNo=" + grNo;
	$.ajax({
		type : 'GET',
		url  : 'setStudNameLib.php',
		data :  dataString,
		success:function(data)
		{
			$('#stdNameBest').html(data);
	  }
	});
}
function checkEmpCode()
{
	var nameStaff = $("#nameStaff").val() != '' ? $("#nameStaff").val() : 0
	var dataString = "nameStaff=" + nameStaff;
	$.ajax({
		type : 'GET',
		url  : 'setEmpNameLib.php',
		data :  dataString,
		success:function(data)
		{
			$('#empCodeBest').html(data);
	  }
	});
}

function checkAcNo()
{	
	var dataString = "bookAccessionNo=" +$("#bookAccessionNo").val();
	$.ajax({
		type : 'GET',
		url  : 'setBookName.php',
		data :  dataString,
		success:function(data)
		{
      $('#bookTitle').val(data);
	  }
	});
}

function checkGrnoBook()
{
	var dataString = "grNo=" +$(".grNo").val();
	$.ajax({
		type : 'GET',
		url  : 'checkGrnoBook.php',
		data :  dataString,
		success:function(data)
		{
			if(data == 2)
			{
				alert("Last Book Not Issue");
				$('.grNo').val('');
				$('.grNo').focus();
				return false;
			}
	  }
	});
}

function checkSecondBook()
{
	var dataString = "bookAccessionNo=" +$("#bookAccessionNo").val();
	$.ajax({
		type : 'GET',
		url  : 'checkSecondBook.php',
		data :  dataString,
		success:function(data)
		{
			if(data == 2)
			{
				alert("This Book Is Already Issued");
				$('#bookTitle').val('');
				$('#bookAccessionNo').focus();
				return false;
			}
	  }
	});
}

function setFine()
{
  var dayExtra    = $('#fromDateDay').val();
  var monthExtra  = $('#fromDateMonth').val();
  var yearExtra   = $('#fromDateYear').val();
  var dayExtraIssue    = $('#issueDateDay').val();
  var monthExtraIssue  = $('#issueDateMonth').val();
  var yearExtraIssue   = $('#issueDateYear').val();
  var issueDate   = yearExtraIssue+'-'+monthExtraIssue+'-'+dayExtraIssue;
	var fullDate    = yearExtra+'-'+monthExtra+'-'+dayExtra;
	var dataString = "returnableDate=" + fullDate + "&issueDate=" + issueDate;
  $.ajax(
  {
    type: "GET",
    url: "setFine.php",
    data: dataString,
    success:function(data)
    {
      $('.getFine').val(data);
    }
  });
}

</script>
{/block}
{block name="body"}

<form name="form1" method="POST" action="libraryTransaction.php" onsubmit="return confirm('DO You Want To Submit !!!!!')";>
<table align="center" border="0" class="table2">
<div class="hd"><h2 align="center">Library Transaction Entry</h2>
<input type="hidden" name="libraryTransactionId" value="{$libraryTransactionId}">

<tr>
  <td align="center" colspan="2" style="color:red; font-size:18px;"><b>{$confirm}</b></td>
</tr>
{if $checktype == 'E'}
{if $libraryTransactionId > 0}
<tr>
	<td align="center" colspan="2" style="color:red; font-size:18px;">
		Student :- <input type='radio' name='checktype' value='S' onclick='hideEmp();'  Autofocus DISABLED >
		Staff :- <input type='radio' name='checktype' id='checktype' value='E' onclick='hideStudent();' CHECKED DISABLED >
  </td>
</tr>
{else}
<tr>
	<td align="center" colspan="2" style="color:red; font-size:18px;">
		Student :- <input type='radio' name='checktype' value='S' onclick='hideEmp();'  Autofocus >
		Staff :- <input type='radio' name='checktype' id='checktype' value='E' onclick='hideStudent();' CHECKED >
  </td>
</tr>
{/if}
{else}
{if $libraryTransactionId > 0}
<tr>
	<td align="center" colspan="2" style="color:red; font-size:18px;">
		Student :- <input type='radio' name='checktype' id='checktype' value='S' onclick='hideEmp();' CHECKED Autofocus DISABLED >
		Staff :- <input type='radio' name='checktype' value='E' onclick='hideStudent();' DISABLED >
  </td>
</tr>
{else}
<tr>
	<td align="center" colspan="2" style="color:red; font-size:18px;">
		Student :- <input type='radio' name='checktype' id='checktype' value='S' onclick='hideEmp();' CHECKED Autofocus >
		Staff :- <input type='radio' name='checktype' value='E' onclick='hideStudent();' >
  </td>
</tr>
{/if}
{/if}
{if $checktype == 'S'}
<tr id="grNoBest">
  <td class="table2">Gr No</td>
  <td class="table2 form01">
  	{if $libraryTransactionId > 0}
  	<input type="text" name="grNo" value="{$grNo}" id="grNo" class="grNo" onblur="checkGrno();" size='50' />
  	{else}
  	<input type="text" name="grNo" value="{$grNo}" id="grNo" class="grNo" onblur="checkGrno(); checkGrnoBook();" size='50' />
  	{/if}
	</td>
</tr>
<tr id="stdNameBest">
  
</tr>
{else}
<tr id="grNoBest">
  <td class="table2">Gr No</td>
  <td class="table2 form01">
  	{if $libraryTransactionId > 0}
  	<input type="text" name="grNo" value="{$grNo}" id="grNo" class="grNo" onblur="checkGrno();" size='50' />
  	{else}
  	<input type="text" name="grNo" value="{$grNo}" id="grNo" class="grNo" onblur="checkGrno(); checkGrnoBook();" size='50' />
  	{/if}
	</td>
</tr>
<tr id="stdNameBest">
 
</tr>
{/if}
{if $checktype == 'E'}
<tr id="empNameBest">
  <td class="table2">Name</td>
  <td class="table2 form01">
  	<select name='nameStaff' id='nameStaff' onblur="checkEmpCode();">
  	  <option value=''>Select</option>
  	  {html_options values=$dArray.name output=$dArray.name selected=$toName}
    </select>
	</td>
</tr>
<tr id="empCodeBest">
  
</tr>
{else}
<tr id="empNameBest">
  <td class="table2">Name</td>
  <td class="table2 form01">
  	<select name='nameStaff' id='nameStaff' onblur="checkEmpCode();">
  	  <option value=''>Select</option>
  	  {html_options values=$dArray.name output=$dArray.name selected=$toName}
    </select>
	</td>
</tr>
<tr id="empCodeBest">
  
</tr>
{/if}
<tr>
  <td class="table2">Book Accession No</td>
  <td class="table2 form01">
  	{if $libraryTransactionId > 0}
  	<input type="text" name="bookAccessionNo" id="bookAccessionNo" value="{$bookAccessionNo}" onblur="checkAcNo();" size='50' />
  	{else}
  	<input type="text" name="bookAccessionNo" id="bookAccessionNo" value="{$bookAccessionNo}" onblur="checkAcNo(); checkSecondBook();" size='50' />
  	{/if}
	</td>
</tr>
<tr>
	<td align="left" class="table2 form01">Book Name : </td>
  <td class="table2 form01"><textarea id="bookTitle" cols='50' DISABLED >{$bookTitle}</textarea></td>
</tr>
<tr>
  <td class="table2">Issue Date</td>
  <td class="table2 form01" onchange="setFine();">
    {html_select_date day_extra="id=\"issueDateDay\"" month_extra="id=\"issueDateMonth\"" year_extra="id=\"issueDateYear\"" prefix="issueDate" start_year="-25" end_year="+25" field_order="DMY" time=$issueDate day_value_format="%02d"}
  </td>
</tr>
{if $libraryTransactionId > 0}
<tr>
  <td class="table2">Actual Date of Return</td>
  <td class="table2 form01" onchange="setFine();">
    {html_select_date day_extra="id=\"fromDateDay\"" month_extra="id=\"fromDateMonth\"" year_extra="id=\"fromDateYear\"" prefix="returnableDate" start_year="-25" end_year="+25" field_order="DMY" time=$returnableDate day_value_format="%02d"}
  </td>
</tr>
<tr>
	<td class="table2">Fine</td>
	<td class="table2 form01" align="center">
    <input type="text" name="fine" value="{$fine}" class="getFine" READONLY />
  </td>
</tr>
<tr>
	<td class="table2">Fine Pay</td>
	<td class="table2 form01" align="center">
    {if $finePay == 'Panding'}
      <input type="checkbox" name='finePay' >
    {else}
      <input type="checkbox" name='finePay' CHECKED >
    {/if}
  </td>
</tr>
{/if}
<tr>
  <td class="table2"></td>
  <td class="table2 form01" align="center"><input type="submit" name="Submit" class="button" value="Save"></td>
</tr>
</table>
</form>
{/block}
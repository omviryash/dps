{include file="./main.tpl"}
{block name="head"}
<script type="text/javascript" src="jquery-1.8.0.min.js"></script>
<script type="text/javascript">
function checkBook()
{
	var dataString = "bookAccessionNo=" +$("#bookAccessionNo").val();
	$.ajax({
		type : 'GET',
		url  : 'checkMasterBook.php',
		data :  dataString,
		success:function(data)
		{
			if(data == 2)
			{
				alert("This Book Is Already Entered");
				$('#bookAccessionNo').val('');
				$('#bookAccessionNo').focus();
				return false;
			}
	  }
	});
}

$(function(){
$(".search").keyup(function() 
{ 
var searchid = $(this).val();
var dataString = 'search='+ searchid;
if(searchid!='')
{
	$.ajax({
	type: "POST",
	url: "search.php",
	data: dataString,
	cache: false,
	success: function(html)
	{
	$("#result").html(html).show();
	}
	});
}return false;    
});

jQuery("#result").live("click",function(e){ 
	var $clicked = $(e.target);
	var $name = $clicked.find('.name').html();
	var decoded = $("<div/>").html($name).text();
	$('#searchid').val(decoded);
});
jQuery(document).live("click", function(e) { 
	var $clicked = $(e.target);
	if (! $clicked.hasClass("search")){
	jQuery("#result").fadeOut(); 
	}
});
$('#searchid').click(function(){
	jQuery("#result").fadeIn();
});
});
</script>
<style type="text/css">
	
	#searchid
	{
		border:solid 1px #000;
		font-size:14px;
	}
	#result
	{
		position:absolute;
		width:500px;
		display:none;
		margin-top:35px;
		border-top:0px;
		overflow:hidden;
		border:1px #CCC solid;
		background-color: white;
	}
	.show
	{
		padding:10px; 
		border-bottom:1px #999 dashed;
		font-size:15px; 
		height:10px;
	}
	.show:hover
	{
		background:#4c66a4;
		color:#FFF;
		cursor:pointer;
	}
</style>
{/block}
{block name="body"}
<form name="form1" method="POST" action="bookEntry.php" onsubmit="return confirm('DO You Want To Submit !!!!!')";>
<table align="center" border="0" class="table2">
<div class="hd"><h2 align="center">Book Entry</h2>
<input type="hidden" name="bookMasterId" value="{$bookMasterId}">
<tr>
  <td align="center" colspan="2" style="color:red; font-size:18px;"><b>{$confirm}</b></td>
</tr>
<tr>
  <td class="table2">Book Type</td>
  <td class="table2 form01">
  	<select name="bookType" id="bookType" REQUIRED Autofocus >
      {html_options values=$typeArr.bookType output=$typeArr.bookType selected=$bookType}
    </select>
	</td>
</tr>
<tr>
  <td align="left" class="table2 form01">Book Accession No : </td>
  <td class="table2 form01"><input type="text" name="bookAccessionNo" id="bookAccessionNo" value="{$bookAccessionNo}" REQUIRED onblur='checkBook();' /></td>
</tr>
<tr>
  <td align="left" class="table2 form01">ISBN : </td>
  <td class="table2 form01"><input type="text" name="ISBN" id="ISBN" value="{$ISBN}" ' /></td>
</tr>

<tr>
  <td class="table2">Book Title</td>
  <td class="table2 form01">
  	<input type="text" name="bookTitle" value="{$bookTitle}" />
	</td>
</tr>
<tr>
  <td class="table2">Author 1</td>
  <td class="table2 form01">
  	<input type="text" name="author1" value="{$author1}" />
	</td>
</tr>
<tr>
  <td class="table2">Author 2</td>
  <td class="table2 form01">
  	<input type="text" name="author2" value="{$author2}" />
	</td>
</tr>
<tr>
  <td class="table2">Location</td>
  <td class="table2 form01">
  	<input type="text" name="location" value="{$location}" />
	</td>
</tr>
<tr>
  <td class="table2">Price</td>
  <td class="table2 form01">
  	<input type="text" name="price" value="{$price}" />
	</td>
</tr>
<tr>
  <td class="table2">Publisher</td>
  <td class="table2 form01">
  	<input type="text" name="publisherId" value="{$publisherId}" class="search" id="searchid" style='margin-top: 1px;' />
  	<div id="result"></div>
	</td>
</tr>
<tr>
  <td class="table2">Language</td>
  <td class="table2 form01">
  	<input type="text" name="language" value="{$language}" />
	</td>
</tr>
<tr>
  <td class="table2"></td>
  <td class="table2 form01" align="center"><input type="submit" name="Submit" class="button" value="Save"></td>
</tr>
</table>
</form>
{/block}
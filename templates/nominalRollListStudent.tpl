{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 10,
		"bJQueryUI":true
  });
});
</script>
{/block}
{block name="body"}
</br></br>
<!--form name="formGet" method="GET" action="nominalRollListStudent.php">
<table align="center">
	<tr>
		<td class="table2 form01">
	    <select name="startYear" id="startDateYear">
	      {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYear}
	    </select>
	  </td>
    <td>
      <input type="submit" name="submit" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
</form-->
<table align="center" border="1">
	<div class="hd"><h2 align="center">My Nominal Roll</h2>
	<thead>
	<tr>
		<td align="left" class="table1"><b>Sr No</b></td>
		<td align="left" class="table1"><b>Academic Year</b></td>
		<td align="left" class="table1"><b>GR No</b></td>
		<td align="left" class="table1"><b>Roll No</b></td>
		<td align="left" class="table1"><b>Student Name</b></td>
		<td align="left" class="table1"><b>Class</b></td>
		<td align="left" class="table1"><b>Section</b></td>
		<!--td align="left" class="table1"><b>Fee Group</b></td>
		<td align="left" class="table1"><b>Library Member Type</b></td>
		<td align="left" class="table1"><b>Bus Route</b></td>
		<td align="left" class="table1"><b>Bus Stop</b></td>
		<td align="left" class="table1"><b>Subject Group</b></td>
		<td align="left" class="table1"><b>Co Scholastic Group</b></td>
		<td align="left" class="table1"><b>Club Term 1</b></td>
		<td align="left" class="table1"><b>Club Term 2</b></td-->
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$nominalArr}
  <tr>
  	<td align="left" class="table2">{$smarty.section.sec.rownum}</td>
  	<td align="left" class="table2">{$nominalArr[sec].academicYear}</td>
    <td align="left" class="table2">{$nominalArr[sec].grNo}</td>
    <td align="left" class="table2">{$nominalArr[sec].rollNo}</td>
    <td align="left" class="table2">{$nominalArr[sec].studentName}</td>
    <td align="left" class="table2">{$nominalArr[sec].class}</td>
    <td align="left" class="table2">{$nominalArr[sec].section}</td>
    <!--td align="left" class="table2">{$nominalArr[sec].feeGroup}</td>
    <td align="left" class="table2">{$nominalArr[sec].libraryMemberType}</td>
    <td align="left" class="table2">{$nominalArr[sec].busRoute}</td>
    <td align="left" class="table2">{$nominalArr[sec].busStop}</td>
    <td align="left" class="table2">{$nominalArr[sec].subjectGroup}</td>
    <td align="left" class="table2">{$nominalArr[sec].coScholasticGroup}</td>
    <td align="left" class="table2">{$nominalArr[sec].clubTerm1}</td>
    <td align="left" class="table2">{$nominalArr[sec].clubTerm2}</td-->
  </tr>
  {/section}
  </tbody>
</table>
{/block}
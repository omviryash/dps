{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 10,
		"bJQueryUI":true
  });
});
</script>
{/block}
{block name="body"}
</br></br>
<!--form name="formGet" method="GET" action="myClassTeacher.php">
<table align="center">
	<tr>
		<td class="table2 form01">
	    <select name="startYear" id="startDateYear">
	      {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYear}
	    </select>
	  </td>
    <td>
      <input type="submit" name="submit" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
</form-->
<table align="center" border="1">
	<div class="hd"><h2 align="center">My Class Teacher</h2>
	<thead>
	<tr>
		<td align="left" class="table1"><b>Academic Year</b></td>
		<td align="left" class="table1"><b>Class</b></td>
		<td align="left" class="table1"><b>Section</b></td>
		<td align="left" class="table1"><b>Teacher Name</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$allotArr}
  <tr>
  	<td align="left" class="table2">{$allotArr[sec].academicYear}</td>
    <td align="left" class="table2">{$allotArr[sec].class}</td>
    <td align="left" class="table2">{$allotArr[sec].section}</td>
    <td align="left" class="table2">{$allotArr[sec].teacherName}</td>
  </tr>
  {/section}
  </tbody>
</table>
{/block}
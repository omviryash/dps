{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script src="./media1/js/jquery-1.4.4.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.jeditable.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[100, 25, 50, 100, 500, 1000], [100, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 100,
		"bJQueryUI":true
  });
  
  /* Editable Start*/
	var oTable = $('#myDataTable').dataTable();
     
  /* Apply the jEditable handlers to the table */
  oTable.$('th').editable( './updateRollNoData.php', {
      "submitdata": function ( value, settings ) {
          return {
              "row_id": this.parentNode.getAttribute('th.id'),
              "column": oTable.fnGetPosition( this )[2]
          };
      },
      "height": "20px",
      "width": "100"
  });
  
  /* Editable End*/
  $(".omAttend").change(function()
  {
  	$('.newGoBtnClick').click();
  });
});
function deleteMarks(grno,sMasterId)
{
  if(sMasterId == 0)
  {
    alert('Please select appropriate schedule test');
    return false;
  }
  else
  {
    if(confirm('Are You Sure you want to delete marks?'))
    {
      /*alert('gr '+grno+' sMasterI '+sMasterId);
      alert('classs '+$('#class').val());
      alert('classSectio '+$('#classSection').val());
      $('#startDateYear').val();*/
      $.ajax({
          type:"POST",
          url: "phpAjax.php",
          cache: false,
          data: {
              gr_no: grno,
              schedule_MasterId: sMasterId,
              action: 'delExamMarks'
            },
          success:function(data)
          { 
            if(data == 'Okay')
            {
              alert('Marks removed successfully.');
              location.href='onlineTestResult.php?class='+$('#class').val()+'&classSection='+$('#classSection').val()+'&scheduleMasterId='+sMasterId+'&startYear='+$('#startDateYear').val()+'&submit=Go';
            }
            else
              alert(data);         
          }
        });
        return false;
    }
    else
      return false;
  }
  
}
</script>
{/block}
{block name="body"}
</br></br>
<form name="formGet" method="GET" action="onlineTestResult.php">
<table align="center">
	<tr>
		<td class="table2 form01">
		  <select id="class" name="class" autofocus="autofocus" required class="omAttend">
		    <option value="">Select class</option>
		    {html_options values=$cArray.className output=$cArray.className selected=$class}
		  </select>
	  </td>
    <td class="table2 form01">
		  <select id="classSection" name="classSection" class="omAttend">
		    <option value="0">Select Section</option>
		    {html_options values=$secArrOut output=$secArrOut selected=$section}
		  </select>
	  </td>
    <td class="table2 form01">
		  <select name="scheduleMasterId" class="omAttend">
		    <option value="0">Select Schedule Test</option>
		    {html_options values=$sArray.scheduleMasterId output=$sArray.scheduleDate selected=$scheduleMasterId}
		  </select>
	  </td>
	  <td class="table2 form01">
      <select name="startYear" id="startDateYear">
        {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYear}
      </select>
	  </td>
    <td>
      <input type="submit" name="submit" class="newGoBtn newGoBtnClick" value="Go">
    </td>
  </tr>
</table>
</form>
<!--
{if $s_userType != 'idCrdUser'}
<center><a href="nominalRollClassPrint.php?class={$class}&section={$section}">Print</a></center>
{/if}
<center><a href="idPrint.php?class={$class}&section={$section}">Id Card Print</a></center-->
<table align="left" border="1" id="myDataTable" class="display">
	<div class="hd"><h2 align="center">Online Test Result</h2></div>
	<thead>
	<tr>
		<td align="left"><b>Sr No</b></td>
		<td align="left"><b>GR No</b></td>
		<td align="left"><b>Roll No</b></td>
		<td align="left"><b>Student Name</b></td>
		<td align="left"><b>Marks</b></td>
    {if $scheduleMasterId > 0}
      <td align="left" width="145"><b>Delete Marks</b></td>
    {/if}
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$nominalArr}
  <tr>
  	<td align="left">{$smarty.section.sec.rownum}</td>
    <td align="left">{$nominalArr[sec].grNo}</td>
    <th align="left" id="{$nominalArr[sec].nominalRollId}_1">{$nominalArr[sec].rollNo}</th>
    <td align="left">{$nominalArr[sec].studentName}</td>
    {if $nominalArr[sec].totalMarks != ''}
    <td align="left">{$nominalArr[sec].totalMarks}</td>
    {else}
    <td align="left">0</td>
    {/if}
    {if $scheduleMasterId > 0}
      <td align="left" id="delMarks"><a href="javascript:;" onclick="javascript:deleteMarks({$nominalArr[sec].grNo},{$scheduleMasterId});"><img src="images/iconRemove.png" width="20" height="20" alt="Delete Marks" title="Delete Marks" style="margin-left:35px;"></a></td>
    {/if}
  </tr>
  {/section}
  </tbody>
</table>
{/block}
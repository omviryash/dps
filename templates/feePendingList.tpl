{include file="./main.tpl"}
{block name="head"}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script src="./media1/js/jquery-1.4.4.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.jeditable.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[-1, 10, 20, 30, 40, 50], ["All", 10, 20, 30, 40, 50]],
  	"iDisplayLength": 500,
  	"aaSorting": [[2, 'desc']],
		"bJQueryUI":true
  });
  $(".omAttend").change(function()
  {
  	$('.newGoBtnClick').click();
  });
  
  $("#startDateYear").change(function()
  {
    var yearExtra  = $('#startDateYear').val();
		var endDate    = yearExtra;
		var datastring = 'endDate=' + endDate;
		$.ajax({	
			type: 'GET',
			url: 'endYear.php',
		  data: datastring,
		  success:function(data)
		  {
		  	$('#endDateYear').val(data);
	    }
	  });
  });
  setStudName();
});

function setStudName()
{
	var grNoNew = $("#grNoNew").val() != '' ? $("#grNoNew").val() : 0;
	var dpsClass = $("#dpsClass").val() != '' ? $("#dpsClass").val() : 0;
	var classSection = $("#classSection").val() != '' ? $("#classSection").val() : '';
	var startYear = $("#startYear").val() != '' ? $("#startYear").val() : '';
	var dataString = "class=" + dpsClass + "&classSection=" + classSection + "&startYear=" + startYear + "&grNoNew=" + grNoNew;
	$.ajax({
		type : 'GET',
		url  : 'setStudNamePendingFee.php',
		data :  dataString,
		success:function(data)
		{
			$('#stdNameBest').html(data);
	  }
	});
}
</script>
{/block}
{block name="body"}
</br></br>
<link rel="stylesheet" href="./css/buttonStyle.css" type="text/css" />
<form name="formGet" method="GET" action="feePendingList.php">
<table align="center">
	<tr>
		<td class="table2 form01">
		  <select name="class" autofocus="autofocus" class='omAttend' id="dpsClass">
		    <option value="">Select Class</option>
		    {html_options values=$cArray.className output=$cArray.className selected=$class}
		  </select>
	  </td>
    <td class="table2 form01">
		  <select name="classSection" id="classSection" onChange="setStudName();">
		    <option value="0">Select Section</option>
		    {html_options values=$secArrOut output=$secArrOut selected=$section}
		  </select>
	  </td>
	  <td class="table2 form01">
		  <select name="startYear" onChange="setStudName();" id="startYear">
		    {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYearSelected}
		  </select>
		</td>
	  <td class="table2 form01" id='stdNameBest'>
	  	<select name="grNo">
				<option value='0'>Select</option>
				{html_options values=$stdArray.grNo output=$stdArray.studentName selected=$grNo}
			</select>
			<input type="" id="grNoNew" value="{$grNo}">
		</td>
    <td>
      <input type="submit" name="submit" class="newGoBtn newGoBtnClick" value="Go">
    </td>
  </tr>
</table>
</form>
<form name="form2" method="POST" action="feePendingList.php">
<table align="center" border="1" id="myDataTable" class="display">  
  </br>
	<div class="hd"><h2 align="center">Fee Pending Report</h2></div>
	</br>
	<thead>
	<tr>
		<td align="left" class="table1"><b>Fee Type</b></td>
		<td align="left" class="table1"><b>Amount</b></td>
		<td align="left" class="table1"><b>Amount Collection</b></td>
		<td align="left" class="table1"><b>Status</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$attendenceArr}
  <tr class="gradeRow">
  	<td align="left" class="table2">{$attendenceArr[sec].feeType}</td>
    <td align="left" class="table2">{$attendenceArr[sec].amount}</td>
    {if $attendenceArr[sec].amountTotal > 0}
    <td align="left" class="table2">{$attendenceArr[sec].amountTotal}</td>
    {else}
    <td align="left" class="table2">0</td>
    {/if}
    <td align="left" class="table2">{$attendenceArr[sec].status}</td>
  </tr>
  {/section}
  </tbody>
</table>
</form>
{/block}
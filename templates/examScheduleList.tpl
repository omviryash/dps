{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
		"bAutoWidth": false,
		"aaSorting": [],
    "aLengthMenu": [[1000], [1000]],
  	"iDisplayLength": 1000,
		"bJQueryUI":true
  });
});
</script>
{/block}
{block name="body"}
</br></br>
<form name="formGet" method="GET" action="examScheduleList.php">
<table align="center">
	<tr>
		<td class="table2 form01">
		  <select name="startYear">
		    {html_options values=$dateArrVal output=$dateArrOut selected=$attendenceStartDateSelected}
		  </select>
		</td>
    <td>
      <input type="submit" name="submit" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
</form>
<table align="left" border="1" id="myDataTable" class="display">
	 <div class="hd"><h2 align="center">Exam Schedule</h2>
	<thead>
	<tr>
		{if $s_userType != 'Teacher'}
		  <td align="left"><b>&nbsp;</b></td>
		{/if}
		<td align="left"><b>Date</b></td>
		<td align="left"><b>Exam Type</b></td>
		<td align="left"><b>Class</b></td>
		<td align="left"><b>Subject Name</b></td>
		<td align="left"><b>Max Marks</b></td>
		<td align="left"><b>Min Marks</b></td>
		<td align="left"><b>Student View</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$empArray}
  <tr>
  	{if $s_userType != 'Teacher'}
  	  <th align="left"><a href="examScheduleEntry.php?examScheduleId={$empArray[sec].examScheduleId}">Edit</a></th>
  	{/if}
  	<td align="left">{$empArray[sec].scheduleDate|date_format:"%d-%m-%Y"}</td>
  	<td align="left">{$empArray[sec].examType}</td>
  	<td align="left">{$empArray[sec].class}</td>
    <td align="left">{$empArray[sec].subjectName}</td>
    <td align="left">{$empArray[sec].maxMarks}</td>
    <td align="left">{$empArray[sec].minMarks}</td>
    <td align="left">{$empArray[sec].studentView}</td>
  </tr>
  {/section}
  </tbody>
</table>
{/block}
{include file="./main.tpl"}
{block name="body"}
<div class="hd"><h2 align="center">My Profile</h2></div>
<table align="center" border="1" cellpadding="1" cellspacing="1" style="width:50%">
	<tr>
		<td align="center" class="table1">{$stdArray.studentImage}</td>
		<td align="center" class="table1">{$stdArray.fatherImage}</td>
		<td align="center" class="table1">{$stdArray.motherImage}</td>
        </tr>
</table>
<table align="center" border="1" cellpadding="1" cellspacing="1" style="width:50%">
	<tr>
		<td align="left" class="table1"><b>Name : </b></td>
		<td align="left" class="table1"><b> : </b></td>
		<td align="left" class="table2">{$stdArray.studentName}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Father Name</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$stdArray.fatherName}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Mother Name</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$stdArray.mothersName}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>G. R. No.</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$stdArray.grNo}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Date Of Birth</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$stdArray.dateOfBirth}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Joined In Class</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$stdArray.joinedInClass}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Joining Date</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$stdArray.joiningDate}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Gender</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$stdArray.gender}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>SMS Mobile</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$stdArray.smsMobile}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Current Address</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$stdArray.currentAddress}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Residence Phone1</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$stdArray.residencePhone1}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Residence Phone2</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$stdArray.residencePhone2}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Email</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$stdArray.studentEmail}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Father Phone</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$stdArray.fatherPhone}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Father Email</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$stdArray.fatherEmail}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Father Mobile</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$stdArray.fatherMobile}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Father Occupation</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$stdArray.fatherOccupation}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Father Designation</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$stdArray.fathersDesignation}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Father Organization</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$stdArray.fathersOrganization}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Mother Phone</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$stdArray.mothersPhone}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Mother Email</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$stdArray.mothersEmailid}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Mother Mobile</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$stdArray.mothersMobile}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Mother Occupation</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$stdArray.motherOccupation}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Mother Designation</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$stdArray.mothersDesignation}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Mother Organization</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$stdArray.mothersOrganization}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Permanent Address</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$stdArray.permanentAddress}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Religion</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$stdArray.religion}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Blood Group</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$stdArray.bloodGroup}</td>
  </tr>
                    <tr>
  	<td align="left" class="table1"><b>House</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$stdArray.houseName}</td>
  </tr>
</table>
{/block}
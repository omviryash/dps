{include file="./main.tpl"}
<br>
<br>
<br>
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 10,
		"bJQueryUI":true
  });
  
});
</script>
{/block}
{block name="body"}
</br></br>

<table align="left" border="1" id="myDataTable" class="display">
	<div class="hd"><h2 align="center">Fee Type List</h2>
	<thead>
	<tr>
		<td align="center"><b>Edit</b></td>
		<td align="center"><b>Fee Type</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$feeTypeArray}
  <tr>
  	<td><a href="feeType.php?feeTypeId={$feeTypeArray[sec].feeTypeId}">Edit</a></td>
    <td align="left">{$feeTypeArray[sec].feeType}</td>
  </tr>
  {/section}
  </tbody>
</table>
{/block}
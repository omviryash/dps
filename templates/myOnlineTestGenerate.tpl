{include file="./main.tpl"}
{block name="head"}
<link rel="stylesheet" href="./css/buttonStyle.css" type="text/css" />
<script type="text/javascript">
$(document).ready(function(){	
setTimeout(function(){
	var dataString = "grNo=" + $("#grNo").val() + "&scheduleMasterId=" + $("#scheduleMasterId").val();
  $.ajax
  ({
    type: "GET",
    url: "page.php",
    data: dataString,
    success:function(data)
    {
      if(data == 0)
      {
      	location.href = "myOnlineResult.php?grNo=" + $("#grNo").val() + "&scheduleMasterId=" + $("#scheduleMasterId").val();
      }
    }
  });
},$('#timeIdNew').val());

noBack();

});

function buttonClickPrev()
{
	$('.previous').click();
}

function buttonClickNext()
{
	$('.nextPage').click();
}

function noBack()
{
  window.history.forward(1);
}

var isNS = (navigator.appName == "Netscape") ? 1 : 0;
if(navigator.appName == "Netscape") document.captureEvents(Event.MOUSEDOWN||Event.MOUSEUP);
function mischandler()
{
  return false;
}

function mousehandler(e)
{
  var myevent = (isNS) ? e : event;
	 var eventbutton = (isNS) ? myevent.which : myevent.button;
  if((eventbutton==2)||(eventbutton==3)) return false;
}
document.oncontextmenu = mischandler;
document.onmousedown = mousehandler;
document.onmouseup = mousehandler;

window.onkeydown = function (e)
{
  if(e.which == 8)//backspace
  {
    return false;
  }
  if(e.which == 9)//tab
  {
    return false;
  }
  if(e.which == 17)//ctrl
  {
    return false;
  }
  if(e.which == 18)//alt
  {
    return false;
  }
  if(e.which == 82)//r
  {
    return false;
  }
  if(e.which == 116)//f5
  {
    return false;
  }
  if(e.which == 117)//f6
  {
    return false;
  }
}
</script>
{/block}
{block name="body"}
</br></br>
<form name="counter" id="counter" action="myOnlineTestGenerate.php" method="get">
<table align="left" border="1" width='100%'>
  <tr>
    <td width="80%"  vAlign='top'>
			<table align="center" border="0" width="100%">
			  <tr>
			  	<td align="left">
			  	  <h1 align="left">{$studentName} ({$class}-{$section})</h1>
			  	</td>
				  <td align="right">
			  	  <input type="text" size="10" name="d2" id='timeId' value='{$d2}' style='font-size:25px;' READONLY >
				    <input type="hidden" id='timeIdNew' value='{$d23New}' >
			    </td>
			  </tr>
			  <tr>
			  	<td colspan='2' align="left" style='color:black; height:50px; font-size:15px;'> Question No : {$questonNo}</td>
			  </tr>
			  {section name="sec" loop=$stdArray}
			  <input type="hidden" size="8" name="d3" id='timeId3' value='{$d3}'>
			  <input type="hidden" name="" id='grNo' value="{$grNo}">
			  <input type="hidden" name="scheduleMasterId" id='scheduleMasterId' value="{$scheduleMasterId}">
			  <input type="hidden" name="myOnlineTestId" value="{$stdArray[sec].myOnlineTestId}">
			  <tr>
			  	<td colspan='2' align="left" class="table1" style='color:black; height:50px; font:20px Arial, Verdana, Helvetica, sans-serif;'>{$stdArray[sec].question}</br></td>
			  </tr>
			  <tr>
			  	{if $stdArray[sec].answer == 'A'}
			    <td colspan='2' align="left" class="table2" style='color:black; height:50px; font-size:20px;'><input type="radio" name="answerMain" value='A' CHECKED >
			    {else}
			    <td colspan='2' align="left" class="table2" style='color:black; height:50px; font-size:20px;'><input type="radio" name="answerMain" value='A'>
			    {/if}
			    {$stdArray[sec].option1}</td>
			  </tr>
			  <tr>
			    {if $stdArray[sec].answer == 'B'}
			    <td colspan='2' align="left" class="table2" style='color:black; height:50px; font-size:20px;'><input type="radio" name="answerMain" value='B' CHECKED >
			    {else}
			    <td colspan='2' align="left" class="table2" style='color:black; height:50px; font-size:20px;'><input type="radio" name="answerMain" value='B'>
			    {/if}
			    {$stdArray[sec].option2}</td>
			  </tr>
			  <tr>
			    {if $stdArray[sec].answer == 'C'}
			    <td colspan='2' align="left" class="table2" style='color:black; height:50px; font-size:20px;'><input type="radio" name="answerMain" value='C' CHECKED >
			    {else}
			    <td colspan='2' align="left" class="table2" style='color:black; height:50px; font-size:20px;'><input type="radio" name="answerMain" value='C'>
			    {/if}
			    {$stdArray[sec].option3}</td>
			  </tr>
			  <tr>
			    {if $stdArray[sec].answer == 'D'}
			    <td colspan='2' align="left" class="table2" style='color:black; height:50px; font-size:20px;'><input type="radio" name="answerMain" value='D' CHECKED >
			    {else}
			    <td colspan='2' align="left" class="table2" style='color:black; height:50px; font-size:20px;'><input type="radio" name="answerMain" value='D' >
			    {/if}
			    {$stdArray[sec].option4}</td>
			  </tr>
			  {/section}
			  </tr>
			</table>
			<table width="100%" border="0" align="right">
				<tr>
			    <td align="left">
			    	<input type="submit" name="page" class='previous' value="{$previous}">
			    	{if $previous > 0}
			    	  <input type="button" name="" class='newSaveBtn' onclick="buttonClickPrev();" value="Previous">
			    	{/if}
			    </td>
			    <td align="right">
			    	<input type="submit" name="page" class='nextPage' value="{$nextPage}">
			    	{if $nextPage > 0}
			    	  <input type="button" name="" class='newSaveBtn' onclick="buttonClickNext();" value="Save And Next">
			    	{else}
			    	  <input type="button" name="" class='newGoBtn' onclick="buttonClickNext();" value="End Test">
			    	{/if}
			    </td>
			  </tr>
	    </table>
    </td>
    <td width="20%" vAlign='top'>
			<table width="10%" border="0" align="center">
				{section name="sec2" loop=$pageArray}
				<tr>
					{if $pageArray[sec2].answer != '' }
			    <td align="left" style='background-color:green; color:white; '>
			    	{$smarty.section.sec2.rownum}
			    </td>
			    {/if}
			    {if $pageArray[sec2].answer == '' }
			    <td align="left" style='background-color:red; color:white;'>
			    	{$smarty.section.sec2.rownum}
			    </td>
			    {/if}
			  </tr>
			  {/section}
			</table>
    </td>
  </tr>
</table>
<!--table width="25%" border="1" align="right">
	<tr>
    <td align="left">
      {$pagination}
    </td>
  </tr>
</table-->
</form>
<script> 
var mySeconds = $('#timeId3').val();
var seconds = mySeconds
var myMinutes = $('#timeId').val();
var minutes = myMinutes

function display(){ 
if(seconds<=0)
{
  seconds=60
  minutes-=1
}
if(minutes<=-1)
{
    seconds=0
    minutes+=1
}
else
  seconds-=1
  document.counter.d2.value=minutes+':'+seconds
  setTimeout("display()",1000)
}
display()

</script>
{/block}
{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
		"bAutoWidth": false,
    "aLengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 10,
		"bJQueryUI":true
  });
});
</script>
{/block}
{block name="body"}
</br></br>
<table align="left" border="1" id="myDataTable" class="display">
	 <div class="hd"><h2 align="center">Teacher List</h2>
	<thead>
	<tr>
		<td align="left"><b>Name</b></td>
		<td align="left"><b>Date Of Birth</b></td>
		<td align="left"><b>Joining Date</b></td>
		<td align="left"><b>Gender</b></td>
		<td align="left"><b>Current Address</b></td>
		<td align="left"><b>Residence</b></td>
		<td align="left"><b>Mobile</b></td>
		<td align="left"><b>Phone1</b></td>
		<td align="left"><b>Email</b></td>
		<td align="left"><b>Marital</b></td>
		<td align="left"><b>Account No</b></td>
		<td align="left"><b>Qualification</b></td>
		<td align="left"><b>Experience</b></td>
		<td align="left"><b>Image</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$empArray}
  <tr>
  	<td align="left" nowrap>{$empArray[sec].name}</td>
    <td align="left">{$empArray[sec].dateOfBirth|date_format:'%d-%m-%Y'}</td>
    <td align="left">{$empArray[sec].joiningDate|date_format:'%d-%m-%Y'}</td>
    <td align="left">{$empArray[sec].gender}</td>
    <td align="left">{$empArray[sec].currentAddress}</td>
    <td align="left">{$empArray[sec].residenceTelephone}</td>
    <td align="left">{$empArray[sec].mobile}</td>
    <td align="left">{$empArray[sec].phone1}</td>
    <td align="left">{$empArray[sec].email}</td>
    <td align="left">{$empArray[sec].marital}</td>
    <td align="left">{$empArray[sec].accountNo}</td>
    <td align="left">{$empArray[sec].qualification}</td>
    <td align="left">{$empArray[sec].experience}</td>
    <td align="left"><img src="./employeeImage/{$empArray[sec].empImage}"></td>
  </tr>
  {/section}
  </tbody>
</table>
{/block}
{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 10,
		"bJQueryUI":true
  });
});
</script>
{/block}
{block name="body"}
<div class="hd"><h2 align="center">My Library Book List</h2></div>
<table align="center" border="2" id="myDataTable" class="display">
	<thead>
	<tr>
		<td align="center" class="table1"><b>Issue Date</b></td>
		<td align="center" class="table1"><b>Book Accession No</b></td>
		<td align="center" class="table1"><b>Book Title</b></td>
		<td align="center" class="table1"><b>Returnable Date</b></td>
		<td align="center" class="table1"><b>Return Book</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$classArray}
  <tr>
    <td align="left" class="table2">{$classArray[sec].issueDate}</td>
    <td align="left" class="table2">{$classArray[sec].bookAccessionNo}</td>
    <td align="left" class="table2">{$classArray[sec].bookTitle}</td>
    <td align="left" class="table2">{$classArray[sec].returnableDate}</td>
    <td align="left" class="table2">{$classArray[sec].returnBook}</td>
  </tr>
  {/section}
  </tbody>
</table>
{/block}
{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 10,
		"bJQueryUI":true
  });
});
</script>
{/block}
{block name="body"}
</br></br>
<form name="formGet" method="GET" action="myFee.php">
<table align="center">
	<tr>
		<td class="table2 form01">
		  <select name="startYear">
		    {html_options values=$dateArrVal output=$dateArrOut selected=$studStartYear}
		  </select>
		</td>
    <td>
      <input type="submit" name="submit" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
</form>
<table align="center" border="1">
	<div class="hd"><h2 align="center">My Fees Transaction List</h2></div>
	<thead>
	<tr>
		<td align="left" class="table1"><b>Academic Year</b></td>
	  <td align="left" class="table1"><b>Fee Type</b></td>
	  <td align="left" class="table1"><b>Amount</b></td>
	  <td align="left" class="table1"><b>Fee Date</b></td>
	  <td align="left" class="table1"><b>Receipt No</b></td>
	  <td align="left" class="table1"><b>Mode Of Pay</b></td>
	  <td align="left" class="table1"><b>Ch No</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$markArr}
  <tr>
  	<td align="left" class="table2">{$studStartYear}-{$studEndYear}</td>
    <td align="left" class="table2">{$markArr[sec].feeType}</td>
    <td align="left" class="table2">{$markArr[sec].amount}</td>
    <td align="left" class="table2">{$markArr[sec].feeDate}</td>
    <td align="left" class="table2">{$markArr[sec].rNo}</td>
    <td align="left" class="table2">{$markArr[sec].modeOfPay}</td>
    <td align="left" class="table2">{$markArr[sec].chNo}</td>
  </tr>
 {/section}
 </tbody>
</table>
{/block}
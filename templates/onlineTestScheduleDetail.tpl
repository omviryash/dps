{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
		"bAutoWidth": false,
    "aLengthMenu": [[100, 500, 1000], [100, 500, 1000]],
  	"iDisplayLength": 100,
  	"aaSorting": [[ 1, "desc" ]],
		"bJQueryUI":true
  });
});
</script>
{/block}
{block name="body"}
<div class="hd"><h2 align="center">Question List</h2></div>
<table align="center" border="2" id="myDataTable" class="display">
	<thead>
	<tr>
		<td align="center" class="table1"><b>Class</b></td>
		<td align="center" class="table1"><b>Question</b></td>
		<td align="center" class="table1"><b>Option 1</b></td>
		<td align="center" class="table1"><b>Option 2</b></td>
		<td align="center" class="table1"><b>Option 3</b></td>
		<td align="center" class="table1"><b>Option 4</b></td>
		<td align="center" class="table1"><b>Answer</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$classArray}
  <tr>
    <td align="left" class="table2">{$classArray[sec].class}</td>
    <td align="left" class="table2">{$classArray[sec].question}</td>
    <td align="left" class="table2">{$classArray[sec].option1}</td>
    <td align="left" class="table2">{$classArray[sec].option2}</td>
    <td align="left" class="table2">{$classArray[sec].option3}</td>
    <td align="left" class="table2">{$classArray[sec].option4}</td>
    <td align="left" class="table2">{$classArray[sec].answer}</td>
  </tr>
  {/section}
  </tbody>
</table>
{/block}
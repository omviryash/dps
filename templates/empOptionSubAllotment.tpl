{include file="./main.tpl"}
{block name="head"}
<script type="text/javascript">
$(document).ready(function() {
  $("#optionSelect").change(function()
  {
    var optionSelect    = $('#optionSelect').val();
		var datastring = 'optionSelect=' + optionSelect;
		$.ajax({	
			type: 'GET',
			url: 'optionSubAjax.php',
		  data: datastring,
		  success:function(data)
		  {
		  	$('#subjectMasterId').html(data);
	    }
	  });
  });
});
</script>
{/block}
{block name="body"}
<form name="form1" method="POST" action="empOptionSubAllotment.php">
<table align="center" border="0" class="table2">
<div class="hd"><h2 align="center">Club Allotment</h2>
<tr>
  <td align="center" colspan="2" style="color:red; font-size:18px;"><b>{$confirm}</b></td>
</tr>
<tr>
  <td class="table2">Student Name</td>
  <td class="table2 form01">
	  <select name="grNo" autofocus="autofocus" required >
	    <option value="">Select Student</option>
	    {html_options values=$studArray.grNo output=$studArray.studentName selected=$employeeMasterId}
	  </select>
  </td>
</tr>
<tr>
  <td class="table2">Acadamic Year</td>
  <td class="table2 form01">
    <select name="startYear" id="startDateYear">
      {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYear}
    </select>
  </td>
</tr>
<tr>
  <td class="table2">Option</td>
  <td class="table2 form01">
	  <select name="termOutput" required id="optionSelect">
	    <option value="">Select Option</option>
	    {html_options values=$termOutput output=$termOutput}
	  </select>
  </td>
  </td>
</tr>
<tr>
  <td class="table2">Subject</td>
  <td class="table2 form01">
	  <select name="subjectMasterId" id="subjectMasterId">
	    <option value="">Select Subject</option>
	    {html_options values=$clubArr.subjectMasterId output=$clubArr.subjectName}
	  </select>
  </td>
</tr>
<tr>
  <td class="table2"></td>
  <td class="table2 form01" align="center"><input type="submit" name="Submit" class="button" value="Submit"></td>
</tr>
</table>
</form>
{/block}
{include file="./main.tpl"}
{block name="body"}
<form name="form1" method="POST" action="examScheduleEntry.php">
<table align="center" border="0" class="table2">
<div class="hd"><h2 align="center">Exam Schedule Entry</h2>
<input type="hidden" name="examScheduleId" value="{$examScheduleId}">
<tr>
  <td align="center" colspan="2" style="color:red; font-size:18px;"><b>{$confirm}</b></td>
</tr>
<tr>
  <td class="table2">Exam Type</td>
  <td class="table2 form01">
	  <select name="examTypeId" autofocus="autofocus" required autofocus="autofocus">
	    <option value="">Select Exam Type</option>
	    {html_options values=$exArray.examTypeId output=$exArray.examType selected=$examTypeId}
	  </select>
  </td>
</tr>
<tr>
  <td class="table2">Schedule Date</td>
  <td class="table2 form01">
    {html_select_date prefix="scheduleDate" start_year="-25" end_year="+25" field_order="DMY" time=$scheduleDate day_value_format="%02d"}
  </td>
</tr>
<tr>
  <td class="table2">Max Marks</td>
  <td class="table2 form01">
  	<input type="text" name="maxMarks" value="{$maxMarks}">
	</td>
</tr>
<tr>
  <td class="table2">Min Marks</td>
  <td class="table2 form01">
  	<input type="text" name="minMarks" value="{$minMarks}">
	</td>
</tr>
<tr>
  <td class="table2">Subject</td>
  <td class="table2 form01">
  	<select name="subjectMasterId" required >
      <option value="">Select Subject</option>
      {html_options values=$clubArr.subjectMasterId output=$clubArr.subjectName selected=$subjectMasterId}
	  </select>
	</td>
</tr>
<tr>
  <td class="table2">Class</td>
  <td class="table2 form01">
  	<select name="class" required >
	    <option value="">Select class</option>
	    {html_options values=$cArray.className output=$cArray.className selected=$class}
	  </select>
  </td>
</tr>
<tr>
  <td class="table2">Student View</td>
  <td class="table2 form01">
  	<select name="studentView">
      <option value="">Select View</option>
      {html_options values=$yesNoArrOut output=$yesNoArrOut selected=$studentView}
	  </select>
	</td>
</tr>
<tr>
  <td class="table2"></td>
  <td class="table2 form01" align="center"><input type="submit" name="Submit" class="button" value="Save"></td>
</tr>
</table>
</form>
<!--table align="center" border="2">
	<h2 align="center">Class List</h2>
	<tr>
		<td align="center" class="table1"><b>&nbsp;</b></td>
		<td align="center" class="table1"><b>Exam Type</b></td>
		<td align="center" class="table1"><b>Schedule Date</b></td>
		<td align="center" class="table1"><b>Max Marks</b></td>
		<td align="center" class="table1"><b>Min Marks</b></td>
		<td align="center" class="table1"><b>Class</b></td>
		<td align="center" class="table1"><b>Subject</b></td>
		<td align="center" class="table1"><b>Student View</b></td>
  </tr>
  {section name="sec" loop=$classArray}
  <tr>
    <td align="left" class="table2"><a href="examScheduleEntry.php?examScheduleId={$classArray[sec].examScheduleId}">Edit</a></td>
    <td align="left" class="table2">{$classArray[sec].examType}</td>
    <td align="left" class="table2">{$classArray[sec].scheduleDate}</td>
    <td align="left" class="table2">{$classArray[sec].maxMarks}</td>
    <td align="left" class="table2">{$classArray[sec].minMarks}</td>
    <td align="left" class="table2">{$classArray[sec].class}</td>
    <td align="left" class="table2">{$classArray[sec].subjectName}</td>
    <td align="left" class="table2">{$classArray[sec].studentView}</td>
  </tr>
  {/section}
</table-->
{/block}
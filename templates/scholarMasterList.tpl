{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script src="media2/js/jquery.dataTables.columnFilter.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable(
  {
    "bProcessing": true,
		"bServerSide": true,
		"bAutoWidth": false,
    "aLengthMenu": [[100, 500, 1000], [100, 500, 1000]],
  	"iDisplayLength": 100,
		"sPaginationType":"full_numbers",
		"bJQueryUI":true,
		"sAjaxSource": "./scholarMasterListAjax.php"
  });
});

</script>
{/block}
{block name="body"}
</br></br>
<table align="left" border="1" id="myDataTable" class="display dataTables_length">
	<div class="hd"><h2 align="center">Scholar Master List</h2>
	<thead>
	<tr>
		<td align="left"><b>Area Code</b></td>
		<td align="left"><b>Area</b></td>
		<td align="left"><b>Assesment</b></td>
		<td align="left"><b>Grade</b></td>
		<td align="left"><b>Descriptive</b></td>
  </tr>
  </thead>
  <tbody>
 
  </tbody>
</table>
{/block}
{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
{/block}
{block name="body"}
</br></br>
<div class="hd"><h2 align="center">Principal Dashboard</h2>
</div>
<div class="content">
<div class="row">
      <div class="col-lg-3 col-xs-6"> 
        <!-- small box -->
        <div class="small-box bg-light-blue">
          <div class="inner">
            <h3> {$presentTeacher}/{$totalTeacher} </h3>
            <p> Teaching Staff </p>
          </div>
          <a class="small-box-footer" href="attendEdit.php?attendDate={$today}"> More info  </a> </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-xs-6"> 
        <!-- small box -->
        <div class="small-box bg-orange">
          <div class="inner">
            <h3 id="count_reg">{$presentStudent}/{$totalStudent}</h3>
            <p> Student </p>
          </div>
          <a class="small-box-footer" href="attendenceReport.php"> More info  </a> </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-xs-6"> 
        <!-- small box -->
        <div class="small-box bg-grass">
          <div class="inner">
            <h3 id="count_present">{$presentAdmin}/{$totalAdmin}</h3>
            <p> Admin Staff </p>
          </div>
          <a class="small-box-footer" href="attendEdit.php?attendDate={$today}&staff_type=Admin Staff"> More info  </a> </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-xs-6"> 
        <!-- small box -->
        <div class="small-box bg-wood">
          <div class="inner">
            <h3 id="count_device">{$presentStudentIV}/{$totalStudentIV}</h3>
            <p> Class IV Staff </p>
          </div>
          <a class="small-box-footer" href="attendEdit.php?attendDate={$today}&staff_type=Class IV"> More info  </a> </div>
      </div>
      <!-- ./col --> 
</div>
</div>
{/block}
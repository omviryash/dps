{include file="./main.tpl"}
{block name="body"}
<form name="form1" method="POST" action="employeeClubAllotment.php">
<table align="center" border="0" class="table2">
<div class="hd"><h2 align="center">Club Allotment</h2>
<tr>
  <td class="table2">Student Name</td>
  <td class="table2 form01">
	  <select name="grNo" autofocus="autofocus" required >
	    <option value="">Select Student</option>
	    {html_options values=$studArray.grNo output=$studArray.studentName selected=$employeeMasterId}
	  </select>
  </td>
</tr>
<tr>
  <td class="table2">Acadamic Year</td>
  <td class="table2 form01">
    <select name="startYear" id="startDateYear">
      {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYear}
    </select>
  </td>
</tr>
<tr>
  <td class="table2">Term</td>
  <td class="table2 form01">
	  <select name="termOutput">
	    <option value="">Select Term</option>
	    {html_options values=$termOutput output=$termOutput}
	  </select>
  </td>
  </td>
</tr>
<tr>
  <td class="table2">Club</td>
  <td class="table2 form01">
	  <select name="clubName">
	    <option value="">Select Club</option>
	    {html_options values=$clubArr.clubName output=$clubArr.clubName}
	  </select>
  </td>
</tr>
<tr>
  <td class="table2"></td>
  <td class="table2 form01" align="center"><input type="submit" name="Submit" class="button" value="Submit"></td>
</tr>
</table>
</form>
{/block}
{include file="./main.tpl"}
{block name="head"}
<link rel="stylesheet" href="./css/buttonStyle.css" type="text/css" />
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
	
	.textWidthHeight { width: 100px; height: 200px }
</style>

<script type="text/javascript">
$(document).ready(function(){
	$('#myDataTable').dataTable({
		"aLengthMenu": [[100, 500, 1000], [100, 500, 1000]],
  	"iDisplayLength": 100,
  	"bAutoWidth": false,
  	"aaSorting": [[ 0, "asc" ]],
		"bJQueryUI":true
  });
  
	$(".omAttend").change(function()
  {
  	$('.newGoBtn').click();
  });
});


function buttonHide()
{
	$('#hideMe').hide();
}

</script>
<style>
.selectCombo
{
	width:500px;
}
</style>
{/block}
{block name="body"}
</br></br>
<center>
<form name="formGet" id="formGet" method="GET" action="generateMarksDetailEntryTry.php">
<table align="center" border="1">
	<div class="hd"><h2 align="center">Part 3 Other Detail</h2></div>
	<tr>
		<td class="table2 form01">
		  <select name="startYear" class="omAttend">
		    {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYearSelected}
		  </select>
		</td>
		{if $s_userType == 'Administrator'}
		<td class="table2 form01 omAttend">
		  <select name="class" autofocus="autofocus">
		    <option value="0">Select class</option>
		    {html_options values=$cArray.className output=$cArray.className selected=$class}
		  </select>
	  </td>
    <td class="table2 form01 omAttend">
		  <select name="section">
		    <option value="0">Select Section</option>
		    {html_options values=$secArrOut output=$secArrOut selected=$section}
		  </select>
	  </td>
	  {/if}
		<td>
	    <input type="submit" name="go" class="newGoBtn" value="go">
	  </td>
	</tr>
</table>
</form>
</center>
{if $count != 0}
<form name="form2" method="POST" action="generateMarksDetailEntryTry.php">
<table align="left" border="1">
</br></br></br>
<h1 align="center">Student List</h1>
</br>
<h1 align="center">{$class}/{$section}</h1>
</br>
<tr>
	<td align="right" class="table1" colspan='4'><b>Report Card Date Term 1 : </b></td>
  <td align="left" class="table2" colspan='3'>
	  {html_select_date prefix="reportDate1" start_year="-25" end_year="+25" field_order="DMY" day_value_format="%02d" time=$reportDate1}
	</td>
	<td align="right" class="table1" colspan='4'><b>Report Card Date Term 2 : </b></td>
	<td align="left" class="table2" colspan='3'>
	  {html_select_date prefix="reportDate2" start_year="-25" end_year="+25" field_order="DMY" day_value_format="%02d" time=$reportDate2}
	</td>
</tr>
</table>
<table align="left" border="1" id="myDataTable" class="display">
	<input type="hidden" name="startYear" value="{$academicStartYearSelected}">
  <thead>
	<tr>
		<td align="left" class="table1"><b>Roll. No.</b></td>
		<td align="left" class="table1"><b>G. R. No.</b></td>
		<td align="left" class="table1"><b>Name</b></td>
    <td align="left" class="table1"><b>My Goals</b></td>
    <td align="left" class="table1"><b>Strengths</b></td>
    <td align="left" class="table1"><b>Interest And Hobbies</b></td>
    <td align="left" class="table1"><b>Reponsibilities</b></td>
    <td align="left" class="table1"><b>Height</b></td>
    <td align="left" class="table1"><b>Weight</b></td>
    
    <td align="left" class="table1"><b>Vision L</b></td>
    <td align="left" class="table1"><b>Vision R</b></td>
    
    <td align="left" class="table1"><b>Dental Hygiene</b></td>
    <td align="left" class="table1"><b>Remark Term 1</b></td>
    <td align="left" class="table1"><b>Remark Term 2</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$dtlArr}
  <tr class="gradeRow">
  	<td align="left" class="table2">{$dtlArr[sec].rollNo}</td>
		<td align="left" class="table2">
    	{$dtlArr[sec].grNo}
    	<input type="hidden" name="examMarksPerTryId[]" value="{$dtlArr[sec].examMarksPerTryId}">
    </td>
    <td align="left" class="table2">{$dtlArr[sec].studentName}</td>
    <td align="left" class="table2"><textarea name="goals[]" class="textWidthHeight" >{$dtlArr[sec].goals}</textarea></td>
    <td align="left" class="table2"><textarea name="strengths[]" class="textWidthHeight" >{$dtlArr[sec].strengths}</textarea></td>
    <td align="left" class="table2"><textarea name="intHobbies[]" class="textWidthHeight" >{$dtlArr[sec].intHobbies}</textarea></td>
    <td align="left" class="table2"><textarea name="responsibilities[]" class="textWidthHeight" >{$dtlArr[sec].responsibilities}</textarea></td>
    <td align="left" class="table2"><input type="text" name="height[]" value="{$dtlArr[sec].height}" size="3" /></td>
    <td align="left" class="table2"><input type="text" name="weight[]" value="{$dtlArr[sec].weight}" size="3" /></td>
    
    <td align="left" class="table2"><textarea name="visionL[]" class="textWidthHeight" >{$dtlArr[sec].visionL}</textarea></td>
    <td align="left" class="table2"><textarea name="visionR[]" class="textWidthHeight" >{$dtlArr[sec].visionR}</textarea></td>
    
    <td align="left" class="table2"><textarea name="dentalHygiene[]" class="textWidthHeight" >{$dtlArr[sec].dentalHygiene}</textarea></td>
    <td align="left" class="table2"><textarea name="resultNote1[]" class="textWidthHeight" >{$dtlArr[sec].resultNote1}</textarea></td>
    <td align="left" class="table2"><textarea name="resultNote2[]" class="textWidthHeight" >{$dtlArr[sec].resultNote2}</textarea></td>
  </tr>
  {/section}
  </tbody>
  <tfoot>
  <tr>
    <td align="center" colspan="14" class="table2"><input type="submit" name="submitTaken" class="newSubmitBtn" value="Save"></td>
  </tr>
  </tfoot>
</table>
</form>
{/if}
{/block}
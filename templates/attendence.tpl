{include file="./main.tpl"}
{block name="head"}
<link rel="stylesheet" href="./css/buttonStyle.css" type="text/css" />
<script type="text/javascript">
$(document).ready(function(){
	$('.presentButton2').hide();
	$('.presentButtonYes2').hide();
	$('.absentButtonYes2').hide();
	$('.absentButton2').hide();
	
	$(".omAttend").change(function()
  {
  	$('.newGoBtn').click();
  });
});

function preAbs(objPa)
{
	var row = $(objPa).parents('.trRow');
	var preAbsButton = row.find(objPa).val();
	if(preAbsButton == 'P')
	{
	  row.find('.presentButton').hide();
	  row.find('.presentButtonYes').show();
	  row.find('.absentButtonYes').hide();
	  row.find('.absentButton').show();
	}
	else
  {
	  row.find('.presentButton').show();
	  row.find('.presentButtonYes').hide();
	  row.find('.absentButtonYes').show();
	  row.find('.absentButton').hide();
  }
}

function buttonHide()
{
	$('#hideMe').hide();
}
function attend(obj)
{
	var row = $(obj).parents('.trRow');
	var grNo         = row.find('.grNo').val();
	var dayExtra    = $('#goDateDay').val();
  var monthExtra  = $('#goDateMonth').val();
  var yearExtra   = $('#goDateYear').val();
	var attendDate  = yearExtra+'-'+monthExtra+'-'+dayExtra;
	var attendButton = row.find(obj).val();
	
	var dataString = "grNo=" + grNo + "&attendDate=" + attendDate + "&attendButton=" + attendButton;
	
	$.ajax({
	  type: "GET",
	  url: "dailyAttend.php",
	  data: dataString,
  });
}

function confirmDelete()
{
	if(confirm('Are You Sure 1 st time?'))
	{
		if(confirm('Are You Sure 2nd time?'))
		  return true;
		else
		  return false;
	}
	else
	  return false;
}
</script>
{/block}
{block name="body"}
</br></br>
<center>
<div class="hd"><h2 align="center">Take Attendance</h2></div>
<form name="formGet" method="GET" action="attendence.php">
<table align="center" border="1">
	<tr>
		{if $s_userType == 'Administrator'}
		<td class="table2 form01 omAttend">
		  <select name="class" autofocus="autofocus">
		    <option value="0">Select class</option>
		    {html_options values=$cArray.className output=$cArray.className selected=$class}
		  </select>
	  </td>
    <td class="table2 form01 omAttend">
		  <select name="section">
		    <option value="0">Select Section</option>
		    {html_options values=$secArrOut output=$secArrOut selected=$section}
		  </select>
	  </td>
	  {/if}
		<td>
      {html_select_date day_extra="id=\"goDateDay\"" month_extra="id=\"goDateMonth\"" year_extra="id=\"goDateYear\"" prefix="goDate" start_year="-5" end_year="+2" field_order="DMY" time=$goDate day_value_format="%02d"}
    </td>
    <td>
      <font size="3" color="Green"><b>{$weekDay}</b></font>
    </td>
    <td>
      <input type="submit" name="go" class="newGoBtn" value="Go"> 
    </td>
  </tr>
</table>
{if $count == 0 && $weekDay != 'Sunday'}
<table align="center" border="0">
  </br>
  <tr>
    <td>
	    <input type="submit" name="submit" onclick="buttonHide();" id="hideMe" class="newSubmitBtn" value="Generate">
	    <a href="reGenerateAttendence.php"> (Re-generate Attendance) </a>
	  </td>
	</tr>
</table>
{/if}
</center>
<table align="center" border="1">
  </br>
	<h1 align="center">Student List - Class:{$class}-{$section} </h1>
	
  </br>
	<tr>
		<td align="left" class="table1"><b>Sr.No.</b></td>
		<td align="left" class="table1"><b>GR. No.</b></td>
		<td align="left" class="table1"><b>Roll No.</b></td>
		<td align="left" class="table1"><b>Name</b></td>
		<!-- td align="left" class="table1"><b>M/F</b></td -->
		<td align="left" class="table1"><b>Present</b></td>
		<td align="left" class="table1"><b>Absent</b></td>
		<td align="left" class="table1">
		  <a href="allAttendDelete.php?class={$class}&section={$section}&goDate={$goDate}" onclick="return confirmDelete();">Delete All</a>
		</td>
  </tr>
  {section name="sec" loop=$stdArray}
  <tr class="trRow">
    <td align="center" class="table2">{$smarty.section.sec.rownum}</td>
    <td align="center" class="table2">{$stdArray[sec].grNo} <input type="hidden" class="grNo" value="{$stdArray[sec].grNo}"></td>
    <td align="center" class="table2">{$stdArray[sec].rollNo} <input type="hidden" class="grNo" value="{$stdArray[sec].grNo}"></td>
    <td align="left" class="table2">{$stdArray[sec].studentName}</td>
    <!-- td align="left" class="table2">{$stdArray[sec].gender}</td -->
    <td align="center" class="table2">
    	{if $stdArray[sec].attendences == 'P'}
    	  <input type="button" value="P" class="presentButtonYes" />
    	  <input type="button" class="presentButton presentButton2" value="P" onclick="attend(this); preAbs(this);">
    	{else}
    	  <input type="button" class="presentButton" value="P" onclick="attend(this); preAbs(this);">
    	  <input type="button" value="P" class="presentButtonYes presentButtonYes2" />
    	{/if}
    </td>
    <td align="center" class="table2">
    	{if $stdArray[sec].attendences == 'A'}
    	  <input type="button" value="A" class="absentButtonYes" />
    	  <input type="button" class="absentButton absentButton2" value="A" onclick="attend(this); preAbs(this);">
    	{else}
    	  <input type="button" class="absentButton" value="A" onclick="attend(this); preAbs(this);">
    	  <input type="button" value="A" class="absentButtonYes absentButtonYes2" />
    	{/if}
    </td>
    <td align="left" class="table2">
    	<a href="attendDelete.php?attendenceId={$stdArray[sec].attendenceId}&goDate={$goDate}" onclick="return confirmDelete();">Delete</a>
    </td>
  </tr>
  {sectionelse}
  <tr>
    <td align="center" colspan="7" class="table2"><h2>Not Taken</h2></td>
  </tr>
  {/section}
  </tr>
  {if $count != 0}
  <tr>
    <td align="center" colspan="7"><input type="submit" name="submitTaken" class="newSubmitBtn" value="Save"></td>
  </tr>
  {/if}
</table>
</form>
<br><br><br>
{/block}
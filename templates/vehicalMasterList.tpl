{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
		"bAutoWidth": false,
    "aLengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 10,
  	"aaSorting": [[ 2, "asc" ]],
		"bJQueryUI":true
  });
});
</script>
{/block}
{block name="body"}
<div class="hd"><h2 align="center">Vehicle Master List</h2></div>
<table align="center" border="2" id="myDataTable" class="display">
	<thead>
	<tr>
		<td align="center" class="table1"><b>Edit</b></td>
		<td align="center" class="table1"><b>Vehicle Type</b></td>
		<td align="center" class="table1"><b>Vehicle No</b></td>
		<td align="center" class="table1"><b>Registration No</b></td>
		<td align="center" class="table1"><b>Capicity</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$classArray}
  <tr>
    <td align="left" class="table2"><a href='vehicalMaster.php?vehicleMasterId={$classArray[sec].vehicleMasterId}'>Edit</a></td>
    <td align="left" class="table2" NOWRAP >{$classArray[sec].vehicleType}</td>
    <td align="left" class="table2" NOWRAP >{$classArray[sec].vehicleNo}</td>
    <td align="left" class="table2">{$classArray[sec].registrationNo}</td>
    <td align="left" class="table2">{$classArray[sec].capicity}</td>
  </tr>
  {/section}
  </tbody>
</table>
{/block}
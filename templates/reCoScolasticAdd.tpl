{include file="./main.tpl"}
{block name="head"}
<link rel="stylesheet" href="./css/buttonStyle.css" type="text/css" />
<script type="text/javascript">
$(document).ready(function(){
	$(".omAttend").change(function()
  {
  	$('.newGoBtn').click();
  });
});

function gradeSet(thisObj)
{
	var row         = $(thisObj).parents('.gradeRow');
	var subjectName = $(row).find('.subjectName').val() != '' ? $(row).find('.subjectName').val() : 0;
	var grade       = $(row).find('.grade').val() != '' ? $(row).find('.grade').val() : 0;
	var dataString  = "subjectName=" + subjectName + "&grade=" + grade;
  $.ajax(
  {
    type: "GET",
    url: "setDescriptive.php",
    data: dataString,
    success:function(data)
    {
      row.find('.getDesc').html(data);
    }
  });
}

function buttonHide()
{
	$('#hideMe').hide();
}

</script>
<style>
.selectCombo
{
	width:500px;
}
</style>
{/block}
{block name="body"}
</br></br></br></br></br></br>
<center>
<form name="formGet" id="formGet" method="GET" action="reCoScolasticAdd.php">
<table align="center" border="1">
	<tr>
		<td class="table2 form01">
		  <select name="startYear" class="omAttend">
		    {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYearSelected}
		  </select>
		</td>
		{if $s_userType == 'Administrator'}
		<td class="table2 form01 omAttend">
		  <select name="class" autofocus="autofocus">
		    <option value="0">Select class</option>
		    {html_options values=$cArray.className output=$cArray.className selected=$class}
		  </select>
	  </td>
    <td class="table2 form01 omAttend">
		  <select name="section">
		    <option value="0">Select Section</option>
		    {html_options values=$secArrOut output=$secArrOut selected=$section}
		  </select>
	  </td>
	  {/if}
	  <td class="table2 form01 omAttend">
	  	<select name="subjectMasterId">
	      <option value="0">Select Subject</option>
	      {html_options values=$clubArr.subjectMasterId output=$clubArr.subjectName selected=$subjectMasterId}
		  </select>
		</td>
		<td>
	    <input type="submit" name="go" class="newGoBtn" value="go">
	  </td>
	</tr>
</table>
{if $subjectMasterId > 0}
<table align="center" border="0">
  </br>
  <tr>
    <td>
	    <input type="submit" name="submit" onclick="buttonHide();" id="hideMe" class="newSubmitBtn" value="Re Generate">
	  </td>
	</tr>
</table>
{/if}
</form>
</center>
<form name="form2" method="POST" action="reCoScolasticAdd.php">
<table align="center" border="1">
  </br></br></br>
	<h1 align="center">Student List</h1>
	</br>
	<h1 align="center">{$class}/{$section}</h1>
  </br>
	<tr>
		<td align="left" class="table1"><b>S.R.No.</b></td>
		<td align="left" class="table1"><b>G. R. No.</b></td>
		<td align="left" class="table1"><b>Roll No.</b></td>
		<td align="left" class="table1"><b>Name</b></td>
		<td align="left" class="table1"><b>M/F</b></td>
		<td align="left" class="table1"><b>Subject</b></td>
		<td align="left" class="table1"><b>Grade/Descriptive</b></td>
  </tr>
  {section name="sec" loop=$stdArray}
  <tr class="gradeRow">
    <td align="center" class="table2">{$smarty.section.sec.rownum}</td>
    <td align="left" class="table2">
    	{$stdArray[sec].grNo} <input type="hidden" name="examScholasticId[]" value="{$stdArray[sec].examScholasticId}">
    </td>
    <td align="left" class="table2">{$stdArray[sec].rollNo}</td>
    <td align="left" class="table2">{$stdArray[sec].studentName}</td>
    <td align="left" class="table2">{$stdArray[sec].gender}</td>
    <td align="left" class="table2">{$stdArray[sec].subjectName}<input type="hidden" value="{$stdArray[sec].subjectName}" class="subjectName"></td>
    <td align="left" class="table2">
	    <select name="grade[]" class="grade" onchange="gradeSet(this);">
	    	<option value="0">Select Grade</option>
	  	  {html_options values=$grade output=$grade selected=$stdArray[sec].grade}
	  	</select>
	  	<select name="descriptive[]" class="selectCombo getDesc">
	  		<option value="0">Select Descriptive</option>
	  		{html_options values=$dArray.descriptive output=$dArray.descriptive selected=$stdArray[sec].descriptive}
	  	</select>
	  </td>
  </tr>
  {/section}
  <tr>
    <td align="center" colspan="8" class="table2"><input type="submit" name="submitTaken" class="newSubmitBtn" value="Save"></td>
  </tr>
</table>
</form>
{/block}
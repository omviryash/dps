{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[-1, 25, 50, 100, 500, 1000], ['All', 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 500,
		"bJQueryUI":true
  });
});

function setBusStop(obj)
{
	var row = $(obj).parents('.stopRow');
  var routeArrival = row.find('.routeArrival').val();
  var busArrival = row.find('.busArrival').val();
	var dataString = "routeArrival=" + routeArrival + "&busArrival=" + busArrival;
  $.ajax(
  {
    type: "GET",
    url: "setBusStop.php",
    data: dataString,
    success:function(data)
    {
      row.find('.getBusStop').html(data);
    }
  });
}
</script>
{/block}
{block name="body"}
</br></br>
<form name="form1" method="POST" action="staffBusAllocation.php">
<table align="left" border="1" id="myDataTable" class="display">
	<div class="hd"><h2 align="center">Staff Bus Allocation</h2>
	<thead>
	<tr>
		<td align="left"><b>Sr No</b></td>
		<td align="left"><b>Employee Name</b></td>
		<td align="left"><b>Bus Arrival</b></td>
		<td align="left"><b>Bus Departure</b></td> 
		<td align="left"><b>Route Arrival</b></td> 
		<td align="left"><b>Route Departure</b></td> 
		<td align="left"><b>Bus Stop</b></td> 
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$nominalArr}
  <tr class="stopRow">
  	<td align="left">{$smarty.section.sec.rownum}</td>
    <td align="left">{$nominalArr[sec].name}</td>
    <td class="table2 form01">
      <input type="hidden" name="employeeMasterId[]" value="{$nominalArr[sec].employeeMasterId}">
      <select name="busArrival[]" class="busArrival">
      	<option value="">Bus Arrival</option>
        {html_options values=$typeArr.vehicleMasterId output=$typeArr.vehicleNo selected=$nominalArr[sec].busArrival}
      </select>
	  </td>
    <td class="table2 form01">
      <select name="busDeparture[]" >
      	<option value="">Bus Departure</option>
        {html_options values=$typeArr.vehicleMasterId output=$typeArr.vehicleNo selected=$nominalArr[sec].busDeparture}
      </select>
	  </td>
    <td class="table2 form01">
      <select name="routeArrival[]" class="routeArrival" onchange="setBusStop(this);" onblur="setBusStop(this);">
      	<option value="">Route Arrival</option>
        {html_options values=$rArr.routeMasterId output=$rArr.routeName selected=$nominalArr[sec].routeArrival}
      </select>
	  </td>
    <td class="table2 form01">
      <select name="routeDeparture[]" >
      	<option value="">Route Departure</option>
        {html_options values=$tArr.routeMasterId output=$tArr.routeName selected=$nominalArr[sec].routeDeparture}
      </select>
	  </td>
    <td class="table2 form01">
      <select name="busStopMasterId[]" class="getBusStop">
      	<option value="">Bus Stop</option>
        {html_options values=$sArr.busStopMasterId output=$sArr.busStop selected=$nominalArr[sec].busStopMasterId}
      </select>
	  </td>
  </tr>
  {/section}
  </tbody>
  <tfoot>
	<tr>
		<td align="left"></td>
		<td align="left"></td>
		<td align="left"></td>
		<td align="left"></td>
		<td align="left"></td>
		<td align="left"></td>
		<td align="left"><input type="submit" name="submit" class="newSubmitBtn" value="Save"></td>
  </tr>
  </tfoot>
</table>
</form>
{/block}
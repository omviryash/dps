{include file="./main.tpl"}
{block name="body"}
<form name="form1" method="POST" action="routeMaster.php">
<table align="center" border="0" class="table2">
<div class="hd"><h2 align="center">Route Entry</h2>
<input type="hidden" name="routeMasterId" value="{$routeMasterId}">
<tr>
  <td align="center" colspan="2" style="color:red; font-size:18px;"><b>{$confirm}</b></td>
</tr>
<tr>
  <td align="left" class="table2 form01">Route Name : </td>
  <td class="table2 form01"><input type="text" name="routeName" id="routeName" value="{$routeName}" REQUIRED AUTOFOCUS /></td>
</tr>
<tr>
  <td class="table2">Start Time</td>
  <td class="table2 form01">
  	{html_select_time prefix=startTime use_24_hours=true display_seconds=false time=$startTime}
	</td>
</tr>
<tr>
  <td class="table2">End Time</td>
  <td class="table2 form01">
  	{html_select_time prefix=endTime use_24_hours=true display_seconds=false time=$endTime}
	</td>
</tr>
<tr>
  <td class="table2">Arrival</td>
  <td class="table2 form01">
  	<select name="arrival" id="arrival" REQUIRED >
      {html_options values=$typeArr output=$typeArr selected=$arrival}
    </select>
	</td>
</tr>
<tr>
  <td class="table2"></td>
  <td class="table2 form01" align="center"><input type="submit" name="Submit" class="button" value="Save"></td>
</tr>
</table>
</form>
{/block}
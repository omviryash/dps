{include file="./main.tpl"}
{block name="head"}
<script type=text/javascript>
function checkGrno()
{
	var studentMasterId = $("#studentMasterId").val();
	var dataString = "grNo=" +$(".grNo").val() + "&studentMasterId=" + studentMasterId;
	$.ajax({
		type : 'GET',
		url  : 'checkGrno.php',
		data :  dataString,
		success:function(data)
		{
			if(data == 1)
			{
				alert("This Grno Allready Entered");
				$('.grNo').val('');
				$('.grNo').focus();
			}
	  }
	});
}
</script>
{/block}
{block name="body"}
<form action="studentEntry.php" enctype="multipart/form-data" method="POST">
<table align="center" border="1">
<input type="hidden" value="{$studentMasterId}" name="studentMasterId" id="studentMasterId" >
<br><br><br>
{if $studentMasterId > 0}
<div class="hd"><h2 align="center">Admission Edit</h2></div>
{else}
<div class="hd"><h2 align="center">New Admission Entry</h2></div>
{/if}
<br><br>
<tr>
  <td align="center" colspan="4" style="color:red; font-size:18px;"><b>{$confirm}</b></td>
</tr>
<tr>
	<td align="left" class="table1">Image P F M : </td>
	<td class="table2 form01">{$studentImage}<input name="imagePupil" type="file" id="files"></td>
	<td class="table2 form01">{$fatherImage}<input name="imageFather" type="file" id="files"></td>
	<td class="table2 form01">{$motherImage}<input name="imageMother" type="file" id="files"></td>
</tr>
{if $studentMasterId == 0}
<tr>
	<td align="left" class="table1">Academic Year : </td>
	<td align="center" class="table2 form01" colspan="3">
		<select name="startYear">
	    {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYearselected}
	  </select>
	</td>
</tr>
{/if}
<tr>
	<td align="left" class="table1">Name : </td>
	<td class="table2 form01"><input type="text" width='255' name="studentName" autofocus required value="{$studentName}" /></td>
</tr>
<tr>	
	<td align="left" class="table1">Father Name : </td>
	<td class="table2 form01"><input type="text" name="fatherName" value="{$fatherName}"></td>
</tr>
<tr>
	<td align="left" class="table1">G.R.No : </td>
	<td class="table2 form01"><input type="text" name="grNo" class="grNo" onblur="checkGrno();" value="{$grNo}" required /></td>
</tr>
<tr>
	<td align="left" class="table1">Date Of Birth : </td>
	<td class="table2 form01">
		{html_select_date prefix='dob' field_order="DmY" month_format="%m" start_year='-25' end_year='+25' time=$dateOfBirth day_value_format="%02d" }
	</td>
</tr>
<tr>
	<td class="table1">Joined In Class : </td>
	<td class="table2 form01">
  	<select name="joinedInClass" autofocus="autofocus" required >
	    <option value="">Select class</option>
	    {html_options values=$cArray.className output=$cArray.className selected=$joinedInClass}
	  </select>
  </td>
</tr>
<tr>
  <td align="left" class="table1">Joining Date : </td>
	<td class="table2 form01">
		{html_select_date prefix='joining' field_order="DmY" month_format="%m" start_year='-25' end_year='+25' time=$joiningDate day_value_format="%02d"}
	</td>
</tr>
{if $studentMasterId > 0}
<tr>
	<td align="left" class="table1">Student Id : </td>
	<td class="table2 form01"><input type="text" name="studentLoginId" value="{$studentLoginId}"></td>
</tr>
<tr>
	<td align="left" class="table1">Student Password : </td>
	<td class="table2 form01"><input type="text" name="studentPassword" value="{$studentPassword}"></td>
</tr>
<tr>
	<td align="left" class="table1">Parent Id : </td>
	<td class="table2 form01"><input type="text" name="parentLoginId" value="{$parentLoginId}"></td>
</tr>
<tr>
	<td align="left" class="table1">Parent Password : </td>
	<td class="table2 form01"><input type="text" name="parentPassword" value="{$parentPassword}"></td>
</tr>
{/if}
<tr>
	<td align="left" class="table1">Gender: </td>
	<td class="table2 form01">
		<select name="gender">
			{html_options values=$maleArrValue output=$maleArrValue selected=$gender}
		</select>
	</td>
</tr>
<tr>
	<td align="left" class="table1">Current Address : </td>
	<td class="table2 form01"><textarea cols="21" rows="10" name="currentAddress">{$currentAddress}</textarea></td>
</tr>
<tr>
	<td align="left" class="table1">Resi. Phone No 1 : </td>
	<td class="table2 form01"><input type="text" name="residencePhone1" value="{$residencePhone1}"></td>
</tr>
<tr>
	<td align="left" class="table1">Resi. Phone No 2 : </td>
	<td class="table2 form01"><input type="text" name="residencePhone2" value="{$residencePhone2}"></td>
</tr>
<tr>
	<td align="left" class="table1">SMS Mobile : </td>
	<td class="table2 form01"><input type="text" name="smsMobile" value="{$smsMobile}"></td>
</tr>
<tr>
	<td align="left" class="table1">Student Email: </td>
	<td class="table2 form01"><input type="text" name="studentEmail" value="{$studentEmail}"></td>
</tr>
<tr>	
	<td align="left" class="table1">Permanent Address : </td>
	<td class="table2 form01"><textarea cols="21" rows="10" name="permanentAddress">{$permanentAddress}</textarea></td>
</tr>
<tr>
	<td align="left" class="table1">Previous School : </td>
	<td class="table2 form01"><input type="text" name="previousSchool" value="{$previousSchool}"></td>
</tr>
<tr>	
	<td align="left" class="table1">Urban : </td>
	<td class="table2 form01"><input type="text" name="urban" value="{$urban}"></td>
</tr>
<tr>
	<td align="center" colspan="4" class="table1"><h1>Postal Details</h1></td>
</tr>
<tr>
	<td align="left" class="table1">Religion: </td>
	<td class="table2 form01"><input type="text" name="religion" value="{$religion}"></td>
</tr>
<tr>	
	<td align="left" class="table1">Blood Group : </td>
	<td class="table2 form01"><!--<input type="text" name="bloodGroup" value="{$bloodGroup}">-->
		<select name="bloodGroup">
			{html_options values=$bloodGroups output=$bloodGroups selected=$bloodGroup}
		</select>
	</td>
</tr>
<tr>
	<td align="left" class="table1">House Name: </td>
	<td class="table2 form01">
		<select name="houseId">
			<option value="0">Select House</option>
			  {html_options values=$houseArray.houseId output=$houseArray.houseName selected=$houseId}
	  </select>
	</td>
</tr>
<tr>
	<td align="left" class="table1">Current Address : </td>
	<td class="table2 form01"><textarea cols="21" rows="10" name="currentAddress">{$currentAddress}</textarea></td>
</tr>
<tr>
	<td align="left" class="table1">Current State : </td>
	<td class="table2 form01"><input type="text" name="currentState" value="{$currentState}"></td>
</tr>
<tr>
	<td align="left" class="table1">Current Address Pin : </td>
	<td class="table2 form01"><input type="text" name="currentAddressPin" value="{$currentAddressPin}"></td>
</tr>
<tr>
	<td align="left" class="table1">Permanent Address: </td>
	<td class="table2 form01"><textarea cols="19" rows="10" name="permanentAddress">{$permanentAddress}</textarea></td>
</tr>
<tr>
	<td align="left" class="table1">Permanent State : </td>
	<td class="table2 form01"><input type="text" name="permanentState" value="{$permanentState}"></td>
</tr>
<tr>
	<td align="left" class="table1">Permanent PIN: </td>
	<td class="table2 form01"><input type="text" name="permanentPIN" value="{$permanentPIN}"></td>
</tr>
<tr>
	<td align="left" class="table1">Created On: </td>
	<td class="table2 form01">
		{html_select_date prefix='createdon' field_order="DmY" month_format="%m" start_year='-5' end_year='+5' time=$createdOn day_value_format="%02d"}
	</td>
</tr>
<tr>
	<td colspan="4" align="center" class="table1"><h1>Parents Details</h1></td>
</tr>
<tr>
	<td align="left" class="table1">Father Mobile : </td>
	<td class="table2 form01"><input type="text" name="fatherMobile" value="{$fatherMobile}"></td>
</tr>
<tr>
	<td align="left" class="table1">Father Email: </td>
	<td class="table2 form01"><input type="text" name="fatherEmail" value="{$fatherEmail}"></td>
</tr>
<tr>
	<td align="left" class="table1">Father Occupation : </td>
	<td class="table2 form01"><input type="text" name="fatherOccupation" value="{$fatherOccupation}"></td>
</tr>
<tr>
	<td align="left" class="table1">Fathers Designation : </td>
	<td class="table2 form01"><input type="text" name="fathersDesignation" value="{$fathersDesignation}"></td>
</tr>

<tr>
	<td align="left" class="table1">Fathers Organization : </td>
	<td class="table2 form01"><input type="text" name="fathersOrganization" value="{$fathersOrganization}"></td>
</tr>
<tr>
	<td align="left" class="table1">Mothers Name : </td>
	<td class="table2 form01"><input type="text" name="mothersName" value="{$mothersName}"></td>
</tr>

<tr>
	<td align="left" class="table1">Mothers Phone : </td>
	<td class="table2 form01"><input type="text" name="mothersPhone" value="{$mothersPhone}"></td>
</tr>
<tr>
	<td align="left" class="table1">Mothers Emailid : </td>
	<td class="table2 form01"><input type="text" name="mothersEmailid" value="{$mothersEmailid}"></td>
</tr>
<tr>
	<td align="left" class="table1">Mothers Mobile : </td>
	<td class="table2 form01"><input type="text" name="mothersMobile" value="{$mothersMobile}"></td>
</tr>
<tr>	
	<td align="left" class="table1">Mother Occupation : </td>
	<td class="table2 form01"><input type="text" name="motherOccupation" value="{$motherOccupation}"></td>
</tr>
<tr>
	<td align="left" class="table1">Mothers Designation : </td>
	<td class="table2 form01"><input type="text" name="mothersDesignation" value="{$mothersDesignation}"></td>
</tr>
<tr>
	<td align="left" class="table1">Mothers Organization : </td>
	<td class="table2 form01"><input type="text" name="mothersOrganization" value="{$mothersOrganization}"></td>
</tr>
<tr>
	<td align="left" class="table1">Father Phone : </td>
	<td class="table2 form01"><input type="text" name="fatherPhone" value="{$fatherPhone}"></td>
</tr>
<tr>
	<td align="left" class="table1">Aadhaar : </td>
	<td class="table2 form01"><input type="text" name="aadhaar" value="{$aadhaar}"></td>
</tr>
<tr>
	<td align="left" class="table1">Activated</td>
	<td align="left" class="table2 form01">
		<select name="activated">
			{html_options values=$secArrValue output=$secArrOut selected=$activated}
	  </select>
	</td>
</tr>
<tr>
	<td colspan="4" align="center"><input type="submit" name="submit" class="newSubmitBtn" value="Save">
</tr>
</table>
</form>
{/block}

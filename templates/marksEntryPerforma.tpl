{include file="./main.tpl"}
{block name="head"}
<link rel="stylesheet" href="./css/buttonStyle.css" type="text/css" />
<script type="text/javascript">
$(document).ready(function(){
	$(".omAttend").change(function()
  {
  	$('.newGoBtn').click();
  });
});


function buttonHide()
{
	$('#hideMe').hide();
}

</script>
{/block}
{block name="body"}
</br></br></br></br></br></br>
<center>
<form name="formGet" id="formGet" method="GET" action="marksEntryPerforma.php">
<table align="center" border="1">
	<tr>
		<td class="table2 form01">
		  <select name="startYear" class="omAttend">
		    {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYearSelected}
		  </select>
		</td>
		{if $s_userType == 'Administrator'}
		<td class="table2 form01 omAttend">
		  <select name="class" autofocus="autofocus">
		    <option value="0">Select class</option>
		    {html_options values=$cArray.className output=$cArray.className selected=$class}
		  </select>
	  </td>
    <td class="table2 form01 omAttend">
		  <select name="section">
		    <option value="0">Select Section</option>
		    {html_options values=$secArrOut output=$secArrOut selected=$section}
		  </select>
	  </td>
	  <td class="table2 form01">
	  	<select name="subjectMasterId" class="omAttend">
	      <option value="0">Select Subject</option>
	      {html_options values=$clubArr.subjectMasterId output=$clubArr.subjectName selected=$subjectMasterId}
		  </select>
		</td>
	  {/if}
	  {if $s_userType != 'Administrator'}
		<td class="table2 form01">
		  <select name="subjectAltId" class="omAttend">
			  <option value="">Select Class/Subject</option>
        {html_options values=$scdArray.subjectAltId output=$scdArray.subjectDtl selected=$subjectAltId}
      </select>
    </td>
    {/if}
		<td>
	    <input type="submit" name="go" class="newGoBtn" value="go">
	  </td>
	</tr>
</table>
</form>
</center>
<form name="form2" method="POST" action="marksEntryPerforma.php">
<table align="center" border="1">
  </br></br></br>
	<h1 align="center">Student List</h1>
	</br>
	<h1 align="center">{$class}/{$section}</h1>
  </br>
	<tr>
		<td align="left" class="table1"><b>S.R.No.</b></td>
		<td align="left" class="table1"><b>G. R. No.</b></td>
		<td align="left" class="table1"><b>Roll No.</b></td>
		<td align="left" class="table1"><b>Name</b></td>
		<td align="left" class="table1"><b>M/F</b></td>
		<td align="left" class="table1"><b>Subject</b></td>
		<td align="left" class="table1"><b>FA1</b></td>
		<td align="left" class="table1"><b>FA2</b></td>
		<td align="left" class="table1"><b>SA1</b></td>
		<td align="left" class="table1"><b>FA3</b></td>
		<td align="left" class="table1"><b>FA4</b></td>
		<td align="left" class="table1"><b>SA2</b></td>
  </tr>
  <input type="hidden" name="subjectAltId" value="{$subjectAltId}">
  {section name="sec" loop=$stdArray}
  <tr class="trRow">
    <td align="center" class="table2">{$smarty.section.sec.rownum}</td>
    <td align="left" class="table2">
    	{$stdArray[sec].grNo}
    	<input type="hidden" name="grNo[]" value="{$stdArray[sec].grNo}">
    	<input type="hidden" name="class[]" value="{$stdArray[sec].class}">
    	<input type="hidden" name="section[]" value="{$stdArray[sec].section}">
    	<input type="hidden" name="subjectMasterId[]" value="{$stdArray[sec].subjectMasterId}">
    </td>
    <td align="left" class="table2">{$stdArray[sec].rollNo}</td>
    <td align="left" class="table2">{$stdArray[sec].studentName}</td>
    <td align="left" class="table2">{$stdArray[sec].gender}</td>
    <td align="left" class="table2">{$stdArray[sec].subjectName}</td>
    <td align="center" class="table2">
    	<input type="text" name="fa1[]" value="{$stdArray[sec].fa1}">
    </td>
    <td align="center" class="table2">
    	<input type="text" name="fa2[]" value="{$stdArray[sec].fa2}">
    </td>
    <td align="center" class="table2">
    	<input type="text" name="sa1[]" value="{$stdArray[sec].sa1}">
    </td>
    <td align="center" class="table2">
    	<input type="text" name="fa3[]" value="{$stdArray[sec].fa3}">
    </td>
    <td align="center" class="table2">
    	<input type="text" name="fa4[]" value="{$stdArray[sec].fa4}">
    </td>
    <td align="center" class="table2">
    	<input type="text" name="sa2[]" value="{$stdArray[sec].sa2}">
    </td>
  </tr>
  {/section}
  </tr>
  {if $count != 0}
  <tr>
    <td align="center" colspan="7" class="table2"><input type="submit" name="submitTaken" class="newSubmitBtn" value="Save"></td>
  </tr>
  {/if}
</table>
</form>
{/block}
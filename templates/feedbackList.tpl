{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
		"bAutoWidth": false,
    "aLengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 10,
  	"aaSorting": [[ 3, "desc" ]],
		"bJQueryUI":true
  });
});
</script>
{/block}
{block name="body"}
<div class="hd"><h2 align="center">Message List</h2></div>
<table align="center" border="2" id="myDataTable" class="display">
	<thead>
	<tr>
		<td align="center" class="table1"><b>From</b></td>
		<td align="center" class="table1"><b>To</b></td>
		<td align="center" class="table1"><b>User Type</b></td>
		<td align="center" class="table1"><b>Message</b></td>
		<td align="center" class="table1"><b>Date & Time</b></td>
		<td align="center" class="table1"><b>Reply</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$classArray}
  <tr>
    <td align="left" class="table2" NOWRAP >{$classArray[sec].fromName}</td>
    <td align="left" class="table2" NOWRAP >{$classArray[sec].name}</td>
    <td align="left" class="table2">{$classArray[sec].userType}</td>
    <td align="left" class="table2">{$classArray[sec].message}</td>
    <td align="left" class="table2" NOWRAP >{$classArray[sec].dateTime}</td>
    <td align="left" class="table2"><a href="feedBackReply.php?feedbackId={$classArray[sec].feedbackId}">Reply</a></td>
  </tr>
  {/section}
  </tbody>
</table>
{/block}
{include file="./main.tpl"}
{block name="head"}
<link rel="stylesheet" href="./css/buttonStyle.css" type="text/css" />
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[-1, 100, 500, 1000], ['All', 100, 500, 1000]],
  	"iDisplayLength": 100,
		"bJQueryUI":true
  });
	$(".submitClick").change(function()
  {
  	$('.newGoBtn').click();
  });
  var examScheduleId  = $('.examScheduleId').val();
  var selectedSection  = $('.selectedSection').val();
	var datastring = 'examScheduleId=' + examScheduleId + '&selectedSection=' + selectedSection;
	$.ajax({	
		type: 'GET',
		url: 'selectSubClass.php',
	  data: datastring,
	  success:function(data)
	  {
	  	$('.classSection').html(data);
    }
  });
	  
  $(".examScheduleId").change(function()
  {
    var examScheduleId  = $('.examScheduleId').val();
    var selectedSection  = $('.selectedSection').val();
		var datastring = 'examScheduleId=' + examScheduleId + '&selectedSection=' + selectedSection;
		$.ajax({	
			type: 'GET',
			url: 'selectSubClass.php',
		  data: datastring,
		  success:function(data)
		  {
		  	$('.classSection').html(data);
	    }
	  });
  });
});


function buttonHide()
{
	$('#hideMe').hide();
}

function confirmDelete()
{
	if(confirm('Are You Sure 1 st time?'))
	{
		if(confirm('Are You Sure 2nd time?'))
		  return true;
		else
		  return false;
	}
	else
	  return false;
}
</script>
{/block}
{block name="body"}
</br></br>
<center>
<form name="formGet" id="formGet" method="GET" action="marksEntryReport.php">
<table align="center" border="1">
	<div class="hd"><h2 align="center">Marks Entry Status Report</h2></div>
	<tr>
		<td class="table2 form01">
		  <select name="startYear" class="submitClick">
		    {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYearSelected}
		  </select>
		</td>
		<td>
	    <input type="submit" name="go" class="newGoBtn" value="go">
	  </td>
	</tr>
</table>
</form>
</center>
<form name="form2" method="POST" action="marksEntryReport.php">
<table align="left" border="1" id="myDataTable" class="display">
	<input type="hidden" name="startYear" value="{$academicStartYearSelected}">
  <thead>
		<tr>
			<td align="center" class="table1"><b>S.R.No.</b></td>
			<td align="center" class="table1"><b>Schedule Date</b></td>
			<td align="center" class="table1"><b>Exam Type</b></td>
			<td align="center" class="table1"><b>Class</b></td>
			<td align="center" class="table1"><b>Section</b></td>
			<td align="center" class="table1"><b>Subject Name</b></td>
			<td align="center" class="table1"><b>Name</b></td>
	  </tr>
  </thead>
  <tbody>
	  {section name="sec" loop=$scdArray}
	  <tr class="trRow">
	    <td align="center" class="table2">{$smarty.section.sec.rownum}</td>
	    <td align="center" class="table2">{$scdArray[sec].scheduleDate}</td>
	    <td align="center" class="table2">{$scdArray[sec].examType}</td>
	    <td align="center" class="table2">{$scdArray[sec].class}</td>
	    <td align="center" class="table2">{$scdArray[sec].section}</td>
	    <td align="center" class="table2">{$scdArray[sec].subjectName}</td>
	    {if $scdArray[sec].status == 'Pending'}
	    <td align="center" class="table2" style="color:red;">{$scdArray[sec].status}</td>
	    {else}
	    <td align="center" class="table2" style="color:green;">{$scdArray[sec].status}</td>
	    {/if}
	  </tr>
	  {/section}
	</tbody>
</table>
</form>
{/block}
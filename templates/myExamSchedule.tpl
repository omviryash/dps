{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 10,
		"bJQueryUI":true
  });
});
</script>
{/block}
{block name="body"}
</br></br>
<form name="formGet" method="GET" action="myExamSchedule.php">
<table align="center">
	<tr>
		<td class="table2 form01">
		  <select name="startYear">
		    {html_options values=$dateArrVal output=$dateArrOut selected=$studStartYear}
		  </select>
		</td>
    <td>
      <input type="submit" name="submit" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
</form>
<table align="center" border="1">
	<div class="hd"><h2 align="center">My Exam Schedule</h2></div>
	<thead>
	<tr>
		<td align="left" class="table1"><b>Class</b></td>
		<td align="left" class="table1"><b>Date</b></td>
	  <td align="left" class="table1"><b>Subject</b></td>
	  <td align="left" class="table1"><b>Exam Type</b></td>
	  <td align="left" class="table1"><b>Max Marks</b></td>
	  <td align="left" class="table1"><b>Min Marks</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$scdArr}
  <tr>
  	<td align="left" class="table2">{$scdArr[sec].class}</td>
    <td align="left" class="table2">{$scdArr[sec].scheduleDate}</td>
    <td align="left" class="table2">{$scdArr[sec].subjectName}</td>
    <td align="left" class="table2">{$scdArr[sec].examType}</td>
    <td align="left" class="table2">{$scdArr[sec].maxMarks}</td>
    <td align="left" class="table2">{$scdArr[sec].minMarks}</td>
  </tr>
 {/section}
 </tbody>
</table>
{/block}
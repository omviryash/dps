{include file="./main.tpl"}
{block name="body"}
<form name="form1" method="POST" action="subjectEntry.php">
<table align="center" border="0" class="table2">
<div class="hd"><h2 align="center">Subject Entry</h2>
<input type="hidden" name="subjectMasterId" value="{$subjectMasterId}">
<tr>
  <td align="center" colspan="2" style="color:red; font-size:18px;"><b>{$confirm}</b></td>
</tr>
<tr>
  <td class="table2">Subject Name</td>
  <td class="table2 form01">
  	<input type="text" name="subjectName" value="{$subjectName}" AUTOFOCUS=AUTOFOCUS REQUIRED >
  </td>
</tr>
<tr>
  <td class="table2">Subject Code</td>
  <td class="table2 form01">
  	<input type="text" name="subjectCode" value="{$subjectCode}" >
  </td>
</tr>
<tr>
  <td class="table2">Start Class</td>
  <td class="table2 form01">
  	<select name="startClass">
	    <option value="">Select class</option>
	    {html_options values=$cArray.className output=$cArray.className selected=$startClass}
	  </select>
  </td>
</tr>
<tr>
  <td class="table2">End Class</td>
  <td class="table2 form01">
  	<select name="endClass">
	    <option value="">Select class</option>
	    {html_options values=$cArray.className output=$cArray.className selected=$endClass}
	  </select>
  </td>
</tr>
<tr>
  <td class="table2">Co - Scholastic Type</td>
  <td class="table2 form01">
  	<select name="subjectDescription">
	    <option value="">Select Co-Scholastic Type</option>
	    {html_options values=$secArrOut1 output=$secArrOut1 selected=$subjectDescription}
	  </select>
  </td>
</tr>
<tr>
  <td class="table2">Type</td>
  <td class="table2 form01">
  	<select name="subjectType">
      <option value="0">Select Type</option>
      {html_options values=$secArrOut output=$secArrOut selected=$subjectType}
	  </select>
	</td>
</tr>
<tr>
  <td class="table2"></td>
  <td class="table2 form01" align="center"><input type="submit" name="Submit" class="button" value="Save"></td>
</tr>
</table>
</form>
{/block}
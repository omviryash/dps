{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
		"bAutoWidth": false,
    "aLengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 100,
		"bJQueryUI":true
  });
});
</script>
{/block}
{block name="body"}
<form name="formGet" method="GET" action="subjectTeacherAllotmentList.php">
<table align="center">
	<tr>
		<td class="table2 form01">
		  <select name="startYear">
		    {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYear}
		  </select>
		</td>
    <td>
      <input type="submit" name="submit" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
</form>
<div class="hd"><h2 align="center">Subject Teacher Allotment List</h2></div>
<table align="center" border="2" id="myDataTable" class="display">
	<thead>
	<tr>
		<td align="center" class="table1"><b>&nbsp;</b></td>
		<td align="center" class="table1"><b>Academic Year</b></td>
		<td align="center" class="table1"><b>Class Name</b></td>
		<td align="center" class="table1"><b>Section</b></td>
		<td align="center" class="table1"><b>Teacher Name</b></td>
		<td align="center" class="table1"><b>Subject Teacher</b></td>
		<td align="center" class="table1"><b>Weekly Periods</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$classArray}
  <tr>
    <td align="left" class="table2"><a href="subjectTeacherAllotment.php?subjectTeacherAllotmentId={$classArray[sec].subjectTeacherAllotmentId}">Edit</a></td>
    <td align="left" class="table2">{$classArray[sec].academicStartYear} - {$classArray[sec].academicEndYear}</td>
    <td align="left" class="table2">{$classArray[sec].class}</td>
    <td align="left" class="table2">{$classArray[sec].section}</td>
    <td align="left" class="table2">{$classArray[sec].name}</td>
    <td align="left" class="table2">{$classArray[sec].subjectName}</td>
    <td align="left" class="table2">{$classArray[sec].period}</td>
  </tr>
  {/section}
  </tbody>
</table>
{/block}
{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"bAutoWidth": true,
  	"aLengthMenu": [[-1, 10, 20, 30, 40, 50], ["All", 10, 20, 30, 40, 50]],
  	"iDisplayLength": 500,
		"bJQueryUI":true
  });
});
</script>
{/block}
{block name="body"}
</br></br>
<form name="formGet" method="GET" action="myEmpAttend.php">
<table align="center">
	<tr>
		<td class="table2 form01">
	    {html_select_date prefix="attendence" start_year="-25" end_year="+25" field_order="DMY" time=$attendenceDate day_value_format="%02d" display_days=false}
	  </td>
    <td>
      <input type="submit" name="submit" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
</form>
<table align="center" border="1">
	<div class="hd"><h2 align="center">My Attendance Report (Monthly)</h2></div>
	<thead>
	<tr>
		<td align="left" class="table1"><b>Date</b></td>
		<td align="left" class="table1"><b>Present/Absent</b></td>
		<td align="left" class="table1"><b>Start Time</b></td>
		<td align="left" class="table1"><b>End Time</b></td>
		<td align="left" class="table1"><b>Late</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$nominalArr}
  <tr>
    <td align="left" class="table2">{$nominalArr[sec].date}</td>
    <td align="left" class="table2">{$nominalArr[sec].attendences}</td>
    <td align="left" class="table2">{$nominalArr[sec].startTime}</td>
    <td align="left" class="table2">{$nominalArr[sec].endTime}</td>
    <td align="left" class="table2">{$nominalArr[sec].late}</td>
  </tr>
  {/section}
  </tbody>
</table>
{/block}
{include file="./main.tpl"}
{block name="head"}
<link rel="stylesheet" href="./css/buttonStyle.css" type="text/css" />
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
    width: 35px;
	}
        .error{
            font-size: 11px;
            color: #FF0000;
        }
        .inputError{
            border: 1px solid #FF0000;
        }
</style>
<script type="text/javascript">
$(document).ready(function(){
	$('#myDataTable').dataTable({
	"aLengthMenu": [[100, 500, 1000], [100, 500, 1000]],
  	"iDisplayLength": 100,
  	"bAutoWidth": false,
  	"aaSorting": [[ 0, "asc" ]],
		"bJQueryUI":true
  });
  
	$(".omAttend").change(function()
  {
  	$('.newGoBtn').click();
  });
});


function buttonHide()
{
	$('#hideMe').hide();
}

function validateForm(){

    var error_count = 0;

    $('.valid0_10, .valid0_30').find('span.error').remove();
    $('.valid0_10, .valid0_30').find('input').removeClass('inputError');

    $('.valid0_10').each(function(){

        if($(this).find('input').val().trim() == ''){
            $(this).find('input').addClass('inputError');
            $(this).append('<span class="error"><br />Value between 0 to 10</span>');
            
            error_count++;
        } else {
            if($(this).find('input').val() < 0 || $(this).find('input').val() > 10){
                $(this).find('input').addClass('inputError');
                $(this).append('<span class="error"><br />Value between 0 to 10</span>');

                error_count++;
            }
        }
    });
    
    $('.valid0_30').each(function(){

        if($(this).find('input').val().trim() == ''){
            $(this).find('input').addClass('inputError');
            $(this).append('<span class="error"><br />Value between 0 to 30</span>');
            
            error_count++;
        } else {
            if($(this).find('input').val() < 0 || $(this).find('input').val() > 30){
                $(this).find('input').addClass('inputError');
                $(this).append('<span class="error"><br />Value between 0 to 30</span>');

                error_count++;
            }
        }
    });
    
    if(error_count > 0)
        return false;
    else
        return true;
}

</script>
{/block}
{block name="body"}
</br></br>
<center>
<form name="formGet" id="formGet" method="GET" action="marksEntryPerformaTry.php">
<table align="center" border="1">
	<div class="hd"><h2 align="center">Part 1 Scholastic Entry</h2></div>
	<tr>
		<td class="table2 form01">
		  <select name="startYear" class="">
		    {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYearSelected}
		  </select>
		</td>
		{if $s_userType == 'Administrator'}
		<td class="table2 form01">
		  <select name="class" autofocus="autofocus">
		    <option value="0">Select class</option>
		    {html_options values=$cArray.className output=$cArray.className selected=$class}
		  </select>
	  </td>
    <td class="table2 form01">
		  <select name="section">
		    <option value="0">Select Section</option>
		    {html_options values=$secArrOut output=$secArrOut selected=$section}
		  </select>
	  </td>
	  <td class="table2 form01">
	  	<select name="subjectMasterId" class="">
	      <option value="0">Select Subject</option>
	      {html_options values=$clubArr.subjectMasterId output=$clubArr.subjectName selected=$subjectMasterId}
		  </select>
		</td>
	  {/if}
	  {if $s_userType != 'Administrator'}
		<td class="table2 form01">
		  <select name="subjectAltId" class="omAttend">
			  <option value="">Select Class/Subject</option>
        {html_options values=$scdArray.subjectAltId output=$scdArray.subjectDtl selected=$subjectAltId}
      </select>
    </td>
    {/if}
		<td>
	    <input type="submit" name="go" class="newGoBtn" value="go">
	  </td>
	</tr>
</table>
</form>
</center>
<form name="form2" method="POST" action="marksEntryPerformaTry.php" onsubmit="return validateForm();">
<table align="left" border="1" id="myDataTable" class="display">
  </br></br></br>
	<h1 align="center">Student List</h1>
	</br>
	<h1 align="center">{$class}/{$section}</h1>
  </br>
  <thead>
  <tr>
    {if $subjectMasterId == 168 || $subjectMasterId == 55}
    <th colspan="4">-</th>
    {else}
    <th colspan="3">-</th>
    {/if}
    <th colspan="3">Term - 1</th>
    <th colspan="3">Term - 2</th>
  </tr>
	<tr>
		<td align="left" class="table1"><b>Roll. No.</b></td>
		<td align="left" class="table1"><b>G. R. No.</b></td>
		
		<td align="left" class="table1"><b>Name</b></td>
                {if $subjectMasterId == 168 || $subjectMasterId == 55}
                <td align="left" class="table1"><b>Subject</b></td>
                {/if}
		
		<td align="left" class="table1"><b>FA1 (10%)</b></td>
		<td align="left" class="table1"><b>FA2 (10%)</b></td>
		<td align="left" class="table1"><b>SA1 (30%)</b></td>
		<td align="left" class="table1"><b>FA3 (10%)</b></td>
		<td align="left" class="table1"><b>FA4 (10%)</b></td>
		<td align="left" class="table1"><b>SA2 (30%)</b></td>
  </tr>
  <input type="hidden" name="subjectAltId" value="{$subjectAltId}">
  <input type="hidden" name="startYear" value="{$academicStartYearSelected}">
  </thead>
  <tbody>
  {section name="sec" loop=$stdArray}
  <tr class="trRow">
    <td align="left" class="table2">{$stdArray[sec].rollNo}</td>
    <td align="left" class="table2">
    	{$stdArray[sec].grNo}
    	<input type="hidden" name="examMarksPerTryId[]" value="{$stdArray[sec].examMarksPerTryId}">
    	<input type="hidden" name="subjectMasterId" value="{$subjectMasterId}">
    </td>
    <td align="left" class="table2">{$stdArray[sec].studentName}</td>
    
    {if $subjectMasterId == 168 || $subjectMasterId == 55}
    <td align="left" class="table2">{$stdArray[sec].subjectName}</td>
    {/if}
    
    {if $subjectMasterId == 2 || $subjectMasterId ==165}
        <td align="center" class="table2 valid0_10"><input type="text" name="sub1Fa1[]" value="{$stdArray[sec].sub1Fa1}"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub1Fa2[]" value="{$stdArray[sec].sub1Fa2}"></td>
  	<td align="center" class="table2 valid0_30"><input type="text" name="sub1Sa1[]" value="{$stdArray[sec].sub1Sa1}"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub1Fa3[]" value="{$stdArray[sec].sub1Fa3}"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub1Fa4[]" value="{$stdArray[sec].sub1Fa4}"></td>
  	<td align="center" class="table2 valid0_30"><input type="text" name="sub1Sa2[]" value="{$stdArray[sec].sub1Sa2}"></td>
    {/if}
    {if $subjectMasterId == 3|| $subjectMasterId ==164}
        <td align="center" class="table2 valid0_10"><input type="text" name="sub2Fa1[]" value="{$stdArray[sec].sub2Fa1}"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub2Fa2[]" value="{$stdArray[sec].sub2Fa2}"></td>
  	<td align="center" class="table2 valid0_30"><input type="text" name="sub2Sa1[]" value="{$stdArray[sec].sub2Sa1}"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub2Fa3[]" value="{$stdArray[sec].sub2Fa3}"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub2Fa4[]" value="{$stdArray[sec].sub2Fa4}"></td>
  	<td align="center" class="table2 valid0_30"><input type="text" name="sub2Sa2[]" value="{$stdArray[sec].sub2Sa2}"></td>
    {/if}
    {if $subjectMasterId == 1 || $subjectMasterId ==166}
        <td align="center" class="table2 valid0_10"><input type="text" name="sub3Fa1[]" value="{$stdArray[sec].sub3Fa1}"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub3Fa2[]" value="{$stdArray[sec].sub3Fa2}"></td>
  	<td align="center" class="table2 valid0_30"><input type="text" name="sub3Sa1[]" value="{$stdArray[sec].sub3Sa1}"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub3Fa3[]" value="{$stdArray[sec].sub3Fa3}"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub3Fa4[]" value="{$stdArray[sec].sub3Fa4}"></td>
  	<td align="center" class="table2 valid0_30"><input type="text" name="sub3Sa2[]" value="{$stdArray[sec].sub3Sa2}"></td>
    {/if}
    {if $subjectMasterId == 192}
        <td align="center" class="table2 valid0_10"><input type="text" name="sub3Fa1[]" value="{$stdArray[sec].sub3Fa1}"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub3Fa2[]" value="{$stdArray[sec].sub3Fa2}"></td>
  	<td align="center" class="table2 valid0_30"><input type="text" name="sub3Sa1[]" value="{$stdArray[sec].sub3Sa1}"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub3Fa3[]" value="{$stdArray[sec].sub3Fa3}"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub3Fa4[]" value="{$stdArray[sec].sub3Fa4}"></td>
  	<td align="center" class="table2 valid0_30"><input type="text" name="sub3Sa2[]" value="{$stdArray[sec].sub3Sa2}"></td>
    {/if}
    {if $subjectMasterId == 85 || $subjectMasterId == 66}
        <td align="center" class="table2 valid0_10"><input type="text" name="sub4Fa1[]" value="{$stdArray[sec].sub4Fa1}"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub4Fa2[]" value="{$stdArray[sec].sub4Fa2}"></td>
  	<td align="center" class="table2 valid0_30"><input type="text" name="sub4Sa1[]" value="{$stdArray[sec].sub4Sa1}"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub4Fa3[]" value="{$stdArray[sec].sub4Fa3}"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub4Fa4[]" value="{$stdArray[sec].sub4Fa4}"></td>
  	<td align="center" class="table2 valid0_30"><input type="text" name="sub4Sa2[]" value="{$stdArray[sec].sub4Sa2}"></td>
    {/if}
    {if $subjectMasterId == 167 || $subjectMasterId == 4}
        <td align="center" class="table2 valid0_10"><input type="text" name="sub5Fa1[]" value="{$stdArray[sec].sub5Fa1}"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub5Fa2[]" value="{$stdArray[sec].sub5Fa2}"></td>
  	<td align="center" class="table2 valid0_30"><input type="text" name="sub5Sa1[]" value="{$stdArray[sec].sub5Sa1}"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub5Fa3[]" value="{$stdArray[sec].sub5Fa3}"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub5Fa4[]" value="{$stdArray[sec].sub5Fa4}"></td>
  	<td align="center" class="table2 valid0_30"><input type="text" name="sub5Sa2[]" value="{$stdArray[sec].sub5Sa2}"></td>
    {/if}
    {if $subjectMasterId == 168 || $subjectMasterId == 55}
        <td align="center" class="table2 valid0_10"><input type="text" name="sub6Fa1[]" value="{$stdArray[sec].sub6Fa1}"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub6Fa2[]" value="{$stdArray[sec].sub6Fa2}"></td>
  	<td align="center" class="table2 valid0_30"><input type="text" name="sub6Sa1[]" value="{$stdArray[sec].sub6Sa1}"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub6Fa3[]" value="{$stdArray[sec].sub6Fa3}"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub6Fa4[]" value="{$stdArray[sec].sub6Fa4}"></td>
  	<td align="center" class="table2 valid0_30"><input type="text" name="sub6Sa2[]" value="{$stdArray[sec].sub6Sa2}"></td>
    {/if}
  </tr>
  {/section}
  </tbody>
  <tfoot>
  {if $count != 0}
  <tr>
    {if $subjectMasterId == 168 || $subjectMasterId == 55}
    <td align="center" colspan="10" class="table2"><input type="submit" name="submitTaken" class="newSubmitBtn" value="Save"></td>
    {else}
    <td align="center" colspan="9" class="table2"><input type="submit" name="submitTaken" class="newSubmitBtn" value="Save"></td>
    {/if}
  </tr>
  {/if}
  </tfoot>
</table>
</form>
{/block}
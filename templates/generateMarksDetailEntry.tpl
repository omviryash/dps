{include file="./main.tpl"}
{block name="head"}
<link rel="stylesheet" href="./css/buttonStyle.css" type="text/css" />
<script type="text/javascript">
$(document).ready(function(){
	$(".omAttend").change(function()
  {
  	$('.newGoBtn').click();
  });
});


function buttonHide()
{
	$('#hideMe').hide();
}

</script>
<style>
.selectCombo
{
	width:500px;
}
</style>
{/block}
{block name="body"}
</br></br></br></br></br></br>
<center>
<form name="formGet" id="formGet" method="GET" action="generateMarksDetailEntry.php">
<table align="center" border="1">
	<tr>
		<td class="table2 form01">
		  <select name="startYear" class="omAttend">
		    {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYearSelected}
		  </select>
		</td>
		{if $s_userType == 'Administrator'}
		<td class="table2 form01 omAttend">
		  <select name="class" autofocus="autofocus">
		    <option value="0">Select class</option>
		    {html_options values=$cArray.className output=$cArray.className selected=$class}
		  </select>
	  </td>
    <td class="table2 form01 omAttend">
		  <select name="section">
		    <option value="0">Select Section</option>
		    {html_options values=$secArrOut output=$secArrOut selected=$section}
		  </select>
	  </td>
	  {/if}
		<td>
	    <input type="submit" name="go" class="newGoBtn" value="go">
	  </td>
	</tr>
</table>
</form>
</center>
{if $count != 0}
<form name="form2" method="POST" action="generateMarksDetailEntry.php">
<table align="center" border="1">
  </br></br></br>
	<h1 align="center">Student List</h1>
	</br>
	<h1 align="center">{$class}/{$section}</h1>
  </br>
	<tr>
		<td align="left" class="table1"><b>G. R. No.</b></td>
    <td align="left" class="table1"><b>Roll No.</b></td>
		<td align="left" class="table1"><b>Name</b></td>
    <td align="left" class="table1"><b>My Goals</b></td>
    <td align="left" class="table1"><b>Strengths</b></td>
    <td align="left" class="table1"><b>Interest And Hobbies</b></td>
    <td align="left" class="table1"><b>Reponsibilities</b></td>
    <td align="left" class="table1"><b>Height</b></td>
    <td align="left" class="table1"><b>Weight</b></td>
    <td align="left" class="table1"><b>Dental Hygiene</b></td>
    <td align="left" class="table1"><b>Report Card View Term 1</b></td>
    <td align="left" class="table1"><b>Report Card View Term 2</b></td>
  </tr>
  {section name="sec" loop=$dtlArr}
  <tr class="gradeRow">
		<td align="left" class="table2">
    	{$dtlArr[sec].grNo}
    	<input type="hidden" name="nominalRollId[]" value="{$dtlArr[sec].nominalRollId}">
    </td>
    <td align="left" class="table2">{$dtlArr[sec].rollNo}</td>
    <td align="left" class="table2">{$dtlArr[sec].studentName}</td>
    <td align="left" class="table2"><textarea name="goals[]" cols="15" >{$dtlArr[sec].goals}</textarea></td>
    <td align="left" class="table2"><textarea name="strength[]" cols="15">{$dtlArr[sec].strength}</textarea></td>
    <td align="left" class="table2"><textarea name="intHobbies[]" cols="15">{$dtlArr[sec].intHobbies}</textarea></td>
    <td align="left" class="table2"><textarea name="responsibilities[]" cols="15">{$dtlArr[sec].responsibilities}</textarea></td>
    <td align="left" class="table2"><textarea name="height[]" cols="10">{$dtlArr[sec].height}</textarea></td>
    <td align="left" class="table2"><textarea name="weight[]" cols="10">{$dtlArr[sec].weight}</textarea></td>
    <td align="left" class="table2"><textarea name="dentalHygience[]" cols="15">{$dtlArr[sec].dentalHygience}</textarea></td>
    <td align="left" class="table2"><select name="reportCardView1[]">{html_options values=$viewArrVal output=$viewArrOut selected=$dtlArr[sec].reportCardView1}</select></td>
    <td align="left" class="table2"><select name="reportCardView2[]">{html_options values=$viewArrVal output=$viewArrOut selected=$dtlArr[sec].reportCardView2}</select></td>
  </tr>
  {/section}
  <tr>
    <td align="center" colspan="9" class="table2"><input type="submit" name="submitTaken" class="newSubmitBtn" value="Save"></td>
  </tr>
</table>
</form>
{/if}
{/block}
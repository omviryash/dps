<form action="getEng.php" method="POST">
<input type="hidden" name="subjectName" value="{$subjectName}">
<input type="hidden" name="termValue" value="{$termValue}">
<table border=2 align="center">
<tr>
 <td align="center">Student Name</td>
 <td align="center">Concept</td>
 	<td align="center">Mental Ability</td>
 	<td nowrap align="center">Activity / Project</td>
</tr>
{section name="sec" loop=$nameArray}
<tr>
	<td nowrap>{$nameArray[sec].studentName}</td>
	<input type="hidden" name="grNo[]" value="{$nameArray[sec].grNo}">
  <td>
    <select name="mathAspCon[]">
    	<option value="">Select Grade</option>
    		{html_options values=$gradeArray.gradeId output=$gradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].mathAspCon}
    </select>
  </td>
  <td>
    <select name="mathAspMen[]">
    	<option value="">Select Grade</option>
    		{html_options values=$gradeArray.gradeId output=$gradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].mathAspMen}
    </select>
  </td>
  <td>
    <select name="mathAspActi[]">
    	<option value="">Select Grade</option>
    		{html_options values=$gradeArray.gradeId output=$gradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].mathAspActi}
    </select>
  </td>
</tr>
{/section}
<tr>
  <td align="center" colspan="4"><input type="submit" value="Save" name="update" class="newSubmitBtn"></td>
</tr>
</table>
</form>
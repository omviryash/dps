{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
		"bAutoWidth": false,
    "aLengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 10,
  	"aaSorting": [[ 1, "asc" ]],
		"bJQueryUI":true
  });
});
</script>
{/block}
{block name="body"}
<div class="hd"><h2 align="center">Route Master List</h2></div>
<table align="center" border="2" id="myDataTable" class="display">
	<thead>
	<tr>
		<td align="center" class="table1"><b>Edit</b></td>
		<td align="center" class="table1"><b>Route Name</b></td>
		<td align="center" class="table1"><b>Start Time</b></td>
		<td align="center" class="table1"><b>End Time</b></td>
		<td align="center" class="table1"><b>Arrival/Departure</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$classArray}
  <tr>
    <td align="left" class="table2"><a href='routeMaster.php?routeMasterId={$classArray[sec].routeMasterId}'>Edit</a></td>
    <td align="left" class="table2" NOWRAP >{$classArray[sec].routeName}</td>
    <td align="left" class="table2" NOWRAP >{$classArray[sec].startTime}</td>
    <td align="left" class="table2">{$classArray[sec].endTime}</td>
    <td align="left" class="table2">{$classArray[sec].arrival}</td>
  </tr>
  {/section}
  </tbody>
</table>
{/block}
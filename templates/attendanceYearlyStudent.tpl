{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[-1, 10, 20, 30, 40, 50], ["All", 10, 20, 30, 40, 50]],
  	"iDisplayLength": 500,
		"bJQueryUI":true
  });
});
</script>
{/block}
{block name="body"}
</br></br>
<form name="formGet" method="GET" action="attendanceYearlyStudent.php">
<table align="center">
	<tr>
		<td class="table2 form01">
		  <select name="startYear">
		    {html_options values=$dateArrVal output=$dateArrOut selected=$studStartYear}
		  </select>
		</td>
    <td>
      <input type="submit" name="submit" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>

</form>
<table align="center" border="1">
	<div class="hd"><h2 align="center">Attendence Report (Yearly)</h2></div>
	<thead>
	<tr>
		<td align="left" class="table1"><b>Academic Year</b></td>
		<td align="left" class="table1"><b>Roll No</b></td>
	  <td align="left" class="table1"><b>Name</b></td>
  	  <td align="left" class="table1"><b>Working Days</b></td>
	  <td align="left" class="table1"><b>Present</b></td>
	  <td align="left" class="table1"><b>Absent</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$attendenceArr}
  <tr>
  	<td align="left" class="table2">{$studStartYear}-{$studEndYear}</td>
  	<td align="left" class="table2">{$attendenceArr[sec].rollNo}</td>
    <td align="left" class="table2">{$attendenceArr[sec].studentName}</td>
    <td align="center" class="table2">{$totalNumberWorkingDay}</td>
    <td align="center" class="table2">{$attendenceArr[sec].countP}</td>
    <td align="center" class="table2">{$attendenceArr[sec].countA}</td>
  </tr>
 {/section}
 </tbody>
</table>
{/block}
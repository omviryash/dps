{include file="./main.tpl"}
{block name="head"}
<link rel="stylesheet" href="./css/buttonStyle.css" type="text/css" />
<script type="text/javascript">
$(document).ready(function(){
	$(".submitClick").change(function()
  {
  	$('.newGoBtn').click();
  });
  var examScheduleId  = $('.examScheduleId').val();
  var selectedSection  = $('.selectedSection').val();
	var datastring = 'examScheduleId=' + examScheduleId + '&selectedSection=' + selectedSection;
	$.ajax({	
		type: 'GET',
		url: 'selectSubClass.php',
	  data: datastring,
	  success:function(data)
	  {
	  	$('.classSection').html(data);
    }
  });
	  
  $(".examScheduleId").change(function()
  {
    var examScheduleId  = $('.examScheduleId').val();
    var selectedSection  = $('.selectedSection').val();
		var datastring = 'examScheduleId=' + examScheduleId + '&selectedSection=' + selectedSection;
		$.ajax({	
			type: 'GET',
			url: 'selectSubClass.php',
		  data: datastring,
		  success:function(data)
		  {
		  	$('.classSection').html(data);
	    }
	  });
  });
});


function buttonHide()
{
	$('#hideMe').hide();
}

</script>
{/block}
{block name="body"}
</br></br>
<center>
<form name="formGet" id="formGet" method="GET" action="reGenerateMarksEntry.php">
<table align="center" border="1">
	<div class="hd"><h2 align="center">Re-generate Marks Entry</h2></div>
	<tr>
		<td class="table2 form01">
		  <select name="startYear" class="submitClick">
		    {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYearSelected}
		  </select>
		</td>
		<td class="table2 form01">
		  <select name="examScheduleId" class="examScheduleId submitClick">
			  <option value="">Select Exam</option>
        {html_options values=$scdArray.examScheduleId output=$scdArray.scheduleDtl selected=$examScheduleId}
      </select>
    </td>
    <td class="table2 form01">
	  	<select name="section" class="classSection submitClick">
	      
		  </select>
		</td>
		<td>
	    <input type="submit" name="go" class="newGoBtn" value="go">
	  </td>
	</tr>
</table>
{if $examScheduleId > 0}
<table align="center" border="0">
  </br>
  <tr>
    <td>
	    <input type="submit" name="submit" onclick="buttonHide();" id="hideMe" class="newSubmitBtn" value="Re Generate">
	  </td>
	</tr>
</table>
{/if}
</form>
</center>
<form name="form2" method="POST" action="reGenerateMarksEntry.php">
<table align="center" border="1">
  </br></br></br>
	<h1 align="center">Student List</h1>
	</br>
	<input type="hidden" name="" class="selectedSection" value="{$sectionReq}">
	<h1 align="center">{$class}/{$sectionReq}</h1>
  </br>
	<tr>
		<td align="left" class="table1"><b>S.R.No.</b></td>
		<td align="left" class="table1"><b>G. R. No.</b></td>
		<td align="left" class="table1"><b>Roll No.</b></td>
		<td align="left" class="table1"><b>Name</b></td>
		<td align="left" class="table1"><b>M/F</b></td>
		<td align="left" class="table1"><b>Marks</b></td>
  </tr>
  <input type="hidden" name="submitSection" value="{$sectionReq}">
  {section name="sec" loop=$stdArray}
  <tr class="trRow">
    <td align="center" class="table2">{$smarty.section.sec.rownum}</td>
    <td align="left" class="table2">{$stdArray[sec].grNo} <input type="hidden" name="grNo[]" value="{$stdArray[sec].grNo}">
    	                                     <input type="hidden" name="examScheduleId[]" value="{$stdArray[sec].examScheduleId}"></td>
    <td align="left" class="table2">{$stdArray[sec].rollNo}</td>
    <td align="left" class="table2">{$stdArray[sec].studentName}</td>
    <td align="left" class="table2">{$stdArray[sec].gender}</td>
    <td align="center" class="table2">
    	<input type="text" name="marks[]" value="{$stdArray[sec].marks}">
    </td>
  </tr>
  {/section}
  </tr>
  {if $count != 0}
  <tr>
    <td align="center" colspan="7" class="table2"><input type="submit" name="submitTaken" class="newSubmitBtn" value="Save"></td>
  </tr>
  {/if}
</table>
</form>
{/block}
<form action="getEng.php" method="POST">
<input type="hidden" name="subjectName" value="{$subjectName}">
<input type="hidden" name="termValue" value="{$termValue}">
<table border=2 align="center">
<tr>
  {if $termValue eq 2}
	  <td>
	    New Session Begin On
	  </td>
	  <td colspan="10" align="center">
	    {html_select_date prefix="promoted" start_year="-2" end_year="+2" time=$promoted field_order="DMY" day_value_format="%02d" }
	  </td>
	{else}
	{/if}
</tr>
<tr>
  <td align="left">Student Name</td>
  {if $termValue eq 1} 
    <td align="center">Participation (Term 1)</td>    
    <td align="center">Achievments (Term 1)</td>
    <td align="center">Remarks (Term 1)</td>
    <td align="center">Attendance(Term 1)</td>
  {else}
    <td align="center">Participation (Term 2)</td>
    <td align="center">Achievments (Term 2)</td>
    <td align="center">Remarks (Term 2)</td>
    <td align="center">Attendance (Term 2)</td>
    <td align="center">Promoted Class</td>
  {/if}
</tr>
{section name="sec" loop=$nameArray}
<tr>
	<td nowrap>{$nameArray[sec].studentName}</td>
	<input type="hidden" name="grNo[]" value="{$nameArray[sec].grNo}">
	{if $termValue eq 1}
	  <td><textarea rows="4" cols="50" name="partici[]">{$gradeSelectionArray[$nameArray[sec].grNo].partici}</textarea></td>
	  <td><textarea rows="4" cols="50" name="achiev[]">{$gradeSelectionArray[$nameArray[sec].grNo].achiev}</textarea></td>
	  <td><textarea rows="4" cols="50" name="remarks[]">{$gradeSelectionArray[$nameArray[sec].grNo].remarks}</textarea></td>
	  <td><input type="text" name="attance[]" value="{$gradeSelectionArray[$nameArray[sec].grNo].attance}"></td>
	{else}
	  <td><textarea rows="4" cols="50" name="partici1[]">{$gradeSelectionArray[$nameArray[sec].grNo].partici1}</textarea></td>
	  <td><textarea rows="4" cols="50" name="achiev1[]">{$gradeSelectionArray[$nameArray[sec].grNo].achiev1}</textarea></td>
	  <td><textarea rows="4" cols="50" name="remarks1[]">{$gradeSelectionArray[$nameArray[sec].grNo].remarks1}</textarea></td>
	  <td><input type="text" name="attance1[]" value="{$gradeSelectionArray[$nameArray[sec].grNo].attance1}"></td>
	  <td>
	  	<select name="classMasterId[]">
	  		<option value="0">Select Class</option>
	  		  {html_options values=$classArray.classMasterId output=$classArray.className selected=$gradeSelectionArray[$nameArray[sec].grNo].classMasterId}
	    </select>
	  </td>
	{/if}
</tr>
{/section}
<tr>
  <td align="center" colspan="10"><input type="submit" value="Save" name="update" class="newSubmitBtn"></td>
</tr>
</table>
</form>
{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
		"bJQueryUI":true,
		"aLengthMenu": [[-1, 10, 20, 30, 40, 50], ["All", 10, 20, 30, 40, 50]],
  	"iDisplayLength": 500
  });
});
</script>
{/block}
{block name="body"}
</br></br>
<form name="formGet" method="GET" action="attendenceReport.php">
<table align="center">
	<tr>
    <td>
      {html_select_date prefix="attendence" start_year="-25" end_year="+25" field_order="DMY" time=$attendenceDate day_value_format="%02d"}
    </td>
    <td>
      <input type="submit" name="submit" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
</form>
<table align="left" border="1" id="myDataTable" class="display">
	<div class="hd"><h2 align="center">Attendance Taken Status Report</h2></div>
	<thead>
	<tr>
	  <td align="center"><b>S R No</b></td>
	  <td align="center"><b>Class</b></td>
	  <td align="center"><b>Section</b></td>
	  <td align="center"><b>Class Teacher</b></td>
	  <td align="center"><b>Present</b></td>
	  <td align="center"><b>Absent</b></td>
	  <td align="center"><b>Total Students</b></td>
	  <td align="center"><b>Attendance Taken Status</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$attendenceArr}
  <tr>
    <td align="center">{$smarty.section.sec.rownum}</td>
    <td align="center">{$attendenceArr[sec].class}</td>
    <td align="center">{$attendenceArr[sec].section}</td>
    <td align="center">{$attendenceArr[sec].name}</td>
    {if $attendenceArr[sec].taken == 'NT'}
	    <td align="center">0</td>
	    <td align="center">0</td>
	    <td align="center">{assign var="c" value=$attendenceArr[sec].countPresent+$attendenceArr[sec].countAbsent}{$c}</td>	
	    <td align="center">Not Taken</td>
    {else}
	    <td align="center">{$attendenceArr[sec].countPresent}</td>
	    <td align="center">{$attendenceArr[sec].countAbsent}</td>
       <td align="center">{assign var="c" value=$attendenceArr[sec].countPresent+$attendenceArr[sec].countAbsent}{$c}</td>	    
      <td align="center">Taken</td>
    {/if}
  </tr>
	 {/section}
	 </tbody>
	 <tfoot>
	   <tr>
	   	 <th colspan="3" align="center"></th>
	   	 <th align="center">Total Students</th>
	   	 <th align="center">{$totalPresent}</th>
	   	 <th align="center">{$totalAbsent}</th>
	   	 <th align="center">{$totalStudent}</th>
	   	 <th></th>
	   </tr>
	 </tfoot>
</table>
{/block}

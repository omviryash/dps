{include file="./main.tpl"}
{block name="body"}
<form action="tcCerEntry.php" enctype="multipart/form-data" method="POST">
<table align="center" border="1">
<input type="hidden" value="{$studentMasterId}" name="studentMasterId" id="studentMasterId" >
<br><br><br>
<div class="hd"><h2 align="center">Tc/Lc Entry</h2></div>
<br><br>
<tr>
  <td align="center" colspan="4" style="color:red; font-size:18px;"><b>{$confirm}</b></td>
</tr>
<tr>
	<td align="left" class="table1">Name : </td>
	<td class="table2 form01"><input type="text" name="studentName" autofocus required value="{$studentName}" DISABLED /></td>
	<td align="left" class="table1">Father Name : </td>
	<td class="table2 form01"><input type="text" name="fatherName" value="{$fatherName}" DISABLED ></td>
</tr>
<tr>
	<td align="left" class="table1">G.R.No : </td>
	<td class="table2 form01"><input type="text" name="grNo" class="grNo" onblur="checkGrno();" value="{$grNo}" DISABLED /></td>
	<td align="left" class="table1">Date Of Birth : </td>
	<td class="table2 form01" >
		{html_select_date prefix='dob' field_order="DmY" month_format="%m" start_year='-25' end_year='+25' time=$dateOfBirth day_value_format="%02d" }
	</td>
</tr>
<tr>
	<td class="table1">Joined In Class : </td>
	<td class="table2 form01">
  	<select name="joinedInClass" DISABLED >
	    <option value="">Select class</option>
	    {html_options values=$cArray.className output=$cArray.className selected=$joinedInClass}
	  </select>
  </td>
  <td align="left" class="table1">Joining Date : </td>
	<td class="table2 form01" >
		{html_select_date prefix='joining' field_order="DmY" month_format="%m" start_year='-25' end_year='+25' time=$joiningDate day_value_format="%02d"}
	</td>
</tr>
<tr>
	<td align="left" class="table1">House Name: </td>
	<td class="table2 form01">
		<select name="houseId" DISABLED >
			<option value="0">Select House</option>
			  {html_options values=$houseArray.houseId output=$houseArray.houseName selected=$houseId}
	  </select>
	</td>
	<td align="left" class="table1">Mothers Name : </td>
	<td class="table2 form01"><input type="text" name="mothersName" value="{$mothersName}" DISABLED ></td>
</tr>
<tr>
	<th align="center" colspan="4" class="table1">School Leaving Entry</th>
</tr>
<tr>
	<td align="left" colspan="" class="table1">Activated</td>
	<td align="left" colspan="" class="table2 form01">
		<select name="activated" >
			{html_options values=$secArrValue output=$secArrOut selected=$activated}
	  </select>
	</td>
	<td align="left" colspan="" class="table1">Sr No.</td>
	<td align="left" colspan="" class="table2 form01">
		<input type="text" name="tcSrNo" value="{$tcSrNo}">
	</td>
</tr>
<tr>
	<td align="left" class="table1">Caste :</td>
	<td align="left" class="table2 form01">
		<input type="text" name="tribe" value="{$tribe}">
	</td>
	<td align="left" class="table1">Last Studies Class :</td>
	<td align="left" class="table2 form01">
		<input type="text" name="lastStud" value="{$lastStud}">
	</td>
</tr>
<tr>
	<td align="left" class="table1">Last Result :</td>
	<td align="left" class="table2 form01">
		<input type="text" name="takenResult" value="{$takenResult}">
	</td>
	<td align="left" class="table1">if so Once/twice :</td>
	<td align="left" class="table2 form01">
		<input type="text" name="sameClass" value="{$sameClass}">
	</td>
</tr>
<tr>
	<td align="left" class="table1">Subject</td>
	<td align="left" colspan="3" class="table2 form01">
		<textarea name="subjectStudies" cols="90">{$subjectStudies}</textarea>
	</td>
</tr>
<tr>
	<td align="left" class="table1">Qualifier For Promotion :</td>
	<td align="left" class="table2 form01">
		<input type="text" name="higherClassYesNo" value="{$higherClassYesNo}">
	</td>
	<td align="left" class="table1">Promotion Class :</td>
	<td align="left" class="table2 form01">
		<input type="text" name="higherClass" value="{$higherClass}">
	</td>
</tr>
<tr>
	<td align="left" class="table1">Pupil Has Paid School Dues :</td>
	<td align="left" class="table2 form01">
		<input type="text" name="schoolDues" value="{$schoolDues}">
	</td>
	<td align="left" class="table1">Working Days :</td>
	<td align="left" class="table2 form01">
		<input type="text" name="workingDays" value="{$workingDays}">
	</td>
</tr>
<tr>
	<td align="left" class="table1">Present Days :</td>
	<td align="left" class="table2 form01">
		<input type="text" name="presentDays" value="{$presentDays}">
	</td>
	<td align="left" class="table1">Ncc Cadet :</td>
	<td align="left" class="table2 form01">
		<input type="text" name="nccCadet" value="{$nccCadet}">
	</td>
</tr>
<tr>
	<td align="left" class="table1">Games Played :</td>
	<td align="left" class="table2 form01">
		<input type="text" name="gamePlayed" value="{$gamePlayed}">
	</td>
	<td align="left" class="table1">General Conduct :</td>
	<td align="left" class="table2 form01">
		<input type="text" name="genConduct" value="{$genConduct}">
	</td>
</tr>
<tr>
	<td align="left" class="table1">Date of application for certificate : </td>
	<td class="table2 form01" >
		{html_select_date prefix='appCer' field_order="DmY" month_format="%m" start_year='-25' end_year='+25' time=$appCer day_value_format="%02d" }
	</td>
  <td align="left" class="table1">Date of issue of certificate : </td>
	<td class="table2 form01" >
		{html_select_date prefix='issueCer' field_order="DmY" month_format="%m" start_year='-25' end_year='+25' time=$issueCer day_value_format="%02d"}
	</td>
</tr>
<tr>
	<td align="left" class="table1">Reasons for leaving the school :</td>
	<td align="left" class="table2 form01">
		<input type="text" name="reasons" value="{$reasons}">
	</td>
	<td align="left" class="table1">Any other remarks</td>
	<td align="left" class="table2 form01">
		<input type="text" name="remarks" value="{$remarks}">
	</td>
</tr>
<tr>
	<td colspan="4" align="center"><input type="submit" name="submit" class="newSubmitBtn" value="Save">
</tr>
</table>
</form>
{/block}
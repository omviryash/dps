{include file="./main.tpl"}
{block name="head"}
<script type="text/javascript">
$(document).ready(function(){
	$('.hideRow').hide();
});

function increment(obj)
{
	var row      = $(obj).parents('.trRow');
	var grNoHide = row.find('.grNo').val();
	row.find('.button').hide();
}

function pass(obj)
{
	var row      = $(obj).parents('.trRow');
	var grNo     = row.find('.grNo').val();
	var gender   = row.find('.gender').val();
	var classnew = row.find('.classnew').val();
	var section  = row.find('.section').val();
	var passFail = row.find(obj).val();
	
	var dataString = "grNo=" + grNo + "&gender=" + gender + "&section=" + section +
	                 "&classnew=" + classnew + "&passFail=" + passFail;
	$.ajax({
	  type: "GET",
	  url: "studentPass.php",
	  data: dataString,
  });
}
</script>
{/block}
{block name="body"}
<table align="center" border="1">
  </br></br>
	<div class="hd"><h2 align="center">Promote Student</h2></div>
	</br>
	<h1 align="center">{$class}/{$section}</h1>
	</br>
	<tr>
		<td align="left"><b>G. R. No.</b></td>
		<td align="left"><b>Name</b></td>
		<td align="left"><b>M/F</b></td>
		<td align="left"><b>Pass</b></td>
		<td align="left"><b>Fail</b></td>
  </tr>
  {section name="sec" loop=$stdArray}
  <tr Class="trRow">
    <td align="left">{$stdArray[sec].grNo}</td>
      <input type="hidden" value="{$stdArray[sec].grNo}" name="studentName" class="grno">
      <input type="hidden" class="grNo" value="{$stdArray[sec].grNo}" name="grNo" class="grno">
    <td align="left">{$stdArray[sec].studentName}</td>
      <input type="hidden" value="{$stdArray[sec].studentName}" name="studentName">
    <td align="left">{$stdArray[sec].gender}
      <input type="hidden" class="gender" value="{$stdArray[sec].gender}" name="gender">
      <input type="hidden" name="class" class="classnew" value="{$stdArray[sec].class}">
      <input type="hidden" name="section" class="section" value="{$section}">
    </td>
    {if $stdArray[sec].nominalDone == 2}
    <td align="left"><input type="submit" name="Pass" class="button presentButton" value="Pass" onclick="pass(this); increment(this);"></td>
    <td class="hideRow"></td>
    <td align="left"><input type="submit" name="Fail" class="button absentButton" value="Fail" onclick="pass(this); increment(this);"></td>
    <td class="hideRow"></td>
    {else}
      <td colspan="2" align="center"><font size="5" color='Green'></b>Promote</b></font></td>
    {/if}
  </tr>
  {/section}
  </tr>
</table>
{/block}
{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 10,
  	"aaSorting": [[0, 'desc']],
		"bJQueryUI":true
  });
});
</script>
{/block}
{block name="body"}
<!-- form name="form1" method="POST" action="feedbackInbox.php">
<table align="center" border="0" class="table2">
<div class="hd"><h2 align="center">Message</h2></div>
<tr>
  <td align="center" colspan="2" style="color:red; font-size:18px;"><b>{$confirm}</b></td>
</tr>
<tr>
  <td class="table2">Name</td>
  <td class="table2 form01">
  	<select name='name' AUTOFOCUS REQUIRED >
  	  <option value=''>Select</option>
  	  {if $s_userType != 'Student'}
  	  {html_options values=$cArray.studentName output=$cArray.studentName selected=$toName}
  	  {/if}
  	  {html_options values=$dArray.name output=$dArray.name selected=$toName}
    </select>
	</td>
  </tr>
	<tr>
  <td class="table2">Message</td>
  <td class="table2 form01">
  	<textarea name="message" style="margin: 0px 2px; width: 530px; height: 300px;" required></textarea>
	</td>
</tr>
<tr>
  <td class="table2"></td>
  <td class="table2 form01" align="center"><input type="submit" name="Submit" class="button" value="Save"></td>
</tr>
</table>
</form -->

<div class="hd"><h2 align="center">My Inbox Messages</h2></div>
<table align="left" border="2" id="myDataTable" class="display">
	<thead>
	<tr>
		<td align="center" class="table1"><b>Date & Time</b></td>
		<td align="center" class="table1"><b>From Name</b></td>
		<td align="center" class="table1"><b>To Name</b></td>
		<td align="center" class="table1"><b>Message</b></td>
		<td align="center" class="table1"><b>Reply</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$classArray}
  <tr>
  	<td align="left" class="table2" NOWRAP >{$classArray[sec].dateTime}</td>
    <td align="left" class="table2" NOWRAP >{$classArray[sec].fromName}</td>
    <td align="left" class="table2" NOWRAP >{$classArray[sec].name}</td>
    <td align="left" class="table2">{$classArray[sec].message}</td>
    <td align="left" class="table2"><a href="feedbackEntry.php?feedbackId={$classArray[sec].feedbackId}">Reply</a></td>
  </tr>
  {/section}
  </tbody>
</table>
{/block}
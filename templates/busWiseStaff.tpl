{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script src="./media1/js/jquery-1.4.4.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.jeditable.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[100, 25, 50, 100, 500, 1000], [100, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 100,
  	"bAutoWidth":false,
		"bJQueryUI":true
  });
  
  /* Editable Start*/
	var oTable = $('#myDataTable').dataTable();
     
  /* Apply the jEditable handlers to the table */
  oTable.$('th').editable( './updateRollNoData.php', {
      "submitdata": function ( value, settings ) {
          return {
              "row_id": this.parentNode.getAttribute('th.id'),
              "column": oTable.fnGetPosition( this )[2]
          };
      },
      "height": "20px",
      "width": "100"
  });
  
  /* Editable End*/
});
</script>
{/block}
{block name="body"}
</br></br>
<!--form name="formGet" method="GET" action="busWiseStaff.php">
<table align="center">
	<tr>
		<td class="table2 form01">
		  <select name="routeMasterId" autofocus="autofocus" required >
		    <option value="">Select Route</option>
		    {html_options values=$rArray.routeMasterId output=$rArray.routeName selected=$routeMasterId}
		  </select>
	  </td>
    <td class="table2 form01">
		  <select name="vehicleMasterId">
		    <option value="0">Select Bus</option>
		    {html_options values=$cArray.vehicleMasterId output=$cArray.vehicleNo selected=$vehicleMasterId}
		  </select>
	  </td>
    <td>
      <input type="submit" name="submit" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
</form-->

<table align="left" border="1" id="myDataTable" class="display">
	<div class="hd"><h2 align="center">Bus List</h2></div>
	<thead>
	<tr>
		<td align="left"><b>Sr No</b></td>
		<td align="left"><b>Name</b></td>
		<td align="left"><b>Bus Arrival</b></td>
		<td align="left"><b>Bus Departure</b></td>
		<td align="left"><b>Route Arrival</b></td>
		<td align="left"><b>Route Departure</b></td>
		<td align="left"><b>Bus Stop</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$nominalArr}
  <tr>
  	<td align="left">{$smarty.section.sec.rownum}</td>
  	<td align="left">{$nominalArr[sec].name}</td>
  	<td align="left">{$nominalArr[sec].vehicleNo}</td>
  	<td align="left">{$nominalArr[sec].vehicleNo2}</td>
  	<td align="left">{$nominalArr[sec].routeName}</td>
  	<td align="left">{$nominalArr[sec].routeName2}</td>
    <td align="left">{$nominalArr[sec].busStop}</td>
  </tr>
  {/section}
  </tbody>
</table>
{/block}
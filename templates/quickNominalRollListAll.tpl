{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script src="./media1/js/jquery-1.4.4.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.jeditable.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[-1, 10, 20, 30, 40, 50], ["All", 10, 20, 30, 40, 50]],
  	"iDisplayLength": 500,
		"bJQueryUI":true
  });
  
  /* Editable Start*/
	var oTable = $('#myDataTable').dataTable();
     
  /* Apply the jEditable handlers to the table */
  oTable.$('th').editable( './updateRollNoData.php', {
      "submitdata": function ( value, settings ) {
          return {
              "row_id": this.parentNode.getAttribute('th.id'),
              "column": oTable.fnGetPosition( this )[2]
          };
      },
      "height": "20px",
      "width": "100"
  });
  
  /* Editable End*/
  
  $("#startDateYear").change(function()
  {
    var yearExtra  = $('#startDateYear').val();
		var endDate    = yearExtra;
		var datastring = 'endDate=' + endDate;
		$.ajax({	
			type: 'GET',
			url: 'endYear.php',
		  data: datastring,
		  success:function(data)
		  {
		  	$('#endDateYear').val(data);
	    }
	  });
  });
});
</script>
{/block}
{block name="body"}
</br></br>
<form name="formGet" method="GET" action="quickNominalRollListAll.php">
<table align="center">
	<tr>
		<td class="table2 form01">
		  <select name="class" autofocus="autofocus" required >
		    <option value="">Select class</option>
		    {html_options values=$cArray.className output=$cArray.className selected=$class}
		  </select>
	  </td>
    <td class="table2 form01">
		  <select name="classSection">
		    <option value="0">Select Section</option>
		    {html_options values=$secArrOut output=$secArrOut selected=$section}
		  </select>
	  </td>
	  <td class="table2 form01">
      <select name="startYear" id="startDateYear">
        {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYear}
      </select>
    </td>
    <td>
      <input type="submit" name="submit" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
</form>
<center><a href="nominalRollClassPrint.php?class={$class}&section={$section}">Print</a></center>
<center><a href="idPrint.php?class={$class}&section={$section}">Id Card Print</a></center>
<table align="left" border="1" id="myDataTable" class="display">
	<div class="hd"><h2 align="center">Quick Nominal Roll</h2>
	<thead>
	<tr>
		<td align="left"><b>Sr No</b></td>
		<td align="left"><b>Academic Year</b></td>
		<td align="left"><b>GR No</b></td>
		<td align="left"><b>Roll No</b></td>
		<td align="left"><b>Student Name</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$nominalArr}
  <tr>
  	<td align="left">{$smarty.section.sec.rownum}</td>
  	<td align="left">{$nominalArr[sec].academicYear}</td>
    <td align="left">{$nominalArr[sec].grNo}</td>
    <th align="left" id="{$nominalArr[sec].nominalRollId}_1">{$nominalArr[sec].rollNo}</th>
    <td align="left">{$nominalArr[sec].studentName}</td>
  </tr>
  {/section}
  </tbody>
</table>
{/block}
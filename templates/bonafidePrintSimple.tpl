<table align="center" width="100%" border="0" style="font-size:20px;">
<tr>
  <td align="center"><img src="./images/logoCertificate.png"></td>
</tr>
<!--tr>
  <td align="center">Haripar, Survey No.12, B/H N.R.I Bungalows, Kalawad  Road ,  Rajkot - 360 005.</td>
</tr-->
</table>
<table align="left" width="100%" border="0" style="font-size:20px;">
<tr>
  <td align="left" width="100px">Phone No. : </td>
  <td align="left">+91 9375070921/22</td>
</tr>
<tr>
  <td align="left" width="100px">Fax No. : </td>
  <td align="left">+91 9375070923</td>
</tr>
</table>
<table align="center" width="100%" border="0" style="font-size:20px;">
<tr>
  <td align="left">CBSE Affilition No. 430054</td>
  <td align="right">School Code: 13015</td>
</tr>
</table>
<hr>
<table align="center" width="100%" border="0" style="font-size:20px;">
<tr>
  <td align="right">Date : {$bonafideDate}</td>
</tr>
</table>
<table align="center" border="0" height="100px" style="font-size:20px;">
<tr>
  <td align="center"></td>
</tr>
</table>
<table align="center" border="1" cellpadding="7" style="font-size:20px;">
<tr>
  <td align="center">BONAFIDE CERTIFICATE</td>
</tr>
</table>
<table align="center" border="0" height="100px" style="font-size:20px;">
<tr>
  <td align="center"></td>
</tr>
</table>
<table align="center" border='0' width="100%" style="font-size:20px;">
<tr>
  <td align="left" style="line-height:200%">This is to certify that <b><u>{$studentName}</u></b> {if $gender == 'Male'} s/o {else} d/o {/if} <b><u>{$fatherName}
  	  </u></b> is a bona fide student of our school w.e.f. <b>{$wefDate}.
      </b> During the session <b>{$academicYear},
      </b> {if $gender == 'Male'} he {else} she {/if} is studying in Class <b>{$classRoma}{if $classWord != ''} ({$classWord}){/if}.
      </b> As per school record {if $gender == 'Male'} his {else} her {/if} date of birth is <b>{$dateOfBirth}
    </b> (<b>{$dateOfBirthWord}</b>).
  </td>
</tr>
</table>
</br></br></br></br>
<table align="left" width="100%" border="0" style="font-size:20px;">
<tr>
  <td align="left" height="100px">Issuing Assistant: ______________</td>
</tr>
</table>

<table align="left" width="100%" border="0" style="font-size:20px;">
	</br></br></br></br></br></br>
<tr>
	
  <td align="right">Principal: ________________</td>
</tr>
</table>
{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 10,
		"bJQueryUI":true
  });
  $(".omAttend").change(function()
  {
  	$('.newGoBtn').click();
  });
});
</script>
{/block}
{block name="body"}
</br></br>
<form name="formGet" method="GET" action="attendenceReportYearly.php">
<table align="center">
	<tr>
		{if $s_userType == 'Administrator'}
		<td class="table2 form01 omAttend">
		  <select name="class" autofocus="autofocus" >
		    <option value="">Select class</option>
		    {html_options values=$cArray.className output=$cArray.className selected=$class}
		  </select>
	  </td>
    <td class="table2 form01 omAttend">
		  <select name="classSection">
		    <option value="0">Select Section</option>
		    {html_options values=$secArrOut output=$secArrOut selected=$section}
		  </select>
	  </td>
	  {/if}
	  <td class="table2 form01">
		  <select name="startYear">
		    {html_options values=$dateArrVal output=$dateArrOut selected=$attendenceStartDateSelected}
		  </select>
		</td>
    <td>
      <input type="submit" name="submit" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
<!--center>Total Number Working Day = {$totalNumberWorkingDay}</center-->
</form>
<table align="left" border="1" id="myDataTable" class="display">
	</br>
	<h1 align="center">{$class}/{$section}</h1>
  </br>
	<div class="hd"><h2 align="center">Attendence Report (Yearly)</h2></div>
	<thead>
	<tr>
		<td align="left"><b>Roll No</b></td>
	  <td align="left"><b>Name</b></td>
	  <td align="left"><b>Working Day</b></td>
	  <td align="left"><b>Present</b></td>
	  <td align="left"><b>Absent</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$attendenceArr}
  <tr>
  	<td align="left">{$attendenceArr[sec].rollNo}</td>
    <td align="left">{$attendenceArr[sec].studentName}</td>
    <td align="left">{$attendenceArr[sec].totalNumberWorkingDay}</td>
    <td align="left">{$attendenceArr[sec].countP}</td>
    <td align="left">{$attendenceArr[sec].countA}</td>
  </tr>
 {/section}
 </tbody>
 <tfoot>
   <tr>
   	<th></th>
   	<th></th>
   	<th></th>
   	<th></th>
   </tr>
 </tfoot>
</table>
{/block}
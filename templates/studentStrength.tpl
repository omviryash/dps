{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[100, 25, 50, 100, 500, 1000], [100, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 100,
  	"bAutoWidth":false,
		"bJQueryUI":true
  });
  
  $("#startDateYear").change(function()
  {
    var yearExtra  = $('#startDateYear').val();
		var endDate    = yearExtra;
		var datastring = 'endDate=' + endDate;
		$.ajax({	
			type: 'GET',
			url: 'endYear.php',
		  data: datastring,
		  success:function(data)
		  {
		  	$('#endDateYear').val(data);
	    }
	  });
  });
});
</script>
{/block}
{block name="body"}
</br></br>
<form name="formGet" method="GET" action="studentStrength.php">
<table align="center">
	<tr>
		<td class="table2 form01">
      <select name="startYear" id="startDateYear">
        {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYear}
      </select>
	  </td>
	  <td>
      <input type="submit" name="submit" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
</form>
<table align="left" border="1" id="myDataTable" class="display">
	<div class="hd"><h2 align="center">Student Strength Report</h2>
	<thead>
	<tr>
		<td align="left"><b>S R No</b></td>
		<td align="left"><b>Class</b></td>
		<td align="left"><b>Section</b></td>
		<td align="left"><b>Class Teacher</b></td>
		<td align="left"><b>Boys</b></td>
		<td align="left"><b>Girls</b></td>
		<td align="left"><b>Total</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$strengthArr}
  <tr>
  	<td align="left">{$smarty.section.sec.rownum}</td>
    <td align="left">{$strengthArr[sec].class}</td>
    <td align="left">{$strengthArr[sec].section}</td>
    <td align="left">{$strengthArr[sec].name}</td>
    <td align="left">{$strengthArr[sec].countMale}</td>
    <td align="left">{$strengthArr[sec].countFemale}</td>
    <td align="left">{$strengthArr[sec].countAll}</td>
  </tr>
  {/section}
  </tbody>
	<tfoot>
	 <tr>
	 	<th></th>
	 	<th></th>
	 	<th></th>
	 	<th></th>
	 	<th align="left">{$totalMale}</th>
	 	<th align="left">{$totalGirls}</th>
	 	<th align="left">{$total}</th>
	 </tr>
	</tfoot>
</table>
{/block}
{include file="./main.tpl"}
{block name="head"}
<link rel="stylesheet" href="./css/buttonStyle.css" type="text/css" />
<script type="text/javascript">
$(document).ready(function(){
	$(".submitClick").change(function()
  {
  	$('.newGoBtn').click();
  });
  var examScheduleId  = $('.examScheduleId').val();
  var selectedSection  = $('.selectedSection').val();
	var datastring = 'examScheduleId=' + examScheduleId + '&selectedSection=' + selectedSection;
	$.ajax({	
		type: 'GET',
		url: 'selectSubClass.php',
	  data: datastring,
	  success:function(data)
	  {
	  	$('.classSection').html(data);
    }
  });
	  
  $(".examScheduleId").change(function()
  {
    var examScheduleId  = $('.examScheduleId').val();
    var selectedSection  = $('.selectedSection').val();
		var datastring = 'examScheduleId=' + examScheduleId + '&selectedSection=' + selectedSection;
		$.ajax({	
			type: 'GET',
			url: 'selectSubClass.php',
		  data: datastring,
		  success:function(data)
		  {
		  	$('.classSection').html(data);
	    }
	  });
  });
});


function buttonHide()
{
	$('#hideMe').hide();
}

function confirmDelete()
{
	if(confirm('Are You Sure 1 st time?'))
	{
		if(confirm('Are You Sure 2nd time?'))
		  return true;
		else
		  return false;
	}
	else
	  return false;
}
</script>
{/block}
{block name="body"}
</br></br>
<center>
<form name="formGet" id="formGet" method="GET" action="marksEntry.php">
<table align="center" border="1">
	<div class="hd"><h2 align="center">Marks Entry</h2></div>
	<tr>
		<td class="table2 form01">
		  <select name="startYear" class="submitClick">
		    {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYearSelected}
		  </select>
		</td>
		<td class="table2 form01">
		  <select name="examScheduleId" class="examScheduleId submitClick">
			  <option value="">Select Exam</option>
        {html_options values=$scdArray.examScheduleId output=$scdArray.scheduleDtl selected=$examScheduleId}
      </select>
    </td>
    <td class="table2 form01">
	  	<select name="section" class="classSection submitClick">
	      
		  </select>
		</td>
		<td>
	    <input type="submit" name="go" class="newGoBtn" value="go">
	  </td>
	</tr>
</table>
{if $count == 0 && $examScheduleId > 0}
<table align="center" border="0">
  </br>
  <tr>
    <td>
	    <input type="submit" name="submit" onclick="buttonHide();" id="hideMe" class="newSubmitBtn" value="Generate">
	    <a href="reGenerateMarksEntry.php"> ( Re-generate Marks Entry ) </a>
	  </td>
	</tr>
</table>
{/if}
</form>
</center>
<form name="form2" method="POST" action="marksEntry.php">
<table align="center" border="1">
	<input type="hidden" name="startYear" value="{$academicStartYearSelected}">
  </br></br>
	<h1 align="center">Student List</h1>
	</br>
	<input type="hidden" name="" class="selectedSection" value="{$sectionReq}">
	<input type="hidden" name="examScheduleIdTaken" value="{$examScheduleId}">
	<h1 align="center">{$class}/{$sectionReq}</h1>
  </br>
	<tr>
		<td align="left" class="table1"><b>S.R.No.</b></td>
		<td align="left" class="table1"><b>Academic Year</b></td>
		<td align="left" class="table1"><b>G. R. No.</b></td>
		<td align="left" class="table1"><b>Class</b></td>
		<td align="left" class="table1"><b>Roll No.</b></td>
		<td align="left" class="table1"><b>Name</b></td>
		<td align="left" class="table1"><b>Maximum Marks</b></td>
		<td align="left" class="table1"><b>Marks</b></td>
		{if $class == '11 SCI.' || $class == '11 COM.' || $class == '12 SCI.' || $class == '12 COM.'}
			
			<td align="left" class="table1"><b>Practical</b></td>
			
		{/if}
		{if $count != 0 && $isUserAdmin == 'no'}
		<td align="left" class="table1"><b>
    	<a href="allMarksDelete.php?examScheduleId={$examScheduleId}&startYear={$academicStartYearSelected}&section={$sectionReq}" onclick="return confirmDelete();">Delete All</a>
    	{/if}
    </td>
  </tr>
  <input type="hidden" name="submitSection" value="{$sectionReq}">
  {section name="sec" loop=$stdArray}
  <tr class="trRow">
    <td align="center" class="table2">{$smarty.section.sec.rownum}</td>
    <td align="left" class="table2">{$academicStartYearSelected}-{$academicStartYearSecondNum}</td>
    <td align="left" class="table2">{$stdArray[sec].grNo} <input type="hidden" name="grNo[]" value="{$stdArray[sec].grNo}">
    	                                     <input type="hidden" name="examScheduleId[]" value="{$stdArray[sec].examScheduleId}"></td>
    <td align="left" class="table2">{$class}/{$sectionReq}</td>
    <td align="left" class="table2">{$stdArray[sec].rollNo}</td>
    <td align="left" class="table2">{$stdArray[sec].studentName}</td>
    <td align="left" class="table2">{$stdArray[sec].maxMarks}</td>
    <td align="center" class="table2">
    	<input type="text" name="marks[]" value="{$stdArray[sec].marks}">
    </td>
    {if $class == '11 SCI.' || $class == '11 COM.' || $class == '12 SCI.' || $class == '12 COM.'}
    	
	    <td align="center" class="table2">
	    	<input type="text" name="marksPractical[]" value="{$stdArray[sec].marksPractical}">
	    </td>
    	
    {/if}
    {if $count != 0 && $isUserAdmin == 'no'}
	    <td align="left" class="table2">
	    	<a href="marksDelete.php?examMarksId={$stdArray[sec].examMarksId}&startYear={$academicStartYearSelected}&examScheduleId={$examScheduleId}&section={$sectionReq}" onclick="return confirmDelete();">Delete</a>
	    </td>
	{/if}
  </tr>
  {/section}
  </tr>
  {if $count != 0 && $isUserAdmin == 'no'}
  <tr>
    <td align="center" colspan="7" class="table2"><input type="submit" name="submitTaken" class="newSubmitBtn" value="Save"></td>
  </tr>
  {/if}
</table>
</form>
{/block}
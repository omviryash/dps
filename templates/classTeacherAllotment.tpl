{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
		"bAutoWidth": false,
    "aLengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 10,
		"bJQueryUI":true
  });
});
</script>
{/block}
{block name="body"}
<form name="form1" method="POST" action="classTeacherAllotment.php">
<table align="center" border="0" class="table2">
<div class="hd"><h2 align="center">Class Teacher Allotment</h2>
<input type="hidden" name="classTeacherAllotmentId" value="{$classTeacherAllotmentId}">
<tr>
  <td align="center" colspan="2" style="color:red; font-size:18px;"><b>{$confirm}</b></td>
</tr>
<tr>
  <td class="table2">Name</td>
  <td class="table2 form01">
	  <select name="employeeMasterId" autofocus="autofocus" required >
	    <option value="">Select Teacher</option>
	    {html_options values=$empArray.employeeMasterId output=$empArray.name selected=$employeeMasterId}
	  </select>
  </td>
</tr>
<tr>
  <td class="table2">Acadamic Year</td>
  <td class="table2 form01">
    <select name="startYear" id="startDateYear">
      {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYear}
    </select>
  </td>
</tr>
<tr>
  <td class="table2">Class</td>
  <td class="table2 form01">
  	<select name="class" autofocus="autofocus" required >
	    <option value="">Select class</option>
	    {html_options values=$cArray.className output=$cArray.className selected=$class}
	  </select>
  </td>
</tr>
<tr>
  <td class="table2">Section</td>
  <td class="table2 form01">
  	<select name="section">
      <option value="0">Select Section</option>
      {html_options values=$secArrOut output=$secArrOut selected=$section}
	  </select>
	</td>
</tr>
<tr>
  <td class="table2"></td>
  <td class="table2 form01" align="center"><input type="submit" name="Submit" class="button" value="Save"></td>
</tr>
</table>
</form>
{/block}
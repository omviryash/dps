{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[100, 25, 50, 100, 500, 1000], [100, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 100,
  	"bAutoWidth":false,
		"bJQueryUI":true
  });
  
  $("#startDateYear").change(function()
  {
    var yearExtra  = $('#startDateYear').val();
		var endDate    = yearExtra;
		var datastring = 'endDate=' + endDate;
		$.ajax({	
			type: 'GET',
			url: 'endYear.php',
		  data: datastring,
		  success:function(data)
		  {
		  	$('#endDateYear').val(data);
	    }
	  });
  });
});
</script>
{/block}
{block name="body"}
</br></br>
<form name="formGet" method="GET" action="classWiseHouseReport.php">
<table align="center">
	<tr>
		<td class="table2 form01">
      <select name="startYear" id="startDateYear">
        {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYear}
      </select>
	  </td>
	  <td>
      <input type="submit" name="submit" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
</form>
<table align="left" border="1" id="myDataTable" class="display">
	<div class="hd"><h2 align="center">Class wise House Count Report</h2>
	<thead>
	<tr>
		<td align="left"><b>Academic Year</b></td>
		<td align="left"><b>Class</b></td>
		<td align="left"><b>Section</b></td>
		<td align="left"><b>Ganga</b></td>
		<td align="left"><b>Yamuna</b></td>
		<td align="left"><b>Jhelum</b></td>
		<td align="left"><b>Narmada</b></td>
		<td align="left"><b>N/A</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$strengthArr}
  {if $strengthArr[sec].ganga > 0 || $strengthArr[sec].Yamuna > 0 || $strengthArr[sec].Jhelum > 0 || $strengthArr[sec].Narmada > 0 || $strengthArr[sec].NA > 0}
  <tr>
  	<td align="left">{$academicStartYear}-{$academicEndYear}</td>
    <td align="left">{$strengthArr[sec].class}</td>
    <td align="left">{$strengthArr[sec].section}</td>
    <td align="left">{$strengthArr[sec].ganga}</td>
    <td align="left">{$strengthArr[sec].Yamuna}</td>
    <td align="left">{$strengthArr[sec].Jhelum}</td>
    <td align="left">{$strengthArr[sec].Narmada}</td>
    <td align="left">{$strengthArr[sec].NA}</td>
  </tr>
  {/if}
  {/section}
  </tbody>
	<tfoot>
	 <tr>
	 	<th></th>
	 	<th></th>
	 	<th></th>
	 	<th align="left">{$countGanga1Total}</th>
	 	<th align="left">{$countGanga2Total}</th>
	 	<th align="left">{$countGanga3Total}</th>
	 	<th align="left">{$countGanga4Total}</th>
	 	<th align="left">{$countGanga5Total}</th>
	 </tr>
	</tfoot>
</table>
{/block}
{include file="./main.tpl"}
{block name="head"}
<script type="text/javascript">
function checkGrno()
{
	var nominalRollId = $("#nominalRollId").val();
	var dataString = "grNo=" +$("#grNo").val() + "&acdYear=" + $("#startDateYear").val();
	$.ajax({
		type : 'GET',
		url  : 'checkGrnoNominal.php',
		data :  dataString,
		success:function(data)
		{
			if(data == 1 && nominalRollId == 0)
			{
				alert("This Grno Allready Entered");
				$('#grNo').val('');
				$('#studentName').val('');
				$('#grNo').focus();
			}
	  }
	});
	
	$.ajax({
		type : 'GET',
		url  : 'setStudName.php',
		data :  dataString,
		success:function(data)
		{
      $('#studentName').val(data);
	  }
	});
}

</script>
{/block}
{block name="body"}
<form action="nominalRollEntry.php" method="POST">
<table align="center" border="1">
<input type="hidden" value="{$nominalRollId}" id="nominalRollId" name="nominalRollId">
<br><br><br>
{if $nominalRollId > 0}
<div class="hd"><h2 align="center">Edit Nominal Roll</h2></div>
{else}
<div class="hd"><h2 align="center">Nominal Roll Entry</h2></div>
{/if}
<br><br>
<tr>
  <td align="center" colspan="2" style="color:red; font-size:18px;"><b>{$confirm}</b></td>
</tr>
<tr>
  <td class="table2 form01">Acadamic Year</td>
  {if $nominalRollId > 0}
  <td class="table2 form01">
    <select name="startYear" id="startDateYear" DISABLED >
      {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYear}
    </select>
  </td>
  {else}
  <td class="table2 form01">
    <select name="startYear" id="startDateYear">
      {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYear}
    </select>
  </td>
  {/if}
</tr>
<tr>
	<td align="left" class="table2 form01">Gr No : </td>
	<td class="table2 form01"><input type="text" name="grNo" id="grNo" value="{$grNo}" onblur="checkGrno();" autofocus required /></td>
</tr>
<tr>
	<td align="left" class="table2 form01">Student Name : </td>
	<td class="table2 form01"><input type="text" name="" id="studentName" value="{$studentName}" REQUIRED DISABLED /></td>
</tr>
<tr>
  <td class="table2">Class</td>
  <td class="table2 form01">
  	<select name="class" autofocus="autofocus" required >
	    <option value="">Select class</option>
	    {html_options values=$cArray.className output=$cArray.className selected=$class}
	  </select>
  </td>
</tr>
<tr>
  <td class="table2">Section</td>
  <td class="table2 form01">
  	<select name="section">
      <option value="0">Select Section</option>
      {html_options values=$secArrOut output=$secArrOut selected=$section}
	  </select>
	</td>
</tr>
<tr>
	<td align="left" class="table2 form01">Roll No : </td>
	<td class="table2 form01"><input type="text" name="rollNo" value="{$rollNo}"></td>
</tr>
<tr>
	<td align="left" class="table2 form01">Board Registrion Id : </td>
	<td class="table2 form01"><input type="text" name="boardRegistrionId" value="{$boardRegistrionId}"></td>
</tr>
<tr>
	<td align="left" class="table2 form01">Board Roll No : </td>
	<td class="table2 form01"><input type="text" name="boardRollNo" value="{$boardRollNo}"></td>
</tr>
<tr>
	<td colspan="2" align="center" class="table2"><input type="submit" name="submit" class="newSubmitBtn" value="Save">
</tr>
</table>
</form>
{/block}
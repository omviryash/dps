{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script src="media2/js/jquery.dataTables.columnFilter.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable(
  {
    "bProcessing": true,
		"bServerSide": true,
		"bAutoWidth": false,
    "aLengthMenu": [[100, 500, 1000, 5000], [100, 500, 1000, 5000]],
  	"iDisplayLength": 100,
		"sPaginationType":"full_numbers",
    "aaSorting":[[0, "ASC"]],
		"bJQueryUI":true,
		"sAjaxSource": "./studentListAjax.php?activated=" + $('#activated').val()
  });
  /* Large Image Show Start*/

	
	/* Large Image Show End*/
});

$(document).ready(function(){
$(".studentImage").hover(function(e){
	alert('');
  if($(this).attr("alt") != "")
  {
    var img = "./studentImage/"+ $(this).attr("alt");
    $("#large").css("top",(e.pageY+5)+"px")
	             .css("left",(e.pageX+5)+"px")                  
	             .html("<img src='"+img+"' alt='Large Image' />")
	             .fadeIn("slow");
	}
}, function(){
  $("#large").fadeOut("fast");
});
});
</script>
{/block}
{block name="body"}
</br></br>
<input type="hidden" name="activated" id="activated" value="{$activated}">
<form name="formGet" method="GET" action="studentAllList.php">
<table align="center" border="1">
	<tr>
		<td class="table2 form01 omAttend">
		  <select name="activated" autofocus="autofocus">
		    <option value="0">Select</option>
		    {html_options values=$activeValues output=$activeOutPut selected=$activated}
		  </select>
	  </td>
    <td>
      <input type="submit" name="go" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
</form>

<table align="left" border="1" id="myDataTable" class="display dataTables_length">
	<div class="hd"><h2 align="center">Student List</h2>
	<thead>
	<tr>
		<td align="left"><b>G. R. No.</b></td>
		<td align="left"><b>Name</b></td>
		<td align="left"><b>Father Name</b></td>
		<td align="left"><b>Mother Name</b></td>
		<td align="left"><b>Activated</b></td>
		<td align="left"><b>Date Of Birth</b></td>
		<td align="left"><b>Joined In Class</b></td>
		<td align="left"><b>Joining Date</b></td>
		<td align="left"><b>House Name</b></td>
		<td align="left"><b>Gender</b></td>
		<td align="left"><b>Current Address</b></td>
		<td align="left"><b>Residence Phone1</b></td>
		<td align="left"><b>Residence Phone2</b></td>
		<!--td align="left"><b>SMS Mobile</b></td -->
		<td align="left"><b>Email</b></td>
		<td align="left"><b>Permanent Address</b></td>
		<td align="left"><b>Religion</b></td>
		<td align="left"><b>BloodGroup</b></td>		
		<td align="left"><b>Aadhaar</b></td>		
  </tr>
  </thead>
  <!--thead>
  <tr>
		<th align="left"><b>Name</b></th>
		<th align="left"><b>Father Name</b></th>
		<th align="left"><b>G. R. No.</b></th>
		<th align="left"><b>Date Of Birth</b></th>
		<th align="left"><b>Joined In Class</b></th>
		<th align="left"><b>Joining Date</b></th>
		<th align="left"><b>House Name</b></th>
		<th align="left"><b>Gender</b></th>
		<th align="left"><b>Current Address</b></th>
		<th align="left"><b>Residence Phone1</b></th>
		<th align="left"><b>Residence Phone2</b></th>
		<th align="left"><b>Email</b></th>
		<th align="left"><b>Permanent Address</b></th>
		<th align="left"><b>Religion</b></th>
		<th align="left"><b>Student Image</b></th>
		<th align="left"><b>Father Image</b></th>
		<th align="left"><b>Mother Image</b></th>
	</tr>
  </thead -->
  <tbody>
  {section name="sec" loop=$stdArray}
  <tr>
  	<td align="left">{$stdArray[sec].studentName}</td>
    <td align="left">{$stdArray[sec].fatherName}</td>
    <td align="left">{$stdArray[sec].grNo}</td>
    <td align="left">{$stdArray[sec].dateOfBirth}</td>
    <td align="left">{$stdArray[sec].joinedInClass}</td>
    <td align="left">{$stdArray[sec].joiningDate}</td>
    <td align="left">{$stdArray[sec].gender}</td>
    <td align="left">{$stdArray[sec].currentAddress}</td>
    <td align="left">{$stdArray[sec].residencePhone1}</td>
    <td align="left">{$stdArray[sec].residencePhone2}</td>
    <td align="left">{$stdArray[sec].studentEmail}</td>
    <td align="left">{$stdArray[sec].permanentAddress}</td>
    <td align="left">{$stdArray[sec].religion}</td>
    <td align="left">{$stdArray[sec].bloodGroup}</td>
    <td align="left">{$stdArray[sec].aadhaar}</td>
    
  </tr>
  {/section}
  </tbody>
</table>
{/block}
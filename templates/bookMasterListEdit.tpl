{include file="./main.tpl"}
{block name=head}
<style type="text/css" media="screen">
@import "./media/css/demo_table_jui.css";
@import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
input
{ 
  border:1px solid #000;
}
</style>
<script src="./media1/js/jquery-1.4.4.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.jeditable.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
	$('#bookAccessionNo').focus();
	$('#example').dataTable({
		"bJQueryUI":true
	});
	/* Editable Start*/
	var oTable = $('#example').dataTable();
     
  /* Apply the jEditable handlers to the table */
  oTable.$('td').editable( './updateStudentData.php', {
      "submitdata": function ( value, settings ) {
          return {
              "row_id": this.parentNode.getAttribute('td.id'),
              "column": oTable.fnGetPosition( this )[2]
          };
      },
      "height": "20px",
      "width": "400"
  });
  
  /* Editable End*/
});
</script>
{/block}
{block name="body"}
</br></br>
<form name="formGet" method="GET" action="bookMasterListEdit.php">
<table align="center" border="1">
	<tr>
		<td>
      Student Book Ac No : - <input type="text" name="bookAccessionNo" id="bookAccessionNo" placeholder="Enter Book Ac No">
    </td>
    <td>
      <input type="submit" name="" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
<form>
</br></br>
<table align="left" border="1" id="example" class="display dataTables_length">
	<div class="hd"><h2 align="center">Book Master List</h2>
	<thead>
	<tr>
		<td align="left"><b>Edit</b></td>
		<td align="left"><b>Book Type</b></td>
		<td align="left"><b>Book Accession No</b></td>
		<td align="left"><b>ISBN</b></td>
		<td align="left"><b>Book Title</b></td>
		<td align="left"><b>Auther1</b></td>
		<td align="left"><b>Auther2</b></td>
		<td align="left"><b>Location</b></td>
		<td align="left"><b>Price</b></td>
		<td align="left"><b>PublisherId</b></td>
		<td align="left"><b>Language</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$classArray}
  <tr>
  	<th align="left"><a href="bookEntry.php?bookMasterId={$classArray[sec].bookMasterId}">Edit</a></th>
  	<th>{$classArray[sec].bookType}</th>
  	<th>{$classArray[sec].bookAccessionNo}</th>
  	<th>{$classArray[sec].ISBN}</th>  	
  	<td>{$classArray[sec].bookTitle}</td>
    <td>{$classArray[sec].author1}</td>
    <td>{$classArray[sec].author2}</td>
    <td>{$classArray[sec].location}</td>
    <td>{$classArray[sec].price}</td>
    <td>{$classArray[sec].publisherId}</td>
    <td>{$classArray[sec].language}</td>
  </tr>
  {/section}
  </tbody>
</table>
{/block}
{include file="./main.tpl"}
{block name="body"}
<form action="bonafideEntry.php" method="POST">
<table align="center" border="1">
<input type="hidden" value="{$nominalRollId}" id="nominalRollId" name="nominalRollId">
<br><br><br>
<div class="hd"><h2 align="center">Bonafide Create</h2></div>
<br><br>
<tr>
  <td align="center" colspan="2" style="color:red; font-size:18px;"><b>{$confirm}</b></td>
</tr>
<tr>
  <td class="table2 form01">Acadamic Year</td>
  <td class="table2 form01">
    <select name="startYear" id="startDateYear" DISABLED >
      {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYear}
    </select>
  </td>
</tr>
<tr>
	<td align="left" class="table2 form01">Gr No : </td>
	<td class="table2 form01"><input type="text" name="grNo" id="grNo" value="{$grNo}" onblur="checkGrno();" DISABLED /></td>
</tr>
<tr>
	<td align="left" class="table2 form01">Student Name : </td>
	<td class="table2 form01"><input type="text" name="" id="studentName" value="{$studentName}" DISABLED /></td>
</tr>
<tr>
  <td class="table2">Class</td>
  <td class="table2 form01">
  	<select name="class" DISABLED >
	    <option value="">Select class</option>
	    {html_options values=$cArray.className output=$cArray.className selected=$class}
	  </select>
  </td>
</tr>
<tr>
  <td class="table2">Section</td>
  <td class="table2 form01">
  	<select name="section" DISABLED >
      <option value="0">Select Section</option>
      {html_options values=$secArrOut output=$secArrOut selected=$section}
	  </select>
	</td>
</tr>
<tr>
	<td align="left" class="table2 form01">Roll No : </td>
	<td class="table2 form01"><input type="text" name="rollNo" value="{$rollNo}" DISABLED ></td>
</tr>
<tr>
	<td align="left" class="table2 form01">Date : </td>
	<td class="table2 form01">
		{html_select_date prefix="bonaDate" start_year="-25" end_year="+25" field_order="DMY" time=$bonafideDate day_value_format="%02d"}
	</td>
</tr>
<tr>
	<td align="left" class="table2 form01">W. E. F. Date : </td>
	<td class="table2 form01">
		{html_select_date prefix="wefDate" start_year="-25" end_year="+25" field_order="DMY" time=$wefDate day_value_format="%02d"}
	</td>
</tr>
<tr>
	<td colspan="2" align="center" class="table2"><input type="submit" name="submit" class="newSubmitBtn" value="Save">
</tr>
</table>
</form>
{/block}
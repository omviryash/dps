{include file="./main.tpl"}
<br>
<br>
<br>
{block name="head"}
{/block}
{block name="body"}
<form name="form1" method="POST" action="timeTableEntry.php">
<table align="center" border="0" class="table2">
<div class="hd"><h2 align="center">Time Table Entry</h2>
<tr>
  <input type="" name="timeTableId" value="{$timeTableId}">
</tr>
<tr>
  <td align="center" colspan="2" style="color:red; font-size:18px;"><b>{$confirm}</b></td>
</tr>
<tr>
	<td class="table2">Academic Year</td>
	<td class="table2 form01">
	  <select name="startYear">
	    {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYear}
	  </select>
	</td>
</tr>
<tr>
  <td class="table2">Class</td>
  <td class="table2 form01">
  	<select name="class" autofocus="autofocus" required >
	    <option value="">Select class</option>
	    {html_options values=$cArray.className output=$cArray.className selected=$class}
	  </select>
  </td>
</tr>
<tr>
  <td class="table2">Section</td>
  <td class="table2 form01">
  	<select name="section">
      <option value="0">Select Section</option>
      {html_options values=$secArrOut output=$secArrOut selected=$section}
	  </select>
	</td>
</tr>
<tr>
  <td class="table2">Teacher</td>
  <td class="table2 form01">
  	<select name="employeeMasterId">
	    {html_options values=$tArr.employeeMasterId output=$tArr.name selected=$employeeMasterId}
	  </select>
  </td>
</tr>
<tr>
  <td class="table2">Subject</td>
  <td class="table2 form01">
  	<select name="subjectMasterId">
	    {html_options values=$sArr.subjectMasterId output=$sArr.subjectName selected=$subjectMasterId}
	  </select>
  </td>
</tr>
<tr>
  <td class="table2">WeekDay</td>
  <td class="table2 form01">
  	<select name="weekDay">
	    {html_options values=$weekOut output=$weekOut selected=$weekDay}
	  </select>
  </td>
</tr>
<tr>
  <td class="table2">Period No</td>
  <td class="table2 form01">
  	<select name="periodNo">
	    {html_options values=$periodOut output=$periodOut selected=$periodNo}
	  </select>
  </td>
</tr>
<tr>
  <td class="table2"></td>
  <td class="table2 form01" align="center"><input type="submit" name="Submit" class="button" value="Submit"></td>
</tr>
</table>
</form>
{/block}
{include file="./main.tpl"}
{block name=head}
<script type="text/javascript">
$(document).ready(function()
{
	$("#selectAllCheck").click(function()
	{
	  if($("#selectAllCheck").is(":checked"))
	  {
	  	$("input[id=techCheck]").attr('checked', 'checked');
	  }
	  else
	  {
	  	$("input[id=techCheck]").removeAttr('checked', 'checked');
	  }
	});
});

function setTemp(obj)
{
	var row = $(obj).parents('.mainRow');
	var smsTemplateId  = row.find('.smsTemplateId').val();
	var datastring = 'smsTemplateId=' + smsTemplateId;
	$.ajax({	
		type: 'GET',
		url: 'getTemp.php',
	  data: datastring,
	  success:function(data)
	  {
	  	$('#msgText').val(data);
    }
  });
}
</script>
{/block}
{block name="body"}
<form name="formGet" method="GET" action="sendSMSStudent.php">
<br><br><br>
<div class="hd"><h2 align="center">Send Sms</h2></div>
<br><br>
<table align="center">
	<tr>
		<td class="table2 form01">
		  <select name="class" autofocus="autofocus" >
		    <option value="All">All class</option>
		    {html_options values=$cArray.className output=$cArray.className selected=$class}
		  </select>
	  </td>
    <td class="table2 form01">
		  <select name="classSection">
		    <option value="All">All Section</option>
		    {html_options values=$secArrOut output=$secArrOut selected=$section}
		  </select>
	  </td>
	  <td class="table2 form01">
      <select name="startYear" id="startDateYear">
        {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYear}
      </select>
	  </td>
    <td>
      <input type="submit" name="submit" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
</form>
<form action="{$smarty.server.PHP_SELF}" name='user' method='POST'>
<table align="left" border="1" cellpadding='3' cellspacing='6' style="margin-left:20px;">
	<thead>
	<tr>
		<td  class="table1" align='center' >
			<input type="checkbox" name="selectAllCheck" id="selectAllCheck" onclick="checkAll();"/>
		</td>
		<th align="left" class="table1"><b>Gr No</b></th>
		<th align="left" class="table1"><b>Name</b></th>
		<th align="left" class="table1"><b>Father Name</b></th>
		<th align="left" class="table1"><b>Class/Section</b></th>
		<th align="left" class="table1"><b>Mobile No</b></th>
		<th align="left" class="table1"><b>Activated</b></th>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$stdArray}
  <tr>
  	<th align="left" class="table2" NOWRAP >
	    <input type="checkbox" name="studentMasterId[]" id="techCheck" value="{$stdArray[sec].studentMasterId}">
	  </th>
    <td align="left" class="table2" NOWRAP >{$stdArray[sec].grNo}</td>
    <td align="left" class="table2" NOWRAP >{$stdArray[sec].studentName}</td>
    <td align="left" class="table2" NOWRAP >{$stdArray[sec].fatherName}</td>
    <td align="left" class="table2" NOWRAP >{$stdArray[sec].class}-{$stdArray[sec].section}</td>
    <td align="left" class="table2" NOWRAP >{$stdArray[sec].smsMobile}</td>
    {if $stdArray[sec].activated == 'Y'}
      <td align="left" class="table2" NOWRAP >Yes</td>
    {else}
      <td align="left" class="table2" NOWRAP >No</td>
    {/if}
  </tr>
  {/section}
</table>
<table border='0' cellpadding='1' cellspacing='2' align='center'>
{section name="sec1" loop=$tempArray}
<tr class='mainRow'>
  <td class="table1" align="center"><input type='radio' name='smsTemplateId' class='smsTemplateId' value='{$tempArray[sec1].smsTemplateId}' onclick='setTemp(this);'></td>
  <td class="table1" align="center">{$tempArray[sec1].smsTemplateId}</td>
  <td class="table1" align="center">{$tempArray[sec1].template}</td>
</tr>
{/section}
</table>
</br>
<table border='0' cellpadding='1' cellspacing='2' align='center'>
<tr>
  <td class="table1" align="center" colspan="2"><font style="font-size: 18px"><b> Message </b></font></td>
</tr>
<tr>
  <td class="table1" align="center"><textarea name="msgText" id="msgText" rows="50" cols="80" style="font-size:12px"></textarea></td>
</tr>
<tr>
  <td class="table1" align="center"><input type="submit" name="techSub" id="techSub" value=" Submit "/></td>
</tr>
</table>
</form>
{/block}
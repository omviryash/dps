{include file="./main.tpl"}
{block name="body"}
<table align="center" border="1" cellpadding="3" cellspacing="2">
	<div class="hd"><h2 align="center">My Profile</h2>
	
	<!-- tr>
		<td align="left" class="table1"> Photo  </td>
		<td align="left" class="table1">: </td>
		<td align="left" class="table1"> <img height="150" src=""></td>
	</tr -->	
	<tr>
		<td align="left" class="table1"><b>Name  </b></td>
		<td align="left" class="table1"><b> : </b></td>
		<td align="left" class="table2">{$empArray.name}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Date Of Birth</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$empArray.dateOfBirth}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Joining Date</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$empArray.joiningDate}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Gender</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$empArray.gender}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Current Address</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$empArray.currentAddress}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Residence Phone</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$empArray.residenceTelephone}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Permanent Address</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$empArray.permanentAddress}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Mobile</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$empArray.mobile}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Phone 1</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$empArray.phone1}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Phone 2</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$empArray.phone2}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Email</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$empArray.email}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Marital Status</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$empArray.marital}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Account No</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$empArray.accountNo}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Pan No</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$empArray.panNo}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Qualification</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$empArray.qualification}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Experience</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$empArray.experience}</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Blood Group</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2">{$empArray.bloodGroup}</td>
  </tr>
</table>
{/block}
{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script src="media2/js/jquery.dataTables.columnFilter.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable(
  {
    "bProcessing": true,
		"bServerSide": true,
		"bAutoWidth": false,
    "aLengthMenu": [[100, 500, 1000], [100, 500, 1000]],
  	"iDisplayLength": 100,
		"sPaginationType":"full_numbers",
    "aaSorting":[[1, "DESC"]],
		"bJQueryUI":true,
		"sAjaxSource": "./bookMasterListAjax.php"
  });
});

</script>
{/block}
{block name="body"}
</br></br>
<table align="left" border="1" id="myDataTable" class="display dataTables_length">
	<div class="hd"><h2 align="center">Book Master List</h2>
	<thead>
	<tr>
		<td align="left"><b>Book Type</b></td>
		<td align="left"><b>Book Accession No</b></td>
		<td align="left"><b>ISBN</b></td>
		<td align="left"><b>Book Title</b></td>
		<td align="left"><b>Author 1</b></td>
		<td align="left"><b>Author 2</b></td>
		<td align="left"><b>Location</b></td>
		<td align="left"><b>Price</b></td>
		<td align="left"><b>PublisherId</b></td>
		<td align="left"><b>Language</b></td>
  </tr>
  </thead>
  <tbody>
 
  </tbody>
</table>
{/block}
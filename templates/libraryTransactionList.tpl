{include file="./main.tpl"}
{block name="head"}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script src="./media1/js/jquery-1.4.4.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.jeditable.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[-1, 10, 20, 30, 40, 50], ["All", 10, 20, 30, 40, 50]],
  	"iDisplayLength": 500,
		"bJQueryUI":true
  });
  $(".omAttend").change(function()
  {
  	$('.newGoBtnClick').click();
  });
  
  /* Editable Start*/
	var oTable = $('#myDataTable').dataTable();
     
  /* Apply the jEditable handlers to the table */
  oTable.$('th').editable( './updateRollNoData.php', {
      "submitdata": function ( value, settings ) {
          return {
              "row_id": this.parentNode.getAttribute('th.id'),
              "column": oTable.fnGetPosition( this )[2]
          };
      },
      "height": "20px",
      "width": "100"
  });
  
  /* Editable End*/
  
  $("#startDateYear").change(function()
  {
    var yearExtra  = $('#startDateYear').val();
		var endDate    = yearExtra;
		var datastring = 'endDate=' + endDate;
		$.ajax({	
			type: 'GET',
			url: 'endYear.php',
		  data: datastring,
		  success:function(data)
		  {
		  	$('#endDateYear').val(data);
	    }
	  });
  });
  $('.checkAll').click(function () {
    $('.checkboxAll').attr('checked', this.checked);
  });
  
  var hideEmpStdName = $('.hideEmpStd').val();
	if(hideEmpStdName == 'Staff')
	{
		$('#hideClass').hide();
		$('#hideSection').hide();
		$('#nameStaff').show();
	}
  else
  {
		$('#nameStaff').hide();
		$('#hideClass').show();
		$('#hideSection').show();
	}
});

function checkbox(obj)
{
	var row = $(obj).parents('.gradeRow');
	if(row.find(".testName").attr("checked") == true)
  {
    row.find(".testNameHidden").val('Yes');
  }
  else
  {
    row.find(".testNameHidden").val('No');
  }
}

function setFinePay(objPay)
{
	var row = $(objPay).parents('.gradeRow');
	if(row.find(".finePay").attr("checked") == true)
  {
    row.find(".finePayHidden").val('Paid');
  }
  else
  {
    row.find(".finePayHidden").val('Panding');
  }
  alert(rowfind(".finePayHidden").val());
}
function checkboxAll()
{
	if($(".checkAll").attr("checked") == true)
  {
    $(".testNameHidden").val('Yes');
  }
  else
  {
    $(".testNameHidden").val('No');
  }
}

//function setFineAll()
//{
//	$('.getFine').val(0);
//}
function setFine(obj)
{
	var row = $(obj).parents('.gradeRow');
  var dayExtra    = row.find('#fromDateDay').val();
  var monthExtra  = row.find('#fromDateMonth').val();
  var yearExtra   = row.find('#fromDateYear').val();
  var issueDate = row.find('.issueDate').val();
	var fullDate = yearExtra+'-'+monthExtra+'-'+dayExtra;
	var dataString = "returnableDate=" + fullDate + "&issueDate=" + issueDate;
  $.ajax(
  {
    type: "GET",
    url: "setFine.php",
    data: dataString,
    success:function(data)
    {
      row.find('.getFine').val(data);
      row.find('.fullDate').val(fullDate);
    }
  });
}
</script>
{/block}
{block name="body"}
</br></br>
<link rel="stylesheet" href="./css/buttonStyle.css" type="text/css" />
<form name="formGet" method="GET" action="libraryTransactionList.php">
<table align="center">
	<tr>
		<td class="table2 form01">
		  <select name="staff" autofocus="autofocus" class='omAttend hideEmpStd' onchange='hideEmpStd();'>
		    {html_options values=$staffOut output=$staffOut selected=$staff}
		  </select>
	  </td>
	  <!--td class="table2">Staff Name</td>
	  <td class="table2 form01">
	  	<select name='nameStaff' id='nameStaff' class='omAttend'>
	  	  <option value=''>Select</option>
	  	  {html_options values=$dArray.name output=$dArray.name selected=$nameStaff}
	    </select>
		</td-->
		<!--td class="table2 form01">
		  <select name="bookAccessionNo" autofocus="autofocus" class='omAttend'>
		    <option value="">Select Book</option>
		    {html_options values=$bArray.bookAccessionNo output=$bArray.bookTitle selected=$bookAccessionNo}
		  </select>
	  </td-->
		<td class="table2 form01">
		  <select name="class" autofocus="autofocus" class='omAttend' id='hideClass'>
		    <option value="">Select class</option>
		    {html_options values=$cArray.className output=$cArray.className selected=$class}
		  </select>
	  </td>
    <td class="table2 form01">
		  <select name="classSection" class='omAttend' id='hideSection'>
		    <option value="0">Select Section</option>
		    {html_options values=$secArrOut output=$secArrOut selected=$section}
		  </select>
	  </td>
    <td>
      <input type="submit" name="submit" class="newGoBtn newGoBtnClick" value="Go">
    </td>
  </tr>
</table>
</form>
<!--form name="formGet" method="GET" action="libraryTransactionList.php">
<table align="center">
	<tr>
		<td class="table2 form01">
	    {html_select_date prefix="attendence" start_year="-25" end_year="+25" field_order="DMY" time=$attendDate day_value_format="%02d" display_days=true}
	  </td>
    <td>
      <input type="submit" name="submitNew" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
</form-->
<form name="form2" method="POST" action="libraryTransactionList.php">
<input type="hidden" name="attendDate" value="{$attendDate}">
<table align="center" border="1" id="myDataTable" class="display">  
  </br>
	<div class="hd"><h2 align="center">Book Return List</h2></div>
	</br>
	<thead>
	<tr>
		<td align="left" class="table1"><b>Edit</b></td>
		{if $staff == 'Staff'}
		  <td align="left" class="table1"><b>Employee Code</b></td>
		  <td align="left" class="table1"><b>Employee Name</b></td>
		{else}
		  <td align="left" class="table1"><b>GrNo</b></td>
		  <td align="left" class="table1"><b>Student Name</b></td>
		  <td align="left" class="table1"><b>Class</b></td>
		{/if}
		<td align="left" class="table1"><b>Book Ac No</b></td>
		<td align="left" class="table1"><b>Book Title</b></td>
		<td align="left" class="table1"><b>Issue Date</b></td>
		<td align="left" class="table1"><b>Scheduled Date of Return</b></td>
		<td align="left" class="table1"><b>Actual Date of Return</b></td>
		{if $staff != 'Staff'}
		<td align="left" class="table1"><b>Fine</b></td>
		{/if}
		<td align="left" class="table1"><b>Fine Pay</b></td>
		<td class="table1" align='center'>
			<input type="checkbox"  name="checkAll" id="checkAll" class="checkAll" onclick='checkboxAll(); setFine(this);'>
		</td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$attendenceArr}
  <tr class="gradeRow">
    </td>
    <td align="left" class="table2"><a href='libraryTransaction.php?libraryTransactionId={$attendenceArr[sec].libraryTransactionId}&checktype={$attendenceArr[sec].checktype}&employeeCode={$attendenceArr[sec].employeeCode}'>Edit</a></td>
    {if $staff == 'Staff'}
		  <td align="left" class="table2">{$attendenceArr[sec].employeeCode} <input type="hidden" name="libraryTransactionId[]" value="{$attendenceArr[sec].libraryTransactionId}"></td>
		  <td align="left" class="table2">{$attendenceArr[sec].name}</td>
		{else}
		  <td align="left" class="table2">{$attendenceArr[sec].grNo}
		  	<input type="hidden" name="libraryTransactionId[]" value="{$attendenceArr[sec].libraryTransactionId}">
		  	<input type="hidden" name="grNo[]" value="{$attendenceArr[sec].grNo}">
		  </td>
      <td align="left" class="table2">{$attendenceArr[sec].studentName}</td>
      <td align="left" class="table2">{$attendenceArr[sec].class} - {$attendenceArr[sec].section}</td>
    {/if}
    <td align="left" class="table2">{$attendenceArr[sec].bookAccessionNo}
    	<input type="hidden" name="bookAccessionNo[]" value="{$attendenceArr[sec].bookAccessionNo}">
    </td>
    <td align="left" class="table2">{$attendenceArr[sec].bookTitle}</td>
    <td align="left" class="table2">{$attendenceArr[sec].issueDate}
    	<input type="hidden" name="" class="issueDate" value="{$attendenceArr[sec].issueDate}">
    </td>
    <td align="left" class="table2">{$attendenceArr[sec].returnableDate|date_format:"%d-%m-%Y"}</td>
    <td align="left" class="table2" onchange="setFine(this);" NOWRAP >
    	{html_select_date day_extra="id=\"fromDateDay\"" month_extra="id=\"fromDateMonth\"" year_extra="id=\"fromDateYear\"" prefix="returnableDate" start_year="-25" end_year="+25" field_order="DMY" time=$today day_value_format="%02d" display_days=true}
    	<input type="hidden" name="returnableDate[]" class="fullDate" value="{$today}">
    </td>
    {if $staff != 'Staff'}
    <td align="left" class="table2">
    	<input type="text" name="fine[]" value="{$attendenceArr[sec].fine}" class="getFine" READONLY size="5" >
    </td>
    {/if}
    <td align="left" class="table2">
    	<input type="checkbox" class="finePay" onclick='setFinePay(this);' CHECKED >
    	<input type="hidden" name="finePay[]" value="Paid" class="finePayHidden">
    </td>
    <td align="left" class="table2">
    	<input type="checkbox" class="testName checkboxAll" onclick='checkbox(this); setFine(this);' >
    	<input type="hidden" name="returnBook[]" value="No" class="testNameHidden">
    	<!--select name="returnBook[]" >
	      {html_options values=$lateOut output=$lateOut selected=$attendenceArr[sec].returnBook}
	    </select-->
    </td>
  </tr>
  {/section}
  </tbody>
  <tfoot>
  <tr>
    <td align="center" colspan="9" class="table2"><input type="submit" name="submitTaken" class="newSubmitBtn" value="Save"></td>
  </tr>
  </tfoot>
</table>
</form>
{/block}
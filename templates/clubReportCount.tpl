{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[-1, 100, 500, 1000], ['All', 100, 500, 1000]],
  	"iDisplayLength": 100,
		"bJQueryUI":true
  });
});
</script>
{/block}
{block name="body"}
</br></br>
<center>
<form name="formGet" id="formGet" method="GET" action="clubReportCount.php">
<table align="center" border="1">
	<div class="hd"><h2 align="center">Club Report Count</h2></div>
	</br></br>
	<tr>
		<td class="table2 form01">
		  <select name="startYear" class="submitClick">
		    {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYearSelected}
		  </select>
		</td>
		<td>
	    <input type="submit" name="go" class="newGoBtn" value="go">
	  </td>
	</tr>
</table>
</form>
</center>
</br></br>
<table align="left" border="1" id="myDataTable" class="display">
	<thead>
	<tr>
		<td align="left"><b>Subject Name</b></td>
	  <td align="left"><b>Term 1</b></td>
	  <td align="left"><b>Term 2</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$attendenceArr}
  <tr>
  	<td align="left">{$attendenceArr[sec].subjectName}</td>
    <td align="left">{$attendenceArr[sec].countP}</td>
    <td align="left">{$attendenceArr[sec].countA}</td>
  </tr>
 {/section}
 </tbody>
 <tfoot>
   <tr>
   	<th></th>
   	<th align="left">{$countTotalP}</th>
   	<th align="left">{$countTotalA}</th>
   </tr>
 </tfoot>
</table>
{/block}
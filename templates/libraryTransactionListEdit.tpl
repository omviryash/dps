{include file="./main.tpl"}
{block name="head"}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script src="./media1/js/jquery-1.4.4.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.jeditable.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[-1, 10, 20, 30, 40, 50], ["All", 10, 20, 30, 40, 50]],
  	"iDisplayLength": 500,
		"bJQueryUI":true
  });
  $(".omAttend").change(function()
  {
  	$('.newGoBtnClick').click();
  });
  
  /* Editable Start*/
	var oTable = $('#myDataTable').dataTable();
     
  /* Apply the jEditable handlers to the table */
  oTable.$('th').editable( './updateFineData.php', {
      "submitdata": function ( value, settings ) {
          return {
              "row_id": this.parentNode.getAttribute('th.id'),
              "column": oTable.fnGetPosition( this )[2]
          };
      },
      "height": "20px",
      "width": "100"
  });
  
  /* Editable End*/
  
  $("#startDateYear").change(function()
  {
    var yearExtra  = $('#startDateYear').val();
		var endDate    = yearExtra;
		var datastring = 'endDate=' + endDate;
		$.ajax({	
			type: 'GET',
			url: 'endYear.php',
		  data: datastring,
		  success:function(data)
		  {
		  	$('#endDateYear').val(data);
	    }
	  });
  });
  
  var hideEmpStdName = $('.hideEmpStd').val();
	if(hideEmpStdName == 'Staff')
	{
		$('#hideClass').hide();
		$('#hideSection').hide();
		$('#nameStaff').show();
	}
  else
  {
		$('#nameStaff').hide();
		$('#hideClass').show();
		$('#hideSection').show();
	}
});


</script>
{/block}
{block name="body"}
</br></br>
<link rel="stylesheet" href="./css/buttonStyle.css" type="text/css" />
<form name="formGet" method="GET" action="libraryTransactionListEdit.php">
<table align="center">
	<tr>
		<td class="table2 form01">
		  <select name="staff" autofocus="autofocus" class='omAttend hideEmpStd' onchange='hideEmpStd();'>
		    {html_options values=$staffOut output=$staffOut selected=$staff}
		  </select>
	  </td>
	  <td class="table2 form01">
	  	<select name='nameStaff' id='nameStaff' class='omAttend'>
	  	  <option value=''>Select Staff</option>
	  	  {html_options values=$dArray.name output=$dArray.name selected=$nameStaff}
	    </select>
		</td>
		
		<td class="table2 form01">
		  <select name="class" autofocus="autofocus" class='omAttend' id='hideClass'>
		    <option value="">Select Class</option>
		    {html_options values=$cArray.className output=$cArray.className selected=$class}
		  </select>
	  </td>
    <td class="table2 form01" >
		  <select name="classSection" class='omAttend' id='hideSection'>
		    <option value="0">Select Section</option>
		    {html_options values=$secArrOut output=$secArrOut selected=$section}
		  </select>
	  </td>
	<td class="table2 form01">
	  <select name="startYear">
	    {html_options values=$dateArrVal output=$dateArrOut selected=$accStartDateSelected}
	  </select>
	</td>
    <td>
      <input type="submit" name="submit" class="newGoBtn newGoBtnClick" value="Go">
    </td>
  </tr>
</table>
</form>

<form name="form2" method="POST" action="libraryTransactionListEdit.php">
<input type="hidden" name="attendDate" value="{$attendDate}">
<table align="center" border="1" id="myDataTable" class="display">  
  </br>
	<div class="hd"><h2 align="center">Library Transaction List</h2></div>
	</br>
	<thead>
	<tr>
		<td align="left" class="table1"><b>Edit</b></td>
		{if $staff == 'Staff'}
		  <td align="left" class="table1"><b>Employee Code</b></td>
		  <td align="left" class="table1"><b>Employee Name</b></td>
		{else}
		  <td align="left" class="table1"><b>GrNo</b></td>
		  <td align="left" class="table1"><b>Student Name</b></td>
		  <td align="left" class="table1"><b>Class</b></td>
		{/if}
		<td align="left" class="table1"><b>Book Ac No</b></td>
		<td align="left" class="table1"><b>Book Title</b></td>
		<td align="left" class="table1"><b>Issue Date</b></td>
		<td align="left" class="table1"><b>Return Date</b></td>
		{if $staff != 'Staff'}
		<td align="left" class="table1"><b>Fine</b></td>
		<td align="left" class="table1"><b>Fine Pay</b></td>
		{/if}
		<td align="left" class="table1"><b>Return</b></td>
    
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$attendenceArr}
  <tr class="gradeRow">
		<td align="left" class="table2"><a href='libraryTransaction.php?libraryTransactionId={$attendenceArr[sec].libraryTransactionId}&checktype={$attendenceArr[sec].checktype}&employeeCode={$attendenceArr[sec].employeeCode}'>Edit</a></td>
		{if $staff == 'Staff'}
		  <td align="left" class="table2">{$attendenceArr[sec].employeeCode}</td>
		  <td align="left" class="table2">{$attendenceArr[sec].name}</td>
		{else}
		  <td align="left" class="table2">{$attendenceArr[sec].grNo}</td>
      <td align="left" class="table2">{$attendenceArr[sec].studentName}</td>
      <td align="left" class="table2">{$attendenceArr[sec].class} - {$attendenceArr[sec].section}</td>
    {/if}
    <td align="left" class="table2">{$attendenceArr[sec].bookAccessionNo}</td>
    <td align="left" class="table2">{$attendenceArr[sec].bookTitle}</td>
    <td align="left" class="table2">{$attendenceArr[sec].issueDate}</td>
    {if $attendenceArr[sec].returnableDate == '00-00-0000'}
    <td align="left" class="table2">Not Return</td>
    {else}
    <td align="left" class="table2">{$attendenceArr[sec].returnableDate}</td>
    {/if}
    {if $staff != 'Staff'}
    <td align="left" class="table2">{$attendenceArr[sec].fine}</td>
    <td align="left" class="table2">{$attendenceArr[sec].finePay}</td>
    {/if}
    {if $attendenceArr[sec].returnBook == 'No'}
    <td align="left" class="table2">Not Returned</td>
    {else}
    <td align="left" class="table2">Returned</td>
    {/if}
  </tr>
  {/section}
  </tbody>
</table>
</form>
{/block}
{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script src="./media1/js/jquery-1.4.4.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.jeditable.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[-1, 25, 50, 100, 500, 1000], ['All', 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 25,
		"bJQueryUI":true
  });
  
  $('.checkAll').click(function () {
    $('.checkboxAll').attr('checked', this.checked);
  });
});

function checkbox(obj)
{
	var row = $(obj).parents('.gradeRow');
	if(row.find(".testName").attr("checked") == true)
  {
    row.find(".testNameHidden").val('Yes');
  }
  else
  {
    row.find(".testNameHidden").val('No');
  }
}

function checkboxAll()
{
	if($(".checkAll").attr("checked") == true)
  {
    $(".testNameHidden").val('Yes');
  }
  else
  {
    $(".testNameHidden").val('No');
  }
}
</script>
{/block}
{block name="body"}
</br></br>
<form name="formGet" method="GET" action="onlineTestSchedule.php">
<table align="center">
	<tr>
		{if $s_userType == 'Administrator'}
		<td class="table2 form01">
		  <select name="class" autofocus="autofocus" required >
		    <option value="">Select class</option>
		    {html_options values=$cArray.className output=$cArray.className selected=$class}
		  </select>
	  </td>
	  <td class="table2 form01">
		  <select name="subjectMasterId" >
      	<option value="">Subject</option>
        {html_options values=$clubArr.subjectMasterId output=$clubArr.subjectName selected=$subjectMasterId}
      </select>
		</td>
		{else}
		<td class="table2 form01">
		  <select name="subjectAltId" class="omAttend">
			  <option value="">Select Class/Subject</option>
        {html_options values=$scdArray.subjectAltId output=$scdArray.subjectDtl selected=$subjectAltId}
      </select>
    </td>
		{/if}
	  <td class="table2 form01">
		  <select name="startYear">
		    {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYearSelected}
		  </select>
		</td>
    <td>
      <input type="submit" name="submit" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
</form>
<form name="form1" method="POST" action="onlineTestSchedule.php">
<table align="center">
	<tr>
		<td class="table1 form01">Questions For Class : </td>
		<td class="table2 form01">
			<input type="hidden" name="subjectAltId" value="{$subjectAltId}">
			<select name="classNew" autofocus="autofocus" required >
		    <option value="">Select class</option>
		    {html_options values=$cArray.className output=$cArray.className selected=$class}
		  </select>
			<input type='hidden' name='subjectMasterId' value='{$subjectMasterId}'>
		<td class="table1 form01">Date : </td>
		<td class="table2 form01">
      {html_select_date day_extra="id=\"fromDateDay\"" month_extra="id=\"fromDateMonth\"" year_extra="id=\"fromDateYear\"" prefix="scheduleDate" start_year="-25" end_year="+25" field_order="DMY" day_value_format="%02d" display_days=true}
		</td>
  </tr>
</table>
<table align="left" border="1" id="myDataTable" class="display">
	<div class="hd"><h2 align="center">Schedule Online Test</h2>
	<thead>
	<tr>
		<td align="left"><b>Sr No</b></td>
		<td align="left"><b>Class</b></td>
		<td align="left"><b>Subject</b></td>

		<td class="table1" align='center'>
			<input type="checkbox"  name="checkAll" id="checkAll" class="checkAll" onclick='checkboxAll();'>
		</td>
		<td align="left"><b>Question</b></td>
		<!--td align="left"><b>Option1</b></td>
		<td align="left"><b>Option2</b></td>
		<td align="left"><b>Option3</b></td>		
		<td align="left"><b>Option4</b></td -->		
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$nominalArr}
  <tr class='gradeRow'>
    <td align="left">{$smarty.section.sec.rownum}</td>
    <td align="left">{$nominalArr[sec].class}</td>
    <td align="left">{$nominalArr[sec].subjectName}</td>
  
	  <td align="left" class="table2">
    	<input type="checkbox" class="testName checkboxAll" onclick='checkbox(this);'>
    	<input type="hidden" name="scheduled[]" value="No" class="testNameHidden">
    </td>
      <td class="table2 form01">
      <input type="hidden" name="onlineTestId[]" value="{$nominalArr[sec].onlineTestId}">
      {$nominalArr[sec].question}
	  </td>
  </tr>
  {/section}
  </tbody>
  <tfoot>
	<tr>
		<td align="left"></td>
		<td align="left"></td>
		<td align="left"></td>
		<td align="left"></td>
		<td align="left"><input type="submit" name="submit" class="newSubmitBtn" value="Save"></td>
  </tr>
  </tfoot>
</table>
</form>
{/block}
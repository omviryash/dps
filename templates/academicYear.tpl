{include file="./main.tpl"}
{block name="head"}
<script type="text/javascript">
$(document).ready(function() {
  $("#startDateYear").change(function()
  {
    var yearExtra  = $('#startDateYear').val();
		var endDate    = yearExtra;
		var datastring = 'endDate=' + endDate;
		$.ajax({	
			type: 'GET',
			url: 'endYear.php',
		  data: datastring,
		  success:function(data)
		  {
		  	$('#endDateYear').val(data);
	    }
	  });
  });
});
</script>
{/block}
<form name="form1" method="POST" action="academicYear.php">
{block name="body"}
<table align="center" border="0" class="table2">
<div class="hd"><h2 align="center">Academic Year</h2>
<tr>
  <td class="table2">Acadamic Year</td>
  <td class="table2 form01">{html_select_date year_extra="id=\"startDateYear\"" prefix='Start' start_year='-1'
   end_year='+1' display_days=false display_months=false}
   <input type="text" id="endDateYear" style="width:60px; height:18px;" required /></td>
</tr>
<tr>
  <td class="table2"></td>
  <td class="table2 form01" align="center"><input type="submit" name="Submit" class="button" value="Submit"></td>
  <td class="table2"></td>
</tr>
</table>
</form>
<table align="center" border="2">
	<h2 align="center">Class List</h2>
	<tr>
		<td align="center"><b>Academic Year</b></td>
  </tr>
  {section name="sec" loop=$acadamicArray}
  <tr>
    <td align="left">{$acadamicArray[sec].acadamicStartYear} - {$acadamicArray[sec].acadamicEndYear}</td>
  </tr>
  {/section}
</table>
{/block}
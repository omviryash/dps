{include file="./main.tpl"}
<br>
<br>
<br>
{block name="head"}
{/block}
{block name="body"}
<form name="form1" method="POST" action="feeStructure.php">
<table align="center" border="0" class="table2">
<div class="hd"><h2 align="center">Fee Structure Entry</h2>
<tr>
  <input type="hidden" name="feeStructureId" value="{$feeStructureId}">
</tr>
<tr>
  <td align="center" colspan="2" style="color:red; font-size:18px;"><b>{$confirm}</b></td>
</tr>
<tr>
	<td class="table2">Academic Year</td>
	<td class="table2 form01">
	  <select name="startYear">
	    {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYear}
	  </select>
	</td>
</tr>
<tr>
  <td class="table2">Fee Type</td>
  <td class="table2 form01">
  	<select name="feeTypeId" id="feeType" REQUIRED Autofocus >
  		<option value=''>Select Fee</option>
      {html_options values=$typeArr.feeTypeId output=$typeArr.feeType selected=$feeTypeId}
    </select>
  </td>
</tr>
<tr>
  <td class="table2">Amount</td>
  <td class="table2 form01"><input name="amount" type="text" value="{$amount}" ></td>
</tr>
<tr>
  <td class="table2">Remarks</td>
  <td class="table2 form01"><textarea name="remarks">{$remarks}</textarea></td>
</tr>
<tr>
  <td class="table2"></td>
  <td class="table2 form01" align="center"><input type="submit" name="Submit" class="button" value="Submit"></td>
</tr>
</table>
</form>
{/block}
{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[-1, 10, 20, 30, 40, 50], ["All", 10, 20, 30, 40, 50]],
  	"iDisplayLength": 500,
		"bJQueryUI":true
  });
  $(".omAttend").change(function()
  {
  	$('.newGoBtn').click();
  });
});
</script>
{/block}
{block name="body"}
</br></br>
<form name="formGet" method="GET" action="attendenceReportMonthlyEmp.php">
<table align="center">
	<tr>
	<td>
		<select name="teachingNonteaching">
		    <option value="0">Select Section</option>
		    {html_options values=$teachingNonteachingCmbVal output=$teachingNonteachingCmbTxt selected=$teachingNonteaching}
		  </select>
	</td>
    <td>
      {html_select_date prefix="attendence" start_year="-50" end_year="+50" field_order="DMY" time=$attendenceDate day_value_format="%02d" display_days=false}
    </td>
    <td>
      <input type="submit" name="submit" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
<!--center>Total Number Working Day = {$totalNumberWorkingDay}</center-->
</form>
<table align="left" border="1" id="myDataTable" class="display">
	</br>
	<div class="hd"><h2 align="center">Staff Attendance Report</h2></div>
	<!--thead>
	<tr>
		<td align="left"><b>Roll No</b></td>
	  <td align="left"><b>Name</b></td>
	  <td align="left"><b>AP</b></td>
	  <td align="left"><b>Present</b></td>
	  <td align="left"><b>Absent</b></td>
  </tr>
  </thead-->
  <thead>
	<tr>
	<td align="left"><b>Sr.No.</b></td>
		<td align="left"><b>Code</b></td>
	  <td align="left"><b>Name</b></td>
	  
	  <td width="10">01</td><td width="10">02</td><td width="10">03</td>
	  <td width="10">04</td><td width="10">05</td><td width="10">06</td>
	  <td width="10">07</td><td width="10">08</td><td width="10">09</td>
	  <td width="10">10</td><td width="10">11</td><td width="10">12</td>
	  <td width="10">13</td><td width="10">14</td><td width="10">15</td>
	  <td width="10">16</td><td width="10">17</td><td width="10">18</td>
	  <td width="10">19</td><td width="10">20</td><td width="10">21</td>
	  <td width="10">22</td><td width="10">23</td><td width="10">24</td>
	  <td width="10">25</td><td width="10">26</td><td width="10">27</td>
	  <td width="10">28</td><td width="10">29</td><td width="10">30</td>
	  <td width="10">31</td>
	  <td align="left"><b>A</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$attendenceArr}
  <tr>
  	<td align="left">{$attendenceArr[sec].SrNo}</td>
    <td align="left">{$attendenceArr[sec].employeeCode}</td>
    <td align="left">{$attendenceArr[sec].name}</a></td>
		<td width="10"><a href='attendEdit.php?attendDate={$attendenceDate}-01'>{$attendenceArr[sec].attendences1}</a></td>
  	<td width="10"><a href='attendEdit.php?attendDate={$attendenceDate}-02'>{$attendenceArr[sec].attendences2}</a></td>
  	<td width="10"><a href='attendEdit.php?attendDate={$attendenceDate}-03'>{$attendenceArr[sec].attendences3}</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate={$attendenceDate}-04'>{$attendenceArr[sec].attendences4}</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate={$attendenceDate}-05'>{$attendenceArr[sec].attendences5}</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate={$attendenceDate}-06'>{$attendenceArr[sec].attendences6}</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate={$attendenceDate}-07'>{$attendenceArr[sec].attendences7}</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate={$attendenceDate}-08'>{$attendenceArr[sec].attendences8}</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate={$attendenceDate}-09'>{$attendenceArr[sec].attendences9}</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate={$attendenceDate}-10'>{$attendenceArr[sec].attendences10}</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate={$attendenceDate}-11'>{$attendenceArr[sec].attendences11}</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate={$attendenceDate}-12'>{$attendenceArr[sec].attendences12}</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate={$attendenceDate}-13'>{$attendenceArr[sec].attendences13}</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate={$attendenceDate}-14'>{$attendenceArr[sec].attendences14}</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate={$attendenceDate}-15'>{$attendenceArr[sec].attendences15}</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate={$attendenceDate}-16'>{$attendenceArr[sec].attendences16}</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate={$attendenceDate}-17'>{$attendenceArr[sec].attendences17}</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate={$attendenceDate}-18'>{$attendenceArr[sec].attendences18}</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate={$attendenceDate}-19'>{$attendenceArr[sec].attendences19}</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate={$attendenceDate}-20'>{$attendenceArr[sec].attendences20}</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate={$attendenceDate}-21'>{$attendenceArr[sec].attendences21}</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate={$attendenceDate}-22'>{$attendenceArr[sec].attendences22}</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate={$attendenceDate}-23'>{$attendenceArr[sec].attendences23}</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate={$attendenceDate}-24'>{$attendenceArr[sec].attendences24}</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate={$attendenceDate}-25'>{$attendenceArr[sec].attendences25}</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate={$attendenceDate}-26'>{$attendenceArr[sec].attendences26}</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate={$attendenceDate}-27'>{$attendenceArr[sec].attendences27}</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate={$attendenceDate}-28'>{$attendenceArr[sec].attendences28}</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate={$attendenceDate}-29'>{$attendenceArr[sec].attendences29}</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate={$attendenceDate}-30'>{$attendenceArr[sec].attendences30}</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate={$attendenceDate}-31'>{$attendenceArr[sec].attendences31}</a></td>
    <td align="left">{$attendenceArr[sec].countA}</td>
  </tr>
 {/section}
 </tbody>
 <tfoot>
   <tr>
   	<th></th>
   	<th></th>
   	<th></th>
   	<th></th>
   </tr>
 </tfoot>
</table>
{/block}
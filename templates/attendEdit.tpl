{include file="./main.tpl"}
{block name="head"}
<link rel="stylesheet" href="./css/buttonStyle.css" type="text/css" />
{literal}
<script src="./js/jquery.min.js"></script>
<SCRIPT language = "javascript">
function confirmedCheckbox()
{
  
  if($("#checkAll").is(':checked') == 1)
  {
    $(".confirmed").attr('checked', true);
  }
  else
    $(".confirmed").attr('checked', false);
}
</SCRIPT>
{/literal}
{/block}
{block name="body"}
</br></br>

<form name="formGet" method="GET" action="attendEdit.php">
<table align="center">
	<tr>
    <td>	
    <select name="staff_type">
    	<option value="Teacher" {if $staff_type== 'Teacher'}selected{/if}>Teacher</option>
    	<option value="Admin Staff" {if $staff_type== 'Admin Staff'}selected{/if}>Admin Staff</option>
    	<option value="Class IV" {if $staff_type== 'Class IV'}selected{/if}>Class IV</option>
    </select> 	
    </td>
    <td>
      {html_select_date prefix="attendence" start_year="-25" end_year="+25" field_order="DMY" time=$attendenceDate day_value_format="%02d"}
    </td>
    <td>
      <input type="submit" name="submit" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
</form>
<form name="form2" method="POST" action="attendEdit.php">
<input type="hidden" name="attendDate" value="{$attendDate}">
<table align="center" border="1">  
  </br>
	<div class="hd"><h2 align="center">Staff Attendance Time Report</h2></div>
	</br>
	<tr>
		<td align="left" class="table1"><b>S R No</b></td>
		<td align="left" class="table1"><b>Name</b></td>
		<td align="left" class="table1"><b>Time</b></td><td align="left" class="table1"><b>Attendences</b></td>
		<td align="left" class="table1"><b>Late</b></td>
		<td align="left" class="table1"><b>Delete All <input type="checkbox" name="checkAll" id="checkAll" onClick="confirmedCheckbox();"></b></td>
    
  </tr>
  {section name="sec" loop=$attendenceArr}
  <tr class="gradeRow">
		<td align="left" class="table2">
    	{$attendenceArr[sec].employeeCode}
    	<input type="hidden" name="employeeattendId[]" value="{$attendenceArr[sec].employeeattendId}">
    </td>
    <td align="left" class="table2">{$attendenceArr[sec].name}</td>
    <td align="left" class="table2"><input type="text" name="startTime[]" value="{$attendenceArr[sec].startTime}" size="5" /></td>
    <td align="left" class="table2"><input type="text" name="attendences[]" value="{$attendenceArr[sec].attendences}" size="5" /></td>
    <td align="left" class="table2">
    	<select name="late[]" >
	      {html_options values=$lateOut output=$lateOut selected=$attendenceArr[sec].late}
	    </select>
    </td>
	<td align="center" class="table2"><input type="checkbox" name="check_list[]" id="confirmed[{$attendenceArr[sec].employeeattendId}]" class="confirmed" value="{$attendenceArr[sec].employeeattendId}"></td>
  </tr>
  {/section}
  <tr>
    <td align="center" colspan="9" class="table2">
		<input type="submit" name="submitTaken" class="newSubmitBtn" value="Save">
		<input type="submit" name="Delete" class="newSubmitBtn" value="Delete">
	</td>
  </tr>
</table>
</form>
{/block}
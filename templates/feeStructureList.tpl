{include file="./main.tpl"}
<br>
<br>
<br>
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 10,
		"bJQueryUI":true
  });
  
});
</script>
{/block}
{block name="body"}
</br></br>
<form name="form1" method="POST" action="feeStructureList.php">
<table align="center">
	<tr>
		<td class="table2 form01">
      <select name="startYear" id="startDateYear">
        {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYear}
      </select>
	  </td>
	  <td>
      <input type="submit" name="submit" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
</form>
<table align="left" border="1" id="myDataTable" class="display">
	<div class="hd"><h2 align="center">Fee Structure List</h2>
	<thead>
	<tr>
		<td align="center"><b>Edit</b></td>
		<td align="center"><b>Academic Year</b></td>
		<td align="center"><b>Fee Type</b></td>
		<td align="center"><b>Amount</b></td>
		<td align="center"><b>Remarks</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$feeStructureArray}
  <tr>
  	<td><a href="feeStructure.php?feeStructureId={$feeStructureArray[sec].feeStructureId}">Edit</a></td>
    <td align="left">{$feeStructureArray[sec].academicStartYear} - {$feeStructureArray[sec].academicEndYear}</td>
    <td align="left">{$feeStructureArray[sec].feeType}</td>
    <td align="left">{$feeStructureArray[sec].amount}</td>
    <td align="left">{$feeStructureArray[sec].remarks}</td>
  </tr>
  {/section}
  </tbody>
</table>
{/block}
<form action="getEng.php" method="POST">
<input type="hidden" name="subjectName" value="{$subjectName}">
<input type="hidden" name="termValue" value="{$termValue}">
<table border=2 align="center">
<tr>
  <td align="left">Student Name</td>    
  <td align="center">Enthusiasm</td>
  <td align="center">Discipline</td>
  <td align="center">Team Sprit</td>
  <td align="center">Talent</td>
  <td align="center">Interest</td>
  <td align="center">Creativity</td>
  <td align="center">Skill</td>
  <td align="center">Interest</td>
  <td align="center">Rhythm</td>
  <td align="center">Melody</td>
  <td align="center">Interest</td>
  <td align="center">Creativity</td>
  <td align="center">Presntation</td>
  <td align="center">Interest</td>
  <td align="center">Action</td>
  <td align="center">Expression</td>
</tr>
{section name="sec" loop=$nameArray}
<tr>
	<td nowrap>{$nameArray[sec].studentName}</td>
	<input type="hidden" name="grNo[]" value="{$nameArray[sec].grNo}">
	<td>
    <select name="coPhyEth[]">
    	<option value="">Select Grade</option>
    		{html_options values=$newGradeArray.gradeId output=$newGradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].coPhyEth}
    </select>
  </td>
  <td>
    <select name="coPhyDis[]">
    	<option value="">Select Grade</option>
    		{html_options values=$newGradeArray.gradeId output=$newGradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].coPhyDis}
    </select>
  </td>
  <td>
    <select name="coPhyTea[]">
    	<option value="">Select Grade</option>
    		{html_options values=$newGradeArray.gradeId output=$newGradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].coPhyTea}
    </select>
  </td>
  <td>
    <select name="coPhyTal[]">
    	<option value="">Select Grade</option>
    		{html_options values=$newGradeArray.gradeId output=$newGradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].coPhyTal}
    </select>
  </td>
  <td>
    <select name="coActInt[]">
    	<option value="">Select Grade</option>
    		{html_options values=$newGradeArray.gradeId output=$newGradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].coActInt}
    </select>
  </td>
  <td>
    <select name="coActCre[]">
    	<option value="">Select Grade</option>
    		{html_options values=$newGradeArray.gradeId output=$newGradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].coActCre}
    </select>
  </td>
  <td>
    <select name="coActSki[]">
    	<option value="">Select Grade</option>
    		{html_options values=$newGradeArray.gradeId output=$newGradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].coActSki}
    </select>
  </td>
  <td>
    <select name="coMusicint[]">
    	<option value="">Select Grade</option>
    		{html_options values=$newGradeArray.gradeId output=$newGradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].coMusicint}
    </select>
  </td>
  <td>
    <select name="coMusicRhy[]">
    	<option value="">Select Grade</option>
    		{html_options values=$newGradeArray.gradeId output=$newGradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].coMusicRhy}
    </select>
  </td>
  <td>
   <select name="coMusicMel[]">
   	<option value="">Select Grade</option>
   		{html_options values=$newGradeArray.gradeId output=$newGradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].coMusicMel}
   </select>
  </td>
  <td>
   <select name="coArtInt[]">
   	<option value="">Select Grade</option>
   		{html_options values=$newGradeArray.gradeId output=$newGradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].coArtInt}
   </select>
  </td>
  <td>
   <select name="coArtCre[]">
   	<option value="">Select Grade</option>
   		{html_options values=$newGradeArray.gradeId output=$newGradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].coArtCre}
   </select>
  </td>
  <td>
   <select name="coArtPre[]">
   	<option value="">Select Grade</option>
   		{html_options values=$newGradeArray.gradeId output=$newGradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].coArtPre}
   </select>
  </td>
  <td>
   <select name="coDanceInt[]">
   	<option value="">Select Grade</option>
   		{html_options values=$newGradeArray.gradeId output=$newGradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].coDanceInt}
   </select>
  </td>
  <td>
   <select name="coDanceAct[]">
   	<option value="">Select Grade</option>
   		{html_options values=$newGradeArray.gradeId output=$newGradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].coDanceAct}
   </select>
  </td>
  <td>
   <select name="coDanceExp[]">
   	<option value="">Select Grade</option>
   		{html_options values=$newGradeArray.gradeId output=$newGradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].coDanceExp}
   </select>
  </td>
</tr>
{/section}
<tr>
  <td align="center" colspan="17"><input type="submit" value="Save" name="update" class="newSubmitBtn"></td>
</tr>
</table>
</form>
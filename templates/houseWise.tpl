{include file="./main.tpl"}
{block name=head}
<style type="text/css" media="screen">
@import "./media/css/demo_table_jui.css";
@import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
input
{ 
  border:1px solid #000;
}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
	$('#example').dataTable({
		"aLengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 10,
		"bJQueryUI":true
	});
});
</script>
{/block}
{block name="body"}
</br></br>
<form name="formGet" method="GET" action="houseWise.php">
<table align="center" border="1">
	<tr>
		<td class="table2 form01">
		  <select name="class" autofocus="autofocus">
		    <option value="">Select class</option>
		    {html_options values=$cArray.className output=$cArray.className selected=$class}
		  </select>
	  </td>
    <td class="table2 form01">
		  <select name="classSection">
		    <option value="0">Select Section</option>
		    {html_options values=$secArrOut output=$secArrOut selected=$section}
		  </select>
	  </td>
	  <td class="table2 form01">
      <select name="startYear" id="startDateYear">
        {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYear}
      </select>
    </td>
		<td>
      <select name='houseId' required>
      	<option>Select House</option>
      	{html_options values=$houseArray.houseId output=$houseArray.houseName selected=$houseId}
      </select>
    </td>
    <td>
      <input type="submit" name="" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
</br>
<center>Total <b>{$countStudent}</b> Student</center>
<form>
</br>
<table align="left" border="1" id="example" class="display">
	<div class="hd"><h2 align="center">Student List</h2></div>
	<thead>
	<tr>
		<th align="left"><b>&nbsp;</b></th>
		<th align="left"><b>G. R. No.</b></th>
		<th align="left"><b>Activated</b></th>
		<th align="left"><b>Name</b></th>
		<th align="left"><b>Father Name</b></th>
		<th align="left"><b>Date Of Birth</b></th>
		<th align="left"><b>Joined In Class</b></th>
		<th align="left"><b>Joining Date</b></th>
		<th align="left"><b>Gender</b></th>
		<th align="left"><b>House Name</b></th>
		<th align="left"><b>Current Address</b></th>
		<th align="left"><b>Current State</b></th>
		<th align="left"><b>Current Pin</b></th>
		<th align="left"><b>Residence Phone1</b></th>
		<th align="left"><b>Email</b></th>
		<th align="left"><b>Permanent Address</b></th>
		<th align="left"><b>Permanent State</b></th>
		<th align="left"><b>Permanent Pin</b></th>
		<th align="left"><b>Religion</b></th>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$stdArray}
  <tr>
  	<th align="left"><a href="studentEntry.php?studentMasterId={$stdArray[sec].studentMasterId}">Edit</a></th>
  	<td align="left">{$stdArray[sec].grNo}</td>
  	<td align="left">{$stdArray[sec].activated}</td>
  	<td align="left">{$stdArray[sec].studentName}</td>
    <td align="left">{$stdArray[sec].fatherName}</td>
    <td align="left">{$stdArray[sec].dateOfBirth|date_format:'%d-%m-%Y'}</td>
    <td align="left">{$stdArray[sec].joinedInClass}</td>
    <td align="left">{$stdArray[sec].joiningDate|date_format:'%d-%m-%Y'}</td>
    <td align="left">{$stdArray[sec].gender}</td>

    <td align="left">{$stdArray[sec].houseName}</td>
    <td align="left">{$stdArray[sec].currentAddress}</td>
    <td align="left">{$stdArray[sec].currentState}</td>
    <td align="left">{$stdArray[sec].currentAddressPin}</td>

    <td align="left">{$stdArray[sec].residencePhone1}</td>
    <td align="left">{$stdArray[sec].studentEmail}</td>
    <td align="left">{$stdArray[sec].permanentAddress}</td>
    <td align="left">{$stdArray[sec].permanentState}</td>
    <td align="left">{$stdArray[sec].permanentPIN}</td>
    
    <td align="left">{$stdArray[sec].religion}</td>
  </tr>
  {sectionelse}
  <tr>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
  </tr>
  {/section}
  </tbody>
</table>
{/block}
{include file="./main.tpl"}
{block name="head"}
<!--<script type="text/javascript">
$(document).ready(function(){
  if($result.val == 1)
  {
  	alert("bye");
  }
});-->
<script type="text/javascript">
function getform(obj)
{
	var subjectName     = $('.subjectName').val() !='' ? $('.subjectName').val() : 0;
	var termValue       = $('.termValue').val() !='' ? $('.termValue').val() : 0;
	var startDateYear   = $('#startDateYear').val() !='' ? $('#startDateYear').val() : "";
	var endDateYear     = (startDateYear)+ 1;
	var dataString    = "subjectName=" + subjectName + "&termValue=" + termValue + "&startDateYear=" + startDateYear;
	$.ajax({
	  type:"GET",
	  url:"getEng.php",
	  data:dataString,
	  success:function(data)
	  {
	  	$('.getForm').html(data);
    }
	});
}
</script>
{/block}
{block name="body"}
<form action="" method="POST">
	<br><br>
	<h2 align="center"><font color="#29833B">Grade Entry</font></h2>
  <table border="2" align="center">
  	<tr>
  		<td class="">
      <select name="startYear" id="startDateYear">
        {html_options values=$dateArrVal output=$dateArrOut selected=$currentSelctedYear}
      </select>
	  </td>
  		<td>Select Subject</td>
  		<td>
  			<select required class="subjectName">
  			  <option value="">Select Subject</option>
  			    {html_options values=$subjectValue output=$subjectOutput}
  			</select>
  	  </td>
  	  <td>Select Term</td>
  		<td>
  			<select required class="termValue">
  			  <option value="">Select Term</option>
  			    {html_options values=$termValue output=$termOutput}
  			</select>
  	  </td>
  	  <td><input type="button" name="submit" value="View" onClick="getform(this);" class="newSubmitBtn"></td>
    </tr>
  </table>
  <br><br><br>
</form>
  <span class="getForm"></span>
{/block}
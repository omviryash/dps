{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script src="./media1/js/jquery-1.4.4.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.jeditable.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[100, 25, 50, 100, 500, 1000], [100, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 100,
		"bJQueryUI":true
  });
  
  /* Editable Start*/
	var oTable = $('#myDataTable').dataTable();
     
  /* Apply the jEditable handlers to the table */
  oTable.$('th').editable( './updateRollNoData.php', {
      "submitdata": function ( value, settings ) {
          return {
              "row_id": this.parentNode.getAttribute('th.id'),
              "column": oTable.fnGetPosition( this )[2]
          };
      },
      "height": "20px",
      "width": "100"
  });
  
  /* Editable End*/
});
</script>
{/block}
{block name="body"}
</br></br>
<form name="formGet" method="GET" action="iCardNominalRollList.php">
<table align="center">
	<tr>
		<td class="table2 form01">
		  <select name="class" autofocus="autofocus" required >
		    <option value="">Select class</option>
		    {html_options values=$cArray.className output=$cArray.className selected=$class}
		  </select>
	  </td>
    <td class="table2 form01">
		  <select name="classSection">
		    <option value="0">Select Section</option>
		    {html_options values=$secArrOut output=$secArrOut selected=$section}
		  </select>
	  </td>
	  <td class="table2 form01">
      <select name="startYear" id="startDateYear">
        {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYear}
      </select>
	  </td>
    <td>
      <input type="submit" name="submit" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
</form>
{if $s_userType != 'idCrdUser'}
<center><a href="nominalRollClassPrint.php?class={$class}&section={$section}">ID-Card Class List</a>
{/if}
 | <a href="idPrint.php?class={$class}&section={$section}">Id Card Print</a></center>
<table align="left" border="1" id="myDataTable" class="display">
	<div class="hd"><h2 align="center">ID-Card Class List</h2></div>
	<thead>
	<tr>
		<!-- td align="left"><b>Academic Year</b></td -->
		<td align="left"><b>Sr No</b></td>
		<td align="left"><b>GR No</b></td>
		<td align="left"><b>Class</b></td>
		<td align="left"><b>Roll No</b></td>
		<td align="left"><b>Student Name</b></td>
		<td align="left"><b>Address</b></td>
		<td align="left"><b>Phone</b></td>

		<!-- td align="left"><b>Section</b></td -->
		<td align="left"><b>Date Of Birth</b></td>
		<td align="left"><b>Blood Group</b></td>
		<td align="left"><b>House</b></td>
		<td align="left"><b>Bus No</b></td>
		<!--td align="left"><b>Fee Group</b></td>
		<td align="left"><b>Library Member Type</b></td>
		<td align="left"><b>Bus Route</b></td>
		<td align="left"><b>Bus Stop</b></td>
		<td align="left"><b>Subject Group</b></td>
		<td align="left"><b>Co Scholastic Group</b></td>
		<td align="left"><b>Club Term 1</b></td>
		<td align="left"><b>Club Term 2</b></td -->
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$nominalArr}
  <tr>
  	<!-- td align="left">{$nominalArr[sec].academicYear}</td -->
  	<td align="left">{$smarty.section.sec.rownum}</td>
    <td align="left">{$nominalArr[sec].grNo}</td>
    <td align="left">{$nominalArr[sec].class}-{$nominalArr[sec].section}</td>
    <td align="left">{$nominalArr[sec].rollNo}</td>
    <td align="left">{$nominalArr[sec].studentName}</td>
    <td align="left">{$nominalArr[sec].currentAddress}</td>
    <td align="left">{$nominalArr[sec].fatherMobile}</td>

    <!-- td align="left"></td -->
	<td align="left">{$nominalArr[sec].dateOfBirth }</td>
	<td align="left">{$nominalArr[sec].bloodGroup}</td>
	<td align="left">{$nominalArr[sec].houseId}</td>
	<td align="left">{$nominalArr[sec].busStopMasterId}</td>
	<!--td align="left">{$nominalArr[sec].feeGroup}</td>
    <td align="left">{$nominalArr[sec].libraryMemberType}</td>
    <td align="left">{$nominalArr[sec].busRoute}</td>
    <td align="left">{$nominalArr[sec].busStop}</td>
    <td align="left">{$nominalArr[sec].subjectGroup}</td>
    <td align="left">{$nominalArr[sec].coScholasticGroup}</td>
    <td align="left">{$nominalArr[sec].clubTerm1}</td>
    <td align="left">{$nominalArr[sec].clubTerm2}</td -->
  </tr>
  {/section}
  </tbody>
</table>
{/block}
{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script src="media2/js/jquery.dataTables.columnFilter.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable(
  {
    "bProcessing": true,
		"bServerSide": true,
		"bAutoWidth": false,
    "aLengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 10,
		"sPaginationType":"full_numbers",
    "aaSorting":[[0, "DESC"]],
		"bJQueryUI":true,
		"sAjaxSource": "./studentListAjaxPhoto.php?activated=" + $('#activated').val()
  });
  /* Large Image Show Start*/

	
	/* Large Image Show End*/
});

$(document).ready(function(){
$(".studentImage").hover(function(e){
	alert('');
  if($(this).attr("alt") != "")
  {
    var img = "./studentImage/"+ $(this).attr("alt");
    $("#large").css("top",(e.pageY+5)+"px")
	             .css("left",(e.pageX+5)+"px")                  
	             .html("<img src='"+img+"' alt='Large Image' />")
	             .fadeIn("slow");
	}
}, function(){
  $("#large").fadeOut("fast");
});
});
</script>
{/block}
{block name="body"}
</br></br>
<input type="hidden" name="activated" id="activated" value="{$activated}">
<form name="formGet" method="GET" action="studentAllListPhoto.php">
<table align="center" border="1">
	<tr>
		<td class="table2 form01 omAttend">
		  <select name="activated" autofocus="autofocus">
		    <option value="0">Select</option>
		    {html_options values=$activeValues output=$activeOutPut selected=$activated}
		  </select>
	  </td>
    <td>
      <input type="submit" name="go" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
</form>

<table align="left" border="1" id="myDataTable" class="display dataTables_length">
	<div class="hd"><h2 align="center">Student Photo List</h2></div>
	<thead>
	<tr>
		<td align="left"><b>G. R. No.</b></td>
		<td align="left"><b>Name</b></td>
		<td align="left"><b>Pupil Image</b></td>
		<td align="left"><b>Father Name</b></td>
		<td align="left"><b>Father Image</b></td>
		<td align="left"><b>Mother Name</b></td>
		<td align="left"><b>Mother Image</b></td>
  </tr>
  </thead>
  <tbody>
  
  </tbody>
</table>
{/block}
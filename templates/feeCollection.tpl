{include file="./main.tpl"}
{block name="head"}
<script type="text/javascript">
$(document).ready(function() {
  $(".amount").blur(function()
	{
    $("#one").clone(true).appendTo("#mainDiv")
    .find('input[type="text"]').val('').end()
    .find('select[option=""]').val('');
    $(".rNo:last").focus();
  });
});
function checkGrno(obj)
{
	var row = $(obj).parents('.tableRow');
	var dataString = "grNo=" + row.find(".grNo").val();
	$.ajax({
		type : 'GET',
		url  : 'setStudNameFee.php',
		data :  dataString,
		success:function(data)
		{
      row.find('.studentName').html(data);
	  }
	});
}
function setFullDate(obj)
{
	var row = $(obj).parents('.tableRow');
  var dayExtra    = row.find('#fromDateDay').val();
  var monthExtra  = row.find('#fromDateMonth').val();
  var yearExtra   = row.find('#fromDateYear').val();
	var fullDate = yearExtra+'-'+monthExtra+'-'+dayExtra;
	
  row.find('.fullDate').val(fullDate);
}
</script>
{/block}
{block name="body"}
<form name="form1" method="POST" action="feeCollection.php">
<table align="center" border="0" class="table2">
<div class="hd"><h2 align="center">Fee Collection Entry</h2>
<tr>
  <td align="center" colspan="2" style="color:red; font-size:18px;"><b>{$confirm}</b></td>
</tr>
<tbody id="mainDiv">
{if $isEdit == 1}
<tr class='tableRow'>
	<td class="table2">R NO</td>
  <td class="table2 form01">
  	<input type="text" name="rNo" value="{$rNo}" class='rNo' Autofocus />
	</td>
	<td class="table2">Fee Date</td>
  <td class="table2 form01" onchange="setFullDate(this);">
    {html_select_date day_extra="id=\"fromDateDay\"" month_extra="id=\"fromDateMonth\"" year_extra="id=\"fromDateYear\"" prefix="" start_year="-25" end_year="+25" field_order="DMY" time=$feeDate day_value_format="%02d"}
    <input type="hidden" name="feeDate" class="fullDate" value="{$feeDate}">
  </td>
</tr>
<tr class='tableRow'>
	<td class="table2">Gr No</td>
  <td class="table2 form01">
  	<input type="text" name="grNo" value="{$grNo}" class="grNo" Autofocus onblur="checkGrno(this);" />
	</td>
	<td align="left" class="table2 form01">Student Name : </td>
  <td class="table2 form01 studentName" colspan="5" ><input type="text" name="" value="{$studentName}" DISABLED /></td>
</tr>
{section name=sec loop=$editArry}
<tr id="one" class='tableRow'>
	<td class="table2">Fee Type</td>
  <td class="table2 form01">
  	<input type="hidden" name="feeCollectionId[]" value="{$editArry[sec].feeCollectionId}">
  	<select name="feeTypeId[]" id="feeType" >
  		<option value=''>Select Fee</option>
      {html_options values=$typeArr.feeTypeId output=$typeArr.feeType selected=$editArry[sec].feeTypeId}
    </select>
	</td>
	<td class="table2">Duration</td>
  <td class="table2 form01">
  	<select name="feeName[]" >
  		<option value=''>Duration</option>
      {html_options values=$feeNameOut output=$feeNameOut selected=$editArry[sec].feeName}
    </select>
	</td>
	<td class="table2">Mode Of Pay</td>
  <td class="table2 form01">
  	<select name="modeOfPay[]" >
      {html_options values=$modeOfPayOut output=$modeOfPayOut selected=$editArry[sec].modeOfPay}
    </select>
	</td>
	<td class="table2">ChNo/DDNo</td>
  <td class="table2 form01">
  	<input type="text" name="chNo[]" value="{$editArry[sec].chNo}" />
	</td>
	<td class="table2">Discount</td>
  <td class="table2 form01">
  	<input type="text" name="discount[]" value="{$editArry[sec].discount}" />
	</td>
	<td class="table2">Amount</td>
  <td class="table2 form01">
  	<input type="text" name="amount[]" class="amount" value="{$editArry[sec].amount}" />
	</td>
</tr>
{/section}
{else}
<tr class='tableRow'>
	<td class="table2">R NO</td>
  <td class="table2 form01">
  	<input type="text" name="rNo" value="{$rNo}" class='rNo' Autofocus />
	</td>
	<td class="table2">Fee Date</td>
  <td class="table2 form01" onchange="setFullDate(this);">
    {html_select_date day_extra="id=\"fromDateDay\"" month_extra="id=\"fromDateMonth\"" year_extra="id=\"fromDateYear\"" prefix="" start_year="-25" end_year="+25" field_order="DMY" time=$feeDate day_value_format="%02d"}
    <input type="hidden" name="feeDate" class="fullDate" value="{$feeDate}">
  </td>
</tr>
<tr class='tableRow'>
	<td class="table2">Gr No</td>
  <td class="table2 form01">
  	<input type="text" name="grNo" value="{$grNo}" class="grNo" Autofocus onblur="checkGrno(this);" />
	</td>
	<td align="left" class="table2 form01">Student Name : </td>
  <td class="table2 form01 studentName" colspan="5" ><input type="text" name="" value="{$studentName}" DISABLED /></td>
</tr>
<tr id="one" class='tableRow'>
	<td class="table2">Fee Type</td>
  <td class="table2 form01">
  	<select name="feeTypeId[]" id="feeType" >
  		<option value=''>Select Fee</option>
      {html_options values=$typeArr.feeTypeId output=$typeArr.feeType selected=$feeTypeId}
    </select>
	</td>
	<td class="table2">Duration</td>
  <td class="table2 form01">
  	<select name="feeName[]" >
  		<option value=''>Duration</option>
      {html_options values=$feeNameOut output=$feeNameOut selected=$feeName}
    </select>
	</td>
	<td class="table2">Mode Of Pay</td>
  <td class="table2 form01">
  	<select name="modeOfPay[]" >
      {html_options values=$modeOfPayOut output=$modeOfPayOut selected=$modeOfPay}
    </select>
	</td>
	<td class="table2">ChNo/DDNo</td>
  <td class="table2 form01">
  	<input type="text" name="chNo[]" value="{$chNo}" />
	</td>
	<td class="table2">Discount</td>
  <td class="table2 form01">
  	<input type="text" name="discount[]" value="{$discount}" />
	</td>
	<td class="table2">Amount</td>
  <td class="table2 form01">
  	<input type="text" name="amount[]" class="amount" value="{$amount}" />
	</td>
</tr>
{/if}
</tbody>
<tr>
  <td class="table2 form01" colspan='10' align="center"><input type="submit" name="Submit" class="button" value="Save"></td>
</tr>
</table>
</form>
{/block}
{include file="./main.tpl"}
{block name="head"}
<link rel="stylesheet" href="./css/buttonStyle.css" type="text/css" />
<script type="text/javascript">
$(document).ready(function(){
	$(".omAttend").change(function()
  {
  	$('.newGoBtn').click();
  });
});


function buttonHide()
{
	$('#hideMe').hide();
}

</script>
{/block}
{block name="body"}
</br></br></br></br></br></br>
<center>
<form name="formGet" id="formGet" method="GET" action="reGenerateMarksEntryPerforma.php">
<table align="center" border="1">
	<tr>
		<td class="table2 form01">
		  <select name="startYear" class="omAttend">
		    {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYearSelected}
		  </select>
		</td>
		{if $s_userType == 'Administrator'}
		<td class="table2 form01 omAttend">
		  <select name="class" autofocus="autofocus">
		    <option value="0">Select class</option>
		    {html_options values=$cArray.className output=$cArray.className selected=$class}
		  </select>
	  </td>
    <td class="table2 form01 omAttend">
		  <select name="section">
		    <option value="0">Select Section</option>
		    {html_options values=$secArrOut output=$secArrOut selected=$section}
		  </select>
	  </td>
	  {/if}
	  <td class="table2 form01 omAttend">
	  	<select name="subjectMasterId">
	      <option value="0">Select Subject</option>
	      {html_options values=$clubArr.subjectMasterId output=$clubArr.subjectName selected=$subjectMasterId}
		  </select>
		</td>
		<td>
	    <input type="submit" name="go" class="newGoBtn" value="go">
	  </td>
	</tr>
</table>
{if $subjectMasterId > 0}
<table align="center" border="0">
  </br>
  <tr>
    <td>
	    <input type="submit" name="submit" onclick="buttonHide();" id="hideMe" class="newSubmitBtn" value="Re Generate">
	  </td>
	</tr>
</table>
{/if}
</form>
</center>
<form name="form2" method="POST" action="reGenerateMarksEntryPerforma.php">
<table align="center" border="1">
  </br></br></br>
	<h1 align="center">Student List</h1>
	</br>
	<h1 align="center">{$class}/{$section}</h1>
  </br>
	<tr>
		<td align="left" class="table1"><b>S.R.No.</b></td>
		<td align="left" class="table1"><b>G. R. No.</b></td>
		<td align="left" class="table1"><b>Roll No.</b></td>
		<td align="left" class="table1"><b>Name</b></td>
		<td align="left" class="table1"><b>M/F</b></td>
		<td align="left" class="table1"><b>Subject</b></td>
		<td align="left" class="table1"><b>FA1</b></td>
		<td align="left" class="table1"><b>FA2</b></td>
		<td align="left" class="table1"><b>SA1</b></td>
		<td align="left" class="table1"><b>FA3</b></td>
		<td align="left" class="table1"><b>FA4</b></td>
		<td align="left" class="table1"><b>SA2</b></td>
  </tr>
  {section name="sec" loop=$stdArray}
  <tr class="trRow">
    <td align="center" class="table2">{$smarty.section.sec.rownum}</td>
    <td align="left" class="table2">
    	{$stdArray[sec].grNo} <input type="hidden" name="grNo[]" value="{$stdArray[sec].grNo}">
    </td>
    <td align="left" class="table2">{$stdArray[sec].rollNo}</td>
    <td align="left" class="table2">{$stdArray[sec].studentName}</td>
    <td align="left" class="table2">{$stdArray[sec].gender}</td>
    <td align="left" class="table2">{$stdArray[sec].subjectName}</td>
    <td align="center" class="table2">
    	<input type="text" name="fa1[]" value="{$stdArray[sec].fa1}">
    </td>
    <td align="center" class="table2">
    	<input type="text" name="fa2[]" value="{$stdArray[sec].fa2}">
    </td>
    <td align="center" class="table2">
    	<input type="text" name="sa1[]" value="{$stdArray[sec].sa1}">
    </td>
    <td align="center" class="table2">
    	<input type="text" name="fa3[]" value="{$stdArray[sec].fa3}">
    </td>
    <td align="center" class="table2">
    	<input type="text" name="fa4[]" value="{$stdArray[sec].fa4}">
    </td>
    <td align="center" class="table2">
    	<input type="text" name="sa2[]" value="{$stdArray[sec].sa2}">
    </td>
  </tr>
  {/section}
  </tr>
</table>
</form>
{/block}
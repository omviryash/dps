{include file="./main.tpl"}
<br>
<br>
<br>
<br>
{block name="head"}
{/block}
{block name="body"}
<form name="form1" method="POST" action="classMaster.php">
<table align="center" border="0" class="table2">
<div class="hd"><h2 align="center">Class Entry</h2>
<tr>
  <input type="hidden" name="classMasterId" value="{$classMasterId}">
</tr>
<tr>
  <td align="center" colspan="2" style="color:red; font-size:18px;"><b>{$confirm}</b></td>
</tr>
<tr>
  <td class="table2">Class Name</td>
  <td class="table2 form01"><input name="className" type="text" autofocus="autofocus" value="{$className}" required ></td>
</tr>
<tr>
  <td class="table2">Section</td>
  <td class="table2 form01"><input type="text" name="classSection" value="{$classSection}"></td>
</tr>
<tr>
  <td class="table2"></td>
  <td class="table2 form01" align="center"><input type="submit" name="Submit" class="button" value="Submit"></td>
</tr>
</table>
</form>
<table align="center" border="2">
	<h2 align="center">Class List</h2>
	<tr>
		<td align="center"><b>Class Name</b></td>
		<td align="center"><b>Section</b></td>
  </tr>
  {section name="sec" loop=$classArray}
  <tr>
    <!--<td align="left"><a href="classMaster.php?classMasterId={$classArray[sec].classMasterId}">Edit</a></td>
    <td align="left"><a onclick="return confirm('Are You Sure To Delete??');" href="classDelete.php?classMasterId={$classArray[sec].classMasterId}">Delete</a></td>-->
    <td align="left">{$classArray[sec].className}</td>
    <td align="left">{$classArray[sec].classSection}</td>
  </tr>
  {/section}
</table>
{/block}
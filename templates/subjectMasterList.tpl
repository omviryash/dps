{include file="./main.tpl"}
<br>
<br>
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[-1, 25, 50, 100, 500, 1000], ['All', 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 200,
		"bJQueryUI":true
  });
});
</script>
{/block}
{block name="body"}
<div class="hd"><h2 align="center">Subject Master List</h2></div>
<table align="left" border="1" id="myDataTable" class="display">
	<thead>
	<tr>
		<td align="left"><b>Edit</b></td>
		<td align="left"><b>Sr No</b></td>
		<td align="left"><b>Subject Name</b></td>
		<td align="left"><b>Subject Code</b></td>
		<td align="left"><b>Start Class</b></td>
		<td align="left"><b>End Class</b></td>
		<td align="left"><b>Subject Type</b></td>
		<td align="left"><b>Co Scholastic Type</b></td>
  </tr>
  </thead>
  <tbody>
	{section name="sec" loop=$clubArray}
  <tr>
    <td align="left" class="table2"><a href="subjectEntry.php?subjectMasterId={$clubArray[sec].subjectMasterId}">Edit</a></td>
    <td align="left" class="table2">{$smarty.section.sec.rownum}</td>
    <td align="left" class="table2">{$clubArray[sec].subjectName}</td>
    <td align="left" class="table2">{$clubArray[sec].subjectCode}</td>
    <td align="left" class="table2">{$clubArray[sec].startClass}</td>
    <td align="left" class="table2">{$clubArray[sec].endClass}</td>
    <td align="left" class="table2">{$clubArray[sec].subjectType}</td>
    <td align="left" class="table2">{$clubArray[sec].subjectDescription}</td>
  </tr>
  {/section}
  </tbody>
</table>
{/block}
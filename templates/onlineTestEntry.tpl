{include file="./main.tpl"}
{block name="body"}
<form name="form1" method="POST" action="onlineTestEntry.php">
<table align="center" border="0" class="table2">
<div class="hd"><h2 align="center">Online Test Entry</h2>
<input type="hidden" name="onlineTestId" value="{$onlineTestId}">
<tr>
  <td align="center" colspan="2" style="color:red; font-size:18px;"><b>{$confirm}</b></td>
</tr>
<tr>
	<td class="table2">Subject</td>
	<input type="hidden" name="qNo" value="0">
  <td class="table2 form01">
  	<select name="subjectMasterId" required >
      <option value="">Select Subject</option>
      {html_options values=$clubArr.subjectMasterId output=$clubArr.subjectName selected=$subjectMasterId}
	  </select>
	</td>
  <td class="table2">Class</td>
  <td class="table2 form01">
  	<select name="class" required >
	    <option value="">Select class</option>
	    {html_options values=$cArray.className output=$cArray.className selected=$class}
	  </select>
  </td>
</tr>
<tr>
  <td class="table2">Question</td>
  <td class="table2 form01">
  	<textarea name="question" style="width:300px; height:100px;">{$question}</textarea>
	</td>
  <td class="table2">A</td>
  <td class="table2 form01">
  	<textarea name="option1">{$option1}</textarea>
	</td>
  <td class="table2">B</td>
  <td class="table2 form01">
  	<textarea name="option2">{$option2}</textarea>
	</td>
  <td class="table2">C</td>
  <td class="table2 form01">
  	<textarea name="option3">{$option3}</textarea>
	</td>
  <td class="table2">D</td>
  <td class="table2 form01">
  	<textarea name="option4">{$option4}</textarea>
	</td>
  <td class="table2">Answer</td>
  <td class="table2 form01">
    <select name="answer">
  	  {html_options values=$ansArr output=$ansArr selected=$answer}
  	</select>
	</td>
</tr>
<tr>
  <td class="table2"></td>
  <td class="table2 form01" align="center"><input type="submit" name="Submit" class="button" value="Save"></td>
</tr>
</table>
</form>
{/block}
{include file="./main.tpl"}
{block name="head"}
<link rel="stylesheet" href="./css/buttonStyle.css" type="text/css" />
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript">
$(document).ready(function(){
	$('#myDataTable').dataTable({
		"aLengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 100,
  	"bAutoWidth": false,
  	"aaSorting": [[ 0, "asc" ]],
		"bJQueryUI":true
  });
  
	$(".omAttend").change(function()
  {
  	$('.newGoBtn').click();
  });
  
  $('.checkAll').click(function () {
    $('.checkboxAll').prop('checked', this.checked);
  });
  
  $('.checkAll2').click(function () {
    $('.checkboxAll2').prop('checked', this.checked);
  });
});


function buttonHide()
{
	$('#hideMe').hide();
}

</script>
<style>
.selectCombo
{
	width:500px;
}
</style>
{/block}
{block name="body"}
</br></br>
<center>
<form name="formGet" id="formGet" method="GET" action="viewReportCard.php">
<table align="center" border="1">
	<div class="hd"><h2 align="center">Report Card Visible</h2></div>
	<tr>
      
		<td class="table2 form01">
		  <select name="startYear" class="omAttend">
		    {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYearSelected}
		  </select>
		</td>
		{if $s_userType == 'Administrator'}
		<td class="table2 form01 omAttend">
		  <select name="class" autofocus="autofocus">
		    <option value="0">Select class</option>
		    {html_options values=$cArray.className output=$cArray.className selected=$class}
		  </select>
	  </td>
    <td class="table2 form01 omAttend">
		  <select name="section">
		    <option value="0">Select Section</option>
		    {html_options values=$secArrOut output=$secArrOut selected=$section_sel}
		  </select>
	  </td>
	  {/if}
		<td>
	    <input type="submit" name="go" class="newGoBtn" value="go">
	  </td>
	</tr>
</table>
</form>
</center>
{if $count != 0}
<form name="form2" method="POST" action="viewReportCard.php">
	<input type="hidden" name="startYear" value="{$academicStartYearSelected}">
  <table align="center" border="1" id="myDataTable" class="display viewreportcard">
  </br></br></br>
	<h1 align="center">Report Card Visibility</h1>
	</br>
	<h1 align="center">{$class}/{$section}</h1>
  </br>
  <thead>
	<tr>
    <td align="left" class="table1"><b>GRNo.</b></td>
    {if $s_userType == 'Administrator'}
    <td align="left" class="table1"><b>Class</b></td>
    <td align="left" class="table1"><b>Section</b></td>
    {/if}
    <td align="left" class="table1"><b>Roll No.</b></td>
		<td align="left" class="table1"><b>Name</b></td>
    <td class="table1" align='center'><b>Report Card Visibility Term-1</b>
			<input type="checkbox"  name="checkAll" id="checkAll" class="checkAll">
		</td>
    <td class="table1" align='center'><b>Report Card Visibility Term-2</b>
			<input type="checkbox"  name="checkAll2" id="checkAll2" class="checkAll2">
		</td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$dtlArr}
  <tr class="gradeRow">
	<td align="left" class="table2">
    	{$dtlArr[sec].grNo}
    	<input type="hidden" name="nominalRollId[]" value="{$dtlArr[sec].nominalRollId}">
    </td>
    {if $s_userType == 'Administrator'}
    <td align="left" class="table2">{$dtlArr[sec].class}</td>
    <td align="left" class="table2">{$dtlArr[sec].section}</td>
    {/if}
    <td align="left" class="table2">{$dtlArr[sec].rollNo}</td>
    <td align="left" class="table2">{$dtlArr[sec].studentName}</td>
    <td align="center" class="table2"><input type="checkbox" class="checkboxAll" name="reportCardView1[]" {if $dtlArr[sec].reportCardView1 == 'Y'}checked=checked{/if} value="{$dtlArr[sec].nominalRollId}"></td>
    
    <td align="center" class="table2"><input type="checkbox" class="checkboxAll2" name="reportCardView2[]" {if $dtlArr[sec].reportCardView2 == 'Y'}checked=checked{/if} value="{$dtlArr[sec].nominalRollId}"></td>
  </tr>
  {/section}
  </tbody>
  <tfoot>
  <tr>
   {if $s_userType == 'Administrator'}
    <td align="center" colspan="7" class="table2"><input type="submit" name="submitTaken" class="newSubmitBtn" value="Save"></td>
   {else} 
   	<td align="center" colspan="5" class="table2"><input type="submit" name="submitTaken" class="newSubmitBtn" value="Save"></td>
   {/if}
  </tr>
  </tfoot>
</table>
</form>
{/if}
{/block}
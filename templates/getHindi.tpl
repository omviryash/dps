<form action="getEng.php" method="POST">
<input type="hidden" name="subjectName" value="{$subjectName}">
<input type="hidden" name="termValue" value="{$termValue}">
<table align="left" border="2">
 <tr>
 	<!--for eng and hindi : Start-->
 	<td align="center">Student Name</td>
 	<td align="center">Pronunciation</td>
 	<td align="center">Fluency</td>
 	<td align="center">Comprehension</td>
 	<td align="center">Creative Writing</td>
 	<td align="center">Hand Writing</td>
 	<td align="center">Grammar</td>
 	<td align="center">Spellings</td>
 	<td align="center">Vocabulary</td>
 	<td align="center">Conversation</td>
 	<td align="center">Recitation</td>
 	<td align="center">Clarity</td>
 	<td align="center">Comprehension</td>
 	<td nowrap align="center">Concentration Span</td>
 	<td nowrap align="center">Extra Reading</td>
 	<td nowrap align="center">Activity / Project</td> 
 	<!--for eng and hidi : end -->
 </tr>
{section name="sec" loop=$nameArray}
<tr>
	<td nowrap>{$nameArray[sec].studentName}</td>
	<input type="hidden" name="grNo[]" value="{$nameArray[sec].grNo}">
	<!--for eng and hidi : Start-->
      <td>
        <select name="hiReadingPro[]">
        	<option value="">Select Grade</option>
        	  {html_options values=$gradeArray.gradeId output=$gradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].hiReadingPro}
        </select>
      </td>
      <td>
        <select name="hiReadingFlu[]">
        	<option value="">Select Grade</option>
        		{html_options values=$gradeArray.gradeId output=$gradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].hiReadingFlu}
        </select>
      </td>
      <td>
        <select name="hiReadingCom[]">
        	<option value="">Select Grade</option>
        		{html_options values=$gradeArray.gradeId output=$gradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].hiReadingCom}
        </select>
      </td>
      <td>
        <select name="hiWritingCre[]">
        	<option value="">Select Grade</option>
        		{html_options values=$gradeArray.gradeId output=$gradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].hiWritingCre}
        </select>
      </td>
      <td>
        <select name="hiWritingHan[]">
        	<option value="">Select Grade</option>
        		{html_options values=$gradeArray.gradeId output=$gradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].hiWritingHan}
        </select>
      </td>
      <td>
        <select name="hiWritingGra[]">
        	<option value="">Select Grade</option>
        		{html_options values=$gradeArray.gradeId output=$gradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].hiWritingGra}
        </select>
      </td>
      <td>
        <select name="hiWritingSpe[]">
        	<option value="">Select Grade</option>
        		{html_options values=$gradeArray.gradeId output=$gradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].hiWritingSpe}
        </select>
      </td>
      <td>
        <select name="hiWritingVoc[]">
        	<option value="">Select Grade</option>
        		{html_options values=$gradeArray.gradeId output=$gradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].hiWritingVoc}
        </select>
      </td>
      <td>
        <select name="hiwSpeakinCon[]">
        	<option value="">Select Grade</option>
        		{html_options values=$gradeArray.gradeId output=$gradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].hiwSpeakinCon}
        </select>
      </td>
      <td>
        <select name="hiwSpeakinRec[]">
        	<option value="">Select Grade</option>
        		{html_options values=$gradeArray.gradeId output=$gradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].hiwSpeakinRec}
        </select>
      </td>
      <td>
        <select name="hiwSpeakinCla[]">
        	<option value="">Select Grade</option>
        		{html_options values=$gradeArray.gradeId output=$gradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].hiwSpeakinCla}
        </select>
      </td>
      <td>
        <select name="hiListingComp[]">
        	<option value="">Select Grade</option>
        		{html_options values=$gradeArray.gradeId output=$gradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].hiListingComp}
        </select>
      </td>
      <td>
        <select name="hiListingCon[]">
        	<option value="">Select Grade</option>
        		{html_options values=$gradeArray.gradeId output=$gradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].hiListingCon}
        </select>
      </td>
      <td>
        <select name="hiextraReading[]">
        	<option value="">Select Grade</option>
        		{html_options values=$gradeArray.gradeId output=$gradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].hiextraReading}
        </select>
      </td>
      <td>
        <select name="hiactivityPro[]">
        	<option value="">Select Grade</option>
        		{html_options values=$gradeArray.gradeId output=$gradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].hiactivityPro}
        </select>
      </td>
      <!--for eng and hidi : End--> 
</tr>
{/section}
<tr>
	<td colspan="16" align="center"><input type="submit" value="Save" name="update" class="newSubmitBtn">
</tr>
</table>
</form>
{include file="./main.tpl"}
{block name="head"}
<script type="text/javascript">
$(document).ready(function() {
  $('.checkAll').click(function(e){
    var table= $(e.target).closest('table');
    $('td input:checkbox',table).prop('checked',this.checked);
});
  $("#startDateYear").change(function()
  {
    var yearExtra  = $('#startDateYear').val();
		var endDate    = yearExtra;
		var datastring = 'endDate=' + endDate;
		$.ajax({	
			type: 'GET',
			url: 'endyear.php',
		  data: datastring,
		  success:function(data)
		  {
		  	$('#endDateYear').val(data);
	    }
	  });
  });
});
</script>
{/block}
{block name="body"}
</br></br>
<form name="formGet" method="POST" action="resultOneToFive.php">
<table align="center" border="2">
	<h2 align="center"><font color="#29833B">Student List</font></h2>
	<tr>
    <td class="table2 form01">
      <select name="startYear" id="startDateYear" onchange="this.form.submit();">
        {html_options values=$dateArrVal output=$dateArrOut selected=$currentSelctedYear}
      </select>
	  </td>
  </tr>
	<tr>
		<td colspan="2" align="center">
			{if $result == 0}
	      <input type="submit" value="Generate" name="newSubmitBtn" class="newSubmitBtn" disabled>
	    {else}
	      <input type="submit" value="Genrate" name="newSubmitBtn" class="newSubmitBtn">
	    {/if}
	  </td>
	</tr>
</table>
</form>
<form name="formGet" method="POST" action="resultOneToFive.php" id="frm1">
<br><br>
<table align="center" border="2">
	<tr>
		<td>Class</td>
		<td>Section</td>
		<td>View To Student</td>
		<td>
			 <input type="checkbox" class="checkAll"  name="checkAllCheckBox" >
		   <input type="submit" value=" Save "  name="update" class="newSubmitBtn">
		</td>
		<td>G.R.No</td>
		<td>Student Name</td>
  </tr>
   {section name="sec" loop=$nameArray}
   <td align="center">{$class}</td>
   <td align="center">{$section}</td>
    	{if $nameArray[sec].status == 'D'}
    	  <td align="center">No</td>
    	{else}
    	  <td align="center">Yes</td>
    	{/if}
    	<input type="hidden" class="studentId" value="{$nameArray[sec].studentId}" name="studentId[]" class="studentId">
    	<td><input type="checkbox" class="grNo" class="checkAll" value="{$nameArray[sec].grNo}" name="grNo[]" class="grNo"></td>
    	<td>{$nameArray[sec].grNo}</td>
    	<td>{$nameArray[sec].studentName}</td>
    </tr>
  {/section}
</table>
</form>
{/block}
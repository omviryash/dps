{include file="./main.tpl"}
{block name="body"}
<form action="employeeEntry.php" enctype="multipart/form-data" method="POST">
<table align="center" border="1">
<input type="hidden" value="{$employeeMasterId}" name="employeeMasterId" id="employeeMasterId" >
<br><br><br>
<div class="hd"><h2 align="center">New Employee Entry</h2></div>
<br><br>
<tr>
  <td align="center" colspan="4" style="color:red; font-size:18px;"><b>{$confirm}</b></td>
</tr>
<tr>
	<td align="left" class="table1">Image P F M : </td>
	<td class="table2 form01">{if $employeeMasterId > 0}<img src="employeeImage/{$empImage}" width='120px'>{/if}
		<input name="imageEmployee" type="file" id="files">
	</td>
</tr>
<tr>
	<td align="left" class="table1">Name : </td>
	<td class="table2 form01"><input type="text" name="name" autofocus required value="{$name}" /></td>
	<td align="left" class="table1">Father Name : </td>
	<td class="table2 form01"><input type="text" name="fathersName" value="{$fathersName}"></td>
</tr>
<tr>
	<td align="left" class="table1">User Type : </td>
	<td class="table2 form01">
		<select name="userType">
			{html_options values=$typeOutPut output=$typeOutPut selected=$userType}
	  </select>
  </td>
	<td align="left" class="table1">Date Of Birth : </td>
	<td class="table2 form01">
		{html_select_date prefix='dob' field_order="DmY" month_format="%m" start_year='-75' end_year='+25' time=$dateOfBirth day_value_format="%02d" }
	</td>
</tr>
<tr>
	<td align="left" class="table1">Gender: </td>
	<td class="table2 form01">
		<select name="gender">
		{html_options values=$maleValue output=$maleValue selected=$gender}
	</td>
  <td align="left" class="table1">Joining Date : </td>
	<td class="table2 form01">
		{html_select_date prefix='joining' field_order="DmY" month_format="%m" start_year='-25' end_year='+25' time=$joiningDate day_value_format="%02d"}
	</td>
</tr>
<tr>
	<td align="left" class="table1">Date Of Appointment : </td>
	<td class="table2 form01">
		{html_select_date prefix='apointmentDate' field_order="DmY" month_format="%m" start_year='-75' end_year='+25' time=$apointmentDate day_value_format="%02d" }
	</td>
	<td align="left" class="table1">Date Of Confirmation : </td>
	<td class="table2 form01">
		{html_select_date prefix='confirmationDate' field_order="DmY" month_format="%m" start_year='-75' end_year='+25' time=$confirmationDate day_value_format="%02d" }
	</td>
</tr>
{if $employeeMasterId == 0}
	<tr>
		<td align="left" class="table1">Login Id : </td>
		<td class="table2 form01"><input type="text" name="loginId" value="{$loginId}"></td>
		<td align="left" class="table1">Login Password : </td>
		<td class="table2 form01"><input type="text" name="password" value="{$password}"></td>
	</tr>
{/if}
<tr>
	<td align="left" class="table1">Current Address : </td>
	<td class="table2 form01"><textarea cols="21" rows="10" name="currentAddress">{$currentAddress}</textarea></td>
	<td align="left" class="table1">Current Address Pin : </td>
	<td class="table2 form01"><input type="text" name="currentAddressPin" value="{$currentAddressPin}"></textarea></td>
</tr>
<tr>
	<td align="left" class="table1">Correspondence Address : </td>
	<td class="table2 form01"><textarea cols="21" rows="10" name="correspondenceAddress">{$correspondenceAddress}</textarea></td>
	<td align="left" class="table1">Permanent Address : </td>
	<td class="table2 form01"><textarea cols="21" rows="10" name="permanentAddress">{$permanentAddress}</textarea></td>
</tr>
<tr>
	<td align="left" class="table1">Resi. Telephone: </td>
	<td class="table2 form01"><input type="text" name="residenceTelephone" value="{$residenceTelephone}"></td>
	<td align="left" class="table1">Mobile : </td>
	<td class="table2 form01"><input type="text" name="mobile" value="{$mobile}"></td>
</tr>
<tr>
	<td align="left" class="table1">Phone No 1 : </td>
	<td class="table2 form01"><input type="text" name="phone1" value="{$phone1}"></td>
	<td align="left" class="table1">Phone No 2 : </td>
	<td class="table2 form01"><input type="text" name="phone2" value="{$phone2}"></td>
</tr>
<tr>
	<td align="left" class="table1">Email: </td>
	<td class="table2 form01"><input type="text" name="email" value="{$email}"></td>
	<td align="left" class="table1">Mother Name : </td>
	<td class="table2 form01"><input type="text" name="mothersName" value="{$mothersName}"></td>
</tr>
<tr>
	<td align="left" class="table1">Marital : </td>
	<td align="left" class="table2 form01">
		<select name="marital">
			{html_options values=$secArrValue output=$secArrOut selected=$marital}
	  </select>
	</td>
	<td align="left" class="table1">Spouse Name : </td>
	<td class="table2 form01"><input type="text" name="spouseName" value="{$spouseName}"></td>
</tr>
<tr>
	<td align="left" class="table1">Spouse Phone: </td>
	<td class="table2 form01"><input type="text" name="spousePhone" value="{$spousePhone}"></td>
	<td align="left" class="table1">Pf No : </td>
	<td class="table2 form01"><input type="text" name="pfNo" value="{$pfNo}"></td>
</tr>
<tr>
	<td align="left" class="table1">Account No : </td>
	<td class="table2 form01"><input type="text" name="accountNo" value="{$accountNo}"></td>
	<td align="left" class="table1">Pan No : </td>
	<td class="table2 form01"><input type="text" name="panNo" value="{$panNo}"></td>
</tr>
<tr>
	<td align="left" class="table1">Qualification : </td>
	<td class="table2 form01"><input type="text" name="qualification" value="{$qualification}"></td>
	<td align="left" class="table1">Category : </td>
	<td class="table2 form01"><input type="text" name="category" value="{$category}"></td>
</tr>
<tr>
	<td align="left" class="table1">Designation : </td>
	<td class="table2 form01"><input type="text" name="designation" value="{$designation}"></td>
	<td align="left" class="table1">Blood Group : </td>
	<td class="table2 form01"><input type="text" name="bloodGroup" value="{$bloodGroup}"></td>
</tr>
<tr>
	<td colspan="4" align="center"><input type="submit" name="submit" class="newSubmitBtn" value="Save">
</tr>
</table>
</form>
{/block}
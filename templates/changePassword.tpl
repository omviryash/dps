{extends file="./main.tpl"}
{block name=head}
<script language="javascript">
$(document).ready(function() {
  $('#oldPassword').focus();
});
function confNewPasswordMatch()
{
  if(document.form1.newPassword.value != document.form1.confNewPassword.value)
  {
    alert("Password MitchMatch!!!!!");
    document.form1.confNewPassword.value="";
    document.form1.newPassword.focus();
    return false;
  }
  if(document.form1.newPassword.value == document.form1.oldPassword.value)
  {
    alert("Old Password & New Password is Same --> Please Reenter Your New Password!!!!!");
    document.form1.newPassword.value="";
    document.form1.confNewPassword.value="";
    document.form1.newPassword.focus();
    return false;
  }
}
</script>
{/block}
{block name=body}
<form name="form1" action="{$smarty.server.PHP_SELF}" method="POST" onsubmit="return confNewPasswordMatch();">
<table border="1" cellpadding="2" cellspacing="0" align="center">
</br></br></br></br>
<tr>
  <td colspan="8" style="background:#EBF5FF;">
    <div class="hd"><h1 align="center">Change Password</h1></div>
  </td>
</tr>
<tr>
  <td align="right">Old Password : </td>
  <td align="right" class="form01"><INPUT type="password" name="oldPassword" id="oldPassword" REQUIRED /></td>
</tr>
<tr>
  <td align="right">New Password : </td>
  <td align="right" class="form01"><input type="password" name="newPassword" REQUIRED /></td>
</tr>
<tr>
  <td align="right">Re-Type New Password : </td>
  <td align="right" class="form01"><input type="password" name="confNewPassword" onblur="confNewPasswordMatch();" REQUIRED /></td>
</tr>
<tr>
  <td align="center" colspan="2"><input type="submit" name="newPasswordSet" value="Change" class="btnSubmit newSubmitBtn" onsubmit="confNewPasswordMatch();" /></td>
</tr>
</table>
</form>
<center><font color='red' size="15">{$msg}</font></center>
{if $done == 'done'}
<center><font color='red' size="20"><a href="index.php">Home</a></font></center>
{/if}
{/block}
{include file="./main.tpl"}
{block name="head"}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script src="./media1/js/jquery-1.4.4.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.jeditable.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[-1, 10, 20, 30, 40, 50], ["All", 10, 20, 30, 40, 50]],
  	"iDisplayLength": 500,
  	"aaSorting": [[2, 'desc']],
		"bJQueryUI":true
  });
  $(".omAttend").change(function()
  {
  	$('.newGoBtnClick').click();
  });
  
  /* Editable Start*/
//	var oTable = $('#myDataTable').dataTable();
//     
//  /* Apply the jEditable handlers to the table */
//  oTable.$('th').editable( './updateRollNoData.php', {
//      "submitdata": function ( value, settings ) {
//          return {
//              "row_id": this.parentNode.getAttribute('th.id'),
//              "column": oTable.fnGetPosition( this )[2]
//          };
//      },
//      "height": "20px",
//      "width": "100"
//  });
  
  /* Editable End*/
  
});
</script>
{/block}
{block name="body"}
</br></br>
<link rel="stylesheet" href="./css/buttonStyle.css" type="text/css" />
<form name="formGet" method="GET" action="onlineTestScheduleList.php">
<table align="center">
	<tr>
		{if $s_userType == 'Administrator'}
		<td class="table2 form01">
		  <select name="class" autofocus="autofocus" class='omAttend'>
		    <option value="">Select Class</option>
		    {html_options values=$cArray.className output=$cArray.className selected=$class}
		  </select>
	  </td>
	  <td class="table2 form01">
		  <select name="subjectMasterId" class="omAttend">
	      <option value="0">Select Subject</option>
	      {html_options values=$clubArr.subjectMasterId output=$clubArr.subjectName selected=$subjectMasterId}
		  </select>
		</td>
		{else}
		<td class="table2 form01">
		  <select name="subjectAltId" class="omAttend">
			  <option value="">Select Class/Subject</option>
        {html_options values=$scdArray.subjectAltId output=$scdArray.subjectDtl selected=$subjectAltId}
      </select>
    </td>
    {/if}
	  <td class="table2 form01">
		  <select name="startYear">
		    {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYearSelected}
		  </select>
		</td>
    <td>
      <input type="submit" name="submit" class="newGoBtn newGoBtnClick" value="Go">
    </td>
  </tr>
</table>
</form>
<form name="form2" method="POST" action="onlineTestScheduleList.php">
<table align="center" border="1" id="myDataTable" class="display">  
  </br>
	<div class="hd"><h2 align="center">Scheduled Online Test List</h2></div>
	</br>
	<thead>
	<tr>
		<td align="left" class="table1"><b>Date</b></td>
		<td align="left" class="table1"><b>Class</b></td>
		<td align="left" class="table1"><b>Subject</b></td>
		<td align="left" class="table1"><b>Question</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$attendenceArr}
  <tr class="gradeRow">
  	<td align="left" class="table2" NOWRAP >{$attendenceArr[sec].scheduleDate}</td>
    <td align="left" class="table2">{$attendenceArr[sec].class}</td>
    <td align="left" class="table2">{$attendenceArr[sec].subjectName}</td>
    <td align="left" class="table2"><a href='onlineTestScheduleDetail.php?scheduleMasterId={$attendenceArr[sec].scheduleMasterId}'>Question</a></td>
  </tr>
  {/section}
  </tbody>
</table>
</form>
{/block}
{include file="./main.tpl"}
{block name=head}
<style type="text/css" media="screen">
@import "./media/css/demo_table_jui.css";
@import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
input
{ 
  border:1px solid #000;
}
</style>
<script src="./media1/js/jquery-1.4.4.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.jeditable.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
	$('#example').dataTable({
		"aLengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 10,
		"bJQueryUI":true
	});
	/* Editable Start*/
	var oTable = $('#example').dataTable();
     
  /* Apply the jEditable handlers to the table */
  oTable.$('td').editable( './updateEmployeeData.php', {
      "submitdata": function ( value, settings ) {
          return {
              "row_id": this.parentNode.getAttribute('td.id'),
              "column": oTable.fnGetPosition( this )[2]
          };
      },
      "height": "20px",
      "width": "900"
  });
  
  /* Editable End*/
});
</script>
{/block}
{block name="body"}
</br></br>
<table align="left" border="1" id="example" class="display">
  <div class="hd"><h2 align="center">Employee Edit</h2>
	<thead>
	<tr>
		<td align="left"><b>Name</b></td>
		<td align="left"><b>User Type</b></td>
		<td align="left"><b>Login Id</b></td>
		<td align="left"><b>Employee Code</b></td>
		<td align="left"><b>Activated</b></td>
		<td align="left"><b>Current Address</b></td>
		<td align="left"><b>Current Address Pin</b></td>
		<td align="left"><b>Residence Phone</b></td>
		<td align="left"><b>Mobile</b></td>
		<td align="left"><b>Phone1</b></td>
		<td align="left"><b>Email</b></td>
		<td align="left"><b>Permanent Address</b></td>
		<td align="left"><b>Marital</b></td>
		<td align="left"><b>Account No</b></td>
		<td align="left"><b>Qualification</b></td>
		<td align="left"><b>Experience</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$empArray}
  <tr>
  	<td align="left" id="{$empArray[sec].employeeMasterId}_1" NOWRAP >{$empArray[sec].name}</td>
  	<td align="left" id="{$empArray[sec].employeeMasterId}_16" NOWRAP >{$empArray[sec].userType}</td>
  	<td align="left" id="{$empArray[sec].employeeMasterId}_17" NOWRAP >{$empArray[sec].loginId}</td>
  	<td align="left" id="{$empArray[sec].employeeMasterId}_18" NOWRAP >{$empArray[sec].employeeCode}</td>
  	<td align="left" id="{$empArray[sec].employeeMasterId}_19" NOWRAP >{$empArray[sec].activated}</td>
  	<td align="left" id="{$empArray[sec].employeeMasterId}_5" NOWRAP >{$empArray[sec].currentAddress}</td>
    <td align="left" id="{$empArray[sec].employeeMasterId}_6" NOWRAP >{$empArray[sec].currentAddressPin}</td>
    <td align="left" id="{$empArray[sec].employeeMasterId}_7" NOWRAP >{$empArray[sec].residenceTelephone}</td>
    <td align="left" id="{$empArray[sec].employeeMasterId}_8" NOWRAP >{$empArray[sec].mobile}</td>
    <td align="left" id="{$empArray[sec].employeeMasterId}_9" NOWRAP >{$empArray[sec].phone1}</td>
    <td align="left" id="{$empArray[sec].employeeMasterId}_10" NOWRAP >{$empArray[sec].email}</td>
    <td align="left" id="{$empArray[sec].employeeMasterId}_11" NOWRAP >{$empArray[sec].permanentAddress}</td>
    <td align="left" id="{$empArray[sec].employeeMasterId}_12" NOWRAP >{$empArray[sec].marital}</td>
    <td align="left" id="{$empArray[sec].employeeMasterId}_13" NOWRAP >{$empArray[sec].accountNo}</td>
    <td align="left" id="{$empArray[sec].employeeMasterId}_14" NOWRAP >{$empArray[sec].qualification}</td>
    <td align="left" id="{$empArray[sec].employeeMasterId}_15" NOWRAP >{$empArray[sec].experience}</td>
  </tr>
  {/section}
  </tbody>
</table>
{/block}
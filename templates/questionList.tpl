{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
		"aLengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 100,
  	"bAutoWidth": false,
		"bJQueryUI":true
  });
  $(".omAttend").change(function()
  {
  	$('.newGoBtnClick').click();
  });
});
</script>
{/block}
{block name="body"}
</br></br>
<link rel="stylesheet" href="./css/buttonStyle.css" type="text/css" />
<form name="formGet" method="GET" action="questionList.php">
<table align="center">
	<tr>
		{if $s_userType == 'Administrator'}
		<td class="table2 form01">
		  <select name="class" autofocus="autofocus" class='omAttend'>
		    <option value="">Select Class</option>
		    {html_options values=$cArray.className output=$cArray.className selected=$class}
		  </select>
	  </td>
	  <td class="table2 form01">
		  <select name="subjectMasterId" class="omAttend">
	      <option value="0">Select Subject</option>
	      {html_options values=$clubArr.subjectMasterId output=$clubArr.subjectName selected=$subjectMasterId}
		  </select>
		</td>
		{else}
		<td class="table2 form01">
		  <select name="subjectAltId" class="omAttend">
			  <option value="">Select Class/Subject</option>
        {html_options values=$scdArray.subjectAltId output=$scdArray.subjectDtl selected=$subjectAltId}
      </select>
    </td>
    {/if}
    <td>
      <input type="submit" name="submit" class="newGoBtn newGoBtnClick" value="Go">
    </td>
  </tr>
</table>
</form>
<table align="left" border="1" id="myDataTable" class="display">
	 <div class="hd"><h2 align="center">Question List</h2>
	<thead>
	<tr>
		<td align="left"><b>Delete</b></td>
		<td align="left"><b>Edit</b></td>
		<td align="left"><b>Question No</b></td>
		<td align="left"><b>Class</b></td>
		<td align="left"><b>Subject Name</b></td>
		<td align="left"><b>Question</b></td>
		<td align="left"><b>A</b></td>
		<td align="left"><b>B</b></td>
		<td align="left"><b>C</b></td>
		<td align="left"><b>D</b></td>
		<td align="left"><b>Answer</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$empArray}
  <tr>
  	<th align="left"><a href="onlineTestEntry.php?onlineTestId={$empArray[sec].onlineTestId}">Delete</a></th>
  	<th align="left"><a href="onlineTestEntry.php?onlineTestId={$empArray[sec].onlineTestId}">Edit</a></th>
  	<td align="left">{$empArray[sec].onlineTestId}</td>
  	<td align="left">{$empArray[sec].class}</td>
    <td align="left">{$empArray[sec].subjectName}</td>
    <td align="left">{$empArray[sec].question}</td>
    <td align="left">{$empArray[sec].option1}</td>
    <td align="left">{$empArray[sec].option2}</td>
    <td align="left">{$empArray[sec].option3}</td>
    <td align="left">{$empArray[sec].option4}</td>
    <td align="left">{$empArray[sec].answer}</td>
  </tr>
  {/section}
  </tbody>
</table>
{/block}
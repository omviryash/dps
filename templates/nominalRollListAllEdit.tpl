{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<link href="./css/stylePagination.css" rel="stylesheet" type="text/css">
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"iDisplayLength": -1,
		"bJQueryUI":true
  });
});
</script>
{/block}
{block name="body"}
</br></br>
<form name="formGet" method="GET" action="nominalRollListAllEdit.php">
<table align="center" border="1">
	<tr>
		<td>
      Student Gr No : - <input type="text" name="grNo" id="grNo" placeholder="Enter Gr No">
    </td>
    <td>
      <input type="submit" name="" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
<form>
<div class="hd"><h2 align="center">Nominal Roll Report</h2></div>
<table align="left" border="1" id="myDataTable" class="display">
	<thead>
	<tr>
		<td align="left"><b>Edit</b></td>
		<td align="left"><b>Academic Year</b></td>
		<td align="left"><b>GR No</b></td>
		<td align="left"><b>Roll No</b></td>
		<td align="left"><b>Student Name</b></td>
		<td align="left"><b>Activated</b></td>
		<td align="left"><b>Class</b></td>
		<td align="left"><b>Section</b></td>
		<!--td align="left"><b>Fee Group</b></td>
		<td align="left"><b>Library Member Type</b></td>
		<td align="left"><b>Bus Route</b></td>
		<td align="left"><b>Bus Stop</b></td>
		<td align="left"><b>Subject Group</b></td>
		<td align="left"><b>Co Scholastic Group</b></td>
		<td align="left"><b>Club Term 1</b></td>
		<td align="left"><b>Club Term 2</b></td-->
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$nominalArr}
  <tr>
  	<td><a href="nominalRollEntry.php?nominalRollId={$nominalArr[sec].nominalRollId}">Edit</a></td>
  	<td align="left">{$nominalArr[sec].academicYear}</td>
    <td align="left">{$nominalArr[sec].grNo}</td>
    <td align="left">{$nominalArr[sec].rollNo}</td>
    <td align="left">{$nominalArr[sec].studentName}</td>
    <td align="left">{$nominalArr[sec].activated}</td>
    <td align="left">{$nominalArr[sec].class}</td>
    <td align="left">{$nominalArr[sec].section}</td>
    <!--td align="left">{$nominalArr[sec].feeGroup}</td>
    <td align="left">{$nominalArr[sec].libraryMemberType}</td>
    <td align="left">{$nominalArr[sec].busRoute}</td>
    <td align="left">{$nominalArr[sec].busStop}</td>
    <td align="left">{$nominalArr[sec].subjectGroup}</td>
    <td align="left">{$nominalArr[sec].coScholasticGroup}</td>
    <td align="left">{$nominalArr[sec].clubTerm1}</td>
    <td align="left">{$nominalArr[sec].clubTerm2}</td-->
  </tr>
  {/section}
  </tbody>
</table>
{/block}
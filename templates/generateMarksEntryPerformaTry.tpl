{include file="./main.tpl"}
{block name="head"}
<link rel="stylesheet" href="./css/buttonStyle.css" type="text/css" />
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript">
$(document).ready(function(){
	$('#myDataTable').dataTable({
		"aLengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 100,
  	"bAutoWidth": false,
  	"aaSorting": [[ 0, "asc" ]],
		"bJQueryUI":true
  });
	$(".omAttend").change(function()
  {
  	$('.newGoBtn').click();
  });
});


function buttonHide()
{
	$('#hideMe').hide();
}

</script>
{/block}
{block name="body"}
</br></br>
<center>
<form name="formGet" id="formGet" method="GET" action="generateMarksEntryPerformaTry.php">
<table align="center" border="1">
	<div class="hd"><h2 align="center">Generate Report Card</h2></div>
	<tr>
		<td class="table2 form01">
		  <select name="startYear" class="omAttend">
		    {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYearSelected}
		  </select>
		</td>
		{if $s_userType == 'Administrator'}
		<td class="table2 form01 omAttend">
		  <select name="class" autofocus="autofocus">
		    <option value="0">Select class</option>
		    {html_options values=$cArray.className output=$cArray.className selected=$class}
		  </select>
	  </td>
    <td class="table2 form01 omAttend">
		  <select name="section">
		    <option value="0">Select Section</option>
		    {html_options values=$secArrOut output=$secArrOut selected=$section}
		  </select>
	  </td>
	  {/if}
	  <td>
	    <input type="submit" name="go" class="newGoBtn" value="go">
	  </td>
	</tr>
</table>
{if $count == 0}
<table align="center" border="0">
  </br>
  <tr>
    <td>
      {if $class > 5 && $class <= 10}
	    <input type="submit" name="submit" onclick="buttonHide();" id="hideMe" class="newSubmitBtn" value="Generate">
	    {/if}
	  </td>
	</tr>
</table>
{/if}
</form>
</center>
<form name="form2" method="POST" action="generateMarksEntryPerformaTry.php">
<table align="center" border="1" id="myDataTable" class="display">
  </br></br></br>
	<h1 align="center">Student List</h1>
	</br>
	<h1 align="center">{$class}/{$section}</h1>
  </br>
  <thead>
	<tr>
		<td align="left" class="table1"><b>Roll.No.</b></td>
		<td align="left" class="table1"><b>G. R. No.</b></td>
		<td align="left" class="table1"><b>Name</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$stdArray}
  <tr class="trRow">
    <td align="center" class="table2">{$stdArray[sec].rollNo}</td>
    <td align="left" class="table2">
    	{$stdArray[sec].grNo} <input type="hidden" name="grNo[]" value="{$stdArray[sec].grNo}">
    </td>
    <td align="left" class="table2">{$stdArray[sec].studentName}</td>
  </tr>
  {/section}
  </tr>
  </tbody>
</table>
</form>
{/block}
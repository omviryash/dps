{include file="./main.tpl"}
{block name=head}
<link href="./css/stylePagination.css" rel="stylesheet" type="text/css">
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script src="media2/js/jquery.dataTables.columnFilter.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable(
  {
  	"iDisplayLength": -1,
		"bJQueryUI":true
  });
  /* Large Image Show Start*/

	$("#limit").change(function()
  {
  	formGet.submit();
  });
	/* Large Image Show End*/
});

</script>
{/block}
{block name="body"}
</br></br>
<div class="hd"><h2 align="center">Student List</h2></div>
<form name="formGet" id="formGet" method="GET" action="studentAllListNew.php">
<table align="left" border="1">
	<tr>
		<td>
      <select name="limit" id="limit">
      	{html_options values=$filterArr output=$filterArr selected=$limit}
      </select>
    </td>
  </tr>
</table>
</form>
<table align="left" border="1" id="myDataTable" class="display dataTables_length">
	<thead>
	<tr>
		<td align="left"><b></b></td>
		<td align="left"><b></b></td>
		<td align="left"><b>G. R. No.</b></td>
		<td align="left"><b>Name</b></td>
		<td align="left"><b>Father Name</b></td>
		<td align="left"><b>Mother Name</b></td>
		<td align="left"><b>Active</b></td>
		<td align="left"><b>Date Of Birth</b></td>
		<td align="left"><b>Joined In Class</b></td>
		<td align="left"><b>Joining Date</b></td>
		<td align="left"><b>House Name</b></td>
		<td align="left"><b>Gender</b></td>
		<td align="left"><b>Current Address</b></td>
		<td align="left"><b>Residence Phone1</b></td>
		<td align="left"><b>Residence Phone2</b></td>
		<td align="left"><b>Email</b></td>
		<td align="left"><b>Permanent Address</b></td>
		<td align="left"><b>Religion</b></td>
  </tr>
  </thead>
  <!--thead>
  <tr>
    <th align="left"><b>G. R. No.</b></th>
		<th align="left"><b>Name</b></th>
		<th align="left"><b>Father Name</b></th>
		<th align="left"><b>Mother Name</b></th>
		<th align="left"><b>Active</b></th>
		<th align="left"><b>Date Of Birth</b></th>
		<th align="left"><b>Joined In Class</b></th>
		<th align="left"><b>Joining Date</b></th>
		<th align="left"><b>House Name</b></th>
		<th align="left"><b>Gender</b></th>
		<th align="left"><b>Current Address</b></th>
		<th align="left"><b>Residence Phone1</b></th>
		<th align="left"><b>Residence Phone2</b></th>
		<th align="left"><b>Email</b></th>
		<th align="left"><b>Permanent Address</b></th>
		<th align="left"><b>Religion</b></th>
	</tr>
  </thead-->
  <tbody>
  {section name="sec" loop=$stdArray}
  <tr>
  	<th align="left"><a href="studentEntry.php?studentMasterId={$stdArray[sec].studentMasterId}">Edit</a></th>
  	<th align="left"><a href="nominalRollListAll.php?grNo={$stdArray[sec].grNo}">Nominal</a></th>
  	<td align="left">{$stdArray[sec].grNo}</td>
  	<td align="left">{$stdArray[sec].studentName}</td>
    <td align="left">{$stdArray[sec].fatherName}</td>
    <td align="left">{$stdArray[sec].mothersName}</td>
    <td align="left">{$stdArray[sec].activated}</td>
    <td align="left">{$stdArray[sec].dateOfBirth}</td>
    <td align="left">{$stdArray[sec].joinedInClass}</td>
    <td align="left">{$stdArray[sec].joiningDate}</td>
    <td align="left">{$stdArray[sec].houseName}</td>
    <td align="left">{$stdArray[sec].gender}</td>
    <td align="left">{$stdArray[sec].currentAddress}</td>
    <td align="left">{$stdArray[sec].residencePhone1}</td>
    <td align="left">{$stdArray[sec].residencePhone2}</td>
    <td align="left">{$stdArray[sec].studentEmail}</td>
    <td align="left">{$stdArray[sec].permanentAddress}</td>
    <td align="left">{$stdArray[sec].religion}</td>
  </tr>
  {/section}
  </tbody>
</table>
<table width="100%" border="0" align="center" style="margin-left:;" cellpadding="3" cellspacing="0">
<tr>
  <td width="100%" align="center" class="">{$pagination}</td>
</tr>
<tr>
  <td align="center" valign="top">
  </td>
</tr>
<tr>
  <td> </td>
</tr>
</table>
{/block}
{include file="./main.tpl"}
{block name=head}
<style type="text/css" media="screen">
@import "./media/css/demo_table_jui.css";
@import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
input
{ 
  border:1px solid #000;
}
</style>
<script src="./media1/js/jquery-1.4.4.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.jeditable.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function() {
	$('#grNo').focus();
	$('#example').dataTable({
		"bJQueryUI":true
	});
	/* Editable Start*/
	var oTable = $('#example').dataTable();
     
  /* Apply the jEditable handlers to the table */
  oTable.$('td').editable( './updateStudentData.php', {
      "submitdata": function ( value, settings ) {
          return {
              "row_id": this.parentNode.getAttribute('td.id'),
              "column": oTable.fnGetPosition( this )[2]
          };
      },
      "height": "20px",
      "width": "400"
  });
  
  /* Editable End*/
});
</script>
{/block}
{block name="body"}
</br></br>
<form name="formGet" method="GET" action="studentAllListEdit.php">
<table align="center" border="1">
	<tr>
		<td>
      Student Gr No : - <input type="text" name="grNo" id="grNo" placeholder="Enter Gr No">
    </td>
    <td>
      <input type="submit" name="" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
<form>
</br></br>	
<table align="left" border="1" id="example" class="display">
	<div class="hd"><h2 align="center">Student List</h2>
	<thead>
	<tr>
		<th align="left"><b>&nbsp;</b></th>
		<th align="left"><b>G. R. No.</b></th>
		<th align="left"><b>Activated</b></th>
		<th align="left"><b>Name</b></th>
		<th align="left"><b>Father Name</b></th>
		<th align="left"><b>Date Of Birth</b></th>
		<th align="left"><b>Joined In Class</b></th>
		<th align="left"><b>Joining Date</b></th>
		<th align="left"><b>Gender</b></th>
		<th align="left"><b>House Name</b></th>
		<th align="left"><b>Current Address</b></th>
		<th align="left"><b>Current State</b></th>
		<th align="left"><b>Current Pin</b></th>
		<th align="left"><b>Residence Phone1</b></th>
		<th align="left"><b>SMS Mobile</b></th>
		<th align="left"><b>Email</b></th>
		<th align="left"><b>Permanent Address</b></th>
		<th align="left"><b>Permanent State</b></th>
		<th align="left"><b>Permanent Pin</b></th>
		<th align="left"><b>Religion</b></th>
		<th align="left"><b>Aadhaar No.</b></th>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$stdArray}
  <tr>
  	<th align="left"><a href="studentEntry.php?studentMasterId={$stdArray[sec].studentMasterId}">Edit</a></th>
  	<th align="left" id="{$stdArray[sec].studentMasterId}_55" NOWRAP >{$stdArray[sec].grNo}</th>
  	<td align="left" id="{$stdArray[sec].studentMasterId}_3" NOWRAP >{$stdArray[sec].activated}</td>
  	<td align="left" id="{$stdArray[sec].studentMasterId}_1" NOWRAP >{$stdArray[sec].studentName}</td>
    <td align="left" id="{$stdArray[sec].studentMasterId}_2" NOWRAP >{$stdArray[sec].fatherName}</td>
    <td align="left" id="{$stdArray[sec].studentMasterId}_4" NOWRAP >{$stdArray[sec].dateOfBirth|date_format:'%d-%m-%Y'}</td>
    <td align="left" id="{$stdArray[sec].studentMasterId}_5" NOWRAP >{$stdArray[sec].joinedInClass}</td>
    <td align="left" id="{$stdArray[sec].studentMasterId}_6" NOWRAP >{$stdArray[sec].joiningDate|date_format:'%d-%m-%Y'}</td>
    <td align="left" id="{$stdArray[sec].studentMasterId}_7" NOWRAP >{$stdArray[sec].gender}</td>
    
    <th align="left" id="{$stdArray[sec].studentMasterId}_8" NOWRAP >{$stdArray[sec].houseName}</th>
    <td align="left" id="{$stdArray[sec].studentMasterId}_9" NOWRAP >{$stdArray[sec].currentAddress}</td>
    <td align="left" id="{$stdArray[sec].studentMasterId}_10" NOWRAP >{$stdArray[sec].currentState}</td>
    <td align="left" id="{$stdArray[sec].studentMasterId}_11" NOWRAP >{$stdArray[sec].currentAddressPin}</td>
    
    <td align="left" id="{$stdArray[sec].studentMasterId}_12" NOWRAP >{$stdArray[sec].residencePhone1}</td>
	<td align="left" id="{$stdArray[sec].studentMasterId}_13" NOWRAP >{$stdArray[sec].smsMobile}</td>
    <td align="left" id="{$stdArray[sec].studentMasterId}_14" NOWRAP >{$stdArray[sec].studentEmail}</td>
    
    <td align="left" id="{$stdArray[sec].studentMasterId}_15" NOWRAP >{$stdArray[sec].permanentAddress}</td>
    <td align="left" id="{$stdArray[sec].studentMasterId}_16" NOWRAP >{$stdArray[sec].permanentState}</td>
    <td align="left" id="{$stdArray[sec].studentMasterId}_17" NOWRAP >{$stdArray[sec].permanentPIN}</td>
    
    <td align="left" id="{$stdArray[sec].studentMasterId}_18" NOWRAP >{$stdArray[sec].religion}</td>
    <td align="left" id="{$stdArray[sec].studentMasterId}_19" NOWRAP >{$stdArray[sec].aadhaar}</td>
  </tr>
  {sectionelse}
  <tr>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
    <td align="center"><h2>{$notFound}</h2></td>
	<td align="center"><h2>{$notFound}</h2></td>
	<td align="center"><h2>{$notFound}</h2></td>
  </tr>
  {/section}
  </tbody>
</table>
{/block}

<form action="getEng.php" method="POST">
<input type="hidden" name="subjectName" value="{$subjectName}">
<input type="hidden" name="termValue" value="{$termValue}">
<table align="left" border="2">
 <tr>
 	<!--for eng and hindi : Start-->
 	<td align="center">Student Name</td>
 	<td align="center">Pronunciation</td>
 	<td align="center">Fluency</td>
 	<td align="center">Comprehension</td>
 	<td align="center">Creative Writing</td>
 	<td align="center">Hand Writing</td>
 	<td align="center">Grammar</td>
 	<td align="center">Spellings</td>
 	<td align="center">Vocabulary</td>
 	<td align="center">Conversation</td>
 	<td align="center">Recitation</td>
 	<td align="center">Clarity</td>
 	<td align="center">Comprehension</td>
 	<td nowrap align="center">Concentration Span</td>
 	<td nowrap align="center">Extra Reading</td>
 	<td nowrap align="center">Activity / Project</td> 
 	<!--for eng and hidi : end -->
 </tr>
{section name="sec" loop=$nameArray}
<tr>
	<td nowrap>{$nameArray[sec].studentName}</td>
	<input type="hidden" name="grNo[]" value="{$nameArray[sec].grNo}">
	<!--for eng and hidi : Start-->
      <td>
        <select name="eReadingPro[]">
        	<option value="">Select Grade</option>
        	  {html_options values=$gradeArray.gradeId output=$gradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].eReadingPro}
        </select>
      </td>
      <td>
        <select name="eReadingFlu[]">
        	<option value="">Select Grade</option>
        		{html_options values=$gradeArray.gradeId output=$gradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].eReadingFlu}
        </select>
      </td>
      <td>
        <select name="eReadingCom[]">
        	<option value="">Select Grade</option>
        		{html_options values=$gradeArray.gradeId output=$gradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].eReadingCom}
        </select>
      </td>
      <td>
        <select name="eWritingCre[]">
        	<option value="">Select Grade</option>
        		{html_options values=$gradeArray.gradeId output=$gradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].eWritingCre}
        </select>
      </td>
      <td>
        <select name="eWritingHan[]">
        	<option value="">Select Grade</option>
        		{html_options values=$gradeArray.gradeId output=$gradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].eWritingHan}
        </select>
      </td>
      <td>
        <select name="eWritingGra[]">
        	<option value="">Select Grade</option>
        		{html_options values=$gradeArray.gradeId output=$gradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].eWritingGra}
        </select>
      </td>
      <td>
        <select name="eWritingSpe[]">
        	<option value="">Select Grade</option>
        		{html_options values=$gradeArray.gradeId output=$gradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].eWritingSpe}
        </select>
      </td>
      <td>
        <select name="eWritingVoc[]">
        	<option value="">Select Grade</option>
        		{html_options values=$gradeArray.gradeId output=$gradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].eWritingVoc}
        </select>
      </td>
      <td>
        <select name="ewSpeakinCon[]">
        	<option value="">Select Grade</option>
        		{html_options values=$gradeArray.gradeId output=$gradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].ewSpeakinCon}
        </select>
      </td>
      <td>
        <select name="ewSpeakinRec[]">
        	<option value="">Select Grade</option>
        		{html_options values=$gradeArray.gradeId output=$gradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].ewSpeakinRec}
        </select>
      </td>
      <td>
        <select name="ewSpeakinCla[]">
        	<option value="">Select Grade</option>
        		{html_options values=$gradeArray.gradeId output=$gradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].ewSpeakinCla}
        </select>
      </td>
      <td>
        <select name="eListingComp[]">
        	<option value="">Select Grade</option>
        		{html_options values=$gradeArray.gradeId output=$gradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].eListingComp}
        </select>
      </td>
      <td>
        <select name="eListingCon[]">
        	<option value="">Select Grade</option>
        		{html_options values=$gradeArray.gradeId output=$gradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].eListingCon}
        </select>
      </td>
      <td>
        <select name="extraReading[]">
        	<option value="">Select Grade</option>
        		{html_options values=$gradeArray.gradeId output=$gradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].extraReading}
        </select>
      </td>
      <td>
        <select name="activityPro[]">
        	<option value="">Select Grade</option>
        		{html_options values=$gradeArray.gradeId output=$gradeArray.grade selected=$gradeSelectionArray[$nameArray[sec].grNo].activityPro}
        </select>
      </td>
      <!--for eng and hidi : End--> 
</tr>
{/section}
<tr>
	<td colspan="16" align="center"><input type="submit" value="Save" name="update" class="newSubmitBtn">
</tr>
</table>
</form>
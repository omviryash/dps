{block name="head"}
<style>
table,td
{
border-width:1px;
border-color:"black";
} 
</style>
{/block}
{block name="body"}
<br><br>
<!--eng Start-->
<table>
	<tr align="center">
		<td><a href="reportCardPrint1to5.php?grNo={$grNo}" target=blank>Print</a>
  </tr>
</table>
<table align="left" width=35% border="1">
	<tr>
		<td align="center" colspan=3><b>English</b></td>
	</tr>
	<tr>
		<td align="left"><b>Aspects</b></td>
		<td align="left"><b>Term 1</b></td>
		<td align="left"><b>Term 2</b></td>
  </tr>
  <tr>
  	<td colspan="3">Reading Skills</td>
  </tr>
  <tr>
  	<td>Pronunciation</td>
  	<td>{$eReadingPro}</td>
  	<td>{$eReadingPro1}</td>
  </tr>
  <tr>
  	<td>Fluency</td>
  	<td>{$eReadingFlu}</td>
  	<td>{$eReadingFlu1}</td>
  </tr>
  <tr>
  	<td>Comprehension</td>
  	<td>{$eReadingCom}</td>
  	<td>{$eReadingCom1}</td>
  </tr>
  <tr>
		<td align="center" colspan=3><b>Writing Skills</b></td>
	</tr>
	<tr>
		<td align="left"><b>Creative Writing</b></td>
		<td align="left">{$eWritingCre}</td>
		<td align="left">{$eWritingCre1}</td>
  </tr>
  <tr>
  	<td>Hand Writing</td>
  	<td>{$eWritingHan}</td>
  	<td>{$eWritingHan1}</td>
  </tr>
  <tr>
  	<td>Grammar</td>
  	<td>{$eWritingGra}</td>
  	<td>{$eWritingGra1}</td>
  </tr>
  <tr>
  	<td>Spellings</td>
  	<td>{$eWritingSpe}</td>
  	<td>{$eWritingSpe1}</td>
  </tr>
  <tr>
  	<td>Vocabulary</td>
  	<td>{$eWritingVoc}</td>
  	<td>{$eWritingVoc1}</td>
  </tr>
  <tr>
		<td align="center" colspan=3><b>Speaking Skills</b></td>
	</tr>
	<tr>
		<td align="left"><b>Conversation</b></td>
		<td align="left">{$ewSpeakinCon}</td>
		<td align="left">{$ewSpeakinCon1}</td>
  </tr>
  <tr>
  	<td>Recitation</td>
  	<td>{$ewSpeakinRec}</td>
  	<td>{$ewSpeakinRec1}</td>
  </tr>
  <tr>
  	<td>Clarity</td>
  	<td>{$ewSpeakinCla}</td>
  	<td>{$ewSpeakinCla1}</td>
  </tr>
  <tr>
		<td align="center" colspan=3><b>Listening Skills</b></td>
	</tr>
	<tr>
		<td align="left"><b>Comprehension</b></td>
		<td align="left">{$eListingComp}</td>
		<td align="left">{$eListingComp1}</td>
  </tr>
  <tr>
  	<td>Concentration Span</td>
  	<td>{$eListingCon}</td>
  	<td>{$eListingCon1}</td>
  </tr>
  <tr>
  	<td>Extra Reading</td>
  	<td>{$extraReading}</td>
  	<td>{$extraReading1}</td>
  </tr>
   <tr>
  	<td>Activity / Project</td>
  	<td>{$activityPro}</td>
  	<td>{$activityPro1}</td>
  </tr>
</table>
<!--eng end-->
<!--hindi Start-->
<table align="center" border="1" width=40%>
	<tr>
		<td align="center" colspan=3><b>Hindi</b></td>
	</tr>
	<tr>
		<td align="left"><b>Aspects</b></td>
		<td align="left"><b>Term 1</b></td>
		<td align="left"><b>Term 2</b></td>
  </tr>
  <tr>
  	<td colspan="3">Reading Skills</td>
  </tr>
  <tr>
  	<td>Pronunciation</td>
  	<td>{$hiReadingPro}</td>
  	<td>{$hiReadingPro1}</td>
  </tr>
  <tr>
  	<td>Fluency</td>
  	<td>{$hiReadingFlu}</td>
  	<td>{$hiReadingFlu1}</td>
  </tr>
  <tr>
  	<td>Comprehension</td>
  	<td>{$hiReadingCom}</td>
  	<td>{$hiReadingCom1}</td>
  </tr>
  <tr>
		<td align="center" colspan=3><b>Writing Skills</b></td>
	</tr>
	<tr>
		<td align="left"><b>Creative Writing</b></td>
		<td align="left">{$hiWritingCre}</td>
		<td align="left">{$hiWritingCre1}</td>
  </tr>
  <tr>
  	<td>Hand Writing</td>
  	<td>{$hiWritingHan}</td>
  	<td>{$hiWritingHan1}</td>
  </tr>
  <tr>
  	<td>Grammar</td>
  	<td>{$hiWritingGra}</td>
  	<td>{$hiWritingGra1}</td>
  </tr>
  <tr>
  	<td>Spellings</td>
  	<td>{$hiWritingSpe}</td>
  	<td>{$hiWritingSpe1}</td>
  </tr>
  <tr>
  	<td>Vocabulary</td>
  	<td>{$hiWritingVoc}</td>
  	<td>{$hiWritingVoc1}</td>
  </tr>
  <tr>
		<td align="center" colspan=3><b>Speaking Skills</b></td>
	</tr>
	<tr>
		<td align="left"><b>Conversation</b></td>
		<td>{$hiwSpeakinCon}</td>
		<td>{$hiwSpeakinCon1}</td>
  </tr>
  <tr>
  	<td>Recitation</td>
  	<td>{$hiwSpeakinRec}</td>
  	<td>{$hiwSpeakinRec1}</td>
  </tr>
  <tr>
  	<td>Clarity</td>
  	<td>{$hiwSpeakinCla}</td>
  	<td>{$hiwSpeakinCla1}</td>
  </tr>
  <tr>
		<td align="center" colspan=3><b>Listening Skills</b></td>
	</tr>
	<tr>
		<td align="left"><b>Comprehension</b></td>
		<td align="left">{$hiListingComp}</td>
		<td align="left">{$hiListingComp1}</td>
  </tr>
  <tr>
  	<td>Concentration Span</td>
  	<td>{$hiListingCon}</td>
  	<td>{$hiListingCon1}</td>
  </tr>
  <tr>
  	<td>Extra Reading</td>
  	<td>{$hiextraReading}</td>
  	<td>{$hiextraReading1}</td>
  </tr>
   <tr>
  	<td>Activity / Project</td>
  	<td>{$hiactivityPro}</td>
  	<td>{$hiactivityPro1}</td>
  </tr>
</table>
<!--hindi end-->
<!-- Maths Start---->
<table align="left" border="1" width=35%>
	<tr>
		<td align="center" colspan=3><b>Mathematics</b></td>
	</tr>
	<tr>
		<td align="left"><b>Aspects</b></td>
		<td align="left"><b>Term 1</b></td>
		<td align="left"><b>Term 2</b></td>
  </tr>
  <tr>
  	<td>Concept</td>
  	<td>{$mathAspCon}</td>
  	<td>{$mathAspCon1}</td>
  </tr>
  <tr>
  	<td>Mental Ability</td>
  	<td>{$mathAspMen}</td>
  	<td>{$mathAspMen1}</td>
  </tr>
  <tr>
  	<td>Activity / Project</td>
  	<td>{$mathAspActi}</td>
  	<td>{$mathAspActi1}</td>
  </tr>
</table>
<!-- Maths end---->
<!-- envirment Start---->
<table align="center" border="1" width=40%>
	<tr>
		<td align="center" colspan=3><b>Enviment Science Genral Knowledge</b></td>
	</tr>
	<tr>
		<td align="left"><b>Aspects</b></td>
		<td align="left"><b>Term 1</b></td>
		<td align="left"><b>Term 2</b></td>
  </tr>
  <tr>
  	<td>Environmental Sensitivity</td>
  	<td>{$envAspenv}</td>
  	<td>{$envAspenv1}</td>
  </tr>
  <tr>
  	<td>Group Discussion</td>
  	<td>{$envGroupDis}</td>
  	<td>{$envGroupDis1}</td>
  </tr>
  <tr>
  	<td>Activity / Project</td>
  	<td>{$envActi}</td>
  	<td>{$envActi1}</td>
  </tr>
</table>
<!--envirment End---->
<!-- computer Start---->
<table align="left" border="1" width=35%>
	<tr>
		<td align="center" colspan=3><b>Computer</b></td>
	</tr>
	<tr>
		<td align="left"><b>Aspects</b></td>
		<td align="left"><b>Term 1</b></td>
		<td align="left"><b>Term 2</b></td>
  </tr>
  <tr>
  	<td>Skill</td>
  	<td>{$comSkills}</td>
  	<td>{$comSkills1}</td>
  </tr>
  <tr>
  	<td>Aptitude</td>
  	<td>{$comAptitude}</td>
  	<td>{$comAptitude1}</td>
  </tr>
</table>
<!--computer End---->
<!-- health Start---->
<table align="center" border="1" width=40%>
	<tr>
		<td align="center" colspan=3><b>Health</b></td>
	</tr>
	<tr>
		<td align="left"><b>Aspects</b></td>
		<td align="left"><b>Term 1</b></td>
		<td align="left"><b>Term 2</b></td>
  </tr>
  <tr>
  	<td>Height(Cm.)</td>
  	<td>{$studHei}</td>
  	<td>{$studHei1}</td>
  </tr>
  <tr>
  	<td>Weight(Kg.)</td>
  	<td>{$studWei}</td>
  	<td>{$studWei1}</td>
  </tr>
</table>
<!--health End---->
<!---Personality Development Start-->
<table align="left" border="1" width=35%>
	<tr>
		<td align="left" colspan=3><b>Personality Development</b></td>
	</tr>
	<tr>
		<td align="left"><b>Aspects</b></td>
		<td align="left"><b>Term 1</b></td>
		<td align="left"><b>Term 2</b></td>
  </tr>
  <tr>
  	<td>Couteousness</td>
  	<td>{$perCou}</td>
  	<td>{$perCou1}</td>
  </tr>
  <tr>
  	<td>Confidence</td>
  	<td>{$perCon}</td>
  	<td>{$perCon1}</td>
  </tr>
  <tr>
  	<td>Care Of Belongings</td>
  	<td>{$perCar}</td>
  	<td>{$perCar1}</td>
  </tr>
	<tr>
		<td align="left">Neatness</td>
		<td align="left">{$perNea}</td>
		<td align="left">{$perNea1}</td>
  </tr>
  <tr>
  	<td>Punctuality</td>
  	<td>{$perPun}</td>
  	<td>{$perPun1}</td>
  </tr>
  <tr>
  	<td>Regularity And Punctuality</td>
  	<td>{$perRegAndPun}</td>
  	<td>{$perRegAndPun1}</td>
  </tr>
  <tr>
  	<td>Inactive</td>
  	<td>{$perIni}</td>
  	<td>{$perIni1}</td>
  </tr>
  <tr>
  	<td>Shareing And Caring</td>
  	<td>{$perSha}</td>
  	<td>{$perSha1}</td>
  </tr>
	<tr>
		<td align="left">Respect For Others Property</td>
		<td>{$perRes}</td>
		<td>{$perRes1}</td>
  </tr>
  	<td>Self Control</td>
  	<td>{$persel}</td>
  	<td>{$persel1}</td>
</table>
<!--Personality Development end-->
<!---Co-curricular Activity Start-->
<table align="center" border="1" width=40%>
	<tr>
		<td align="left" colspan=3><b>Co-Curricular Activities</b></td>
	</tr>
	<tr>
		<td align="left"><b>Physical Education</b></td>
		<td align="left"><b>Term 1</b></td>
		<td align="left"><b>Term 2</b></td>
  </tr>
  <tr>
  	<td>Enthusiasm</td>
  	<td>{$coPhyEth}</td>
  	<td>{$coPhyEth1}</td>
  </tr>
  <tr>
  	<td>Discipline</td>
  	<td>{$coPhyDis}</td>
  	<td>{$coPhyDis1}</td>
  </tr>
  <tr>
  	<td>Team Sprit</td>
  	<td>{$coPhyTea}</td>
  	<td>{$coPhyTea1}</td>
  </tr>
	<tr>
		<td align="left">Talent</td>
		<td align="left">{$coPhyTal}</td>
		<td align="left">{$coPhyTal1}</td>
  </tr>
  <tr>
		<td align="left"><b>Activity / Club</b></td>
		<td>{$coActSki}</td>
		<td>{$coActSki1}</td>
  </tr>
  <tr>
  	<td>Interest</td>
  	<td>{$coActInt}</td>
  	<td>{$coActInt1}</td>
  </tr>
  <tr>
  	<td>Creativity</td>
  	<td>{$coActCre}</td>
  	<td>{$coActCre1}</td>
  </tr>
  <tr>
		<td align="left"><b>Music(Vocal/Instrumental)</b></td>
		<td align="left"></td>
		<td align="left"></td>
  </tr>
  <tr>
  	<td>Interest</td>
  	<td>{$coMusicint}</td>
  	<td>{$coMusicint1}</td>
  </tr>
  <tr>
  	<td>Rhythm</td>
  	<td>{$coMusicRhy}</td>
  	<td>{$coMusicRhy1}</td>
  </tr>
	<tr>
		<td align="left">Melody</td>
		<td align="left">{$coMusicMel}</td>
		<td align="left">{$coMusicMel1}</td>
  </tr>
  <tr>
  	<td><b>Art</b></td>
  	<td></td>
  	<td></td>
  </tr>
  <tr>
  	<td>Interest</td>
  	<td>{$coArtInt}</td>
  	<td>{$coArtInt1}</td>
  </tr>
  <tr>
  	<td>Creativity</td>
  	<td>{$coArtCre}</td>
  	<td>{$coArtCre1}</td>
  </tr>
  <tr>
  	<td>Presntation</td>
  	<td>{$coArtPre}</td>
  	<td>{$coArtPre1}</td>
  </tr>
   <tr>
  	<td><b>Dance</b></td>
  	<td></td>
  	<td></td>
  </tr>
   <tr>
  	<td>Interest</td>
  	<td>{$coDanceInt}</td>
  	<td>{$coDanceInt1}</td>
  </tr>
   <tr>
  	<td>Action</td>
  	<td>{$coDanceAct}</td>
  	<td>{$coDanceAct1}</td>
  </tr>
   <tr>
  	<td>Expression</td>
  	<td>{$coDanceExp}</td>
  	<td>{$coDanceExp1}</td>
  </tr>
</table>
<!--Co-curricular Activity end-->
{/block}
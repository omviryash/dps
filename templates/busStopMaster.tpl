{include file="./main.tpl"}
{block name="body"}
<form name="form1" method="POST" action="busStopMaster.php">
<table align="center" border="0" class="table2">
<div class="hd"><h2 align="center">Bus Stop Entry</h2>
<input type="hidden" name="busStopMasterId" value="{$busStopMasterId}">
<tr>
  <td align="center" colspan="2" style="color:red; font-size:18px;"><b>{$confirm}</b></td>
</tr>
<tr>
  <td class="table2">Vehicle</td>
  <td class="table2 form01">
  	<select name="vehicleMasterId" id="vehicleMasterId" REQUIRED Autofocus >
      {html_options values=$typeArr.vehicleMasterId output=$typeArr.vehicleNo selected=$vehicleMasterId}
    </select>
	</td>
</tr>
<tr>
  <td class="table2">Route</td>
  <td class="table2 form01">
  	<select name="routeMasterId" id="routeMasterId" REQUIRED Autofocus >
      {html_options values=$rArr.routeMasterId output=$rArr.routeName selected=$routeMasterId}
    </select>
	</td>
</tr>
<tr>
  <td class="table2">Time</td>
  <td class="table2 form01">
  	{html_select_time prefix=busTime use_24_hours=true display_seconds=false time=$busTime}
	</td>
</tr>
<tr>
  <td class="table2">Bus Stop</td>
  <td class="table2 form01">
  	<input type="text" name="busStop" value="{$busStop}" />
	</td>
</tr>
<tr>
  <td class="table2">Distance</td>
  <td class="table2 form01">
  	<input type="text" name="distance" value="{$distance}" />
	</td>
</tr>
<tr>
  <td class="table2"></td>
  <td class="table2 form01" align="center"><input type="submit" name="Submit" class="button" value="Save"></td>
</tr>
</table>
</form>
{/block}
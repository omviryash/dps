<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] != 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$attendenceArr       = array();
	$markArr             = array();
	$attendenceEndDate   = "";
	$attendenceStartDate = "";
	$academicStartYear   = "";
	$academicEndYear     = "";
  
	$totalNumberWorkingDay = 0;
	
	if(isset($_REQUEST['startYear']))
  {
  	$academicStartYear = $_REQUEST['startYear']."-04-01";
  	$nextYear          = $_REQUEST['startYear'] + 1;
  	$academicEndYear   = $nextYear."-03-31";
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y')."-04-01";
	  	$nextYear          = date('Y') + 1;
	  	$academicEndYear   = $nextYear."-03-31";
		}
		else
		{
			$prevYear          = date('Y') - 1;
			$academicStartYear = $prevYear."-04-01";
	  	$academicEndYear   = date('Y')."-03-31";
		}
	}
	
	if(isset($_REQUEST['startYear']))
  {
	  $attendenceStartDate = $_REQUEST['startYear'];
	  $attendenceEndDate   = $_REQUEST['startYear'] + 1;
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$attendenceStartDate = date('Y')."-04-01";
	  	$nextYear            = date('Y') + 1;
	  	$attendenceEndDate   = $nextYear;
		}
		else
		{
			$prevYear            = date('Y') - 1;
			$attendenceStartDate = $prevYear."-04-01";
	  	$attendenceEndDate   = date('Y');
		}
	}
		
	$class   = isset($_REQUEST['class']) ? $_REQUEST['class'] : 0;
	$section = isset($_REQUEST['classSection']) ? $_REQUEST['classSection'] : 0;
	
  $i = 0;
  $selectNominal = "SELECT studentmaster.grNo,studentmaster.activated,
                           studentmaster.studentLoginId,studentmaster.parentLoginId
                      FROM studentmaster
                     WHERE 1 = 1
                       AND studentmaster.studentLoginId = '".$_SESSION['s_activName']."' 
                           OR studentmaster.parentLoginId = '".$_SESSION['s_activName']."'
                       AND studentmaster.activated = 'Y'";
  $selectNominalRes = mysql_query($selectNominal);
  while($nominalRow = mysql_fetch_array($selectNominalRes))
  {
    $g = 0;
    $selectAtnP = "SELECT feecollection.grNo,feecollection.amount,feecollection.rNo,
                          DATE_FORMAT(feecollection.feeDate, '%d-%m-%Y') AS feeDate,
                          feecollection.modeOfPay,feecollection.chNo,feetype.feeType
                     FROM feecollection
                LEFT JOIN feetype ON feetype.feeTypeId = feecollection.feeTypeId
                    WHERE feecollection.grNo = '".$nominalRow['grNo']."'
                      AND feecollection.feeDate >= '".$attendenceStartDate."-04-01'
                      AND feecollection.feeDate <= '".$attendenceEndDate."-03-31'
                 ORDER BY feecollection.feeDate DESC";
    $selectAtnPRes = mysql_query($selectAtnP);
    while($markRow = mysql_fetch_array($selectAtnPRes))
    {
    	$markArr[$g]['amount']    = $markRow['amount'];
    	$markArr[$g]['feeDate']   = $markRow['feeDate'];
    	$markArr[$g]['rNo']       = $markRow['rNo'];
    	$markArr[$g]['modeOfPay'] = $markRow['modeOfPay'];
    	$markArr[$g]['chNo']      = $markRow['chNo'];
    	$markArr[$g]['feeType']   = $markRow['feeType'];
    	$g++;
    }
  	$i++;
  }
  
  $c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  $secArrOut[0] = 'A';
  $secArrOut[1] = 'B';
  $secArrOut[2] = 'C';
  $secArrOut[3] = 'D';
  
  $studStartYear = substr($academicStartYear,0,4);
  $studEndYear   = substr($academicEndYear,2,2);
  include("./bottom.php");
  $smarty->assign('attendenceArr',$attendenceArr);
  $smarty->assign('studStartYear',$studStartYear);
  $smarty->assign('studEndYear',$studEndYear);
  $smarty->assign('markArr',$markArr);
  $smarty->assign('cArray',$cArray);
  $smarty->assign('class',$class);
  $smarty->assign('section',$section);
  $smarty->assign('secArrOut',$secArrOut);
  $smarty->assign('totalNumberWorkingDay',$totalNumberWorkingDay);
  $smarty->assign('attendenceStartDate',$attendenceStartDate);
  $smarty->assign('attendenceEndDate',$attendenceEndDate);
  $smarty->display('myFee.tpl');
}
?>
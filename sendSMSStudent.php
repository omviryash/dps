<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student' && $_SESSION['s_userType'] == 'Teacher')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	if(isset($_REQUEST['startYear']))
  {
	  $academicStartYear = $_REQUEST['startYear'];
	  $academicEndYear   = $_REQUEST['startYear'] + 1;
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y');
	  	$nextYear            = date('Y') + 1;
	  	$academicEndYear   = $nextYear;
		}
		else
		{
			$prevYear            = date('Y') - 1;
			$academicStartYear = $prevYear;
	  	$academicEndYear   = date('Y');
		}
	}
	
	$class   = isset($_REQUEST['class']) ? $_REQUEST['class'] : 0;
	$section = isset($_REQUEST['classSection']) ? $_REQUEST['classSection'] : 0;
	if($class == 0 && $section == 0)
	{
		$classQuery = "AND 1 = 1";
	}
	else
	{
		$classQuery = "AND nominalroll.class = '".$class."'
                   AND nominalroll.section = '".$section."'";
	}
	
	$stdArray = array();
	$i = 0;
  $selectStd = "SELECT studentmaster.studentMasterId,studentmaster.studentName,studentmaster.fatherName,nominalroll.grNo,
                       nominalroll.class,nominalroll.section,studentmaster.activated,studentmaster.smsMobile
                  FROM nominalroll
                  LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
                 WHERE 1 = 1
                   ".$classQuery."
                   AND nominalroll.academicStartYear = '".$academicStartYear."-04-01'
                   AND nominalroll.academicEndYear = '".$academicEndYear."-03-31'
                   AND activated = 'Y' order by nominalroll.class,nominalroll.section";
  $selectStdRes = mysql_query($selectStd);
  while($stdRow = mysql_fetch_array($selectStdRes))
  {
  	$stdArray[$i]['studentMasterId']  = $stdRow['studentMasterId'];
  	$stdArray[$i]['class']            = $stdRow['class'];
  	$stdArray[$i]['section']          = $stdRow['section'];
  	$stdArray[$i]['studentName']      = $stdRow['studentName'];
  	$stdArray[$i]['fatherName']       = $stdRow['fatherName'];
  	$stdArray[$i]['grNo']             = $stdRow['grNo'];
  	$stdArray[$i]['activated']        = $stdRow['activated'];
  	$stdArray[$i]['smsMobile']        = $stdRow['smsMobile'];
  	$i++;
  }
  
  $tempArray = array();
	$t = 0;
  $selectTemp = "SELECT smsTemplateId,template
                   FROM smstemplate";
  $selectTempRes = mysql_query($selectTemp);
  while($tempRow = mysql_fetch_array($selectTempRes))
  {
  	$tempArray[$t]['smsTemplateId'] = $tempRow['smsTemplateId'];
  	$tempArray[$t]['template']      = $tempRow['template'];
  	$t++;
  }
  
	if(isset($_POST['techSub']))
  {
  	$currentDate = date('Y-m-d H:i:s');
    $loopCount = 0;
    while($loopCount < count($_POST['studentMasterId']))
    {
    	$studentMasterId  = ($_POST['studentMasterId'][$loopCount] != '') ? $_POST['studentMasterId'][$loopCount] : 0;
      if($_POST['studentMasterId'][$loopCount] != "")
      {
        $selectTech = "SELECT studentMasterId,smsMobile
                         FROM studentmaster
                        WHERE studentMasterId = ".$studentMasterId;
        $techRes = mysql_query($selectTech);
        
        while($techRow = mysql_fetch_array($techRes))
        {
        	if($techRow['smsMobile'] != '')
        	{
	          $insertSms = "INSERT INTO smsFacility(studentMasterId,smsMobile,message,date)
	                        VALUES(".$studentMasterId.",'".$techRow['smsMobile']."','".$_POST['msgText']."','".$currentDate."')";
	          $inserSmsRes = om_query($insertSms);
	          
	          $phNo91 = substr($techRow['smsMobile'],0,2);
	          
	          if($phNo91 == '91')
	          {
	            $PhNo = $techRow['smsMobile'];
	          }
	          else
	          {
	          	$PhNo = "91".$techRow['smsMobile'];
	          }
	          
	          $Text = urlencode($_POST['msgText']);
	          $url = "http://smsby.megasoftware.in/new/api/api_http.php?username=DPSRAJKOT&password=dpsoffice&senderid=DPSRJT&to=$PhNo&text=$Text&route=Transactional&type=text&datetime=$currentDate";
	          $ret = file_get_contents($url);
	        }
        }
      }
      $loopCount++;
    }
	}
	
	$c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  $secArrOut[0] = 'A';
  $secArrOut[1] = 'B';
  $secArrOut[2] = 'C';
  $secArrOut[3] = 'D';
  
  include("./bottom.php");
  $smarty->assign('stdArray',$stdArray);
  $smarty->assign('tempArray',$tempArray);
  $smarty->assign('cArray',$cArray);
  $smarty->assign('secArrOut',$secArrOut);
  $smarty->assign('academicStartYear',$academicStartYear);
  $smarty->display("sendSMSStudent.tpl");
}
?>
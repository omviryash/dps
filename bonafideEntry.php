<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$nominalRollId = isset($_REQUEST['nominalRollId']) ? $_REQUEST['nominalRollId'] : 0;
  if(isset($_POST['submit']))
  {
  	$bonafideDate = $_REQUEST['bonaDateYear']."-".$_REQUEST['bonaDateMonth']."-".$_REQUEST['bonaDateDay'];
  	$wefDate      = $_REQUEST['wefDateYear']."-".$_REQUEST['wefDateMonth']."-".$_REQUEST['wefDateDay'];
	  if(isset($_POST['nominalRollId']) && $_POST['nominalRollId'] > 0)
	  {
	  	$nominalEntry = "UPDATE nominalroll 
	    										SET bonafideDate  = '".$bonafideDate."',
	    											  wefDate       = '".$wefDate."'
	  								  	WHERE nominalRollId = ".$_POST['nominalRollId'];
	    $nominalEntryRes = om_query($nominalEntry);
	    if(!$nominalEntryRes)
	    {
	    	echo "Update Fail";
	    }
	    else
	    {
	      header("Location:bonafideEntry.php?nominalRollId=".$_POST['nominalRollId']."&done=1");
	    }
	  }
	}
	
  if($nominalRollId > 0)
	{
	  $selectStd = "SELECT nominalroll.grNo,academicStartYear,academicEndYear,class,section,rollNo,clubTerm1,
	                       clubTerm2,boardRegistrionId,boardRollNo,studentmaster.studentName,nominalroll.wefDate,nominalroll.bonafideDate
	                  FROM nominalroll
	             LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
	                 WHERE nominalroll.nominalRollId = ".$nominalRollId."";
	  $selectStdRes = mysql_query($selectStd);
	  while($stdRow = mysql_fetch_array($selectStdRes))
	  {
	  	$grNo                  = $stdRow['grNo'];
	  	$studentName           = $stdRow['studentName'];
	  	$academicStartYear     = substr($stdRow['academicStartYear'],0,4);
	  	$academicEndYear       = substr($stdRow['academicEndYear'],0,4);
	  	$class                 = $stdRow['class'];
	  	$section               = $stdRow['section'];
	  	$rollNo                = $stdRow['rollNo'];
	  	$boardRegistrionId     = $stdRow['boardRegistrionId'];
	  	$boardRollNo           = $stdRow['boardRollNo'];
	  	if($stdRow['bonafideDate'] != '')
	  	{
	  	  $bonafideDate = $stdRow['bonafideDate'];
	  	}
	  	else
	  	{
	  	  $bonafideDate = date('Y-m-d');
	  	}
	  	if($stdRow['wefDate'] != '')
	  	{
	  	  $wefDate = $stdRow['wefDate'];
	  	}
	  	else
	  	{
	  	  $wefDate = date('Y-m-d');
	  	}
	  }
	}
	
  $c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  $secArrOut[0] = 'A';
  $secArrOut[1] = 'B';
  $secArrOut[2] = 'C';
  $secArrOut[3] = 'D';
    
  include("./bottom.php");
  $smarty->assign("nominalRollId",$nominalRollId);
  $smarty->assign("academicStartYear",$academicStartYear);
  $smarty->assign("academicEndYear",$academicEndYear);
  $smarty->assign("class",$class);
  $smarty->assign("grNo",$grNo);
  $smarty->assign("section",$section);
  $smarty->assign("rollNo",$rollNo);
  $smarty->assign("cArray",$cArray);
  $smarty->assign("secArrOut",$secArrOut);
  $smarty->assign("studentName",$studentName);
  $smarty->assign("bonafideDate",$bonafideDate);
  $smarty->assign("wefDate",$wefDate);
  $smarty->display("bonafideEntry.tpl");
}
?>
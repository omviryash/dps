<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
  $academicStartYear = isset($_REQUEST['startDateYear']) ? $_REQUEST['startDateYear'] : "";
  $subjectName = isset($_REQUEST['subjectName']) ? $_REQUEST['subjectName'] : 0;	
  $termValue = isset($_REQUEST['termValue']) ? $_REQUEST['termValue'] : 0;	
  
  if(isset($_REQUEST['update']))
  {
    for($k=0; $k< count($_REQUEST['grNo']); $k++)
  	{
  		echo $_REQUEST['startDateYear'];
  	  $grNo            = isset($_POST['grNo'][$k]) && $_POST['grNo'][$k] != '' ? $_POST['grNo'][$k] : 0;
  	  $eReadingPro     = isset($_POST['eReadingPro'][$k]) && $_POST['eReadingPro'][$k] != '' ? $_POST['eReadingPro'][$k] : 0;
  	  $eReadingFlu     = isset($_POST['eReadingFlu'][$k]) && $_POST['eReadingFlu'][$k] != '' ? $_POST['eReadingFlu'][$k] : 0;
  	  $eReadingCom     = isset($_POST['eReadingCom'][$k]) && $_POST['eReadingCom'][$k] != '' ? $_POST['eReadingCom'][$k] : 0;
  	  $eWritingCre     = isset($_POST['eWritingCre'][$k]) && $_POST['eWritingCre'][$k] != '' ? $_POST['eWritingCre'][$k] : 0;
  	  $eWritingHan     = isset($_POST['eWritingHan'][$k]) && $_POST['eWritingHan'][$k] != '' ? $_POST['eWritingHan'][$k] : 0;
  	  $eWritingGra     = isset($_POST['eWritingGra'][$k]) && $_POST['eWritingGra'][$k] != '' ? $_POST['eWritingGra'][$k] : 0;
  	  $eWritingSpe     = isset($_POST['eWritingSpe'][$k]) && $_POST['eWritingSpe'][$k] != '' ? $_POST['eWritingSpe'][$k] : 0;
  	  $eWritingVoc     = isset($_POST['eWritingVoc'][$k]) && $_POST['eWritingVoc'][$k] != '' ? $_POST['eWritingVoc'][$k] : 0;
  	  $ewSpeakinCon    = isset($_POST['ewSpeakinCon'][$k]) && $_POST['ewSpeakinCon'][$k] != '' ? $_POST['ewSpeakinCon'][$k] : 0;
  	  $ewSpeakinRec    = isset($_POST['ewSpeakinRec'][$k]) && $_POST['ewSpeakinRec'][$k] != '' ? $_POST['ewSpeakinRec'][$k] : 0;
  	  $ewSpeakinCla    = isset($_POST['ewSpeakinCla'][$k]) && $_POST['ewSpeakinCla'][$k] != '' ? $_POST['ewSpeakinCla'][$k] : 0;
  	  $eListingComp    = isset($_POST['eListingComp'][$k]) && $_POST['eListingComp'][$k] != '' ? $_POST['eListingComp'][$k] : 0;
  	  $eListingCon     = isset($_POST['eListingCon'][$k]) && $_POST['eListingCon'][$k] != '' ? $_POST['eListingCon'][$k] : 0;
  	  $extraReading    = isset($_POST['extraReading'][$k]) && $_POST['extraReading'][$k] != '' ? $_POST['extraReading'][$k] : 0;
  	  $activityPro     = isset($_POST['activityPro'][$k]) && $_POST['activityPro'][$k] != '' ? $_POST['activityPro'][$k] : 0;
  	  $hiReadingPro    = isset($_POST['hiReadingPro'][$k]) && $_POST['hiReadingPro'][$k] != '' ? $_POST['hiReadingPro'][$k] : 0;
  	  $hiReadingFlu    = isset($_POST['hiReadingFlu'][$k]) && $_POST['hiReadingFlu'][$k] != '' ? $_POST['hiReadingFlu'][$k] : 0;
  	  $hiReadingCom    = isset($_POST['hiReadingCom'][$k]) && $_POST['hiReadingCom'][$k] != '' ? $_POST['hiReadingCom'][$k] : 0;
  	  $hiWritingCre    = isset($_POST['hiWritingCre'][$k]) && $_POST['hiWritingCre'][$k] != '' ? $_POST['hiWritingCre'][$k] : 0;
  	  $hiWritingHan    = isset($_POST['hiWritingHan'][$k]) && $_POST['hiWritingHan'][$k] != '' ? $_POST['hiWritingHan'][$k] : 0;
  	  $hiWritingGra    = isset($_POST['hiWritingGra'][$k]) && $_POST['hiWritingGra'][$k] != '' ? $_POST['hiWritingGra'][$k] : 0;
  	  $hiWritingSpe    = isset($_POST['hiWritingSpe'][$k]) && $_POST['hiWritingSpe'][$k] != '' ? $_POST['hiWritingSpe'][$k] : 0;
  	  $hiWritingVoc    = isset($_POST['hiWritingVoc'][$k]) && $_POST['hiWritingVoc'][$k] != '' ? $_POST['hiWritingVoc'][$k] : 0;
  	  $hiwSpeakinCon   = isset($_POST['hiwSpeakinCon'][$k]) && $_POST['hiwSpeakinCon'][$k] != '' ? $_POST['hiwSpeakinCon'][$k] : 0;
  	  $hiwSpeakinRec   = isset($_POST['hiwSpeakinRec'][$k]) && $_POST['hiwSpeakinRec'][$k] != '' ? $_POST['hiwSpeakinRec'][$k] : 0;
  	  $hiwSpeakinCla   = isset($_POST['hiwSpeakinCla'][$k]) && $_POST['hiwSpeakinCla'][$k] != '' ? $_POST['hiwSpeakinCla'][$k] : 0;
  	  $hiListingComp   = isset($_POST['hiListingComp'][$k]) && $_POST['hiListingComp'][$k] != '' ? $_POST['hiListingComp'][$k] : 0;
  	  $hiListingCon    = isset($_POST['hiListingCon'][$k]) && $_POST['hiListingCon'][$k] != '' ? $_POST['hiListingCon'][$k] : 0;
  	  $hiextraReading  = isset($_POST['hiextraReading'][$k]) && $_POST['hiextraReading'][$k] != '' ? $_POST['hiextraReading'][$k] : 0;
  	  $hiactivityPro   = isset($_POST['hiactivityPro'][$k]) && $_POST['hiactivityPro'][$k] != '' ? $_POST['hiactivityPro'][$k] : 0;
  	  $comSkills       = isset($_POST['comSkills'][$k]) && $_POST['comSkills'][$k] != '' ? $_POST['comSkills'][$k] : 0;
  	  $comAptitude     = isset($_POST['comAptitude'][$k]) && $_POST['comAptitude'][$k] != '' ? $_POST['comAptitude'][$k] : 0;
  	  $mathAspCon      = isset($_POST['mathAspCon'][$k]) && $_POST['mathAspCon'][$k] != '' ? $_POST['mathAspCon'][$k] : 0;
  	  $mathAspMen      = isset($_POST['mathAspMen'][$k]) && $_POST['mathAspMen'][$k] != '' ? $_POST['mathAspMen'][$k] : 0;
  	  $mathAspActi     = isset($_POST['mathAspActi'][$k]) && $_POST['mathAspActi'][$k] != '' ? $_POST['mathAspActi'][$k] : 0;
  	  $envAspenv       = isset($_POST['envAspenv'][$k]) && $_POST['envAspenv'][$k] != '' ? $_POST['envAspenv'][$k] : 0;
  	  $envGroupDis     = isset($_POST['envGroupDis'][$k]) && $_POST['envGroupDis'][$k] != '' ? $_POST['envGroupDis'][$k] : 0;
  	  $envActi         = isset($_POST['envActi'][$k]) && $_POST['envActi'][$k] != '' ? $_POST['envActi'][$k] : 0;
  	  $perCou          = isset($_POST['perCou'][$k]) && $_POST['perCou'][$k] != '' ? $_POST['perCou'][$k] : 0;
  	  $perCon          = isset($_POST['perCon'][$k]) && $_POST['perCon'][$k] != '' ? $_POST['perCon'][$k] : 0;
  	  $perCar          = isset($_POST['perCar'][$k]) && $_POST['perCar'][$k] != '' ? $_POST['perCar'][$k] : 0;
  	  $perNea          = isset($_POST['perNea'][$k]) && $_POST['perNea'][$k] != '' ? $_POST['perNea'][$k] : 0;
  	  $perPun          = isset($_POST['perPun'][$k]) && $_POST['perPun'][$k] != '' ? $_POST['perPun'][$k] : 0;
  	  $perRegAndPun    = isset($_POST['perRegAndPun'][$k]) && $_POST['perRegAndPun'][$k] != '' ? $_POST['perRegAndPun'][$k] : 0;
  	  $perIni          = isset($_POST['perIni'][$k]) && $_POST['perIni'][$k] != '' ? $_POST['perIni'][$k] : 0;
  	  $perSha          = isset($_POST['perSha'][$k]) && $_POST['perSha'][$k] != '' ? $_POST['perSha'][$k] : 0;
  	  $perRes          = isset($_POST['perRes'][$k]) && $_POST['perRes'][$k] != '' ? $_POST['perRes'][$k] : 0;
  	  $persel          = isset($_POST['persel'][$k]) && $_POST['persel'][$k] != '' ? $_POST['persel'][$k] : 0;
  	  $coPhyEth        = isset($_POST['coPhyEth'][$k]) && $_POST['coPhyEth'][$k] != '' ? $_POST['coPhyEth'][$k] : 0;
  	  $coPhyDis        = isset($_POST['coPhyDis'][$k]) && $_POST['coPhyDis'][$k] != '' ? $_POST['coPhyDis'][$k] : 0;
  	  $coPhyTea        = isset($_POST['coPhyTea'][$k]) && $_POST['coPhyTea'][$k] != '' ? $_POST['coPhyTea'][$k] : 0;
  	  $coPhyTal        = isset($_POST['coPhyTal'][$k]) && $_POST['coPhyTal'][$k] != '' ? $_POST['coPhyTal'][$k] : 0;
  	  $coActInt        = isset($_POST['coActInt'][$k]) && $_POST['coActInt'][$k] != '' ? $_POST['coActInt'][$k] : 0;
  	  $coActCre        = isset($_POST['coActCre'][$k]) && $_POST['coActCre'][$k] != '' ? $_POST['coActCre'][$k] : 0;
  	  $coActSki        = isset($_POST['coActSki'][$k]) && $_POST['coActSki'][$k] != '' ? $_POST['coActSki'][$k] : 0;
  	  $coMusicint      = isset($_POST['coMusicint'][$k]) && $_POST['coMusicint'][$k] != '' ? $_POST['coMusicint'][$k] : 0;
  	  $coMusicRhy      = isset($_POST['coMusicRhy'][$k]) && $_POST['coMusicRhy'][$k] != '' ? $_POST['coMusicRhy'][$k] : 0;
  	  $coMusicMel      = isset($_POST['coMusicMel'][$k]) && $_POST['coMusicMel'][$k] != '' ? $_POST['coMusicMel'][$k] : 0;
  	  $coArtInt        = isset($_POST['coArtInt'][$k]) && $_POST['coArtInt'][$k] != '' ? $_POST['coArtInt'][$k] : 0;
  	  $coArtCre        = isset($_POST['coArtCre'][$k]) && $_POST['coArtCre'][$k] != '' ? $_POST['coArtCre'][$k] : 0;
  	  $coArtPre        = isset($_POST['coArtPre'][$k]) && $_POST['coArtPre'][$k] != '' ? $_POST['coArtPre'][$k] : 0;
  	  $coDanceInt      = isset($_POST['coDanceInt'][$k]) && $_POST['coDanceInt'][$k] != '' ? $_POST['coDanceInt'][$k] : 0;
  	  $coDanceAct      = isset($_POST['coDanceAct'][$k]) && $_POST['coDanceAct'][$k] != '' ? $_POST['coDanceAct'][$k] : 0;
  	  $coDanceExp      = isset($_POST['coDanceExp'][$k]) && $_POST['coDanceExp'][$k] != '' ? $_POST['coDanceExp'][$k] : 0;
  	  $studHei         = isset($_POST['studHei'][$k]) && $_POST['studHei'][$k] != '' ? $_POST['studHei'][$k] : 0;
  	  $studWei         = isset($_POST['studWei'][$k]) && $_POST['studWei'][$k] != '' ? $_POST['studWei'][$k] : 0;
  	  $partici 	       = isset($_POST['partici'][$k]) && $_POST['partici'][$k] != '' ? $_POST['partici'][$k] : "";
      $achiev 	       = isset($_POST['achiev'][$k]) && $_POST['achiev'][$k] != '' ? $_POST['achiev'][$k] : "";
      $remarks 	       = isset($_POST['remarks'][$k]) && $_POST['remarks'][$k] != '' ? $_POST['remarks'][$k] : "";
      $attance 	       = isset($_POST['attance'][$k]) && $_POST['attance'][$k] != '' ? $_POST['attance'][$k] : 0;
      $partici1 	     = isset($_POST['partici1'][$k]) && $_POST['partici1'][$k] != '' ? $_POST['partici1'][$k] : "";
      $achiev1 	       = isset($_POST['achiev1'][$k]) && $_POST['achiev1'][$k] != '' ? $_POST['achiev1'][$k] : "";
      $remarks1 	     = isset($_POST['remarks1'][$k]) && $_POST['remarks1'][$k] != '' ? $_POST['remarks1'][$k] : "";
      $attance1 	     = isset($_POST['attance1'][$k]) && $_POST['attance1'][$k] != '' ? $_POST['attance1'][$k] : 0;
      $classMasterId   = isset($_POST['classMasterId'][$k]) && $_POST['classMasterId'][$k] != '' ? $_POST['classMasterId'][$k] : 0;
      $promoted        = $_POST['promotedYear']."-".$_POST['promotedMonth']."-".$_POST['promotedDay'];

  	  if($subjectName == 2)
  	  {
  	    $updateMaster = "UPDATE gradeterm1
  	                              SET eReadingPro  = ".$eReadingPro.",
  	                                  eReadingFlu  = ".$eReadingFlu.",
  	                                  eReadingCom  = ".$eReadingCom.",
  	                                  eWritingCre  = ".$eWritingCre.",
  	                                  eWritingHan  = ".$eWritingHan.",
  	                                  eWritingGra  = ".$eWritingGra.",
  	                                  eWritingSpe  = ".$eWritingSpe.",
  	                                  eWritingVoc  = ".$eWritingVoc.",
  	                                  ewSpeakinCon = ".$ewSpeakinCon.",
  	                                  ewSpeakinRec = ".$ewSpeakinRec.",
  	                                  ewSpeakinCla = ".$ewSpeakinCla.",
  	                                  eListingComp = ".$eListingComp.",
  	                                  eListingCon  = ".$eListingCon.",
  	                                  extraReading = ".$extraReading.",
  	                                  activityPro  = ".$activityPro."
  	                        WHERE grNo  = ".$grNo."
  	                          AND termValue  = ".$termValue."
							  AND  academicStartYear  LIKE '".$academicStartYear."%'";
        $updateMasterRes = mysql_query($updateMaster);
        if(!$updateMasterRes)
        {
          echo "Teram 1 Update Fail";
        }
        else
        {
          header("Location:gradeEntry.php");
        }
      }
      if($subjectName == 6)
      {
        $updateComputer = "UPDATE gradeterm1
      	                     SET
      	                      comSkills    = ".$comSkills.",
  	                         comAptitude  = ".$comAptitude."
  	                        WHERE grNo  = ".$grNo."
  	                          AND termValue  = ".$termValue;
        $updateComputerRes = mysql_query($updateComputer);
        if(!$updateComputerRes)
        {
          echo "Computer Update Fail";
        }
        else
        {
          header("Location:gradeEntry.php");
        }
      }
      if($subjectName == 4)
      {
        $updatePerDev = "UPDATE gradeterm1
      	                     SET  perCou       = ".$perCou.",
  	                             perCon       = ".$perCon.",
  	                             perCar       = ".$perCar.",
  	                             perNea       = ".$perNea.",
  	                             perPun       = ".$perPun.",
  	                             perRegAndPun = ".$perRegAndPun.",
  	                             perIni       = ".$perIni.",
  	                             perSha       = ".$perSha.",
  	                             perRes       = ".$perRes.",
  	                             persel       = ".$persel."
  	                      WHERE grNo  = ".$grNo."
  	                          AND termValue  = ".$termValue."
							  AND  academicStartYear  LIKE '".$academicStartYear."%'";
        $updatePerDevRes = mysql_query($updatePerDev);
        if(!$updatePerDevRes)
        {
          echo "Persnality Devlopment Update Fail";
        }
        else
        {
          header("Location:gradeEntry.php");
        }
      }
      if($subjectName == 5)
      {
        $updateCoAct = "UPDATE gradeterm1
      	                     SET
      	                      coPhyEth 	  = ".$coPhyEth.",
                             coPhyDis     = ".$coPhyDis.",
                             coPhyTea     = ".$coPhyTea.",
                             coPhyTal     = ".$coPhyTal.",
                             coActInt     = ".$coActInt.",
                             coActCre     = ".$coActCre.",
                             coActSki     = ".$coActSki.",
                             coMusicint   = ".$coMusicint.",
                             coMusicRhy   = ".$coMusicRhy.",
                             coMusicMel   = ".$coMusicMel.",
                             coArtInt 	  = ".$coArtInt.",
                             coArtCre 	  = ".$coArtCre.",
                             coArtPre 	  = ".$coArtPre.",
                             coDanceInt   = ".$coDanceInt.",
                             coDanceAct   = ".$coDanceAct.",
                             coDanceExp   = ".$coDanceExp."
  	                        WHERE grNo  = ".$grNo."
  	                          AND termValue  = ".$termValue."
							  AND  academicStartYear  LIKE '".$academicStartYear."%'";
        $updateCoActRes = mysql_query($updateCoAct);
        if(!$updateCoActRes)
        {
          echo "Co-Activity Update Fail";
        }
        else
        {
          header("Location:gradeEntry.php");
        }
      }
      if($subjectName == 8)
      {
        $updateMath   = "UPDATE gradeterm1
      	                     SET
      	                      mathAspCon   = ".$mathAspCon.",
  	                         mathAspMen   = ".$mathAspMen.",
  	                         mathAspActi  = ".$mathAspActi."
  	                        WHERE grNo  = ".$grNo."
  	                          AND termValue  = ".$termValue."
							  AND  academicStartYear  LIKE '".$academicStartYear."%'";
        $updateMathRes = mysql_query($updateMath);
        if(!$updateMathRes)
        {
          echo "Maths Update Fail";
        }
        else
        {
          header("Location:gradeEntry.php");
        }
      }
      if($subjectName == 3)
      {
        $updateEnv = "UPDATE gradeterm1
      	                     SET
      	                       envAspenv    = ".$envAspenv.",
  	                          envGroupDis  = ".$envGroupDis.",
  	                          envActi      = ".$envActi."
  	                        WHERE grNo  = ".$grNo."
  	                          AND termValue  = ".$termValue."
							  AND  academicStartYear  LIKE '".$academicStartYear."%'";
        $updateEnvRes = mysql_query($updateEnv);
        if(!$updateEnvRes)
        {
          echo "Envirment Science Update Fail";
        }
        else
        {
          header("Location:gradeEntry.php");
        }
      }
      if($subjectName == 7)
      {
        $updateHealth = "UPDATE gradeterm1
                             SET
                               studHei = '".addslashes($studHei)."',
                              studWei = ".$studWei."
  	                        WHERE grNo  = ".$grNo."
  	                          AND termValue  = ".$termValue."
							  AND  academicStartYear  LIKE '".$academicStartYear."%'";
        $updateHealthRes = mysql_query($updateHealth);
        if(!$updateHealthRes)
        {
          echo "Health Update Fail";
        }
        else
        {
          header("Location:gradeEntry.php");
        }
      }
      if($subjectName == 1)
      {
         $updateHindi = "UPDATE gradeterm1
  	                              SET hiReadingPro  = ".$hiReadingPro.",
  	                                  hiReadingFlu  = ".$hiReadingFlu.",
  	                                  hiReadingCom  = ".$hiReadingCom.",
  	                                  hiWritingCre  = ".$hiWritingCre.",
  	                                  hiWritingHan  = ".$hiWritingHan.",
  	                                  hiWritingGra  = ".$hiWritingGra.",
  	                                  hiWritingSpe  = ".$hiWritingSpe.",
  	                                  hiWritingVoc  = ".$hiWritingVoc.",
  	                                  hiwSpeakinCon = ".$hiwSpeakinCon.",
  	                                  hiwSpeakinRec = ".$hiwSpeakinRec.",
  	                                  hiwSpeakinCla = ".$hiwSpeakinCla.",
  	                                  hiListingComp = ".$hiListingComp.",
  	                                  hiListingCon  = ".$hiListingCon.",
  	                                  hiextraReading = ".$hiextraReading.",
  	                                  hiactivityPro  = ".$hiactivityPro."
  	                           WHERE grNo  = ".$grNo."
  	                            AND termValue  = ".$termValue."
								AND  academicStartYear  LIKE '".$academicStartYear."%'";
        $updateHindiRes = mysql_query($updateHindi);
        if(!$updateHindiRes)
        {
          echo "Hindi Update Fail";
        }
        else
        {
          header("Location:gradeEntry.php");
        }
      }
      if($subjectName == 9  AND $termValue == 1)
      {
        $updateOther = "UPDATE gradeterm1
                             SET partici 	     = '".$partici."',      
  	                             achiev 	     = '".$achiev."', 	      
  	                             remarks 	     = '".$remarks."', 	    
  	                             attance 	     = '".$attance."'
     	                       WHERE grNo      = ".$grNo."
  	                          AND termValue    = ".$termValue."
							  AND  academicStartYear  LIKE '".$academicStartYear."%'";
        $updateOtherRes = mysql_query($updateOther);
        if(!$updateOtherRes)
        {
          echo "First Term Update Fail";
        }
        else
        {
          header("Location:gradeEntry.php");
        }
      }
      if($subjectName == 9  AND $termValue == 2)
      {
        $updateOther2 = "UPDATE gradeterm1
                            SET partici1 	   = '".$partici1."', 	    
                             achiev1 	     = '".$achiev1."', 	      
                             remarks1 	   = '".$remarks1."', 	    
                             attance1 	   = '".$attance1."', 	    
                             classMasterId = ".$classMasterId.",    
                             promoted      = '".$promoted."'        
     	                      WHERE grNo      = ".$grNo."
  	                          AND termValue    = ".$termValue."
							  AND  academicStartYear  LIKE '".$academicStartYear."%'";
        $updateOther2Res = mysql_query($updateOther2);
        if(!$updateOther2Res)
        {
          echo "Secound Term  Update Fail";
        }
        else
        {
          header("Location:gradeEntry.php");
        }
      }
    }
  }

  $nameArray     = array();
  $gradeArray    = array();
  $newGradeArray = array();
  $i             = 0;
  $k             = 0;
  $l             = 0;
  $selectId = "SELECT employeeMasterId
                 FROM employeemaster
                WHERE loginId = '".$_SESSION['s_activName']."'";
  $selectIdRes = mysql_query($selectId);
  if($idRow = mysql_fetch_array($selectIdRes))
  {
    $selectClass = "SELECT class,section
                      FROM classteacherallotment
                     WHERE employeeMasterId = ".$idRow['employeeMasterId']."
					 AND  academicStartYear  LIKE '".$academicStartYear."%'";
    $selectClassRes = mysql_query($selectClass);
    if($classRow = mysql_fetch_array($selectClassRes))
    {
    	$class   = $classRow['class'];
    	$section = $classRow['section'];
    }
    
    $selectStud = "SELECT studentmaster.studentName,studentmaster.studentId,nominalroll.grNo
  	                FROM nominalroll
  	                JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
  	                   WHERE class = '".$classRow['class']."'
  	                     AND section = '".$classRow['section']."'
                         AND activated = 'Y'
                         AND nominalroll.academicStartYear LIKE '".$academicStartYear."%'
                       ORDER BY studentName";
    $selectStudRes = mysql_query($selectStud);
    $count = mysql_num_rows($selectStudRes);
    while($studRow = mysql_fetch_array($selectStudRes))
    {
    	$nameArray[$i]['studentName']  = $studRow['studentName'];
    	$nameArray[$i]['studentId']    = $studRow['studentId'];
    	$nameArray[$i]['grNo']         = $studRow['grNo'];
    	$i++;
    }
  }
  
  $selectGrade = "SELECT gradeId,grade 
      	                 FROM grade
      	                WHERE priority <=9";
  $selectGradeRes = mysql_query($selectGrade);
  while($gradeRow = mysql_fetch_array($selectGradeRes))
  {
    $gradeArray['gradeId'][$k]  = $gradeRow['gradeId'];
    $gradeArray['grade'][$k]    = $gradeRow['grade'];
    $k++;
  }	
	  
  $selectGrade = "SELECT gradeId,grade 
      	            FROM grade
      	           WHERE priority > 9";
  $selectGradeRes = mysql_query($selectGrade);
  while($newGradeRow = mysql_fetch_array($selectGradeRes))
  {
    $newGradeArray['gradeId'][$l]  = $newGradeRow['gradeId'];
    $newGradeArray['grade'][$l]    = $newGradeRow['grade'];
    $l++;
  }
  
  $gradeSelectionArray = array();
  $selectData = "SELECT * 
                   FROM gradeterm1
                  WHERE termValue = ".$termValue."
				  AND academicStartYear LIKE '".$academicStartYear."%'";
  $selectDataRes = mysql_query($selectData);
  while($dataRow = mysql_fetch_array($selectDataRes))
  {
    $gradeSelectionArray[$dataRow['grNo']]  = $dataRow;
    $promoted = $dataRow['promoted'];
    //echo"<br>". $gradeSelectionArray[$dataRow['promoted']];
  }
  
  $w=0;
  $classArray = array();
  $selectClass = "SELECT classMasterId,className 
                    FROM  classmaster
                  GROUP BY className
                  ORDER BY classMasterId";
  $selectClassRes = mysql_query($selectClass);
  while($classRow = mysql_fetch_array($selectClassRes))
  {
    $classArray['classMasterId'][$w] =  $classRow['classMasterId'];
    $classArray['className'][$w]     =  $classRow['className'];
    $w++;
  }
  
	include("./bottom.php");
	$smarty->assign('promoted',$promoted);
	$smarty->assign('newGradeArray',$newGradeArray);
	$smarty->assign('gradeSelectionArray',$gradeSelectionArray);
	$smarty->assign('gradeArray',$gradeArray);
	$smarty->assign('classArray',$classArray);
	$smarty->assign('nameArray',$nameArray);
  $smarty->assign('termValue',$termValue);
  $smarty->assign('subjectName',$subjectName);
  if ($subjectName == 6)
  {
    $smarty->display('getCom.tpl');
  }
  elseif($subjectName == 8)
  {
	  $smarty->display('getMath.tpl');
  }
  elseif($subjectName == 3)
  {
	  $smarty->display('envSci.tpl');
  }
  elseif($subjectName == 4)
  {
	  $smarty->display('perDev.tpl');
  }
  elseif($subjectName == 5)
  {
	  $smarty->display('coAct.tpl');
  }
  elseif($subjectName == 7)
  {
	  $smarty->display('health.tpl');
  }
  elseif($subjectName == 1)
  {
	  $smarty->display('getHindi.tpl');
  }
  elseif($subjectName == 2)
  {
	  $smarty->display('getEng.tpl');
  }
   elseif($subjectName == 9)
  {
	  $smarty->display('other.tpl');
  }
}
?>
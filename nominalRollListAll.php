<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$activated = isset($_REQUEST['activated']) ? $_REQUEST['activated'] : 'Y';
	
	if($activated == 'Both')
	{
		$activatedQuery = "AND 1 = 1";
	}
	else
	{
		$activatedQuery = "AND activated = '".$activated."'";
	}
	
	if(isset($_REQUEST['startYear']))
  {
  	$academicStartYearSelected = $_REQUEST['startYear'];
	  $academicStartYear = $_REQUEST['startYear'];
	  $academicEndYear = $_REQUEST['startYear'] + 1;
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
			$academicStartYearSelected = date('Y');
	  	$academicStartYear = date('Y');
	  	$nextYear          = date('Y') + 1;
	  	$academicEndYear   = $nextYear;
		}
		else
		{
			$prevYear          = date('Y') - 1;
			$academicStartYear = $prevYear;
	  	$academicEndYear   = date('Y');
	  	$academicStartYearSelected = $prevYear;
		}
	}
	
	$nominalArr = array();
  $i = 0;
  $selectNominal = "SELECT DISTINCT nominalRollId,nominalroll.grNo,academicStartYear,academicEndYear,class,
                           section,rollNo,feeGroup,libraryMemberType,libraryMemberType,studentmaster.studentName,
                           busRoute,busStop,subjectGroup,coScholasticGroup,clubTerm1,clubTerm2,
                           boardRegistrionId,boardRollNo,activated
                      FROM nominalroll
                 LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
                 LEFT JOIN classmaster ON classmaster.className = nominalroll.class
                     WHERE academicStartYear = '".$academicStartYear."-04-01'
                       AND academicEndYear = '".$academicEndYear."-03-31'
                       ".$activatedQuery."
                  ORDER BY classmaster.priority,nominalroll.section,nominalroll.rollNo";
  $selectNominalRes = mysql_query($selectNominal);
  while($nominalRow = mysql_fetch_array($selectNominalRes))
  {
    $nominalArr[$i]['nominalRollId']      = $nominalRow['nominalRollId'];
    $nominalArr[$i]['grNo']               = $nominalRow['grNo'];
    $nominalArr[$i]['academicYear']       = substr($nominalRow['academicStartYear'],0,4).'-'.substr($nominalRow['academicEndYear'],0,4);
    $nominalArr[$i]['class']              = $nominalRow['class'];
    $nominalArr[$i]['section']            = $nominalRow['section'];
    $nominalArr[$i]['rollNo']             = $nominalRow['rollNo'];
    $nominalArr[$i]['feeGroup']           = $nominalRow['feeGroup'];
    $nominalArr[$i]['libraryMemberType']  = $nominalRow['libraryMemberType'];
    $nominalArr[$i]['libraryMemberType']  = $nominalRow['libraryMemberType'];
    $nominalArr[$i]['studentName']        = $nominalRow['studentName'];
    $nominalArr[$i]['busRoute']           = $nominalRow['busRoute'];
    $nominalArr[$i]['busStop']            = $nominalRow['busStop'];
    $nominalArr[$i]['subjectGroup']       = $nominalRow['subjectGroup'];
    $nominalArr[$i]['coScholasticGroup']  = $nominalRow['coScholasticGroup'];
    $nominalArr[$i]['clubTerm1']          = $nominalRow['clubTerm1'];
    $nominalArr[$i]['clubTerm2']          = $nominalRow['clubTerm2'];
    $nominalArr[$i]['boardRegistrionId']  = $nominalRow['boardRegistrionId'];
    $nominalArr[$i]['boardRollNo']        = $nominalRow['boardRollNo'];
    $nominalArr[$i]['activated']          = $nominalRow['activated'];
    $i++;
  }
	
	$activatedVal[0] = 'Y';
	$activatedVal[1] = 'N';
	$activatedVal[2] = 'Both';
	$activatedOut[0] = 'Yes';
	$activatedOut[1] = 'No';
	$activatedOut[2] = 'Both';
	
  include("./bottom.php");
  $smarty->assign('nominalArr',$nominalArr);
  $smarty->assign('activatedVal',$activatedVal);
  $smarty->assign('activatedOut',$activatedOut);
  $smarty->assign('activated',$activated);
  $smarty->assign('academicStartYear',$academicStartYear);
  $smarty->assign('academicStartYearSelected',$academicStartYearSelected);
  $smarty->display('nominalRollListAll.tpl');  
}
?>
<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$academicStartYear = '0000';
	$academicEndYear   = '0000';
	$classArray        = array();
	
  if(isset($_REQUEST['Submit']))
  {
  	$nextYear          = $_REQUEST['startYear'] + 1;
  	$academicStartYear = $_REQUEST['startYear']."-04-01";
  	$academicEndYear   = $nextYear."-03-31";
  	$subjectMasterId   = isset($_REQUEST['subjectMasterId']) ? $_REQUEST['subjectMasterId'] : "";
  	$grNo              = isset($_REQUEST['grNo']) ? $_REQUEST['grNo'] : 0;
  	
  	if($_REQUEST['termOutput'] == 'Option 1')
  	{
      $updateClub = "UPDATE nominalroll 
                        SET optionSubject1 = '".$subjectMasterId."'
                      WHERE grNo = ".$grNo."
                        AND academicStartYear = '".$academicStartYear."'
                        AND academicEndYear   = '".$academicEndYear."'";
      $updateClubRes = om_query($updateClub);
      if(!$updateClubRes)
      {
      	echo "Option 1 Update Fail";
      }
      else
	    {
	    	header("Location:optionSubAllotment.php?done=1");
	    }
    }
	  else if($_REQUEST['termOutput'] == 'Option 2')
	  {
	    $updateClub2 = "UPDATE nominalroll 
                         SET optionSubject2 = '".$subjectMasterId."'
                       WHERE grNo = ".$grNo;
      $updateClub2Res = om_query($updateClub2);
      if(!$updateClub2Res)
      {
      	echo "Option 2 Update Fail";
      }
      else
	    {
	    	header("Location:optionSubAllotment.php?done=1");
	    }
	  }
  }
  
  $p=0;
	$studArray = array();
	$selectStudent = "SELECT grNo,studentName
                      FROM studentmaster
                     WHERE activated = 'Y'";
	$selectStudentRes = mysql_query($selectStudent);
	while($empRow = mysql_fetch_array($selectStudentRes))
	{
	  $studArray['grNo'][$p]          = $empRow['grNo'];
	  $studArray['studentName'][$p]   = $empRow['studentName'];
	  $p++;
	}
	
	$k = 0;
  $selectClub = "SELECT subjectMasterId,subjectName
                   FROM subjectmaster
                  WHERE subjectType = 'Main'";
  $selectClubRes = mysql_query($selectClub);
  while($clubRow = mysql_fetch_array($selectClubRes))
  {
  	$clubArr['subjectMasterId'][$k]   = $clubRow['subjectMasterId'];
  	$clubArr['subjectName'][$k]       = $clubRow['subjectName'];
  	$k++;
  }
	
	
	$termValue[0] = "Option 1";
	$termOutput[0] = "Option 1";
  $termValue[1] = "Option 2";
	$termOutput[1] = "Option 2";
		
  include("./bottom.php");
  $smarty->assign('academicStartYear',$academicStartYear);
  $smarty->assign('academicEndYear',$academicEndYear);
  $smarty->assign('termValue',$termValue);
  $smarty->assign('termOutput',$termOutput);
  $smarty->assign('clubArr',$clubArr);
  $smarty->assign('classArray',$classArray);
  $smarty->assign('studArray',$studArray);
  $smarty->display('optionSubAllotment.tpl');  
}
?>
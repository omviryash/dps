<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$countStudent = 0;
	if(isset($_REQUEST['startYear']))
  {
	  $academicStartYear = $_REQUEST['startYear'];
	  $academicEndYear   = $_REQUEST['startYear'] + 1;
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y');
	  	$nextYear            = date('Y') + 1;
	  	$academicEndYear   = $nextYear;
		}
		else
		{
			$prevYear            = date('Y') - 1;
			$academicStartYear = $prevYear;
	  	$academicEndYear   = date('Y');
		}
	}
	
	$stdArray = array();
	$notFound = 'Record Not Found';
	$i = 0;
	
	$class   = isset($_REQUEST['class']) ? $_REQUEST['class'] : 0;
	$section = isset($_REQUEST['classSection']) ? $_REQUEST['classSection'] : 0;
	$houseId = isset($_REQUEST['houseId']) && $_REQUEST['houseId'] != '' ? $_REQUEST['houseId'] : 0;
	
	if($class != 0 || $class != '')
	{
	  $classQuery = "AND nominalroll.class = '".$class."'
									 AND nominalroll.section = '".$section."'
									 AND nominalroll.academicStartYear = '".$academicStartYear."-04-01'
									 AND nominalroll.academicEndYear = '".$academicEndYear."-03-31'";
	}
	else
	{
		$classQuery = "AND 1 = 1";
	}
	
	if($houseId > 0)
	{
	  $houseQuery = "AND studentmaster.houseId = ".$_REQUEST['houseId']."";
	}
	else
	{
		$houseQuery = "AND 1 = 1";
	}
	
	if(isset($_REQUEST['houseId']))
	{
	  $selectStd = "SELECT DISTINCT studentMasterId,studentName,fatherName,mothersName,studentmaster.grNo,activated,dateOfBirth,joinedInClass,joiningDate,gender,bloodGroup,house.houseName,
	                       currentAddress,currentState,currentAddressPin,residencePhone1,residencePhone2,studentEmail,
	                       permanentAddress,permanentState,permanentPIN,religion,previousSchool
	                  FROM studentmaster
	             LEFT JOIN house ON house.houseId = studentmaster.houseId
	             LEFT JOIN nominalroll ON nominalroll.grNo = studentmaster.grNo
	                 WHERE 1 = 1
	                   ".$houseQuery."
	                   ".$classQuery."
	                   AND studentmaster.activated = 'Y'";
	  $selectStdRes = mysql_query($selectStd);
	  $countStudent = mysql_num_rows($selectStdRes);
	  while($stdRow = mysql_fetch_array($selectStdRes))
	  {
	  	$stdArray[$i]['studentMasterId']   = $stdRow['studentMasterId'];
	  	$stdArray[$i]['studentName']       = $stdRow['studentName'];
	  	$stdArray[$i]['fatherName']        = $stdRow['fatherName'];
	  	$stdArray[$i]['mothersName']        = $stdRow['mothersName'];
	  	$stdArray[$i]['grNo']              = $stdRow['grNo'];
	  	$stdArray[$i]['activated']         = $stdRow['activated'];
	  	$stdArray[$i]['dateOfBirth']       = $stdRow['dateOfBirth'];
	  	$stdArray[$i]['joinedInClass']     = $stdRow['joinedInClass'];
	  	$stdArray[$i]['joiningDate']       = $stdRow['joiningDate'];
	  	$stdArray[$i]['gender']            = $stdRow['gender'];
	  	$stdArray[$i]['bloodGroup']        = $stdRow['bloodGroup'];
	  	$stdArray[$i]['houseName']         = $stdRow['houseName'];
	  	$stdArray[$i]['currentAddress']    = $stdRow['currentAddress'];
      $stdArray[$i]['currentState']      = $stdRow['currentState'];
      $stdArray[$i]['currentAddressPin'] = $stdRow['currentAddressPin'];
	  	$stdArray[$i]['residencePhone1']   = $stdRow['residencePhone1'];
	  	$stdArray[$i]['residencePhone2']   = $stdRow['residencePhone2'];
	  	$stdArray[$i]['studentEmail']      = $stdRow['studentEmail'];
	  	$stdArray[$i]['permanentAddress']  = $stdRow['permanentAddress'];
	  	$stdArray[$i]['permanentState']    = $stdRow['permanentState'];
      $stdArray[$i]['permanentPIN']      = $stdRow['permanentPIN'];
	  	$stdArray[$i]['religion']          = $stdRow['religion'];
	  	$stdArray[$i]['previousSchool']    = $stdRow['previousSchool'];
	  	$i++;
	  }
	}
	else
	{
		$notFound = 'Record Not Found';
	}
	
	$houseArray = array();
	$h = 0;
  $selectClass = "SELECT houseId,houseName
                    FROM house
                ORDER BY houseName";
  $selectClassRes = mysql_query($selectClass);
  while($classRow = mysql_fetch_array($selectClassRes))
  {
  	$houseArray['houseId'][$h]   = $classRow['houseId'];
  	$houseArray['houseName'][$h] = $classRow['houseName'];
  	$h++;
  }
  
  $c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  $secArrOut[0] = 'A';
  $secArrOut[1] = 'B';
  $secArrOut[2] = 'C';
  $secArrOut[3] = 'D';
  
	include("./bottom.php");
	$smarty->assign('cArray',$cArray);
	$smarty->assign('class',$class);
  $smarty->assign('section',$section);
  $smarty->assign('secArrOut',$secArrOut);
  $smarty->assign('academicStartYear',$academicStartYear);
  $smarty->assign('academicEndYear',$academicEndYear);
	$smarty->assign('stdArray',$stdArray);
	$smarty->assign('houseArray',$houseArray);
	$smarty->assign('notFound',$notFound);
	$smarty->assign('houseId',$houseId);
	$smarty->assign('countStudent',$countStudent);
  $smarty->display('houseWise.tpl');
}
?>
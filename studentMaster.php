<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$filePath = '';
	$fileName = '';
	
	$studentId           = '';
	$grNo                = '';
	$userType            = '';
	$studentLoginId      = '';
	$studentPassword     = '';
	$parentLoginId       = '';
	$parentPassword      = '';
	$activated           = '';
	$class               = '';
	$joiningDate         = '';
	$studentName         = '';
	$dateOfBirth         = '';
	$gender              = '';
	$bloodGroup          = '';
	$houseName           = '';
	$currentAddress      = '';
	$currentState        = '';
	$currentAddressPin   = '';
	$residencePhone1     = '';
	$residencePhone2     = '';
	$smsMobile           = '';
	$studentEmail        = '';
	$fatherName          = '';
	$fatherPhone         = '';
	$fatherEmail         = '';
	$fatherMobile        = '';
	$fatherOccupation    = '';
	$fathersDesignation  = '';
	$fathersOrganization = '';
	$mothersName         = '';
	$mothersPhone        = '';
	$mothersEmailid      = '';
	$mothersMobile       = '';
	$motherOccupation    = '';
	$mothersDesignation  = '';
	$mothersOrganization = '';
	$permanentAddress    = '';
	$permanentState      = '';
	$permanentPIN        = '';
	$urban               = '';
	$religion            = '';
	$createdOn           = '';
	$previousSchool      = '';
	
	
	if(isset($_POST['submitBtn']))
	{
	  $uploaddir = dirname($_POST['filePath']);
	
	  $uploadfile = $uploaddir . basename($_FILES['fileName']['name']);
	  
	  $target_path = './uploadXls';
	  $target_path = $target_path ."/". basename( $_FILES['fileName']['name']);
	  $_FILES['fileName']['tmp_name']; // temp file
	  
	  $oldfile =  basename($_FILES['fileName']['name']);
	
	  // getting the extention
	
	  $pos = strpos($oldfile,".",0);
	  $ext = trim(substr($oldfile,$pos+1,strlen($oldfile))," ");
	  
	  if(move_uploaded_file($_FILES['fileName']['tmp_name'], $target_path)) 
	  {
	  	$row = 0;
	    $handle = fopen($target_path, "r");
	    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
	    {
	      if($row > 0)
	      {
	        $studentId           = $data[0];
					$grNo                = $data[11];
					if($grNo != "")
					{
						if($grNo <= 9)
						{
							//IFS00001
					    $grNo = "000".$grNo;
					  }
					  elseif($grNo > 9 && $grNo <= 99)
					  {
					  	//IFS00010
					  	$grNo = "00".$grNo;
					  }
					  elseif($grNo > 99 && $grNo <= 999)
					  {
					  	//IFS00100
					  	$grNo = "0".$grNo;
					  }
					  else
					  {
					  	//IFS00100
					  	$grNo = $grNo;
					  }
					}
					$userType            = $data[3];
					$studentLoginId      = $data[4];
					$studentPassword     = $data[5];
					$parentLoginId       = $data[6];
					$parentPassword      = $data[7];
	        if($data[10] == 'Yes')
	        {
					  $activated           = 'Y';
					}
					else
					{
					  $activated           = 'N';
					}
					
					if($data[14] == 'I')
          {
            $class = 1;
          }
          elseif($data[14] == 'II')
          {
          	$class = 2;
          }
          elseif($data[14] == 'III')
          {
          	$class = 3;
          }
          elseif($data[14] == 'IV')
          {
          	$class = 4;
          }
          elseif($data[14] == 'V')
          {
          	$class = 5;
          }
          elseif($data[14] == 'VI')
          {
          	$class = 6;
          }
          elseif($data[14] == 'VII')
          {
          	$class = 7;
          }
          elseif($data[14] == 'VIII')
          {
          	$class = 8;
          }
          elseif($data[14] == 'IX')
          {
          	$class = 9;
          }
          elseif($data[14] == 'X')
          {
          	$class = 10;
          }
          elseif($data[14] == 'XI Science')
          {
          	$class = 11 .' SCI.';
          }
          elseif($data[14] == 'XI Commerce')
          {
          	$class = 11 .' COM.';
          }
          elseif($data[14] == 'XII Science')
          {
          	$class = 12 .' SCI.';
          }
          elseif($data[14] == 'XII Commerce')
          {
          	$class = 12 .' COM.';
          }
          else
          {
          	$class = $data[14];
          }
					
					if($data[15] != '')
					{
					  $joiningDate       = substr($data[15], 6, 4)."-".substr($data[15], 3, 2)."-".substr($data[15], 0, 2);
				  }
				  else
				  {
				  	$joiningDate       = '0000-00-00';
				  }
					$studentName         = $data[18];
					if($data[19] != '')
					{
					  $dateOfBirth       = substr($data[19], 6, 4)."-".substr($data[19], 3, 2)."-".substr($data[19], 0, 2);
					}
					else
					{
						$dateOfBirth       = '0000-00-00';
					}
					$gender              = $data[20];
					$bloodGroup          = $data[21];
					
					$houseId = 0;
					$selectHouse = "SELECT houseId
					                  FROM house
					                 WHERE houseName = '".$data[23]."'";
          $selectHouseRes = mysql_query($selectHouse);
          if($houseRow = mysql_fetch_array($selectHouseRes))
          {
          	$houseId = $houseRow['houseId'];
          }
          
					$houseName           = $houseId;
					$currentAddress      = addslashes($data[24]);
					$currentState        = $data[25];
					$currentAddressPin   = $data[26];
					$residencePhone1     = $data[27];
					$residencePhone2     = $data[28];
					$smsMobile           = $data[29];
					$studentEmail        = $data[30];
					$fatherName          = $data[31];
					$fatherPhone         = $data[32];
					$fatherEmail         = $data[33];
					$fatherMobile        = $data[34];
					$fatherOccupation    = addslashes($data[35]);
					$fathersDesignation  = addslashes($data[36]);
					$fathersOrganization = addslashes($data[37]);
					$mothersName         = $data[38];
					$mothersPhone        = $data[39];
					$mothersEmailid      = $data[40];
					$mothersMobile       = $data[41];
					$motherOccupation    = addslashes($data[42]);
					$mothersDesignation  = addslashes($data[43]);
					$mothersOrganization = addslashes($data[44]);
					$permanentAddress    = addslashes($data[49]);
					$permanentState      = $data[50];
					$permanentPIN        = $data[51];
					$urban               = $data[53];
					$religion            = $data[54];
					if($data[56] != '')
				  {
					  $createdOn         = substr($data[56], 6, 4)."-".substr($data[56], 3, 2)."-".substr($data[56], 0, 2);
					}
					else
					{
						$createdOn         = '0000-00-00';
					}
					$studentImage        = '<img height="150" src="studentImage/'.$grNo.'_P.jpg" alt="'.$grNo.'_P.jpg" class="studentImage"><p id="large"></p>';
					$fatherImage         = '<img height="150" src="studentImage/'.$grNo.'_F.jpg" alt="'.$grNo.'_F.jpg" class="studentImage"><p id="large"></p>';
					$motherImage         = '<img height="150" src="studentImage/'.$grNo.'_M.jpg" alt="'.$grNo.'_M.jpg" class="studentImage"><p id="large"></p>';
					if (!isset($data[58]))
					{
					  $previousSchool    = '';
				  }
				  else
				  {
				  	$previousSchool    = $data[58];
				  }
	       
	        $uploadStudentMaster = "INSERT INTO studentmaster 
	                                       (studentId,
																					grNo,
																					userType,
																					studentLoginId,
																					studentPassword,
																					parentLoginId,
																					parentPassword,
																					activated,
																					joinedInClass,
																					joiningDate,
																					studentName,
																					dateOfBirth,
																					gender,
																					bloodGroup,
																					houseId,
																					currentAddress,
																					currentState,
																					currentAddressPin,
																					residencePhone1,
																					residencePhone2,
																					smsMobile,
																					studentEmail,
																					fatherName,
																					fatherPhone,
																					fatherEmail,
																					fatherMobile,
																					fatherOccupation,
																					fathersDesignation,
																					fathersOrganization,
																					mothersName,
																					mothersPhone,
																					mothersEmailid,
																					mothersMobile,
																					motherOccupation,
																					mothersDesignation,
																					mothersOrganization,
																					permanentAddress,
																					permanentState,
																					permanentPIN,
																					urban,
																					religion,
																					createdOn,
																					previousSchool,
																					studentImage,
																					fatherImage,
																					motherImage)
	      	                        VALUES ('".$studentId."',
	      	                                '".$grNo."',
	      	                                '".$userType."',
	      	                                '".$studentLoginId."',
	      	                                '".$studentPassword."',
	      	                                '".$parentLoginId."',
	      	                                '".$parentPassword."',
	      	                                '".$activated."',
	      	                                '".$class."',
	      	                                '".$joiningDate."',
	      	                                '".$studentName."',
	      	                                '".$dateOfBirth."',
	      	                                '".$gender."',
	      	                                '".$bloodGroup."',
	      	                                '".$houseName."',
	      	                                '".$currentAddress."',
	      	                                '".$currentState."',
	      	                                '".$currentAddressPin."',
	      	                                '".$residencePhone1."',
	      	                                '".$residencePhone2."',
	      	                                '".$smsMobile."',
	      	                                '".$studentEmail."',
	      	                                '".$fatherName."',
	      	                                '".$fatherPhone."',
	      	                                '".$fatherEmail."',
	      	                                '".$fatherMobile."',
	      	                                '".$fatherOccupation."',
	      	                                '".$fathersDesignation."',
	      	                                '".$fathersOrganization."',
	      	                                '".$mothersName."',
	      	                                '".$mothersPhone."',
	      	                                '".$mothersEmailid."',
	      	                                '".$mothersMobile."',
	      	                                '".$motherOccupation."',
	      	                                '".$mothersDesignation."',
	      	                                '".$mothersOrganization."',
	      	                                '".$permanentAddress."',
	      	                                '".$permanentState."',
	      	                                '".$permanentPIN."',
	      	                                '".$urban."',
	      	                                '".$religion."',
	      	                                '".$createdOn."',
	      	                                '".$previousSchool."',
	      	                                '".$studentImage."',
	      	                                '".$fatherImage."',
	      	                                '".$motherImage."')";
	      	om_query($uploadStudentMaster);
		    }
	      $row++;
	    }
	  } 
	  else
	  {
	    echo "There was an error uploading the file, please try again!";
	  }
	}
}
?>
<body>
<H1><CENTER><FONT color="red">Student Profile</FONT></CENTER></H1><HR>
<CENTER>
  <FORM enctype="multipart/form-data" action="" name="studentMaster" method="POST"> 
	<a href="index.php">Home</a>
	<br /><br />
  <INPUT size="60" type="hidden" name="filePath"><BR>
  File Name: 
  <INPUT name="fileName" type="file" onChange="document.studentMaster.filePath.value=document.studentMaster.fileName.value;">
  <INPUT type="submit" name="submitBtn" value="Submit File"> 
</FORM>
</CENTER>
<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$empArray = array();
  $i = 0;
  $selectEmp = "SELECT employeeMasterId,name,userType,loginId,employeeCode,password,dateOfBirth,joiningDate,gender,currentAddress,currentAddressPin,residenceTelephone,
                       permanentAddress,mobile,phone1,phone2,email,marital,accountNo,panNo,qualification,experience,bloodGroup,activated,retireDate,designation
                  FROM employeemaster order by activated desc,userType,name";
  $selectEmpRes = mysql_query($selectEmp);
  while($empRow = mysql_fetch_array($selectEmpRes))
  {
  	$empArray[$i]['employeeMasterId']   = $empRow['employeeMasterId'];
  	$empArray[$i]['name']               = $empRow['name'];
  	$empArray[$i]['userType']           = $empRow['userType'];
  	$empArray[$i]['employeeCode']       = $empRow['employeeCode'];
  	$empArray[$i]['loginId']            = $empRow['loginId'];
  	$empArray[$i]['password']           = $empRow['password'];
  	$empArray[$i]['activated']          = $empRow['activated'];
  	$empArray[$i]['retireDate']         = $empRow['retireDate'];
  	$empArray[$i]['dateOfBirth']        = $empRow['dateOfBirth'];
  	$empArray[$i]['joiningDate']        = $empRow['joiningDate'];
  	$empArray[$i]['gender']             = $empRow['gender'];
  	$empArray[$i]['currentAddress']     = $empRow['currentAddress'];
  	$empArray[$i]['currentAddressPin']  = $empRow['currentAddressPin'];
  	$empArray[$i]['residenceTelephone'] = $empRow['residenceTelephone'];
  	$empArray[$i]['permanentAddress']   = $empRow['permanentAddress'];
  	$empArray[$i]['mobile']             = $empRow['mobile'];
  	$empArray[$i]['phone1']             = $empRow['phone1'];
  	$empArray[$i]['phone2']             = $empRow['phone2'];
  	$empArray[$i]['email']              = $empRow['email'];
  	$empArray[$i]['marital']            = $empRow['marital'];
  	$empArray[$i]['accountNo']          = $empRow['accountNo'];
  	$empArray[$i]['panNo']              = $empRow['panNo'];
  	$empArray[$i]['qualification']      = $empRow['qualification'];
  	$empArray[$i]['experience']         = $empRow['experience'];
  	$empArray[$i]['bloodGroup']         = $empRow['bloodGroup'];
  	$empArray[$i]['designation']         = $empRow['designation'];
  	$i++;
  }
  include("./bottom.php");
  $smarty->assign('empArray',$empArray);
  $smarty->display('employeAllListEdit.tpl');  
}
?>
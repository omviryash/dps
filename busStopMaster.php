<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$isEdit          = 0;
	$vehicleMasterId = '';
	$routeMasterId   = '';
	$busTime         = '';
	$localArea       = '';
	$busStop         = '';
	$distance        = '';
	$busStopMasterId = 0;
	
	$classArray      = array();
	$typeArr         = array();
	$rArr            = array();
	
  if(isset($_POST['Submit']))
  {
  	$vehicleMasterId = isset($_POST['vehicleMasterId']) ? $_POST['vehicleMasterId'] : 0;
  	$routeMasterId   = isset($_POST['routeMasterId']) ? $_POST['routeMasterId'] : 0;
  	$busTime         = $_POST['busTimeHour'].':'.$_POST['busTimeMinute'].':00';
  	$localArea       = isset($_POST['localArea']) ? $_POST['localArea'] : 0;
  	$busStop         = isset($_POST['busStop']) ? $_POST['busStop'] : '';
  	$distance        = isset($_POST['distance']) ? $_POST['distance'] : '';
  	$busStopMasterId = isset($_POST['busStopMasterId']) ? $_POST['busStopMasterId'] : 0;
  	
  	if($busStopMasterId == 0)
  	{
  	  $insertClass = "INSERT INTO busstopmaster (vehicleMasterId,routeMasterId,busTime,localArea,busStop,distance)
  	                  VALUES (".$vehicleMasterId.",".$routeMasterId.",'".$busTime."','".$localArea."','".$busStop."','".$distance."')";
  	  $insertClassRes = om_query($insertClass);
  	  if(!$insertClassRes)
  	  {
  	  	echo "Insert Fail";
  	  }
  	  else
  	  {
  	  	header("Location:busStopMaster.php?done=1");
  	  }
  	}
  	else
  	{
  	  $isEdit = 1;
  	  $updateClass = "UPDATE busstopmaster
  	                     SET vehicleMasterId   = ".$vehicleMasterId.",
  	                         routeMasterId = ".$routeMasterId.",
  	                         busTime = '".$busTime."',
  	                         localArea = '".$localArea."',
  	                         busStop    = '".$busStop."',
  	                         distance = '".$distance."'
  	                   WHERE busStopMasterId = ".$_REQUEST['busStopMasterId'];
      $updateClassRes = om_query($updateClass);
      if(!$updateClassRes)
      {
      	echo "Update Fail";
      }
      else
      {
      	header("Location:busStopMaster.php?done=1");
      }
  	}
  }
  
  $i = 0;
  $selectClass = "SELECT busstopmaster.busStopMasterId,busstopmaster.vehicleMasterId,busstopmaster.routeMasterId,busstopmaster.busTime,busstopmaster.localArea,busstopmaster.busStop,
                         busstopmaster.distance
                    FROM busstopmaster";
  $selectClassRes = mysql_query($selectClass);
  while($classRow = mysql_fetch_array($selectClassRes))
  {
  	$classArray[$i]['busStopMasterId'] = $classRow['busStopMasterId'];
  	$classArray[$i]['vehicleMasterId']        = $classRow['vehicleMasterId'];
  	$classArray[$i]['routeMasterId']        = $classRow['routeMasterId'];
  	$classArray[$i]['busTime']        = $classRow['busTime'];
  	$classArray[$i]['localArea'] = $classRow['localArea'];
  	$classArray[$i]['busStop']       = $classRow['busStop'];
  	$classArray[$i]['distance']         = $classRow['distance'];
  	$i++;
  }
  
  if(isset($_REQUEST['busStopMasterId']) > 0)
  {
    $selectClass = "SELECT busstopmaster.busStopMasterId,busstopmaster.vehicleMasterId,busstopmaster.routeMasterId,busstopmaster.busTime,
                           busstopmaster.localArea,busstopmaster.busStop,busstopmaster.distance
	                    FROM busstopmaster
                     WHERE busStopMasterId = ".$_REQUEST['busStopMasterId']."";
    $selectClassRes = mysql_query($selectClass);
    if($classRow = mysql_fetch_array($selectClassRes))
    {
    	$busStopMasterId = $classRow['busStopMasterId'];
    	$vehicleMasterId = $classRow['vehicleMasterId'];
    	$routeMasterId   = $classRow['routeMasterId'];
    	$busTime         = $classRow['busTime'];
    	$localArea       = $classRow['localArea'];
    	$busStop         = $classRow['busStop'];
    	$distance        = $classRow['distance'];
    }
  }
  
  $k = 0;
  $selectClub = "SELECT vehicleMasterId,vehicleNo
                   FROM vehiclemaster";
  $selectClubRes = mysql_query($selectClub);
  while($clubRow = mysql_fetch_array($selectClubRes))
  {
  	$typeArr['vehicleMasterId'][$k] = $clubRow['vehicleMasterId'];
  	$typeArr['vehicleNo'][$k]   = $clubRow['vehicleNo'];
  	$k++;
  }
  
  $r = 0;
  $selectR = "SELECT routeMasterId,routeName
                FROM routemaster";
  $selectRRes = mysql_query($selectR);
  while($rRow = mysql_fetch_array($selectRRes))
  {
  	$rArr['routeMasterId'][$r] = $rRow['routeMasterId'];
  	$rArr['routeName'][$r]     = $rRow['routeName'];
  	$r++;
  }
  
  include("./bottom.php");
  $smarty->assign('busStopMasterId',$busStopMasterId);
  $smarty->assign('isEdit',$isEdit);
  $smarty->assign('classArray',$classArray);
  $smarty->assign('vehicleMasterId',$vehicleMasterId);
  $smarty->assign('routeMasterId',$routeMasterId);
  $smarty->assign('busTime',$busTime);
  $smarty->assign('localArea',$localArea);
  $smarty->assign('busStop',$busStop);
  $smarty->assign('distance',$distance);
  $smarty->assign('typeArr',$typeArr);
  $smarty->assign('rArr',$rArr);
  $smarty->display('busStopMaster.tpl'); 
}
?>
<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$isEdit          = 0;
	$bookType        = '';
	$bookAccessionNo = '';
	$ISBN= '';
	$bookTitle       = '';
	$author1         = '';
	$author2         = '';
	$location        = '';
	$price           = '';
	$publisherId     = '';
	$language        = 'English';
	$bookMasterId    = 0;
	
	$classArray      = array();
	
  if(isset($_POST['Submit']))
  {
  	$bookType            = isset($_POST['bookType']) ? $_POST['bookType'] : 0;
  	$bookAccessionNo     = isset($_POST['bookAccessionNo']) ? $_POST['bookAccessionNo'] : 0;
 	$ISBN		     = isset($_POST['ISBN']) ? $_POST['ISBN'] : 0;
  	$bookTitle           = isset($_POST['bookTitle']) ? $_POST['bookTitle'] : '';
  	$author1             = isset($_POST['author1']) ? $_POST['author1'] : '';
  	$author2             = isset($_POST['author2']) ? $_POST['author2'] : '';
  	$location            = isset($_POST['location']) ? $_POST['location'] : '';
  	$price               = isset($_POST['price']) ? $_POST['price'] : '';
  	$publisherId         = isset($_POST['publisherId']) ? $_POST['publisherId'] : '';
  	$language            = isset($_POST['language']) ? $_POST['language'] : '';
  	$bookMasterId        = isset($_POST['bookMasterId']) ? $_POST['bookMasterId'] : 0;
  	
  	if($bookMasterId == 0)
  	{
  	  $insertClass = "INSERT INTO bookmaster (bookType,bookAccessionNo,ISBN,bookTitle,author1,author2,location,price,publisherId,language)
  	                  VALUES ('".$bookType."',".$bookAccessionNo.",'".$ISBN."','".$bookTitle."','".$author1."','".$author2."','".$location."','".$price."','".$publisherId."','".$language."')";
  	  $insertClassRes = om_query($insertClass);
  	  if(!$insertClassRes)
  	  {
  	  	echo "Insert Fail";
  	  }
  	  else
  	  {
  	  	header("Location:bookEntry.php?done=1");
  	  }
  	}
  	else
  	{
  	  $isEdit = 1;
  	  $updateClass = "UPDATE bookmaster
  	                     SET bookType   = '".$bookType."',
  	                         bookAccessionNo = ".$bookAccessionNo.",
     	                         ISBN = '".$ISBN."',
  	                         bookTitle    = '".$bookTitle."',
  	                         author1 = '".$author1."',
  	                         author2 = '".$author2."',
  	                         location = '".$location."',
  	                         price = '".$price."',
  	                         publisherId = '".$publisherId."',
  	                         language = '".$language."'
  	                   WHERE bookMasterId = ".$_REQUEST['bookMasterId'];
      $updateClassRes = om_query($updateClass);
      if(!$updateClassRes)
      {
      	echo "Update Fail";
      }
      else
      {
      	header("Location:bookEntry.php?done=1");
      }
  	}
  }
  
  $i = 0;
  $selectClass = "SELECT bookmaster.bookMasterId,bookmaster.bookType,bookmaster.bookAccessionNo,bookmaster.ISBN,bookmaster.bookTitle,
                         bookmaster.author1,bookmaster.author2,bookmaster.location,bookmaster.price,bookmaster.publisherId,bookmaster.language
                    FROM bookmaster";
  $selectClassRes = mysql_query($selectClass);
  while($classRow = mysql_fetch_array($selectClassRes))
  {
  	$classArray[$i]['bookMasterId']    = $classRow['bookMasterId'];
  	$classArray[$i]['bookType']        = $classRow['bookType'];
  	$classArray[$i]['bookAccessionNo'] = $classRow['bookAccessionNo'];
  	$classArray[$i]['ISBN'] 	   = $classRow['ISBN'];
  	$classArray[$i]['bookTitle']       = $classRow['bookTitle'];
  	$classArray[$i]['author1']         = $classRow['author1'];
  	$classArray[$i]['author2']         = $classRow['author2'];
  	$classArray[$i]['location']        = $classRow['location'];
  	$classArray[$i]['price']           = $classRow['price'];
  	$classArray[$i]['publisherId']     = $classRow['publisherId'];
  	$classArray[$i]['language']        = $classRow['language'];
  	$i++;
  }
  
  if(isset($_REQUEST['bookMasterId']) > 0)
  {
    $selectClass = "SELECT bookmaster.bookMasterId,bookmaster.bookType,
                           bookmaster.bookAccessionNo,bookmaster.ISBN,bookmaster.bookTitle,bookmaster.author1,
                           bookmaster.author2,bookmaster.location,bookmaster.price,bookmaster.publisherId,bookmaster.language
	                    FROM bookmaster
                     WHERE bookMasterId = ".$_REQUEST['bookMasterId']."";
    $selectClassRes = mysql_query($selectClass);
    if($classRow = mysql_fetch_array($selectClassRes))
    {
    	$bookMasterId    = $classRow['bookMasterId'];
    	$bookType        = $classRow['bookType'];
    	$bookAccessionNo = $classRow['bookAccessionNo'];
    	$ISBN 		 = $classRow['ISBN'];
    	$bookTitle       = $classRow['bookTitle'];
    	$author1         = $classRow['author1'];
    	$author2         = $classRow['author2'];
    	$location        = $classRow['location'];
    	$price           = $classRow['price'];
    	$publisherId     = $classRow['publisherId'];
    	$language        = $classRow['language'];
    }
  }
  
  $k = 0;
  $selectClub = "SELECT bookTypeId,bookType
                   FROM booktype";
  $selectClubRes = mysql_query($selectClub);
  while($clubRow = mysql_fetch_array($selectClubRes))
  {
  	$typeArr['bookTypeId'][$k]   = $clubRow['bookTypeId'];
  	$typeArr['bookType'][$k]     = $clubRow['bookType'];
  	$k++;
  }
  
  include("./bottom.php");
  $smarty->assign('bookMasterId',$bookMasterId);
  $smarty->assign('isEdit',$isEdit);
  $smarty->assign('classArray',$classArray);
  $smarty->assign('bookType',$bookType);
  $smarty->assign('bookAccessionNo',$bookAccessionNo);
  $smarty->assign('ISBN',$ISBN);
  $smarty->assign('bookTitle',$bookTitle);
  $smarty->assign('author1',$author1);
  $smarty->assign('author2',$author2);
  $smarty->assign('location',$location);
  $smarty->assign('price',$price);
  $smarty->assign('publisherId',$publisherId);
  $smarty->assign('language',$language);
  $smarty->assign('typeArr',$typeArr);
  $smarty->display('bookEntry.tpl'); 
}
?>
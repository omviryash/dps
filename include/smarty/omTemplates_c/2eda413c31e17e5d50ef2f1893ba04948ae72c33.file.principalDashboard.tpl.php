<?php /* Smarty version Smarty-3.1.16, created on 2016-12-27 11:37:14
         compiled from "./templates/principalDashboard.tpl" */ ?>
<?php /*%%SmartyHeaderCode:50100154658620512317930-57542066%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2eda413c31e17e5d50ef2f1893ba04948ae72c33' => 
    array (
      0 => './templates/principalDashboard.tpl',
      1 => 1482817430,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '50100154658620512317930-57542066',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'presentTeacher' => 0,
    'totalTeacher' => 0,
    'today' => 0,
    'presentStudent' => 0,
    'totalStudent' => 0,
    'presentAdmin' => 0,
    'totalAdmin' => 0,
    'presentStudentIV' => 0,
    'totalStudentIV' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_58620512382298_59999005',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58620512382298_59999005')) {function content_58620512382298_59999005($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("./main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />


</br></br>
<div class="hd"><h2 align="center">Principal Dashboard</h2>
</div>
<div class="content">
<div class="row">
      <div class="col-lg-3 col-xs-6"> 
        <!-- small box -->
        <div class="small-box bg-light-blue">
          <div class="inner">
            <h3> <?php echo $_smarty_tpl->tpl_vars['presentTeacher']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['totalTeacher']->value;?>
 </h3>
            <p> Teaching Staff </p>
          </div>
          <a class="small-box-footer" href="attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['today']->value;?>
"> More info  </a> </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-xs-6"> 
        <!-- small box -->
        <div class="small-box bg-orange">
          <div class="inner">
            <h3 id="count_reg"><?php echo $_smarty_tpl->tpl_vars['presentStudent']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['totalStudent']->value;?>
</h3>
            <p> Student </p>
          </div>
          <a class="small-box-footer" href="attendenceReport.php"> More info  </a> </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-xs-6"> 
        <!-- small box -->
        <div class="small-box bg-grass">
          <div class="inner">
            <h3 id="count_present"><?php echo $_smarty_tpl->tpl_vars['presentAdmin']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['totalAdmin']->value;?>
</h3>
            <p> Admin Staff </p>
          </div>
          <a class="small-box-footer" href="attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['today']->value;?>
&staff_type=Admin Staff"> More info  </a> </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-3 col-xs-6"> 
        <!-- small box -->
        <div class="small-box bg-wood">
          <div class="inner">
            <h3 id="count_device"><?php echo $_smarty_tpl->tpl_vars['presentStudentIV']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['totalStudentIV']->value;?>
</h3>
            <p> Class IV Staff </p>
          </div>
          <a class="small-box-footer" href="attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['today']->value;?>
&staff_type=Class IV"> More info  </a> </div>
      </div>
      <!-- ./col --> 
</div>
</div>
<?php }} ?>

<?php /* Smarty version Smarty-3.1.16, created on 2016-12-27 11:42:03
         compiled from "./templates/timeTableEntry.tpl" */ ?>
<?php /*%%SmartyHeaderCode:13355296958620633aff642-51386910%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c6d6959fa6d43cb340ddad7dcfe7bb3be1dc4f30' => 
    array (
      0 => './templates/timeTableEntry.tpl',
      1 => 1482817430,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13355296958620633aff642-51386910',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'timeTableId' => 0,
    'confirm' => 0,
    'dateArrVal' => 0,
    'dateArrOut' => 0,
    'academicStartYear' => 0,
    'cArray' => 0,
    'class' => 0,
    'secArrOut' => 0,
    'section' => 0,
    'tArr' => 0,
    'employeeMasterId' => 0,
    'sArr' => 0,
    'subjectMasterId' => 0,
    'weekOut' => 0,
    'weekDay' => 0,
    'periodOut' => 0,
    'periodNo' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_58620633b5b1b5_68958008',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58620633b5b1b5_68958008')) {function content_58620633b5b1b5_68958008($_smarty_tpl) {?><?php if (!is_callable('smarty_function_html_options')) include '/opt/lampp/htdocs/dps/include/smarty/libs/plugins/function.html_options.php';
?><?php echo $_smarty_tpl->getSubTemplate ("./main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<br>
<br>
<br>



<form name="form1" method="POST" action="timeTableEntry.php">
<table align="center" border="0" class="table2">
<div class="hd"><h2 align="center">Time Table Entry</h2>
<tr>
  <input type="" name="timeTableId" value="<?php echo $_smarty_tpl->tpl_vars['timeTableId']->value;?>
">
</tr>
<tr>
  <td align="center" colspan="2" style="color:red; font-size:18px;"><b><?php echo $_smarty_tpl->tpl_vars['confirm']->value;?>
</b></td>
</tr>
<tr>
	<td class="table2">Academic Year</td>
	<td class="table2 form01">
	  <select name="startYear">
	    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['dateArrVal']->value,'output'=>$_smarty_tpl->tpl_vars['dateArrOut']->value,'selected'=>$_smarty_tpl->tpl_vars['academicStartYear']->value),$_smarty_tpl);?>

	  </select>
	</td>
</tr>
<tr>
  <td class="table2">Class</td>
  <td class="table2 form01">
  	<select name="class" autofocus="autofocus" required >
	    <option value="">Select class</option>
	    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['cArray']->value['className'],'output'=>$_smarty_tpl->tpl_vars['cArray']->value['className'],'selected'=>$_smarty_tpl->tpl_vars['class']->value),$_smarty_tpl);?>

	  </select>
  </td>
</tr>
<tr>
  <td class="table2">Section</td>
  <td class="table2 form01">
  	<select name="section">
      <option value="0">Select Section</option>
      <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['secArrOut']->value,'output'=>$_smarty_tpl->tpl_vars['secArrOut']->value,'selected'=>$_smarty_tpl->tpl_vars['section']->value),$_smarty_tpl);?>

	  </select>
	</td>
</tr>
<tr>
  <td class="table2">Teacher</td>
  <td class="table2 form01">
  	<select name="employeeMasterId">
	    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['tArr']->value['employeeMasterId'],'output'=>$_smarty_tpl->tpl_vars['tArr']->value['name'],'selected'=>$_smarty_tpl->tpl_vars['employeeMasterId']->value),$_smarty_tpl);?>

	  </select>
  </td>
</tr>
<tr>
  <td class="table2">Subject</td>
  <td class="table2 form01">
  	<select name="subjectMasterId">
	    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['sArr']->value['subjectMasterId'],'output'=>$_smarty_tpl->tpl_vars['sArr']->value['subjectName'],'selected'=>$_smarty_tpl->tpl_vars['subjectMasterId']->value),$_smarty_tpl);?>

	  </select>
  </td>
</tr>
<tr>
  <td class="table2">WeekDay</td>
  <td class="table2 form01">
  	<select name="weekDay">
	    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['weekOut']->value,'output'=>$_smarty_tpl->tpl_vars['weekOut']->value,'selected'=>$_smarty_tpl->tpl_vars['weekDay']->value),$_smarty_tpl);?>

	  </select>
  </td>
</tr>
<tr>
  <td class="table2">Period No</td>
  <td class="table2 form01">
  	<select name="periodNo">
	    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['periodOut']->value,'output'=>$_smarty_tpl->tpl_vars['periodOut']->value,'selected'=>$_smarty_tpl->tpl_vars['periodNo']->value),$_smarty_tpl);?>

	  </select>
  </td>
</tr>
<tr>
  <td class="table2"></td>
  <td class="table2 form01" align="center"><input type="submit" name="Submit" class="button" value="Submit"></td>
</tr>
</table>
</form>
<?php }} ?>

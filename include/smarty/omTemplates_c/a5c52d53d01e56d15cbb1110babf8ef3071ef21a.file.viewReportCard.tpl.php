<?php /* Smarty version Smarty-3.1.16, created on 2016-12-27 11:42:34
         compiled from "./templates/viewReportCard.tpl" */ ?>
<?php /*%%SmartyHeaderCode:62378121958620652174b51-64458994%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a5c52d53d01e56d15cbb1110babf8ef3071ef21a' => 
    array (
      0 => './templates/viewReportCard.tpl',
      1 => 1482817430,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '62378121958620652174b51-64458994',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'dateArrVal' => 0,
    'dateArrOut' => 0,
    'academicStartYearSelected' => 0,
    's_userType' => 0,
    'cArray' => 0,
    'class' => 0,
    'secArrOut' => 0,
    'section_sel' => 0,
    'count' => 0,
    'section' => 0,
    'dtlArr' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_586206521e7b79_04712648',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_586206521e7b79_04712648')) {function content_586206521e7b79_04712648($_smarty_tpl) {?><?php if (!is_callable('smarty_function_html_options')) include '/opt/lampp/htdocs/dps/include/smarty/libs/plugins/function.html_options.php';
?><?php echo $_smarty_tpl->getSubTemplate ("./main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<link rel="stylesheet" href="./css/buttonStyle.css" type="text/css" />
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript">
$(document).ready(function(){
	$('#myDataTable').dataTable({
		"aLengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 100,
  	"bAutoWidth": false,
  	"aaSorting": [[ 0, "asc" ]],
		"bJQueryUI":true
  });
  
	$(".omAttend").change(function()
  {
  	$('.newGoBtn').click();
  });
  
  $('.checkAll').click(function () {
    $('.checkboxAll').prop('checked', this.checked);
  });
  
  $('.checkAll2').click(function () {
    $('.checkboxAll2').prop('checked', this.checked);
  });
});


function buttonHide()
{
	$('#hideMe').hide();
}

</script>
<style>
.selectCombo
{
	width:500px;
}
</style>


</br></br>
<center>
<form name="formGet" id="formGet" method="GET" action="viewReportCard.php">
<table align="center" border="1">
	<div class="hd"><h2 align="center">Report Card Visible</h2></div>
	<tr>
      
		<td class="table2 form01">
		  <select name="startYear" class="omAttend">
		    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['dateArrVal']->value,'output'=>$_smarty_tpl->tpl_vars['dateArrOut']->value,'selected'=>$_smarty_tpl->tpl_vars['academicStartYearSelected']->value),$_smarty_tpl);?>

		  </select>
		</td>
		<?php if ($_smarty_tpl->tpl_vars['s_userType']->value=='Administrator') {?>
		<td class="table2 form01 omAttend">
		  <select name="class" autofocus="autofocus">
		    <option value="0">Select class</option>
		    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['cArray']->value['className'],'output'=>$_smarty_tpl->tpl_vars['cArray']->value['className'],'selected'=>$_smarty_tpl->tpl_vars['class']->value),$_smarty_tpl);?>

		  </select>
	  </td>
    <td class="table2 form01 omAttend">
		  <select name="section">
		    <option value="0">Select Section</option>
		    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['secArrOut']->value,'output'=>$_smarty_tpl->tpl_vars['secArrOut']->value,'selected'=>$_smarty_tpl->tpl_vars['section_sel']->value),$_smarty_tpl);?>

		  </select>
	  </td>
	  <?php }?>
		<td>
	    <input type="submit" name="go" class="newGoBtn" value="go">
	  </td>
	</tr>
</table>
</form>
</center>
<?php if ($_smarty_tpl->tpl_vars['count']->value!=0) {?>
<form name="form2" method="POST" action="viewReportCard.php">
	<input type="hidden" name="startYear" value="<?php echo $_smarty_tpl->tpl_vars['academicStartYearSelected']->value;?>
">
  <table align="center" border="1" id="myDataTable" class="display viewreportcard">
  </br></br></br>
	<h1 align="center">Report Card Visibility</h1>
	</br>
	<h1 align="center"><?php echo $_smarty_tpl->tpl_vars['class']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['section']->value;?>
</h1>
  </br>
  <thead>
	<tr>
    <td align="left" class="table1"><b>GRNo.</b></td>
    <?php if ($_smarty_tpl->tpl_vars['s_userType']->value=='Administrator') {?>
    <td align="left" class="table1"><b>Class</b></td>
    <td align="left" class="table1"><b>Section</b></td>
    <?php }?>
    <td align="left" class="table1"><b>Roll No.</b></td>
		<td align="left" class="table1"><b>Name</b></td>
    <td class="table1" align='center'><b>Report Card Visibility Term-1</b>
			<input type="checkbox"  name="checkAll" id="checkAll" class="checkAll">
		</td>
    <td class="table1" align='center'><b>Report Card Visibility Term-2</b>
			<input type="checkbox"  name="checkAll2" id="checkAll2" class="checkAll2">
		</td>
  </tr>
  </thead>
  <tbody>
  <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['name'] = "sec";
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['dtlArr']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total']);
?>
  <tr class="gradeRow">
	<td align="left" class="table2">
    	<?php echo $_smarty_tpl->tpl_vars['dtlArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['grNo'];?>

    	<input type="hidden" name="nominalRollId[]" value="<?php echo $_smarty_tpl->tpl_vars['dtlArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['nominalRollId'];?>
">
    </td>
    <?php if ($_smarty_tpl->tpl_vars['s_userType']->value=='Administrator') {?>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['dtlArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['class'];?>
</td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['dtlArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['section'];?>
</td>
    <?php }?>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['dtlArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['rollNo'];?>
</td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['dtlArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['studentName'];?>
</td>
    <td align="center" class="table2"><input type="checkbox" class="checkboxAll" name="reportCardView1[]" <?php if ($_smarty_tpl->tpl_vars['dtlArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['reportCardView1']=='Y') {?>checked=checked<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['dtlArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['nominalRollId'];?>
"></td>
    
    <td align="center" class="table2"><input type="checkbox" class="checkboxAll2" name="reportCardView2[]" <?php if ($_smarty_tpl->tpl_vars['dtlArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['reportCardView2']=='Y') {?>checked=checked<?php }?> value="<?php echo $_smarty_tpl->tpl_vars['dtlArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['nominalRollId'];?>
"></td>
  </tr>
  <?php endfor; endif; ?>
  </tbody>
  <tfoot>
  <tr>
   <?php if ($_smarty_tpl->tpl_vars['s_userType']->value=='Administrator') {?>
    <td align="center" colspan="7" class="table2"><input type="submit" name="submitTaken" class="newSubmitBtn" value="Save"></td>
   <?php } else { ?> 
   	<td align="center" colspan="5" class="table2"><input type="submit" name="submitTaken" class="newSubmitBtn" value="Save"></td>
   <?php }?>
  </tr>
  </tfoot>
</table>
</form>
<?php }?>
<?php }} ?>

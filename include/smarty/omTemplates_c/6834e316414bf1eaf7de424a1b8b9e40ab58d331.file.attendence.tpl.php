<?php /* Smarty version Smarty-3.1.16, created on 2016-05-11 16:17:08
         compiled from ".\templates\attendence.tpl" */ ?>
<?php /*%%SmartyHeaderCode:10628570cdb87d9e7b8-67252009%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6834e316414bf1eaf7de424a1b8b9e40ab58d331' => 
    array (
      0 => '.\\templates\\attendence.tpl',
      1 => 1462870598,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '10628570cdb87d9e7b8-67252009',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_570cdb87e4a5d7_77062639',
  'variables' => 
  array (
    's_userType' => 0,
    'cArray' => 0,
    'class' => 0,
    'secArrOut' => 0,
    'section' => 0,
    'goDate' => 0,
    'weekDay' => 0,
    'count' => 0,
    'stdArray' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_570cdb87e4a5d7_77062639')) {function content_570cdb87e4a5d7_77062639($_smarty_tpl) {?><?php if (!is_callable('smarty_function_html_options')) include 'D:\\xampp\\htdocs\\dps\\include\\smarty\\libs\\plugins\\function.html_options.php';
if (!is_callable('smarty_function_html_select_date')) include 'D:\\xampp\\htdocs\\dps\\include\\smarty\\libs\\plugins\\function.html_select_date.php';
?><?php echo $_smarty_tpl->getSubTemplate ("./main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<link rel="stylesheet" href="./css/buttonStyle.css" type="text/css" />
<script type="text/javascript">
$(document).ready(function(){
	$('.presentButton2').hide();
	$('.presentButtonYes2').hide();
	$('.absentButtonYes2').hide();
	$('.absentButton2').hide();
	
	$(".omAttend").change(function()
  {
  	$('.newGoBtn').click();
  });
});

function preAbs(objPa)
{
	var row = $(objPa).parents('.trRow');
	var preAbsButton = row.find(objPa).val();
	if(preAbsButton == 'P')
	{
	  row.find('.presentButton').hide();
	  row.find('.presentButtonYes').show();
	  row.find('.absentButtonYes').hide();
	  row.find('.absentButton').show();
	}
	else
  {
	  row.find('.presentButton').show();
	  row.find('.presentButtonYes').hide();
	  row.find('.absentButtonYes').show();
	  row.find('.absentButton').hide();
  }
}

function buttonHide()
{
	$('#hideMe').hide();
}
function attend(obj)
{
	var row = $(obj).parents('.trRow');
	var grNo         = row.find('.grNo').val();
	var dayExtra    = $('#goDateDay').val();
  var monthExtra  = $('#goDateMonth').val();
  var yearExtra   = $('#goDateYear').val();
	var attendDate  = yearExtra+'-'+monthExtra+'-'+dayExtra;
	var attendButton = row.find(obj).val();
	
	var dataString = "grNo=" + grNo + "&attendDate=" + attendDate + "&attendButton=" + attendButton;
	
	$.ajax({
	  type: "GET",
	  url: "dailyAttend.php",
	  data: dataString,
  });
}

function confirmDelete()
{
	if(confirm('Are You Sure 1 st time?'))
	{
		if(confirm('Are You Sure 2nd time?'))
		  return true;
		else
		  return false;
	}
	else
	  return false;
}
</script>


</br></br>
<center>
<div class="hd"><h2 align="center">Take Attendance</h2></div>
<form name="formGet" method="GET" action="attendence.php">
<table align="center" border="1">
	<tr>
		<?php if ($_smarty_tpl->tpl_vars['s_userType']->value=='Administrator') {?>
		<td class="table2 form01 omAttend">
		  <select name="class" autofocus="autofocus">
		    <option value="0">Select class</option>
		    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['cArray']->value['className'],'output'=>$_smarty_tpl->tpl_vars['cArray']->value['className'],'selected'=>$_smarty_tpl->tpl_vars['class']->value),$_smarty_tpl);?>

		  </select>
	  </td>
    <td class="table2 form01 omAttend">
		  <select name="section">
		    <option value="0">Select Section</option>
		    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['secArrOut']->value,'output'=>$_smarty_tpl->tpl_vars['secArrOut']->value,'selected'=>$_smarty_tpl->tpl_vars['section']->value),$_smarty_tpl);?>

		  </select>
	  </td>
	  <?php }?>
		<td>
      <?php echo smarty_function_html_select_date(array('day_extra'=>"id=\"goDateDay\"",'month_extra'=>"id=\"goDateMonth\"",'year_extra'=>"id=\"goDateYear\"",'prefix'=>"goDate",'start_year'=>"-5",'end_year'=>"+2",'field_order'=>"DMY",'time'=>$_smarty_tpl->tpl_vars['goDate']->value,'day_value_format'=>"%02d"),$_smarty_tpl);?>

    </td>
    <td>
      <font size="3" color="Green"><b><?php echo $_smarty_tpl->tpl_vars['weekDay']->value;?>
</b></font>
    </td>
    <td>
      <input type="submit" name="go" class="newGoBtn" value="Go"> 
    </td>
  </tr>
</table>
<?php if ($_smarty_tpl->tpl_vars['count']->value==0&&$_smarty_tpl->tpl_vars['weekDay']->value!='Sunday') {?>
<table align="center" border="0">
  </br>
  <tr>
    <td>
	    <input type="submit" name="submit" onclick="buttonHide();" id="hideMe" class="newSubmitBtn" value="Generate">
	    <a href="reGenerateAttendence.php"> (Re-generate Attendance) </a>
	  </td>
	</tr>
</table>
<?php }?>
</center>
<table align="center" border="1">
  </br>
	<h1 align="center">Student List - Class:<?php echo $_smarty_tpl->tpl_vars['class']->value;?>
-<?php echo $_smarty_tpl->tpl_vars['section']->value;?>
 </h1>
	
  </br>
	<tr>
		<td align="left" class="table1"><b>Sr.No.</b></td>
		<td align="left" class="table1"><b>GR. No.</b></td>
		<td align="left" class="table1"><b>Roll No.</b></td>
		<td align="left" class="table1"><b>Name</b></td>
		<!-- td align="left" class="table1"><b>M/F</b></td -->
		<td align="left" class="table1"><b>Present</b></td>
		<td align="left" class="table1"><b>Absent</b></td>
		<td align="left" class="table1">
		  <a href="allAttendDelete.php?class=<?php echo $_smarty_tpl->tpl_vars['class']->value;?>
&section=<?php echo $_smarty_tpl->tpl_vars['section']->value;?>
&goDate=<?php echo $_smarty_tpl->tpl_vars['goDate']->value;?>
" onclick="return confirmDelete();">Delete All</a>
		</td>
  </tr>
  <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['name'] = "sec";
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['stdArray']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total']);
?>
  <tr class="trRow">
    <td align="center" class="table2"><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['sec']['rownum'];?>
</td>
    <td align="center" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['grNo'];?>
 <input type="hidden" class="grNo" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['grNo'];?>
"></td>
    <td align="center" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['rollNo'];?>
 <input type="hidden" class="grNo" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['grNo'];?>
"></td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['studentName'];?>
</td>
    <!-- td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['gender'];?>
</td -->
    <td align="center" class="table2">
    	<?php if ($_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences']=='P') {?>
    	  <input type="button" value="P" class="presentButtonYes" />
    	  <input type="button" class="presentButton presentButton2" value="P" onclick="attend(this); preAbs(this);">
    	<?php } else { ?>
    	  <input type="button" class="presentButton" value="P" onclick="attend(this); preAbs(this);">
    	  <input type="button" value="P" class="presentButtonYes presentButtonYes2" />
    	<?php }?>
    </td>
    <td align="center" class="table2">
    	<?php if ($_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences']=='A') {?>
    	  <input type="button" value="A" class="absentButtonYes" />
    	  <input type="button" class="absentButton absentButton2" value="A" onclick="attend(this); preAbs(this);">
    	<?php } else { ?>
    	  <input type="button" class="absentButton" value="A" onclick="attend(this); preAbs(this);">
    	  <input type="button" value="A" class="absentButtonYes absentButtonYes2" />
    	<?php }?>
    </td>
    <td align="left" class="table2">
    	<a href="attendDelete.php?attendenceId=<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendenceId'];?>
&goDate=<?php echo $_smarty_tpl->tpl_vars['goDate']->value;?>
" onclick="return confirmDelete();">Delete</a>
    </td>
  </tr>
  <?php endfor; else: ?>
  <tr>
    <td align="center" colspan="7" class="table2"><h2>Not Taken</h2></td>
  </tr>
  <?php endif; ?>
  </tr>
  <?php if ($_smarty_tpl->tpl_vars['count']->value!=0) {?>
  <tr>
    <td align="center" colspan="7"><input type="submit" name="submitTaken" class="newSubmitBtn" value="Save"></td>
  </tr>
  <?php }?>
</table>
</form>
<br><br><br>
<?php }} ?>

<?php /* Smarty version Smarty-3.1.16, created on 2016-04-11 17:31:02
         compiled from ".\templates\classTeacherAllotment.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17978570b91fee4c208-30548190%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3f23cf11651927b74fabf1075da85e98454c4074' => 
    array (
      0 => '.\\templates\\classTeacherAllotment.tpl',
      1 => 1458044849,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17978570b91fee4c208-30548190',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'classTeacherAllotmentId' => 0,
    'confirm' => 0,
    'empArray' => 0,
    'employeeMasterId' => 0,
    'dateArrVal' => 0,
    'dateArrOut' => 0,
    'academicStartYear' => 0,
    'cArray' => 0,
    'class' => 0,
    'secArrOut' => 0,
    'section' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_570b91fee92717_28526797',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_570b91fee92717_28526797')) {function content_570b91fee92717_28526797($_smarty_tpl) {?><?php if (!is_callable('smarty_function_html_options')) include 'F:\\xampp\\htdocs\\dps\\include\\smarty\\libs\\plugins\\function.html_options.php';
?><?php echo $_smarty_tpl->getSubTemplate ("./main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
		"bAutoWidth": false,
    "aLengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 10,
		"bJQueryUI":true
  });
});
</script>


<form name="form1" method="POST" action="classTeacherAllotment.php">
<table align="center" border="0" class="table2">
<div class="hd"><h2 align="center">Class Teacher Allotment</h2>
<input type="hidden" name="classTeacherAllotmentId" value="<?php echo $_smarty_tpl->tpl_vars['classTeacherAllotmentId']->value;?>
">
<tr>
  <td align="center" colspan="2" style="color:red; font-size:18px;"><b><?php echo $_smarty_tpl->tpl_vars['confirm']->value;?>
</b></td>
</tr>
<tr>
  <td class="table2">Name</td>
  <td class="table2 form01">
	  <select name="employeeMasterId" autofocus="autofocus" required >
	    <option value="">Select Teacher</option>
	    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['empArray']->value['employeeMasterId'],'output'=>$_smarty_tpl->tpl_vars['empArray']->value['name'],'selected'=>$_smarty_tpl->tpl_vars['employeeMasterId']->value),$_smarty_tpl);?>

	  </select>
  </td>
</tr>
<tr>
  <td class="table2">Acadamic Year</td>
  <td class="table2 form01">
    <select name="startYear" id="startDateYear">
      <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['dateArrVal']->value,'output'=>$_smarty_tpl->tpl_vars['dateArrOut']->value,'selected'=>$_smarty_tpl->tpl_vars['academicStartYear']->value),$_smarty_tpl);?>

    </select>
  </td>
</tr>
<tr>
  <td class="table2">Class</td>
  <td class="table2 form01">
  	<select name="class" autofocus="autofocus" required >
	    <option value="">Select class</option>
	    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['cArray']->value['className'],'output'=>$_smarty_tpl->tpl_vars['cArray']->value['className'],'selected'=>$_smarty_tpl->tpl_vars['class']->value),$_smarty_tpl);?>

	  </select>
  </td>
</tr>
<tr>
  <td class="table2">Section</td>
  <td class="table2 form01">
  	<select name="section">
      <option value="0">Select Section</option>
      <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['secArrOut']->value,'output'=>$_smarty_tpl->tpl_vars['secArrOut']->value,'selected'=>$_smarty_tpl->tpl_vars['section']->value),$_smarty_tpl);?>

	  </select>
	</td>
</tr>
<tr>
  <td class="table2"></td>
  <td class="table2 form01" align="center"><input type="submit" name="Submit" class="button" value="Save"></td>
</tr>
</table>
</form>
<?php }} ?>

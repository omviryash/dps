<?php /* Smarty version Smarty-3.1.16, created on 2016-12-29 10:57:11
         compiled from "./templates/vehicalMasterList.tpl" */ ?>
<?php /*%%SmartyHeaderCode:189636029058649eafc6af13-32417652%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7123fcca450bf219aa684e790b248501314f44d8' => 
    array (
      0 => './templates/vehicalMasterList.tpl',
      1 => 1482817430,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '189636029058649eafc6af13-32417652',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'classArray' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_58649eafcf2b31_27736991',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58649eafcf2b31_27736991')) {function content_58649eafcf2b31_27736991($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("./main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
		"bAutoWidth": false,
    "aLengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 10,
  	"aaSorting": [[ 2, "asc" ]],
		"bJQueryUI":true
  });
});
</script>


<div class="hd"><h2 align="center">Vehicle Master List</h2></div>
<table align="center" border="2" id="myDataTable" class="display">
	<thead>
	<tr>
		<td align="center" class="table1"><b>Edit</b></td>
		<td align="center" class="table1"><b>Vehicle Type</b></td>
		<td align="center" class="table1"><b>Vehicle No</b></td>
		<td align="center" class="table1"><b>Registration No</b></td>
		<td align="center" class="table1"><b>Capicity</b></td>
  </tr>
  </thead>
  <tbody>
  <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['name'] = "sec";
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['classArray']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total']);
?>
  <tr>
    <td align="left" class="table2"><a href='vehicalMaster.php?vehicleMasterId=<?php echo $_smarty_tpl->tpl_vars['classArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['vehicleMasterId'];?>
'>Edit</a></td>
    <td align="left" class="table2" NOWRAP ><?php echo $_smarty_tpl->tpl_vars['classArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['vehicleType'];?>
</td>
    <td align="left" class="table2" NOWRAP ><?php echo $_smarty_tpl->tpl_vars['classArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['vehicleNo'];?>
</td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['classArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['registrationNo'];?>
</td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['classArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['capicity'];?>
</td>
  </tr>
  <?php endfor; endif; ?>
  </tbody>
</table>
<?php }} ?>

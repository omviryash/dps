<?php /* Smarty version Smarty-3.1.16, created on 2016-12-29 12:01:32
         compiled from "./templates/feePendingList.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4745115315864adc4880d55-73586790%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '78a85e27f2621d33e3eafa4a1faa546dc6922e11' => 
    array (
      0 => './templates/feePendingList.tpl',
      1 => 1482817430,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4745115315864adc4880d55-73586790',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'cArray' => 0,
    'class' => 0,
    'secArrOut' => 0,
    'section' => 0,
    'dateArrVal' => 0,
    'dateArrOut' => 0,
    'academicStartYearSelected' => 0,
    'stdArray' => 0,
    'grNo' => 0,
    'attendenceArr' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5864adc48f85f0_79523523',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5864adc48f85f0_79523523')) {function content_5864adc48f85f0_79523523($_smarty_tpl) {?><?php if (!is_callable('smarty_function_html_options')) include '/opt/lampp/htdocs/dps/include/smarty/libs/plugins/function.html_options.php';
?><?php echo $_smarty_tpl->getSubTemplate ("./main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script src="./media1/js/jquery-1.4.4.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.jeditable.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[-1, 10, 20, 30, 40, 50], ["All", 10, 20, 30, 40, 50]],
  	"iDisplayLength": 500,
  	"aaSorting": [[2, 'desc']],
		"bJQueryUI":true
  });
  $(".omAttend").change(function()
  {
  	$('.newGoBtnClick').click();
  });
  
  $("#startDateYear").change(function()
  {
    var yearExtra  = $('#startDateYear').val();
		var endDate    = yearExtra;
		var datastring = 'endDate=' + endDate;
		$.ajax({	
			type: 'GET',
			url: 'endYear.php',
		  data: datastring,
		  success:function(data)
		  {
		  	$('#endDateYear').val(data);
	    }
	  });
  });
  setStudName();
});

function setStudName()
{
	var grNoNew = $("#grNoNew").val() != '' ? $("#grNoNew").val() : 0;
	var dpsClass = $("#dpsClass").val() != '' ? $("#dpsClass").val() : 0;
	var classSection = $("#classSection").val() != '' ? $("#classSection").val() : '';
	var startYear = $("#startYear").val() != '' ? $("#startYear").val() : '';
	var dataString = "class=" + dpsClass + "&classSection=" + classSection + "&startYear=" + startYear + "&grNoNew=" + grNoNew;
	$.ajax({
		type : 'GET',
		url  : 'setStudNamePendingFee.php',
		data :  dataString,
		success:function(data)
		{
			$('#stdNameBest').html(data);
	  }
	});
}
</script>


</br></br>
<link rel="stylesheet" href="./css/buttonStyle.css" type="text/css" />
<form name="formGet" method="GET" action="feePendingList.php">
<table align="center">
	<tr>
		<td class="table2 form01">
		  <select name="class" autofocus="autofocus" class='omAttend' id="dpsClass">
		    <option value="">Select Class</option>
		    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['cArray']->value['className'],'output'=>$_smarty_tpl->tpl_vars['cArray']->value['className'],'selected'=>$_smarty_tpl->tpl_vars['class']->value),$_smarty_tpl);?>

		  </select>
	  </td>
    <td class="table2 form01">
		  <select name="classSection" id="classSection" onChange="setStudName();">
		    <option value="0">Select Section</option>
		    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['secArrOut']->value,'output'=>$_smarty_tpl->tpl_vars['secArrOut']->value,'selected'=>$_smarty_tpl->tpl_vars['section']->value),$_smarty_tpl);?>

		  </select>
	  </td>
	  <td class="table2 form01">
		  <select name="startYear" onChange="setStudName();" id="startYear">
		    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['dateArrVal']->value,'output'=>$_smarty_tpl->tpl_vars['dateArrOut']->value,'selected'=>$_smarty_tpl->tpl_vars['academicStartYearSelected']->value),$_smarty_tpl);?>

		  </select>
		</td>
	  <td class="table2 form01" id='stdNameBest'>
	  	<select name="grNo">
				<option value='0'>Select</option>
				<?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['stdArray']->value['grNo'],'output'=>$_smarty_tpl->tpl_vars['stdArray']->value['studentName'],'selected'=>$_smarty_tpl->tpl_vars['grNo']->value),$_smarty_tpl);?>

			</select>
			<input type="" id="grNoNew" value="<?php echo $_smarty_tpl->tpl_vars['grNo']->value;?>
">
		</td>
    <td>
      <input type="submit" name="submit" class="newGoBtn newGoBtnClick" value="Go">
    </td>
  </tr>
</table>
</form>
<form name="form2" method="POST" action="feePendingList.php">
<table align="center" border="1" id="myDataTable" class="display">  
  </br>
	<div class="hd"><h2 align="center">Fee Pending Report</h2></div>
	</br>
	<thead>
	<tr>
		<td align="left" class="table1"><b>Fee Type</b></td>
		<td align="left" class="table1"><b>Amount</b></td>
		<td align="left" class="table1"><b>Amount Collection</b></td>
		<td align="left" class="table1"><b>Status</b></td>
  </tr>
  </thead>
  <tbody>
  <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['name'] = "sec";
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['attendenceArr']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total']);
?>
  <tr class="gradeRow">
  	<td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['feeType'];?>
</td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['amount'];?>
</td>
    <?php if ($_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['amountTotal']>0) {?>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['amountTotal'];?>
</td>
    <?php } else { ?>
    <td align="left" class="table2">0</td>
    <?php }?>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['status'];?>
</td>
  </tr>
  <?php endfor; endif; ?>
  </tbody>
</table>
</form>
<?php }} ?>

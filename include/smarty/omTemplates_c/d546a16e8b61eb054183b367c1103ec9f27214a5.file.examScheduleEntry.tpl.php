<?php /* Smarty version Smarty-3.1.16, created on 2016-04-12 16:56:56
         compiled from ".\templates\examScheduleEntry.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7702570cdb80e35454-90054034%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd546a16e8b61eb054183b367c1103ec9f27214a5' => 
    array (
      0 => '.\\templates\\examScheduleEntry.tpl',
      1 => 1458044849,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7702570cdb80e35454-90054034',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'examScheduleId' => 0,
    'confirm' => 0,
    'exArray' => 0,
    'examTypeId' => 0,
    'scheduleDate' => 0,
    'maxMarks' => 0,
    'minMarks' => 0,
    'clubArr' => 0,
    'subjectMasterId' => 0,
    'cArray' => 0,
    'class' => 0,
    'yesNoArrOut' => 0,
    'studentView' => 0,
    'classArray' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_570cdb810cbb35_69087860',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_570cdb810cbb35_69087860')) {function content_570cdb810cbb35_69087860($_smarty_tpl) {?><?php if (!is_callable('smarty_function_html_options')) include 'F:\\xampp\\htdocs\\dps\\include\\smarty\\libs\\plugins\\function.html_options.php';
if (!is_callable('smarty_function_html_select_date')) include 'F:\\xampp\\htdocs\\dps\\include\\smarty\\libs\\plugins\\function.html_select_date.php';
?><?php echo $_smarty_tpl->getSubTemplate ("./main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<form name="form1" method="POST" action="examScheduleEntry.php">
<table align="center" border="0" class="table2">
<div class="hd"><h2 align="center">Exam Schedule Entry</h2>
<input type="hidden" name="examScheduleId" value="<?php echo $_smarty_tpl->tpl_vars['examScheduleId']->value;?>
">
<tr>
  <td align="center" colspan="2" style="color:red; font-size:18px;"><b><?php echo $_smarty_tpl->tpl_vars['confirm']->value;?>
</b></td>
</tr>
<tr>
  <td class="table2">Exam Type</td>
  <td class="table2 form01">
	  <select name="examTypeId" autofocus="autofocus" required autofocus="autofocus">
	    <option value="">Select Exam Type</option>
	    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['exArray']->value['examTypeId'],'output'=>$_smarty_tpl->tpl_vars['exArray']->value['examType'],'selected'=>$_smarty_tpl->tpl_vars['examTypeId']->value),$_smarty_tpl);?>

	  </select>
  </td>
</tr>
<tr>
  <td class="table2">Schedule Date</td>
  <td class="table2 form01">
    <?php echo smarty_function_html_select_date(array('prefix'=>"scheduleDate",'start_year'=>"-25",'end_year'=>"+25",'field_order'=>"DMY",'time'=>$_smarty_tpl->tpl_vars['scheduleDate']->value,'day_value_format'=>"%02d"),$_smarty_tpl);?>

  </td>
</tr>
<tr>
  <td class="table2">Max Marks</td>
  <td class="table2 form01">
  	<input type="text" name="maxMarks" value="<?php echo $_smarty_tpl->tpl_vars['maxMarks']->value;?>
">
	</td>
</tr>
<tr>
  <td class="table2">Min Marks</td>
  <td class="table2 form01">
  	<input type="text" name="minMarks" value="<?php echo $_smarty_tpl->tpl_vars['minMarks']->value;?>
">
	</td>
</tr>
<tr>
  <td class="table2">Subject</td>
  <td class="table2 form01">
  	<select name="subjectMasterId" required >
      <option value="">Select Subject</option>
      <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['clubArr']->value['subjectMasterId'],'output'=>$_smarty_tpl->tpl_vars['clubArr']->value['subjectName'],'selected'=>$_smarty_tpl->tpl_vars['subjectMasterId']->value),$_smarty_tpl);?>

	  </select>
	</td>
</tr>
<tr>
  <td class="table2">Class</td>
  <td class="table2 form01">
  	<select name="class" required >
	    <option value="">Select class</option>
	    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['cArray']->value['className'],'output'=>$_smarty_tpl->tpl_vars['cArray']->value['className'],'selected'=>$_smarty_tpl->tpl_vars['class']->value),$_smarty_tpl);?>

	  </select>
  </td>
</tr>
<tr>
  <td class="table2">Student View</td>
  <td class="table2 form01">
  	<select name="studentView">
      <option value="">Select View</option>
      <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['yesNoArrOut']->value,'output'=>$_smarty_tpl->tpl_vars['yesNoArrOut']->value,'selected'=>$_smarty_tpl->tpl_vars['studentView']->value),$_smarty_tpl);?>

	  </select>
	</td>
</tr>
<tr>
  <td class="table2"></td>
  <td class="table2 form01" align="center"><input type="submit" name="Submit" class="button" value="Save"></td>
</tr>
</table>
</form>
<!--table align="center" border="2">
	<h2 align="center">Class List</h2>
	<tr>
		<td align="center" class="table1"><b>&nbsp;</b></td>
		<td align="center" class="table1"><b>Exam Type</b></td>
		<td align="center" class="table1"><b>Schedule Date</b></td>
		<td align="center" class="table1"><b>Max Marks</b></td>
		<td align="center" class="table1"><b>Min Marks</b></td>
		<td align="center" class="table1"><b>Class</b></td>
		<td align="center" class="table1"><b>Subject</b></td>
		<td align="center" class="table1"><b>Student View</b></td>
  </tr>
  <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['name'] = "sec";
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['classArray']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total']);
?>
  <tr>
    <td align="left" class="table2"><a href="examScheduleEntry.php?examScheduleId=<?php echo $_smarty_tpl->tpl_vars['classArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['examScheduleId'];?>
">Edit</a></td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['classArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['examType'];?>
</td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['classArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['scheduleDate'];?>
</td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['classArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['maxMarks'];?>
</td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['classArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['minMarks'];?>
</td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['classArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['class'];?>
</td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['classArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['subjectName'];?>
</td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['classArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['studentView'];?>
</td>
  </tr>
  <?php endfor; endif; ?>
</table-->
<?php }} ?>

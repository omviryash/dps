<?php /* Smarty version Smarty-3.1.16, created on 2016-05-11 16:16:58
         compiled from ".\templates\attendenceReportMonthlyEmp.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1803257330da2bed103-71219686%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0ae4f536c9f460e35de2faf30860d17ca8430996' => 
    array (
      0 => '.\\templates\\attendenceReportMonthlyEmp.tpl',
      1 => 1450506623,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1803257330da2bed103-71219686',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'teachingNonteachingCmbVal' => 0,
    'teachingNonteachingCmbTxt' => 0,
    'teachingNonteaching' => 0,
    'attendenceDate' => 0,
    'totalNumberWorkingDay' => 0,
    'attendenceArr' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_57330da2e0ff27_11798022',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57330da2e0ff27_11798022')) {function content_57330da2e0ff27_11798022($_smarty_tpl) {?><?php if (!is_callable('smarty_function_html_options')) include 'D:\\xampp\\htdocs\\dps\\include\\smarty\\libs\\plugins\\function.html_options.php';
if (!is_callable('smarty_function_html_select_date')) include 'D:\\xampp\\htdocs\\dps\\include\\smarty\\libs\\plugins\\function.html_select_date.php';
?><?php echo $_smarty_tpl->getSubTemplate ("./main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[-1, 10, 20, 30, 40, 50], ["All", 10, 20, 30, 40, 50]],
  	"iDisplayLength": 500,
		"bJQueryUI":true
  });
  $(".omAttend").change(function()
  {
  	$('.newGoBtn').click();
  });
});
</script>


</br></br>
<form name="formGet" method="GET" action="attendenceReportMonthlyEmp.php">
<table align="center">
	<tr>
	<td>
		<select name="teachingNonteaching">
		    <option value="0">Select Section</option>
		    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['teachingNonteachingCmbVal']->value,'output'=>$_smarty_tpl->tpl_vars['teachingNonteachingCmbTxt']->value,'selected'=>$_smarty_tpl->tpl_vars['teachingNonteaching']->value),$_smarty_tpl);?>

		  </select>
	</td>
    <td>
      <?php echo smarty_function_html_select_date(array('prefix'=>"attendence",'start_year'=>"-50",'end_year'=>"+50",'field_order'=>"DMY",'time'=>$_smarty_tpl->tpl_vars['attendenceDate']->value,'day_value_format'=>"%02d",'display_days'=>false),$_smarty_tpl);?>

    </td>
    <td>
      <input type="submit" name="submit" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
<!--center>Total Number Working Day = <?php echo $_smarty_tpl->tpl_vars['totalNumberWorkingDay']->value;?>
</center-->
</form>
<table align="left" border="1" id="myDataTable" class="display">
	</br>
	<div class="hd"><h2 align="center">Staff Attendance Report</h2></div>
	<!--thead>
	<tr>
		<td align="left"><b>Roll No</b></td>
	  <td align="left"><b>Name</b></td>
	  <td align="left"><b>AP</b></td>
	  <td align="left"><b>Present</b></td>
	  <td align="left"><b>Absent</b></td>
  </tr>
  </thead-->
  <thead>
	<tr>
	<td align="left"><b>Sr.No.</b></td>
		<td align="left"><b>Code</b></td>
	  <td align="left"><b>Name</b></td>
	  
	  <td width="10">01</td><td width="10">02</td><td width="10">03</td>
	  <td width="10">04</td><td width="10">05</td><td width="10">06</td>
	  <td width="10">07</td><td width="10">08</td><td width="10">09</td>
	  <td width="10">10</td><td width="10">11</td><td width="10">12</td>
	  <td width="10">13</td><td width="10">14</td><td width="10">15</td>
	  <td width="10">16</td><td width="10">17</td><td width="10">18</td>
	  <td width="10">19</td><td width="10">20</td><td width="10">21</td>
	  <td width="10">22</td><td width="10">23</td><td width="10">24</td>
	  <td width="10">25</td><td width="10">26</td><td width="10">27</td>
	  <td width="10">28</td><td width="10">29</td><td width="10">30</td>
	  <td width="10">31</td>
	  <td align="left"><b>A</b></td>
  </tr>
  </thead>
  <tbody>
  <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['name'] = "sec";
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['attendenceArr']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total']);
?>
  <tr>
  	<td align="left"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['SrNo'];?>
</td>
    <td align="left"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['employeeCode'];?>
</td>
    <td align="left"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['name'];?>
</a></td>
		<td width="10"><a href='attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['attendenceDate']->value;?>
-01'><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences1'];?>
</a></td>
  	<td width="10"><a href='attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['attendenceDate']->value;?>
-02'><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences2'];?>
</a></td>
  	<td width="10"><a href='attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['attendenceDate']->value;?>
-03'><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences3'];?>
</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['attendenceDate']->value;?>
-04'><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences4'];?>
</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['attendenceDate']->value;?>
-05'><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences5'];?>
</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['attendenceDate']->value;?>
-06'><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences6'];?>
</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['attendenceDate']->value;?>
-07'><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences7'];?>
</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['attendenceDate']->value;?>
-08'><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences8'];?>
</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['attendenceDate']->value;?>
-09'><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences9'];?>
</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['attendenceDate']->value;?>
-10'><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences10'];?>
</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['attendenceDate']->value;?>
-11'><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences11'];?>
</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['attendenceDate']->value;?>
-12'><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences12'];?>
</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['attendenceDate']->value;?>
-13'><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences13'];?>
</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['attendenceDate']->value;?>
-14'><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences14'];?>
</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['attendenceDate']->value;?>
-15'><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences15'];?>
</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['attendenceDate']->value;?>
-16'><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences16'];?>
</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['attendenceDate']->value;?>
-17'><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences17'];?>
</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['attendenceDate']->value;?>
-18'><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences18'];?>
</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['attendenceDate']->value;?>
-19'><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences19'];?>
</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['attendenceDate']->value;?>
-20'><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences20'];?>
</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['attendenceDate']->value;?>
-21'><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences21'];?>
</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['attendenceDate']->value;?>
-22'><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences22'];?>
</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['attendenceDate']->value;?>
-23'><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences23'];?>
</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['attendenceDate']->value;?>
-24'><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences24'];?>
</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['attendenceDate']->value;?>
-25'><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences25'];?>
</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['attendenceDate']->value;?>
-26'><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences26'];?>
</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['attendenceDate']->value;?>
-27'><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences27'];?>
</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['attendenceDate']->value;?>
-28'><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences28'];?>
</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['attendenceDate']->value;?>
-29'><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences29'];?>
</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['attendenceDate']->value;?>
-30'><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences30'];?>
</a></td>
	  <td width="10"><a href='attendEdit.php?attendDate=<?php echo $_smarty_tpl->tpl_vars['attendenceDate']->value;?>
-31'><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences31'];?>
</a></td>
    <td align="left"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['countA'];?>
</td>
  </tr>
 <?php endfor; endif; ?>
 </tbody>
 <tfoot>
   <tr>
   	<th></th>
   	<th></th>
   	<th></th>
   	<th></th>
   </tr>
 </tfoot>
</table>
<?php }} ?>

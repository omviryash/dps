<?php /* Smarty version Smarty-3.1.16, created on 2016-12-29 12:02:06
         compiled from "./templates/feeStructureList.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4114417515864ade6dd66a6-54915803%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3e568a33c0f9089a2970dc8a2628c7701cacae9c' => 
    array (
      0 => './templates/feeStructureList.tpl',
      1 => 1482817430,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4114417515864ade6dd66a6-54915803',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'dateArrVal' => 0,
    'dateArrOut' => 0,
    'academicStartYear' => 0,
    'feeStructureArray' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5864ade6e1d441_44212672',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5864ade6e1d441_44212672')) {function content_5864ade6e1d441_44212672($_smarty_tpl) {?><?php if (!is_callable('smarty_function_html_options')) include '/opt/lampp/htdocs/dps/include/smarty/libs/plugins/function.html_options.php';
?><?php echo $_smarty_tpl->getSubTemplate ("./main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<br>
<br>
<br>

<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 10,
		"bJQueryUI":true
  });
  
});
</script>


</br></br>
<form name="form1" method="POST" action="feeStructureList.php">
<table align="center">
	<tr>
		<td class="table2 form01">
      <select name="startYear" id="startDateYear">
        <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['dateArrVal']->value,'output'=>$_smarty_tpl->tpl_vars['dateArrOut']->value,'selected'=>$_smarty_tpl->tpl_vars['academicStartYear']->value),$_smarty_tpl);?>

      </select>
	  </td>
	  <td>
      <input type="submit" name="submit" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
</form>
<table align="left" border="1" id="myDataTable" class="display">
	<div class="hd"><h2 align="center">Fee Structure List</h2>
	<thead>
	<tr>
		<td align="center"><b>Edit</b></td>
		<td align="center"><b>Academic Year</b></td>
		<td align="center"><b>Fee Type</b></td>
		<td align="center"><b>Amount</b></td>
		<td align="center"><b>Remarks</b></td>
  </tr>
  </thead>
  <tbody>
  <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['name'] = "sec";
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['feeStructureArray']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total']);
?>
  <tr>
  	<td><a href="feeStructure.php?feeStructureId=<?php echo $_smarty_tpl->tpl_vars['feeStructureArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['feeStructureId'];?>
">Edit</a></td>
    <td align="left"><?php echo $_smarty_tpl->tpl_vars['feeStructureArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['academicStartYear'];?>
 - <?php echo $_smarty_tpl->tpl_vars['feeStructureArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['academicEndYear'];?>
</td>
    <td align="left"><?php echo $_smarty_tpl->tpl_vars['feeStructureArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['feeType'];?>
</td>
    <td align="left"><?php echo $_smarty_tpl->tpl_vars['feeStructureArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['amount'];?>
</td>
    <td align="left"><?php echo $_smarty_tpl->tpl_vars['feeStructureArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['remarks'];?>
</td>
  </tr>
  <?php endfor; endif; ?>
  </tbody>
</table>
<?php }} ?>

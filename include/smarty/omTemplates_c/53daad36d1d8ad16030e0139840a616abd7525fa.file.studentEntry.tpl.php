<?php /* Smarty version Smarty-3.1.16, created on 2016-12-29 11:34:21
         compiled from "./templates/studentEntry.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1327966252586236f064acf1-17313456%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '53daad36d1d8ad16030e0139840a616abd7525fa' => 
    array (
      0 => './templates/studentEntry.tpl',
      1 => 1482991395,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1327966252586236f064acf1-17313456',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_586236f076a391_27704493',
  'variables' => 
  array (
    'studentMasterId' => 0,
    'confirm' => 0,
    'studentImage' => 0,
    'fatherImage' => 0,
    'motherImage' => 0,
    'dateArrVal' => 0,
    'dateArrOut' => 0,
    'academicStartYearselected' => 0,
    'studentName' => 0,
    'fatherName' => 0,
    'grNo' => 0,
    'dateOfBirth' => 0,
    'cArray' => 0,
    'joinedInClass' => 0,
    'joiningDate' => 0,
    'studentLoginId' => 0,
    'studentPassword' => 0,
    'parentLoginId' => 0,
    'parentPassword' => 0,
    'maleArrValue' => 0,
    'gender' => 0,
    'currentAddress' => 0,
    'residencePhone1' => 0,
    'residencePhone2' => 0,
    'smsMobile' => 0,
    'studentEmail' => 0,
    'permanentAddress' => 0,
    'previousSchool' => 0,
    'urban' => 0,
    'religion' => 0,
    'bloodGroup' => 0,
    'bloodGroups' => 0,
    'houseArray' => 0,
    'houseId' => 0,
    'currentState' => 0,
    'currentAddressPin' => 0,
    'permanentState' => 0,
    'permanentPIN' => 0,
    'createdOn' => 0,
    'fatherMobile' => 0,
    'fatherEmail' => 0,
    'fatherOccupation' => 0,
    'fathersDesignation' => 0,
    'fathersOrganization' => 0,
    'mothersName' => 0,
    'mothersPhone' => 0,
    'mothersEmailid' => 0,
    'mothersMobile' => 0,
    'motherOccupation' => 0,
    'mothersDesignation' => 0,
    'mothersOrganization' => 0,
    'fatherPhone' => 0,
    'aadhaar' => 0,
    'secArrValue' => 0,
    'secArrOut' => 0,
    'activated' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_586236f076a391_27704493')) {function content_586236f076a391_27704493($_smarty_tpl) {?><?php if (!is_callable('smarty_function_html_options')) include '/opt/lampp/htdocs/dps/include/smarty/libs/plugins/function.html_options.php';
if (!is_callable('smarty_function_html_select_date')) include '/opt/lampp/htdocs/dps/include/smarty/libs/plugins/function.html_select_date.php';
?><?php echo $_smarty_tpl->getSubTemplate ("./main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<script type=text/javascript>
function checkGrno()
{
	var studentMasterId = $("#studentMasterId").val();
	var dataString = "grNo=" +$(".grNo").val() + "&studentMasterId=" + studentMasterId;
	$.ajax({
		type : 'GET',
		url  : 'checkGrno.php',
		data :  dataString,
		success:function(data)
		{
			if(data == 1)
			{
				alert("This Grno Allready Entered");
				$('.grNo').val('');
				$('.grNo').focus();
			}
	  }
	});
}
</script>


<form action="studentEntry.php" enctype="multipart/form-data" method="POST">
<table align="center" border="1">
<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['studentMasterId']->value;?>
" name="studentMasterId" id="studentMasterId" >
<br><br><br>
<?php if ($_smarty_tpl->tpl_vars['studentMasterId']->value>0) {?>
<div class="hd"><h2 align="center">Admission Edit</h2></div>
<?php } else { ?>
<div class="hd"><h2 align="center">New Admission Entry</h2></div>
<?php }?>
<br><br>
<tr>
  <td align="center" colspan="4" style="color:red; font-size:18px;"><b><?php echo $_smarty_tpl->tpl_vars['confirm']->value;?>
</b></td>
</tr>
<tr>
	<td align="left" class="table1">Image P F M : </td>
	<td class="table2 form01"><?php echo $_smarty_tpl->tpl_vars['studentImage']->value;?>
<input name="imagePupil" type="file" id="files"></td>
	<td class="table2 form01"><?php echo $_smarty_tpl->tpl_vars['fatherImage']->value;?>
<input name="imageFather" type="file" id="files"></td>
	<td class="table2 form01"><?php echo $_smarty_tpl->tpl_vars['motherImage']->value;?>
<input name="imageMother" type="file" id="files"></td>
</tr>
<?php if ($_smarty_tpl->tpl_vars['studentMasterId']->value==0) {?>
<tr>
	<td align="left" class="table1">Academic Year : </td>
	<td align="center" class="table2 form01" colspan="3">
		<select name="startYear">
	    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['dateArrVal']->value,'output'=>$_smarty_tpl->tpl_vars['dateArrOut']->value,'selected'=>$_smarty_tpl->tpl_vars['academicStartYearselected']->value),$_smarty_tpl);?>

	  </select>
	</td>
</tr>
<?php }?>
<tr>
	<td align="left" class="table1">Name : </td>
	<td class="table2 form01"><input type="text" width='255' name="studentName" autofocus required value="<?php echo $_smarty_tpl->tpl_vars['studentName']->value;?>
" /></td>
</tr>
<tr>	
	<td align="left" class="table1">Father Name : </td>
	<td class="table2 form01"><input type="text" name="fatherName" value="<?php echo $_smarty_tpl->tpl_vars['fatherName']->value;?>
"></td>
</tr>
<tr>
	<td align="left" class="table1">G.R.No : </td>
	<td class="table2 form01"><input type="text" name="grNo" class="grNo" onblur="checkGrno();" value="<?php echo $_smarty_tpl->tpl_vars['grNo']->value;?>
" required /></td>
</tr>
<tr>
	<td align="left" class="table1">Date Of Birth : </td>
	<td class="table2 form01">
		<?php echo smarty_function_html_select_date(array('prefix'=>'dob','field_order'=>"DmY",'month_format'=>"%m",'start_year'=>'-25','end_year'=>'+25','time'=>$_smarty_tpl->tpl_vars['dateOfBirth']->value,'day_value_format'=>"%02d"),$_smarty_tpl);?>

	</td>
</tr>
<tr>
	<td class="table1">Joined In Class : </td>
	<td class="table2 form01">
  	<select name="joinedInClass" autofocus="autofocus" required >
	    <option value="">Select class</option>
	    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['cArray']->value['className'],'output'=>$_smarty_tpl->tpl_vars['cArray']->value['className'],'selected'=>$_smarty_tpl->tpl_vars['joinedInClass']->value),$_smarty_tpl);?>

	  </select>
  </td>
</tr>
<tr>
  <td align="left" class="table1">Joining Date : </td>
	<td class="table2 form01">
		<?php echo smarty_function_html_select_date(array('prefix'=>'joining','field_order'=>"DmY",'month_format'=>"%m",'start_year'=>'-25','end_year'=>'+25','time'=>$_smarty_tpl->tpl_vars['joiningDate']->value,'day_value_format'=>"%02d"),$_smarty_tpl);?>

	</td>
</tr>
<?php if ($_smarty_tpl->tpl_vars['studentMasterId']->value>0) {?>
<tr>
	<td align="left" class="table1">Student Id : </td>
	<td class="table2 form01"><input type="text" name="studentLoginId" value="<?php echo $_smarty_tpl->tpl_vars['studentLoginId']->value;?>
"></td>
</tr>
<tr>
	<td align="left" class="table1">Student Password : </td>
	<td class="table2 form01"><input type="text" name="studentPassword" value="<?php echo $_smarty_tpl->tpl_vars['studentPassword']->value;?>
"></td>
</tr>
<tr>
	<td align="left" class="table1">Parent Id : </td>
	<td class="table2 form01"><input type="text" name="parentLoginId" value="<?php echo $_smarty_tpl->tpl_vars['parentLoginId']->value;?>
"></td>
</tr>
<tr>
	<td align="left" class="table1">Parent Password : </td>
	<td class="table2 form01"><input type="text" name="parentPassword" value="<?php echo $_smarty_tpl->tpl_vars['parentPassword']->value;?>
"></td>
</tr>
<?php }?>
<tr>
	<td align="left" class="table1">Gender: </td>
	<td class="table2 form01">
		<select name="gender">
			<?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['maleArrValue']->value,'output'=>$_smarty_tpl->tpl_vars['maleArrValue']->value,'selected'=>$_smarty_tpl->tpl_vars['gender']->value),$_smarty_tpl);?>

		</select>
	</td>
</tr>
<tr>
	<td align="left" class="table1">Current Address : </td>
	<td class="table2 form01"><textarea cols="21" rows="10" name="currentAddress"><?php echo $_smarty_tpl->tpl_vars['currentAddress']->value;?>
</textarea></td>
</tr>
<tr>
	<td align="left" class="table1">Resi. Phone No 1 : </td>
	<td class="table2 form01"><input type="text" name="residencePhone1" value="<?php echo $_smarty_tpl->tpl_vars['residencePhone1']->value;?>
"></td>
</tr>
<tr>
	<td align="left" class="table1">Resi. Phone No 2 : </td>
	<td class="table2 form01"><input type="text" name="residencePhone2" value="<?php echo $_smarty_tpl->tpl_vars['residencePhone2']->value;?>
"></td>
</tr>
<tr>
	<td align="left" class="table1">SMS Mobile : </td>
	<td class="table2 form01"><input type="text" name="smsMobile" value="<?php echo $_smarty_tpl->tpl_vars['smsMobile']->value;?>
"></td>
</tr>
<tr>
	<td align="left" class="table1">Student Email: </td>
	<td class="table2 form01"><input type="text" name="studentEmail" value="<?php echo $_smarty_tpl->tpl_vars['studentEmail']->value;?>
"></td>
</tr>
<tr>	
	<td align="left" class="table1">Permanent Address : </td>
	<td class="table2 form01"><textarea cols="21" rows="10" name="permanentAddress"><?php echo $_smarty_tpl->tpl_vars['permanentAddress']->value;?>
</textarea></td>
</tr>
<tr>
	<td align="left" class="table1">Previous School : </td>
	<td class="table2 form01"><input type="text" name="previousSchool" value="<?php echo $_smarty_tpl->tpl_vars['previousSchool']->value;?>
"></td>
</tr>
<tr>	
	<td align="left" class="table1">Urban : </td>
	<td class="table2 form01"><input type="text" name="urban" value="<?php echo $_smarty_tpl->tpl_vars['urban']->value;?>
"></td>
</tr>
<tr>
	<td align="center" colspan="4" class="table1"><h1>Postal Details</h1></td>
</tr>
<tr>
	<td align="left" class="table1">Religion: </td>
	<td class="table2 form01"><input type="text" name="religion" value="<?php echo $_smarty_tpl->tpl_vars['religion']->value;?>
"></td>
</tr>
<tr>	
	<td align="left" class="table1">Blood Group : </td>
	<td class="table2 form01"><!--<input type="text" name="bloodGroup" value="<?php echo $_smarty_tpl->tpl_vars['bloodGroup']->value;?>
">-->
		<select name="bloodGroup">
			<?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['bloodGroups']->value,'output'=>$_smarty_tpl->tpl_vars['bloodGroups']->value,'selected'=>$_smarty_tpl->tpl_vars['bloodGroup']->value),$_smarty_tpl);?>

		</select>
	</td>
</tr>
<tr>
	<td align="left" class="table1">House Name: </td>
	<td class="table2 form01">
		<select name="houseId">
			<option value="0">Select House</option>
			  <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['houseArray']->value['houseId'],'output'=>$_smarty_tpl->tpl_vars['houseArray']->value['houseName'],'selected'=>$_smarty_tpl->tpl_vars['houseId']->value),$_smarty_tpl);?>

	  </select>
	</td>
</tr>
<tr>
	<td align="left" class="table1">Current Address : </td>
	<td class="table2 form01"><textarea cols="21" rows="10" name="currentAddress"><?php echo $_smarty_tpl->tpl_vars['currentAddress']->value;?>
</textarea></td>
</tr>
<tr>
	<td align="left" class="table1">Current State : </td>
	<td class="table2 form01"><input type="text" name="currentState" value="<?php echo $_smarty_tpl->tpl_vars['currentState']->value;?>
"></td>
</tr>
<tr>
	<td align="left" class="table1">Current Address Pin : </td>
	<td class="table2 form01"><input type="text" name="currentAddressPin" value="<?php echo $_smarty_tpl->tpl_vars['currentAddressPin']->value;?>
"></td>
</tr>
<tr>
	<td align="left" class="table1">Permanent Address: </td>
	<td class="table2 form01"><textarea cols="19" rows="10" name="permanentAddress"><?php echo $_smarty_tpl->tpl_vars['permanentAddress']->value;?>
</textarea></td>
</tr>
<tr>
	<td align="left" class="table1">Permanent State : </td>
	<td class="table2 form01"><input type="text" name="permanentState" value="<?php echo $_smarty_tpl->tpl_vars['permanentState']->value;?>
"></td>
</tr>
<tr>
	<td align="left" class="table1">Permanent PIN: </td>
	<td class="table2 form01"><input type="text" name="permanentPIN" value="<?php echo $_smarty_tpl->tpl_vars['permanentPIN']->value;?>
"></td>
</tr>
<tr>
	<td align="left" class="table1">Created On: </td>
	<td class="table2 form01">
		<?php echo smarty_function_html_select_date(array('prefix'=>'createdon','field_order'=>"DmY",'month_format'=>"%m",'start_year'=>'-5','end_year'=>'+5','time'=>$_smarty_tpl->tpl_vars['createdOn']->value,'day_value_format'=>"%02d"),$_smarty_tpl);?>

	</td>
</tr>
<tr>
	<td colspan="4" align="center" class="table1"><h1>Parents Details</h1></td>
</tr>
<tr>
	<td align="left" class="table1">Father Mobile : </td>
	<td class="table2 form01"><input type="text" name="fatherMobile" value="<?php echo $_smarty_tpl->tpl_vars['fatherMobile']->value;?>
"></td>
</tr>
<tr>
	<td align="left" class="table1">Father Email: </td>
	<td class="table2 form01"><input type="text" name="fatherEmail" value="<?php echo $_smarty_tpl->tpl_vars['fatherEmail']->value;?>
"></td>
</tr>
<tr>
	<td align="left" class="table1">Father Occupation : </td>
	<td class="table2 form01"><input type="text" name="fatherOccupation" value="<?php echo $_smarty_tpl->tpl_vars['fatherOccupation']->value;?>
"></td>
</tr>
<tr>
	<td align="left" class="table1">Fathers Designation : </td>
	<td class="table2 form01"><input type="text" name="fathersDesignation" value="<?php echo $_smarty_tpl->tpl_vars['fathersDesignation']->value;?>
"></td>
</tr>

<tr>
	<td align="left" class="table1">Fathers Organization : </td>
	<td class="table2 form01"><input type="text" name="fathersOrganization" value="<?php echo $_smarty_tpl->tpl_vars['fathersOrganization']->value;?>
"></td>
</tr>
<tr>
	<td align="left" class="table1">Mothers Name : </td>
	<td class="table2 form01"><input type="text" name="mothersName" value="<?php echo $_smarty_tpl->tpl_vars['mothersName']->value;?>
"></td>
</tr>

<tr>
	<td align="left" class="table1">Mothers Phone : </td>
	<td class="table2 form01"><input type="text" name="mothersPhone" value="<?php echo $_smarty_tpl->tpl_vars['mothersPhone']->value;?>
"></td>
</tr>
<tr>
	<td align="left" class="table1">Mothers Emailid : </td>
	<td class="table2 form01"><input type="text" name="mothersEmailid" value="<?php echo $_smarty_tpl->tpl_vars['mothersEmailid']->value;?>
"></td>
</tr>
<tr>
	<td align="left" class="table1">Mothers Mobile : </td>
	<td class="table2 form01"><input type="text" name="mothersMobile" value="<?php echo $_smarty_tpl->tpl_vars['mothersMobile']->value;?>
"></td>
</tr>
<tr>	
	<td align="left" class="table1">Mother Occupation : </td>
	<td class="table2 form01"><input type="text" name="motherOccupation" value="<?php echo $_smarty_tpl->tpl_vars['motherOccupation']->value;?>
"></td>
</tr>
<tr>
	<td align="left" class="table1">Mothers Designation : </td>
	<td class="table2 form01"><input type="text" name="mothersDesignation" value="<?php echo $_smarty_tpl->tpl_vars['mothersDesignation']->value;?>
"></td>
</tr>
<tr>
	<td align="left" class="table1">Mothers Organization : </td>
	<td class="table2 form01"><input type="text" name="mothersOrganization" value="<?php echo $_smarty_tpl->tpl_vars['mothersOrganization']->value;?>
"></td>
</tr>
<tr>
	<td align="left" class="table1">Father Phone : </td>
	<td class="table2 form01"><input type="text" name="fatherPhone" value="<?php echo $_smarty_tpl->tpl_vars['fatherPhone']->value;?>
"></td>
</tr>
<tr>
	<td align="left" class="table1">Aadhaar : </td>
	<td class="table2 form01"><input type="text" name="aadhaar" value="<?php echo $_smarty_tpl->tpl_vars['aadhaar']->value;?>
"></td>
</tr>
<tr>
	<td align="left" class="table1">Activated</td>
	<td align="left" class="table2 form01">
		<select name="activated">
			<?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['secArrValue']->value,'output'=>$_smarty_tpl->tpl_vars['secArrOut']->value,'selected'=>$_smarty_tpl->tpl_vars['activated']->value),$_smarty_tpl);?>

	  </select>
	</td>
</tr>
<tr>
	<td colspan="4" align="center"><input type="submit" name="submit" class="newSubmitBtn" value="Save">
</tr>
</table>
</form>

<?php }} ?>

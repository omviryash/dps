<?php /* Smarty version Smarty-3.1.16, created on 2016-04-12 16:57:06
         compiled from ".\templates\houseReport.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5485570cdb8aef3031-38060074%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8ef53ff71168447b7ab249b6a8a7d461cd6c0c07' => 
    array (
      0 => '.\\templates\\houseReport.tpl',
      1 => 1458044849,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5485570cdb8aef3031-38060074',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'attendenceArr' => 0,
    'countTotal' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_570cdb8b031ad4_27007109',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_570cdb8b031ad4_27007109')) {function content_570cdb8b031ad4_27007109($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("./main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 10,
		"bJQueryUI":true
  });
});
</script>


</br></br>
<table align="left" border="1" id="myDataTable" class="display">
	<div class="hd"><h2 align="center">House Report</h2></div>
	<thead>
	<tr>
		<td align="left"><b>House Name</b></td>
	  <td align="left"><b>Male</b></td>
	  <td align="left"><b>Female</b></td>
	  <td align="left"><b>Total</b></td>
  </tr>
  </thead>
  <tbody>
  <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['name'] = "sec";
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['attendenceArr']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total']);
?>
  <tr>
  	<td align="left"><a href='houseWise.php?houseId=<?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['houseId'];?>
'><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['houseName'];?>
</a></td>
    <td align="left"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['countP'];?>
</td>
    <td align="left"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['countA'];?>
</td>
    <td align="left"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['totalNumberWorkingDay'];?>
</td>
  </tr>
 <?php endfor; endif; ?>
 </tbody>
 <tfoot>
   <tr>
   	<th></th>
   	<th></th>
   	<th></th>
   	<th align="left"><?php echo $_smarty_tpl->tpl_vars['countTotal']->value;?>
</th>
   </tr>
 </tfoot>
</table>
<?php }} ?>

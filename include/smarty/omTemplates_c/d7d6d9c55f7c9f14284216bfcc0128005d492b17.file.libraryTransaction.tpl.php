<?php /* Smarty version Smarty-3.1.16, created on 2016-05-11 17:03:12
         compiled from ".\templates\libraryTransaction.tpl" */ ?>
<?php /*%%SmartyHeaderCode:285825733187813efe9-30225898%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd7d6d9c55f7c9f14284216bfcc0128005d492b17' => 
    array (
      0 => '.\\templates\\libraryTransaction.tpl',
      1 => 1449819293,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '285825733187813efe9-30225898',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'libraryTransactionId' => 0,
    'confirm' => 0,
    'checktype' => 0,
    'grNo' => 0,
    'dArray' => 0,
    'toName' => 0,
    'bookAccessionNo' => 0,
    'bookTitle' => 0,
    'issueDate' => 0,
    'returnableDate' => 0,
    'fine' => 0,
    'finePay' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_573318782b6047_68205563',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_573318782b6047_68205563')) {function content_573318782b6047_68205563($_smarty_tpl) {?><?php if (!is_callable('smarty_function_html_options')) include 'D:\\xampp\\htdocs\\dps\\include\\smarty\\libs\\plugins\\function.html_options.php';
if (!is_callable('smarty_function_html_select_date')) include 'D:\\xampp\\htdocs\\dps\\include\\smarty\\libs\\plugins\\function.html_select_date.php';
?><?php echo $_smarty_tpl->getSubTemplate ("./main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<script type="text/javascript">
$(document).ready(function()
{
	if($("#checktype").val() == 'S')
	{
	  $('#empNameBest').hide();
	  $('#empCodeBest').hide();
	}
	if($("#checktype").val() == 'E')
	{
		$('#grNoBest').hide();
	}
});
function hideStudent()
{
	$('#empNameBest').show();
	$('#empCodeBest').show();
	$('#grNoBest').hide();
	$('#stdNameBest').hide();
}
function hideEmp()
{
	$('#empNameBest').hide();
	$('#empCodeBest').hide();
	$('#grNoBest').show();
	$('#stdNameBest').show();
}
function checkGrno()
{
	var grNo = $("#grNo").val() != '' ? $("#grNo").val() : 0
	var dataString = "grNo=" + grNo;
	$.ajax({
		type : 'GET',
		url  : 'setStudNameLib.php',
		data :  dataString,
		success:function(data)
		{
			$('#stdNameBest').html(data);
	  }
	});
}
function checkEmpCode()
{
	var nameStaff = $("#nameStaff").val() != '' ? $("#nameStaff").val() : 0
	var dataString = "nameStaff=" + nameStaff;
	$.ajax({
		type : 'GET',
		url  : 'setEmpNameLib.php',
		data :  dataString,
		success:function(data)
		{
			$('#empCodeBest').html(data);
	  }
	});
}

function checkAcNo()
{	
	var dataString = "bookAccessionNo=" +$("#bookAccessionNo").val();
	$.ajax({
		type : 'GET',
		url  : 'setBookName.php',
		data :  dataString,
		success:function(data)
		{
      $('#bookTitle').val(data);
	  }
	});
}

function checkGrnoBook()
{
	var dataString = "grNo=" +$(".grNo").val();
	$.ajax({
		type : 'GET',
		url  : 'checkGrnoBook.php',
		data :  dataString,
		success:function(data)
		{
			if(data == 2)
			{
				alert("Last Book Not Issue");
				$('.grNo').val('');
				$('.grNo').focus();
				return false;
			}
	  }
	});
}

function checkSecondBook()
{
	var dataString = "bookAccessionNo=" +$("#bookAccessionNo").val();
	$.ajax({
		type : 'GET',
		url  : 'checkSecondBook.php',
		data :  dataString,
		success:function(data)
		{
			if(data == 2)
			{
				alert("This Book Is Already Issued");
				$('#bookTitle').val('');
				$('#bookAccessionNo').focus();
				return false;
			}
	  }
	});
}

function setFine()
{
  var dayExtra    = $('#fromDateDay').val();
  var monthExtra  = $('#fromDateMonth').val();
  var yearExtra   = $('#fromDateYear').val();
  var dayExtraIssue    = $('#issueDateDay').val();
  var monthExtraIssue  = $('#issueDateMonth').val();
  var yearExtraIssue   = $('#issueDateYear').val();
  var issueDate   = yearExtraIssue+'-'+monthExtraIssue+'-'+dayExtraIssue;
	var fullDate    = yearExtra+'-'+monthExtra+'-'+dayExtra;
	var dataString = "returnableDate=" + fullDate + "&issueDate=" + issueDate;
  $.ajax(
  {
    type: "GET",
    url: "setFine.php",
    data: dataString,
    success:function(data)
    {
      $('.getFine').val(data);
    }
  });
}

</script>



<form name="form1" method="POST" action="libraryTransaction.php" onsubmit="return confirm('DO You Want To Submit !!!!!')";>
<table align="center" border="0" class="table2">
<div class="hd"><h2 align="center">Library Transaction Entry</h2>
<input type="hidden" name="libraryTransactionId" value="<?php echo $_smarty_tpl->tpl_vars['libraryTransactionId']->value;?>
">

<tr>
  <td align="center" colspan="2" style="color:red; font-size:18px;"><b><?php echo $_smarty_tpl->tpl_vars['confirm']->value;?>
</b></td>
</tr>
<?php if ($_smarty_tpl->tpl_vars['checktype']->value=='E') {?>
<?php if ($_smarty_tpl->tpl_vars['libraryTransactionId']->value>0) {?>
<tr>
	<td align="center" colspan="2" style="color:red; font-size:18px;">
		Student :- <input type='radio' name='checktype' value='S' onclick='hideEmp();'  Autofocus DISABLED >
		Staff :- <input type='radio' name='checktype' id='checktype' value='E' onclick='hideStudent();' CHECKED DISABLED >
  </td>
</tr>
<?php } else { ?>
<tr>
	<td align="center" colspan="2" style="color:red; font-size:18px;">
		Student :- <input type='radio' name='checktype' value='S' onclick='hideEmp();'  Autofocus >
		Staff :- <input type='radio' name='checktype' id='checktype' value='E' onclick='hideStudent();' CHECKED >
  </td>
</tr>
<?php }?>
<?php } else { ?>
<?php if ($_smarty_tpl->tpl_vars['libraryTransactionId']->value>0) {?>
<tr>
	<td align="center" colspan="2" style="color:red; font-size:18px;">
		Student :- <input type='radio' name='checktype' id='checktype' value='S' onclick='hideEmp();' CHECKED Autofocus DISABLED >
		Staff :- <input type='radio' name='checktype' value='E' onclick='hideStudent();' DISABLED >
  </td>
</tr>
<?php } else { ?>
<tr>
	<td align="center" colspan="2" style="color:red; font-size:18px;">
		Student :- <input type='radio' name='checktype' id='checktype' value='S' onclick='hideEmp();' CHECKED Autofocus >
		Staff :- <input type='radio' name='checktype' value='E' onclick='hideStudent();' >
  </td>
</tr>
<?php }?>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['checktype']->value=='S') {?>
<tr id="grNoBest">
  <td class="table2">Gr No</td>
  <td class="table2 form01">
  	<?php if ($_smarty_tpl->tpl_vars['libraryTransactionId']->value>0) {?>
  	<input type="text" name="grNo" value="<?php echo $_smarty_tpl->tpl_vars['grNo']->value;?>
" id="grNo" class="grNo" onblur="checkGrno();" size='50' />
  	<?php } else { ?>
  	<input type="text" name="grNo" value="<?php echo $_smarty_tpl->tpl_vars['grNo']->value;?>
" id="grNo" class="grNo" onblur="checkGrno(); checkGrnoBook();" size='50' />
  	<?php }?>
	</td>
</tr>
<tr id="stdNameBest">
  
</tr>
<?php } else { ?>
<tr id="grNoBest">
  <td class="table2">Gr No</td>
  <td class="table2 form01">
  	<?php if ($_smarty_tpl->tpl_vars['libraryTransactionId']->value>0) {?>
  	<input type="text" name="grNo" value="<?php echo $_smarty_tpl->tpl_vars['grNo']->value;?>
" id="grNo" class="grNo" onblur="checkGrno();" size='50' />
  	<?php } else { ?>
  	<input type="text" name="grNo" value="<?php echo $_smarty_tpl->tpl_vars['grNo']->value;?>
" id="grNo" class="grNo" onblur="checkGrno(); checkGrnoBook();" size='50' />
  	<?php }?>
	</td>
</tr>
<tr id="stdNameBest">
 
</tr>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['checktype']->value=='E') {?>
<tr id="empNameBest">
  <td class="table2">Name</td>
  <td class="table2 form01">
  	<select name='nameStaff' id='nameStaff' onblur="checkEmpCode();">
  	  <option value=''>Select</option>
  	  <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['dArray']->value['name'],'output'=>$_smarty_tpl->tpl_vars['dArray']->value['name'],'selected'=>$_smarty_tpl->tpl_vars['toName']->value),$_smarty_tpl);?>

    </select>
	</td>
</tr>
<tr id="empCodeBest">
  
</tr>
<?php } else { ?>
<tr id="empNameBest">
  <td class="table2">Name</td>
  <td class="table2 form01">
  	<select name='nameStaff' id='nameStaff' onblur="checkEmpCode();">
  	  <option value=''>Select</option>
  	  <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['dArray']->value['name'],'output'=>$_smarty_tpl->tpl_vars['dArray']->value['name'],'selected'=>$_smarty_tpl->tpl_vars['toName']->value),$_smarty_tpl);?>

    </select>
	</td>
</tr>
<tr id="empCodeBest">
  
</tr>
<?php }?>
<tr>
  <td class="table2">Book Accession No</td>
  <td class="table2 form01">
  	<?php if ($_smarty_tpl->tpl_vars['libraryTransactionId']->value>0) {?>
  	<input type="text" name="bookAccessionNo" id="bookAccessionNo" value="<?php echo $_smarty_tpl->tpl_vars['bookAccessionNo']->value;?>
" onblur="checkAcNo();" size='50' />
  	<?php } else { ?>
  	<input type="text" name="bookAccessionNo" id="bookAccessionNo" value="<?php echo $_smarty_tpl->tpl_vars['bookAccessionNo']->value;?>
" onblur="checkAcNo(); checkSecondBook();" size='50' />
  	<?php }?>
	</td>
</tr>
<tr>
	<td align="left" class="table2 form01">Book Name : </td>
  <td class="table2 form01"><textarea id="bookTitle" cols='50' DISABLED ><?php echo $_smarty_tpl->tpl_vars['bookTitle']->value;?>
</textarea></td>
</tr>
<tr>
  <td class="table2">Issue Date</td>
  <td class="table2 form01" onchange="setFine();">
    <?php echo smarty_function_html_select_date(array('day_extra'=>"id=\"issueDateDay\"",'month_extra'=>"id=\"issueDateMonth\"",'year_extra'=>"id=\"issueDateYear\"",'prefix'=>"issueDate",'start_year'=>"-25",'end_year'=>"+25",'field_order'=>"DMY",'time'=>$_smarty_tpl->tpl_vars['issueDate']->value,'day_value_format'=>"%02d"),$_smarty_tpl);?>

  </td>
</tr>
<?php if ($_smarty_tpl->tpl_vars['libraryTransactionId']->value>0) {?>
<tr>
  <td class="table2">Actual Date of Return</td>
  <td class="table2 form01" onchange="setFine();">
    <?php echo smarty_function_html_select_date(array('day_extra'=>"id=\"fromDateDay\"",'month_extra'=>"id=\"fromDateMonth\"",'year_extra'=>"id=\"fromDateYear\"",'prefix'=>"returnableDate",'start_year'=>"-25",'end_year'=>"+25",'field_order'=>"DMY",'time'=>$_smarty_tpl->tpl_vars['returnableDate']->value,'day_value_format'=>"%02d"),$_smarty_tpl);?>

  </td>
</tr>
<tr>
	<td class="table2">Fine</td>
	<td class="table2 form01" align="center">
    <input type="text" name="fine" value="<?php echo $_smarty_tpl->tpl_vars['fine']->value;?>
" class="getFine" READONLY />
  </td>
</tr>
<tr>
	<td class="table2">Fine Pay</td>
	<td class="table2 form01" align="center">
    <?php if ($_smarty_tpl->tpl_vars['finePay']->value=='Panding') {?>
      <input type="checkbox" name='finePay' >
    <?php } else { ?>
      <input type="checkbox" name='finePay' CHECKED >
    <?php }?>
  </td>
</tr>
<?php }?>
<tr>
  <td class="table2"></td>
  <td class="table2 form01" align="center"><input type="submit" name="Submit" class="button" value="Save"></td>
</tr>
</table>
</form>
<?php }} ?>

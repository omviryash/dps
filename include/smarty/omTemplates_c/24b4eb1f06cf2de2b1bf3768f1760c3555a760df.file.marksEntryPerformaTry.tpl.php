<?php /* Smarty version Smarty-3.1.16, created on 2016-04-11 17:39:31
         compiled from ".\templates\marksEntryPerformaTry.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2022355d1fa3e9adfa5-07670968%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '24b4eb1f06cf2de2b1bf3768f1760c3555a760df' => 
    array (
      0 => '.\\templates\\marksEntryPerformaTry.tpl',
      1 => 1458044849,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2022355d1fa3e9adfa5-07670968',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_55d1fa3ecb3763_81043345',
  'variables' => 
  array (
    'dateArrVal' => 0,
    'dateArrOut' => 0,
    'academicStartYearSelected' => 0,
    's_userType' => 0,
    'cArray' => 0,
    'class' => 0,
    'secArrOut' => 0,
    'section' => 0,
    'clubArr' => 0,
    'subjectMasterId' => 0,
    'scdArray' => 0,
    'subjectAltId' => 0,
    'stdArray' => 0,
    'count' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55d1fa3ecb3763_81043345')) {function content_55d1fa3ecb3763_81043345($_smarty_tpl) {?><?php if (!is_callable('smarty_function_html_options')) include 'F:\\xampp\\htdocs\\dps\\include\\smarty\\libs\\plugins\\function.html_options.php';
?><?php echo $_smarty_tpl->getSubTemplate ("./main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<link rel="stylesheet" href="./css/buttonStyle.css" type="text/css" />
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
    width: 35px;
	}
        .error{
            font-size: 11px;
            color: #FF0000;
        }
        .inputError{
            border: 1px solid #FF0000;
        }
</style>
<script type="text/javascript">
$(document).ready(function(){
	$('#myDataTable').dataTable({
	"aLengthMenu": [[100, 500, 1000], [100, 500, 1000]],
  	"iDisplayLength": 100,
  	"bAutoWidth": false,
  	"aaSorting": [[ 0, "asc" ]],
		"bJQueryUI":true
  });
  
	$(".omAttend").change(function()
  {
  	$('.newGoBtn').click();
  });
});


function buttonHide()
{
	$('#hideMe').hide();
}

function validateForm(){

    var error_count = 0;

    $('.valid0_10, .valid0_30').find('span.error').remove();
    $('.valid0_10, .valid0_30').find('input').removeClass('inputError');

    $('.valid0_10').each(function(){

        if($(this).find('input').val().trim() == ''){
            $(this).find('input').addClass('inputError');
            $(this).append('<span class="error"><br />Value between 0 to 10</span>');
            
            error_count++;
        } else {
            if($(this).find('input').val() < 0 || $(this).find('input').val() > 10){
                $(this).find('input').addClass('inputError');
                $(this).append('<span class="error"><br />Value between 0 to 10</span>');

                error_count++;
            }
        }
    });
    
    $('.valid0_30').each(function(){

        if($(this).find('input').val().trim() == ''){
            $(this).find('input').addClass('inputError');
            $(this).append('<span class="error"><br />Value between 0 to 30</span>');
            
            error_count++;
        } else {
            if($(this).find('input').val() < 0 || $(this).find('input').val() > 30){
                $(this).find('input').addClass('inputError');
                $(this).append('<span class="error"><br />Value between 0 to 30</span>');

                error_count++;
            }
        }
    });
    
    if(error_count > 0)
        return false;
    else
        return true;
}

</script>


</br></br>
<center>
<form name="formGet" id="formGet" method="GET" action="marksEntryPerformaTry.php">
<table align="center" border="1">
	<div class="hd"><h2 align="center">Part 1 Scholastic Entry</h2></div>
	<tr>
		<td class="table2 form01">
		  <select name="startYear" class="">
		    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['dateArrVal']->value,'output'=>$_smarty_tpl->tpl_vars['dateArrOut']->value,'selected'=>$_smarty_tpl->tpl_vars['academicStartYearSelected']->value),$_smarty_tpl);?>

		  </select>
		</td>
		<?php if ($_smarty_tpl->tpl_vars['s_userType']->value=='Administrator') {?>
		<td class="table2 form01">
		  <select name="class" autofocus="autofocus">
		    <option value="0">Select class</option>
		    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['cArray']->value['className'],'output'=>$_smarty_tpl->tpl_vars['cArray']->value['className'],'selected'=>$_smarty_tpl->tpl_vars['class']->value),$_smarty_tpl);?>

		  </select>
	  </td>
    <td class="table2 form01">
		  <select name="section">
		    <option value="0">Select Section</option>
		    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['secArrOut']->value,'output'=>$_smarty_tpl->tpl_vars['secArrOut']->value,'selected'=>$_smarty_tpl->tpl_vars['section']->value),$_smarty_tpl);?>

		  </select>
	  </td>
	  <td class="table2 form01">
	  	<select name="subjectMasterId" class="">
	      <option value="0">Select Subject</option>
	      <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['clubArr']->value['subjectMasterId'],'output'=>$_smarty_tpl->tpl_vars['clubArr']->value['subjectName'],'selected'=>$_smarty_tpl->tpl_vars['subjectMasterId']->value),$_smarty_tpl);?>

		  </select>
		</td>
	  <?php }?>
	  <?php if ($_smarty_tpl->tpl_vars['s_userType']->value!='Administrator') {?>
		<td class="table2 form01">
		  <select name="subjectAltId" class="omAttend">
			  <option value="">Select Class/Subject</option>
        <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['scdArray']->value['subjectAltId'],'output'=>$_smarty_tpl->tpl_vars['scdArray']->value['subjectDtl'],'selected'=>$_smarty_tpl->tpl_vars['subjectAltId']->value),$_smarty_tpl);?>

      </select>
    </td>
    <?php }?>
		<td>
	    <input type="submit" name="go" class="newGoBtn" value="go">
	  </td>
	</tr>
</table>
</form>
</center>
<form name="form2" method="POST" action="marksEntryPerformaTry.php" onsubmit="return validateForm();">
<table align="left" border="1" id="myDataTable" class="display">
  </br></br></br>
	<h1 align="center">Student List</h1>
	</br>
	<h1 align="center"><?php echo $_smarty_tpl->tpl_vars['class']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['section']->value;?>
</h1>
  </br>
  <thead>
  <tr>
    <?php if ($_smarty_tpl->tpl_vars['subjectMasterId']->value==168||$_smarty_tpl->tpl_vars['subjectMasterId']->value==55) {?>
    <th colspan="4">-</th>
    <?php } else { ?>
    <th colspan="3">-</th>
    <?php }?>
    <th colspan="3">Term - 1</th>
    <th colspan="3">Term - 2</th>
  </tr>
	<tr>
		<td align="left" class="table1"><b>Roll. No.</b></td>
		<td align="left" class="table1"><b>G. R. No.</b></td>
		
		<td align="left" class="table1"><b>Name</b></td>
                <?php if ($_smarty_tpl->tpl_vars['subjectMasterId']->value==168||$_smarty_tpl->tpl_vars['subjectMasterId']->value==55) {?>
                <td align="left" class="table1"><b>Subject</b></td>
                <?php }?>
		
		<td align="left" class="table1"><b>FA1 (10%)</b></td>
		<td align="left" class="table1"><b>FA2 (10%)</b></td>
		<td align="left" class="table1"><b>SA1 (30%)</b></td>
		<td align="left" class="table1"><b>FA3 (10%)</b></td>
		<td align="left" class="table1"><b>FA4 (10%)</b></td>
		<td align="left" class="table1"><b>SA2 (30%)</b></td>
  </tr>
  <input type="hidden" name="subjectAltId" value="<?php echo $_smarty_tpl->tpl_vars['subjectAltId']->value;?>
">
  <input type="hidden" name="startYear" value="<?php echo $_smarty_tpl->tpl_vars['academicStartYearSelected']->value;?>
">
  </thead>
  <tbody>
  <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['name'] = "sec";
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['stdArray']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total']);
?>
  <tr class="trRow">
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['rollNo'];?>
</td>
    <td align="left" class="table2">
    	<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['grNo'];?>

    	<input type="hidden" name="examMarksPerTryId[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['examMarksPerTryId'];?>
">
    	<input type="hidden" name="subjectMasterId" value="<?php echo $_smarty_tpl->tpl_vars['subjectMasterId']->value;?>
">
    </td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['studentName'];?>
</td>
    
    <?php if ($_smarty_tpl->tpl_vars['subjectMasterId']->value==168||$_smarty_tpl->tpl_vars['subjectMasterId']->value==55) {?>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['subjectName'];?>
</td>
    <?php }?>
    
    <?php if ($_smarty_tpl->tpl_vars['subjectMasterId']->value==2||$_smarty_tpl->tpl_vars['subjectMasterId']->value==165) {?>
        <td align="center" class="table2 valid0_10"><input type="text" name="sub1Fa1[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub1Fa1'];?>
"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub1Fa2[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub1Fa2'];?>
"></td>
  	<td align="center" class="table2 valid0_30"><input type="text" name="sub1Sa1[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub1Sa1'];?>
"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub1Fa3[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub1Fa3'];?>
"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub1Fa4[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub1Fa4'];?>
"></td>
  	<td align="center" class="table2 valid0_30"><input type="text" name="sub1Sa2[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub1Sa2'];?>
"></td>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['subjectMasterId']->value==3||$_smarty_tpl->tpl_vars['subjectMasterId']->value==164) {?>
        <td align="center" class="table2 valid0_10"><input type="text" name="sub2Fa1[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub2Fa1'];?>
"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub2Fa2[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub2Fa2'];?>
"></td>
  	<td align="center" class="table2 valid0_30"><input type="text" name="sub2Sa1[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub2Sa1'];?>
"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub2Fa3[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub2Fa3'];?>
"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub2Fa4[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub2Fa4'];?>
"></td>
  	<td align="center" class="table2 valid0_30"><input type="text" name="sub2Sa2[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub2Sa2'];?>
"></td>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['subjectMasterId']->value==1||$_smarty_tpl->tpl_vars['subjectMasterId']->value==166) {?>
        <td align="center" class="table2 valid0_10"><input type="text" name="sub3Fa1[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub3Fa1'];?>
"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub3Fa2[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub3Fa2'];?>
"></td>
  	<td align="center" class="table2 valid0_30"><input type="text" name="sub3Sa1[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub3Sa1'];?>
"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub3Fa3[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub3Fa3'];?>
"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub3Fa4[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub3Fa4'];?>
"></td>
  	<td align="center" class="table2 valid0_30"><input type="text" name="sub3Sa2[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub3Sa2'];?>
"></td>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['subjectMasterId']->value==192) {?>
        <td align="center" class="table2 valid0_10"><input type="text" name="sub3Fa1[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub3Fa1'];?>
"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub3Fa2[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub3Fa2'];?>
"></td>
  	<td align="center" class="table2 valid0_30"><input type="text" name="sub3Sa1[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub3Sa1'];?>
"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub3Fa3[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub3Fa3'];?>
"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub3Fa4[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub3Fa4'];?>
"></td>
  	<td align="center" class="table2 valid0_30"><input type="text" name="sub3Sa2[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub3Sa2'];?>
"></td>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['subjectMasterId']->value==85||$_smarty_tpl->tpl_vars['subjectMasterId']->value==66) {?>
        <td align="center" class="table2 valid0_10"><input type="text" name="sub4Fa1[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub4Fa1'];?>
"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub4Fa2[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub4Fa2'];?>
"></td>
  	<td align="center" class="table2 valid0_30"><input type="text" name="sub4Sa1[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub4Sa1'];?>
"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub4Fa3[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub4Fa3'];?>
"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub4Fa4[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub4Fa4'];?>
"></td>
  	<td align="center" class="table2 valid0_30"><input type="text" name="sub4Sa2[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub4Sa2'];?>
"></td>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['subjectMasterId']->value==167||$_smarty_tpl->tpl_vars['subjectMasterId']->value==4) {?>
        <td align="center" class="table2 valid0_10"><input type="text" name="sub5Fa1[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub5Fa1'];?>
"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub5Fa2[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub5Fa2'];?>
"></td>
  	<td align="center" class="table2 valid0_30"><input type="text" name="sub5Sa1[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub5Sa1'];?>
"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub5Fa3[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub5Fa3'];?>
"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub5Fa4[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub5Fa4'];?>
"></td>
  	<td align="center" class="table2 valid0_30"><input type="text" name="sub5Sa2[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub5Sa2'];?>
"></td>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['subjectMasterId']->value==168||$_smarty_tpl->tpl_vars['subjectMasterId']->value==55) {?>
        <td align="center" class="table2 valid0_10"><input type="text" name="sub6Fa1[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub6Fa1'];?>
"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub6Fa2[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub6Fa2'];?>
"></td>
  	<td align="center" class="table2 valid0_30"><input type="text" name="sub6Sa1[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub6Sa1'];?>
"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub6Fa3[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub6Fa3'];?>
"></td>
  	<td align="center" class="table2 valid0_10"><input type="text" name="sub6Fa4[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub6Fa4'];?>
"></td>
  	<td align="center" class="table2 valid0_30"><input type="text" name="sub6Sa2[]" value="<?php echo $_smarty_tpl->tpl_vars['stdArray']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['sub6Sa2'];?>
"></td>
    <?php }?>
  </tr>
  <?php endfor; endif; ?>
  </tbody>
  <tfoot>
  <?php if ($_smarty_tpl->tpl_vars['count']->value!=0) {?>
  <tr>
    <?php if ($_smarty_tpl->tpl_vars['subjectMasterId']->value==168||$_smarty_tpl->tpl_vars['subjectMasterId']->value==55) {?>
    <td align="center" colspan="10" class="table2"><input type="submit" name="submitTaken" class="newSubmitBtn" value="Save"></td>
    <?php } else { ?>
    <td align="center" colspan="9" class="table2"><input type="submit" name="submitTaken" class="newSubmitBtn" value="Save"></td>
    <?php }?>
  </tr>
  <?php }?>
  </tfoot>
</table>
</form>
<?php }} ?>

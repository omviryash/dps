<?php /* Smarty version Smarty-3.1.16, created on 2016-04-11 17:39:44
         compiled from ".\templates\nominalRollListReportCard.tpl" */ ?>
<?php /*%%SmartyHeaderCode:23763570b9408f1e842-87957208%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f7f81c21ff59f4dccdddc85ecb042b3041dc85f2' => 
    array (
      0 => '.\\templates\\nominalRollListReportCard.tpl',
      1 => 1458044849,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '23763570b9408f1e842-87957208',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'cArray' => 0,
    'class' => 0,
    'secArrOut' => 0,
    'section' => 0,
    'dateArrVal' => 0,
    'dateArrOut' => 0,
    'academicStartYear' => 0,
    'nominalArr' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_570b940904d8d8_83830654',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_570b940904d8d8_83830654')) {function content_570b940904d8d8_83830654($_smarty_tpl) {?><?php if (!is_callable('smarty_function_html_options')) include 'F:\\xampp\\htdocs\\dps\\include\\smarty\\libs\\plugins\\function.html_options.php';
?><?php echo $_smarty_tpl->getSubTemplate ("./main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[100, 500, 1000], [100, 500, 1000]],
  	"iDisplayLength": 100,
		"bJQueryUI":true
  });
});
</script>


</br></br>
<form name="formGet" method="GET" action="nominalRollListReportCard.php">
<table align="center">
	<tr>
		<td class="table2 form01">
		  <select name="class" autofocus="autofocus" required >
		    <option value="">Select class</option>
		    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['cArray']->value['className'],'output'=>$_smarty_tpl->tpl_vars['cArray']->value['className'],'selected'=>$_smarty_tpl->tpl_vars['class']->value),$_smarty_tpl);?>

		  </select>
	  </td>
    <td class="table2 form01">
		  <select name="classSection">
		    <option value="0">Select Section</option>
		    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['secArrOut']->value,'output'=>$_smarty_tpl->tpl_vars['secArrOut']->value,'selected'=>$_smarty_tpl->tpl_vars['section']->value),$_smarty_tpl);?>

		  </select>
	  </td>
	  <td class="table2 form01">
      <select name="startYear" id="startDateYear">
        <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['dateArrVal']->value,'output'=>$_smarty_tpl->tpl_vars['dateArrOut']->value,'selected'=>$_smarty_tpl->tpl_vars['academicStartYear']->value),$_smarty_tpl);?>

      </select>
	  </td>
    <td>
      <input type="submit" name="submit" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
</form>
<table align="left" border="1" id="myDataTable" class="display">
	<div class="hd"><h2 align="center">Report Card</h2>
	<thead>
	<tr>
		<td align="left"><b>Academic Year</b></td>
		<td align="left"><b>Sr No</b></td>
		<td align="left"><b>GR No</b></td>
		<td align="left"><b>Roll No</b></td>
		<td align="left"><b>Student Name</b></td>
		<td align="left"><b>Class</b></td>
		<td align="left"><b>Section</b></td>
		<td align="left"><b>Term 1 Report Card</b></td>
		<td align="left"><b>Term 2 Report Card</b></td>
  </tr>
  </thead>
  <tbody>
  <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['name'] = "sec";
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['nominalArr']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total']);
?>
  <tr>
  	<td align="left"><?php echo $_smarty_tpl->tpl_vars['nominalArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['academicYear'];?>
</td>
  	<td align="left"><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['sec']['rownum'];?>
</td>
    <td align="left"><?php echo $_smarty_tpl->tpl_vars['nominalArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['grNo'];?>
</td>
    <td align="left"><?php echo $_smarty_tpl->tpl_vars['nominalArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['rollNo'];?>
</td>
    <td align="left"><?php echo $_smarty_tpl->tpl_vars['nominalArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['studentName'];?>
</td>
    <td align="left"><?php echo $_smarty_tpl->tpl_vars['nominalArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['class'];?>
</td>
    <td align="left"><?php echo $_smarty_tpl->tpl_vars['nominalArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['section'];?>
</td>
    <td align="left"><a href="myReportCardT1Try.php?academicYear=<?php echo $_smarty_tpl->tpl_vars['nominalArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['academicYear'];?>
&grNo=<?php echo $_smarty_tpl->tpl_vars['nominalArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['grNo'];?>
">Report Card</a></td>
    <td align="left"><a href="myReportCardTry.php?academicYear=<?php echo $_smarty_tpl->tpl_vars['nominalArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['academicYear'];?>
&grNo=<?php echo $_smarty_tpl->tpl_vars['nominalArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['grNo'];?>
">Report Card</a></td>
  </tr>
  <?php endfor; endif; ?>
  </tbody>
</table>
<?php }} ?>

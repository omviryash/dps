<?php /* Smarty version Smarty-3.1.16, created on 2016-05-11 12:37:10
         compiled from ".\templates\studentList.tpl" */ ?>
<?php /*%%SmartyHeaderCode:82395732da1ec585b3-48483877%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '304c32b72533e168e9ae25d87b2a9bc85c02f7be' => 
    array (
      0 => '.\\templates\\studentList.tpl',
      1 => 1440195559,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '82395732da1ec585b3-48483877',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'stdArray' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5732da1ee21693_91967719',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5732da1ee21693_91967719')) {function content_5732da1ee21693_91967719($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("./main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<div class="hd"><h2 align="center">My Profile</h2></div>
<table align="center" border="1" cellpadding="1" cellspacing="1" style="width:50%">
	<tr>
		<td align="center" class="table1"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value['studentImage'];?>
</td>
		<td align="center" class="table1"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value['fatherImage'];?>
</td>
		<td align="center" class="table1"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value['motherImage'];?>
</td>
        </tr>
</table>
<table align="center" border="1" cellpadding="1" cellspacing="1" style="width:50%">
	<tr>
		<td align="left" class="table1"><b>Name : </b></td>
		<td align="left" class="table1"><b> : </b></td>
		<td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value['studentName'];?>
</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Father Name</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value['fatherName'];?>
</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Mother Name</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value['mothersName'];?>
</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>G. R. No.</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value['grNo'];?>
</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Date Of Birth</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value['dateOfBirth'];?>
</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Joined In Class</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value['joinedInClass'];?>
</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Joining Date</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value['joiningDate'];?>
</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Gender</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value['gender'];?>
</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>SMS Mobile</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value['smsMobile'];?>
</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Current Address</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value['currentAddress'];?>
</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Residence Phone1</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value['residencePhone1'];?>
</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Residence Phone2</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value['residencePhone2'];?>
</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Email</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value['studentEmail'];?>
</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Father Phone</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value['fatherPhone'];?>
</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Father Email</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value['fatherEmail'];?>
</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Father Mobile</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value['fatherMobile'];?>
</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Father Occupation</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value['fatherOccupation'];?>
</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Father Designation</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value['fathersDesignation'];?>
</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Father Organization</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value['fathersOrganization'];?>
</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Mother Phone</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value['mothersPhone'];?>
</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Mother Email</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value['mothersEmailid'];?>
</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Mother Mobile</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value['mothersMobile'];?>
</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Mother Occupation</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value['motherOccupation'];?>
</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Mother Designation</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value['mothersDesignation'];?>
</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Mother Organization</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value['mothersOrganization'];?>
</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Permanent Address</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value['permanentAddress'];?>
</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Religion</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value['religion'];?>
</td>
  </tr>
  <tr>
  	<td align="left" class="table1"><b>Blood Group</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value['bloodGroup'];?>
</td>
  </tr>
                    <tr>
  	<td align="left" class="table1"><b>House</b></td>
  	<td align="left" class="table1"><b> : </b></td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['stdArray']->value['houseName'];?>
</td>
  </tr>
</table>
<?php }} ?>

<?php /* Smarty version Smarty-3.1.16, created on 2016-05-11 16:46:14
         compiled from ".\templates\attendEdit.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2725357330de43ec228-27506214%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a665203f74dd51485e05c396547aefa19c892991' => 
    array (
      0 => '.\\templates\\attendEdit.tpl',
      1 => 1462965365,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2725357330de43ec228-27506214',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_57330de4555789_36043976',
  'variables' => 
  array (
    'staff_type' => 0,
    'attendenceDate' => 0,
    'attendDate' => 0,
    'attendenceArr' => 0,
    'lateOut' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57330de4555789_36043976')) {function content_57330de4555789_36043976($_smarty_tpl) {?><?php if (!is_callable('smarty_function_html_select_date')) include 'D:\\xampp\\htdocs\\dps\\include\\smarty\\libs\\plugins\\function.html_select_date.php';
if (!is_callable('smarty_function_html_options')) include 'D:\\xampp\\htdocs\\dps\\include\\smarty\\libs\\plugins\\function.html_options.php';
?><?php echo $_smarty_tpl->getSubTemplate ("./main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<link rel="stylesheet" href="./css/buttonStyle.css" type="text/css" />

<script src="./js/jquery.min.js"></script>
<SCRIPT language = "javascript">
function confirmedCheckbox()
{
  
  if($("#checkAll").is(':checked') == 1)
  {
    $(".confirmed").attr('checked', true);
  }
  else
    $(".confirmed").attr('checked', false);
}
</SCRIPT>



</br></br>

<form name="formGet" method="GET" action="attendEdit.php">
<table align="center">
	<tr>
    <td>	
    <select name="staff_type">
    	<option value="Teacher" <?php if ($_smarty_tpl->tpl_vars['staff_type']->value=='Teacher') {?>selected<?php }?>>Teacher</option>
    	<option value="Admin Staff" <?php if ($_smarty_tpl->tpl_vars['staff_type']->value=='Admin Staff') {?>selected<?php }?>>Admin Staff</option>
    	<option value="Class IV" <?php if ($_smarty_tpl->tpl_vars['staff_type']->value=='Class IV') {?>selected<?php }?>>Class IV</option>
    </select> 	
    </td>
    <td>
      <?php echo smarty_function_html_select_date(array('prefix'=>"attendence",'start_year'=>"-25",'end_year'=>"+25",'field_order'=>"DMY",'time'=>$_smarty_tpl->tpl_vars['attendenceDate']->value,'day_value_format'=>"%02d"),$_smarty_tpl);?>

    </td>
    <td>
      <input type="submit" name="submit" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
</form>
<form name="form2" method="POST" action="attendEdit.php">
<input type="hidden" name="attendDate" value="<?php echo $_smarty_tpl->tpl_vars['attendDate']->value;?>
">
<table align="center" border="1">  
  </br>
	<div class="hd"><h2 align="center">Staff Attendance Time Report</h2></div>
	</br>
	<tr>
		<td align="left" class="table1"><b>S R No</b></td>
		<td align="left" class="table1"><b>Name</b></td>
		<td align="left" class="table1"><b>Time</b></td><td align="left" class="table1"><b>Attendences</b></td>
		<td align="left" class="table1"><b>Late</b></td>
		<td align="left" class="table1"><b>Delete All <input type="checkbox" name="checkAll" id="checkAll" onClick="confirmedCheckbox();"></b></td>
    
  </tr>
  <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['name'] = "sec";
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['attendenceArr']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total']);
?>
  <tr class="gradeRow">
		<td align="left" class="table2">
    	<?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['employeeCode'];?>

    	<input type="hidden" name="employeeattendId[]" value="<?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['employeeattendId'];?>
">
    </td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['name'];?>
</td>
    <td align="left" class="table2"><input type="text" name="startTime[]" value="<?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['startTime'];?>
" size="5" /></td>
    <td align="left" class="table2"><input type="text" name="attendences[]" value="<?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['attendences'];?>
" size="5" /></td>
    <td align="left" class="table2">
    	<select name="late[]" >
	      <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['lateOut']->value,'output'=>$_smarty_tpl->tpl_vars['lateOut']->value,'selected'=>$_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['late']),$_smarty_tpl);?>

	    </select>
    </td>
	<td align="center" class="table2"><input type="checkbox" name="check_list[]" id="confirmed[<?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['employeeattendId'];?>
]" class="confirmed" value="<?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['employeeattendId'];?>
"></td>
  </tr>
  <?php endfor; endif; ?>
  <tr>
    <td align="center" colspan="9" class="table2">
		<input type="submit" name="submitTaken" class="newSubmitBtn" value="Save">
		<input type="submit" name="Delete" class="newSubmitBtn" value="Delete">
	</td>
  </tr>
</table>
</form>
<?php }} ?>

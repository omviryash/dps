<?php /* Smarty version Smarty-3.1.16, created on 2016-12-29 10:54:01
         compiled from "./templates/attendenceReport.tpl" */ ?>
<?php /*%%SmartyHeaderCode:140295910958620516f082d2-07840752%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c10cdb06592b8afa5aafbdb91d32d5bcc472c4ce' => 
    array (
      0 => './templates/attendenceReport.tpl',
      1 => 1482987607,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '140295910958620516f082d2-07840752',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_58620517116071_07200405',
  'variables' => 
  array (
    'attendenceDate' => 0,
    'attendenceArr' => 0,
    'c' => 0,
    'totalPresent' => 0,
    'totalAbsent' => 0,
    'totalStudent' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58620517116071_07200405')) {function content_58620517116071_07200405($_smarty_tpl) {?><?php if (!is_callable('smarty_function_html_select_date')) include '/opt/lampp/htdocs/dps/include/smarty/libs/plugins/function.html_select_date.php';
?><?php echo $_smarty_tpl->getSubTemplate ("./main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
		"bJQueryUI":true,
		"aLengthMenu": [[-1, 10, 20, 30, 40, 50], ["All", 10, 20, 30, 40, 50]],
  	"iDisplayLength": 500
  });
});
</script>


</br></br>
<form name="formGet" method="GET" action="attendenceReport.php">
<table align="center">
	<tr>
    <td>
      <?php echo smarty_function_html_select_date(array('prefix'=>"attendence",'start_year'=>"-25",'end_year'=>"+25",'field_order'=>"DMY",'time'=>$_smarty_tpl->tpl_vars['attendenceDate']->value,'day_value_format'=>"%02d"),$_smarty_tpl);?>

    </td>
    <td>
      <input type="submit" name="submit" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
</form>
<table align="left" border="1" id="myDataTable" class="display">
	<div class="hd"><h2 align="center">Attendance Taken Status Report</h2></div>
	<thead>
	<tr>
	  <td align="center"><b>S R No</b></td>
	  <td align="center"><b>Class</b></td>
	  <td align="center"><b>Section</b></td>
	  <td align="center"><b>Class Teacher</b></td>
	  <td align="center"><b>Present</b></td>
	  <td align="center"><b>Absent</b></td>
	  <td align="center"><b>Total Students</b></td>
	  <td align="center"><b>Attendance Taken Status</b></td>
  </tr>
  </thead>
  <tbody>
  <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['name'] = "sec";
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['attendenceArr']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total']);
?>
  <tr>
    <td align="center"><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['sec']['rownum'];?>
</td>
    <td align="center"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['class'];?>
</td>
    <td align="center"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['section'];?>
</td>
    <td align="center"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['name'];?>
</td>
    <?php if ($_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['taken']=='NT') {?>
	    <td align="center">0</td>
	    <td align="center">0</td>
	    <td align="center"><?php $_smarty_tpl->tpl_vars["c"] = new Smarty_variable($_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['countPresent']+$_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['countAbsent'], null, 0);?><?php echo $_smarty_tpl->tpl_vars['c']->value;?>
</td>	
	    <td align="center">Not Taken</td>
    <?php } else { ?>
	    <td align="center"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['countPresent'];?>
</td>
	    <td align="center"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['countAbsent'];?>
</td>
       <td align="center"><?php $_smarty_tpl->tpl_vars["c"] = new Smarty_variable($_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['countPresent']+$_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['countAbsent'], null, 0);?><?php echo $_smarty_tpl->tpl_vars['c']->value;?>
</td>	    
      <td align="center">Taken</td>
    <?php }?>
  </tr>
	 <?php endfor; endif; ?>
	 </tbody>
	 <tfoot>
	   <tr>
	   	 <th colspan="3" align="center"></th>
	   	 <th align="center">Total Students</th>
	   	 <th align="center"><?php echo $_smarty_tpl->tpl_vars['totalPresent']->value;?>
</th>
	   	 <th align="center"><?php echo $_smarty_tpl->tpl_vars['totalAbsent']->value;?>
</th>
	   	 <th align="center"><?php echo $_smarty_tpl->tpl_vars['totalStudent']->value;?>
</th>
	   	 <th></th>
	   </tr>
	 </tfoot>
</table>

<?php }} ?>

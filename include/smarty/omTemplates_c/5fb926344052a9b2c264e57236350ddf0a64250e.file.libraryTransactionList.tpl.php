<?php /* Smarty version Smarty-3.1.16, created on 2016-05-11 18:01:14
         compiled from ".\templates\libraryTransactionList.tpl" */ ?>
<?php /*%%SmartyHeaderCode:279875733186a64cd03-68504421%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5fb926344052a9b2c264e57236350ddf0a64250e' => 
    array (
      0 => '.\\templates\\libraryTransactionList.tpl',
      1 => 1462968140,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '279875733186a64cd03-68504421',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5733186a7eece6_42913977',
  'variables' => 
  array (
    'staffOut' => 0,
    'staff' => 0,
    'dArray' => 0,
    'nameStaff' => 0,
    'bArray' => 0,
    'bookAccessionNo' => 0,
    'cArray' => 0,
    'class' => 0,
    'secArrOut' => 0,
    'section' => 0,
    'attendDate' => 0,
    'attendenceArr' => 0,
    'today' => 0,
    'lateOut' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5733186a7eece6_42913977')) {function content_5733186a7eece6_42913977($_smarty_tpl) {?><?php if (!is_callable('smarty_function_html_options')) include 'D:\\xampp\\htdocs\\dps\\include\\smarty\\libs\\plugins\\function.html_options.php';
if (!is_callable('smarty_function_html_select_date')) include 'D:\\xampp\\htdocs\\dps\\include\\smarty\\libs\\plugins\\function.html_select_date.php';
if (!is_callable('smarty_modifier_date_format')) include 'D:\\xampp\\htdocs\\dps\\include\\smarty\\libs\\plugins\\modifier.date_format.php';
?><?php echo $_smarty_tpl->getSubTemplate ("./main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script src="./media1/js/jquery-1.4.4.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.jeditable.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[-1, 10, 20, 30, 40, 50], ["All", 10, 20, 30, 40, 50]],
  	"iDisplayLength": 500,
		"bJQueryUI":true
  });
  $(".omAttend").change(function()
  {
  	$('.newGoBtnClick').click();
  });
  
  /* Editable Start*/
	var oTable = $('#myDataTable').dataTable();
     
  /* Apply the jEditable handlers to the table */
  oTable.$('th').editable( './updateRollNoData.php', {
      "submitdata": function ( value, settings ) {
          return {
              "row_id": this.parentNode.getAttribute('th.id'),
              "column": oTable.fnGetPosition( this )[2]
          };
      },
      "height": "20px",
      "width": "100"
  });
  
  /* Editable End*/
  
  $("#startDateYear").change(function()
  {
    var yearExtra  = $('#startDateYear').val();
		var endDate    = yearExtra;
		var datastring = 'endDate=' + endDate;
		$.ajax({	
			type: 'GET',
			url: 'endYear.php',
		  data: datastring,
		  success:function(data)
		  {
		  	$('#endDateYear').val(data);
	    }
	  });
  });
  $('.checkAll').click(function () {
    $('.checkboxAll').attr('checked', this.checked);
  });
  
  var hideEmpStdName = $('.hideEmpStd').val();
	if(hideEmpStdName == 'Staff')
	{
		$('#hideClass').hide();
		$('#hideSection').hide();
		$('#nameStaff').show();
	}
  else
  {
		$('#nameStaff').hide();
		$('#hideClass').show();
		$('#hideSection').show();
	}
});

function checkbox(obj)
{
	var row = $(obj).parents('.gradeRow');
	if(row.find(".testName").attr("checked") == true)
  {
    row.find(".testNameHidden").val('Yes');
  }
  else
  {
    row.find(".testNameHidden").val('No');
  }
}

function setFinePay(objPay)
{
	var row = $(objPay).parents('.gradeRow');
	if(row.find(".finePay").attr("checked") == true)
  {
    row.find(".finePayHidden").val('Paid');
  }
  else
  {
    row.find(".finePayHidden").val('Panding');
  }
  alert(rowfind(".finePayHidden").val());
}
function checkboxAll()
{
	if($(".checkAll").attr("checked") == true)
  {
    $(".testNameHidden").val('Yes');
  }
  else
  {
    $(".testNameHidden").val('No');
  }
}

//function setFineAll()
//{
//	$('.getFine').val(0);
//}
function setFine(obj)
{
	var row = $(obj).parents('.gradeRow');
  var dayExtra    = row.find('#fromDateDay').val();
  var monthExtra  = row.find('#fromDateMonth').val();
  var yearExtra   = row.find('#fromDateYear').val();
  var issueDate = row.find('.issueDate').val();
	var fullDate = yearExtra+'-'+monthExtra+'-'+dayExtra;
	var dataString = "returnableDate=" + fullDate + "&issueDate=" + issueDate;
  $.ajax(
  {
    type: "GET",
    url: "setFine.php",
    data: dataString,
    success:function(data)
    {
      row.find('.getFine').val(data);
      row.find('.fullDate').val(fullDate);
    }
  });
}
</script>


</br></br>
<link rel="stylesheet" href="./css/buttonStyle.css" type="text/css" />
<form name="formGet" method="GET" action="libraryTransactionList.php">
<table align="center">
	<tr>
		<td class="table2 form01">
		  <select name="staff" autofocus="autofocus" class='omAttend hideEmpStd' onchange='hideEmpStd();'>
		    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['staffOut']->value,'output'=>$_smarty_tpl->tpl_vars['staffOut']->value,'selected'=>$_smarty_tpl->tpl_vars['staff']->value),$_smarty_tpl);?>

		  </select>
	  </td>
	  <!--td class="table2">Staff Name</td>
	  <td class="table2 form01">
	  	<select name='nameStaff' id='nameStaff' class='omAttend'>
	  	  <option value=''>Select</option>
	  	  <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['dArray']->value['name'],'output'=>$_smarty_tpl->tpl_vars['dArray']->value['name'],'selected'=>$_smarty_tpl->tpl_vars['nameStaff']->value),$_smarty_tpl);?>

	    </select>
		</td-->
		<!--td class="table2 form01">
		  <select name="bookAccessionNo" autofocus="autofocus" class='omAttend'>
		    <option value="">Select Book</option>
		    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['bArray']->value['bookAccessionNo'],'output'=>$_smarty_tpl->tpl_vars['bArray']->value['bookTitle'],'selected'=>$_smarty_tpl->tpl_vars['bookAccessionNo']->value),$_smarty_tpl);?>

		  </select>
	  </td-->
		<td class="table2 form01">
		  <select name="class" autofocus="autofocus" class='omAttend' id='hideClass'>
		    <option value="">Select class</option>
		    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['cArray']->value['className'],'output'=>$_smarty_tpl->tpl_vars['cArray']->value['className'],'selected'=>$_smarty_tpl->tpl_vars['class']->value),$_smarty_tpl);?>

		  </select>
	  </td>
    <td class="table2 form01">
		  <select name="classSection" class='omAttend' id='hideSection'>
		    <option value="0">Select Section</option>
		    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['secArrOut']->value,'output'=>$_smarty_tpl->tpl_vars['secArrOut']->value,'selected'=>$_smarty_tpl->tpl_vars['section']->value),$_smarty_tpl);?>

		  </select>
	  </td>
    <td>
      <input type="submit" name="submit" class="newGoBtn newGoBtnClick" value="Go">
    </td>
  </tr>
</table>
</form>
<!--form name="formGet" method="GET" action="libraryTransactionList.php">
<table align="center">
	<tr>
		<td class="table2 form01">
	    <?php echo smarty_function_html_select_date(array('prefix'=>"attendence",'start_year'=>"-25",'end_year'=>"+25",'field_order'=>"DMY",'time'=>$_smarty_tpl->tpl_vars['attendDate']->value,'day_value_format'=>"%02d",'display_days'=>true),$_smarty_tpl);?>

	  </td>
    <td>
      <input type="submit" name="submitNew" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
</form-->
<form name="form2" method="POST" action="libraryTransactionList.php">
<input type="hidden" name="attendDate" value="<?php echo $_smarty_tpl->tpl_vars['attendDate']->value;?>
">
<table align="center" border="1" id="myDataTable" class="display">  
  </br>
	<div class="hd"><h2 align="center">Book Return List</h2></div>
	</br>
	<thead>
	<tr>
		<td align="left" class="table1"><b>Edit</b></td>
		<?php if ($_smarty_tpl->tpl_vars['staff']->value=='Staff') {?>
		  <td align="left" class="table1"><b>Employee Code</b></td>
		  <td align="left" class="table1"><b>Employee Name</b></td>
		<?php } else { ?>
		  <td align="left" class="table1"><b>GrNo</b></td>
		  <td align="left" class="table1"><b>Student Name</b></td>
		  <td align="left" class="table1"><b>Class</b></td>
		<?php }?>
		<td align="left" class="table1"><b>Book Ac No</b></td>
		<td align="left" class="table1"><b>Book Title</b></td>
		<td align="left" class="table1"><b>Issue Date</b></td>
		<td align="left" class="table1"><b>Scheduled Date of Return</b></td>
		<td align="left" class="table1"><b>Actual Date of Return</b></td>
		<?php if ($_smarty_tpl->tpl_vars['staff']->value!='Staff') {?>
		<td align="left" class="table1"><b>Fine</b></td>
		<?php }?>
		<td align="left" class="table1"><b>Fine Pay</b></td>
		<td class="table1" align='center'>
			<input type="checkbox"  name="checkAll" id="checkAll" class="checkAll" onclick='checkboxAll(); setFine(this);'>
		</td>
  </tr>
  </thead>
  <tbody>
  <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['name'] = "sec";
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['attendenceArr']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total']);
?>
  <tr class="gradeRow">
    </td>
    <td align="left" class="table2"><a href='libraryTransaction.php?libraryTransactionId=<?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['libraryTransactionId'];?>
&checktype=<?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['checktype'];?>
&employeeCode=<?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['employeeCode'];?>
'>Edit</a></td>
    <?php if ($_smarty_tpl->tpl_vars['staff']->value=='Staff') {?>
		  <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['employeeCode'];?>
 <input type="hidden" name="libraryTransactionId[]" value="<?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['libraryTransactionId'];?>
"></td>
		  <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['name'];?>
</td>
		<?php } else { ?>
		  <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['grNo'];?>

		  	<input type="hidden" name="libraryTransactionId[]" value="<?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['libraryTransactionId'];?>
">
		  	<input type="hidden" name="grNo[]" value="<?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['grNo'];?>
">
		  </td>
      <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['studentName'];?>
</td>
      <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['class'];?>
 - <?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['section'];?>
</td>
    <?php }?>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['bookAccessionNo'];?>

    	<input type="hidden" name="bookAccessionNo[]" value="<?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['bookAccessionNo'];?>
">
    </td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['bookTitle'];?>
</td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['issueDate'];?>

    	<input type="hidden" name="" class="issueDate" value="<?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['issueDate'];?>
">
    </td>
    <td align="left" class="table2"><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['returnableDate'],"%d-%m-%Y");?>
</td>
    <td align="left" class="table2" onchange="setFine(this);" NOWRAP >
    	<?php echo smarty_function_html_select_date(array('day_extra'=>"id=\"fromDateDay\"",'month_extra'=>"id=\"fromDateMonth\"",'year_extra'=>"id=\"fromDateYear\"",'prefix'=>"returnableDate",'start_year'=>"-25",'end_year'=>"+25",'field_order'=>"DMY",'time'=>$_smarty_tpl->tpl_vars['today']->value,'day_value_format'=>"%02d",'display_days'=>true),$_smarty_tpl);?>

    	<input type="hidden" name="returnableDate[]" class="fullDate" value="<?php echo $_smarty_tpl->tpl_vars['today']->value;?>
">
    </td>
    <?php if ($_smarty_tpl->tpl_vars['staff']->value!='Staff') {?>
    <td align="left" class="table2">
    	<input type="text" name="fine[]" value="<?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['fine'];?>
" class="getFine" READONLY size="5" >
    </td>
    <?php }?>
    <td align="left" class="table2">
    	<input type="checkbox" class="finePay" onclick='setFinePay(this);' CHECKED >
    	<input type="hidden" name="finePay[]" value="Paid" class="finePayHidden">
    </td>
    <td align="left" class="table2">
    	<input type="checkbox" class="testName checkboxAll" onclick='checkbox(this); setFine(this);' >
    	<input type="hidden" name="returnBook[]" value="No" class="testNameHidden">
    	<!--select name="returnBook[]" >
	      <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['lateOut']->value,'output'=>$_smarty_tpl->tpl_vars['lateOut']->value,'selected'=>$_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['returnBook']),$_smarty_tpl);?>

	    </select-->
    </td>
  </tr>
  <?php endfor; endif; ?>
  </tbody>
  <tfoot>
  <tr>
    <td align="center" colspan="9" class="table2"><input type="submit" name="submitTaken" class="newSubmitBtn" value="Save"></td>
  </tr>
  </tfoot>
</table>
</form>
<?php }} ?>

<?php /* Smarty version Smarty-3.1.16, created on 2016-12-29 12:01:24
         compiled from "./templates/feeCollection.tpl" */ ?>
<?php /*%%SmartyHeaderCode:205766870058624346509072-98791977%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '806193de561b9c5b329f044798426dfd9309a878' => 
    array (
      0 => './templates/feeCollection.tpl',
      1 => 1482987607,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '205766870058624346509072-98791977',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_586243465ac963_61648833',
  'variables' => 
  array (
    'confirm' => 0,
    'isEdit' => 0,
    'rNo' => 0,
    'feeDate' => 0,
    'grNo' => 0,
    'studentName' => 0,
    'editArry' => 0,
    'typeArr' => 0,
    'feeNameOut' => 0,
    'modeOfPayOut' => 0,
    'feeTypeId' => 0,
    'feeName' => 0,
    'modeOfPay' => 0,
    'chNo' => 0,
    'discount' => 0,
    'amount' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_586243465ac963_61648833')) {function content_586243465ac963_61648833($_smarty_tpl) {?><?php if (!is_callable('smarty_function_html_select_date')) include '/opt/lampp/htdocs/dps/include/smarty/libs/plugins/function.html_select_date.php';
if (!is_callable('smarty_function_html_options')) include '/opt/lampp/htdocs/dps/include/smarty/libs/plugins/function.html_options.php';
?><?php echo $_smarty_tpl->getSubTemplate ("./main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<script type="text/javascript">
$(document).ready(function() {
  $(".amount").blur(function()
	{
    $("#one").clone(true).appendTo("#mainDiv")
    .find('input[type="text"]').val('').end()
    .find('select[option=""]').val('');
    $(".rNo:last").focus();
  });
});
function checkGrno(obj)
{
	var row = $(obj).parents('.tableRow');
	var dataString = "grNo=" + row.find(".grNo").val();
	$.ajax({
		type : 'GET',
		url  : 'setStudNameFee.php',
		data :  dataString,
		success:function(data)
		{
      row.find('.studentName').html(data);
	  }
	});
}
function setFullDate(obj)
{
	var row = $(obj).parents('.tableRow');
  var dayExtra    = row.find('#fromDateDay').val();
  var monthExtra  = row.find('#fromDateMonth').val();
  var yearExtra   = row.find('#fromDateYear').val();
	var fullDate = yearExtra+'-'+monthExtra+'-'+dayExtra;
	
  row.find('.fullDate').val(fullDate);
}
</script>


<form name="form1" method="POST" action="feeCollection.php">
<table align="center" border="0" class="table2">
<div class="hd"><h2 align="center">Fee Collection Entry</h2>
<tr>
  <td align="center" colspan="2" style="color:red; font-size:18px;"><b><?php echo $_smarty_tpl->tpl_vars['confirm']->value;?>
</b></td>
</tr>
<tbody id="mainDiv">
<?php if ($_smarty_tpl->tpl_vars['isEdit']->value==1) {?>
<tr class='tableRow'>
	<td class="table2">R NO</td>
  <td class="table2 form01">
  	<input type="text" name="rNo" value="<?php echo $_smarty_tpl->tpl_vars['rNo']->value;?>
" class='rNo' Autofocus />
	</td>
	<td class="table2">Fee Date</td>
  <td class="table2 form01" onchange="setFullDate(this);">
    <?php echo smarty_function_html_select_date(array('day_extra'=>"id=\"fromDateDay\"",'month_extra'=>"id=\"fromDateMonth\"",'year_extra'=>"id=\"fromDateYear\"",'prefix'=>'','start_year'=>"-25",'end_year'=>"+25",'field_order'=>"DMY",'time'=>$_smarty_tpl->tpl_vars['feeDate']->value,'day_value_format'=>"%02d"),$_smarty_tpl);?>

    <input type="hidden" name="feeDate" class="fullDate" value="<?php echo $_smarty_tpl->tpl_vars['feeDate']->value;?>
">
  </td>
</tr>
<tr class='tableRow'>
	<td class="table2">Gr No</td>
  <td class="table2 form01">
  	<input type="text" name="grNo" value="<?php echo $_smarty_tpl->tpl_vars['grNo']->value;?>
" class="grNo" Autofocus onblur="checkGrno(this);" />
	</td>
	<td align="left" class="table2 form01">Student Name : </td>
  <td class="table2 form01 studentName" colspan="5" ><input type="text" name="" value="<?php echo $_smarty_tpl->tpl_vars['studentName']->value;?>
" DISABLED /></td>
</tr>
<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['sec'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['sec']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['name'] = 'sec';
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['editArry']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['sec']['total']);
?>
<tr id="one" class='tableRow'>
	<td class="table2">Fee Type</td>
  <td class="table2 form01">
  	<input type="hidden" name="feeCollectionId[]" value="<?php echo $_smarty_tpl->tpl_vars['editArry']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['feeCollectionId'];?>
">
  	<select name="feeTypeId[]" id="feeType" >
  		<option value=''>Select Fee</option>
      <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['typeArr']->value['feeTypeId'],'output'=>$_smarty_tpl->tpl_vars['typeArr']->value['feeType'],'selected'=>$_smarty_tpl->tpl_vars['editArry']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['feeTypeId']),$_smarty_tpl);?>

    </select>
	</td>
	<td class="table2">Duration</td>
  <td class="table2 form01">
  	<select name="feeName[]" >
  		<option value=''>Duration</option>
      <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['feeNameOut']->value,'output'=>$_smarty_tpl->tpl_vars['feeNameOut']->value,'selected'=>$_smarty_tpl->tpl_vars['editArry']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['feeName']),$_smarty_tpl);?>

    </select>
	</td>
	<td class="table2">Mode Of Pay</td>
  <td class="table2 form01">
  	<select name="modeOfPay[]" >
      <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['modeOfPayOut']->value,'output'=>$_smarty_tpl->tpl_vars['modeOfPayOut']->value,'selected'=>$_smarty_tpl->tpl_vars['editArry']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['modeOfPay']),$_smarty_tpl);?>

    </select>
	</td>
	<td class="table2">ChNo/DDNo</td>
  <td class="table2 form01">
  	<input type="text" name="chNo[]" value="<?php echo $_smarty_tpl->tpl_vars['editArry']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['chNo'];?>
" />
	</td>
	<td class="table2">Discount</td>
  <td class="table2 form01">
  	<input type="text" name="discount[]" value="<?php echo $_smarty_tpl->tpl_vars['editArry']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['discount'];?>
" />
	</td>
	<td class="table2">Amount</td>
  <td class="table2 form01">
  	<input type="text" name="amount[]" class="amount" value="<?php echo $_smarty_tpl->tpl_vars['editArry']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['amount'];?>
" />
	</td>
</tr>
<?php endfor; endif; ?>
<?php } else { ?>
<tr class='tableRow'>
	<td class="table2">R NO</td>
  <td class="table2 form01">
  	<input type="text" name="rNo" value="<?php echo $_smarty_tpl->tpl_vars['rNo']->value;?>
" class='rNo' Autofocus />
	</td>
	<td class="table2">Fee Date</td>
  <td class="table2 form01" onchange="setFullDate(this);">
    <?php echo smarty_function_html_select_date(array('day_extra'=>"id=\"fromDateDay\"",'month_extra'=>"id=\"fromDateMonth\"",'year_extra'=>"id=\"fromDateYear\"",'prefix'=>'','start_year'=>"-25",'end_year'=>"+25",'field_order'=>"DMY",'time'=>$_smarty_tpl->tpl_vars['feeDate']->value,'day_value_format'=>"%02d"),$_smarty_tpl);?>

    <input type="hidden" name="feeDate" class="fullDate" value="<?php echo $_smarty_tpl->tpl_vars['feeDate']->value;?>
">
  </td>
</tr>
<tr class='tableRow'>
	<td class="table2">Gr No</td>
  <td class="table2 form01">
  	<input type="text" name="grNo" value="<?php echo $_smarty_tpl->tpl_vars['grNo']->value;?>
" class="grNo" Autofocus onblur="checkGrno(this);" />
	</td>
	<td align="left" class="table2 form01">Student Name : </td>
  <td class="table2 form01 studentName" colspan="5" ><input type="text" name="" value="<?php echo $_smarty_tpl->tpl_vars['studentName']->value;?>
" DISABLED /></td>
</tr>
<tr id="one" class='tableRow'>
	<td class="table2">Fee Type</td>
  <td class="table2 form01">
  	<select name="feeTypeId[]" id="feeType" >
  		<option value=''>Select Fee</option>
      <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['typeArr']->value['feeTypeId'],'output'=>$_smarty_tpl->tpl_vars['typeArr']->value['feeType'],'selected'=>$_smarty_tpl->tpl_vars['feeTypeId']->value),$_smarty_tpl);?>

    </select>
	</td>
	<td class="table2">Duration</td>
  <td class="table2 form01">
  	<select name="feeName[]" >
  		<option value=''>Duration</option>
      <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['feeNameOut']->value,'output'=>$_smarty_tpl->tpl_vars['feeNameOut']->value,'selected'=>$_smarty_tpl->tpl_vars['feeName']->value),$_smarty_tpl);?>

    </select>
	</td>
	<td class="table2">Mode Of Pay</td>
  <td class="table2 form01">
  	<select name="modeOfPay[]" >
      <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['modeOfPayOut']->value,'output'=>$_smarty_tpl->tpl_vars['modeOfPayOut']->value,'selected'=>$_smarty_tpl->tpl_vars['modeOfPay']->value),$_smarty_tpl);?>

    </select>
	</td>
	<td class="table2">ChNo/DDNo</td>
  <td class="table2 form01">
  	<input type="text" name="chNo[]" value="<?php echo $_smarty_tpl->tpl_vars['chNo']->value;?>
" />
	</td>
	<td class="table2">Discount</td>
  <td class="table2 form01">
  	<input type="text" name="discount[]" value="<?php echo $_smarty_tpl->tpl_vars['discount']->value;?>
" />
	</td>
	<td class="table2">Amount</td>
  <td class="table2 form01">
  	<input type="text" name="amount[]" class="amount" value="<?php echo $_smarty_tpl->tpl_vars['amount']->value;?>
" />
	</td>
</tr>
<?php }?>
</tbody>
<tr>
  <td class="table2 form01" colspan='10' align="center"><input type="submit" name="Submit" class="button" value="Save"></td>
</tr>
</table>
</form>
<?php }} ?>

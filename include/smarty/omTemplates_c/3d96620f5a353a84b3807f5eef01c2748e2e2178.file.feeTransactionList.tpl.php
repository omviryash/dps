<?php /* Smarty version Smarty-3.1.16, created on 2016-12-27 16:02:27
         compiled from "./templates/feeTransactionList.tpl" */ ?>
<?php /*%%SmartyHeaderCode:15888325595862433b683e71-24118001%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3d96620f5a353a84b3807f5eef01c2748e2e2178' => 
    array (
      0 => './templates/feeTransactionList.tpl',
      1 => 1482817430,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15888325595862433b683e71-24118001',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'typeArr' => 0,
    'feeTypeId' => 0,
    'cArray' => 0,
    'class' => 0,
    'secArrOut' => 0,
    'section' => 0,
    'dateArrVal' => 0,
    'dateArrOut' => 0,
    'academicStartYearSelected' => 0,
    'attendDate' => 0,
    'attendenceArr' => 0,
    'academicStartYearSecondNum' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5862433b7139b9_50856132',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5862433b7139b9_50856132')) {function content_5862433b7139b9_50856132($_smarty_tpl) {?><?php if (!is_callable('smarty_function_html_options')) include '/opt/lampp/htdocs/dps/include/smarty/libs/plugins/function.html_options.php';
if (!is_callable('smarty_function_html_select_date')) include '/opt/lampp/htdocs/dps/include/smarty/libs/plugins/function.html_select_date.php';
if (!is_callable('smarty_modifier_Date_format')) include '/opt/lampp/htdocs/dps/include/smarty/libs/plugins/modifier.date_format.php';
?><?php echo $_smarty_tpl->getSubTemplate ("./main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script src="./media1/js/jquery-1.4.4.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="./media1/js/jquery.jeditable.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[-1, 10, 20, 30, 40, 50], ["All", 10, 20, 30, 40, 50]],
  	"iDisplayLength": 500,
  	"aaSorting": [[2, 'desc']],
		"bJQueryUI":true
  });
  $(".omAttend").change(function()
  {
  	$('.newGoBtnClick').click();
  });
  
  /* Editable Start*/
//	var oTable = $('#myDataTable').dataTable();
//     
//  /* Apply the jEditable handlers to the table */
//  oTable.$('th').editable( './updateRollNoData.php', {
//      "submitdata": function ( value, settings ) {
//          return {
//              "row_id": this.parentNode.getAttribute('th.id'),
//              "column": oTable.fnGetPosition( this )[2]
//          };
//      },
//      "height": "20px",
//      "width": "100"
//  });
  
  /* Editable End*/
  
  $("#startDateYear").change(function()
  {
    var yearExtra  = $('#startDateYear').val();
		var endDate    = yearExtra;
		var datastring = 'endDate=' + endDate;
		$.ajax({	
			type: 'GET',
			url: 'endYear.php',
		  data: datastring,
		  success:function(data)
		  {
		  	$('#endDateYear').val(data);
	    }
	  });
  });
});
</script>


</br></br>
<link rel="stylesheet" href="./css/buttonStyle.css" type="text/css" />
<form name="formGet" method="GET" action="feeTransactionList.php">
<table align="center">
	<tr>
		<td class="table2">Fee Type</td>
	  <td class="table2 form01">
	  	<select name="feeTypeId" id="feeType" Autofocus >
	  		<option value="">Select Fee Type</option>
	      <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['typeArr']->value['feeTypeId'],'output'=>$_smarty_tpl->tpl_vars['typeArr']->value['feeType'],'selected'=>$_smarty_tpl->tpl_vars['feeTypeId']->value),$_smarty_tpl);?>

	    </select>
		</td>
		<td class="table2 form01">
		  <select name="class" autofocus="autofocus" class='omAttend'>
		    <option value="">Select Class</option>
		    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['cArray']->value['className'],'output'=>$_smarty_tpl->tpl_vars['cArray']->value['className'],'selected'=>$_smarty_tpl->tpl_vars['class']->value),$_smarty_tpl);?>

		  </select>
	  </td>
    <td class="table2 form01">
		  <select name="classSection" class='omAttend'>
		    <option value="0">Select Section</option>
		    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['secArrOut']->value,'output'=>$_smarty_tpl->tpl_vars['secArrOut']->value,'selected'=>$_smarty_tpl->tpl_vars['section']->value),$_smarty_tpl);?>

		  </select>
	  </td>
	  <td class="table2 form01">
		  <select name="startYear">
		    <?php echo smarty_function_html_options(array('values'=>$_smarty_tpl->tpl_vars['dateArrVal']->value,'output'=>$_smarty_tpl->tpl_vars['dateArrOut']->value,'selected'=>$_smarty_tpl->tpl_vars['academicStartYearSelected']->value),$_smarty_tpl);?>

		  </select>
		</td>
    <td>
      <input type="submit" name="submit" class="newGoBtn newGoBtnClick" value="Go">
    </td>
  </tr>
</table>
</form>
<form name="formGet" method="GET" action="feeTransactionList.php">
<table align="center">
	<tr>
		<td class="table2 form01">
	    <?php echo smarty_function_html_select_date(array('prefix'=>"attendence",'start_year'=>"-25",'end_year'=>"+25",'field_order'=>"DMY",'time'=>$_smarty_tpl->tpl_vars['attendDate']->value,'day_value_format'=>"%02d",'display_days'=>true),$_smarty_tpl);?>

	  </td>
    <td>
      <input type="submit" name="submitNew" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
</form>
<form name="form2" method="POST" action="feeTransactionList.php">
<input type="hidden" name="attendDate" value="<?php echo $_smarty_tpl->tpl_vars['attendDate']->value;?>
">
<table align="center" border="1" id="myDataTable" class="display">  
  </br>
	<div class="hd"><h2 align="center">Fee Collection Report</h2></div>
	</br>
	<thead>
	<tr>
		<td align="left" class="table1"><b>Edit</b></td>
		<td align="left" class="table1"><b>Academic Year</b></td>
		<td align="left" class="table1"><b>Date</b></td>
		<td align="left" class="table1"><b>R No</b></td>
		<td align="left" class="table1"><b>GrNo</b></td>
		<td align="left" class="table1"><b>Class</b></td>
		<td align="left" class="table1"><b>Student Name</b></td>
		<td align="left" class="table1"><b>Fee Type</b></td>
		<td align="left" class="table1"><b>Duration</b></td>
		<td align="left" class="table1"><b>Mode of Pay</b></td>
		<td align="left" class="table1"><b>Ch No.</b></td>
		<td align="left" class="table1"><b>Discount</b></td>
		<td align="left" class="table1"><b>Amount</b></td>
  </tr>
  </thead>
  <tbody>
  <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['name'] = "sec";
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['attendenceArr']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']["sec"]['total']);
?>
  <tr class="gradeRow">
  	<td><a href="feeCollection.php?feeCollectionId=<?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['feeCollectionId'];?>
&rNo=<?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['rNo'];?>
&feeDate=<?php echo smarty_modifier_Date_format($_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['feeDate'],"%Y-%m-%d");?>
">Edit</a></td>
  	<td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['academicStartYearSelected']->value;?>
-<?php echo $_smarty_tpl->tpl_vars['academicStartYearSecondNum']->value;?>
</td>
  	<td align="left" class="table2" NOWRAP ><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['feeDate'];?>
</td>
  	<td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['rNo'];?>
</td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['grNo'];?>
</td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['class'];?>
 - <?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['section'];?>
</td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['studentName'];?>
</td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['feeType'];?>
</td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['feeName'];?>
</td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['modeOfPay'];?>
</td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['chNo'];?>
</td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['discount'];?>
</td>
    <td align="left" class="table2"><?php echo $_smarty_tpl->tpl_vars['attendenceArr']->value[$_smarty_tpl->getVariable('smarty')->value['section']['sec']['index']]['amount'];?>
</td>
  </tr>
  <?php endfor; endif; ?>
  </tbody>
</table>
</form>
<?php }} ?>

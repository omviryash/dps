<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$classTeacherAllotmentId = 0;
	$employeeMasterId        = 0;
	$todayAcademic = date('m-d');
	if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
	{
  	$academicStartYear = date('Y');
	}
	else
	{
		$academicStartYear = date('Y') - 1;
	}
	$academicEndYear         = '0000';
	$class   = "";
	$section = "";
	$isEdit  = 0;
	
	$classArray    = array();
	
  if(isset($_POST['Submit']))
  {
  	$nextYear = $_POST['startYear'] + 1;
  	
  	$academicStartYear = $_POST['startYear']."-04-01";
  	$academicEndYear   = $nextYear."-03-31";
  	$employeeMasterId  = isset($_POST['employeeMasterId']) ? $_POST['employeeMasterId'] : 0;
  	
  	$class   = isset($_POST['class']) ? $_POST['class'] : "";
  	$section = isset($_POST['section']) ? $_POST['section'] : "";
  	$classTeacherAllotmentId = isset($_POST['classTeacherAllotmentId']) ? $_POST['classTeacherAllotmentId'] : 0;
  	
  	if($classTeacherAllotmentId == 0)
  	{
  	  $insertClass = "INSERT INTO classteacherallotment (employeeMasterId,academicStartYear,academicEndYear,class,section)
  	                  VALUES (".$employeeMasterId.",'".$academicStartYear."','".$academicEndYear."','".$class."','".$section."')";
  	  $insertClassRes = om_query($insertClass);
  	  if(!$insertClassRes)
  	  {
  	  	echo "Insert Fail";
  	  }
  	  else
  	  {
  	  	header("Location:classTeacherAllotment.php?done=1");
  	  }
  	}
  	else
  	{
  	  $isEdit = 1;
  	  $updateClass = "UPDATE classteacherallotment
  	                     SET employeeMasterId  = ".$employeeMasterId.",
  	                         academicStartYear = '".$academicStartYear."',
  	                         academicEndYear   = '".$academicEndYear."',
  	                         class   = '".$class."',
  	                         section = '".$section."'
  	                   WHERE classTeacherAllotmentId = ".$_REQUEST['classTeacherAllotmentId'];
      $updateClassRes = om_query($updateClass);
      if(!$updateClassRes)
      {
      	echo "Update Fail";
      }
      else
      {
      	header("Location:classTeacherAllotment.php?done=1");
      }
  	}
  }
  
  $i = 0;
  $selectClass = "SELECT classteacherallotment.classTeacherAllotmentId,
                         classteacherallotment.academicStartYear,classteacherallotment.academicEndYear,
                         classteacherallotment.class,classteacherallotment.section,
                         employeemaster.name
                    FROM classteacherallotment
               LEFT JOIN employeemaster ON employeemaster.employeeMasterId = classteacherallotment.employeeMasterId";
  $selectClassRes = mysql_query($selectClass);
  while($classRow = mysql_fetch_array($selectClassRes))
  {
  	$classArray[$i]['classTeacherAllotmentId'] = $classRow['classTeacherAllotmentId'];
  	$classArray[$i]['name']                    = $classRow['name'];
  	$classArray[$i]['academicStartYear']       = substr($classRow['academicStartYear'],0,4);
  	$classArray[$i]['academicEndYear']         = substr($classRow['academicEndYear'],0,4);
  	
  	$classArray[$i]['class']   = $classRow['class'];
  	$classArray[$i]['section'] = $classRow['section'];
  	$i++;
  }
  
  if(isset($_REQUEST['classTeacherAllotmentId']) > 0)
  {
    $selectClass = "SELECT classteacherallotment.classTeacherAllotmentId,
                           classteacherallotment.academicStartYear,classteacherallotment.academicEndYear,
                           classteacherallotment.class,classteacherallotment.section,
                           employeemaster.name,employeemaster.employeeMasterId
                      FROM classteacherallotment
                 LEFT JOIN employeemaster ON employeemaster.employeeMasterId = classteacherallotment.employeeMasterId
                    WHERE  classTeacherAllotmentId = ".$_REQUEST['classTeacherAllotmentId'];
    $selectClassRes = mysql_query($selectClass);
    if($classRow = mysql_fetch_array($selectClassRes))
    {
    	$classTeacherAllotmentId = $classRow['classTeacherAllotmentId'];
    	$employeeMasterId        = $classRow['employeeMasterId'];
    	$academicStartYear       = substr($classRow['academicStartYear'],0,4);
    	$academicEndYear         = substr($classRow['academicEndYear'],0,4);
    	$class                   = $classRow['class'];
    	$section                 = $classRow['section'];
    }
  }
  
  $p=0;
	$empArray = array();
	$selectEmp = "SELECT employeeMasterId,name
                  FROM employeemaster
	               WHERE userType = 'Teacher'";
	$selectEmpRes = mysql_query($selectEmp);
	while($empRow = mysql_fetch_array($selectEmpRes))
	{
	  $empArray['employeeMasterId'][$p] = $empRow['employeeMasterId'];
	  $empArray['name'][$p]             = $empRow['name'];
	  $p++;
	}
	
	$c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  $secArrOut[0] = 'A';
  $secArrOut[1] = 'B';
  $secArrOut[2] = 'C';
  $secArrOut[3] = 'D';
  
  include("./bottom.php");
  $smarty->assign('classTeacherAllotmentId',$classTeacherAllotmentId);
  $smarty->assign('employeeMasterId',$employeeMasterId);
  $smarty->assign('academicStartYear',$academicStartYear);
  $smarty->assign('academicEndYear',$academicEndYear);
  $smarty->assign('class',$class);
  $smarty->assign('isEdit',$isEdit);
  $smarty->assign('section',$section);
  $smarty->assign('cArray',$cArray);
  $smarty->assign('secArrOut',$secArrOut);
  $smarty->assign('classArray',$classArray);
  $smarty->assign('empArray',$empArray);
  $smarty->display('classTeacherAllotment.tpl');  
}
?>
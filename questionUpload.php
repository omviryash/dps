<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$filePath = '';
  $fileName = '';
  
  $qNo       = '';
  $class     = '';
  $subjectMasterId  = '';
  $question  = '';
  $option1   = '';
  $option2   = '';
  $option3   = '';
  $option4   = '';
  $answer    = '';
  
  if(isset($_POST['submitBtn']))
  {
    $uploaddir = dirname($_POST['filePath']);
  
    $uploadfile = $uploaddir . basename($_FILES['fileName']['name']);
    
    $target_path = 'data';
    $target_path = $target_path ."/". basename( $_FILES['fileName']['name']);
    $_FILES['fileName']['tmp_name']; // temp file
    
    $oldfile =  basename($_FILES['fileName']['name']);
  
    // getting the extention
  
    $pos = strpos($oldfile,".",0);
    $ext = trim(substr($oldfile,$pos+1,strlen($oldfile))," ");
    
    if(move_uploaded_file($_FILES['fileName']['tmp_name'], $target_path)) 
    {
    	$row = 0;
      $handle = fopen($target_path, "r");
      while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
      {
        if($row > 0)
        {
        	$qNo             = $data[2];
        	$class           = $data[0];
        	$subjectMasterId = $data[1];
        	$question        = $data[3];
        	$option1         = $data[4];
        	$option2         = $data[5];
        	$option3         = $data[6];
        	$option4         = $data[7];
        	$answer          = $data[8];
          
          $insertsubMaster = "INSERT INTO onlinetest(qNo,class,subjectMasterId,question,option1,
                                                     option2,option3,option4,answer)
        	                         VALUES ('".$qNo."','".$class."','".$subjectMasterId."','".$question."','".$option1."',
        	                                 '".$option2."','".$option3."','".$option4."','".$answer."')";
        	om_query($insertsubMaster);
  	    }
        $row++;
      }
    } 
    else
    {
      echo "There was an error uploading the file, please try again!";
    }
  }
}
?>
<BODY>
  <H1><CENTER><FONT color="red">Question Upload</FONT></CENTER></H1><HR>
<CENTER>
<FORM enctype="multipart/form-data" action="" name="question" method="POST"> 
  <a href="index.php">Home</a>
	<br /><br />
  <INPUT size="60" type="hidden" name="filePath"><BR>
  File Name:
  <INPUT name="fileName" type="file" onChange="document.question.filePath.value=document.question.fileName.value;">
  <INPUT type="submit" name="submitBtn" value="Submit File"> 
</FORM>
</CENTER>
<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$isEdit          = 0;
	$vehicleTypeId        = '';
	$vehicleNo = '';
	$registrationNo       = '';
	$capicity         = '';
	$vehicleMasterId = 0;
	
	$classArray      = array();
	
  if(isset($_POST['Submit']))
  {
  	$vehicleTypeId            = isset($_POST['vehicleTypeId']) ? $_POST['vehicleTypeId'] : 0;
  	$vehicleNo     = isset($_POST['vehicleNo']) ? $_POST['vehicleNo'] : 0;
  	$registrationNo           = isset($_POST['registrationNo']) ? $_POST['registrationNo'] : '';
  	$capicity             = isset($_POST['capicity']) ? $_POST['capicity'] : '';
  	$vehicleMasterId     = isset($_POST['vehicleMasterId']) ? $_POST['vehicleMasterId'] : 0;
  	
  	if($vehicleMasterId == 0)
  	{
  	  $insertClass = "INSERT INTO vehiclemaster (vehicleTypeId,vehicleNo,registrationNo,capicity)
  	                  VALUES (".$vehicleTypeId.",'".$vehicleNo."','".$registrationNo."',".$capicity.")";
  	  $insertClassRes = om_query($insertClass);
  	  if(!$insertClassRes)
  	  {
  	  	echo "Insert Fail";
  	  }
  	  else
  	  {
  	  	header("Location:vehicalMaster.php?done=1");
  	  }
  	}
  	else
  	{
  	  $isEdit = 1;
  	  $updateClass = "UPDATE vehiclemaster
  	                     SET vehicleTypeId   = ".$vehicleTypeId.",
  	                         vehicleNo = '".$vehicleNo."',
  	                         registrationNo    = '".$registrationNo."',
  	                         capicity = ".$capicity."
  	                   WHERE vehicleMasterId = ".$_REQUEST['vehicleMasterId'];
      $updateClassRes = om_query($updateClass);
      if(!$updateClassRes)
      {
      	echo "Update Fail";
      }
      else
      {
      	header("Location:vehicalMaster.php?done=1");
      }
  	}
  }
  
  $i = 0;
  $selectClass = "SELECT vehiclemaster.vehicleMasterId,vehiclemaster.vehicleTypeId,vehiclemaster.vehicleNo,vehiclemaster.registrationNo,
                         vehiclemaster.capicity
                    FROM vehiclemaster";
  $selectClassRes = mysql_query($selectClass);
  while($classRow = mysql_fetch_array($selectClassRes))
  {
  	$classArray[$i]['vehicleMasterId'] = $classRow['vehicleMasterId'];
  	$classArray[$i]['vehicleTypeId']        = $classRow['vehicleTypeId'];
  	$classArray[$i]['vehicleNo'] = $classRow['vehicleNo'];
  	$classArray[$i]['registrationNo']       = $classRow['registrationNo'];
  	$classArray[$i]['capicity']         = $classRow['capicity'];
  	$i++;
  }
  
  if(isset($_REQUEST['vehicleMasterId']) > 0)
  {
    $selectClass = "SELECT vehiclemaster.vehicleMasterId,vehiclemaster.vehicleTypeId,
                           vehiclemaster.vehicleNo,vehiclemaster.registrationNo,vehiclemaster.capicity
	                    FROM vehiclemaster
                     WHERE vehicleMasterId = ".$_REQUEST['vehicleMasterId']."";
    $selectClassRes = mysql_query($selectClass);
    if($classRow = mysql_fetch_array($selectClassRes))
    {
    	$vehicleMasterId = $classRow['vehicleMasterId'];
    	$vehicleTypeId        = $classRow['vehicleTypeId'];
    	$vehicleNo = $classRow['vehicleNo'];
    	$registrationNo       = $classRow['registrationNo'];
    	$capicity         = $classRow['capicity'];
    }
  }
  
  $k = 0;
  $selectClub = "SELECT vehicleTypeId,vehicleType
                   FROM vehicletype";
  $selectClubRes = mysql_query($selectClub);
  while($clubRow = mysql_fetch_array($selectClubRes))
  {
  	$typeArr['vehicleTypeId'][$k] = $clubRow['vehicleTypeId'];
  	$typeArr['vehicleType'][$k]   = $clubRow['vehicleType'];
  	$k++;
  }
  
  include("./bottom.php");
  $smarty->assign('vehicleMasterId',$vehicleMasterId);
  $smarty->assign('isEdit',$isEdit);
  $smarty->assign('classArray',$classArray);
  $smarty->assign('vehicleTypeId',$vehicleTypeId);
  $smarty->assign('vehicleNo',$vehicleNo);
  $smarty->assign('registrationNo',$registrationNo);
  $smarty->assign('capicity',$capicity);
  $smarty->assign('typeArr',$typeArr);
  $smarty->display('vehicalMaster.tpl'); 
}
?>
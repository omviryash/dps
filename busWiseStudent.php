<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$arrival = '';
	if(isset($_REQUEST['startYear']))
  {
	  $academicStartYear = $_REQUEST['startYear'];
	  $academicEndYear   = $_REQUEST['startYear'] + 1;
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y');
	  	$nextYear            = date('Y') + 1;
	  	$academicEndYear   = $nextYear;
		}
		else
		{
			$prevYear            = date('Y') - 1;
			$academicStartYear = $prevYear;
	  	$academicEndYear   = date('Y');
		}
	}
	
	$routeMasterId   = isset($_REQUEST['routeMasterId']) ? $_REQUEST['routeMasterId'] : 0;
	$vehicleMasterId = isset($_REQUEST['vehicleMasterId']) ? $_REQUEST['vehicleMasterId'] : 0;
	
	$selectQuery = "SELECT arrival
                    FROM routemaster
                   WHERE routeMasterId = ".$routeMasterId."";
  $selectQueryRes = mysql_query($selectQuery);
  if($queryRow = mysql_fetch_array($selectQueryRes))
  {
  	$arrival = $queryRow['arrival'];
  }
  
  if($arrival == 'Arrival')
  {
  	$busRouteQuery = "AND nominalroll.busArrival = '".$vehicleMasterId."'
                      AND nominalroll.routeArrival = '".$routeMasterId."'";
  }
  else
  {
  	$busRouteQuery = "AND nominalroll.busDeparture = '".$vehicleMasterId."'
                      AND nominalroll.routeDeparture = '".$routeMasterId."'";
  }
	$nominalArr = array();
  $i = 0;
  // $selectNominal = "SELECT nominalRollId,nominalroll.grNo,academicStartYear,academicEndYear,class,
  //                          section,rollNo,studentmaster.studentName,activated,studentmaster.studentMasterId,
  //                          studentmaster.fatherMobile,studentmaster.studentMasterId,currentAddress,busstopmaster.busStop
  //                     FROM nominalroll
  //                LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
  //                LEFT JOIN busstopmaster ON busstopmaster.busStopMasterId = nominalroll.busStopMasterId
  //                    WHERE 1 = 1
  //                      ".$busRouteQuery."
  //                      AND nominalroll.academicStartYear = '".$academicStartYear."-04-01'
  //                      AND nominalroll.academicEndYear = '".$academicEndYear."-03-31'
  //                      AND studentmaster.activated = 'Y'
  //                 ORDER BY nominalroll.rollNo";
	$selectNominal = "SELECT nominalRollId,nominalroll.grNo,academicStartYear,academicEndYear,class,
                           section,rollNo,studentmaster.studentName,activated,studentmaster.studentMasterId,
                           studentmaster.fatherMobile,studentmaster.studentMasterId,currentAddress,busstopmaster.busStop
                      FROM nominalroll
                 LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
                 LEFT JOIN busstopmaster ON busstopmaster.busStopMasterId = nominalroll.busStopMasterId
                     WHERE 1 = 1
                       ".$busRouteQuery."
                       AND nominalroll.academicStartYear = '".$academicStartYear."-04-01'
                       AND nominalroll.academicEndYear = '".$academicEndYear."-03-31'
                       AND studentmaster.activated = 'Y'
                  ORDER BY busstopmaster.busStop";
  
  $selectNominalRes = mysql_query($selectNominal);
  while($nominalRow = mysql_fetch_array($selectNominalRes))
  {
    $nominalArr[$i]['studentMasterId']    = $nominalRow['studentMasterId'];
    $nominalArr[$i]['nominalRollId']      = $nominalRow['nominalRollId'];
    $nominalArr[$i]['grNo']               = $nominalRow['grNo'];
    $nominalArr[$i]['currentAddress']     = $nominalRow['currentAddress'];
    $nominalArr[$i]['fatherMobile']       = $nominalRow['fatherMobile'];
    $nominalArr[$i]['academicYear']       = substr($nominalRow['academicStartYear'],0,4).'-'.substr($nominalRow['academicEndYear'],0,4);
    $nominalArr[$i]['class']              = $nominalRow['class'];
    $nominalArr[$i]['section']            = $nominalRow['section'];
    $nominalArr[$i]['rollNo']             = $nominalRow['rollNo'];
    $nominalArr[$i]['studentName']        = $nominalRow['studentName'];
    $nominalArr[$i]['activated']          = $nominalRow['activated'];
    $nominalArr[$i]['busStop']            = $nominalRow['busStop'];
    $i++;
  }
  
  $c=0;
	$cArray = array();
	$selectClass = "SELECT vehicleMasterId,vehicleNo
                    FROM vehiclemaster";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['vehicleMasterId'][$c] = $classRow['vehicleMasterId'];
	  $cArray['vehicleNo'][$c]       = $classRow['vehicleNo'];
	  $c++;
	}
	
  $r=0;
	$rArray = array();
	$selectClassR = "SELECT routeMasterId,routeName
                     FROM routemaster";
	$selectClassRRes = mysql_query($selectClassR);
	while($classRRow = mysql_fetch_array($selectClassRRes))
	{
	  $rArray['routeMasterId'][$r] = $classRRow['routeMasterId'];
	  $rArray['routeName'][$r]     = $classRRow['routeName'];
	  $r++;
	}
	
  include("./bottom.php");
  $smarty->assign('nominalArr',$nominalArr);
  $smarty->assign('cArray',$cArray);
  $smarty->assign('rArray',$rArray);
  $smarty->assign('routeMasterId',$routeMasterId);
  $smarty->assign('vehicleMasterId',$vehicleMasterId);
  $smarty->assign('academicStartYear',$academicStartYear);
  $smarty->assign('academicEndYear',$academicEndYear);
  $smarty->display('busWiseStudent.tpl');  
}
?>
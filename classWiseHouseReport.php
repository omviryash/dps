<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$countGanga1Total = 0;
	$countGanga2Total = 0;
	$countGanga3Total = 0;
	$countGanga4Total = 0;
	$countGanga5Total = 0;
	$strengthArr = array();
  $i = 0;
  //$startDate = date('Y').'-04-01';
  //$endDate   = '2015-03-31';
  //echo $Date = date('Y') + 1.'-03-31';
  if(isset($_REQUEST['startYear']))
  {
	  $academicStartYear = $_REQUEST['startYear'];
	  $academicEndYear   = $_REQUEST['startYear'] + 1;
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y');
	  	$nextYear            = date('Y') + 1;
	  	$academicEndYear   = $nextYear;
		}
		else
		{
			$prevYear            = date('Y') - 1;
			$academicStartYear = $prevYear;
	  	$academicEndYear   = date('Y');
		}
	}
  
  $selectEmp = "SELECT DISTINCT class,section
                  FROM classteacherallotment
             LEFT JOIN classmaster ON classmaster.className = classteacherallotment.class
  	          ORDER BY classmaster.priority,classteacherallotment.section";
  $selectEmpRes = mysql_query($selectEmp);
  while($empRow = mysql_fetch_array($selectEmpRes))
  {
  	$strengthArr[$i]['class']     = $empRow['class'];
  	$strengthArr[$i]['section']   = $empRow['section'];
  	
  	$selectNominalRollGanga1 = "SELECT studentmaster.activated,academicStartYear,academicEndYear
      	                        FROM nominalroll
      	                   LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
  	                           WHERE nominalroll.class = '".$empRow['class']."'
  	                             AND nominalroll.section = '".$empRow['section']."'
  	                             AND studentmaster.houseId = '1'
  	                             AND academicStartYear = '".$academicStartYear."-04-01'
  	                             AND academicEndYear = '".$academicEndYear."-03-31'
  	                             AND studentmaster.activated = 'Y'";
    $selectNominalRollGanga1Res = mysql_query($selectNominalRollGanga1);
    $countGanga1 = mysql_num_rows($selectNominalRollGanga1Res);
    $strengthArr[$i]['ganga'] = $countGanga1;
    $countGanga1Total += $countGanga1;
    
  	$selectNominalRollGanga2 = "SELECT studentmaster.activated,academicStartYear,academicEndYear
      	                        FROM nominalroll
      	                   LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
  	                           WHERE nominalroll.class = '".$empRow['class']."'
  	                             AND nominalroll.section = '".$empRow['section']."'
  	                             AND studentmaster.houseId = '3'
  	                             AND academicStartYear = '".$academicStartYear."-04-01'
  	                             AND academicEndYear = '".$academicEndYear."-03-31'
  	                             AND studentmaster.activated = 'Y'";
    $selectNominalRollGanga2Res = mysql_query($selectNominalRollGanga2);
    $countGanga2 = mysql_num_rows($selectNominalRollGanga2Res);
    $strengthArr[$i]['Yamuna'] = $countGanga2;
    $countGanga2Total += $countGanga2;
    
  	$selectNominalRollGanga3 = "SELECT studentmaster.activated,academicStartYear,academicEndYear
      	                        FROM nominalroll
      	                   LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
  	                           WHERE nominalroll.class = '".$empRow['class']."'
  	                             AND nominalroll.section = '".$empRow['section']."'
  	                             AND studentmaster.houseId = '4'
  	                             AND academicStartYear = '".$academicStartYear."-04-01'
  	                             AND academicEndYear = '".$academicEndYear."-03-31'
  	                             AND studentmaster.activated = 'Y'";
    $selectNominalRollGanga3Res = mysql_query($selectNominalRollGanga3);
    $countGanga3 = mysql_num_rows($selectNominalRollGanga3Res);
    $strengthArr[$i]['Jhelum'] = $countGanga3;
    $countGanga3Total += $countGanga3;
    
  	$selectNominalRollGanga4 = "SELECT studentmaster.activated,academicStartYear,academicEndYear
      	                        FROM nominalroll
      	                   LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
  	                           WHERE nominalroll.class = '".$empRow['class']."'
  	                             AND nominalroll.section = '".$empRow['section']."'
  	                             AND studentmaster.houseId = '2'
  	                             AND academicStartYear = '".$academicStartYear."-04-01'
  	                             AND academicEndYear = '".$academicEndYear."-03-31'
  	                             AND studentmaster.activated = 'Y'";
    $selectNominalRollGanga4Res = mysql_query($selectNominalRollGanga4);
    $countGanga4 = mysql_num_rows($selectNominalRollGanga4Res);
    $strengthArr[$i]['Narmada'] = $countGanga4;
    $countGanga4Total += $countGanga4;
    
  	$selectNominalRollGanga5 = "SELECT studentmaster.activated,academicStartYear,academicEndYear
      	                        FROM nominalroll
      	                   LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
  	                           WHERE nominalroll.class = '".$empRow['class']."'
  	                             AND nominalroll.section = '".$empRow['section']."'
  	                             AND studentmaster.houseId = '5'
  	                             AND academicStartYear = '".$academicStartYear."-04-01'
  	                             AND academicEndYear = '".$academicEndYear."-03-31'
  	                             AND studentmaster.activated = 'Y'";
    $selectNominalRollGanga5Res = mysql_query($selectNominalRollGanga5);
    $countGanga5 = mysql_num_rows($selectNominalRollGanga5Res);
    $strengthArr[$i]['NA'] = $countGanga5;
    $countGanga5Total += $countGanga5;
    
  	$i++;
  }
  include("./bottom.php");
  $smarty->assign('strengthArr',$strengthArr);
  $smarty->assign('academicStartYear',$academicStartYear);
  $smarty->assign('academicEndYear',$academicEndYear);
  $smarty->assign('countGanga1Total',$countGanga1Total);
  $smarty->assign('countGanga2Total',$countGanga2Total);
  $smarty->assign('countGanga3Total',$countGanga3Total);
  $smarty->assign('countGanga4Total',$countGanga4Total);
  $smarty->assign('countGanga5Total',$countGanga5Total);
  $smarty->display('classWiseHouseReport.tpl');  
}
?>
<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$classArray = array();
  $i = 0;
  $selectClass = "SELECT feedbackId,fromName,name,userType,message,DATE_FORMAT(dateTime, '%d-%m-%Y %H:%i:%s') AS dateTime
                    FROM feedback";
  $selectClassRes = mysql_query($selectClass);
  while($classRow = mysql_fetch_array($selectClassRes))
  {
  	$classArray[$i]['feedbackId'] = $classRow['feedbackId'];
  	$classArray[$i]['fromName']   = $classRow['fromName'];
  	$classArray[$i]['name']       = $classRow['name'];
  	$classArray[$i]['userType']   = $classRow['userType'];
  	$classArray[$i]['message']    = $classRow['message'];
  	$classArray[$i]['dateTime']   = $classRow['dateTime'];
  	$i++;
  }
  
  include("./bottom.php");
  $smarty->assign('classArray',$classArray);
  $smarty->display('feedbackList.tpl');  
}
?>
<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$filePath = '';
  $fileName = '';
  
  $academicStartYear = '';
  $academicEndYear   = '';
  $grNo              = '';
  $rollNo            = '';
  $class             = '';
  $section           = '';
  $subjectMasterId   = '';
  $fa1               = '';
  $fa2               = '';
  $sa1               = '';
  $fa3               = '';
  $fa4               = '';
  $sa2               = '';
  
  if(isset($_POST['submitBtn']))
  {
    $uploaddir = dirname($_POST['filePath']);
  
    $uploadfile = $uploaddir . basename($_FILES['fileName']['name']);
    
    $target_path = 'data';
    $target_path = $target_path ."/". basename( $_FILES['fileName']['name']);
    $_FILES['fileName']['tmp_name']; // temp file
    
    $oldfile =  basename($_FILES['fileName']['name']);
  
    // getting the extention
  
    $pos = strpos($oldfile,".",0);
    $ext = trim(substr($oldfile,$pos+1,strlen($oldfile))," ");
    
    if(move_uploaded_file($_FILES['fileName']['tmp_name'], $target_path)) 
    {
    	$row = 0;
      $handle = fopen($target_path, "r");
      while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
      {
        if($row > 0)
        {
        	$academicStartYear = '2013-04-01';
				  $academicEndYear   = '2014-03-31';
				  $grNo              = $data[6];
				  $rollNo            = $data[2];
				  $class             = '6';
				  $section           = 'A';
				  $subjectMasterId   = $data[7];
				  if($subjectMasterId == 2)
				  {
					  $fa1               = $data[20];
					  $fa2               = $data[27];
					  $sa1               = $data[36];
					  $fa3               = $data[43];
					  $fa4               = $data[50];
					  $sa2               = $data[59];
					}
          if($subjectMasterId == 3)
				  {
					  $fa1               = $data[21];
					  $fa2               = $data[28];
					  $sa1               = $data[37];
					  $fa3               = $data[44];
					  $fa4               = $data[51];
					  $sa2               = $data[60];
					}
          if($subjectMasterId == 1)
				  {
					  $fa1               = $data[22];
					  $fa2               = $data[29];
					  $sa1               = $data[38];
					  $fa3               = $data[45];
					  $fa4               = $data[52];
					  $sa2               = $data[61];
					}
          if($subjectMasterId == 120)
				  {
					  $fa1               = $data[23];
					  $fa2               = $data[30];
					  $sa1               = $data[39];
					  $fa3               = $data[46];
					  $fa4               = $data[53];
					  $sa2               = $data[62];
					}
          if($subjectMasterId == 167)
				  {
					  $fa1               = $data[24];
					  $fa2               = $data[31];
					  $sa1               = $data[40];
					  $fa3               = $data[47];
					  $fa4               = $data[54];
					  $sa2               = $data[63];
					}
          if($subjectMasterId == 168)
				  {
					  $fa1               = $data[25];
					  $fa2               = $data[32];
					  $sa1               = $data[41];
					  $fa3               = $data[48];
					  $fa4               = $data[55];
					  $sa2               = $data[64];
					}
          
          
          $insertMaster = "INSERT INTO exammarksper (academicStartYear,academicEndYear,grNo,rollNo,
                                                     class,section,subjectMasterId,fa1,fa2,sa1,fa3,fa4,sa2)
        	                      VALUES ('".$academicStartYear."','".$academicEndYear."',".$grNo.",".$rollNo.",
        	                              '".$class."','".$section."',".$subjectMasterId.",".$fa1.",".$fa2.",".$sa1.",
        	                              ".$fa3.",".$fa4.",0)";
        	om_query($insertMaster);
  	    }
        $row++;
      }
    } 
    else
    {
      echo "There was an error uploading the file, please try again!";
    }
  }
}
?>
<BODY>
  <H1><CENTER><FONT color="red">Marks Upload</FONT></CENTER></H1><HR>
<CENTER>
<FORM enctype="multipart/form-data" action="" name="marksUpload" method="POST"> 
  <a href="index.php">Home</a>
	<br /><br />
  <INPUT size="60" type="hidden" name="filePath"><BR>
  File Name:
  <INPUT name="fileName" type="file" onChange="document.marksUpload.filePath.value=document.marksUpload.fileName.value;">
  <INPUT type="submit" name="submitBtn" value="Submit File"> 
</FORM>
</CENTER>
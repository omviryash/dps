<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$classArray = array();
  $i = 0;
  $selectClass = "SELECT busstopmaster.busStopMasterId,busstopmaster.busTime,busstopmaster.localArea,
                         busstopmaster.busStop,busstopmaster.distance,vehiclemaster.vehicleNo,routemaster.routeName
                    FROM busstopmaster
               LEFT JOIN vehiclemaster ON vehiclemaster.vehicleMasterId = busstopmaster.vehicleMasterId
               LEFT JOIN routemaster ON routemaster.routeMasterId = busstopmaster.routeMasterId";
  $selectClassRes = mysql_query($selectClass);
  while($classRow = mysql_fetch_array($selectClassRes))
  {
  	$classArray[$i]['busStopMasterId'] = $classRow['busStopMasterId'];
  	$classArray[$i]['vehicleNo']       = $classRow['vehicleNo'];
  	$classArray[$i]['routeName']       = $classRow['routeName'];
  	$classArray[$i]['busTime']         = $classRow['busTime'];
  	$classArray[$i]['localArea']       = $classRow['localArea'];
  	$classArray[$i]['busStop']         = $classRow['busStop'];
  	$classArray[$i]['distance']        = $classRow['distance'];
  	$i++;
  }
  
  include("./bottom.php");
  $smarty->assign('classArray',$classArray);
  $smarty->display('busStopMasterList.tpl');  
}
?>
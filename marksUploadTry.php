<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$filePath = '';
  $fileName = '';
  
  $academicStartYear = '';
  $academicEndYear   = '';
  $grNo              = '';
  $class             = '';
  $section           = '';
  
  $sub1Fa1 = 0;
	$sub1Fa2 = 0;
	$sub1Sa1 = 0;
	$sub1Fa3 = 0;
	$sub1Fa4 = 0;
	$sub1Sa2 = 0;

	$sub2Fa1 = 0;
  $sub2Fa2 = 0;
  $sub2Sa1 = 0;
  $sub2Fa3 = 0;
  $sub2Fa4 = 0;
  $sub2Sa2 = 0;

	$sub3Fa1 = 0;
  $sub3Fa2 = 0;
  $sub3Sa1 = 0;
  $sub3Fa3 = 0;
  $sub3Fa4 = 0;
  $sub3Sa2 = 0;

	$sub4Fa1 = 0;
  $sub4Fa2 = 0;
  $sub4Sa1 = 0;
  $sub4Fa3 = 0;
  $sub4Fa4 = 0;
  $sub4Sa2 = 0;

	$sub5Fa1 = 0;
  $sub5Fa2 = 0;
  $sub5Sa1 = 0;
  $sub5Fa3 = 0;
  $sub5Fa4 = 0;
  $sub5Sa2 = 0;

	$sub6Fa1 = 0;
  $sub6Fa2 = 0;
  $sub6Sa1 = 0;
  $sub6Fa3 = 0;
  $sub6Fa4 = 0;
  $sub6Sa2 = 0;
  
  $co2A1 = 0;
  $co2A2 = 0;
  $co2A3 = 0;
  $co2B1 = 0;
  $co2C1 = 0;
  $co2D1 = 0;
  $co2D2 = 0;
  $co2D3 = 0;
  $co2D4 = 0;
  $co3A1 = 0;
  $co3A2 = 0;
  $co3B1 = 0;
  $co3B2 = 0;
  
  
  if(isset($_POST['submitBtn']))
  {
    $uploaddir = dirname($_POST['filePath']);
  
    $uploadfile = $uploaddir . basename($_FILES['fileName']['name']);
    
    $target_path = 'data';
    $target_path = $target_path ."/". basename( $_FILES['fileName']['name']);
    $_FILES['fileName']['tmp_name']; // temp file
    
    $oldfile =  basename($_FILES['fileName']['name']);
  
    // getting the extention
  
    $pos = strpos($oldfile,".",0);
    $ext = trim(substr($oldfile,$pos+1,strlen($oldfile))," ");
    
    if(move_uploaded_file($_FILES['fileName']['tmp_name'], $target_path)) 
    {
    	$row = 0;
      $handle = fopen($target_path, "r");
      while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
      {
        if($row > 0)
        {
        	$academicStartYear = '2013-04-01';
				  $academicEndYear   = '2014-03-31';
				  $grNo              = $data[6];
				  $class             = substr($data[1],0,1);
				  $section           = substr($data[1],2,1);
				  
				  $sub1Fa1 = $data[20] != '' ? $data[20] : 0;
					$sub1Fa2 = $data[27] != '' ? $data[27] : 0;
					$sub1Sa1 = $data[36] != '' ? $data[36] : 0;
					$sub1Fa3 = $data[43] != '' ? $data[43] : 0;
					$sub1Fa4 = $data[50] != '' ? $data[50] : 0;
					$sub1Sa2 = $data[59] != '' ? $data[59] : 0;
					
					$sub2Fa1 = $data[21] != '' ? $data[21] : 0;
				  $sub2Fa2 = $data[28] != '' ? $data[28] : 0;
				  $sub2Sa1 = $data[37] != '' ? $data[37] : 0;
				  $sub2Fa3 = $data[44] != '' ? $data[44] : 0;
				  $sub2Fa4 = $data[51] != '' ? $data[51] : 0;
				  $sub2Sa2 = $data[60] != '' ? $data[60] : 0;
					
					$sub3Fa1 = $data[22] != '' ? $data[22] : 0;
				  $sub3Fa2 = $data[29] != '' ? $data[29] : 0;
				  $sub3Sa1 = $data[38] != '' ? $data[38] : 0;
				  $sub3Fa3 = $data[45] != '' ? $data[45] : 0;
				  $sub3Fa4 = $data[52] != '' ? $data[52] : 0;
				  $sub3Sa2 = $data[61] != '' ? $data[61] : 0;
					
					$sub4Fa1 = $data[23] != '' ? $data[23] : 0;
				  $sub4Fa2 = $data[30] != '' ? $data[30] : 0;
				  $sub4Sa1 = $data[39] != '' ? $data[39] : 0;
				  $sub4Fa3 = $data[46] != '' ? $data[46] : 0;
				  $sub4Fa4 = $data[53] != '' ? $data[53] : 0;
				  $sub4Sa2 = $data[62] != '' ? $data[62] : 0;
					
					$sub5Fa1 = $data[24] != '' ? $data[24] : 0;
				  $sub5Fa2 = $data[31] != '' ? $data[31] : 0;
				  $sub5Sa1 = $data[40] != '' ? $data[40] : 0;
				  $sub5Fa3 = $data[47] != '' ? $data[47] : 0;
				  $sub5Fa4 = $data[54] != '' ? $data[54] : 0;
				  $sub5Sa2 = $data[63] != '' ? $data[63] : 0;
					
					$sub6Fa1 = $data[25] != '' ? $data[25] : 0;
				  $sub6Fa2 = $data[32] != '' ? $data[32] : 0;
				  $sub6Sa1 = $data[41] != '' ? $data[41] : 0;
				  $sub6Fa3 = $data[48] != '' ? $data[48] : 0;
				  $sub6Fa4 = $data[55] != '' ? $data[55] : 0;
				  $sub6Sa2 = $data[64] != '' ? $data[64] : 0;
          
          $co2A1 = $data[80] != '' ? $data[80] : 0;
				  $co2A2 = $data[81] != '' ? $data[81] : 0;
				  $co2A3 = $data[82] != '' ? $data[82] : 0;
				  $co2B1 = $data[83] != '' ? $data[83] : 0;
				  $co2C1 = $data[84] != '' ? $data[84] : 0;
				  $co2D1 = $data[85] != '' ? $data[85] : 0;
				  $co2D2 = $data[86] != '' ? $data[86] : 0;
				  $co2D3 = $data[87] != '' ? $data[87] : 0;
				  $co2D4 = $data[88] != '' ? $data[88] : 0;
				  $co3A1 = $data[89] != '' ? $data[89] : 0;
				  $co3A2 = $data[90] != '' ? $data[90] : 0;
				  $co3B1 = $data[91] != '' ? $data[91] : 0;
				  $co3B2 = $data[92] != '' ? $data[92] : 0;
				  
				  $goals            = $data[94] != '' ? $data[94] : '';
				  $intHobbies       = $data[95] != '' ? $data[95] : '';
				  $strengths        = $data[96] != '' ? $data[96] : '';
				  $responsibilities = $data[97] != '' ? $data[97] : '';
				  
				  $visionL = $data[15] != '' ? $data[15] : 0;
				  $visionR = $data[16] != '' ? $data[16] : 0;
				  
				  $dentalHygiene = $data[19] != '' ? $data[19] : '';
				  
				  $height = $data[12] != '' ? $data[12] : '';
				  $weight = $data[13] != '' ? $data[13] : '';

  
          $insertMaster = "INSERT INTO exammarkspertry (academicStartYear,academicEndYear,grNo,class,section,
                                                        sub1Fa1,sub1Fa2,sub1Sa1,sub1Fa3,sub1Fa4,sub1Sa2,
                                                        sub2Fa1,sub2Fa2,sub2Sa1,sub2Fa3,sub2Fa4,sub2Sa2,
                                                        sub3Fa1,sub3Fa2,sub3Sa1,sub3Fa3,sub3Fa4,sub3Sa2,
                                                        sub4Fa1,sub4Fa2,sub4Sa1,sub4Fa3,sub4Fa4,sub4Sa2,
                                                        sub5Fa1,sub5Fa2,sub5Sa1,sub5Fa3,sub5Fa4,sub5Sa2,
                                                        sub6Fa1,sub6Fa2,sub6Sa1,sub6Fa3,sub6Fa4,sub6Sa2,
                                                        2A1,2A2,2A3,2B1,2C1,2D1,2D2,2D3,2D4,3A1,3A2,3B1,3B2,
                                                        goals,intHobbies,strengths,responsibilities,dentalHygiene,
                                                        visionL,visionR,height,weight)
        	                      VALUES ('".$academicStartYear."','".$academicEndYear."',".$grNo.",'".$class."','".$section."',
        	                              ".$sub1Fa1.",".$sub1Fa2.",".$sub1Sa1.",".$sub1Fa3.",".$sub1Fa4.",".$sub1Sa2.",
        	                              ".$sub2Fa1.",".$sub2Fa2.",".$sub2Sa1.",".$sub2Fa3.",".$sub2Fa4.",".$sub2Sa2.",
        	                              ".$sub3Fa1.",".$sub3Fa2.",".$sub3Sa1.",".$sub3Fa3.",".$sub3Fa4.",".$sub3Sa2.",
        	                              ".$sub4Fa1.",".$sub4Fa2.",".$sub4Sa1.",".$sub4Fa3.",".$sub4Fa4.",".$sub4Sa2.",
        	                              ".$sub5Fa1.",".$sub5Fa2.",".$sub5Sa1.",".$sub5Fa3.",".$sub5Fa4.",".$sub5Sa2.",
        	                              ".$sub6Fa1.",".$sub6Fa2.",".$sub6Sa1.",".$sub6Fa3.",".$sub6Fa4.",".$sub6Sa2.",
        	                              ".$co2A1.",".$co2A2.",".$co2A3.",".$co2B1.",".$co2C1.",".$co2D1.",
        	                              ".$co2D2.",".$co2D3.",".$co2D4.",".$co3A1.",".$co3A2.",".$co3B1.",".$co3B2.",
        	                              '".$goals."','".$intHobbies."','".$strengths."','".$responsibilities."',
        	                              '".$dentalHygiene."','".$visionL."','".$visionR."','".$height."','".$weight."')";
        	om_query($insertMaster);
  	    }
        $row++;
      }
    } 
    else
    {
      echo "There was an error uploading the file, please try again!";
    }
  }
}
?>
<BODY>
  <H1><CENTER><FONT color="red">Marks Upload All</FONT></CENTER></H1><HR>
<CENTER>
<FORM enctype="multipart/form-data" action="" name="marksUploadTry" method="POST"> 
  <a href="index.php">Home</a>
	<br /><br />
  <INPUT size="60" type="hidden" name="filePath"><BR>
  File Name:
  <INPUT name="fileName" type="file" onChange="document.marksUploadTry.filePath.value=document.marksUploadTry.fileName.value;">
  <INPUT type="submit" name="submitBtn" value="Submit File"> 
</FORM>
</CENTER>
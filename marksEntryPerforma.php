<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$class      = '';
  $count      = 0;
  $today      = date('Y-m-d');
	$stdArray   = array();
	$section     = '';
	
	if(isset($_REQUEST['startYear']))
  {
  	$academicStartYear = $_REQUEST['startYear']."-04-01";
  	$nextYear          = $_REQUEST['startYear'] + 1;
  	$academicEndYear   = $nextYear."-03-31";
  	$academicStartYearSelected = $_REQUEST['startYear'];
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y')."-04-01";
	  	$nextYear          = date('Y') + 1;
	  	$academicEndYear   = $nextYear."-03-31";
	  	$academicStartYearSelected = date('Y');
		}
		else
		{
			$prevYear          = date('Y') - 1;
			$academicStartYear = $prevYear."-04-01";
	  	$academicEndYear   = date('Y')."-03-31";
	  	$academicStartYearSelected = $prevYear;
		}
	}
	
	$subjectAltId = isset($_REQUEST['subjectAltId']) && $_REQUEST['subjectAltId'] != '' ? $_REQUEST['subjectAltId'] : '0_0_0';
	
	$subjectAltIdExp = explode("_",$subjectAltId);
	$subjectAltIdExp[0];
	$subjectAltIdExp[1];
	$subjectAltIdExp[2];
	
	if(isset($_REQUEST['class']))
	{
	  $class           = isset($_REQUEST['class']) && $_REQUEST['class'] != '' ? $_REQUEST['class'] : 0;
		$section         = isset($_REQUEST['section']) && $_REQUEST['section'] != '' ? $_REQUEST['section'] : 0;
		$subjectMasterId = isset($_REQUEST['subjectMasterId']) && $_REQUEST['subjectMasterId'] != '' ? $_REQUEST['subjectMasterId'] : 0;
	}
	else
	{
		$class           = $subjectAltIdExp[0];
		$section         = $subjectAltIdExp[1];
		$subjectMasterId = $subjectAltIdExp[2];
	}
	
	if(isset($_POST['submitTaken']))
  {
  	$loopCount = 0;
  	while($loopCount < count($_POST['fa1']))
  	{
  		$grNoSubmit      = ($_POST['grNo'][$loopCount] != '') ? $_POST['grNo'][$loopCount] : 0;
  		$subjectMasterId = ($_POST['subjectMasterId'][$loopCount] != '') ? $_POST['subjectMasterId'][$loopCount] : 0;
  		$class           = ($_POST['class'][$loopCount] != '') ? $_POST['class'][$loopCount] : 0;
  		$section         = ($_POST['section'][$loopCount] != '') ? $_POST['section'][$loopCount] : 0;
  		$fa1             = isset($_POST['fa1'][$loopCount]) && ($_POST['fa1'][$loopCount] != '') ? $_POST['fa1'][$loopCount] : 'null';
  		$fa2             = isset($_POST['fa2'][$loopCount]) && ($_POST['fa2'][$loopCount] != '') ? $_POST['fa2'][$loopCount] : 'null';
  		$sa1             = isset($_POST['sa1'][$loopCount]) && ($_POST['sa1'][$loopCount] != '') ? $_POST['sa1'][$loopCount] : 'null';
  		$fa3             = isset($_POST['fa3'][$loopCount]) && ($_POST['fa3'][$loopCount] != '') ? $_POST['fa3'][$loopCount] : 'null';
  		$fa4             = isset($_POST['fa4'][$loopCount]) && ($_POST['fa4'][$loopCount] != '') ? $_POST['fa4'][$loopCount] : 'null';
  		$sa2             = isset($_POST['sa2'][$loopCount]) && ($_POST['sa2'][$loopCount] != '') ? $_POST['sa2'][$loopCount] : 'null';
  		if($_POST['fa1'][$loopCount] != '' && $_POST['grNo'][$loopCount] > 0)
  		{
  			$updateClass = "UPDATE exammarksper
		  	                   SET fa1 = ".$fa1.",
		  	                       fa2 = ".$fa2.",
		  	                       sa1 = ".$sa1.",
		  	                       fa3 = ".$fa3.",
		  	                       fa4 = ".$fa4.",
		  	                       sa2 = ".$sa2."
		  	                 WHERE grNo = ".$grNoSubmit."
		  	                   AND subjectMasterId = ".$subjectMasterId."
		  	                   AND class = '".$class."'
		  	                   AND section = '".$section."'
		  	                   AND academicStartYear = '".$academicStartYear."'
		  	                   AND academicEndYear = '".$academicEndYear."'";
	  	  $updateClassRes = om_query($updateClass);
	  	  if(!$updateClassRes)
	  	  {
	  	  	echo "Update Fail";
	  	  }
	  	  else
	  	  {
	  	  	//header("Location:marksEntryPerforma.php?subjectAltId=".$subjectAltId."&go=go");
	  	  }
  		}
  		$loopCount++;
  	}
  }
	
	$i = 0;
  $selectStd = "SELECT exammarksper.grNo,exammarksper.rollNo,studentmaster.gender,studentmaster.studentName,exammarksper.fa1,
                       exammarksper.fa2,exammarksper.sa1,exammarksper.fa3,exammarksper.fa4,exammarksper.sa2,
                       subjectmaster.subjectName,exammarksper.subjectMasterId,exammarksper.class,exammarksper.section
                  FROM exammarksper
             LEFT JOIN studentmaster ON studentmaster.grNo = exammarksper.grNo
             LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = exammarksper.subjectMasterId
                 WHERE exammarksper.class = '".$class."'
                   AND exammarksper.section = '".$section."'
                   AND exammarksper.subjectMasterId = ".$subjectMasterId."
                   AND exammarksper.academicStartYear = '".$academicStartYear."'
                   AND exammarksper.academicEndYear = '".$academicEndYear."'
                   AND studentmaster.activated = 'Y'
              ORDER BY exammarksper.rollNo";
  $selectStdRes = mysql_query($selectStd);
  $count = mysql_num_rows($selectStdRes);
  while($stdRow = mysql_fetch_array($selectStdRes))
  {
  	$stdArray[$i]['class']           = $stdRow['class'];
  	$stdArray[$i]['section']         = $stdRow['section'];
  	$stdArray[$i]['subjectMasterId'] = $stdRow['subjectMasterId'];
  	$stdArray[$i]['grNo']           = $stdRow['grNo'];
  	$stdArray[$i]['rollNo']         = $stdRow['rollNo'];
  	$stdArray[$i]['gender']         = $stdRow['gender'];
  	$stdArray[$i]['studentName']    = $stdRow['studentName'];
  	$stdArray[$i]['subjectName']    = $stdRow['subjectName'];
  	$stdArray[$i]['fa1']            = $stdRow['fa1'];
  	$stdArray[$i]['fa2']            = $stdRow['fa2'];
  	$stdArray[$i]['sa1']            = $stdRow['sa1'];
  	$stdArray[$i]['fa3']            = $stdRow['fa3'];
  	$stdArray[$i]['fa4']            = $stdRow['fa4'];
  	$stdArray[$i]['sa2']            = $stdRow['sa2'];
  	$i++;
  }
  
	$scdArray = array();
  $f = 0;
	$selectId = "SELECT employeeMasterId
                 FROM employeemaster
                WHERE loginId = '".$_SESSION['s_activName']."'";
  $selectIdRes = mysql_query($selectId);
  if($subRow = mysql_fetch_array($selectIdRes))
  {
  	$selectSub = "SELECT subjectteacherallotment.class,subjectteacherallotment.section,subjectteacherallotment.subjectMasterId,subjectmaster.subjectName
                    FROM subjectteacherallotment
               LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = subjectteacherallotment.subjectMasterId
                   WHERE subjectteacherallotment.employeeMasterId = '".$subRow['employeeMasterId']."'
                     AND subjectteacherallotment.academicStartYear = '".$academicStartYear."'
                     AND subjectteacherallotment.academicEndYear = '".$academicEndYear."'
                     AND subjectteacherallotment.class > 5
                     AND subjectteacherallotment.class <= 10";
	  $selectSubRes = mysql_query($selectSub);
	  while($subRow = mysql_fetch_array($selectSubRes))
	  {
	  	$classTeacherSubjectClass    = $subRow['class'];
	  	$classTeacherSubjectsection  = $subRow['section'];
	  	$classTeachersubjectMasterId = $subRow['subjectMasterId'];
		  
	  	$scdArray['subjectAltId'][$f] = $subRow['class'].'_'.$subRow['section'].'_'.$subRow['subjectMasterId'];
	  	$scdArray['subjectDtl'][$f]   = $subRow['class'].'_'.$subRow['section'].'_'.$subRow['subjectName'];
	  	$f++;
	  }
  }
  
  $c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster
                   WHERE className > 5
                     AND className <= 10";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
	$k = 0;
  $selectClub = "SELECT subjectMasterId,subjectName
                   FROM subjectmaster
                  WHERE subjectType = 'Main'";
  $selectClubRes = mysql_query($selectClub);
  while($clubRow = mysql_fetch_array($selectClubRes))
  {
  	$clubArr['subjectMasterId'][$k]   = $clubRow['subjectMasterId'];
  	$clubArr['subjectName'][$k]       = $clubRow['subjectName'];
  	$k++;
  }
  
  $secArrOut[0] = 'A';
  $secArrOut[1] = 'B';
  $secArrOut[2] = 'C';
  $secArrOut[3] = 'D';
  
  include("./bottom.php");
  $smarty->assign('academicStartYearSelected',$academicStartYearSelected);
  $smarty->assign('scdArray',$scdArray);
  $smarty->assign('secArrOut',$secArrOut);
  $smarty->assign('class',$class);
  $smarty->assign('section',$section);
  $smarty->assign('stdArray',$stdArray);
  $smarty->assign('count',$count);
  $smarty->assign('cArray',$cArray);
  $smarty->assign('clubArr',$clubArr);
  $smarty->assign('subjectMasterId',$subjectMasterId);
  $smarty->assign('subjectAltId',$subjectAltId);
  $smarty->assign('subjectMasterId',$subjectMasterId);
  $smarty->display('marksEntryPerforma.tpl');  
}
?>
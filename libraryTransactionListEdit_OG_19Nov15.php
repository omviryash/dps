<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$today = date('Y-m-d');
	if(isset($_REQUEST['attendenceYear']))
	{
	  $attendDate = $_REQUEST['attendenceYear']."-".$_REQUEST['attendenceMonth']."-".$_REQUEST['attendenceDay'];
	}
	else
	{
		$attendDate = $today;
	}
	
  $nameStaff = isset($_REQUEST['nameStaff']) ? $_REQUEST['nameStaff'] : '';
	$staff = isset($_REQUEST['staff']) ? $_REQUEST['staff'] : 'Student';
	$bookAccessionNo = isset($_REQUEST['bookAccessionNo']) ? $_REQUEST['bookAccessionNo'] : 0;
	$class   = isset($_REQUEST['class']) ? $_REQUEST['class'] : 0;
	$section = isset($_REQUEST['classSection']) ? $_REQUEST['classSection'] : 0;
	
	if($nameStaff != '')
	{
		$newQueryNameStaff = "AND employeemaster.name= '".$nameStaff."'";
	}
	else
	{
		$newQueryNameStaff = '';
	}
	
	if($staff != 'Staff')
	{
		$newQueryStaff = "AND librarytransaction.grNo > 0
		                  AND nominalroll.academicStartYear < '".$attendDate."'
                      AND nominalroll.academicEndYear > '".$attendDate."'";
	}
	else
	{
		$newQueryStaff = "AND librarytransaction.employeeCode > 0";
	}
	
	if($bookAccessionNo != '')
	{
		$newQueryBook = "AND librarytransaction.bookAccessionNo = '".$bookAccessionNo."'";
	}
	else
	{
		$newQueryBook = '';
	}
	
	if($class != '')
	{
		$newQuery = "AND nominalroll.class = '".$class."'
	               AND nominalroll.section = '".$section."'";
	}
	else
	{
		$newQuery = '';
	}
	
	if(isset($_REQUEST['submitNew']))
	{
		$newQuery1 = "AND librarytransaction.issueDate = '".$attendDate."'";
	}
	else
	{
		$newQuery1 = '';
	}
	$libraryTransactionId = 0;
	$attendenceArr    = array();
	$i = 0;
  $selectAttendM= "SELECT librarytransaction.libraryTransactionId,librarytransaction.grNo,librarytransaction.bookAccessionNo,
                          librarytransaction.returnBook,librarytransaction.employeeCode,librarytransaction.checktype,
                          DATE_FORMAT(librarytransaction.issueDate, '%d-%m-%Y') AS issueDate,
                          DATE_FORMAT(librarytransaction.returnableDate, '%d-%m-%Y') AS returnableDate,librarytransaction.fine,
                          studentmaster.studentName,bookmaster.bookTitle,employeemaster.name,librarytransaction.finePay,
                          nominalroll.class,nominalroll.section,nominalroll.academicStartYear,nominalroll.academicEndYear
                     FROM librarytransaction
                LEFT JOIN studentmaster ON studentmaster.grNo = librarytransaction.grNo
                LEFT JOIN nominalroll ON nominalroll.grNo = studentmaster.grNo
                LEFT JOIN bookmaster ON bookmaster.bookAccessionNo = librarytransaction.bookAccessionNo
                LEFT JOIN employeemaster ON employeemaster.employeeCode = librarytransaction.employeeCode
                    WHERE 1 = 1
                      ".$newQuery."
                      ".$newQuery1."
                      ".$newQueryStaff."
                      ".$newQueryNameStaff."
                      ".$newQueryBook."
                 ORDER BY librarytransaction.libraryTransactionId DESC";
  $selectAttendMRes = mysql_query($selectAttendM);
  while($maRow = mysql_fetch_array($selectAttendMRes))
  {
  	$attendenceArr[$i]['libraryTransactionId'] = $maRow['libraryTransactionId'];
  	$attendenceArr[$i]['issueDate']            = $maRow['issueDate'];
  	$attendenceArr[$i]['checktype']            = $maRow['checktype'];
  	$attendenceArr[$i]['employeeCode']         = $maRow['employeeCode'];
  	$attendenceArr[$i]['name']                 = $maRow['name'];
  	$attendenceArr[$i]['grNo']                 = $maRow['grNo'];
  	$attendenceArr[$i]['studentName']          = $maRow['studentName'];
  	$attendenceArr[$i]['class']                = $maRow['class'];
  	$attendenceArr[$i]['section']              = $maRow['section'];
  	$attendenceArr[$i]['bookAccessionNo']      = $maRow['bookAccessionNo'];
  	$attendenceArr[$i]['bookTitle']            = $maRow['bookTitle'];
  	$attendenceArr[$i]['returnableDate']       = $maRow['returnableDate'];
  	$attendenceArr[$i]['returnBook']           = $maRow['returnBook'];
  	
  	$ts1 = strtotime($today);
		$ts2 = strtotime(date('Y-m-d',strtotime('+7 days',strtotime($maRow['issueDate']))));
		
		$seconds_diff = $ts1 - $ts2;
		
		$fine = floor($seconds_diff/3600/24);
		
		$newFine = $fine/7;
		
		$newFineAll = explode('.',$newFine);
		$newFineAll[0];
		if($newFineAll[0] > 0 && $maRow['returnBook'] == 'No')
		{
		  $attendenceArr[$i]['fine'] = $newFineAll[0] * 7;
		}
		else
		{
			$attendenceArr[$i]['fine'] = $maRow['fine'];
		}
		if($attendenceArr[$i]['fine'] == 0)
		{
		  $attendenceArr[$i]['finePay'] = '-';
		}
		elseif($attendenceArr[$i]['returnBook'] == 'No')
		{
		  $attendenceArr[$i]['finePay'] = 'Pending';
		}
		else
		{
			$attendenceArr[$i]['finePay'] = $maRow['finePay'];
		}
		
  	$i++;
  }
  
  if(isset($_POST['submitTaken']))
  {
  	$loopCount = 0;
  	while($loopCount < count($_POST['libraryTransactionId']))
  	{
  		$libraryTransactionId = ($_POST['libraryTransactionId'][$loopCount] != '') ? $_POST['libraryTransactionId'][$loopCount] : 0;
  		$grNo                 = ($_POST['grNo'][$loopCount] != '') ? $_POST['grNo'][$loopCount] : 0;
  		$bookAccessionNo      = ($_POST['bookAccessionNo'][$loopCount] != '') ? $_POST['bookAccessionNo'][$loopCount] : 0;
  		$returnableDate       = $_REQUEST['returnableDateYear']."-".$_REQUEST['returnableDateMonth']."-".$_REQUEST['returnableDateDay'];
  		$returnBook           = ($_POST['returnBook'][$loopCount] != '') ? $_POST['returnBook'][$loopCount] : 'No';
  		
  		
  		if($_POST['libraryTransactionId'][$loopCount] != '' && $_POST['libraryTransactionId'][$loopCount] > 0)
  		{
				$updateClass = "UPDATE librarytransaction
		  	                   SET grNo = ".$grNo.",
		  	                       bookAccessionNo = ".$bookAccessionNo.",
		  	                       returnableDate = '".$returnableDate."',
		  	                       returnBook = '".$returnBook."'
		  	                 WHERE libraryTransactionId = ".$libraryTransactionId."";
			  $updateClassRes = om_query($updateClass);
			  if(!$updateClassRes)
			  {
			  	echo "Update Fail";
			  }
			  else
			  {
			  	header("Location:libraryTransactionListEdit.php?attendDate=".$attendDate."");
			  }
			}
			$loopCount++;
		}
  }
  
  $c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  $b=0;
	$bArray = array();
	$selectClassb = "SELECT bookAccessionNo,bookTitle
                     FROM bookmaster";
	$selectClassbRes = mysql_query($selectClassb);
	while($classbRow = mysql_fetch_array($selectClassbRes))
	{
	  $bArray['bookAccessionNo'][$b] = $classbRow['bookAccessionNo'];
	  $bArray['bookTitle'][$b]       = $classbRow['bookTitle'];
	  $b++;
	}
	
  $secArrOut[0] = 'A';
  $secArrOut[1] = 'B';
  $secArrOut[2] = 'C';
  $secArrOut[3] = 'D';
  
  $lateOut[0] = 'Yes';
  $lateOut[1] = 'No';
  
  $staffOut[0] = 'Student';
  $staffOut[1] = 'Staff';
  
  $d=0;
	$dArray = array();
	$selectClass1 = "SELECT name
                     FROM employeemaster";
	$selectClass1Res = mysql_query($selectClass1);
	while($class1Row = mysql_fetch_array($selectClass1Res))
	{
	  $dArray['name'][$d]    = $class1Row['name'];
	  $d++;
	}
	
  include("./bottom.php");
  $smarty->assign('nameStaff',$nameStaff);
  $smarty->assign('dArray',$dArray);
  $smarty->assign('staff',$staff);
  $smarty->assign('staffOut',$staffOut);
  $smarty->assign('cArray',$cArray);
  $smarty->assign('bArray',$bArray);
  $smarty->assign('class',$class);
  $smarty->assign('section',$section);
  $smarty->assign('secArrOut',$secArrOut);
  $smarty->assign('libraryTransactionId',$libraryTransactionId);
  $smarty->assign('attendenceArr',$attendenceArr);
  $smarty->assign('attendDate',$attendDate);
  $smarty->assign('lateOut',$lateOut);
  $smarty->assign('bookAccessionNo',$bookAccessionNo);
  
  $smarty->display('libraryTransactionListEdit.tpl');  
  
}
?>
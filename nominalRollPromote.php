<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$class    = '';
  $section  = '';
	$stdArray = array();
	$year     = date('Y');
	$nominalDone = "";
	
	$selectId = "SELECT employeeMasterId
                 FROM employeemaster
                WHERE loginId = '".$_SESSION['s_activName']."'";
  $selectIdRes = mysql_query($selectId);
  if($idRow = mysql_fetch_array($selectIdRes))
  {
  	$selectClass = "SELECT class,section
	                    FROM classteacherallotment
	                   WHERE employeeMasterId = '".$idRow['employeeMasterId']."'";
	  $selectClassRes = mysql_query($selectClass);
	  if($classRow = mysql_fetch_array($selectClassRes))
	  {
	  	$class   = $classRow['class'];
	  	$section = $classRow['section'];
	  	$i = 0;
		  $selectStd = "SELECT nominalroll.grNo,studentmaster.gender,studentmaster.studentName,nominalroll.class,studentmaster.activated
		                  FROM nominalroll
		             LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
		                 WHERE nominalroll.class = '".$classRow['class']."'
		                   AND nominalroll.section = '".$classRow['section']."'
		                   AND nominalroll.academicEndYear = '".$year."-03-31'
		                   AND studentmaster.activated = 'Y'";
		  $selectStdRes = mysql_query($selectStd);
		  while($stdRow = mysql_fetch_array($selectStdRes))
		  {
		  	$stdArray[$i]['grNo']        = $stdRow['grNo'];
		  	$stdArray[$i]['gender']      = $stdRow['gender'];
		  	$stdArray[$i]['class']       = $stdRow['class'];
		  	$stdArray[$i]['studentName'] = $stdRow['studentName'];

		    $selectMaxYear = "SELECT MAX(academicStartYear) AS academicStartYear,grNo
		                        FROM nominalroll
  		                     WHERE grNo = ".$stdRow['grNo'];
		    $selectMaxYearRes = mysql_query($selectMaxYear);
		    if($maxYear = mysql_fetch_array($selectMaxYearRes))
		    {
		    	$academicStartYear = substr($maxYear['academicStartYear'],0,4);
		    	if($academicStartYear == $year)
		    	{
		    		$stdArray[$i]['nominalDone'] = 1;
		    	}
		    	else
		    	{
		    		$stdArray[$i]['nominalDone'] = 2;
		    	}
		    }
		  	$i++;
		  }
		}
	}
  include("./bottom.php");
  $smarty->assign('class',$class);
  $smarty->assign('nominalDone',$nominalDone);
  $smarty->assign('section',$section);
  $smarty->assign('stdArray',$stdArray);
  $smarty->display('nominalRollPromote.tpl');  
}
?>
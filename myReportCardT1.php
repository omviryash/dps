<?php
define('FPDF_FONTPATH', 'font/');
require('./font/fpdf.php');
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
  $pdf=new FPDF('P','mm','A4');   //Create new pdf file
  $pdf->Open();     //Open file
  $pdf->SetAutoPageBreak(false);  //Disable automatic page break
  $pdf->AddPage();  //Add first page


  //header part start
  $pdf->Image('./images/logo.png',10,10,22,25);
  $pdf->Image('./images/logoIndex.png',65,2,70,20);
  //header part end
  //table header part start  
  $pdf->SetFillColor(232, 232, 232);
  $pdf->SetFont('Arial', '', 8);
  $pdf->SetXY(5,20);
  $pdf->Cell(200, 5, 'Haripar, Survey No. 12, Behind NRI Bunglows, Rajkot - 360 007', 0,0,'C',0);
  $pdf->SetXY(5,25);
  $pdf->Cell(200, 5, 'Ph: 0281 2923170, 2926801, Email : Info@dpsrajkot.org, Website: www.dpsrajkot.org', 0,0,'C',0);
  $pdf->SetXY(5,30);
  $pdf->Cell(200, 5, '(Affilited to C B S E, New Delhi, Affiliation No.430054)', 0,0,'C',0);
  $pdf->SetFont('Arial', 'B', 12);
  $pdf->SetXY(5,35);
  $pdf->Cell(200, 5, 'Record of Academic Performance', 0,0,'C',0);
  
  
  $pdf->SetFillColor(232, 232, 232);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->SetXY(5,95);
  $pdf->Cell(40, 10, 'Scholastic Area', 1,0,'C',1);
  $pdf->Cell(160, 10, 'Term - 1', 1,0,'C',1);
  
  $pdf->SetFillColor(232, 232, 232);
  $pdf->SetFont('Arial', 'B', 8);
  $pdf->SetXY(5,105);
  $pdf->Cell(40, 10, 'Subject', 1,0,'C',1);
  $pdf->Cell(40, 10, 'FA1', 1,0,'C',1);
  $pdf->Cell(40, 10, 'FA2', 1,0,'C',1);
  $pdf->Cell(40, 10, 'SA1', 1,0,'C',1);
  $pdf->Cell(40, 10, 'Total', 1,0,'C',1);
//  
//  $pdf->SetFillColor(232, 232, 232);
//  $pdf->SetFont('Arial', 'B', 8);
//  $pdf->SetXY(5,110);
//  $pdf->Cell(38, 5, '', 0,0,'C',0);
//  $pdf->Cell(12, 5, '(10%)', 1,0,'C',1);
//  $pdf->Cell(12, 5, '(10%)', 1,0,'C',1);
//  $pdf->Cell(12, 5, '(30%)', 1,0,'C',1);
//  $pdf->Cell(12, 5, '(50%)', 1,0,'C',1);
  
  $i         = 0; 
  $rowHeight = 10;
  $yAxis     = 105;

  $yAxis = $yAxis + $rowHeight;
  $totalNumberWorkingDay = 1;
  //table header part end 

  if(isset($_REQUEST['academicYear']))
  {
  	$academicStartYear = substr($_REQUEST['academicYear'],0,4)."-04-01";
  	$nextYear          = substr($_REQUEST['academicYear'],0,4) + 1;
  	$academicEndYear   = $nextYear."-03-31";
  }
  else
  {
	  $todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y')."-04-01";
	  	$nextYear          = date('Y') + 1;
	  	$academicEndYear   = $nextYear."-03-31";
		}
		else
		{
			$prevYear          = date('Y') - 1;
			$academicStartYear = $prevYear."-04-01";
	  	$academicEndYear   = date('Y')."-03-31";
		}
	}
	
	if(isset($_REQUEST['grNo']))
	{
	  $myGrNo = "AND nominalroll.grNo = ".$_REQUEST['grNo']."";
	}
	else
	{
		$myGrNo = "AND studentmaster.studentLoginId = '".$_SESSION['s_activName']."' 
                   OR studentmaster.parentLoginId = '".$_SESSION['s_activName']."'";
	}
	//print column titles for the actual page
  $pdf->SetFillColor(232, 232, 232);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->SetXY(0,40);
  $pdf->Cell(210, 5, substr($academicStartYear,0,4).' - '.substr($academicEndYear,0,4), 0, 0, 'C', 0);
  
  $selectNominal = "SELECT nominalroll.grNo,nominalroll.academicStartYear,nominalroll.academicEndYear,nominalroll.class,
                           nominalroll.section,nominalroll.rollNo,studentmaster.activated,studentmaster.bloodGroup,studentmaster.studentName,
                           studentmaster.fatherName,studentmaster.mothersName,studentmaster.dateOfBirth,studentmaster.currentAddress,
                           nominalroll.goals,nominalroll.strength,nominalroll.intHobbies,nominalroll.responsibilities,
                           nominalroll.height,nominalroll.weight,nominalroll.dentalHygience,nominalroll.3a1,
                           nominalroll.3a2,nominalroll.3b1,nominalroll.3b2
                      FROM nominalroll
                 LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
                     WHERE 1 = 1
                       ".$myGrNo."
                       AND nominalroll.academicStartYear = '".$academicStartYear."'
                       AND nominalroll.academicEndYear = '".$academicEndYear."'
                       AND studentmaster.activated = 'Y'
                  ORDER BY nominalroll.rollNo";
  $selectNominalRes = mysql_query($selectNominal);
  while($nominalRow = mysql_fetch_array($selectNominalRes))
  {
    $class              = $nominalRow['class'];
    $section            = $nominalRow['section'];
    $grNo               = $nominalRow['grNo'];
    $studentName        = $nominalRow['studentName'];
    $fatherName         = $nominalRow['fatherName'];
    $mothersName        = $nominalRow['mothersName'];
    $dateOfBirth        = $nominalRow['dateOfBirth'];
    $currentAddress     = $nominalRow['currentAddress'];
    $academicYear       = substr($nominalRow['academicStartYear'],0,4).'-'.substr($nominalRow['academicEndYear'],0,4);
    $rollNo             = $nominalRow['rollNo'];
    $goals              = $nominalRow['goals'];
  	$strength           = $nominalRow['strength'];
  	$intHobbies         = $nominalRow['intHobbies'];
  	$responsibilities   = $nominalRow['responsibilities'];
  	
  	$height             = $nominalRow['height'];
  	$weight             = $nominalRow['weight'];
  	$bloodGroup         = $nominalRow['bloodGroup'];
  	$dentalHygience     = $nominalRow['dentalHygience'];
  	$co3a1 = $nominalRow['3a1'];
  	$co3a2 = $nominalRow['3a2'];
  	$co3b1 = $nominalRow['3b1'];
  	$co3b2 = $nominalRow['3b2'];
    
    if($co3a1 == '')
    {
      $co3a1 = 0;
    }
    if($co3a2 == '')
    {
      $co3a2 = 0;
    }
    if($co3b1 == '')
    {
      $co3b1 = 0;
    }
    if($co3b2 == '')
    {
      $co3b2 = 0;
    }
    $selectCo1 = "SELECT subjectmaster.subjectName
	                  FROM subjectmaster
	                 WHERE subjectMasterId = ".$co3a1."";
	  $selectCo1Res = mysql_query($selectCo1);
	  if($co1Row = mysql_fetch_array($selectCo1Res))
	  {
	  	$co3a1 = $co1Row['subjectName'];
	  }
    
    $selectCo2 = "SELECT subjectmaster.subjectName
	                  FROM subjectmaster
	                 WHERE subjectMasterId = ".$co3a2."";
	  $selectCo2Res = mysql_query($selectCo2);
	  if($co2Row = mysql_fetch_array($selectCo2Res))
	  {
	  	$co3a2 = $co2Row['subjectName'];
	  }
    
    $selectCo3 = "SELECT subjectmaster.subjectName
	                  FROM subjectmaster
	                 WHERE subjectMasterId = ".$co3b1."";
	  $selectCo3Res = mysql_query($selectCo3);
	  if($co3Row = mysql_fetch_array($selectCo3Res))
	  {
	  	$co3b1 = $co3Row['subjectName'];
	  }
    
    $selectCo4 = "SELECT subjectmaster.subjectName
	                  FROM subjectmaster
	                 WHERE subjectMasterId = ".$co3b2."";
	  $selectCo4Res = mysql_query($selectCo4);
	  if($co4Row = mysql_fetch_array($selectCo4Res))
	  {
	  	$co3b2 = $co4Row['subjectName'];
	  }
    $pdf->SetFillColor(232, 232, 232);
	  $pdf->SetFont('Arial', 'B', 10);
	  $pdf->SetXY(5,45);
	  $pdf->Cell(35, 5, 'Registration No : ', 0, 0, 'L', 0);
	  $pdf->Cell(105,5, $grNo, 0, 0, 'L', 0);
	  $pdf->Cell(35, 5, 'Admission No : ', 0, 0, 'R', 0);
	  $pdf->Cell(25, 5, $grNo, 0, 0, 'R', 0);
	  
	  $pdf->SetFillColor(232, 232, 232);
	  $pdf->SetFont('Arial', 'B', 10);
	  $pdf->SetXY(5,55);
	  $pdf->Cell(35, 5, 'Name of Student : ', 0, 0, 'L', 0);
	  $pdf->Cell(105,5, $studentName, 0, 0, 'L', 0);
	  $pdf->Cell(35, 5, 'Roll No : ', 0, 0, 'R', 0);
	  $pdf->Cell(25, 5, $rollNo, 0, 0, 'R', 0);
	  
    $pdf->SetFillColor(232, 232, 232);
	  $pdf->SetFont('Arial', 'B', 10);
	  $pdf->SetXY(5,65);
	  $pdf->Cell(35, 5, 'Fathes Name : ', 0, 0, 'L', 0);
	  $pdf->Cell(105,5, $fatherName, 0, 0, 'L', 0);
	  $pdf->Cell(35, 5, 'Class : ', 0, 0, 'R', 0);
	  $pdf->Cell(25, 5, $class.' - '.$section, 0, 0, 'R', 0);
	  
	  $pdf->SetFillColor(232, 232, 232);
	  $pdf->SetFont('Arial', 'B', 10);
	  $pdf->SetXY(5,75);
	  $pdf->Cell(35, 5, 'Mothers Name : ', 0, 0, 'L', 0);
	  $pdf->Cell(105,5, $mothersName, 0, 0, 'L', 0);
	  $pdf->Cell(35, 5, 'DOB : ', 0, 0, 'R', 0);
	  $pdf->Cell(25, 5, $dateOfBirth, 0, 0, 'R', 0);
	  
    $pdf->SetFillColor(232, 232, 232);
	  $pdf->SetFont('Arial', 'B', 10);
	  $pdf->SetXY(5,85);
	  $pdf->Cell(35, 5, 'Address : ', 0, 0, 'L', 0);
	  $pdf->Cell(165,5, $currentAddress, 0, 0, 'L', 0);
	  
	  $selectStd = "SELECT exammarksper.grNo,exammarksper.fa1,exammarksper.fa2,exammarksper.sa1,
	                       exammarksper.fa3,exammarksper.fa4,exammarksper.sa2,subjectmaster.subjectName
	                  FROM exammarksper
	             LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = exammarksper.subjectMasterId
	                 WHERE exammarksper.grNo = '".$grNo."'
	                   AND exammarksper.class = '".$class."'
	                   AND exammarksper.section = '".$section."'
	                   AND exammarksper.academicStartYear = '".$academicStartYear."'
	                   AND exammarksper.academicEndYear = '".$academicEndYear."'
	              ORDER BY exammarksper.subjectMasterId";
	  $selectStdRes = mysql_query($selectStd);
	  while($stdRow = mysql_fetch_array($selectStdRes))
	  {
	  	$subjectName = $stdRow['subjectName'];
	  	$fa1         = $stdRow['fa1'];
	  	$fa1Per      = $fa1 * 100/10;
      if($fa1Per >= 91 && $fa1Per <= 100)
			{
			  $fa1Per = 'A1';
			}
			elseif($fa1Per >= 81 && $fa1Per <= 90)
			{
				$fa1Per = 'A2';
			}
			elseif($fa1Per >= 71 && $fa1Per <= 80)
			{
				$fa1Per = 'B1';
			}
			elseif($fa1Per >= 61 && $fa1Per <= 70)
			{
				$fa1Per = 'B2';
			}
			elseif($fa1Per >= 51 && $fa1Per <= 60)
			{
				$fa1Per = 'C1';
			}
			elseif($fa1Per >= 40 && $fa1Per <= 50)
			{
				$fa1Per = 'C2';
			}
			elseif($fa1Per >= 33 && $fa1Per <= 40)
			{
				$fa1Per = 'D';
			}
			elseif($fa1Per >= 21 && $fa1Per <= 32)
			{
				$fa1Per = 'E1';
			}
			elseif($fa1Per <= 20)
			{
				$fa1Per = 'E2';
			}
			
	  	$fa2         = $stdRow['fa2'];
	  	$fa2Per = $fa2 * 100/10;
	  	if($fa2Per >= 91 && $fa2Per <= 100)
			{
			  $fa2Per = 'A1';
			}
			elseif($fa2Per >= 81 && $fa2Per <= 90)
			{
				$fa2Per = 'A2';
			}
			elseif($fa2Per >= 71 && $fa2Per <= 80)
			{
				$fa2Per = 'B1';
			}
			elseif($fa2Per >= 61 && $fa2Per <= 70)
			{
				$fa2Per = 'B2';
			}
			elseif($fa2Per >= 51 && $fa2Per <= 60)
			{
				$fa2Per = 'C1';
			}
			elseif($fa2Per >= 40 && $fa2Per <= 50)
			{
				$fa2Per = 'C2';
			}
			elseif($fa2Per >= 33 && $fa2Per <= 40)
			{
				$fa2Per = 'D';
			}
			elseif($fa2Per >= 21 && $fa2Per <= 32)
			{
				$fa2Per = 'E1';
			}
			elseif($fa2Per <= 20)
			{
				$fa2Per = 'E2';
			}
	  	
	  	$sa1         = $stdRow['sa1'];
	  	$sa1Per = $sa1 * 100/30;
	  	if($sa1Per >= 91 && $sa1Per <= 100)
			{
			  $sa1Per = 'A1';
			}
			elseif($sa1Per >= 81 && $sa1Per <= 90)
			{
				$sa1Per = 'A2';
			}
			elseif($sa1Per >= 71 && $sa1Per <= 80)
			{
				$sa1Per = 'B1';
			}
			elseif($sa1Per >= 61 && $sa1Per <= 70)
			{
				$sa1Per = 'B2';
			}
			elseif($sa1Per >= 51 && $sa1Per <= 60)
			{
				$sa1Per = 'C1';
			}
			elseif($sa1Per >= 40 && $sa1Per <= 50)
			{
				$sa1Per = 'C2';
			}
			elseif($sa1Per >= 33 && $sa1Per <= 40)
			{
				$sa1Per = 'D';
			}
			elseif($sa1Per >= 21 && $sa1Per <= 32)
			{
				$sa1Per = 'E1';
			}
			elseif($sa1Per <= 20)
			{
				$sa1Per = 'E2';
			}
	    
	    $sa1Total = $fa1 + $fa2 + $sa1;
	    $sa1TotalPer = $sa1Total * 100/50;
	    if($sa1TotalPer >= 91 && $sa1TotalPer <= 100)
			{
			  $sa1TotalPer = 'A1';
			}
			elseif($sa1TotalPer >= 81 && $sa1TotalPer <= 90)
			{
				$sa1TotalPer = 'A2';
			}
			elseif($sa1TotalPer >= 71 && $sa1TotalPer <= 80)
			{
				$sa1TotalPer = 'B1';
			}
			elseif($sa1TotalPer >= 61 && $sa1TotalPer <= 70)
			{
				$sa1TotalPer = 'B2';
			}
			elseif($sa1TotalPer >= 51 && $sa1TotalPer <= 60)
			{
				$sa1TotalPer = 'C1';
			}
			elseif($sa1TotalPer >= 40 && $sa1TotalPer <= 50)
			{
				$sa1TotalPer = 'C2';
			}
			elseif($sa1TotalPer >= 33 && $sa1TotalPer <= 40)
			{
				$sa1TotalPer = 'D';
			}
			elseif($sa1TotalPer >= 21 && $sa1TotalPer <= 32)
			{
				$sa1TotalPer = 'E1';
			}
			elseif($sa1TotalPer <= 20)
			{
				$sa1TotalPer = 'E2';
			}

	    
	  	$fa3         = $stdRow['fa3'];
	  	$fa3Per = $fa3 * 100/10;
	  	if($fa3Per >= 91 && $fa3Per <= 100)
			{
			  $fa3Per = 'A1';
			}
			elseif($fa3Per >= 81 && $fa3Per <= 90)
			{
				$fa3Per = 'A2';
			}
			elseif($fa3Per >= 71 && $fa3Per <= 80)
			{
				$fa3Per = 'B1';
			}
			elseif($fa3Per >= 61 && $fa3Per <= 70)
			{
				$fa3Per = 'B2';
			}
			elseif($fa3Per >= 51 && $fa3Per <= 60)
			{
				$fa3Per = 'C1';
			}
			elseif($fa3Per >= 40 && $fa3Per <= 50)
			{
				$fa3Per = 'C2';
			}
			elseif($fa3Per >= 33 && $fa3Per <= 40)
			{
				$fa3Per = 'D';
			}
			elseif($fa3Per >= 21 && $fa3Per <= 32)
			{
				$fa3Per = 'E1';
			}
			elseif($fa3Per <= 20)
			{
				$fa3Per = 'E2';
			}
			
	  	$fa4         = $stdRow['fa4'];
	  	$fa4Per = $fa4 * 100/10;
	  	if($fa4Per >= 91 && $fa4Per <= 100)
			{
			  $fa4Per = 'A1';
			}
			elseif($fa4Per >= 81 && $fa4Per <= 90)
			{
				$fa4Per = 'A2';
			}
			elseif($fa4Per >= 71 && $fa4Per <= 80)
			{
				$fa4Per = 'B1';
			}
			elseif($fa4Per >= 61 && $fa4Per <= 70)
			{
				$fa4Per = 'B2';
			}
			elseif($fa4Per >= 51 && $fa4Per <= 60)
			{
				$fa4Per = 'C1';
			}
			elseif($fa4Per >= 40 && $fa4Per <= 50)
			{
				$fa4Per = 'C2';
			}
			elseif($fa4Per >= 33 && $fa4Per <= 40)
			{
				$fa4Per = 'D';
			}
			elseif($fa4Per >= 21 && $fa4Per <= 32)
			{
				$fa4Per = 'E1';
			}
			elseif($fa4Per <= 20)
			{
				$fa4Per = 'E2';
			}
	  	
	  	$sa2         = $stdRow['sa2'];
	  	$sa2Per = $sa2 * 100/30;
	  	if($sa2Per >= 91 && $sa2Per <= 100)
			{
			  $sa2Per = 'A1';
			}
			elseif($sa2Per >= 81 && $sa2Per <= 90)
			{
				$sa2Per = 'A2';
			}
			elseif($sa2Per >= 71 && $sa2Per <= 80)
			{
				$sa2Per = 'B1';
			}
			elseif($sa2Per >= 61 && $sa2Per <= 70)
			{
				$sa2Per = 'B2';
			}
			elseif($sa2Per >= 51 && $sa2Per <= 60)
			{
				$sa2Per = 'C1';
			}
			elseif($sa2Per >= 40 && $sa2Per <= 50)
			{
				$sa2Per = 'C2';
			}
			elseif($sa2Per >= 33 && $sa2Per <= 40)
			{
				$sa2Per = 'D';
			}
			elseif($sa2Per >= 21 && $sa2Per <= 32)
			{
				$sa2Per = 'E1';
			}
			elseif($sa2Per <= 20)
			{
				$sa2Per = 'E2';
			}
	  	
	  	$sa2Total = $fa3 + $fa4 + $sa2;
	  	$sa2TotalPer = $sa2Total * 100/50;
	  	if($sa2TotalPer >= 91 && $sa2TotalPer <= 100)
			{
			  $sa2TotalPer = 'A1';
			}
			elseif($sa2TotalPer >= 81 && $sa2TotalPer <= 90)
			{
				$sa2TotalPer = 'A2';
			}
			elseif($sa2TotalPer >= 71 && $sa2TotalPer <= 80)
			{
				$sa2TotalPer = 'B1';
			}
			elseif($sa2TotalPer >= 61 && $sa2TotalPer <= 70)
			{
				$sa2TotalPer = 'B2';
			}
			elseif($sa2TotalPer >= 51 && $sa2TotalPer <= 60)
			{
				$sa2TotalPer = 'C1';
			}
			elseif($sa2TotalPer >= 40 && $sa2TotalPer <= 50)
			{
				$sa2TotalPer = 'C2';
			}
			elseif($sa2TotalPer >= 33 && $sa2TotalPer <= 40)
			{
				$sa2TotalPer = 'D';
			}
			elseif($sa2TotalPer >= 21 && $sa2TotalPer <= 32)
			{
				$sa2TotalPer = 'E1';
			}
			elseif($sa2TotalPer <= 20)
			{
				$sa2TotalPer = 'E2';
			}
			
	  	$faTotal = $fa1 + $fa2 + $fa3 + $fa4;
	  	$faTotalPer = $faTotal * 100/40;
	  	if($faTotalPer >= 91 && $faTotalPer <= 100)
			{
			  $faTotalPer = 'A1';
			}
			elseif($faTotalPer >= 81 && $faTotalPer <= 90)
			{
				$faTotalPer = 'A2';
			}
			elseif($faTotalPer >= 71 && $faTotalPer <= 80)
			{
				$faTotalPer = 'B1';
			}
			elseif($faTotalPer >= 61 && $faTotalPer <= 70)
			{
				$faTotalPer = 'B2';
			}
			elseif($faTotalPer >= 51 && $faTotalPer <= 60)
			{
				$faTotalPer = 'C1';
			}
			elseif($faTotalPer >= 40 && $faTotalPer <= 50)
			{
				$faTotalPer = 'C2';
			}
			elseif($faTotalPer >= 33 && $faTotalPer <= 40)
			{
				$faTotalPer = 'D';
			}
			elseif($faTotalPer >= 21 && $faTotalPer <= 32)
			{
				$faTotalPer = 'E1';
			}
			elseif($faTotalPer <= 20)
			{
				$faTotalPer = 'E2';
			}
	  	
	  	$saTotal = $sa1 + $sa2;
	  	$saTotalPer = $saTotal * 100/60;
	  	if($saTotalPer >= 91 && $saTotalPer <= 100)
			{
			  $saTotalPer = 'A1';
			}
			elseif($saTotalPer >= 81 && $saTotalPer <= 90)
			{
				$saTotalPer = 'A2';
			}
			elseif($saTotalPer >= 71 && $saTotalPer <= 80)
			{
				$saTotalPer = 'B1';
			}
			elseif($saTotalPer >= 61 && $saTotalPer <= 70)
			{
				$saTotalPer = 'B2';
			}
			elseif($saTotalPer >= 51 && $saTotalPer <= 60)
			{
				$saTotalPer = 'C1';
			}
			elseif($saTotalPer >= 40 && $saTotalPer <= 50)
			{
				$saTotalPer = 'C2';
			}
			elseif($saTotalPer >= 33 && $saTotalPer <= 40)
			{
				$saTotalPer = 'D';
			}
			elseif($saTotalPer >= 21 && $saTotalPer <= 32)
			{
				$saTotalPer = 'E1';
			}
			elseif($saTotalPer <= 20)
			{
				$saTotalPer = 'E2';
			}
			
			$fasaTotal = $faTotal + $saTotal;
	  	if($fasaTotal >= 91 && $fasaTotal <= 100)
			{
			  $fasaTotal = 'A1';
			}
			elseif($fasaTotal >= 81 && $fasaTotal <= 90)
			{
				$fasaTotal = 'A2';
			}
			elseif($fasaTotal >= 71 && $fasaTotal <= 80)
			{
				$fasaTotal = 'B1';
			}
			elseif($fasaTotal >= 61 && $fasaTotal <= 70)
			{
				$fasaTotal = 'B2';
			}
			elseif($fasaTotal >= 51 && $fasaTotal <= 60)
			{
				$fasaTotal = 'C1';
			}
			elseif($fasaTotal >= 40 && $fasaTotal <= 50)
			{
				$fasaTotal = 'C2';
			}
			elseif($fasaTotal >= 33 && $fasaTotal <= 40)
			{
				$fasaTotal = 'D';
			}
			elseif($fasaTotal >= 21 && $fasaTotal <= 32)
			{
				$fasaTotal = 'E1';
			}
			elseif($fasaTotal <= 20)
			{
				$fasaTotal = 'E2';
			}
	  	
	  	$pdf->SetFillColor(232, 232, 232);
	    $pdf->SetXY(5,$yAxis);
	    $pdf->SetFont('Arial', '', 10);
	    $pdf->Cell(40, 10, $subjectName, 1,0,'L');
		  $pdf->Cell(40, 10, $fa1Per, 1,0,'C');
		  $pdf->Cell(40, 10, $fa2Per, 1,0,'C');
		  $pdf->Cell(40, 10, $sa1Per, 1,0,'C');
		  $pdf->Cell(40, 10, $sa1TotalPer, 1,0,'C');
	    
	    $yAxis = $yAxis + $rowHeight;
	    $i = $i + 1;
	  }
	  
	  $selectAtnP = "SELECT attendences
                     FROM attendence
                    WHERE class = '".$class."'
                      AND section = '".$section."'
                      AND grNo = '".$grNo."'
                      AND date >= '".$academicStartYear."-04-01'
                      AND date <= '".$academicEndYear."-03-31'
                      AND attendences = 'P'";
    $selectAtnPRes = mysql_query($selectAtnP);
    $countP = mysql_num_rows($selectAtnPRes);
    
    $selectAtnA = "SELECT attendences
                     FROM attendence
                    WHERE class = '".$class."'
                      AND section = '".$section."'
                      AND grNo = '".$grNo."'
                      AND date >= '".$academicStartYear."-04-01'
                      AND date <= '".$academicEndYear."-03-31'
                      AND attendences = 'A'";
    $selectAtnARes = mysql_query($selectAtnA);
    $countA = mysql_num_rows($selectAtnARes);
    $totalNumberWorkingDay = $countA + $countP;
    
    if($totalNumberWorkingDay != 0)
    {
  	  $countPPer = $countP * 100/$totalNumberWorkingDay;
  	}
  	else
  	{
  		$countPPer = 0;
  	}
  	
    $pdf->SetFont('Arial', '', 8);
	  $pdf->SetXY(5,$yAxis);
	  $pdf->Cell(40, 10, 'Attendance', 1,0,'C',0);
	  $pdf->Cell(80, 10, $countP.'/'.$totalNumberWorkingDay, 1,0,'C');
	  $pdf->Cell(40, 10, number_format($countPPer, 2, '.', ' '), 1,0,'C');
	  $pdf->Cell(40, 10, '', 1,0,'C');
	  
	  $pdf->SetXY(5,$yAxis+10);
	  $pdf->Cell(134, 5, 'Grading Scale : A1 = 91% - 100%; A2 = 81% - 90%; B1 = 71% - 80%; B2 = 61% - 70%; C1 = 51% - 60%;', 1,0,'L',0);
	  $pdf->Cell(66, 10, '', 1,0,'C',0);
	  
	  $pdf->SetXY(5,$yAxis+15);
	  $pdf->Cell(134, 5, 'C2 = 41% - 50%; D = 33% - 40%; E1 = 21% - 32%; E2 = 20% AND BELOW', 1,0,'L',0);
	  
	  
	  
	  $pdf->SetFillColor(232, 232, 232);
	  $pdf->SetFont('Arial', '', 10);
	  $pdf->SetXY(5,$yAxis+50);
	  $pdf->Cell(15, 5, 'Result : ', 0, 0, 'L', 0);
	  $pdf->Cell(160,5, '', 0, 0, 'L', 0);
	  $pdf->Cell(25, 5, '', 0, 0, 'R', 0);
	  
	  $pdf->SetFillColor(232, 232, 232);
	  $pdf->SetFont('Arial', '', 8);
	  $pdf->SetXY(5,$yAxis+93);
	  $pdf->Cell(100, 5, 'CLASS TEACHERS SIGNATURE', 0, 0, 'L', 0);
	  $pdf->Cell(100, 5, 'PRINCIPALS SIGNATURE & SEAL', 0, 0, 'R', 0);
	  
	  $pdf->SetFillColor(232, 232, 232);
	  $pdf->SetFont('Arial', '', 7);
	  $pdf->SetXY(5,$yAxis+99);
	  $pdf->Cell(125, 3, 'Note: (1)Promotion is based on the day-to-day continous assessment throughout the year', 0, 0, 'L', 0);
	  $pdf->Cell(75,3, '', 0, 0, 'L', 0);
	  
//	  $pdf->SetXY(5,$yAxis+122);
//	  $pdf->Cell(125, 3, '(2)CGPA = Cumulative Grade Point Avarage (3)Subject wise/Overall indicative percentage of', 0, 0, 'L', 0);
//	  $pdf->Cell(75,3, '', 0, 0, 'L', 0);
//	  
//	  $pdf->SetXY(5,$yAxis+125);
//	  $pdf->Cell(125, 3, 'Marks = 9.5 X GP of the subject/CGPA.', 0, 0, 'L', 0);
//	  $pdf->Cell(75,3, '', 0, 0, 'L', 0);
	  
	  $pdf->line(5,$yAxis+50,205,$yAxis+50);
	  $pdf->line(5,$yAxis+55,205,$yAxis+55);
	  $pdf->line(5,$yAxis+98,205,$yAxis+98);
	  $pdf->line(5,$yAxis+110,205,$yAxis+110);
	  
	  $pdf->line(5,$yAxis+20,5,$yAxis+110);
	  $pdf->line(205,$yAxis+20,205,$yAxis+110);
	  
  }
  
  $pdf->Output();
}
?>
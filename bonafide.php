<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$grNo = isset($_REQUEST['grNo']) ? $_REQUEST['grNo'] : 0;
	$nominalArr = array();
  $i = 0;
  $selectNominal = "SELECT nominalRollId,nominalroll.grNo,academicStartYear,academicEndYear,class,
                           section,rollNo,bonafideDate,studentmaster.studentName,activated,bonafideDate,wefDate
                      FROM nominalroll
                 LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
                     WHERE nominalroll.grNo = ".$grNo."
                  ORDER BY nominalroll.grNo";
  $selectNominalRes = mysql_query($selectNominal);
  while($nominalRow = mysql_fetch_array($selectNominalRes))
  {
    $nominalArr[$i]['nominalRollId']      = $nominalRow['nominalRollId'];
    $nominalArr[$i]['grNo']               = $nominalRow['grNo'];
    $nominalArr[$i]['academicYear']       = substr($nominalRow['academicStartYear'],0,4).'-'.substr($nominalRow['academicEndYear'],0,4);
    $nominalArr[$i]['class']              = $nominalRow['class'];
    $nominalArr[$i]['section']            = $nominalRow['section'];
    $nominalArr[$i]['rollNo']             = $nominalRow['rollNo'];
    $nominalArr[$i]['bonafideDate']       = $nominalRow['bonafideDate'];
    $nominalArr[$i]['wefDate']            = $nominalRow['wefDate'];
    $nominalArr[$i]['studentName']        = $nominalRow['studentName'];
    $nominalArr[$i]['activated']          = $nominalRow['activated'];
    $i++;
  }
  
  include("./bottom.php");
  $smarty->assign('nominalArr',$nominalArr);
  $smarty->display('bonafide.tpl');  
}
?>
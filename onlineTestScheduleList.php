<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	if(isset($_REQUEST['startYear']))
  {
  	$academicStartYear = $_REQUEST['startYear']."-04-01";
  	$nextYear          = $_REQUEST['startYear'] + 1;
  	$academicEndYear   = $nextYear."-03-31";
  	$academicStartYearSelected = $_REQUEST['startYear'];
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y')."-04-01";
	  	$nextYear          = date('Y') + 1;
	  	$academicEndYear   = $nextYear."-03-31";
	  	$academicStartYearSelected = date('Y');
		}
		else
		{
			$prevYear          = date('Y') - 1;
			$academicStartYear = $prevYear."-04-01";
	  	$academicEndYear   = date('Y')."-03-31";
	  	$academicStartYearSelected = $prevYear;
		}
	}
	
	$subjectAltId = isset($_REQUEST['subjectAltId']) && $_REQUEST['subjectAltId'] != '' ? $_REQUEST['subjectAltId'] : '0_0_0';
	
	$subjectAltIdExp = explode("_",$subjectAltId);
	$subjectAltIdExp[0];
	$subjectAltIdExp[1];
	$subjectAltIdExp[2];
	
	if(isset($_REQUEST['class']))
	{
	  $class           = isset($_REQUEST['class']) && $_REQUEST['class'] != '' ? $_REQUEST['class'] : 0;
		$subjectMasterId = isset($_REQUEST['subjectMasterId']) && $_REQUEST['subjectMasterId'] != '' ? $_REQUEST['subjectMasterId'] : 0;
	}
	else
	{
		$class           = $subjectAltIdExp[0];
		$subjectMasterId = $subjectAltIdExp[2];
	}
	
	$scheduleMasterId = 0;
	$attendenceArr = array();
	$i             = 0;
	$classArray = array();
  $r = 0;
  $selectClass = "SELECT subjectteacherallotment.subjectTeacherAllotmentId,employeemaster.loginId,
                         subjectteacherallotment.academicStartYear,subjectteacherallotment.academicEndYear,subjectteacherallotment.subjectMasterId,
                         subjectteacherallotment.class,subjectteacherallotment.section,subjectteacherallotment.period,
                         employeemaster.name,subjectmaster.subjectName
                    FROM subjectteacherallotment
               LEFT JOIN employeemaster ON employeemaster.employeeMasterId = subjectteacherallotment.employeeMasterId
               LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = subjectteacherallotment.subjectMasterId
                   WHERE employeemaster.loginId = '".$_SESSION['s_activName']."'
                     AND subjectteacherallotment.academicStartYear = '".$academicStartYear."'
	                   AND subjectteacherallotment.academicEndYear = '".$academicEndYear."'";
  $selectClassRes = mysql_query($selectClass);
  $count = mysql_num_rows($selectClassRes);
  while($classRow = mysql_fetch_array($selectClassRes))
  {
  	$classArray[$r]['subjectTeacherAllotmentId'] = $classRow['subjectTeacherAllotmentId'];
  	$classArray[$r]['name']                      = $classRow['name'];
  	$classArray[$r]['academicStartYear']         = substr($classRow['academicStartYear'],0,4);
  	$classArray[$r]['academicEndYear']           = substr($classRow['academicEndYear'],2,2);
    
    $classArray[$r]['class']           = $classRow['class'];
  	$classArray[$r]['section']         = $classRow['section'];
  	$classArray[$r]['subjectMasterId'] = $classRow['subjectMasterId'];
  	$classArray[$r]['subjectName']     = $classRow['subjectName'];
  	$classArray[$r]['period']          = $classRow['period'];
  	$r++;
  
	  if($class == 0 || $subjectMasterId == 0 || $subjectAltId == '0_0_0')
	  {
		  $selectAttendM= "SELECT onlineschedule.scheduleMasterId,DATE_FORMAT(onlineschedule.scheduleDate, '%d-%m-%Y') AS scheduleDate,
		                          onlineschedule.class,subjectmaster.subjectName
		                     FROM onlineschedule
		                LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = onlineschedule.subjectMasterId
		                    WHERE 1 = 1
		                      AND onlineschedule.class = '".$classRow['class']."'
			                    AND onlineschedule.subjectMasterId = '".$classRow['subjectMasterId']."'
		                      AND onlineschedule.scheduleDate > '".$academicStartYear."'
			                    AND onlineschedule.scheduleDate < '".$academicEndYear."' order by onlineschedule.scheduleDate desc ";
		  $selectAttendMRes = mysql_query($selectAttendM);
		  while($maRow = mysql_fetch_array($selectAttendMRes))
		  {
		  	$attendenceArr[$i]['scheduleMasterId'] = $maRow['scheduleMasterId'];
		  	$attendenceArr[$i]['scheduleDate']     = $maRow['scheduleDate'];
		  	$attendenceArr[$i]['class']            = $maRow['class'];
		  	$attendenceArr[$i]['subjectName']      = $maRow['subjectName'];
		  	$i++;
		  }
		}
	}
	
	if(isset($_REQUEST['subjectAltId']) &&  $subjectAltId != '0_0_0')
	{
		$selectAttendM= "SELECT DISTINCT onlineschedule.scheduleMasterId,DATE_FORMAT(onlineschedule.scheduleDate, '%d-%m-%Y') AS scheduleDate,
	                          onlineschedule.class,subjectmaster.subjectName
	                     FROM onlineschedule
	                LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = onlineschedule.subjectMasterId
	                    WHERE 1 = 1
	                      AND onlineschedule.class = '".$class."'
		                    AND onlineschedule.subjectMasterId = ".$subjectMasterId."
	                      AND onlineschedule.scheduleDate > '".$academicStartYear."'
		                    AND onlineschedule.scheduleDate < '".$academicEndYear."' order by onlineschedule.scheduleDate desc";
	  $selectAttendMRes = mysql_query($selectAttendM);
	  while($maRow = mysql_fetch_array($selectAttendMRes))
	  {
	  	$attendenceArr[$i]['scheduleMasterId'] = $maRow['scheduleMasterId'];
	  	$attendenceArr[$i]['scheduleDate']     = $maRow['scheduleDate'];
	  	$attendenceArr[$i]['class']            = $maRow['class'];
	  	$attendenceArr[$i]['subjectName']      = $maRow['subjectName'];
	  	$i++;
	  }
	}
	
	if($count == 0)
  {
    if($class > 0 && $subjectMasterId == 0)
    {
      $newQuery = "AND onlineschedule.class = '".$class."'";
    }
  	elseif($subjectMasterId > 0 && $class == 0)
		{
			$newQuery = "AND onlineschedule.subjectMasterId = '".$subjectMasterId."'";
		}
  	elseif($class > 0 && $subjectMasterId > 0)
		{
			$newQuery = "AND onlineschedule.class = '".$class."'
			             AND onlineschedule.subjectMasterId = '".$subjectMasterId."'";
		}
		else
		{
			$newQuery = "AND 1 = 1";
		}
		$selectAttendM= "SELECT onlineschedule.scheduleMasterId,DATE_FORMAT(onlineschedule.scheduleDate, '%d-%m-%Y') AS scheduleDate,
	                          onlineschedule.class,subjectmaster.subjectName
	                     FROM onlineschedule
	                LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = onlineschedule.subjectMasterId
	                    WHERE 1 = 1
	                      ".$newQuery."
	                      AND onlineschedule.scheduleDate > '".$academicStartYear."'
		                    AND onlineschedule.scheduleDate < '".$academicEndYear."'";
	  $selectAttendMRes = mysql_query($selectAttendM);
	  while($maRow = mysql_fetch_array($selectAttendMRes))
	  {
	  	$attendenceArr[$i]['scheduleMasterId'] = $maRow['scheduleMasterId'];
	  	$attendenceArr[$i]['scheduleDate']     = $maRow['scheduleDate'];
	  	$attendenceArr[$i]['class']            = $maRow['class'];
	  	$attendenceArr[$i]['subjectName']      = $maRow['subjectName'];
	  	$i++;
	  }
	}
	$k = 0;
  $clubArr = array();
  $selectClub = "SELECT subjectMasterId,subjectName
                   FROM subjectmaster
                  WHERE subjectType = 'Main' OR subjectType = 'Optional'
               ORDER BY subjectName";
  $selectClubRes = mysql_query($selectClub);
  while($clubRow = mysql_fetch_array($selectClubRes))
  {
  	$clubArr['subjectMasterId'][$k]   = $clubRow['subjectMasterId'];
  	$clubArr['subjectName'][$k]       = $clubRow['subjectName'];
  	$k++;
  }
  
  $c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
	$scdArray = array();
  $f = 0;
	$selectId = "SELECT employeeMasterId
                 FROM employeemaster
                WHERE loginId = '".$_SESSION['s_activName']."'";
  $selectIdRes = mysql_query($selectId);
  if($subRow = mysql_fetch_array($selectIdRes))
  {
  	$selectSub = "SELECT subjectteacherallotment.class,subjectteacherallotment.section,subjectteacherallotment.subjectMasterId,subjectmaster.subjectName
                    FROM subjectteacherallotment
               LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = subjectteacherallotment.subjectMasterId
                   WHERE subjectteacherallotment.employeeMasterId = '".$subRow['employeeMasterId']."'
                     AND subjectteacherallotment.academicStartYear = '".$academicStartYear."'
                     AND subjectteacherallotment.academicEndYear = '".$academicEndYear."'";
	  $selectSubRes = mysql_query($selectSub);
	  while($subRow = mysql_fetch_array($selectSubRes))
	  {
	  	$classTeacherSubjectClass    = $subRow['class'];
	  	$classTeacherSubjectsection  = $subRow['section'];
	  	$classTeachersubjectMasterId = $subRow['subjectMasterId'];
	  	$subjectName = $subRow['subjectName'];
		  
	  	$scdArray['subjectAltId'][$f] = $subRow['class'].'_'.$subRow['section'].'_'.$subRow['subjectMasterId'];
	  	$scdArray['subjectDtl'][$f]   = $subRow['class'].'_'.$subRow['section'].'_'.$subRow['subjectName'];
	  	$f++;
	  }
  }
  
  include("./bottom.php");
  $smarty->assign('scdArray',$scdArray);
  $smarty->assign('cArray',$cArray);
  $smarty->assign('class',$class);
  $smarty->assign('clubArr',$clubArr);
  $smarty->assign('classArray',$classArray);
  $smarty->assign('classArray',$classArray);
  $smarty->assign('scheduleMasterId',$scheduleMasterId);
  $smarty->assign('attendenceArr',$attendenceArr);
  $smarty->assign('academicStartYear',$academicStartYear);
  $smarty->assign('academicStartYearSelected',$academicStartYearSelected);
  $smarty->assign('subjectAltId',$subjectAltId);
  $smarty->assign('subjectMasterId',$subjectMasterId);
  $smarty->display('onlineTestScheduleList.tpl');  
}
?>
<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
  $nominalRollId       = isset($_REQUEST['nominalRollId']) ? $_REQUEST['nominalRollId'] : 0;
  $class               = ""; 
  $grNo                = "";
  $section             = "";
  $rollNo              = "";
  $boardRegistrionId   = "";
  $boardRollNo         = "";
  $clubMasterId        = "";
  $clubName            = "";
  $academicStartYear   = "";
  $academicEndYear     = "";
  $studentName         = "";
  $todayAcademic = date('m-d');
	if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
	{
  	$academicStartYear = date('Y');
	}
	else
	{
		$academicStartYear = date('Y') - 1;
	}
  if(isset($_POST['submit']))
  {
    $nextYear            = $_POST['startYear'] + 1;
    $academicStartYear   = $_POST['startYear']."-04-01";
    $academicEndYear     = $nextYear."-03-31";
	  $class             = isset($_REQUEST['class']) && $_REQUEST['class'] != '' ? $_REQUEST['class'] : 0;
	  $grNo              = isset($_REQUEST['grNo']) && $_REQUEST['grNo'] != '' ? $_REQUEST['grNo'] : 0;
	  $section           = isset($_REQUEST['section']) && $_REQUEST['section'] != '' ? $_REQUEST['section'] : "";
	  $rollNo            = isset($_REQUEST['rollNo']) && $_REQUEST['rollNo'] != '' ? $_REQUEST['rollNo'] : 0;
	  $boardRegistrionId = isset($_REQUEST['boardRegistrionId']) && $_REQUEST['boardRegistrionId'] != '' ? $_REQUEST['boardRegistrionId'] : 0;
	  $boardRollNo       = isset($_REQUEST['boardRollNo']) && $_REQUEST['boardRollNo'] != '' ? $_REQUEST['boardRollNo'] : 0;
	  
	  if(isset($_POST['nominalRollId']) && $_POST['nominalRollId'] == 0)
	  {
	  	$nominalEntry = "INSERT INTO nominalroll (grNo,academicStartYear,academicEndYear,class,section,rollNo,
	  	  																             boardRegistrionId,boardRollNo)
	  	                          VALUES (".$grNo.",'".$academicStartYear."','".$academicEndYear."','".$class."',
	  	                                  '".$section."','".$rollNo."','".$boardRegistrionId."',
	  	                                  '".$boardRollNo."')";
  	  $nominalEntryRes = om_query($nominalEntry);
  	  if(!$nominalEntryRes)
  	  {
  	    echo "Nominal Entry Fail";
  	  }
  	  else
  	  {
  	    header("Location:nominalRollEntry.php?done=1");
  	  }
	  }
	  else
	  {
	    $nominalEntry = "UPDATE nominalroll 
	    										SET grNo               = ".$grNo.",
	    											  class              = '".$class."',
	    											  section            = '".$section."',
	    											  rollNo             = ".$rollNo.",
	    											  boardRegistrionId  = '".$boardRegistrionId."',
	    											  boardRollNo        = '".$boardRollNo."'
	  								  	WHERE nominalRollId       = ".$_POST['nominalRollId'];
	    $nominalEntryRes = om_query($nominalEntry);
	    if(!$nominalEntryRes)
	    {
	    	echo "Update Fail";
	    }
	    else
	    {
	      header("Location:nominalRollEntry.php?done=1");
	    }
	  }
	}
  if($nominalRollId > 0)
	{
	  $selectStd = "SELECT nominalroll.grNo,academicStartYear,academicEndYear,class,section,rollNo,
	                       boardRegistrionId,boardRollNo,studentmaster.studentName
	                  FROM nominalroll
	             LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
	                 WHERE nominalroll.nominalRollId = ".$nominalRollId."";
	  $selectStdRes = mysql_query($selectStd);
	  while($stdRow = mysql_fetch_array($selectStdRes))
	  {
	  	$grNo                  = $stdRow['grNo'];
	  	$studentName           = $stdRow['studentName'];
	  	$academicStartYear     = substr($stdRow['academicStartYear'],0,4);
	  	$academicEndYear       = substr($stdRow['academicEndYear'],0,4);
	  	$class                 = $stdRow['class'];
	  	$section               = $stdRow['section'];
	  	$rollNo                = $stdRow['rollNo'];
	  	$boardRegistrionId     = $stdRow['boardRegistrionId'];
	  	$boardRollNo           = $stdRow['boardRollNo'];
	  }
	}
	
  $k = 0;
  $selectClub = "SELECT subjectMasterId,subjectName
                   FROM subjectmaster
                  WHERE subjectType = 'Club'";
  $selectClubRes = mysql_query($selectClub);
  while($clubRow = mysql_fetch_array($selectClubRes))
  {
  	$clubArray['clubMasterId'][$k]   = $clubRow['subjectMasterId'];
  	$clubArray['clubName'][$k]       = $clubRow['subjectName'];
  	$k++;
  }
  
  $c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  $secArrOut[0] = 'A';
  $secArrOut[1] = 'B';
  $secArrOut[2] = 'C';
  $secArrOut[3] = 'D';
    
  include("./bottom.php");
  $smarty->assign("clubMasterId",$clubMasterId);
  $smarty->assign("nominalRollId",$nominalRollId);
  $smarty->assign("academicStartYear",$academicStartYear);
  $smarty->assign("academicEndYear",$academicEndYear);
  $smarty->assign("clubName",$clubName);
  $smarty->assign("class",$class);
  $smarty->assign("grNo",$grNo);
  $smarty->assign("section",$section);
  $smarty->assign("rollNo",$rollNo);
  $smarty->assign("boardRegistrionId",$boardRegistrionId);
  $smarty->assign("boardRollNo",$boardRollNo);
  $smarty->assign("clubArray",$clubArray);
  $smarty->assign("cArray",$cArray);
  $smarty->assign("secArrOut",$secArrOut);
  $smarty->assign("studentName",$studentName);
  $smarty->display("nominalRollEntry.tpl");
}
?>
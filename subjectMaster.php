<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$filePath = '';
  $fileName = '';
  
  $subjectId          = 0;
  $subjectName        = '';
  $subjectDescription = '';
  $subjectType        = '';
  $parentSubject      = '';
  
  if(isset($_POST['submitBtn']))
  {
    $uploaddir = dirname($_POST['filePath']);
  
    $uploadfile = $uploaddir . basename($_FILES['fileName']['name']);
    
    $target_path = 'data';
    $target_path = $target_path ."/". basename( $_FILES['fileName']['name']);
    $_FILES['fileName']['tmp_name']; // temp file
    
    $oldfile =  basename($_FILES['fileName']['name']);
  
    // getting the extention
  
    $pos = strpos($oldfile,".",0);
    $ext = trim(substr($oldfile,$pos+1,strlen($oldfile))," ");
    
    if(move_uploaded_file($_FILES['fileName']['tmp_name'], $target_path)) 
    {
    	$row = 0;
      $handle = fopen($target_path, "r");
      while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
      {
        if($row > 0)
        {
          $subjectId             = $data[0];
          $subjectName           = $data[1];
          $subjectDescription    = $data[2];
          $subjectType           = $data[3];
          $parentSubject         = $data[4];
         
          $insertsubMaster = "INSERT INTO subjectmaster(subjectId,subjectName,subjectDescription,subjectType)
        	                         VALUES (".$subjectId.",'".$subjectName."',
        	                                 '".addslashes($subjectDescription)."','".addslashes($subjectType)."')";
        	om_query($insertsubMaster);
  	    }
        $row++;
      }
    } 
    else
    {
      echo "There was an error uploading the file, please try again!";
    }
  }
}
?>
<BODY>
  <H1><CENTER><FONT color="red">Subject Master</FONT></CENTER></H1><HR>
<CENTER>
<FORM enctype="multipart/form-data" action="" name="subjectMaster" method="POST"> 
  <a href="index.php">Home</a>
	<br /><br />
  <INPUT size="60" type="hidden" name="filePath"><BR>
  File Name:
  <INPUT name="fileName" type="file" onChange="document.subjectMaster.filePath.value=document.subjectMaster.fileName.value;">
  <INPUT type="submit" name="submitBtn" value="Submit File"> 
</FORM>
</CENTER>
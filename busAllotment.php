<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$nominalArr = array();
	if(isset($_REQUEST['startYear']))
  {
  	$academicStartYear = $_REQUEST['startYear']."-04-01";
  	$nextYear          = $_REQUEST['startYear'] + 1;
  	$academicEndYear   = $nextYear."-03-31";
  	$academicStartYearSelected = $_REQUEST['startYear'];
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y')."-04-01";
	  	$nextYear          = date('Y') + 1;
	  	$academicEndYear   = $nextYear."-03-31";
	  	$academicStartYearSelected = date('Y');
		}
		else
		{
			$prevYear          = date('Y') - 1;
			$academicStartYear = $prevYear."-04-01";
	  	$academicEndYear   = date('Y')."-03-31";
	  	$academicStartYearSelected = $prevYear;
		}
	}
	
	$class   = isset($_REQUEST['class']) ? $_REQUEST['class'] : 0;
	$section = isset($_REQUEST['classSection']) ? $_REQUEST['classSection'] : 0;
	
	$selectId = "SELECT employeeMasterId
                 FROM employeemaster
                WHERE loginId = '".$_SESSION['s_activName']."'";
  $selectIdRes = mysql_query($selectId);
  if($idRow = mysql_fetch_array($selectIdRes))
  {
  	$selectClass = "SELECT class,section
	                    FROM classteacherallotment
	                   WHERE employeeMasterId = '".$idRow['employeeMasterId']."'
	                     AND academicStartYear = '".$academicStartYear."'
	                     AND academicEndYear = '".$academicEndYear."'";
	  $selectClassRes = mysql_query($selectClass);
	  if($classRow = mysql_fetch_array($selectClassRes))
	  {
	  	if($class == 0)
	  	{
		  	$class   = $classRow['class'];
		  	$section = $classRow['section'];
		  }
	  }
	  
	  if($class != 0)
	  {
	    $classQuery = "AND nominalroll.class = '".$class."'
	                   AND nominalroll.section = '".$section."'";
    }
    else
    {
    	$classQuery = "AND nominalroll.class = '".$class."'
	                   AND nominalroll.section = '".$section."'";
    	//$classQuery = "AND 1 = 1";
    }
    
	                 
	  $i = 0;
	  $selectNominal = "SELECT nominalRollId,nominalroll.grNo,academicStartYear,academicEndYear,nominalroll.class,
	                           nominalroll.section,rollNo,studentmaster.studentName,activated,studentmaster.studentLoginId,studentmaster.parentLoginId,
	                           nominalroll.busArrival,nominalroll.busDeparture,nominalroll.routeArrival,nominalroll.routeDeparture,
	                           nominalroll.busStopMasterId
	                      FROM nominalroll
	                 LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
	                     WHERE studentmaster.activated = 'Y'
	                       ".$classQuery."
	                       AND academicStartYear = '".$academicStartYear."'
	                       AND academicEndYear = '".$academicEndYear."'
	                  ORDER BY nominalroll.rollNo";
	  $selectNominalRes = mysql_query($selectNominal);
	  while($nominalRow = mysql_fetch_array($selectNominalRes))
	  {
	    $nominalArr[$i]['nominalRollId']      = $nominalRow['nominalRollId'];
	    $nominalArr[$i]['grNo']               = $nominalRow['grNo'];
	    $nominalArr[$i]['academicYear']       = substr($nominalRow['academicStartYear'],0,4).'-'.substr($nominalRow['academicEndYear'],0,4);
	    $nominalArr[$i]['class']              = $nominalRow['class'];
	    $nominalArr[$i]['section']            = $nominalRow['section'];
	    $nominalArr[$i]['rollNo']             = $nominalRow['rollNo'];
	    $nominalArr[$i]['studentName']        = $nominalRow['studentName'];
	    $nominalArr[$i]['activated']          = $nominalRow['activated'];
	    $nominalArr[$i]['busArrival']         = $nominalRow['busArrival'];
	    $nominalArr[$i]['busDeparture']       = $nominalRow['busDeparture'];
	    $nominalArr[$i]['routeArrival']       = $nominalRow['routeArrival'];
	    $nominalArr[$i]['routeDeparture']     = $nominalRow['routeDeparture'];
	    $nominalArr[$i]['busStopMasterId']    = $nominalRow['busStopMasterId'];
	    $i++;
	  }
	}
  
	if(isset($_POST['submit']))
	{
		$loopCount1 = 0;
	  while($loopCount1 < count($_POST['nominalRollId']))
	  {
	    $nominalRollId   = isset($_POST['nominalRollId'][$loopCount1]) && $_POST['nominalRollId'][$loopCount1] != '' ? $_POST['nominalRollId'][$loopCount1] : 0;
	    $busArrival      = isset($_POST['busArrival'][$loopCount1]) && $_POST['busArrival'][$loopCount1] != '' ? $_POST['busArrival'][$loopCount1] : 0;
		  $busDeparture    = isset($_POST['busDeparture'][$loopCount1]) && $_POST['busDeparture'][$loopCount1] != '' ? $_POST['busDeparture'][$loopCount1] : 0;
		  $routeArrival    = isset($_POST['routeArrival'][$loopCount1]) && $_POST['routeArrival'][$loopCount1] != '' ? $_POST['routeArrival'][$loopCount1] : 0;
		  $routeDeparture  = isset($_POST['routeDeparture'][$loopCount1]) && $_POST['routeDeparture'][$loopCount1] != '' ? $_POST['routeDeparture'][$loopCount1] : 0;
		  $busStopMasterId = isset($_POST['busStopMasterId'][$loopCount1]) && $_POST['busStopMasterId'][$loopCount1] != '' ? $_POST['busStopMasterId'][$loopCount1] : 0;
		  
	    if($busArrival > 0 || $busDeparture > 0 || $routeArrival > 0 || $routeDeparture > 0 || $busStopMasterId > 0)
	    {
	      $nominalEntry = "UPDATE nominalroll 
			  										SET busArrival = '".$busArrival."',
			  											  busDeparture = '".$busDeparture."',
			  											  routeArrival = '".$routeArrival."',
			  											  routeDeparture = '".$routeDeparture."',
			  											  busStopMasterId = '".$busStopMasterId."'
											  	WHERE nominalRollId  = ".$nominalRollId."";
			  $nominalEntryRes = om_query($nominalEntry);
			  if(!$nominalEntryRes)
			  {
			  	echo "Update Fail";
			  }
			  else
			  {
			    header("Location:busAllotment.php?done=1");
			  }
	    }
	    $loopCount1++;
	  }
  }
  
  $c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  $secArrOut[0] = 'A';
  $secArrOut[1] = 'B';
  $secArrOut[2] = 'C';
  $secArrOut[3] = 'D';
  
	
	$typeArr         = array();
	$rArr            = array();
	$tArr            = array();
	$sArr            = array();
	
	$k = 0;
  $selectClub = "SELECT vehicleMasterId,vehicleNo
                   FROM vehiclemaster";
  $selectClubRes = mysql_query($selectClub);
  while($clubRow = mysql_fetch_array($selectClubRes))
  {
  	$typeArr['vehicleMasterId'][$k] = $clubRow['vehicleMasterId'];
  	$typeArr['vehicleNo'][$k]   = $clubRow['vehicleNo'];
  	$k++;
  }
  
  $r = 0;
  $selectR = "SELECT routeMasterId,routeName
                FROM routemaster
               WHERE arrival = 'Arrival'";
  $selectRRes = mysql_query($selectR);
  while($rRow = mysql_fetch_array($selectRRes))
  {
  	$rArr['routeMasterId'][$r] = $rRow['routeMasterId'];
  	$rArr['routeName'][$r]     = $rRow['routeName'];
  	$r++;
  }
  
  $t = 0;
  $selectT = "SELECT routeMasterId,routeName
                FROM routemaster
               WHERE arrival = 'Departure'";
  $selectTRes = mysql_query($selectT);
  while($tRow = mysql_fetch_array($selectTRes))
  {
  	$tArr['routeMasterId'][$t] = $tRow['routeMasterId'];
  	$tArr['routeName'][$t]     = $tRow['routeName'];
  	$t++;
  }
  
  $s = 0;
  $selectS = "SELECT busStopMasterId,busStop
                FROM busstopmaster";
  $selectSRes = mysql_query($selectS);
  while($sRow = mysql_fetch_array($selectSRes))
  {
  	$sArr['busStopMasterId'][$s] = $sRow['busStopMasterId'];
  	$sArr['busStop'][$s]         = $sRow['busStop'];
  	$s++;
  }
  
  include("./bottom.php");
  $smarty->assign('class',$class);
  $smarty->assign('section',$section);
  $smarty->assign('cArray',$cArray);
  $smarty->assign('secArrOut',$secArrOut);
  $smarty->assign('nominalArr',$nominalArr);
  $smarty->assign('academicStartYear',$academicStartYear);
  $smarty->assign('academicEndYear',$academicEndYear);
  $smarty->assign('academicStartYearSelected',$academicStartYearSelected);
  $smarty->assign('typeArr',$typeArr);
  $smarty->assign('rArr',$rArr);
  $smarty->assign('tArr',$tArr);
  $smarty->assign('sArr',$sArr);
  $smarty->display('busAllotment.tpl');  
}
?>
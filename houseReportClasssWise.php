<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$countTotal = '';
	$attendenceArr = array();
  $i = 0;
  
  if(isset($_REQUEST['startYear']))
  {
	  $academicStartYear = $_REQUEST['startYear'];
	  $academicEndYear   = $_REQUEST['startYear'] + 1;
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y');
	  	$nextYear            = date('Y') + 1;
	  	$academicEndYear   = $nextYear;
		}
		else
		{
			$prevYear            = date('Y') - 1;
			$academicStartYear = $prevYear;
	  	$academicEndYear   = date('Y');
		}
	}
	
	$class   = isset($_REQUEST['class']) ? $_REQUEST['class'] : 0;
	$section = isset($_REQUEST['classSection']) ? $_REQUEST['classSection'] : 0;
	
	if($class != 0)
	{
	  $classQuery = "AND nominalroll.class = '".$class."'
									 AND nominalroll.section = '".$section."'
									 AND nominalroll.academicStartYear = '".$academicStartYear."-04-01'
									 AND nominalroll.academicEndYear = '".$academicEndYear."-03-31'";
	}
	else
	{
		$classQuery = "AND 1 = 1";
	}
	
  $selectAttendM = "SELECT houseId,houseName
                      FROM house";
  $selectAttendMRes = mysql_query($selectAttendM);
  while($maRow = mysql_fetch_array($selectAttendMRes))
  {
  	$attendenceArr[$i]['houseId']   = $maRow['houseId'];
  	$attendenceArr[$i]['houseName'] = $maRow['houseName'];
  	
  	$selectAtnP = "SELECT gender
                     FROM studentmaster
                    WHERE houseId = '".$maRow['houseId']."'
                      ".$classQuery."
                      AND gender = 'Male'
                      AND activated = 'Y'";
    $selectAtnPRes = mysql_query($selectAtnP);
    $countP = mysql_num_rows($selectAtnPRes);
    $attendenceArr[$i]['countP'] = $countP;
    
    $selectAtnA = "SELECT gender
                     FROM studentmaster
                    WHERE houseId = '".$maRow['houseId']."'
                      ".$classQuery."
                      AND gender = 'Female'
                      AND activated = 'Y'";
    $selectAtnARes = mysql_query($selectAtnA);
    $countA = mysql_num_rows($selectAtnARes);
    $attendenceArr[$i]['countA'] = $countA;
    $attendenceArr[$i]['totalNumberWorkingDay'] = $countA + $countP;
    
    $countTotal += $attendenceArr[$i]['totalNumberWorkingDay'];
  	$i++;
  }
  
  $c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  $secArrOut[0] = 'A';
  $secArrOut[1] = 'B';
  $secArrOut[2] = 'C';
  $secArrOut[3] = 'D';
  
  include("./bottom.php");
  $smarty->assign('cArray',$cArray);
	$smarty->assign('class',$class);
  $smarty->assign('section',$section);
  $smarty->assign('secArrOut',$secArrOut);
  $smarty->assign('academicStartYear',$academicStartYear);
  $smarty->assign('academicEndYear',$academicEndYear);
  $smarty->assign('attendenceArr',$attendenceArr);
  $smarty->assign('countTotal',$countTotal);
  $smarty->display('houseReportClasssWise.tpl');
}
?>
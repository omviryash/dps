<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$attendenceArr  = array();
	$atnArr         = array();
  $today          = date('Y-m');
  
  $academicStartYear = '';
  $academicEndYear   = '';
	//$totalNumberWorkingDay = 0;
	
	if(isset($_REQUEST['attendenceYear']))
	{

		// added to set correct requested date value to compare with  #hari
		if(isset($_REQUEST['attendenceMonth']))
		{
			// Considering some date (15th) from the same month to compare
			$todayAcademic = $_REQUEST['attendenceMonth']."-15";
		}
		else
		{
		  $todayAcademic = date('m-d');
		}

		// WRONG need to set according to get details from user #hari
		// $todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = $_REQUEST['attendenceYear']."-04-01";
	  	$nextYear          = $_REQUEST['attendenceYear'] + 1;
	  	$academicEndYear   = $nextYear."-03-31";
		}
		else
		{
			$prevYear          = $_REQUEST['attendenceYear'] - 1;
			$academicStartYear = $prevYear."-04-01";
                        $academicEndYear   = $_REQUEST['attendenceYear']."-03-31";
		}


	}
	
	$class   = isset($_REQUEST['class']) ? $_REQUEST['class'] : 0;
	$section = isset($_REQUEST['classSection']) ? $_REQUEST['classSection'] : 0;
	
	if(isset($_REQUEST['attendenceYear']))
	{
	  $attendenceDate = $_REQUEST['attendenceYear']."-".$_REQUEST['attendenceMonth'];
	}
	else
	{
		$attendenceDate = $today;
	}
  
  $selectId = "SELECT employeeMasterId
                 FROM employeemaster
                WHERE loginId = '".$_SESSION['s_activName']."'";
  $selectIdRes = mysql_query($selectId);
  if($idRow = mysql_fetch_array($selectIdRes))
  {
  	if($_SESSION['s_userType'] != "Administrator") {

	  	$selectClass = "SELECT class,section
		                    FROM classteacherallotment
		                   WHERE employeeMasterId = '".$idRow['employeeMasterId']."'
		                     AND academicStartYear = '".$academicStartYear."'
	                       AND academicEndYear = '".$academicEndYear."'";
		  $selectClassRes = mysql_query($selectClass);
		  if($classRow = mysql_fetch_array($selectClassRes))
		  {
		  	if($class == 0)
		  	{
			  	$class   = $classRow['class'];
			  	$section = $classRow['section'];
			  }
			}
  	} else {
	  	$class   = $_REQUEST['class'];
	  	$section = $_REQUEST['classSection'];
  	}

	  $i = 0;

	  // NEW QUERY  #hari
	  // **Note: here we are taking max(rollno) because some times it's 0 for some values
		$selectAttendM = "SELECT
						`studentmaster`.`grNo` AS grNo,
						`studentmaster`.`studentName` AS studentName,
						`studentmaster`.`activated` AS activated,
						`attendence`.`class` AS class,
						`attendence`.`section` AS section,
						MAX(`attendence`.`rollNo`) AS rollNo 
					FROM `attendence`
					LEFT JOIN `studentmaster` ON `studentmaster`.`grNo` = `attendence`.`grNo`
					WHERE `attendence`.`class` =  '".$class."'
					AND `attendence`.`section` =  '".$section."'
					AND `attendence`.`date` >=  '".$academicStartYear."'
					AND `attendence`.`date` <=  '".$academicEndYear."'
					AND `studentmaster`.`activated` =  'Y'
					GROUP BY `attendence`.`grNo`
					ORDER BY `attendence`.`rollNo` ASC 
					LIMIT 0 , 60";
		//		echo $selectAttendM ; exit;

		// OLD QUERY COMMENTED BY Hari #hari
		/*
	  $selectAttendM= "SELECT studentmaster.grNo,studentmaster.studentName,studentmaster.activated,nominalroll.class,nominalroll.section,rollNo
	                     FROM nominalroll
	                LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
	                    WHERE nominalroll.class = '".$class."'
	                      AND nominalroll.section = '".$section."'
	                      AND nominalroll.academicStartYear = '".$academicStartYear."'
	                      AND nominalroll.academicEndYear = '".$academicEndYear."'
	                      AND studentmaster.activated = 'Y'";
	  */
	  $selectAttendMRes = mysql_query($selectAttendM);
	  while($maRow = mysql_fetch_array($selectAttendMRes))
	  {
	  	$attendenceArr[$i]['rollNo']      = $maRow['rollNo'];
	  	$attendenceArr[$i]['grNo']        = $maRow['grNo'];
	  	$attendenceArr[$i]['studentName'] = $maRow['studentName'];
	  	$attendenceArr[$i]['class']       = $maRow['class'];
	  	$attendenceArr[$i]['section']     = $maRow['section'];
	  	
	  	$selectAtnP = "SELECT attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date >= '".$attendenceDate."-01'
	                      AND date <= '".$attendenceDate."-31'
	                      AND attendences = 'P' Group By `date`";
	    $selectAtnPRes = mysql_query($selectAtnP);
	    $countP = mysql_num_rows($selectAtnPRes);
		$attendenceArr[$i]['countP'] = $countP;
	    
	    $selectAtnA = "SELECT attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date >= '".$attendenceDate."-01'
	                      AND date <= '".$attendenceDate."-31'
	                      AND attendences = 'A' Group By `date`";
	    $selectAtnARes = mysql_query($selectAtnA);
	    $countA = mysql_num_rows($selectAtnARes);
	    $attendenceArr[$i]['countA'] = $countA;
	    $attendenceArr[$i]['totalNumberWorkingDay'] = $countA + $countP;
	    //$totalNumberWorkingDay = $countA + $countP;
	    
	    $attendenceArr[$i]['attendences01'] = '';
	    $attendenceArr[$i]['attendences02'] = '';
	    $attendenceArr[$i]['attendences03'] = '';
	    $attendenceArr[$i]['attendences04'] = '';
	    $attendenceArr[$i]['attendences05'] = '';
	    $attendenceArr[$i]['attendences06'] = '';
	    $attendenceArr[$i]['attendences07'] = '';
	    $attendenceArr[$i]['attendences08'] = '';
	    $attendenceArr[$i]['attendences09'] = '';
	    $attendenceArr[$i]['attendences10'] = '';
	    $attendenceArr[$i]['attendences11'] = '';
	    $attendenceArr[$i]['attendences12'] = '';
	    $attendenceArr[$i]['attendences13'] = '';
	    $attendenceArr[$i]['attendences14'] = '';
	    $attendenceArr[$i]['attendences15'] = '';
	    $attendenceArr[$i]['attendences16'] = '';
	    $attendenceArr[$i]['attendences17'] = '';
	    $attendenceArr[$i]['attendences18'] = '';
	    $attendenceArr[$i]['attendences19'] = '';
	    $attendenceArr[$i]['attendences20'] = '';
	    $attendenceArr[$i]['attendences21'] = '';
	    $attendenceArr[$i]['attendences22'] = '';
	    $attendenceArr[$i]['attendences23'] = '';
	    $attendenceArr[$i]['attendences24'] = '';
	    $attendenceArr[$i]['attendences25'] = '';
	    $attendenceArr[$i]['attendences26'] = '';
	    $attendenceArr[$i]['attendences27'] = '';
	    $attendenceArr[$i]['attendences28'] = '';
	    $attendenceArr[$i]['attendences29'] = '';
	    $attendenceArr[$i]['attendences30'] = '';
	    $attendenceArr[$i]['attendences31'] = '';

	    //Main Monthly Attendences Start
	    $selectAtnP = "SELECT date,grNo,attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date between '".$attendenceDate."-01' and '".$attendenceDate."-31'";
	    // echo $selectAtnP."<br/>";
	    $selectAtnPRes = mysql_query($selectAtnP);
	    while($allRow = mysql_fetch_array($selectAtnPRes))
	    {
	    	$day = date('d',strtotime($allRow['date']));
		    $attendenceArr[$i]['attendences'.$day] = $allRow['attendences'];
	    }
	    //echo $selectAtnP; exit;

	    $selectAtnP = "SELECT date,grNo,attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date = '".$attendenceDate."-02'";
            //echo $selectAtnP;                   
	    $selectAtnPRes = mysql_query($selectAtnP);
	    if($allRow = mysql_fetch_array($selectAtnPRes))
	    {
		    $attendenceArr[$i]['attendences2'] = $allRow['attendences'];
	    }
	    
	    /*$selectAtnP = "SELECT date,grNo,attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date = '".$attendenceDate."-03'";
	    $selectAtnPRes = mysql_query($selectAtnP);
	    if($allRow = mysql_fetch_array($selectAtnPRes))
	    {
		    $attendenceArr[$i]['attendences3'] = $allRow['attendences'];
	    }
	    
	    $selectAtnP = "SELECT date,grNo,attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date = '".$attendenceDate."-04'";
	    $selectAtnPRes = mysql_query($selectAtnP);
	    if($allRow = mysql_fetch_array($selectAtnPRes))
	    {
		    $attendenceArr[$i]['attendences4'] = $allRow['attendences'];
	    }
	    
	    $selectAtnP = "SELECT date,grNo,attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date = '".$attendenceDate."-05'";
						  //echo $selectAtnP."<br /><br />";
	    $selectAtnPRes = mysql_query($selectAtnP);
	    if($allRow = mysql_fetch_array($selectAtnPRes))
	    {
		    $attendenceArr[$i]['attendences5'] = $allRow['attendences'];
	    }
	    
	    $selectAtnP = "SELECT date,grNo,attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date = '".$attendenceDate."-06'";
	    $selectAtnPRes = mysql_query($selectAtnP);
	    if($allRow = mysql_fetch_array($selectAtnPRes))
	    {
		    $attendenceArr[$i]['attendences6'] = $allRow['attendences'];
	    }
	    
	    $selectAtnP = "SELECT date,grNo,attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date = '".$attendenceDate."-07'";
	    $selectAtnPRes = mysql_query($selectAtnP);
	    if($allRow = mysql_fetch_array($selectAtnPRes))
	    {
		    $attendenceArr[$i]['attendences7'] = $allRow['attendences'];
	    }
	    
	    $selectAtnP = "SELECT date,grNo,attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date = '".$attendenceDate."-08'";
	    $selectAtnPRes = mysql_query($selectAtnP);
	    if($allRow = mysql_fetch_array($selectAtnPRes))
	    {
		    $attendenceArr[$i]['attendences8'] = $allRow['attendences'];
	    }
	    
	    $selectAtnP = "SELECT date,grNo,attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date = '".$attendenceDate."-09'";
	    $selectAtnPRes = mysql_query($selectAtnP);
	    if($allRow = mysql_fetch_array($selectAtnPRes))
	    {
		    $attendenceArr[$i]['attendences9'] = $allRow['attendences'];
	    }
	    
	    $selectAtnP = "SELECT date,grNo,attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date = '".$attendenceDate."-10'";
	    $selectAtnPRes = mysql_query($selectAtnP);
	    if($allRow = mysql_fetch_array($selectAtnPRes))
	    {
		    $attendenceArr[$i]['attendences10'] = $allRow['attendences'];
	    }
	    
	    $selectAtnP = "SELECT date,grNo,attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date = '".$attendenceDate."-11'";
	    $selectAtnPRes = mysql_query($selectAtnP);
	    if($allRow = mysql_fetch_array($selectAtnPRes))
	    {
		    $attendenceArr[$i]['attendences11'] = $allRow['attendences'];
	    }
	    
	    $selectAtnP = "SELECT date,grNo,attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date = '".$attendenceDate."-12'";
	    $selectAtnPRes = mysql_query($selectAtnP);
	    if($allRow = mysql_fetch_array($selectAtnPRes))
	    {
		    $attendenceArr[$i]['attendences12'] = $allRow['attendences'];
	    }
	    
	    $selectAtnP = "SELECT date,grNo,attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date = '".$attendenceDate."-13'";
	    $selectAtnPRes = mysql_query($selectAtnP);
	    if($allRow = mysql_fetch_array($selectAtnPRes))
	    {
		    $attendenceArr[$i]['attendences13'] = $allRow['attendences'];
	    }
	    
	    $selectAtnP = "SELECT date,grNo,attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date = '".$attendenceDate."-14'";
	    $selectAtnPRes = mysql_query($selectAtnP);
	    if($allRow = mysql_fetch_array($selectAtnPRes))
	    {
		    $attendenceArr[$i]['attendences14'] = $allRow['attendences'];
	    }
	    
	    $selectAtnP = "SELECT date,grNo,attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date = '".$attendenceDate."-15'";
	    $selectAtnPRes = mysql_query($selectAtnP);
	    if($allRow = mysql_fetch_array($selectAtnPRes))
	    {
		    $attendenceArr[$i]['attendences15'] = $allRow['attendences'];
	    }
	    
	    $selectAtnP = "SELECT date,grNo,attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date = '".$attendenceDate."-16'";
	    $selectAtnPRes = mysql_query($selectAtnP);
	    if($allRow = mysql_fetch_array($selectAtnPRes))
	    {
		    $attendenceArr[$i]['attendences16'] = $allRow['attendences'];
	    }
	    
	    $selectAtnP = "SELECT date,grNo,attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date = '".$attendenceDate."-17'";
	    $selectAtnPRes = mysql_query($selectAtnP);
	    if($allRow = mysql_fetch_array($selectAtnPRes))
	    {
		    $attendenceArr[$i]['attendences17'] = $allRow['attendences'];
	    }
	    
	    $selectAtnP = "SELECT date,grNo,attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date = '".$attendenceDate."-18'";
	    $selectAtnPRes = mysql_query($selectAtnP);
	    if($allRow = mysql_fetch_array($selectAtnPRes))
	    {
		    $attendenceArr[$i]['attendences18'] = $allRow['attendences'];
	    }
	    
	    $selectAtnP = "SELECT date,grNo,attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date = '".$attendenceDate."-19'";
	    $selectAtnPRes = mysql_query($selectAtnP);
	    if($allRow = mysql_fetch_array($selectAtnPRes))
	    {
		    $attendenceArr[$i]['attendences19'] = $allRow['attendences'];
	    }
	    
	    $selectAtnP = "SELECT date,grNo,attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date = '".$attendenceDate."-20'";
	    $selectAtnPRes = mysql_query($selectAtnP);
	    if($allRow = mysql_fetch_array($selectAtnPRes))
	    {
		    $attendenceArr[$i]['attendences20'] = $allRow['attendences'];
	    }
	    
	    $selectAtnP = "SELECT date,grNo,attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date = '".$attendenceDate."-21'";
	    $selectAtnPRes = mysql_query($selectAtnP);
	    if($allRow = mysql_fetch_array($selectAtnPRes))
	    {
		    $attendenceArr[$i]['attendences21'] = $allRow['attendences'];
	    }
	    
	    $selectAtnP = "SELECT date,grNo,attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date = '".$attendenceDate."-22'";
	    $selectAtnPRes = mysql_query($selectAtnP);
	    if($allRow = mysql_fetch_array($selectAtnPRes))
	    {
		    $attendenceArr[$i]['attendences22'] = $allRow['attendences'];
	    }
	    
	    $selectAtnP = "SELECT date,grNo,attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date = '".$attendenceDate."-23'";
	    $selectAtnPRes = mysql_query($selectAtnP);
	    if($allRow = mysql_fetch_array($selectAtnPRes))
	    {
		    $attendenceArr[$i]['attendences23'] = $allRow['attendences'];
	    }
	    
	    $selectAtnP = "SELECT date,grNo,attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date = '".$attendenceDate."-24'";
	    $selectAtnPRes = mysql_query($selectAtnP);
	    if($allRow = mysql_fetch_array($selectAtnPRes))
	    {
		    $attendenceArr[$i]['attendences24'] = $allRow['attendences'];
	    }
	    
	    $selectAtnP = "SELECT date,grNo,attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date = '".$attendenceDate."-25'";
	    $selectAtnPRes = mysql_query($selectAtnP);
	    if($allRow = mysql_fetch_array($selectAtnPRes))
	    {
		    $attendenceArr[$i]['attendences25'] = $allRow['attendences'];
	    }
	    
	    $selectAtnP = "SELECT date,grNo,attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date = '".$attendenceDate."-26'";
	    $selectAtnPRes = mysql_query($selectAtnP);
	    if($allRow = mysql_fetch_array($selectAtnPRes))
	    {
		    $attendenceArr[$i]['attendences26'] = $allRow['attendences'];
	    }
	    
	    $selectAtnP = "SELECT date,grNo,attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date = '".$attendenceDate."-27'";
	    $selectAtnPRes = mysql_query($selectAtnP);
	    if($allRow = mysql_fetch_array($selectAtnPRes))
	    {
		    $attendenceArr[$i]['attendences27'] = $allRow['attendences'];
	    }
	    
	    $selectAtnP = "SELECT date,grNo,attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date = '".$attendenceDate."-28'";
	    $selectAtnPRes = mysql_query($selectAtnP);
	    if($allRow = mysql_fetch_array($selectAtnPRes))
	    {
		    $attendenceArr[$i]['attendences28'] = $allRow['attendences'];
	    }
	    
	    $selectAtnP = "SELECT date,grNo,attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date = '".$attendenceDate."-29'";
	    $selectAtnPRes = mysql_query($selectAtnP);
	    if($allRow = mysql_fetch_array($selectAtnPRes))
	    {
		    $attendenceArr[$i]['attendences29'] = $allRow['attendences'];
	    }
	    
	    $selectAtnP = "SELECT date,grNo,attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date = '".$attendenceDate."-30'";
	    $selectAtnPRes = mysql_query($selectAtnP);
	    if($allRow = mysql_fetch_array($selectAtnPRes))
	    {
		    $attendenceArr[$i]['attendences30'] = $allRow['attendences'];
	    }
	    
	    $selectAtnP = "SELECT date,grNo,attendences
	                     FROM attendence
	                    WHERE class = '".$maRow['class']."'
	                      AND section = '".$maRow['section']."'
	                      AND grNo = '".$maRow['grNo']."'
	                      AND date = '".$attendenceDate."-31'";
	    $selectAtnPRes = mysql_query($selectAtnP);
	    if($allRow = mysql_fetch_array($selectAtnPRes))
	    {
		    $attendenceArr[$i]['attendences31'] = $allRow['attendences'];
	    }*/
	    
	    //Main Monthly Attendences End
	  	$i++;
	  }
	}
  
  $c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  $secArrOut[0] = 'A';
  $secArrOut[1] = 'B';
  $secArrOut[2] = 'C';
  $secArrOut[3] = 'D';
  
  include("./bottom.php");
  $smarty->assign('attendenceArr',$attendenceArr);
  $smarty->assign('atnArr',$atnArr);
  $smarty->assign('attendenceDate',$attendenceDate);
  $smarty->assign('cArray',$cArray);
  $smarty->assign('class',$class);
  $smarty->assign('section',$section);
  $smarty->assign('secArrOut',$secArrOut);
  //$smarty->assign('totalNumberWorkingDay',$totalNumberWorkingDay);
  $smarty->display('attendenceReportMonthly.tpl');
}
?>
<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$feeType      = "";
	$feeTypeId    = "";
	$isEdit        = 0;
	
  if(isset($_POST['Submit']))
  {
  	$feeType   = isset($_POST['feeType']) ? $_POST['feeType'] : "";
  	$feeTypeId = isset($_POST['feeTypeId']) ? $_POST['feeTypeId'] : 0;
  	if($feeTypeId == 0)
  	{
  	  $insertFeeType = "INSERT INTO feetype (feeType)
  	                     VALUES('".$feeType."')";
  	  $insertFeeTypeRes = om_query($insertFeeType);
  	  if(!$insertFeeTypeRes)
  	  {
  	  	echo "Insert Fail";
  	  }
  	  else
  	  {
  	  	header("Location:feeType.php?done=1");
  	  }
  	}
  	else
  	{
  	  $isEdit = 1;
  	  $updateFeeType = "UPDATE feetype
	                         SET feeType = '".$feeType."'
	                       WHERE feeTypeId = ".$_REQUEST['feeTypeId'];
      $updateFeeTypeRes = om_query($updateFeeType);
      if(!$updateFeeTypeRes)
      {
      	echo "Update Fail";
      }
      else
      {
      	header("Location:feeType.php?done=1");
      }
  	}
  }
  
  if(isset($_REQUEST['feeTypeId']) > 0)
  {
    $selectFeeType = "SELECT feeTypeId,feeType
                        FROM feetype
                       WHERE feeTypeId = ".$_REQUEST['feeTypeId'];
    $selectFeeTypeRes = mysql_query($selectFeeType);
    if($feeTypeRow = mysql_fetch_array($selectFeeTypeRes))
    {
    	$feeTypeId         = $feeTypeRow['feeTypeId'];
    	$feeType           = $feeTypeRow['feeType'];
    }
  }
  include("./bottom.php");
  $smarty->assign('feeType',$feeType);
  $smarty->assign('feeTypeId',$feeTypeId);
  $smarty->assign('isEdit',$isEdit);
  $smarty->display('feeType.tpl');  
}
?>
<?php
define('FPDF_FONTPATH', 'font/');
require('./font/fpdf.php');
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
  $pdf=new FPDF('P','mm','A4');   //Create new pdf file
  $pdf->Open();     //Open file
  $pdf->SetAutoPageBreak(false);  //Disable automatic page break
  $pdf->AddPage();  //Add first page


  //header part start
  //header part end
  //table header part start  
  

  
  $i         = 0; 
  $rowHeight = 70;
  $yAxis     = 10;

  //table header part end 
    
  $todayAcademic = date('m-d');
	if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
	{
  	$academicStartYear = date('Y')."-04-01";
  	$nextYear          = date('Y') + 1;
  	$academicEndYear   = $nextYear."-03-31";
	}
	else
	{
		$prevYear          = date('Y') - 1;
		$academicStartYear = $prevYear."-04-01";
  	$academicEndYear   = date('Y')."-03-31";
	}
	$class   = isset($_REQUEST['class']) ? $_REQUEST['class'] : 0;
	$section = isset($_REQUEST['section']) ? $_REQUEST['section'] : 0;
	
	//print column titles for the actual page
  
  $selectNominal = "SELECT nominalroll.grNo,nominalroll.academicStartYear,nominalroll.academicEndYear,nominalroll.class,
                           nominalroll.section,nominalroll.rollNo,studentmaster.activated,studentmaster.studentName,
                           DATE_FORMAT(studentmaster.dateOfBirth, '%d-%m-%Y') AS dateOfBirth,house.houseName,studentmaster.bloodGroup,
                           nominalroll.busRoute,studentmaster.fatherMobile,studentmaster.currentAddress
                      FROM nominalroll
                 LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
                 LEFT JOIN house ON house.houseId = studentmaster.houseId
                     WHERE nominalroll.class = '".$class."'
                       AND nominalroll.section = '".$section."'
                       AND nominalroll.academicStartYear = '".$academicStartYear."'
                       AND nominalroll.academicEndYear = '".$academicEndYear."'
                       AND studentmaster.activated = 'Y'
                  ORDER BY nominalroll.rollNo";
  $selectNominalRes = mysql_query($selectNominal);
  while($nominalRow = mysql_fetch_array($selectNominalRes))
  {
  	if ($i == 4)
    {
      $pdf->AddPage();
      //Go to next row
      $yAxis = 10;
      
      //Set $i variable to 0 (first row)
      $i = 0;
    }
    
    $academicYear = substr($nominalRow['academicStartYear'],0,4).'-'.substr($nominalRow['academicEndYear'],0,4);
    $studentName  = $nominalRow['studentName'];
    $class        = $nominalRow['class'];
    $section      = $nominalRow['section'];
    $dateOfBirth  = $nominalRow['dateOfBirth'];
    $houseName    = $nominalRow['houseName'];
    $bloodGroup   = $nominalRow['bloodGroup'];
    $grNo         = $nominalRow['grNo'];
    $busRoute     = $nominalRow['busRoute'];
    $fatherMobile = $nominalRow['fatherMobile'];
    $address      = $nominalRow['currentAddress'];
    
    $filename = 'studentImage/'.$grNo.'_P.jpg';

		if(file_exists($filename))
		{
		  $pdf->Image('studentImage/'.$grNo.'_P.jpg',10,$yAxis,30,35);
		}
		else
		{
		  $pdf->Image('studentImage/P.jpg',10,$yAxis,30,35);
		}

    $pdf->SetFillColor(232, 232, 232);
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetXY(45,$yAxis);
    $pdf->Cell(45, 5, 'Academic Year : '.$academicYear,       0,0,'L');
    $pdf->SetXY(45,$yAxis);
    $pdf->Cell(45, 15, 'Name : '.$studentName,         0,0,'L');
    $pdf->SetXY(45,$yAxis);
    $pdf->Cell(45, 25, 'Class : '.$class.' - '.$section, 0,0,'L');
    $pdf->SetXY(45,$yAxis);
    $pdf->Cell(45, 35, 'D.O.B. : '.$dateOfBirth, 0,0,'L');
    $pdf->SetXY(45,$yAxis);
    $pdf->Cell(45, 45, 'House : '.$houseName, 0,0,'L');
    $pdf->SetXY(45,$yAxis);
    $pdf->Cell(45, 55, 'Blood Group : '.$bloodGroup, 0,0,'L');
    $pdf->SetXY(45,$yAxis);
    $pdf->Cell(45, 65, 'Admisson No : '.$grNo, 0,0,'L');
    $pdf->SetXY(45,$yAxis);
    $pdf->Cell(45, 75, 'Phone No : '.$fatherMobile, 0,0,'L');
    $pdf->SetXY(45,$yAxis);
    $pdf->Cell(45, 85, 'Bus Number : '.$busRoute, 0,0,'L');
    $pdf->SetFillColor(232, 232, 232);
    $pdf->SetFont('Arial', '', 10);
    $pdf->SetXY(45,$yAxis+45);
    $pdf->MultiCell(155, 5, 'Address : '.$address, 0,'L',0);
    
    $pdf->line(5,$yAxis-5,205,$yAxis-5);
    $pdf->line(5,285,205,285);
    $pdf->line(5,5,5,285);
    $pdf->line(205,5,205,285);
    $yAxis = $yAxis + $rowHeight;
    $i = $i + 1;
  }
  
  $pdf->Output();
}
?>
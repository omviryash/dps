<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$today = date('Y-m-d');
	$academicStartYearSecondNum = 0;
	if(isset($_REQUEST['attendenceYear']))
	{
	  $attendDate = $_REQUEST['attendenceYear']."-".$_REQUEST['attendenceMonth']."-".$_REQUEST['attendenceDay'];
	}
	else
	{
		$attendDate = $today;
	}
	
	if(isset($_REQUEST['startYear']))
  {
  	$academicStartYear = $_REQUEST['startYear']."-04-01";
  	$nextYear          = $_REQUEST['startYear'] + 1;
  	$academicEndYear   = $nextYear."-03-31";
  	$academicStartYearSelected = $_REQUEST['startYear'];
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y')."-04-01";
	  	$nextYear          = date('Y') + 1;
	  	$academicEndYear   = $nextYear."-03-31";
	  	$academicStartYearSelected = date('Y');
		}
		else
		{
			$prevYear          = date('Y') - 1;
			$academicStartYear = $prevYear."-04-01";
	  	$academicEndYear   = date('Y')."-03-31";
	  	$academicStartYearSelected = $prevYear;
		}
	}
	
	$feeTypeId = isset($_REQUEST['feeTypeId']) ? $_REQUEST['feeTypeId'] : 0;
	$class   = isset($_REQUEST['class']) ? $_REQUEST['class'] : 0;
	$section = isset($_REQUEST['classSection']) ? $_REQUEST['classSection'] : 0;
	
	if($class != '')
	{
		$newQuery = "AND nominalroll.class = '".$class."'
	               AND nominalroll.section = '".$section."'";
	}
	else
	{
		$newQuery = '';
	}
	
	if($feeTypeId != '')
	{
		$newQuery2 = "AND feecollection.feeTypeId = ".$feeTypeId."";
	}
	else
	{
		$newQuery2 = '';
	}
	
	if(isset($_REQUEST['submitNew']))
	{
		$newQuery1 = "AND feecollection.feeDate = '".$attendDate."'";
	}
	else
	{
		$newQuery1 = '';
	}
	
	$feeCollectionId = 0;
	$attendenceArr    = array();
	$i = 0;
  $selectAttendM= "SELECT feecollection.feeCollectionId,feecollection.grNo,feecollection.feeName,feecollection.amount,
                          feecollection.rNo,DATE_FORMAT(feecollection.feeDate, '%d-%m-%Y') AS feeDate,feecollection.modeOfPay,
                          studentmaster.studentName,feecollection.chNo,feecollection.discount,feetype.feeType,
                          nominalroll.class,nominalroll.section,nominalroll.academicStartYear,nominalroll.academicEndYear
                     FROM feecollection
                LEFT JOIN studentmaster ON studentmaster.grNo = feecollection.grNo
                LEFT JOIN nominalroll ON nominalroll.grNo = studentmaster.grNo
                LEFT JOIN feetype ON feetype.feeTypeId = feecollection.feeTypeId
                    WHERE 1 = 1
                      ".$newQuery."
                      ".$newQuery1."
                      ".$newQuery2."
                      AND feecollection.feeDate > '".$academicStartYear."'
	                    AND feecollection.feeDate < '".$academicEndYear."'
                      AND nominalroll.academicStartYear < '".$attendDate."'
                      AND nominalroll.academicEndYear > '".$attendDate."'";
  $selectAttendMRes = mysql_query($selectAttendM);
  while($maRow = mysql_fetch_array($selectAttendMRes))
  {
  	$attendenceArr[$i]['feeCollectionId'] = $maRow['feeCollectionId'];
  	$attendenceArr[$i]['feeDate']         = $maRow['feeDate'];
  	$attendenceArr[$i]['grNo']            = $maRow['grNo'];
  	$attendenceArr[$i]['studentName']     = $maRow['studentName'];
  	$attendenceArr[$i]['class']           = $maRow['class'];
  	$attendenceArr[$i]['section']         = $maRow['section'];
  	$attendenceArr[$i]['feeType']         = $maRow['feeType'];
  	$attendenceArr[$i]['feeName']         = $maRow['feeName'];
  	$attendenceArr[$i]['amount']          = $maRow['amount'];
  	$attendenceArr[$i]['rNo']             = $maRow['rNo'];
  	$attendenceArr[$i]['modeOfPay']       = $maRow['modeOfPay'];
  	$attendenceArr[$i]['chNo']            = $maRow['chNo'];
  	$attendenceArr[$i]['discount']        = $maRow['discount'];
  	$i++;
  }
  
  $c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  $secArrOut[0] = 'A';
  $secArrOut[1] = 'B';
  $secArrOut[2] = 'C';
  $secArrOut[3] = 'D';
  
  
  $k = 0;
  $selectClub = "SELECT feeTypeId,feeType
                   FROM feetype";
  $selectClubRes = mysql_query($selectClub);
  while($clubRow = mysql_fetch_array($selectClubRes))
  {
  	$typeArr['feeTypeId'][$k]   = $clubRow['feeTypeId'];
  	$typeArr['feeType'][$k]     = $clubRow['feeType'];
  	$k++;
  }
  
  $academicStartYearSecondNum = substr($academicStartYearSelected + 1,-2);
  include("./bottom.php");
  $smarty->assign('cArray',$cArray);
  $smarty->assign('class',$class);
  $smarty->assign('section',$section);
  $smarty->assign('secArrOut',$secArrOut);
  $smarty->assign('feeCollectionId',$feeCollectionId);
  $smarty->assign('attendenceArr',$attendenceArr);
  $smarty->assign('attendDate',$attendDate);
  $smarty->assign('feeTypeId',$feeTypeId);
  $smarty->assign('typeArr',$typeArr);
  $smarty->assign('academicStartYear',$academicStartYear);
  $smarty->assign('academicStartYearSelected',$academicStartYearSelected);
  $smarty->assign('academicStartYearSecondNum',$academicStartYearSecondNum);
  $smarty->display('feeTransactionList.tpl');  
}
?>
<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$clubArray     = array();
  $i = 0;
  $selectClass = "SELECT subjectMasterId,subjectName,subjectCode,subjectType,startClass,endClass,subjectDescription
                    FROM subjectmaster
                   WHERE subjectType != 'Club'
                     AND subjectType != 'Art Education'
                ORDER BY subjectType";
  $selectClassRes = mysql_query($selectClass);
  while($classRow = mysql_fetch_array($selectClassRes))
  {
  	$clubArray[$i]['subjectMasterId'] = $classRow['subjectMasterId'];
  	$clubArray[$i]['subjectName']     = $classRow['subjectName'];
  	$clubArray[$i]['subjectCode']     = $classRow['subjectCode'];
  	$clubArray[$i]['startClass']      = $classRow['startClass'];
  	$clubArray[$i]['endClass']        = $classRow['endClass'];
  	$clubArray[$i]['subjectType']     = $classRow['subjectType'];
  	$clubArray[$i]['subjectDescription'] = $classRow['subjectDescription'];
  	$i++;
  }
  include("./bottom.php");
  $smarty->assign('clubArray',$clubArray);
  $smarty->display('subjectMasterList.tpl');  
}
?>
<?php
define('FPDF_FONTPATH', 'font/');
require('./font/fpdf.php');
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Teacher	')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
  error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
  $pdf=new FPDF('P','mm','A4');   //Create new pdf file
  $pdf->Open();     //Open file
  $pdf->SetAutoPageBreak(false);  //Disable automatic page break
  $pdf->AddPage();  //Add first page


  //header part start
  $pdf->SetFont('Arial','B',14);
  $pdf->SetXY(70,5);
  $pdf->Cell(75,7,'DELHI PUBLIC SCHOOL RAJKOT',0,0,'C',0);
  
  $selectInfo = "SELECT academicStartYear,academicEndYear,class,section,rollNo,studentmaster.studentName,studentmaster.studentImage,studentmaster.grNo,
                        house.houseName,DATE_FORMAT(dateOfBirth,'%d-%m-%Y') AS dob
                   FROM nominalroll
               LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
               LEFT JOIN house ON house.houseId = studentmaster.houseId
                 WHERE 	nominalroll.grNo = ".$_REQUEST['grNo']."
                 AND academicStartYear = '".$_REQUEST['academicStartYear']."'
                 AND academicEndYear   = '".$_REQUEST['academicEndYear']."'";
  $selectInfoRes = mysql_query($selectInfo);
  if($infoRow = mysql_fetch_array($selectInfoRes))
  {
    $academicStartYear = substr($infoRow['academicStartYear'],0,4);	
    $academicEndYear   = substr($infoRow['academicEndYear'],0,4);	
    $class             = $infoRow['class'];	
    $section           = $infoRow['section'];	
    $rollNo            = $infoRow['rollNo'];	
    $studentName       = $infoRow['studentName'];	
    $dob               = $infoRow['dob'];	
    $studentName       = $infoRow['studentName'];	
    $studentImage      = $infoRow['studentImage'];	
    $grNo              = $infoRow['grNo'];	
    $house             = $infoRow['houseName'];	
  }
  
  
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
  $k=0;
  $gradeArray = array();
  $selectGrade = "SELECT gradeId,grade
                    FROM grade";
  $selectGradeRes = mysql_query($selectGrade);
  while($gradeRow = mysql_fetch_array($selectGradeRes))
  {
  	$gradeArray[$gradeRow['gradeId']] = $gradeRow['grade'];
  	$k++;
  }
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
  $pdf->Image('./images/icon.png',25,0,0,0);  
  
  //$pdf->Image('./studentImage/'.$grNo.'_P.jpg',4,0,0,0);
  if(file_exists("./studentImage/".$grNo."_P.jpg"))
  {
   $pdf->Image("./studentImage/".$grNo."_P.jpg",158,1,30,25,'JPG');
  }
  else
  {
  	$pdf->Image("./studentImage/noImg.jpg",158,1,30,25,'JPG'); 
  }
  $pdf->SetFont('Arial','B',10);
  $pdf->SetXY(75,11);
  $pdf->Cell(80,7,'Academic Year :'.$academicStartYear.'-'.$academicEndYear,0);
  $pdf->SetXY(80,18);
  $pdf->Cell(70,7,'Progress Profile',0);
  $pdf->SetXY(28,25);
  $pdf->Cell(80,7,'Name : '.$studentName,0);
  $pdf->SetXY(28,30);
  $pdf->Cell(80,7,'Class : '.$class.'  Section : ' .$section,0);
  $pdf->SetXY(120,25);
  $pdf->Cell(80,7,'Roll No : ' .$rollNo,0);
  $pdf->SetXY(120,30);
  $pdf->Cell(80,7,'D.O.B. : ' .$dob,0);
  //header part end
  
  //////////////////
  //////////////////

  $pdf->SetFont('Arial','B',9);
  $pdf->SetXY(30,40);
  $pdf->Cell(75,7,"English",1);
  $pdf->Ln();

  $pdf->SetXY(30,47);

  $pdf->Cell(25,7,"Aspects",1);
  $pdf->Cell(25,7,"Term 1",1);
  $pdf->Cell(25,7,"Term 2",1);
  $pdf->Ln();

  $pdf->SetXY(30,54);
  $pdf->Cell(75,7,"Reading Skills",1);
  $pdf->Ln();
  $pdf->SetXY(30,61);
  $pdf->SetFont('Arial','',9);

  $selectEng = "SELECT academicStartYear,academicEndYear,eReadingPro,eReadingFlu,eReadingCom,eWritingCre,eWritingCre,eWritingHan,eWritingGra,eWritingSpe,
                       eWritingVoc,ewSpeakinCon,ewSpeakinRec,ewSpeakinCla,eListingComp,eListingCon,extraReading,activityPro
                   FROM gradeterm1
                   LEFT JOIN exminationmaster ON exminationmaster.grno = gradeterm1.grNo 
                  WHERE gradeterm1.grNo = ".$_REQUEST['grNo']."
                  AND academicStartYear = '".$_REQUEST['academicStartYear']."'
                 AND academicEndYear   = '".$_REQUEST['academicEndYear']."'
                  AND termValue = 1";
  $selectEngRes = mysql_query($selectEng);
  if($engRow = mysql_fetch_array($selectEngRes))
  {
	  $selectEngTerm2 = "SELECT eReadingPro,eReadingFlu,eReadingCom,eWritingCre,eWritingCre,eWritingHan,eWritingGra,eWritingSpe,
	                            eWritingVoc,ewSpeakinCon,ewSpeakinRec,ewSpeakinCla,eListingComp,eListingCon,extraReading,activityPro
	                       FROM gradeterm1
	                     LEFT JOIN exminationmaster ON exminationmaster.grno = gradeterm1.grNo 
	                    WHERE gradeterm1.grNo = ".$_REQUEST['grNo']."
                      AND academicStartYear = '".$_REQUEST['academicStartYear']."'
                      AND academicEndYear   = '".$_REQUEST['academicEndYear']."'
	                     AND termValue = 2";
	  $selectEngResTerm2 = mysql_query($selectEngTerm2);
	  if($engRowTerm2 = mysql_fetch_array($selectEngResTerm2))
	  {
	  	
	    $pdf->Cell(25,7,"Pronunciation",1);
	    $pdf->Cell(25,7,$gradeArray[$engRow['eReadingPro']],1);
	    $pdf->Cell(25,7,$gradeArray[$engRowTerm2['eReadingPro']],1);
	    $pdf->Ln();
	    $pdf->SetXY(30,68);
	    $pdf->Cell(25,7,"Fluency",1);
	    $pdf->Cell(25,7,$gradeArray[$engRow['eReadingFlu']],1);
	    $pdf->Cell(25,7,$gradeArray[$engRowTerm2['eReadingFlu']],1);
	    $pdf->Ln();
      $pdf->SetXY(30,75);
	    $pdf->Cell(25,7,"Comprehension",1);
	    $pdf->Cell(25,7,$gradeArray[$engRow['eReadingCom']],1);
	    $pdf->Cell(25,7,$gradeArray[$engRowTerm2['eReadingCom']],1);
	    $pdf->Ln();	 
	    $pdf->SetFont('Arial','B',9);
	    $pdf->SetXY(30,82);
	    $pdf->Cell(75,7,"Writing Skills",1);
	    $pdf->Ln();	 
	    $pdf->SetFont('Arial','',9);
	    $pdf->SetXY(30,89);
	    $pdf->Cell(25,7,"Creative Writing",1);
	    $pdf->Cell(25,7,$gradeArray[$engRow['eWritingCre']],1);
	    $pdf->Cell(25,7,$gradeArray[$engRowTerm2['eWritingCre']],1);
	    $pdf->Ln();
	    $pdf->SetXY(30,96);
	    $pdf->Cell(25,7,"Handwriting",1);
	    $pdf->Cell(25,7,$gradeArray[$engRow['eWritingHan']],1);
	    $pdf->Cell(25,7,$gradeArray[$engRowTerm2['eWritingHan']],1);
	    $pdf->Ln();	 
	    $pdf->SetXY(30,103);
	    $pdf->Cell(25,7,"Grammar",1);
	    $pdf->Cell(25,7,$gradeArray[$engRow['eWritingGra']],1);
	    $pdf->Cell(25,7,$gradeArray[$engRowTerm2['eWritingGra']],1);
	    $pdf->Ln();	 
	    $pdf->SetXY(30,110);
	    $pdf->Cell(25,7,"Spellings",1);
	    $pdf->Cell(25,7,$gradeArray[$engRow['eWritingSpe']],1);
	    $pdf->Cell(25,7,$gradeArray[$engRowTerm2['eWritingSpe']],1);
	    $pdf->Ln();	 
	    $pdf->SetXY(30,117);
	    $pdf->Cell(25,7,"Vocabulary",1);
	    $pdf->Cell(25,7,$gradeArray[$engRow['eWritingVoc']],1);
	    $pdf->Cell(25,7,$gradeArray[$engRowTerm2['eWritingVoc']],1);
	    $pdf->Ln();	 
	    $pdf->SetFont('Arial','B',9);
	    $pdf->SetXY(30,124);
	    $pdf->Cell(75,7,"Speaking Skills",1);
	    $pdf->Ln();	 
	    $pdf->SetFont('Arial','',9);
	    $pdf->SetXY(30,131);
	    $pdf->Cell(25,7,"Conversation",1);
	    $pdf->Cell(25,7,$gradeArray[$engRow['ewSpeakinCon']],1);
	    $pdf->Cell(25,7,$gradeArray[$engRowTerm2['ewSpeakinCon']],1);
	    $pdf->Ln();	 
	    $pdf->SetXY(30,138);
	    $pdf->Cell(25,7,"Recitation",1);
	    $pdf->Cell(25,7,$gradeArray[$engRow['ewSpeakinRec']],1);
	    $pdf->Cell(25,7,$gradeArray[$engRowTerm2['ewSpeakinRec']],1);
	    $pdf->Ln();	 
	    $pdf->SetXY(30,145);
	    $pdf->Cell(25,7,"Clarity",1);
	    $pdf->Cell(25,7,$gradeArray[$engRow['ewSpeakinCla']],1);
	    $pdf->Cell(25,7,$gradeArray[$engRowTerm2['ewSpeakinCla']],1);
	    $pdf->Ln();	 	
	    $pdf->SetFont('Arial','B',9);
	    $pdf->SetXY(30,152);
	    $pdf->Cell(75,7,"Listening Skills",1);
	    $pdf->Ln();	
	    $pdf->SetFont('Arial','',9);
	    $pdf->SetXY(30,159);
	    $pdf->Cell(25,7,"Comprehension",1);
	    $pdf->Cell(25,7,$gradeArray[$engRow['eListingComp']],1);
	    $pdf->Cell(25,7,$gradeArray[$engRowTerm2['eListingComp']],1);
	    $pdf->Ln();	 	
	    $pdf->SetXY(30,166);
	    $pdf->SetFont('Arial','',7.5);
	    $pdf->Cell(25,7,"Concentration Span",1);
	    $pdf->Cell(25,7,$gradeArray[$engRow['eListingCon']],1);
	    $pdf->Cell(25,7,$gradeArray[$engRowTerm2['eListingCon']],1);
	    $pdf->Ln();	 	
	    $pdf->SetXY(30,173);
	    $pdf->SetFont('Arial','',9);
	    $pdf->Cell(25,7,"Extra Reading",1);
	    $pdf->Cell(25,7,$gradeArray[$engRow['extraReading']],1);
	    $pdf->Cell(25,7,$gradeArray[$engRowTerm2['extraReading']],1);
	    $pdf->Ln();	 	
	    $pdf->SetXY(30,180);
	    $pdf->Cell(25,7,"Activity / Project",1);
	    $pdf->Cell(25,7,$gradeArray[$engRow['activityPro']],1);
	    $pdf->Cell(25,7,$gradeArray[$engRowTerm2['activityPro']],1);
	    $pdf->Ln();	 
	  }
  }
  /////////////////maths start ////////////////////
  
  $selectMath = "SELECT mathAspCon,mathAspMen,mathAspActi
                   FROM gradeterm1
                  LEFT JOIN exminationmaster ON exminationmaster.grno = gradeterm1.grNo 
                   AND academicStartYear = '".$_REQUEST['academicStartYear']."'
                   AND academicEndYear   = '".$_REQUEST['academicEndYear']."'
                 WHERE gradeterm1.grNo = ".$_REQUEST['grNo']."
                  AND termValue = 1";
  $selectMathRes = mysql_query($selectMath);
  if($mathRow = mysql_fetch_array($selectMathRes))
  {
  	$l=0;
    $selectMath2 = "SELECT mathAspCon,mathAspMen,mathAspActi
                      FROM gradeterm1
                     LEFT JOIN exminationmaster ON exminationmaster.grno = gradeterm1.grNo 
                     AND academicStartYear = '".$_REQUEST['academicStartYear']."'
                     AND academicEndYear   = '".$_REQUEST['academicEndYear']."'
                   WHERE gradeterm1.grNo = ".$_REQUEST['grNo']."
                    AND termValue = 2";
    $selectMathRes2 = mysql_query($selectMath2);
    if($mathRow2 = mysql_fetch_array($selectMathRes2))
    {
    	$pdf->SetFont('Arial','B',9);
      $pdf->SetXY(30,190);
	    $pdf->Cell(75,6,"Mathematics",1);
	    $pdf->Ln();	
	    $pdf->SetXY(30,196);
	    $pdf->Cell(25,6,"Aspects",1);
	    $pdf->Cell(25,6,"Term 1",1);
	    $pdf->Cell(25,6,"Term 2",1);
	    $pdf->Ln();	
	    $pdf->SetFont('Arial','',9);
	    $pdf->SetXY(30,202);
	    $pdf->Cell(25,6,"Concept",1);
	    $pdf->Cell(25,6,$gradeArray[$mathRow['mathAspCon']],1);
	    $pdf->Cell(25,6,$gradeArray[$mathRow2['mathAspCon']],1);
	    $pdf->Ln();	
	    $pdf->SetXY(30,208);
	    $pdf->Cell(25,6,"Mental Ability",1);
	    $pdf->Cell(25,6,$gradeArray[$mathRow['mathAspMen']],1);
	    $pdf->Cell(25,6,$gradeArray[$mathRow2['mathAspMen']],1);
	    $pdf->Ln();	 		
	    $pdf->SetXY(30,214);
	    $pdf->Cell(25,6,"Activity / Project",1);
	    $pdf->Cell(25,6,$gradeArray[$mathRow['mathAspActi']],1);
	    $pdf->Cell(25,6,$gradeArray[$mathRow2['mathAspActi']],1);
	    $pdf->Ln();	 		
    }
  }
  /////////////////maths end ////////////////////
  
  
  /////////////////computer start ////////////////////
  
  $selectCom = "SELECT comSkills,comAptitude
                   FROM gradeterm1
                  LEFT JOIN exminationmaster ON exminationmaster.grno = gradeterm1.grNo 
                  AND academicStartYear = '".$_REQUEST['academicStartYear']."'
                  AND academicEndYear   = '".$_REQUEST['academicEndYear']."'
                  WHERE gradeterm1.grNo = ".$_REQUEST['grNo']."
                  AND termValue = 1";
  $selectComRes = mysql_query($selectCom);
  if($comRow = mysql_fetch_array($selectComRes))
  {
  	$l=0;
    $selectCom2 = "SELECT  comSkills,comAptitude
                   FROM gradeterm1
                  LEFT JOIN exminationmaster ON exminationmaster.grno = gradeterm1.grNo 
                  AND academicStartYear = '".$_REQUEST['academicStartYear']."'
                  AND academicEndYear   = '".$_REQUEST['academicEndYear']."'
                 WHERE gradeterm1.grNo = ".$_REQUEST['grNo']."
                  AND termValue = 2";
    $selectComRes2 = mysql_query($selectCom2);
    if($comRow2 = mysql_fetch_array($selectComRes2))
    {
    	$pdf->SetFont('Arial','B',9);
      $pdf->SetXY(30,222);
	    $pdf->Cell(75,6,"Computer",1);
	    $pdf->Ln();	
	    $pdf->SetXY(30,228);
	    $pdf->Cell(25,6,"Aspects",1);
	    $pdf->Cell(25,6,"Term 1",1);
	    $pdf->Cell(25,6,"Term 2",1);
	    $pdf->Ln();	
	    $pdf->SetFont('Arial','',9);
	    $pdf->SetXY(30,234);
	    $pdf->Cell(25,6,"Skill",1);
	    $pdf->Cell(25,6,$gradeArray[$comRow['comSkills']],1);
	    $pdf->Cell(25,6,$gradeArray[$comRow2['comSkills']],1);
	    $pdf->Ln();	
	    $pdf->SetXY(30,240);
	    $pdf->Cell(25,6,"Aptitude",1);
	    $pdf->Cell(25,6,$gradeArray[$comRow['comAptitude']],1);
	    $pdf->Cell(25,6,$gradeArray[$comRow2['comAptitude']],1);
	    $pdf->Ln();	 		
    }
  }
  /////////////////computer end ////////////////////
   /////////////////hindi start ////////////////////
  
  $selectHindi = "SELECT hiReadingPro,hiReadingFlu,hiReadingCom,hiWritingCre,hiWritingHan,hiWritingGra,hiWritingSpe,hiWritingVoc,
                        hiwSpeakinCon,hiwSpeakinRec,hiwSpeakinCla,hiListingComp,hiListingCon,hiextraReading,hiactivityPro
                   FROM gradeterm1
                  LEFT JOIN exminationmaster ON exminationmaster.grno = gradeterm1.grNo 
                      AND academicStartYear = '".$_REQUEST['academicStartYear']."'
                      AND academicEndYear   = '".$_REQUEST['academicEndYear']."'
                    WHERE gradeterm1.grNo = ".$_REQUEST['grNo']." 
                  AND termValue = 1";
  $selectHindiRes = mysql_query($selectHindi);
  if($hindiRow = mysql_fetch_array($selectHindiRes))
  {
  	
    $selectHindi2 = "SELECT hiReadingPro,hiReadingFlu,hiReadingCom,hiWritingCre,hiWritingHan,hiWritingGra,hiWritingSpe,hiWritingVoc,
                           hiwSpeakinCon,hiwSpeakinRec,hiwSpeakinCla,hiListingComp,hiListingCon,hiextraReading,hiactivityPro
                   FROM gradeterm1
                   LEFT JOIN exminationmaster ON exminationmaster.grno = gradeterm1.grNo 
                     AND academicStartYear = '".$_REQUEST['academicStartYear']."'
                     AND academicEndYear   = '".$_REQUEST['academicEndYear']."'
                   WHERE gradeterm1.grNo = ".$_REQUEST['grNo']."
                  AND termValue = 2";
    $selectHindiRes2 = mysql_query($selectHindi2);
    if($hindiRow2 = mysql_fetch_array($selectHindiRes2))
    {
    	$pdf->SetFont('Arial','B',9);
      $pdf->SetXY(115,40);
	    $pdf->Cell(75,7,"Hindi",1);
	    $pdf->Ln();	
	    $pdf->SetXY(115,47);
	    $pdf->Cell(25,7,"Aspects",1);
	    $pdf->Cell(25,7,"Term 1",1);
	    $pdf->Cell(25,7,"Term 2",1);
	    $pdf->Ln();	
	    $pdf->SetXY(115,54);
	    $pdf->Cell(75,7,"Reading Skills",1);
	    $pdf->Ln();	
	    $pdf->SetFont('Arial','',9);
	    $pdf->SetXY(115,61);
	    $pdf->Cell(25,7,"Pronunciation",1);
	    $pdf->Cell(25,7,$gradeArray[$hindiRow['hiReadingPro']],1);
	    $pdf->Cell(25,7,$gradeArray[$hindiRow2['hiReadingPro']],1);
	    $pdf->Ln();	 		
	    $pdf->SetXY(115,68);
	    $pdf->Cell(25,7,"Fluency",1);
	    $pdf->Cell(25,7,$gradeArray[$hindiRow['hiReadingFlu']],1);
	    $pdf->Cell(25,7,$gradeArray[$hindiRow2['hiReadingFlu']],1);
	    $pdf->Ln();	 		
	    $pdf->SetXY(115,75);
	    $pdf->Cell(25,7,"Comprehension",1);
	    $pdf->Cell(25,7,$gradeArray[$hindiRow['hiReadingCom']],1);
	    $pdf->Cell(25,7,$gradeArray[$hindiRow2['hiReadingCom']],1);
	    $pdf->Ln();
	    $pdf->SetFont('Arial','B',9);
	    $pdf->SetXY(115,82);
	    $pdf->Cell(75,7,"Writing Skills",1);
	    $pdf->Ln();	 			 		
	    $pdf->SetFont('Arial','',9);
	    $pdf->SetXY(115,89);
	    $pdf->Cell(25,7,"Creative Writing",1);
	    $pdf->Cell(25,7,$gradeArray[$hindiRow['hiWritingCre']],1);
	    $pdf->Cell(25,7,$gradeArray[$hindiRow2['hiWritingCre']],1);
	    $pdf->Ln();	 		
	    $pdf->SetXY(115,96);
	    $pdf->Cell(25,7,"Handwriting",1);
	    $pdf->Cell(25,7,$gradeArray[$hindiRow['hiWritingHan']],1);
	    $pdf->Cell(25,7,$gradeArray[$hindiRow2['hiWritingHan']],1);
	    $pdf->Ln();	 		
	    $pdf->SetXY(115,103);
	    $pdf->Cell(25,7,"Grammar",1);
	    $pdf->Cell(25,7,$gradeArray[$hindiRow['hiWritingGra']],1);
	    $pdf->Cell(25,7,$gradeArray[$hindiRow2['hiWritingGra']],1);
	    $pdf->Ln();	 		
	    $pdf->SetXY(115,110);
	    $pdf->Cell(25,7,"Spellings",1);
	    $pdf->Cell(25,7,$gradeArray[$hindiRow['hiWritingSpe']],1);
	    $pdf->Cell(25,7,$gradeArray[$hindiRow2['hiWritingSpe']],1);
	    $pdf->Ln();	 		
	    $pdf->SetXY(115,117);
	    $pdf->Cell(25,7,"Vocabulary",1);
	    $pdf->Cell(25,7,$gradeArray[$hindiRow['hiWritingVoc']],1);
	    $pdf->Cell(25,7,$gradeArray[$hindiRow2['hiWritingVoc']],1);
	    $pdf->Ln();	 
	    $pdf->SetFont('Arial','B',9);		
	    $pdf->SetXY(115,124);
	    $pdf->Cell(75,7,"Speaking Skills",1);
	    $pdf->Ln();	 		
	    $pdf->SetFont('Arial','',9);
	    $pdf->SetXY(115,131);
	    $pdf->Cell(25,7,"Conversation",1);
	    $pdf->Cell(25,7,$gradeArray[$hindiRow['hiwSpeakinCon']],1);
	    $pdf->Cell(25,7,$gradeArray[$hindiRow2['hiwSpeakinCon']],1);
	    $pdf->Ln();	 		
	    $pdf->SetXY(115,138);
	    $pdf->Cell(25,7,"Recitation",1);
	    $pdf->Cell(25,7,$gradeArray[$hindiRow['hiwSpeakinRec']],1);
	    $pdf->Cell(25,7,$gradeArray[$hindiRow2['hiwSpeakinRec']],1);
	    $pdf->Ln();	 		
	    $pdf->SetXY(115,145);
	    $pdf->Cell(25,7,"Clarity",1);
	    $pdf->Cell(25,7,$gradeArray[$hindiRow['hiwSpeakinCla']],1);
	    $pdf->Cell(25,7,$gradeArray[$hindiRow2['hiwSpeakinCla']],1);
	    $pdf->Ln();	 		
	    $pdf->SetXY(115,152);
	    $pdf->SetFont('Arial','B',9);
	    $pdf->Cell(75,7,"Listening Skills",1);
	    $pdf->Ln();	 		
	    $pdf->SetFont('Arial','',9);
	    $pdf->SetXY(115,159);
	    $pdf->Cell(25,7,"Comprehension",1);
	    $pdf->Cell(25,7,$gradeArray[$hindiRow['hiListingComp']],1);
	    $pdf->Cell(25,7,$gradeArray[$hindiRow2['hiListingComp']],1);
	    $pdf->Ln();	 		
	    $pdf->SetXY(115,166);  
	    $pdf->SetFont('Arial','',7.5);
	    $pdf->Cell(25,7,"Concentration Span",1);
	    $pdf->Cell(25,7,$gradeArray[$hindiRow['hiListingCon']],1);
	    $pdf->Cell(25,7,$gradeArray[$hindiRow2['hiListingCon']],1);
	    $pdf->Ln();	 		
	    $pdf->SetXY(115,173);
	    $pdf->SetFont('Arial','',9);
	    $pdf->Cell(25,7,"Extra Reading",1);
	    $pdf->Cell(25,7,$gradeArray[$hindiRow['hiextraReading']],1);
	    $pdf->Cell(25,7,$gradeArray[$hindiRow2['hiextraReading']],1);
	    $pdf->Ln();	 		
	    $pdf->SetXY(115,180);
	    $pdf->Cell(25,7,"Activity / Project",1);
	    $pdf->Cell(25,7,$gradeArray[$hindiRow['hiactivityPro']],1);
	    $pdf->Cell(25,7,$gradeArray[$hindiRow2['hiactivityPro']],1);
	    $pdf->Ln();		
    }
  }
  /////////////////hindi end ////////////////////
  /////////////////env.sci start ////////////////////
  $selectEnvsci = "SELECT envAspenv,envGroupDis,envActi
                   FROM gradeterm1
                  LEFT JOIN exminationmaster ON exminationmaster.grno = gradeterm1.grNo 
                     AND academicStartYear = '".$_REQUEST['academicStartYear']."'
                     AND academicEndYear   = '".$_REQUEST['academicEndYear']."'
                   WHERE gradeterm1.grNo = ".$_REQUEST['grNo']."
                  AND termValue = 1";
  $selectEnvsciRes = mysql_query($selectEnvsci);
  if($envRow = mysql_fetch_array($selectEnvsciRes))
  {
    $selectEnvsci2 = "SELECT envAspenv,envGroupDis,envActi
                   FROM gradeterm1
                  LEFT JOIN exminationmaster ON exminationmaster.grno = gradeterm1.grNo 
                     AND academicStartYear = '".$_REQUEST['academicStartYear']."'
                     AND academicEndYear   = '".$_REQUEST['academicEndYear']."'
                   WHERE gradeterm1.grNo = ".$_REQUEST['grNo']."
                  AND termValue = 2";
    $selectEnvsciRes2 = mysql_query($selectEnvsci2);
    if($envRow2 = mysql_fetch_array($selectEnvsciRes2))
    {
    	$pdf->SetFont('Arial','B',9);
      $pdf->SetXY(115,190);
	    $pdf->Cell(75,6,"Environment Science / General Knowledge",1);
	    $pdf->Ln();	
	    $pdf->SetXY(115,196);
	    $pdf->Cell(25,6,"Aspects",1);
	    $pdf->Cell(25,6,"Term 1",1);
	    $pdf->Cell(25,6,"Term 2",1);
	    $pdf->Ln();	
	    $pdf->SetFont('Arial','',6);
	    $pdf->SetXY(115,202);
	    $pdf->Cell(25,6,"Environmental Sensitivity",1);
	    $pdf->SetFont('Arial','',8);
	    $pdf->Cell(25,6,$gradeArray[$envRow['envAspenv']],1);
	    $pdf->Cell(25,6,$gradeArray[$envRow2['envAspenv']],1);
	    $pdf->Ln();	
	    $pdf->SetXY(115,208);
	    $pdf->Cell(25,6,"Group Discussion",1);
	    $pdf->Cell(25,6,$gradeArray[$envRow['envGroupDis']],1);
	    $pdf->Cell(25,6,$gradeArray[$envRow2['envGroupDis']],1);
	    $pdf->Ln();	 		
	    $pdf->SetXY(115,214);
	    $pdf->Cell(25,6,"Activity / Project",1);
	    $pdf->Cell(25,6,$gradeArray[$envRow['envActi']],1);
	    $pdf->Cell(25,6,$gradeArray[$envRow2['envActi']],1);
	    $pdf->Ln();	 		
    }
  }
  /////////////////env.sci end ////////////////////
  /////////////////health start ////////////////////
  $selectHealth = "SELECT studHei,studWei
                   FROM gradeterm1
                  LEFT JOIN exminationmaster ON exminationmaster.grno = gradeterm1.grNo 
                     AND academicStartYear = '".$_REQUEST['academicStartYear']."'
                     AND academicEndYear   = '".$_REQUEST['academicEndYear']."'
                   WHERE gradeterm1.grNo = ".$_REQUEST['grNo']."
                  AND termValue = 1";
  $selectHealthRes = mysql_query($selectHealth);
  if($healthRow = mysql_fetch_array($selectHealthRes))
  {
    $selectHealth2 = "SELECT studHei,studWei
                   FROM gradeterm1
                  LEFT JOIN exminationmaster ON exminationmaster.grno = gradeterm1.grNo 
                     AND academicStartYear = '".$_REQUEST['academicStartYear']."'
                     AND academicEndYear   = '".$_REQUEST['academicEndYear']."'
                   WHERE gradeterm1.grNo = ".$_REQUEST['grNo']."
                  AND termValue = 2";
    $selectHealthRes2 = mysql_query($selectHealth2);
    if($healthRow2 = mysql_fetch_array($selectHealthRes2))
    {
    	$pdf->SetFont('Arial','B',9);
      $pdf->SetXY(115,223);
	    $pdf->Cell(75,6,"Health",1);
	    $pdf->Ln();	
	    $pdf->SetXY(115,229);
	    $pdf->Cell(25,6,"Aspects",1);
	    $pdf->Cell(25,6,"Term 1",1);
	    $pdf->Cell(25,6,"Term 2",1);
	    $pdf->Ln();	
	    $pdf->SetFont('Arial','',9);
	    $pdf->SetXY(115,235);
	    $pdf->Cell(25,6,"Height(cm)",1);
	    $pdf->Cell(25,6,$healthRow['studHei'],1);
	    $pdf->Cell(25,6,$healthRow2['studHei'],1);
	    $pdf->Ln();	
	    $pdf->SetXY(115,241);
	    $pdf->Cell(25,6,"Weight(kg)",1);
	    $pdf->Cell(25,6,$healthRow['studWei'],1);
	    $pdf->Cell(25,6,$healthRow2['studWei'],1);
	    $pdf->Ln();	 		
    }
  }
  /////////////////health end ////////////////////
  ///////////////Scholastics start ///////////
  $pdf->SetFont('Arial','B',9);
  $pdf->SetXY(30,249);
	$pdf->Cell(160,4,"Scholastics",1);
	$pdf->Ln();	
  $pdf->SetFont('Arial','B',9);
  $pdf->SetXY(30,253);
	$pdf->Cell(80,4,"Grade Point",1);
	$pdf->SetXY(110,253);
	$pdf->Cell(80,4,"Grade",1);
	$pdf->Ln();	
	$pdf->SetFont('Arial','',9);
	$pdf->SetXY(30,257);
	$pdf->Cell(80,4,"10.0",1);
	$pdf->SetXY(110,257);
	$pdf->Cell(80,4,"A1",1);
	$pdf->Ln();	
	$pdf->SetXY(30,261);
	$pdf->Cell(80,4,"9.0",1);
	$pdf->SetXY(110,261);
	$pdf->Cell(80,4,"A2",1);
	$pdf->Ln();
	$pdf->SetXY(30,265);
	$pdf->Cell(80,4,"8.0",1);
	$pdf->SetXY(110,265);
	$pdf->Cell(80,4,"B1",1);
	$pdf->Ln();
	$pdf->SetXY(30,269);
	$pdf->Cell(80,4,"7.0",1);
	$pdf->SetXY(110,269);
	$pdf->Cell(80,4,"B2",1);
	$pdf->Ln();
	$pdf->SetXY(30,273);
	$pdf->Cell(80,4,"6.0",1);
	$pdf->SetXY(110,273);
	$pdf->Cell(80,4,"C1",1);
	$pdf->Ln();
	$pdf->SetXY(30,277);
	$pdf->Cell(80,4,"5.0",1);
	$pdf->SetXY(110,277);
	$pdf->Cell(80,4,"C2",1);
	$pdf->Ln();
  $pdf->SetXY(30,281);
	$pdf->Cell(80,4,"4.0",1);
	$pdf->SetXY(110,281);
	$pdf->Cell(80,4,"D",1);
	$pdf->Ln();
	$pdf->SetXY(30,285);
	$pdf->Cell(80,4,"Below 40",1);
	$pdf->SetXY(110,285);
	$pdf->Cell(80,4,"E",1);
	$pdf->Ln();
	$pdf->SetXY(30,289);
	$pdf->Cell(80,4,"Co-Scholestics:  A, B, C, D, E",0);
	$pdf->Ln();
  ///////////////Scholastics end ///////////
  
  
  
//////////////New page and Header :Start
//////////////New page and Header :End
  $pdf->AddPage(); 
  $pdf->SetFont('Arial','B',12);
  $pdf->SetXY(75,0);
  $pdf->Cell(85,7,'DELHI PUBLIC SCHOOL RAJKOT',0);
  $pdf->SetFont('Arial','B',9);
  $pdf->SetXY(80,6);
  $pdf->Cell(80,7,'Academic Year :'.$academicStartYear.'-'.$academicEndYear,0);
  $pdf->SetXY(85,12);
  $pdf->Cell(80,7,'Progress Profile',0);
  $pdf->SetXY(25,17);
  $pdf->Cell(80,7,'Name : '.$studentName,0);
  $pdf->SetXY(95,17);
  $pdf->Cell(175,7,'Class : '.$class.'  Section : ' .$section,0);
  $pdf->SetXY(165,17);
  $pdf->Cell(180,7,'House : ' .$house,0);
  $pdf->SetFont('Arial','',9);
  $pdf->SetXY(50,5);
   /////////////////perdev start ////////////////////
  $selectper = "SELECT perCou,perCon,perCar,perNea,perPun,perRegAndPun,perIni,perSha,perRes,persel
                   FROM gradeterm1
                  LEFT JOIN exminationmaster ON exminationmaster.grno = gradeterm1.grNo 
                     AND academicStartYear = '".$_REQUEST['academicStartYear']."'
                     AND academicEndYear   = '".$_REQUEST['academicEndYear']."'
                   WHERE gradeterm1.grNo = ".$_REQUEST['grNo']."
                  AND termValue = 1";
  $selectperRes = mysql_query($selectper);
  if($perRow = mysql_fetch_array($selectperRes))
  {
    $selectper2 = "SELECT perCou,perCon,perCar,perNea,perPun,perRegAndPun,perIni,perSha,perRes,persel
                   FROM gradeterm1
                  LEFT JOIN exminationmaster ON exminationmaster.grno = gradeterm1.grNo 
                     AND academicStartYear = '".$_REQUEST['academicStartYear']."'
                     AND academicEndYear   = '".$_REQUEST['academicEndYear']."'
                   WHERE gradeterm1.grNo = ".$_REQUEST['grNo']."
                  AND termValue = 2";
    $selectperRes2 = mysql_query($selectper2);
    if($perRow2 = mysql_fetch_array($selectperRes2))
    {
    	$pdf->SetFont('Arial','B',9);
      $pdf->SetXY(25,25);
	    $pdf->Cell(164,4,"Personality Development",1);
	    $pdf->Ln();	
	    $pdf->SetXY(25,29);
	    $pdf->Cell(80,4,"Aspects",1);
	    $pdf->Cell(42,4,"Term 1",1);
	    $pdf->Cell(42,4,"Term 2",1);
	    $pdf->Ln();	
	    $pdf->SetFont('Arial','',9);
	    $pdf->SetXY(25,33);
	    $pdf->Cell(80,4,"Courteousness",1);
	    $pdf->Cell(42,4,$gradeArray[$perRow['perCou']],1);
	    $pdf->Cell(42,4,$gradeArray[$perRow2['perCou']],1);
	    $pdf->Ln();	
	    $pdf->SetXY(25,37);
	    $pdf->Cell(80,4,"Confidence",1);
	    $pdf->Cell(42,4,$gradeArray[$perRow['perCon']],1);
	    $pdf->Cell(42,4,$gradeArray[$perRow2['perCon']],1);
	    $pdf->Ln();	
	    $pdf->SetXY(25,41);
	    $pdf->Cell(80,4,"Care of Belongings",1);
	    $pdf->Cell(42,4,$gradeArray[$perRow['perCar']],1);
	    $pdf->Cell(42,4,$gradeArray[$perRow2['perCar']],1);
	    $pdf->Ln();	 		
	    $pdf->SetXY(25,45);
	    $pdf->Cell(80,4,"Neatness",1);
	    $pdf->Cell(42,4,$gradeArray[$perRow['perNea']],1);
	    $pdf->Cell(42,4,$gradeArray[$perRow2['perNea']],1);
	    $pdf->Ln();	 		
	    $pdf->SetXY(25,49);
	    $pdf->Cell(80,4,"Punctuality",1);
	    $pdf->Cell(42,4,$gradeArray[$perRow['perPun']],1);
	    $pdf->Cell(42,4,$gradeArray[$perRow2['perPun']],1);
	    $pdf->Ln();	 		
	    $pdf->SetXY(25,53);
	    $pdf->Cell(80,4,"Regularity",1);
	    $pdf->Cell(42,4,$gradeArray[$perRow['perRegAndPun']],1);
	    $pdf->Cell(42,4,$gradeArray[$perRow2['perRegAndPun']],1);
	    $pdf->Ln();
	    $pdf->SetXY(25,57);
	    $pdf->Cell(80,4,"Initiative",1);
	    $pdf->Cell(42,4,$gradeArray[$perRow['perIni']],1);
	    $pdf->Cell(42,4,$gradeArray[$perRow2['perIni']],1);
	    $pdf->Ln();
	    $pdf->SetXY(25,61);
	    $pdf->Cell(80,4,"Sharing and Caring",1);
	    $pdf->Cell(42,4,$gradeArray[$perRow['perSha']],1);
	    $pdf->Cell(42,4,$gradeArray[$perRow2['perSha']],1);
	    $pdf->Ln();
	    $pdf->SetXY(25,65);
	    $pdf->Cell(80,4,"Respect for Others' Property",1);
	    $pdf->Cell(42,4,$gradeArray[$perRow['perRes']],1);
	    $pdf->Cell(42,4,$gradeArray[$perRow2['perRes']],1);
	    $pdf->Ln();
	    $pdf->SetXY(25,69);
	    $pdf->Cell(80,4,"Self Control",1);
	    $pdf->Cell(42,4,$gradeArray[$perRow['persel']],1);
	    $pdf->Cell(42,4,$gradeArray[$perRow2['persel']],1);
	    $pdf->Ln();
    }
  }
  //////////////////perdev end////////////////////
  //////////////////cocurri end////////////////////
  $selectCo = "SELECT coPhyEth,coPhyDis,coPhyTea,coPhyTal,coActInt,coActCre,coActSki,coMusicint,coMusicRhy,coMusicMel,coArtInt,
                      coArtCre,coArtPre,coDanceInt,coDanceAct,coDanceExp
                   FROM gradeterm1
                  LEFT JOIN exminationmaster ON exminationmaster.grno = gradeterm1.grNo 
                     AND academicStartYear = '".$_REQUEST['academicStartYear']."'
                     AND academicEndYear   = '".$_REQUEST['academicEndYear']."'
                   WHERE gradeterm1.grNo = ".$_REQUEST['grNo']."
                  AND termValue = 1";
  $selectCoRes = mysql_query($selectCo);
  if($coRow = mysql_fetch_array($selectCoRes))
  {
    $selectCo2 = "SELECT coPhyEth,coPhyDis,coPhyTea,coPhyTal,coActInt,coActCre,coActSki,coMusicint,coMusicRhy,coMusicMel,coArtInt,coArtCre,
                         coArtPre,coDanceInt,coDanceAct,coDanceExp
                   FROM gradeterm1
                  LEFT JOIN exminationmaster ON exminationmaster.grno = gradeterm1.grNo 
                     AND academicStartYear = '".$_REQUEST['academicStartYear']."'
                     AND academicEndYear   = '".$_REQUEST['academicEndYear']."'
                   WHERE gradeterm1.grNo = ".$_REQUEST['grNo']."
                  AND termValue = 2";
    $selectCoRes2 = mysql_query($selectCo2);
    if($coRow2 = mysql_fetch_array($selectCoRes2))
    {
    	$pdf->SetFont('Arial','B',9);
      $pdf->SetXY(25,78);
	    $pdf->Cell(164,4,"Co-Curricular Activities",1);
	    $pdf->Ln();	
	    $pdf->SetXY(25,82);
	    $pdf->Cell(80,4,"Physical Education",1);
	    $pdf->Cell(42,4,"Term 1",1);
	    $pdf->Cell(42,4,"Term 2",1);
	    $pdf->Ln();	
	    $pdf->SetFont('Arial','',9);
	    $pdf->SetXY(25,86);
	    $pdf->Cell(80,4,"Enthusiasm",1);
	    $pdf->Cell(42,4,$gradeArray[$coRow['coPhyEth']],1);
	    $pdf->Cell(42,4,$gradeArray[$coRow2['coPhyEth']],1);
	    $pdf->Ln();	
	    $pdf->SetXY(25,90);
	    $pdf->Cell(80,4,"Discipline",1);
	    $pdf->Cell(42,4,$gradeArray[$coRow['coPhyDis']],1);
	    $pdf->Cell(42,4,$gradeArray[$coRow2['coPhyDis']],1);
	    $pdf->Ln();	
	    $pdf->SetXY(25,94);
	    $pdf->Cell(80,4,"Team Sprit",1);
	    $pdf->Cell(42,4,$gradeArray[$coRow['coPhyTea']],1);
	    $pdf->Cell(42,4,$gradeArray[$coRow2['coPhyTea']],1);
	    $pdf->Ln();	 		
	    $pdf->SetXY(25,98);
	    $pdf->Cell(80,4,"Talent",1);
	    $pdf->Cell(42,4,$gradeArray[$coRow['coPhyTal']],1);
	    $pdf->Cell(42,4,$gradeArray[$coRow2['coPhyTal']],1);
	    $pdf->Ln();	 		
	    $pdf->SetFont('Arial','B',9);
	    $pdf->SetXY(25,102);
	    $pdf->Cell(164,4,"Activity / Club",1);
	    $pdf->Ln();
	    $pdf->SetFont('Arial','',9);	 		
	    $pdf->SetXY(25,106);
	    $pdf->Cell(80,4,"Interest",1);
	    $pdf->Cell(42,4,$gradeArray[$coRow['coActInt']],1);
	    $pdf->Cell(42,4,$gradeArray[$coRow2['coActInt']],1);
	    $pdf->Ln();
	    $pdf->SetXY(25,110);
	    $pdf->Cell(80,4,"Creativity",1);
	    $pdf->Cell(42,4,$gradeArray[$coRow['coActCre']],1);
	    $pdf->Cell(42,4,$gradeArray[$coRow2['coActCre']],1);
	    $pdf->Ln();
	    $pdf->SetXY(25,114);
	    $pdf->Cell(80,4,"Skill",1);
	    $pdf->Cell(42,4,$gradeArray[$coRow['coActSki']],1);
	    $pdf->Cell(42,4,$gradeArray[$coRow2['coActSki']],1);
	    $pdf->Ln();
	    $pdf->SetXY(25,118);
	    $pdf->SetFont('Arial','B',9);
	    $pdf->Cell(164,4,"Music(Vocal/Instrumental)",1);
	    $pdf->Ln();
	    $pdf->SetFont('Arial','',9);
	    $pdf->SetXY(25,122);
	    $pdf->Cell(80,4,"Interest",1);
	    $pdf->Cell(42,4,$gradeArray[$coRow['coMusicint']],1);
	    $pdf->Cell(42,4,$gradeArray[$coRow2['coMusicint']],1);
	    $pdf->Ln();
	    $pdf->SetXY(25,126);
	    $pdf->Cell(80,4,"Rhythm",1);
	    $pdf->Cell(42,4,$gradeArray[$coRow['coMusicRhy']],1);
	    $pdf->Cell(42,4,$gradeArray[$coRow2['coMusicRhy']],1);
	    $pdf->Ln();
	    $pdf->SetXY(25,130);
	    $pdf->Cell(80,4,"Melody",1);
	    $pdf->Cell(42,4,$gradeArray[$coRow['coMusicMel']],1);
	    $pdf->Cell(42,4,$gradeArray[$coRow2['coMusicMel']],1);
	    $pdf->Ln();
	    $pdf->SetXY(25,134);
	    $pdf->SetFont('Arial','B',9);
	    $pdf->Cell(164,4,"Art",1);
	    $pdf->Ln();
	    $pdf->SetFont('Arial','',9);
	    $pdf->SetXY(25,138);
	    $pdf->Cell(80,4,"Interest",1);
	    $pdf->Cell(42,4,$gradeArray[$coRow['coArtInt']],1);
	    $pdf->Cell(42,4,$gradeArray[$coRow2['coArtInt']],1);
	    $pdf->Ln();
	    $pdf->SetXY(25,142);
	    $pdf->Cell(80,4,"Creativity",1);
	    $pdf->Cell(42,4,$gradeArray[$coRow['coArtCre']],1);
	    $pdf->Cell(42,4,$gradeArray[$coRow2['coArtCre']],1);
	    $pdf->Ln();
	    $pdf->SetXY(25,146);
	    $pdf->Cell(80,4,"Presentation",1);
	    $pdf->Cell(42,4,$gradeArray[$coRow['coArtPre']],1);
	    $pdf->Cell(42,4,$gradeArray[$coRow2['coArtPre']],1);
	    $pdf->Ln();
	    $pdf->SetXY(25,150);
	    $pdf->SetFont('Arial','B',9);
	    $pdf->Cell(164,4,"Dance",1);
	    $pdf->Ln();
	    $pdf->SetXY(25,154);
	    $pdf->SetFont('Arial','',9);
	    $pdf->Cell(80,4,"Interest",1);
	    $pdf->Cell(42,4,$gradeArray[$coRow['coDanceInt']],1);
	    $pdf->Cell(42,4,$gradeArray[$coRow2['coDanceInt']],1);
	    $pdf->Ln();
	    $pdf->SetXY(25,158);
	    $pdf->Cell(80,4,"Action",1);
	    $pdf->Cell(42,4,$gradeArray[$coRow['coDanceAct']],1);
	    $pdf->Cell(42,4,$gradeArray[$coRow2['coDanceAct']],1);
	    $pdf->Ln();
	    $pdf->SetXY(25,162);
	    $pdf->Cell(80,4,"Expression",1);
	    $pdf->Cell(42,4,$gradeArray[$coRow['coDanceExp']],1);
	    $pdf->Cell(42,4,$gradeArray[$coRow2['coDanceExp']],1);
	    $pdf->Ln();
    }
  }
  //////////////////cocurri end////////////////////  
   /////////////////extra start ////////////////////
 $selectExtra = "SELECT partici,achiev,remarks,attance,partici1,achiev1,remarks1,attance1
                   FROM gradeterm1
                  LEFT JOIN exminationmaster ON exminationmaster.grno = gradeterm1.grNo 
                     AND academicStartYear = '".$_REQUEST['academicStartYear']."'
                     AND academicEndYear   = '".$_REQUEST['academicEndYear']."'
                   WHERE gradeterm1.grNo = ".$_REQUEST['grNo']."
                  AND termValue = 1";
  $selectExtraRes = mysql_query($selectExtra);
  if($extraRow = mysql_fetch_array($selectExtraRes))
  {
    $partici       = $extraRow['partici'];
    $achiev        = $extraRow['achiev'];
    $remarks       = $extraRow['remarks'];
    $attance       = $extraRow['attance'];
    $selectYear = "SELECT promoted,partici1,achiev1,remarks1,attance1,className FROM gradeterm1
                        JOIN classmaster ON classmaster.classMasterId = gradeterm1.classMasterId
                      LEFT JOIN exminationmaster ON exminationmaster.grno = gradeterm1.grNo 
                     AND academicStartYear = '".$_REQUEST['academicStartYear']."'
                     AND academicEndYear   = '".$_REQUEST['academicEndYear']."'
                   WHERE gradeterm1.grNo = ".$_REQUEST['grNo']."
                      AND termValue = 2";
    $selectYearRes = mysql_query($selectYear);
    if($extraRow = mysql_fetch_array($selectYearRes))
    {
      $promoted             = date("d-m-Y", strtotime($extraRow['promoted']));
      $pendingPartici1      = $extraRow['partici1'];
      $achiev1              = $extraRow['achiev1'];
      $pendingRemarks1      = $extraRow['remarks1'];
      $attance1             = $extraRow['attance1'];
      $className            = $extraRow['className'];
    }
  }                
  $pdf->SetFont('Arial','B',9);
  $pdf->SetXY(25,170);
	$pdf->Cell(164,4,"1st Term",1);
	$pdf->Ln();	
	$pdf->SetFont('Arial','B',8);
	$pdf->SetXY(25,174);
	$pdf->Cell(164,4,"Participation : ",1);
	$pdf->SetFont('Arial','',7.5);
	$pdf->SetXY(45,174);
	$pdf->Cell(200,4,$partici,0);
	$pdf->Ln();	
	$pdf->SetFont('Arial','B',9);
	$pdf->SetXY(25,178);
	$pdf->Cell(164,4,"Achievements : ",1);
	$pdf->SetFont('Arial','',8);
	$pdf->SetXY(50,178);
	$pdf->Cell(200,4,$achiev,0);
	$pdf->Ln();	
	$pdf->SetFont('Arial','B',9);
	$pdf->SetXY(25,182);
	$pdf->Cell(164,4,"Remarks :",1);
	$pdf->SetFont('Arial','',8);
	$pdf->SetXY(42,182);
	$pdf->Cell(200,4,$remarks,0);
	$pdf->Ln();	
	$pdf->SetXY(25,186);
	$pdf->Cell(164,4,"",1);
	$pdf->Ln();	 		
	$pdf->SetXY(25,190);
	$pdf->SetFont('Arial','B',9);
	$pdf->Cell(164,4,"Attendance",1);
	$pdf->SetFont('Arial','',8);
	$pdf->SetXY(43,190);
	$pdf->Cell(200,4,$attance,0);
	$pdf->Ln();	 		
	$pdf->SetFont('Arial','B',9);
 
	
	$pdf->SetXY(25,210);
	$pdf->Cell(164,4,"2nd Term",1);
	$pdf->Ln();	
	$pdf->SetXY(25,214);
	$pdf->Cell(164,4,"Participation : ",1);
	$pdf->SetFont('Arial','',8);
	$yForParticular = 4;
	$particularRequiredWidth = 105;
	while(strlen($pendingPartici1) > 0)
  {
    if(strlen($pendingPartici1) > $particularRequiredWidth)
    {
      $partici1 = substr($pendingPartici1,0,$particularRequiredWidth);
      $pendingPartici1 = substr($pendingPartici1,$particularRequiredWidth,strlen($pendingPartici1));
    }
    else
    {
      $partici1 = substr($pendingPartici1,0,strlen($pendingPartici1));
      $pendingPartici1 = substr($pendingPartici1,$particularRequiredWidth,strlen($pendingPartici1));
    }
    $pdf->SetXY(47,214);
    $pdf->Cell(164,$yForParticular,$partici1,0);
    $yForParticular += 7;
  }
	$pdf->Ln();	
	$pdf->SetXY(25,218);
	$pdf->Cell(164,4,"",1);
	$pdf->Ln();	
	$pdf->SetFont('Arial','B',9);
	
	$pdf->SetXY(25,222);
	$pdf->Cell(164,4,"Achievements : ",1);
	$pdf->SetFont('Arial','',8);
	$pdf->SetXY(25,226);
	$pdf->Cell(164,4,"",1);
	$pdf->SetFont('Arial','',8);
	$pdf->SetXY(49,222);
	$pdf->Cell(200,4,$achiev1,0);
	$pdf->Ln();	
	$pdf->SetFont('Arial','B',9);
	$pdf->SetXY(25,226);
	$pdf->Cell(164,4,"Remarks : ",1);
	$pdf->SetFont('Arial','',8);
	//$pdf->SetXY(42,226);
	//$pdf->Cell(200,4,$remarks1,0);
	$yForParticular = 4;
	$particularRequiredWidth = 111;
	while(strlen($pendingRemarks1) > 0)
  {
    if(strlen($pendingRemarks1) > $particularRequiredWidth)
    {
      $partici1 = substr($pendingRemarks1,0,$particularRequiredWidth);
      $pendingRemarks1 = substr($pendingRemarks1,$particularRequiredWidth,strlen($pendingRemarks1));
    }
    else
    {
      $partici1 = substr($pendingRemarks1,0,strlen($pendingRemarks1));
      $pendingRemarks1 = substr($pendingRemarks1,$particularRequiredWidth,strlen($pendingRemarks1));
    }
    $pdf->SetXY(41,226);
    $pdf->Cell(164,$yForParticular,$partici1,0);
    $yForParticular += 7;
  }
	$pdf->Ln();	
	$pdf->SetXY(25,230);
	$pdf->Cell(164,4,"",1);
	$pdf->Ln();	 		
	$pdf->SetFont('Arial','B',9);
	$pdf->SetXY(25,234);
	$pdf->Cell(164,4,"Attendance : ",1);
	$pdf->SetFont('Arial','',8);
	$pdf->SetXY(46,234);
	$pdf->Cell(200,4,$attance1,0);
	$pdf->Ln();	 		
	$pdf->SetFont('Arial','B',9);
	$pdf->SetXY(25,238);
	$pdf->Cell(164,4,"Congratulations ! Promoted To Class : ",1);
	$pdf->SetFont('Arial','',8);
	$pdf->SetXY(85,238);
	$pdf->Cell(200,4,$className,0);
	$pdf->Ln();
	$pdf->SetFont('Arial','B',9);
	$pdf->SetXY(25,242);
	$pdf->Cell(164,4,"New Session Begins On : ",1);
	$pdf->SetFont('Arial','',8);
	$pdf->SetXY(65,242);
	$pdf->Cell(200,4,$promoted,0);
	$pdf->Ln();
	$pdf->SetFont('Arial','B',9);
	$pdf->SetXY(25,260);
	$pdf->Cell(200,11,"Class Teacher ",0);
	$pdf->SetXY(94,260);
	$pdf->Cell(200,11,"Parent ",0);
	$pdf->Image('./studentImage/stamp1.jpg',120,250,30,25);
	$pdf->Image('./studentImage/sign.png',163,253,30,10);
	$pdf->SetXY(170,260);
	$pdf->Cell(200,11,"Principal",0);
	$pdf->Ln();	 		
  //////////////////extra end////////////////////
  $pdf->Output();

}
?>
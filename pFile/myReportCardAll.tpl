{include file="./main.tpl"}
{block name=head}
<style type="text/css" title="currentStyle">
	@import "./media/css/demo_table_jui.css";
  @import "./media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
  input
	{
		border:1px solid black;
	}
</style>
<script type="text/javascript" charset="utf-8">
$(document).ready(function(){
  $('#myDataTable').dataTable({
  	"aLengthMenu": [[10, 25, 50, 100, 500, 1000], [10, 25, 50, 100, 500, 1000]],
  	"iDisplayLength": 10,
		"bJQueryUI":true
  });
});
</script>
{/block}
{block name="body"}
</br></br>
<form name="formGet" method="GET" action="nominalRollListStudent.php">
<table align="center">
	<tr>
		<td class="table2 form01">
	    <select name="startYear" id="startDateYear">
	      {html_options values=$dateArrVal output=$dateArrOut selected=$academicStartYear}
	    </select>
	  </td>
    <td>
      <input type="submit" name="submit" class="newGoBtn" value="Go">
    </td>
  </tr>
</table>
</form>
<table align="center" border="1">
	<div class="hd"><h2 align="center">Nominal Roll Report</h2>
	<thead>
	<tr>
		<td align="left" class="table1"><b>Sr No</b></td>
		<td align="left" class="table1"><b>Academic Year</b></td>
		<td align="left" class="table1"><b>GR No</b></td>
		<td align="left" class="table1"><b>Roll No</b></td>
		<td align="left" class="table1"><b>Student Name</b></td>
		<td align="left" class="table1"><b>Class</b></td>
		<td align="left" class="table1"><b>Section</b></td>
		<td align="left" class="table1"><b>Term 1 Report Card</b></td>
		<td align="left" class="table1"><b>Term 2 Report Card</b></td>
  </tr>
  </thead>
  <tbody>
  {section name="sec" loop=$nominalArr}
  <tr>
  	<td align="left" class="table2">{$smarty.section.sec.rownum}</td>
  	<td align="left" class="table2">{$nominalArr[sec].academicYear}</td>
    <td align="left" class="table2">{$nominalArr[sec].grNo}</td>
    <td align="left" class="table2">{$nominalArr[sec].rollNo}</td>
    <td align="left" class="table2">{$nominalArr[sec].studentName}</td>
    <td align="left" class="table2">{$nominalArr[sec].class}</td>
    <td align="left" class="table2">{$nominalArr[sec].section}</td>
    {if $nominalArr[sec].class == '11 SCI.'}
      <td align="left" class="table2"><a href="myReportCardNewT1.php?academicYear={$nominalArr[sec].academicYear}&grNo={$nominalArr[sec].grNo}">Report Card</a></td>
    {/if}
    {if $nominalArr[sec].class == '11 COM.'}
      <td align="left" class="table2"><a href="myReportCardNewT1.php?academicYear={$nominalArr[sec].academicYear}&grNo={$nominalArr[sec].grNo}">Report Card</a></td>
    {/if}
    {if $nominalArr[sec].class == '12 SCI.'}
      <td align="left" class="table2"><a href="myReportCardNewT1.php?academicYear={$nominalArr[sec].academicYear}&grNo={$nominalArr[sec].grNo}">Report Card</a></td>
    {/if}
    {if $nominalArr[sec].class == '12 COM.'}
      <td align="left" class="table2"><a href="myReportCardNewT1.php?academicYear={$nominalArr[sec].academicYear}&grNo={$nominalArr[sec].grNo}">Report Card</a></td>
    {/if}
    {if $nominalArr[sec].class == '11 SCI.'}
      <td align="left" class="table2"><a href="myReportCardNew.php?academicYear={$nominalArr[sec].academicYear}&grNo={$nominalArr[sec].grNo}">Report Card</a></td>
    {/if}
    {if $nominalArr[sec].class == '11 COM.'}
      <td align="left" class="table2"><a href="myReportCardNew.php?academicYear={$nominalArr[sec].academicYear}&grNo={$nominalArr[sec].grNo}">Report Card</a></td>
    {/if}
    {if $nominalArr[sec].class == '12 SCI.'}
      <td align="left" class="table2"><a href="myReportCardNew.php?academicYear={$nominalArr[sec].academicYear}&grNo={$nominalArr[sec].grNo}">Report Card</a></td>
    {/if}
    {if $nominalArr[sec].class == '12 COM.'}
      <td align="left" class="table2"><a href="myReportCardNew.php?academicYear={$nominalArr[sec].academicYear}&grNo={$nominalArr[sec].grNo}">Report Card</a></td>
    {/if}
    {if $nominalArr[sec].class == '6' || $nominalArr[sec].class == '7' || $nominalArr[sec].class == '8' || $nominalArr[sec].class == '9' || $nominalArr[sec].class == '10'}
      <td align="left" class="table2"><a href="myReportCard.php?academicYear={$nominalArr[sec].academicYear}&grNo={$nominalArr[sec].grNo}">Report Card</a></td>
    {/if}
    {if $nominalArr[sec].status == 'C' &&($nominalArr[sec].class == '1' || $nominalArr[sec].class == '2' || $nominalArr[sec].class == '3' || $nominalArr[sec].class == '4' || $nominalArr[sec].class == '5') }
    <td align="left" class="table2">&nbsp;</td>
    <td align="left" class="table2">
      <a href="reportCardPrint1to5.php?academicStartYear={$nominalArr[sec].academicStartYear}&academicEndYear={$nominalArr[sec].academicEndYear}&grNo={$nominalArr[sec].grNo}">Report Card</a>
    </td>
    {/if}
  </tr>
  {/section}
  </tbody>
</table>
{/block}
<!DOCTYPE HTML>
<html>
<head>
  <script type="text/javascript">
  window.onload = function () {
  var chart = new CanvasJS.Chart("chartContainer",
  {
    legend: {
      maxWidth: 950,
      itemWidth: 120
    },
    data: [
    {
      type: "pie",
      showInLegend: false,
      legendText: "{indexLabel}",
      dataPoints: [
        { y: 45, label: "Teaching (50)", indexLabel: "Teaching" },
        { y: 16, label: "Non-Teaching (20)", indexLabel: "Non-Teaching" },
        { y: 854, label: "Students (1000)", indexLabel: "Students" }
      ]
    }
    ]
  });
  chart.render();
  $('.canvasjs-chart-credit').html('');

}
    
  </script>
  <script type="text/javascript" src="canvasjs.min.js"></script>
  <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
</head>
<body>
  <div id="chartTitle" style=" width:70%;text-align: center;"><h2>Staff & Student Attendance</h2></div>
  <div id="chartContainer" style="height:400px; width:70%;">
  </div>
</body>
</html>
<!DOCTYPE HTML>
<html>
<head>
  <script type="text/javascript">
  window.onload = function () {
    var chart = new CanvasJS.Chart("chartContainer", {

      data: [//array of dataSeries              
        { //dataSeries object

         /*** Change type "column" to "bar", "area", "line" or "pie"***/
         type: "column",
         dataPoints: [
         { label: "Teaching (50)", y: 45 },
         { label: "Non-Teaching (20)", y: 19 },
         { label: "Student (1000)", y: 840 }
         ]
       }
       ]
     });

    
    chart.render();
    $('.canvasjs-chart-credit').html('');
  }
  </script>
  <script type="text/javascript" src="canvasjs.min.js"></script>
  <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
</head>
<body>
  <div id="chartTitle" style=" width:70%;text-align: center;"><h2>Staff & Student Attendance</h2></div>
  <div id="chartContainer" style="height:300px; width:70%;">
  </div>
</body>
</html>
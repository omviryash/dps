<!DOCTYPE html>
<html>
<head>

<?php 
	include ("../include/conn.inc");
	include("include/header.php"); 
	$currentYr = date('Y');
	$nextYr = ($currentYr+1);

	$totStuds = 0;

	$selTotStud = "SELECT DISTINCT count(grNo) AS totStud FROM nominalroll WHERE academicStartYear='".$currentYr."-04-01' AND academicEndYear='".$nextYr."-03-31'";
	$resTotStud = mysql_query($selTotStud);
	$rowTotStud = mysql_fetch_array($resTotStud);

	$totStuds = $rowTotStud['totStud'];

	/// Present Absent students
	$today = date('Y-m-d');
	$attendenceDate = '2015-11-05';
	$totalPresent   = 0;
	$totalAbsent    = 0;
	$attendences    = '';
		$i = 0;
	  $selectEmp = "SELECT DISTINCT classteacherallotment.class,classteacherallotment.section,employeemaster.name,classmaster.priority
	                  FROM classteacherallotment
	             LEFT JOIN employeemaster ON employeemaster.employeeMasterId = classteacherallotment.employeeMasterId
	             LEFT JOIN classmaster ON classmaster.className = classteacherallotment.class
	                 WHERE classteacherallotment.academicStartYear = '".$currentYr."-04-01'
	                   AND classteacherallotment.academicEndYear = '".$nextYr."-03-31'
	              ORDER BY classmaster.priority,classteacherallotment.section";
	  $selectEmpRes = mysql_query($selectEmp);
	  while($empRow = mysql_fetch_array($selectEmpRes))
	  {
	  	$attendenceArr[$i]['class']    = $empRow['class'];
	  	$attendenceArr[$i]['section']  = $empRow['section'];
	  	$attendenceArr[$i]['name']     = $empRow['name'];
	  	$selectAbsent = "SELECT attendences,date AS attendenceDate,class,section,taken
	     	                 FROM attendence
	  	                  WHERE class = '".$empRow['class']."'
	  	                    AND section = '".$empRow['section']."'
	  	                    AND attendences = 'A'
	  	                    AND date = '".$attendenceDate."'
	  	                    AND taken = 'AT'";
	    $selectAbsentRes = mysql_query($selectAbsent);
	    $countAbsent = mysql_num_rows($selectAbsentRes);
	    $attendenceArr[$i]['countAbsent'] = $countAbsent;
	    $selectPresent = "SELECT attendences,date AS attendenceDate,taken
	                        FROM attendence 
	  	                   WHERE class = '".$empRow['class']."'
	  	                     AND section = '".$empRow['section']."'
	  	                     AND attendences = 'P'
	  	                     AND date = '".$attendenceDate."'
	  	                     AND taken = 'AT'";
	    $selectPresentRes = mysql_query($selectPresent);
	    $countPresent     = mysql_num_rows($selectPresentRes);
	    $attendenceArr[$i]['countPresent'] = $countPresent;
	    $attendenceArr[$i]['countAll']     = $countPresent + $countAbsent;
	  	
	  	$taken = '';
	  	$selectStdIn = "SELECT taken
		                    FROM attendence
		                   WHERE class = '".$empRow['class']."'
		                     AND section = '".$empRow['section']."'
		                     AND date = '".$attendenceDate."'";
		  $selectStdInRes = mysql_query($selectStdIn);
		  $countTaken = mysql_num_rows($selectPresentRes);
		  if($row = mysql_fetch_array($selectStdInRes))
		  $taken = $row['taken'];
	    if($countTaken == '')
	    {
	      $attendenceArr[$i]['taken'] = 'NT';
	    }
	    else
	    {
	    	$attendenceArr[$i]['taken'] = $taken;
	    }
	    $totalPresent += $countPresent;
	    $totalAbsent  += $countAbsent;
	    $i++;
	  }

	///////// Total Teaching Non-teaching Staff & their attendnace details.
	$totStaff = $totTeaching = $totNonTeaching = $teachingPresent = $nonteachingPresent = 0;

	$selectAttendM= "SELECT employeeCode,name,activated,userType
                     FROM employeemaster
                    WHERE activated = 'Yes' AND employeeCode IS NOT NULL ORDER BY userType DESC";
	$selectAttendMRes = mysql_query($selectAttendM);
	while($maRow = mysql_fetch_array($selectAttendMRes))
	{
		$totStaff = $totStaff + 1;

		if($maRow['userType'] == 'Teacher')
		{
			$totTeaching = $totTeaching+1;
		}
		else
			$totNonTeaching = $totNonTeaching+1;

		$selectAtnP = "SELECT attendences
		             FROM employeeattend
		            WHERE employeeCode = '".$maRow['employeeCode']."'
		              AND date = '".$attendenceDate."'	              
		              AND attendences = 'P'";
		$selectAtnPRes = mysql_query($selectAtnP);
		if(mysql_num_rows($selectAtnPRes) > 0)
		{
			if($maRow['userType'] == 'Teacher')
			{
				$teachingPresent = $teachingPresent+1;
			}
			else
				$nonteachingPresent = $nonteachingPresent+1;
		}
	}


?>
	<script type="text/javascript">
		window.onload = function () {
			var chart = new CanvasJS.Chart("chartContainer", {

			  data: [//array of dataSeries              
				{ //dataSeries object

				 /*** Change type "column" to "bar", "area", "line" or "pie"***/
				 type: "column",
				 dataPoints: [
				 { label: "Teaching (<?php echo $totTeaching;?>)", y: <?php echo $teachingPresent;?> },
				 { label: "Non-Teaching (<?php echo $totNonTeaching;?>)", y: <?php echo $nonteachingPresent;?> },
				 { label: "Students (<?php echo $totStuds;?>)", y: <?php echo $totalPresent;?> }
				 ]
			   }
			   ]
			 });
			chart.render();
			$('.canvasjs-chart-credit').html('');
			var chart1 = new CanvasJS.Chart("chartContainer1",
			{
			/*legend: {
			maxWidth: 950,
			itemWidth: 120
			},*/
			data: [
			{
				type: "pie",
				showInLegend: false,
				legendText: "{indexLabel}",
				dataPoints: [
				{ y: <?php echo $teachingPresent;?>, label: "Teaching (<?php echo $totTeaching;?>)", indexLabel: "Teaching" },
				{ y: <?php echo $nonteachingPresent;?>, label: "Non-Teaching (<?php echo $totNonTeaching;?>)", indexLabel: "Non-Teaching" },
				{ y: <?php echo $totalPresent;?>, label: "Students (<?php echo $totStuds;?>)", indexLabel: "Students" }
				]
				}
			]
			});
			
			chart1.render();
			$('.canvasjs-chart-credit').html('');
		}
	</script>  
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <?php include("include/mainheader.php"); ?>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>USER</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <?php include("include/leftsidebar.php"); ?>
	</section>
	<!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div id="chartTitle" style=" width:70%;text-align: center;"><h2>Staff & Student Attendance</h2></div>
		</div>
		</br>
		</br>
		<div class="row">
			<div class="col-xs-2">
			</div>
			<div class="col-xs-10">
				<div id="chartContainer" style="height:350px; width:80%;">
				</div>
			</div>
			<div class="col-xs-2">
			</div>
			<div class="col-xs-10">
				<div id="chartContainer1" style="height:350px; width:80%;">
				</div>
			</div>
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <?php include("include/footer.php"); ?>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- right side bar when required -->
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
	<?php include("include/filelinks.php"); ?>
</body>
</html>

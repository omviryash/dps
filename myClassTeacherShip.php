<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] != 'Teacher')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
  $i = 0;
  $selectClass = "SELECT classteacherallotment.classTeacherAllotmentId,employeemaster.loginId,
                         classteacherallotment.academicStartYear,classteacherallotment.academicEndYear,
                         classteacherallotment.class,classteacherallotment.section,
                         employeemaster.name
                    FROM classteacherallotment
               LEFT JOIN employeemaster ON employeemaster.employeeMasterId = classteacherallotment.employeeMasterId
                   WHERE employeemaster.loginId = '".$_SESSION['s_activName']."'";
  $selectClassRes = mysql_query($selectClass);
  while($classRow = mysql_fetch_array($selectClassRes))
  {
  	$classArray[$i]['classTeacherAllotmentId'] = $classRow['classTeacherAllotmentId'];
  	$classArray[$i]['name']                    = $classRow['name'];
  	$classArray[$i]['academicStartYear']       = substr($classRow['academicStartYear'],0,4);
  	$classArray[$i]['academicEndYear']         = substr($classRow['academicEndYear'],2,2);
  	
  	$classArray[$i]['class']   = $classRow['class'];
  	$classArray[$i]['section'] = $classRow['section'];
  	$i++;
  }
  
  include("./bottom.php");
  $smarty->assign('classArray',$classArray);
  $smarty->display('myClassTeacherShip.tpl');  
}
?>
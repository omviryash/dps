<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$qNo             = "";
	$subjectMasterId = "";
	$class           = "";
	$question        = "";
	$option1         = "";
	$option2         = "";
	$option3         = "";
	$option4         = "";
	$answer          = "";

	$isEdit          = 0;
	$onlineTestId    = 0;
	$classArray      = array();
	
  if(isset($_POST['Submit']))
  {
  	$qNo             = isset($_POST['qNo']) ? $_POST['qNo'] : '';
  	$subjectMasterId = isset($_POST['subjectMasterId']) ? $_POST['subjectMasterId'] : 0;
  	$class           = isset($_POST['class']) ? $_POST['class'] : '';
  	$question        = isset($_POST['question']) ? $_POST['question'] : '';
  	$option1         = isset($_POST['option1']) ? $_POST['option1'] : '';
  	$option2         = isset($_POST['option2']) ? $_POST['option2'] : '';
  	$option3         = isset($_POST['option3']) ? $_POST['option3'] : '';
  	$option4         = isset($_POST['option4']) ? $_POST['option4'] : '';
  	$answer          = isset($_POST['answer']) ? $_POST['answer'] : '';
  	
  	$onlineTestId  = isset($_POST['onlineTestId']) ? $_POST['onlineTestId'] : 0;
  	
  	if($onlineTestId == 0)
  	{
  	  $insertClass = "INSERT INTO onlinetest (qNo,subjectMasterId,class,question,option1,option2,option3,option4,answer)
  	                  VALUES ('".$qNo."','".$subjectMasterId."','".$class."','".addslashes($question)."','".$option1."','".$option2."','".$option3."','".$option4."','".$answer."')";
  	  $insertClassRes = om_query($insertClass);
  	  if(!$insertClassRes)
  	  {
  	  	echo "Insert Fail";
  	  }
  	  else
  	  {
  	  	header("Location:onlineTestEntry.php?done=1");
  	  }
  	}
  	else
  	{
  	  $isEdit = 1;
  	  $updateClass = "UPDATE onlinetest
  	                     SET qNo             = '".$qNo."',
  	                         subjectMasterId = '".$subjectMasterId."',
  	                         class           = '".$class."',
  	                         question        = '".addslashes($question)."',
  	                         option1         = '".$option1."',
  	                         option2         = '".$option2."',
  	                         option3         = '".$option3."',
  	                         option4         = '".$option4."',
  	                         answer          = '".$answer."'
  	                   WHERE onlineTestId = ".$_REQUEST['onlineTestId'];
      $updateClassRes = om_query($updateClass);
      if(!$updateClassRes)
      {
      	echo "Update Fail";
      }
      else
      {
      	header("Location:onlineTestEntry.php?done=1");
      }
  	}
  }
  
  $i = 0;
  $selectClass = "SELECT onlinetest.onlineTestId,subjectmaster.subjectName,onlinetest.class,onlinetest.question,
                         onlinetest.option1,onlinetest.option2,onlinetest.option3,onlinetest.option4,onlinetest.answer
                    FROM onlinetest
               LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = onlinetest.subjectMasterId";
  $selectClassRes = mysql_query($selectClass);
  while($classRow = mysql_fetch_array($selectClassRes))
  {
  	$classArray[$i]['onlineTestId']   = $classRow['onlineTestId'];
  	$classArray[$i]['subjectName']    = $classRow['subjectName'];
  	$classArray[$i]['question']       = $classRow['question'];
  	$classArray[$i]['option1']        = $classRow['option1'];
  	$classArray[$i]['option2']        = $classRow['option2'];
  	$classArray[$i]['option3']        = $classRow['option3'];
  	$classArray[$i]['option4']        = $classRow['option4'];
  	$classArray[$i]['answer']         = $classRow['answer'];
  	$i++;
  }
  
  if(isset($_REQUEST['onlineTestId']) > 0)
  {
    $selectClass = "SELECT onlinetest.onlineTestId,onlinetest.qNo,subjectmaster.subjectMasterId,onlinetest.class,onlinetest.question,
                           onlinetest.option1,onlinetest.option2,onlinetest.option3,onlinetest.option4,onlinetest.answer
	                    FROM onlinetest
	               LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = onlinetest.subjectMasterId
                     WHERE onlineTestId = ".$_REQUEST['onlineTestId']."";
    $selectClassRes = mysql_query($selectClass);
    if($classRow = mysql_fetch_array($selectClassRes))
    {
    	$onlineTestId    = $classRow['onlineTestId'];
    	$qNo             = $classRow['qNo'];
    	$subjectMasterId = $classRow['subjectMasterId'];
    	$class           = $classRow['class'];
    	$question        = $classRow['question'];
    	$option1         = $classRow['option1'];
    	$option2         = $classRow['option2'];
    	$option3         = $classRow['option3'];
    	$option4         = $classRow['option4'];
    	$answer          = $classRow['answer'];
    }
  }
  
	$c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  $k = 0;
  $selectClub = "SELECT subjectMasterId,subjectName
                   FROM subjectmaster
                  WHERE subjectType = 'Main' OR subjectType = 'Optional'
               ORDER BY subjectName";
  $selectClubRes = mysql_query($selectClub);
  while($clubRow = mysql_fetch_array($selectClubRes))
  {
  	$clubArr['subjectMasterId'][$k]   = $clubRow['subjectMasterId'];
  	$clubArr['subjectName'][$k]       = $clubRow['subjectName'];
  	$k++;
  }
	
  $ansArr[0] = 'A';
  $ansArr[1] = 'B';
  $ansArr[2] = 'C';
  $ansArr[3] = 'D';
  
  include("./bottom.php");
  $smarty->assign('onlineTestId',$onlineTestId);
  $smarty->assign('isEdit',$isEdit);
  $smarty->assign('qNo',$qNo);
  $smarty->assign('subjectMasterId',$subjectMasterId);
  $smarty->assign('class',$class);
  $smarty->assign('question',$question);
  $smarty->assign('option1',$option1);
  $smarty->assign('option2',$option2);
  $smarty->assign('option3',$option3);
  $smarty->assign('option4',$option4);
  $smarty->assign('ansArr',$ansArr);
  $smarty->assign('answer',$answer);
  $smarty->assign('cArray',$cArray);
  $smarty->assign('classArray',$classArray);
  $smarty->assign('clubArr',$clubArr);
  $smarty->display('onlineTestEntry.tpl');  
}
?>
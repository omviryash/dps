<?php
define('FPDF_FONTPATH', 'font/');
require('./font/fpdf.php');
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']))
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
  $pdf=new FPDF('P','mm','A4');   //Create new pdf file
  $pdf->Open();     //Open file
  $pdf->SetAutoPageBreak(false);  //Disable automatic page break
  $pdf->AddPage();  //Add first page


  //header part start
  $pdf->Image('./images/logo.png',10,10,22,25);
  $pdf->Image('./images/logoIndex.png',65,2,70,20);
  //header part end
  //table header part start  
  $pdf->SetFillColor(232, 232, 232);
  $pdf->SetFont('Arial', '', 8);
  $pdf->SetXY(5,20);
  $pdf->Cell(200, 5, 'Haripar, Survey No. 12, Behind NRI Bunglows, Rajkot - 360 007', 0,0,'C',0);
  $pdf->SetXY(5,25);
  $pdf->Cell(200, 5, 'Ph: 0281 2923170, 2926801, Email : Info@dpsrajkot.org, Website: www.dpsrajkot.org', 0,0,'C',0);
  $pdf->SetXY(5,30);
  $pdf->Cell(200, 5, '(Affilited to C B S E, New Delhi, Affiliation No.430054)', 0,0,'C',0);
  $pdf->SetFont('Arial', 'B', 12);
  $pdf->SetXY(5,35);
  $pdf->Cell(200, 5, 'Record of Academic Performance', 0,0,'C',0);
  
  $pdf->SetFillColor(232, 232, 232);
  $pdf->SetFont('Arial', 'B', 12);
  $pdf->SetXY(5,95);
  $pdf->Cell(50, 10, 'Subject', 1,0,'C',1);
  $pdf->Cell(30, 10, 'Half Yearly', 1,0,'C',1);
  $pdf->Cell(30, 10, 'Annual Exam', 1,0,'C',1);
  $pdf->Cell(30, 10, 'Monday Test', 1,0,'C',1);
  $pdf->Cell(30, 10, 'Practicle', 1,0,'C',1);
  $pdf->Cell(30, 10, 'Cumulative', 1,0,'C',1);
  
  $i         = 0; 
  $rowHeight = 10;
  $yAxis     = 95;

  $yAxis = $yAxis + $rowHeight;
  
  //table header part end 

  if(isset($_REQUEST['academicYear']))
  {
  	$academicStartYear = substr($_REQUEST['academicYear'],0,4)."-04-01";
  	$nextYear          = substr($_REQUEST['academicYear'],0,4) + 1;
  	$academicEndYear   = $nextYear."-03-31";
  }
  else
  {
	  $todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y')."-04-01";
	  	$nextYear          = date('Y') + 1;
	  	$academicEndYear   = $nextYear."-03-31";
		}
		else
		{
			$prevYear          = date('Y') - 1;
			$academicStartYear = $prevYear."-04-01";
	  	$academicEndYear   = date('Y')."-03-31";
		}
	}
	
	if(isset($_REQUEST['grNo']))
	{
	  $myGrNo = "AND nominalroll.grNo = ".$_REQUEST['grNo']."";
	}
	else
	{
		$myGrNo = "AND studentmaster.studentLoginId = '".$_SESSION['s_activName']."' 
                   OR studentmaster.parentLoginId = '".$_SESSION['s_activName']."'";
	}
	//print column titles for the actual page
  $pdf->SetFillColor(232, 232, 232);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->SetXY(0,40);
  $pdf->Cell(210, 5, substr($academicStartYear,0,4).' - '.substr($academicEndYear,0,4), 0, 0, 'C', 0);
  
  $marksEng1  = 0;
	$marksEng2  = 0;
	$marksPracticalEng2  = 0;
	$marksEng3  = 0;
	$marksPhy1  = 0;
	$marksPhy2  = 0;
	$marksPracticalPhy2  = 0;
	$marksPhy3  = 0;
	$marksChe1  = 0;
	$marksChe2  = 0;
	$marksPracticalChe2  = 0;
	$marksChe3  = 0;
	$marksBio1  = 0;
	$marksBio2  = 0;
	$marksPracticalBio2  = 0;
	$marksBio3  = 0;
	$marksIp1   = 0;
	$marksIp2   = 0;
	$marksPracticalIp2   = 0;
	$marksIp3   = 0;
  $marksPe1   = 0;
	$marksPe2   = 0;
	$marksPracticalPe2   = 0;
	$marksPe3   = 0;
	
  $selectNominal = "SELECT nominalroll.grNo,nominalroll.academicStartYear,nominalroll.academicEndYear,nominalroll.class,
                           nominalroll.section,nominalroll.rollNo,studentmaster.activated,studentmaster.bloodGroup,studentmaster.studentName,
                           studentmaster.fatherName,studentmaster.mothersName,studentmaster.dateOfBirth,studentmaster.currentAddress,
                           house.houseName,nominalroll.height,nominalroll.weight
                      FROM nominalroll
                 LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
                 LEFT JOIN house ON house.houseId = studentmaster.houseId
                     WHERE 1 = 1
                       ".$myGrNo."
                       AND nominalroll.academicStartYear = '".$academicStartYear."'
                       AND nominalroll.academicEndYear = '".$academicEndYear."'
                       AND studentmaster.activated = 'Y'
                  ORDER BY nominalroll.rollNo";
  $selectNominalRes = mysql_query($selectNominal);
  while($nominalRow = mysql_fetch_array($selectNominalRes))
  {
    $class              = $nominalRow['class'];
    $section            = $nominalRow['section'];
    $grNo               = $nominalRow['grNo'];
    $studentName        = $nominalRow['studentName'];
    $fatherName         = $nominalRow['fatherName'];
    $mothersName        = $nominalRow['mothersName'];
    $dateOfBirth        = $nominalRow['dateOfBirth'];
    $currentAddress     = $nominalRow['currentAddress'];
    $academicYear       = substr($nominalRow['academicStartYear'],0,4).'-'.substr($nominalRow['academicEndYear'],0,4);
    $rollNo             = $nominalRow['rollNo'];
    $house              = $nominalRow['houseName'];
  	$height             = $nominalRow['height'];
  	$weight             = $nominalRow['weight'];
  	$bloodGroup         = $nominalRow['bloodGroup'];
    
    $pdf->SetFillColor(232, 232, 232);
	  $pdf->SetFont('Arial', 'B', 10);
	  $pdf->SetXY(5,45);
	  $pdf->Cell(35, 5, 'Name of Student : ', 0, 0, 'L', 1);
	  $pdf->Cell(105,5, $studentName, 0, 0, 'L', 1);
	  $pdf->Cell(35, 5, 'Gr No : ', 0, 0, 'R', 1);
	  $pdf->Cell(25, 5, $grNo, 0, 0, 'R', 1);
	  
    $pdf->SetFillColor(232, 232, 232);
	  $pdf->SetFont('Arial', 'B', 10);
	  $pdf->SetXY(5,55);
	  $pdf->Cell(35, 5, 'Fathes Name : ', 0, 0, 'L', 1);
	  $pdf->Cell(105,5, $fatherName, 0, 0, 'L', 1);
	  $pdf->Cell(35, 5, 'Class : ', 0, 0, 'R', 1);
	  $pdf->Cell(25, 5, $class.' - '.$section, 0, 0, 'R', 1);
	  
	  $pdf->SetFillColor(232, 232, 232);
	  $pdf->SetFont('Arial', 'B', 10);
	  $pdf->SetXY(5,65);
	  $pdf->Cell(35, 5, 'Mothers Name : ', 0, 0, 'L', 1);
	  $pdf->Cell(105,5, $mothersName, 0, 0, 'L', 1);
	  $pdf->Cell(35, 5, 'DOB : ', 0, 0, 'R', 1);
	  $pdf->Cell(25, 5, $dateOfBirth, 0, 0, 'R', 1);
	  
    $pdf->SetFillColor(232, 232, 232);
	  $pdf->SetFont('Arial', 'B', 10);
	  $pdf->SetXY(5,75);
	  $pdf->Cell(35, 5, 'Roll No : ', 0, 0, 'L', 1);
	  $pdf->Cell(105,5, $rollNo, 0, 0, 'L', 1);
	  $pdf->Cell(35, 5, 'House : ', 0, 0, 'R', 1);
	  $pdf->Cell(25, 5, $house, 0, 0, 'R', 1);
	  
    $pdf->SetFillColor(232, 232, 232);
	  $pdf->SetFont('Arial', 'B', 10);
	  $pdf->SetXY(5,85);
	  $pdf->Cell(35, 5, 'Address : ', 0, 0, 'L', 1);
	  $pdf->Cell(165,5, $currentAddress, 0, 0, 'L', 1);
	  
    $selectAtnP = "SELECT exammarks.marks,exammarks.class,exammarks.section,subjectmaster.subjectName,examtype.examType,
                          examschedule.scheduleDate
                     FROM exammarks
                LEFT JOIN examschedule ON examschedule.examScheduleId = exammarks.examScheduleId
                LEFT JOIN examtype ON examtype.examTypeId = examschedule.examTypeId
                LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = examschedule.subjectMasterId
                    WHERE grNo = '".$grNo."'
                      AND examschedule.scheduleDate >= '".$academicStartYear."'
                      AND examschedule.scheduleDate <= '".$academicEndYear."'
                      AND examtype.examType = 'Half Yearly Examination'";
    $selectAtnPRes = mysql_query($selectAtnP);
    while($markRow = mysql_fetch_array($selectAtnPRes))
    {
    	$subjectName1  = 'English';
    	$subjectName2  = 'Accountancy(055)';
    	$subjectName3  = 'Business Studies(054)';
    	$subjectName4  = 'Economics(033)';
    	$subjectName6  = 'Informatics Practices(065)';
    	$subjectName7  = 'Physical Education(048)';
    	$subjectName   = $markRow['subjectName'];
    	if($markRow['subjectName'] == 'English')
    	{
    		$subjectName1  = $markRow['subjectName'];
    	}
    	if($markRow['subjectName'] == 'Accountancy(055)')
    	{
    		$subjectName2  = $markRow['subjectName'];
    	}
    	if($markRow['subjectName'] == 'Business Studies(054)')
    	{
    		$subjectName3  = $markRow['subjectName'];
    	}
    	if($markRow['subjectName'] == 'Economics(033)')
    	{
    		$subjectName4  = $markRow['subjectName'];
    	}
    	if($markRow['subjectName'] == 'Informatics Practices(065)')
    	{
    		$subjectName6  = $markRow['subjectName'];
    	}
    	if($markRow['subjectName'] == 'Physical Education(048)')
    	{
    		$subjectName7  = $markRow['subjectName'];
    	}
    	$marks        = $markRow['marks'];
	  	
	  	$pdf->SetFillColor(232, 232, 232);
	    $pdf->SetFont('Arial', '', 10);
	    if($subjectName == 'English')
	    {
	    	$marksEng1 = $marks * 40/100;
	    	$pdf->SetXY(5,$yAxis);
	    	$pdf->Cell(50, 10, $subjectName1, 1,0,'L');
	    	$pdf->Cell(30, 10, $marksEng1, 1,0,'C');
	    }
	    else
	    {
	    	$pdf->SetXY(5,$yAxis);
	    	$pdf->Cell(50, 10, $subjectName1, 1,0,'L');
	    	$pdf->Cell(30, 10, '', 1,0,'C');
	    }
		  if($subjectName == 'Accountancy(055)')
		  {
		  	$marksPhy1 = $marks * 30/80;
		  	$pdf->SetXY(5,$yAxis+10);
	    	$pdf->Cell(50, 10, $subjectName2, 1,0,'L');
	    	$pdf->Cell(30, 10, $marksPhy1, 1,0,'C');
		  }
	    else
	    {
	    	$pdf->SetXY(5,$yAxis+10);
	    	$pdf->Cell(50, 10, $subjectName2, 1,0,'L');
	    	$pdf->Cell(30, 10, '', 1,0,'C');
	    }
		  if($subjectName == 'Business Studies(054)')
		  {
		  	$marksChe1 = $marks * 35/90;
		  	$pdf->SetXY(5,$yAxis+20);
	    	$pdf->Cell(50, 10, $subjectName3, 1,0,'L');
	    	$pdf->Cell(30, 10, $marksChe1, 1,0,'C');
		  }
	    else
	    {
	    	$pdf->SetXY(5,$yAxis+20);
	    	$pdf->Cell(50, 10, $subjectName3, 1,0,'L');
	    	$pdf->Cell(30, 10, '', 1,0,'C');
	    }
		  if($subjectName == 'Economics(033)')
		  {
		  	$marksBio1 = $marks * 35/95;
		  	$pdf->SetXY(5,$yAxis+30);
	    	$pdf->Cell(50, 10, $subjectName4, 1,0,'L');
	    	$pdf->Cell(30, 10, $marksBio1, 1,0,'C');
		  }
	    else
	    {
	    	$pdf->SetXY(5,$yAxis+30);
	    	$pdf->Cell(50, 10, $subjectName4, 1,0,'L');
	    	$pdf->Cell(30, 10, '', 1,0,'C');
	    }
		  if($subjectName == 'Informatics Practices(065)')
		  {
		  	$marksIp1 = $marks * 25/70;
		  	$pdf->SetXY(5,$yAxis+40);
	    	$pdf->Cell(50, 10, $subjectName6, 1,0,'L');
	    	$pdf->Cell(30, 10, $marksIp1, 1,0,'C');
		  }
	    else
	    {
	    	$pdf->SetXY(5,$yAxis+40);
	    	$pdf->Cell(50, 10, $subjectName6, 1,0,'L');
	    	$pdf->Cell(30, 10, '', 1,0,'C');
	    }
		  if($subjectName == 'Physical Education(048)')
		  {
		  	$marksPe1 = $marks * 25/70;
		  	$pdf->SetXY(5,$yAxis+50);
	    	$pdf->Cell(50, 10, $subjectName7, 1,0,'L');
	    	$pdf->Cell(30, 10, $marksPe1, 1,0,'C');
		  }
	    else
	    {
	    	$pdf->SetXY(5,$yAxis+50);
	    	$pdf->Cell(50, 10, $subjectName7, 1,0,'L');
	    	$pdf->Cell(30, 10, '', 1,0,'C');
	    }
	  }
	  
	  $selectAtnP = "SELECT exammarks.marks,exammarks.class,exammarks.section,subjectmaster.subjectName,examtype.examType,
                          examschedule.scheduleDate
                     FROM exammarks
                LEFT JOIN examschedule ON examschedule.examScheduleId = exammarks.examScheduleId
                LEFT JOIN examtype ON examtype.examTypeId = examschedule.examTypeId
                LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = examschedule.subjectMasterId
                    WHERE grNo = '".$grNo."'
                      AND examschedule.scheduleDate >= '".$academicStartYear."'
                      AND examschedule.scheduleDate <= '".$academicEndYear."'
                      AND examtype.examType = 'Annual Examination'";
    $selectAtnPRes = mysql_query($selectAtnP);
    while($markRow = mysql_fetch_array($selectAtnPRes))
    {
    	$subjectName1  = 'English';
    	$subjectName2  = 'Accountancy(055)';
    	$subjectName3  = 'Business Studies(054)';
    	$subjectName4  = 'Economics(033)';
    	$subjectName6  = 'Informatics Practices(065)';
    	$subjectName7  = 'Physical Education(048)';
    	$subjectName   = $markRow['subjectName'];
    	if($markRow['subjectName'] == 'English')
    	{
    		$subjectName1  = $markRow['subjectName'];
    	}
    	if($markRow['subjectName'] == 'Accountancy(055)')
    	{
    		$subjectName2  = $markRow['subjectName'];
    	}
    	if($markRow['subjectName'] == 'Business Studies(054)')
    	{
    		$subjectName3  = $markRow['subjectName'];
    	}
    	if($markRow['subjectName'] == 'Economics(033)')
    	{
    		$subjectName4  = $markRow['subjectName'];
    	}
    	if($markRow['subjectName'] == 'Informatics Practices(065)')
    	{
    		$subjectName6  = $markRow['subjectName'];
    	}
    	if($markRow['subjectName'] == 'Physical Education(048)')
    	{
    		$subjectName7  = $markRow['subjectName'];
    	}
    	$marks        = $markRow['marks'];
	  	
	  	$pdf->SetFillColor(232, 232, 232);
	    $pdf->SetFont('Arial', '', 10);
	    if($subjectName == 'English')
	    {
	    	$marksEng2 = $marks * 40/100;
	    	$pdf->SetXY(85,$yAxis);
	    	$pdf->Cell(30, 10, $marksEng2, 1,0,'C');
	    }
	    else
	    {
	    	$pdf->SetXY(85,$yAxis);
	    	$pdf->Cell(30, 10, '', 1,0,'C');
	    }
		  if($subjectName == 'Accountancy(055)')
		  {
		  	$marksPhy2 = $marks * 30/80;
		  	$pdf->SetXY(85,$yAxis+10);
	    	$pdf->Cell(30, 10, $marksPhy2, 1,0,'C');
		  }
	    else
	    {
	    	$pdf->SetXY(85,$yAxis+10);
	    	$pdf->Cell(30, 10, '', 1,0,'C');
	    }
		  if($subjectName == 'Business Studies(054)')
		  {
		  	$marksChe2 = $marks * 35/90;
		  	$pdf->SetXY(85,$yAxis+20);
	    	$pdf->Cell(30, 10, $marksChe2, 1,0,'C');
		  }
	    else
	    {
	    	$pdf->SetXY(85,$yAxis+20);
	    	$pdf->Cell(30, 10, '', 1,0,'C');
	    }
		  if($subjectName == 'Economics(033)')
		  {
		  	$marksBio2 = $marks * 40/95;
		  	$pdf->SetXY(85,$yAxis+30);
	    	$pdf->Cell(30, 10, $marksBio2, 1,0,'C');
		  }
	    else
	    {
	    	$pdf->SetXY(85,$yAxis+30);
	    	$pdf->Cell(30, 10, '', 1,0,'C');
	    }
		  if($subjectName == 'Informatics Practices(065)')
		  {
		  	$marksIp2 = $marks * 25/70;
		  	$pdf->SetXY(85,$yAxis+40);
	    	$pdf->Cell(30, 10, $marksIp2, 1,0,'C');
		  }
	    else
	    {
	    	$pdf->SetXY(85,$yAxis+40);
	    	$pdf->Cell(30, 10, '', 1,0,'C');
	    }
		  if($subjectName == 'Physical Education(048)')
		  {
		  	$marksPe2 = $marks * 25/70;
		  	$pdf->SetXY(85,$yAxis+50);
	    	$pdf->Cell(30, 10, $marksPe2, 1,0,'C');
		  }
	    else
	    {
	    	$pdf->SetXY(85,$yAxis+50);
	    	$pdf->Cell(30, 10, '', 1,0,'C');
	    }
	  }
	  
	  $selectAtnP = "SELECT exammarks.marks,exammarks.class,exammarks.section,subjectmaster.subjectName,examtype.examType,
                          examschedule.scheduleDate,exammarks.marksPractical
                     FROM exammarks
                LEFT JOIN examschedule ON examschedule.examScheduleId = exammarks.examScheduleId
                LEFT JOIN examtype ON examtype.examTypeId = examschedule.examTypeId
                LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = examschedule.subjectMasterId
                    WHERE grNo = '".$grNo."'
                      AND examschedule.scheduleDate >= '".$academicStartYear."'
                      AND examschedule.scheduleDate <= '".$academicEndYear."'
                      AND examtype.examType = 'Annual Examination'";
    $selectAtnPRes = mysql_query($selectAtnP);
    while($markRow = mysql_fetch_array($selectAtnPRes))
    {
    	$subjectName1  = 'English';
    	$subjectName2  = 'Accountancy(055)';
    	$subjectName3  = 'Business Studies(054)';
    	$subjectName4  = 'Economics(033)';
    	$subjectName6  = 'Informatics Practices(065)';
    	$subjectName7  = 'Physical Education(048)';
    	$subjectName   = $markRow['subjectName'];
    	if($markRow['subjectName'] == 'English')
    	{
    		$subjectName1  = $markRow['subjectName'];
    	}
    	if($markRow['subjectName'] == 'Accountancy(055)')
    	{
    		$subjectName2  = $markRow['subjectName'];
    	}
    	if($markRow['subjectName'] == 'Business Studies(054)')
    	{
    		$subjectName3  = $markRow['subjectName'];
    	}
    	if($markRow['subjectName'] == 'Economics(033)')
    	{
    		$subjectName4  = $markRow['subjectName'];
    	}
    	if($markRow['subjectName'] == 'Informatics Practices(065)')
    	{
    		$subjectName6  = $markRow['subjectName'];
    	}
    	if($markRow['subjectName'] == 'Physical Education(048)')
    	{
    		$subjectName7  = $markRow['subjectName'];
    	}
    	$marks          = $markRow['marks'];
    	$marksPractical = $markRow['marksPractical'];
	  	
	  	$pdf->SetFillColor(232, 232, 232);
	    $pdf->SetFont('Arial', '', 10);
	    if($subjectName == 'English')
	    {
	    	$marksPracticalEng2 = $marksPractical;
	    	$pdf->SetXY(145,$yAxis);
	    	$pdf->Cell(30, 10, $marksPracticalEng2, 1,0,'C');
	    }
	    else
	    {
	    	$pdf->SetXY(145,$yAxis);
	    	$pdf->Cell(30, 10, '', 1,0,'C');
	    }
		  if($subjectName == 'Accountancy(055)')
		  {
		  	$marksPracticalPhy2 = $marksPractical;
		  	$pdf->SetXY(145,$yAxis+10);
	    	$pdf->Cell(30, 10, $marksPracticalPhy2, 1,0,'C');
		  }
	    else
	    {
	    	$pdf->SetXY(145,$yAxis+10);
	    	$pdf->Cell(30, 10, '', 1,0,'C');
	    }
		  if($subjectName == 'Business Studies(054)')
		  {
		  	$marksPracticalChe2 = $marksPractical;
		  	$pdf->SetXY(145,$yAxis+20);
	    	$pdf->Cell(30, 10, $marksPracticalChe2, 1,0,'C');
		  }
	    else
	    {
	    	$pdf->SetXY(145,$yAxis+20);
	    	$pdf->Cell(30, 10, '', 1,0,'C');
	    }
		  if($subjectName == 'Economics(033)')
		  {
		  	$marksPracticalBio2 = $marksPractical;
		  	$pdf->SetXY(145,$yAxis+30);
	    	$pdf->Cell(30, 10, $marksPracticalBio2, 1,0,'C');
		  }
	    else
	    {
	    	$pdf->SetXY(145,$yAxis+30);
	    	$pdf->Cell(30, 10, '', 1,0,'C');
	    }
		  if($subjectName == 'Informatics Practices(065)')
		  {
		  	$marksPracticalIp2 = $marksPractical;
		  	$pdf->SetXY(145,$yAxis+50);
	    	$pdf->Cell(30, 10, $marksPracticalIp2, 1,0,'C');
		  }
	    else
	    {
	    	$pdf->SetXY(145,$yAxis+50);
	    	$pdf->Cell(30, 10, '', 1,0,'C');
	    }
		  if($subjectName == 'Physical Education(048)')
		  {
		  	$marksPracticalPe2 = $marksPractical;
		  	$pdf->SetXY(145,$yAxis+60);
	    	$pdf->Cell(30, 10, $marksPracticalPe2, 1,0,'C');
		  }
	    else
	    {
	    	$pdf->SetXY(145,$yAxis+60);
	    	$pdf->Cell(30, 10, '', 1,0,'C');
	    }
	  }
	  
	  $selectAtnP = "SELECT MAX(exammarks.marks) AS marks,exammarks.class,exammarks.section,subjectmaster.subjectName,examtype.examType,
                          examschedule.scheduleDate
                     FROM exammarks
                LEFT JOIN examschedule ON examschedule.examScheduleId = exammarks.examScheduleId
                LEFT JOIN examtype ON examtype.examTypeId = examschedule.examTypeId
                LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = examschedule.subjectMasterId
                    WHERE grNo = '".$grNo."'
                      AND examschedule.scheduleDate >= '".$academicStartYear."'
                      AND examschedule.scheduleDate <= '".$academicEndYear."'
                      AND examtype.examType = 'Monday Test'
                      GROUP BY subjectmaster.subjectName,examtype.examType";
    $selectAtnPRes = mysql_query($selectAtnP);
    while($markRow = mysql_fetch_array($selectAtnPRes))
    {
    	$subjectName1  = 'English';
    	$subjectName2  = 'Accountancy(055)';
    	$subjectName3  = 'Business Studies(054)';
    	$subjectName4  = 'Economics(033)';
    	$subjectName6  = 'Informatics Practices(065)';
    	$subjectName7  = 'Physical Education(048)';
    	$subjectName   = $markRow['subjectName'];
    	if($markRow['subjectName'] == 'English')
    	{
    		$subjectName1  = $markRow['subjectName'];
    	}
    	if($markRow['subjectName'] == 'Accountancy(055)')
    	{
    		$subjectName2  = $markRow['subjectName'];
    	}
    	if($markRow['subjectName'] == 'Business Studies(054)')
    	{
    		$subjectName3  = $markRow['subjectName'];
    	}
    	if($markRow['subjectName'] == 'Economics(033)')
    	{
    		$subjectName4  = $markRow['subjectName'];
    	}
    	if($markRow['subjectName'] == 'Informatics Practices(065)')
    	{
    		$subjectName6  = $markRow['subjectName'];
    	}
    	if($markRow['subjectName'] == 'Physical Education(048)')
    	{
    		$subjectName7  = $markRow['subjectName'];
    	}
    	$marks        = $markRow['marks'];
	  	
	  	$pdf->SetFillColor(232, 232, 232);
	    $pdf->SetFont('Arial', '', 10);
	    if($subjectName == 'English')
	    {
	    	$marksEng3 = $marks;
	    	$pdf->SetXY(115,$yAxis);
	    	$pdf->Cell(30, 10, $marksEng3, 1,0,'C');
	    }
	    else
	    {
	    	$pdf->SetXY(115,$yAxis);
	    	$pdf->Cell(30, 10, '', 1,0,'C');
	    }
		  if($subjectName == 'Accountancy(055)')
		  {
		  	$marksPhy3 = $marks;
		  	$pdf->SetXY(115,$yAxis+10);
	    	$pdf->Cell(30, 10, $marksPhy3, 1,0,'C');
		  }
	    else
	    {
	    	$pdf->SetXY(115,$yAxis+10);
	    	$pdf->Cell(30, 10, '', 1,0,'C');
	    }
		  if($subjectName == 'Business Studies(054)')
		  {
		  	$marksChe3 = $marks;
		  	$pdf->SetXY(115,$yAxis+20);
	    	$pdf->Cell(30, 10, $marksChe3, 1,0,'C');
		  }
	    else
	    {
	    	$pdf->SetXY(115,$yAxis+20);
	    	$pdf->Cell(30, 10, '', 1,0,'C');
	    }
		  if($subjectName == 'Economics(033)')
		  {
		  	$marksBio3 = $marks;
		  	$pdf->SetXY(115,$yAxis+30);
	    	$pdf->Cell(30, 10, $marksBio3, 1,0,'C');
		  }
	    else
	    {
	    	$pdf->SetXY(115,$yAxis+30);
	    	$pdf->Cell(30, 10, '', 1,0,'C');
	    }
		  if($subjectName == 'Informatics Practices(065)')
		  {
		  	$marksIp3 = $marks;
		  	$pdf->SetXY(115,$yAxis+40);
	    	$pdf->Cell(30, 10, $marksIp3, 1,0,'C');
		  }
	    else
	    {
	    	$pdf->SetXY(115,$yAxis+40);
	    	$pdf->Cell(30, 10, '', 1,0,'C');
	    }
		  if($subjectName == 'Physical Education(048)')
		  {
		  	$marksPe3 = $marks;
		  	$pdf->SetXY(115,$yAxis+50);
	    	$pdf->Cell(30, 10, $marksPe3, 1,0,'C');
		  }
	    else
	    {
	    	$pdf->SetXY(115,$yAxis+50);
	    	$pdf->Cell(30, 10, '', 1,0,'C');
	    }
	    
	    $pdf->SetFillColor(232, 232, 232);
	    $pdf->SetFont('Arial', '', 10);
	    if($subjectName == 'English')
	    {
	    	$marksEngTotal = $marksEng1 + $marksEng2 + $marksEng3 + $marksPracticalEng2;
	    	$pdf->SetXY(175,$yAxis);
	    	$pdf->Cell(30, 10, $marksEngTotal, 1,0,'C');
	    }
	    else
	    {
	    	$pdf->SetXY(175,$yAxis);
	    	$pdf->Cell(30, 10, '', 1,0,'C');
	    }
		  if($subjectName == 'Accountancy(055)')
		  {
		  	$marksPhyTotal = $marksPhy1 + $marksPhy2 + $marksPhy3 + $marksPracticalPhy2;
		  	$pdf->SetXY(175,$yAxis+10);
	    	$pdf->Cell(30, 10, $marksPhyTotal, 1,0,'C');
		  }
	    else
	    {
	    	$pdf->SetXY(175,$yAxis+10);
	    	$pdf->Cell(30, 10, '', 1,0,'C');
	    }
		  if($subjectName == 'Business Studies(054)')
		  {
		  	$marksCheTotal = $marksChe1 + $marksChe2 + $marksChe3 + $marksPracticalChe2;
		  	$pdf->SetXY(175,$yAxis+20);
	    	$pdf->Cell(30, 10, $marksCheTotal, 1,0,'C');
		  }
	    else
	    {
	    	$pdf->SetXY(175,$yAxis+20);
	    	$pdf->Cell(30, 10, '', 1,0,'C');
	    }
		  if($subjectName == 'Economics(033)')
		  {
		  	$marksBioTotal = $marksBio1 + $marksBio2 + $marksBio3 + $marksPracticalBio2;
		  	$pdf->SetXY(175,$yAxis+30);
	    	$pdf->Cell(30, 10, $marksBioTotal, 1,0,'C');
		  }
	    else
	    {
	    	$pdf->SetXY(175,$yAxis+30);
	    	$pdf->Cell(30, 10, '', 1,0,'C');
	    }
		  if($subjectName == 'Informatics Practices(065)')
		  {
		  	$marksIpTotal = $marksIp1 + $marksIp2 + $marksIp3 + $marksPracticalIp2;
		  	$pdf->SetXY(175,$yAxis+40);
	    	$pdf->Cell(30, 10, $marksIpTotal, 1,0,'C');
		  }
	    else
	    {
	    	$pdf->SetXY(175,$yAxis+40);
	    	$pdf->Cell(30, 10, '', 1,0,'C');
	    }
		  if($subjectName == 'Physical Education(048)')
		  {
		  	$marksPeTotal = $marksPe1 + $marksPe2 + $marksPe3 + $marksPracticalPe2;
		  	$pdf->SetXY(175,$yAxis+50);
	    	$pdf->Cell(30, 10, $marksPeTotal, 1,0,'C');
		  }
	    else
	    {
	    	$pdf->SetXY(175,$yAxis+50);
	    	$pdf->Cell(30, 10, '', 1,0,'C');
	    }
	  }
	  
	  $selectAtnP = "SELECT attendences
                     FROM attendence
                    WHERE class = '".$class."'
                      AND section = '".$section."'
                      AND grNo = '".$grNo."'
                      AND date >= '".$academicStartYear."-04-01'
                      AND date <= '".$academicEndYear."-03-31'
                      AND attendences = 'P'";
    $selectAtnPRes = mysql_query($selectAtnP);
    $countP = mysql_num_rows($selectAtnPRes);
    
    $selectAtnA = "SELECT attendences
                     FROM attendence
                    WHERE class = '".$class."'
                      AND section = '".$section."'
                      AND grNo = '".$grNo."'
                      AND date >= '".$academicStartYear."-04-01'
                      AND date <= '".$academicEndYear."-03-31'
                      AND attendences = 'A'";
    $selectAtnARes = mysql_query($selectAtnA);
    $countA = mysql_num_rows($selectAtnARes);
    $totalNumberWorkingDay = $countA + $countP;
    
    $pdf->SetFont('Arial', '', 8);
	  $pdf->SetXY(5,$yAxis+100);
	  $pdf->Cell(40, 10, 'Attendance', 1,0,'C',0);
	  $pdf->Cell(40, 10, $countP.'/'.$totalNumberWorkingDay, 1,0,'C');
	  $pdf->Cell(30, 10, 'Height', 1,0,'C');
	  $pdf->Cell(30, 10, $height, 1,0,'C');
	  $pdf->Cell(30, 10, 'Weight', 1,0,'C');
	  $pdf->Cell(30, 10, $weight, 1,0,'C');
	  
	  $pdf->SetFillColor(232, 232, 232);
	  $pdf->SetFont('Arial', '', 10);
	  $pdf->SetXY(5,$yAxis+130);
	  $pdf->Cell(50, 5, 'Class Teacher Remarks : ', 0, 0, 'L', 0);
	  $pdf->Cell(150,5, '', 0, 0, 'L', 0);
	  
	  $pdf->SetFillColor(232, 232, 232);
	  $pdf->SetFont('Arial', '', 8);
	  $pdf->SetXY(5,$yAxis+160);
	  $pdf->Cell(100, 5, 'CLASS TEACHERS SIGNATURE', 0, 0, 'L', 0);
	  $pdf->Cell(100, 5, 'PRINCIPALS SIGNATURE & SEAL', 0, 0, 'R', 0);
  }
  
  $pdf->Output();
}
?>
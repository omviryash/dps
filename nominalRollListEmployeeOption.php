<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$classNew = '';
	$nominalArr = array();
	if(isset($_REQUEST['startYear']))
  {
  	$academicStartYear = $_REQUEST['startYear']."-04-01";
  	$nextYear          = $_REQUEST['startYear'] + 1;
  	$academicEndYear   = $nextYear."-03-31";
  	$academicStartYearSelected = $_REQUEST['startYear'];
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y')."-04-01";
	  	$nextYear          = date('Y') + 1;
	  	$academicEndYear   = $nextYear."-03-31";
	  	$academicStartYearSelected = date('Y');
		}
		else
		{
			$prevYear          = date('Y') - 1;
			$academicStartYear = $prevYear."-04-01";
	  	$academicEndYear   = date('Y')."-03-31";
	  	$academicStartYearSelected = $prevYear;
		}
	}
	
	$class   = isset($_REQUEST['class']) ? $_REQUEST['class'] : 0;
	$section = isset($_REQUEST['classSection']) ? $_REQUEST['classSection'] : 0;
	
	$selectId = "SELECT employeeMasterId
                 FROM employeemaster
                WHERE loginId = '".$_SESSION['s_activName']."'";
  $selectIdRes = mysql_query($selectId);
  if($idRow = mysql_fetch_array($selectIdRes))
  {
  	$selectClass = "SELECT class,section
	                    FROM classteacherallotment
	                   WHERE employeeMasterId = '".$idRow['employeeMasterId']."'
	                     AND academicStartYear = '".$academicStartYear."'
	                     AND academicEndYear = '".$academicEndYear."'";
	  $selectClassRes = mysql_query($selectClass);
	  if($classRow = mysql_fetch_array($selectClassRes))
	  {
	  	if($class == 0)
	  	{
		  	$class   = $classRow['class'];
		  	$section = $classRow['section'];
		  }
	  }
	  $i = 0;
	  $selectNominal = "SELECT nominalRollId,nominalroll.grNo,academicStartYear,academicEndYear,nominalroll.class,
	                           nominalroll.section,rollNo,feeGroup,libraryMemberType,libraryMemberType,studentmaster.studentName,
	                           busRoute,busStop,subjectGroup,coScholasticGroup,clubTerm1,clubTerm2,
	                           boardRegistrionId,boardRollNo,activated,studentmaster.studentLoginId,studentmaster.parentLoginId,
	                           nominalroll.optionSubject1,nominalroll.optionSubject2,nominalroll.3a1,nominalroll.3a2,nominalroll.3b1,nominalroll.3b2
	                      FROM nominalroll
	                 LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
	                     WHERE studentmaster.activated = 'Y'
	                       AND nominalroll.class = '".$class."'
	                       AND nominalroll.section = '".$section."'
	                       AND academicStartYear = '".$academicStartYear."'
	                       AND academicEndYear = '".$academicEndYear."'
	                  ORDER BY nominalroll.rollNo";
	  $selectNominalRes = mysql_query($selectNominal);
	  while($nominalRow = mysql_fetch_array($selectNominalRes))
	  {
	    $nominalArr[$i]['nominalRollId']      = $nominalRow['nominalRollId'];
	    $nominalArr[$i]['grNo']               = $nominalRow['grNo'];
	    $nominalArr[$i]['academicYear']       = substr($nominalRow['academicStartYear'],0,4).'-'.substr($nominalRow['academicEndYear'],0,4);
	    $nominalArr[$i]['class']              = $nominalRow['class'];
	    $nominalArr[$i]['section']            = $nominalRow['section'];
	    $nominalArr[$i]['rollNo']             = $nominalRow['rollNo'];
	    $nominalArr[$i]['studentName']        = $nominalRow['studentName'];
	    $nominalArr[$i]['activated']          = $nominalRow['activated'];
	    $nominalArr[$i]['optionSubject1']     = $nominalRow['optionSubject1'];
	    $nominalArr[$i]['optionSubject2']     = $nominalRow['optionSubject2'];
	    $nominalArr[$i]['3a1']     = $nominalRow['3a1'];
	    $nominalArr[$i]['3a2']     = $nominalRow['3a2'];
	    $nominalArr[$i]['3b1']     = $nominalRow['3b1'];
	    $nominalArr[$i]['3b2']     = $nominalRow['3b2'];
	    $i++;
	  }
	}
  
  if($class == '6')
  {
    $classNew = $class;
  }
  if($class == '7')
  {
    $classNew = $class - 1;
  }
  if($class == '8')
  {
    $classNew = $class - 2;
  }
  if($class == '11 SCI.' || $class == '11 COM.')
  {
    $classNew = 11;
  }
  if($class == '12 SCI.' || $class == '12 COM.')
  {
    $classNew = 11;
  }
  $clubArr  = array();
  $clubArr2 = array();
  $clubArr2u = array();
	$k = 0;
	$selectClub1 = "SELECT subjectMasterId,subjectName,subjectType
	                  FROM subjectmaster
	                 WHERE subjectType = 'optional'
	                   AND startClass = '".$classNew."'";
	$selectClub1Res = mysql_query($selectClub1);
	while($club1Row = mysql_fetch_array($selectClub1Res))
	{
		$clubArr['subjectMasterId'][$k]   = $club1Row['subjectMasterId'];
		$clubArr['subjectName'][$k]       = $club1Row['subjectName'];
		$k++;
	}
	
	$t = 0;
	$selectClub2 = "SELECT subjectMasterId,subjectName,subjectType,subjectDescription
	                  FROM subjectmaster
	                 WHERE subjectDescription = '3A'";
	$selectClub2Res = mysql_query($selectClub2);
	while($club2Row = mysql_fetch_array($selectClub2Res))
	{
		$clubArr2['subjectMasterIdCo'][$t]   = $club2Row['subjectMasterId'];
		$clubArr2['subjectNameCo'][$t]       = $club2Row['subjectName'];
		$t++;
	}
	
	$u = 0;
	$selectClub2u = "SELECT subjectMasterId,subjectName,subjectType,subjectDescription
	                   FROM subjectmaster
	                  WHERE subjectDescription = '3B'";
	$selectClub2uRes = mysql_query($selectClub2u);
	while($club2uRow = mysql_fetch_array($selectClub2uRes))
	{
		$clubArr2u['subjectMasterIdCo'][$u]   = $club2uRow['subjectMasterId'];
		$clubArr2u['subjectNameCo'][$u]       = $club2uRow['subjectName'];
		$u++;
	}
	
	
	if(isset($_POST['submit']))
	{
		$loopCount1 = 0;
	  while($loopCount1 < count($_POST['nominalRollId']))
	  {
	    $nominalRollId  = isset($_POST['nominalRollId'][$loopCount1]) && $_POST['nominalRollId'][$loopCount1] != '' ? $_POST['nominalRollId'][$loopCount1] : 0;
	    $optionSubject1 = isset($_POST['optionSubject1'][$loopCount1]) && $_POST['optionSubject1'][$loopCount1] != '' ? $_POST['optionSubject1'][$loopCount1] : 0;
		  $optionSubject2 = isset($_POST['optionSubject2'][$loopCount1]) && $_POST['optionSubject2'][$loopCount1] != '' ? $_POST['optionSubject2'][$loopCount1] : 0;
		  $option3a1            = isset($_POST['3a1'][$loopCount1]) && $_POST['3a1'][$loopCount1] != '' ? $_POST['3a1'][$loopCount1] : 0;
		  $option3a2            = isset($_POST['3a2'][$loopCount1]) && $_POST['3a2'][$loopCount1] != '' ? $_POST['3a2'][$loopCount1] : 0;
		  $option3b1            = isset($_POST['3b1'][$loopCount1]) && $_POST['3b1'][$loopCount1] != '' ? $_POST['3b1'][$loopCount1] : 0;
		  $option3b2            = isset($_POST['3b2'][$loopCount1]) && $_POST['3b2'][$loopCount1] != '' ? $_POST['3b2'][$loopCount1] : 0;
		  
	    if($optionSubject1 > 0 || $optionSubject2 > 0 || $option3a1 > 0 || $option3a2 > 0 || $option3b1 > 0 || $option3b2 > 0)
	    {
	      $nominalEntry = "UPDATE nominalroll 
			  										SET optionSubject1 = ".$optionSubject1.",
			  											  optionSubject2 = ".$optionSubject2.",
			  											  3a1            = ".$option3a1.",
			  											  3a2            = ".$option3a2.",
			  											  3b1            = ".$option3b1.",
			  											  3b2            = ".$option3b2."
											  	WHERE nominalRollId  = ".$nominalRollId."";
			  $nominalEntryRes = om_query($nominalEntry);
			  if(!$nominalEntryRes)
			  {
			  	echo "Update Fail";
			  }
			  else
			  {
			    header("Location:nominalRollListEmployeeOption.php?done=1");
			  }
	    }
	    $loopCount1++;
	  }
  }
  
  $c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster
                   WHERE priority > 8";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  $secArrOut[0] = 'A';
  $secArrOut[1] = 'B';
  $secArrOut[2] = 'C';
  $secArrOut[3] = 'D';
  include("./bottom.php");
  $smarty->assign('class',$class);
  $smarty->assign('section',$section);
  $smarty->assign('cArray',$cArray);
  $smarty->assign('secArrOut',$secArrOut);
  $smarty->assign('clubArr',$clubArr);
  $smarty->assign('clubArr2',$clubArr2);
  $smarty->assign('clubArr2u',$clubArr2u);
  $smarty->assign('nominalArr',$nominalArr);
  $smarty->assign('academicStartYear',$academicStartYear);
  $smarty->assign('academicEndYear',$academicEndYear);
  $smarty->assign('academicStartYearSelected',$academicStartYearSelected);
  $smarty->display('nominalRollListEmployeeOption.tpl');  
}
?>
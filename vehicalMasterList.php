<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$classArray = array();
  $i = 0;
  $selectClass = "SELECT vehiclemaster.vehicleMasterId,vehiclemaster.vehicleTypeId,vehiclemaster.vehicleNo,
                         vehiclemaster.registrationNo,vehiclemaster.capicity,vehicletype.vehicleType
                    FROM vehiclemaster
               LEFT JOIN vehicletype ON vehicletype.vehicleTypeId = vehiclemaster.vehicleTypeId";
  $selectClassRes = mysql_query($selectClass);
  while($classRow = mysql_fetch_array($selectClassRes))
  {
  	$classArray[$i]['vehicleMasterId'] = $classRow['vehicleMasterId'];
  	$classArray[$i]['vehicleType']     = $classRow['vehicleType'];
  	$classArray[$i]['vehicleNo']       = $classRow['vehicleNo'];
  	$classArray[$i]['registrationNo']  = $classRow['registrationNo'];
  	$classArray[$i]['capicity']        = $classRow['capicity'];
  	$i++;
  }
  
  include("./bottom.php");
  $smarty->assign('classArray',$classArray);
  $smarty->display('vehicalMasterList.tpl');  
}
?>
<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$className     = "";
	$classMasterId = "";
	$classSection  = "";
	$isEdit        = 0;
	$classArray    = array();
	$UpdateArray   = array();
  if(isset($_POST['Submit']))
  {
  	$className        = isset($_POST['className']) ? $_POST['className'] : "";
  	$classSection     = isset($_POST['classSection']) ? $_POST['classSection'] : "";
  	$classMasterId    = isset($_POST['classMasterId']) ? $_POST['classMasterId'] : 0;
  	if($classMasterId == 0)
  	{
  	  $insertClass = "INSERT INTO classmaster (className,classSection)
  	                  VALUES('".$className."','".$classSection."')";
  	  $insertClassRes = om_query($insertClass);
  	  if(!$insertClassRes)
  	  {
  	  	echo "Insert Fail";
  	  }
  	  else
  	  {
  	  	header("Location:classMaster.php?done=1");
  	  }
  	}
  	else
  	{
  	  $isEdit = 1;
  	  $updateClass = "UPDATE classmaster
  	                     SET className = '".$className."',
  	                         classSection = '".$classSection."'
  	                     WHERE classMasterId = ".$_REQUEST['classMasterId'];
      $updateClassRes = om_query($updateClass);
      if(!$updateClassRes)
      {
      	echo "Update Fail";
      }
      else
      {
      	header("Location:classMaster.php?done=1");
      }
  	}
  }
  $i = 0;
  $selectClass = "SELECT classMasterId,className,classSection
                    FROM classmaster
                ORDER BY priority";
  $selectClassRes = mysql_query($selectClass);
  while($classRow = mysql_fetch_array($selectClassRes))
  {
  	$classArray[$i]['classMasterId'] = $classRow['classMasterId'];
  	$classArray[$i]['className']     = $classRow['className'];
  	$classArray[$i]['classSection']       = $classRow['classSection'];
  	$i++;
  }
  if(isset($_REQUEST['classMasterId']) > 0)
  {
    $selectClass = "SELECT classMasterId,className,classSection
                      FROM classmaster
                    WHERE  classMasterId = ".$_REQUEST['classMasterId'];
    $selectClassRes = mysql_query($selectClass);
    if($classRow = mysql_fetch_array($selectClassRes))
    {
    	$classMasterId    = $classRow['classMasterId'];
    	$className        = $classRow['className'];
    	$classSection     = $classRow['classSection'];
    }
  }
  include("./bottom.php");
  $smarty->assign('UpdateArray',$UpdateArray);
  $smarty->assign('className',$className);
  $smarty->assign('classMasterId',$classMasterId);
  $smarty->assign('isEdit',$isEdit);
  $smarty->assign('classSection',$classSection);
  $smarty->assign('classArray',$classArray);
  $smarty->display('classMaster.tpl');  
}
?>
<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$countTotal = '';
	$attendenceArr = array();
  $i = 0;
  $selectAttendM = "SELECT houseId,houseName
                      FROM house";
  $selectAttendMRes = mysql_query($selectAttendM);
  while($maRow = mysql_fetch_array($selectAttendMRes))
  {
  	$attendenceArr[$i]['houseId']   = $maRow['houseId'];
  	$attendenceArr[$i]['houseName'] = $maRow['houseName'];
  	
  	$selectAtnP = "SELECT gender
                     FROM studentmaster
                    WHERE houseId = '".$maRow['houseId']."'
                      AND gender = 'Male'
                      AND activated = 'Y'";
    $selectAtnPRes = mysql_query($selectAtnP);
    $countP = mysql_num_rows($selectAtnPRes);
    $attendenceArr[$i]['countP'] = $countP;
    
    $selectAtnA = "SELECT gender
                     FROM studentmaster
                    WHERE houseId = '".$maRow['houseId']."'
                      AND gender = 'Female'
                      AND activated = 'Y'";
    $selectAtnARes = mysql_query($selectAtnA);
    $countA = mysql_num_rows($selectAtnARes);
    $attendenceArr[$i]['countA'] = $countA;
    $attendenceArr[$i]['totalNumberWorkingDay'] = $countA + $countP;
    
    $countTotal += $attendenceArr[$i]['totalNumberWorkingDay'];
  	$i++;
  }
  
  include("./bottom.php");
  $smarty->assign('attendenceArr',$attendenceArr);
  $smarty->assign('countTotal',$countTotal);
  $smarty->display('houseReport.tpl');
}
?>
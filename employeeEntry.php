<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$employeeMasterId   = isset($_REQUEST['employeeMasterId']) ? $_REQUEST['employeeMasterId'] : 0;
	$name               = '';
	$dateOfBirth        = '';
	$joiningDate        = '';
	$apointmentDate     = '';
	$confirmationDate   = '';
	$gender             = '';
	$activated          = '';
	$retireDate         = '';
	$currentAddress     = '';
	$currentAddressPin  = '';
	$residenceTelephone = '';
	$permanentAddress   = '';
	$correspondenceAddress = '';
	$mothersName        = '';
	$mobile             = '';
	$phone1             = '';
	$phone2             = '';
	$email              = '';
	$marital            = '';
	$spouseName         = '';
	$spousePhone        = '';
	$loginId            = '';
	$password           = '';
	$pfNo               = '';
	$category           = '';
	$designation        = '';
	$accountNo          = '';
	$panNo              = '';
	$qualification      = '';
	$experience         = '';
	$bloodGroup         = '';
	$userType           = '';
	$fathersName        = '';
	
	$empImage           = '';
	
	if(isset($_POST['submit']))
	{
	  $userType            = isset($_REQUEST['userType']) && $_REQUEST['userType'] != '' ? $_REQUEST['userType'] : 'Teacher';
	  $name                = isset($_REQUEST['name']) && $_REQUEST['name'] != '' ? $_REQUEST['name'] : '';
	  $fathersName         = isset($_REQUEST['fathersName']) && $_REQUEST['fathersName'] != '' ? $_REQUEST['fathersName'] : "";
	  $dateOfBirth         = $_POST['dobYear']."-".$_POST['dobMonth']."-".$_POST['dobDay'];
	  $gender              = isset($_REQUEST['gender']) && $_REQUEST['gender'] != '' ? $_REQUEST['gender'] : "";
	  $joiningDate         = $_POST['joiningYear']."-".$_POST['joiningMonth']."-".$_POST['joiningDay'];
	  $apointmentDate      = $_POST['apointmentDateYear']."-".$_POST['apointmentDateMonth']."-".$_POST['apointmentDateDay'];
	  $confirmationDate    = $_POST['confirmationDateYear']."-".$_POST['confirmationDateMonth']."-".$_POST['confirmationDateDay'];
	  
	  $loginId             = isset($_REQUEST['loginId']) && $_REQUEST['loginId'] != '' ? $_REQUEST['loginId'] : '';
	  $password            = isset($_REQUEST['password']) && $_REQUEST['password'] != '' ? $_REQUEST['password'] : 'dps1234';
	  
	  $activated           = isset($_REQUEST['activated']) && $_REQUEST['activated'] != '' ? $_REQUEST['activated'] : "Yes";
	  
	  $currentAddress      = isset($_REQUEST['currentAddress']) && $_REQUEST['currentAddress'] != '' ? $_REQUEST['currentAddress'] : "";
	  $currentAddressPin   = isset($_REQUEST['currentAddressPin']) && $_REQUEST['currentAddressPin'] != '' ? $_REQUEST['currentAddressPin'] : "";
	  
	  $correspondenceAddress = isset($_REQUEST['correspondenceAddress']) && $_REQUEST['correspondenceAddress'] != '' ? $_REQUEST['correspondenceAddress'] : "";
	  $permanentAddress      = isset($_REQUEST['permanentAddress']) && $_REQUEST['permanentAddress'] != '' ? $_REQUEST['permanentAddress'] : "";
	  
	  $residenceTelephone  = isset($_REQUEST['residenceTelephone']) && $_REQUEST['residenceTelephone'] != '' ? $_REQUEST['residenceTelephone'] : "";
	  $mobile              = isset($_REQUEST['mobile']) && $_REQUEST['mobile'] != '' ? $_REQUEST['mobile'] : "";
	  $phone1              = isset($_REQUEST['phone1']) && $_REQUEST['phone1'] != '' ? $_REQUEST['phone1'] : "";
	  $phone2              = isset($_REQUEST['phone2']) && $_REQUEST['phone2'] != '' ? $_REQUEST['phone2'] : "";
	  
	  $email               = isset($_REQUEST['email']) && $_REQUEST['email'] != '' ? $_REQUEST['email'] : "";
	  $mothersName         = isset($_REQUEST['mothersName']) && $_REQUEST['mothersName'] != '' ? $_REQUEST['mothersName'] : "";
	  $marital             = isset($_REQUEST['marital']) && $_REQUEST['marital'] != '' ? $_REQUEST['marital'] : "";
	  $spouseName          = isset($_REQUEST['spouseName']) && $_REQUEST['spouseName'] != '' ? $_REQUEST['spouseName'] : "";
	  
	  $spousePhone         = isset($_REQUEST['spousePhone']) && $_REQUEST['spousePhone'] != '' ? $_REQUEST['spousePhone'] : "";
	  $pfNo                = isset($_REQUEST['pfNo']) && $_REQUEST['pfNo'] != '' ? $_REQUEST['pfNo'] : "";
	  
	  $accountNo           = isset($_REQUEST['accountNo']) && $_REQUEST['accountNo'] != '' ? $_REQUEST['accountNo'] : "";
	  $panNo               = isset($_REQUEST['panNo']) && $_REQUEST['panNo'] != '' ? $_REQUEST['panNo'] : "";
	  $qualification       = isset($_REQUEST['qualification']) && $_REQUEST['qualification'] != '' ? $_REQUEST['qualification'] : "";
	  $category            = isset($_REQUEST['category']) && $_REQUEST['category'] != '' ? $_REQUEST['category'] : "";
	  $designation         = isset($_REQUEST['designation']) && $_REQUEST['designation'] != '' ? $_REQUEST['designation'] : "";
	  $bloodGroup          = isset($_REQUEST['bloodGroup']) && $_REQUEST['bloodGroup'] != '' ? $_REQUEST['bloodGroup'] : "";

	  $createdOn           = date('Y-m-d');
	  if(isset($_POST['employeeMasterId']) && $_POST['employeeMasterId'] == 0)
    {
		  $employeeEntry = "INSERT INTO employeemaster 
		                                (userType,name,fathersName,dateOfBirth,gender,joiningDate,apointmentDate,confirmationDate,loginId,password,activated,
		                                 currentAddress,currentAddressPin,correspondenceAddress,permanentAddress,residenceTelephone,
		                                 mobile,phone1,phone2,email,mothersName,marital,spouseName,spousePhone,pfNo,accountNo,panNo,
		                                 createdOn,qualification,category,designation,bloodGroup)
		                         VALUES ('".$userType."','".$name."','".$fathersName."','".$dateOfBirth."',
		                                 '".$gender."','".$joiningDate."','".$apointmentDate."','".$confirmationDate."','".$loginId."','".$password."',
		                                 '".$activated."','".$currentAddress."','".$currentAddressPin."','".$correspondenceAddress."',
		                                 '".$permanentAddress."','".$residenceTelephone."','".$mobile."','".$phone1."',
		                                 '".$phone2."','".$email."','".$mothersName."','".$marital."','".$spouseName."','".$spousePhone."',
		                                 '".$pfNo."','".$accountNo."','".$panNo."','".$createdOn."','".$qualification."','".$category."',
		                                 '".$designation."','".$bloodGroup."')";
		  $employeeEntryRes = om_query($employeeEntry);
		  if(isset($_FILES["imageEmployee"]["name"]))
	   	{
				$filename1  =  pathinfo($_FILES["imageEmployee"]["name"]);
				
				$empName = explode(" ",$name);
				$empName[0];
				$empName[1];
				$empNameImage = implode("_", $empName);
				
				$imageP = $empNameImage.".jpg";
				$fileP = "employeeImage/".$imageP; 
				if (move_uploaded_file($_FILES['imageEmployee']['tmp_name'], $fileP))
				{
					  @copy("employeeImage/".$imageP);
				} 
				else 
				{
					echo "Image error";
				}
	  	}
		  if(!$employeeEntryRes)
		  {
		    echo "Employee Entry Fail";
		  }
		  else
		  {
		    header("Location:employeeEntry.php?done=1");
		  }
		}
	  
	  if(isset($_POST['employeeMasterId']) && $_POST['employeeMasterId'] > 0)
	  {
	    $updateEntry = "UPDATE employeemaster 
                         SET userType            = '".$userType."',                                                         
	  							  				  name                 = '".$name."',                             
	  							  				  fathersName             = '".$fathersName."',                       
	  							  				  dateOfBirth       = '".$dateOfBirth."',                 
	  							  				  gender      = '".$gender."',                
	  							  				  joiningDate        = '".$joiningDate."',                  
	  							  				  apointmentDate = '".$apointmentDate."',                  
	  							  				  confirmationDate = '".$confirmationDate."',                  
	  							  				  activated       = '".$activated."',                 
	  							  				  currentAddress            = '".$currentAddress."',                      
	  							  				  currentAddressPin        = '".$currentAddressPin."',                  
	  							  				  correspondenceAddress          = '".$correspondenceAddress."',                    
	  							  				  permanentAddress          = '".$permanentAddress."',                    
	  							  				  residenceTelephone          = '".$residenceTelephone."',                    
	  							  				  mobile               = '".$mobile."',                         
	  							  				  phone1           = '".$phone1."',                     
	  							  				  phone2              =  '".$phone2."',                      
	  							  				  email       = '".$email."',     
	  							  				  mothersName         = '".$mothersName."',                   
	  							  				  marital    = '".$marital."',  
	  							  				  spouseName      = '".$spouseName."',                
	  							  				  spousePhone      = '".$spousePhone."',                
	  							  				  pfNo            = '".$pfNo."',                      
	  							  				  accountNo         = '".$accountNo."',                   
	  							  				  panNo           = '".$panNo."',                     
	  							  				  qualification          = '".$qualification."',                    
	  							  				  category          = '".$category."',                    
	  							  				  designation         = '".$designation."',                   
	  							  				  bloodGroup     = '".$bloodGroup."'
									  WHERE employeeMasterId      = ".$_POST['employeeMasterId'];
	    $updateEntryRes = om_query($updateEntry);
	    if(isset($_FILES["imageEmployee"]["name"]))
	   	{
				$filename1  =  pathinfo($_FILES["imageEmployee"]["name"]);
				
				$empName = explode(" ",$name);
				$empName[0];
				$empName[1];
				$empNameImage = implode("_", $empName);
				
				$imageP = $empNameImage.".jpg";
				$fileP = "employeeImage/".$imageP; 
				if (move_uploaded_file($_FILES['imageEmployee']['tmp_name'], $fileP))
				{
					  @copy("employeeImage/".$imageP);
				} 
				else 
				{
					echo "Image error";
				}
	  	}
	    if(!$updateEntryRes)
	    {
	    	echo "Update Fail";
	    }
	    else
	    {
		    header("Location:employeAllList.php");
	    }
	  }
	}
  
  if($employeeMasterId > 0)
	{
	  $selectEmp = "SELECT employeeMasterId,userType,name,fathersName,dateOfBirth,joiningDate,apointmentDate,confirmationDate,gender,currentAddress,currentAddressPin,residenceTelephone,
	                       correspondenceAddress,permanentAddress,mobile,phone1,phone2,email,marital,accountNo,panNo,qualification,experience,bloodGroup,
	                       mothersName,activated,retireDate,spouseName,spousePhone,pfNo,category,designation
	                  FROM employeemaster
	                 WHERE employeeMasterId = ".$employeeMasterId."";
	  $selectEmpRes = mysql_query($selectEmp);
	  while($empRow = mysql_fetch_array($selectEmpRes))
	  {
	  	$employeeMasterId   = $empRow['employeeMasterId'];
	  	$userType           = $empRow['userType'];
	  	$name               = $empRow['name'];
	  	$fathersName        = $empRow['fathersName'];
	  	$dateOfBirth        = $empRow['dateOfBirth'];
	  	$joiningDate        = $empRow['joiningDate'];
	  	$apointmentDate     = $empRow['apointmentDate'];
	  	$confirmationDate   = $empRow['confirmationDate'];
	  	$gender             = $empRow['gender'];
	  	$activated          = $empRow['activated'];
	  	$retireDate         = $empRow['retireDate'];
	  	$currentAddress     = $empRow['currentAddress'];
	  	$currentAddressPin  = $empRow['currentAddressPin'];
	  	$residenceTelephone = $empRow['residenceTelephone'];
	  	$permanentAddress   = $empRow['permanentAddress'];
	  	$correspondenceAddress = $empRow['correspondenceAddress'];
	  	$mothersName        = $empRow['mothersName'];
	  	$mobile             = $empRow['mobile'];
	  	$phone1             = $empRow['phone1'];
	  	$phone2             = $empRow['phone2'];
	  	$email              = $empRow['email'];
	  	$marital            = $empRow['marital'];
	  	$spouseName         = $empRow['spouseName'];
	  	$spousePhone        = $empRow['spousePhone'];
	  	$pfNo               = $empRow['pfNo'];
	  	$category           = $empRow['category'];
	  	$designation        = $empRow['designation'];
	  	$accountNo          = $empRow['accountNo'];
	  	$panNo              = $empRow['panNo'];
	  	$qualification      = $empRow['qualification'];
	  	$experience         = $empRow['experience'];
	  	$bloodGroup         = $empRow['bloodGroup'];
	  	
	  	$empName = explode(" ",$empRow['name']);
			$empName[0];
			$empName[1];
			$empNameImage = implode("_", $empName);
			
	  	$empImage = $empNameImage.'.jpg';
	  }
	}
	
  $secArrOut[0] = 'YES';
  $secArrValue[0] = 'YES';
  $secArrOut[1] = 'NO';
  $secArrValue[1] = 'NO';  
  
  $maleValue[0] = 'Male';
  $maleValue[1] = 'Female';  
  
  $typeOutPut[0] = 'Teacher';
  $typeOutPut[1] = 'Admin Staff';
  $typeOutPut[2] = 'Administrator';
  $typeOutPut[3] = 'Principal';
  $typeOutPut[4] = 'Transport';
  $typeOutPut[5] = 'Class IV';
  
  include("./bottom.php");
  $smarty->assign("secArrOut",$secArrOut);
  $smarty->assign("secArrValue",$secArrValue);
  $smarty->assign("maleValue",$maleValue);
  $smarty->assign("typeOutPut",$typeOutPut);
  $smarty->assign("userType",$userType);
  $smarty->assign("name",$name);
  $smarty->assign("fathersName",$fathersName);
  $smarty->assign("dateOfBirth",$dateOfBirth);
  $smarty->assign("joiningDate",$joiningDate);
  $smarty->assign("apointmentDate",$apointmentDate);
  $smarty->assign("confirmationDate",$confirmationDate);
  $smarty->assign("gender",$gender);
  $smarty->assign("activated",$activated);
  $smarty->assign("retireDate",$retireDate);
  $smarty->assign("currentAddress",$currentAddress);
  $smarty->assign("currentAddressPin",$currentAddressPin);
  $smarty->assign("residenceTelephone",$residenceTelephone);
  $smarty->assign("permanentAddress",$permanentAddress);
  $smarty->assign("correspondenceAddress",$correspondenceAddress);
  $smarty->assign("mothersName",$mothersName);
  $smarty->assign("mobile",$mobile);
  $smarty->assign("phone1",$phone1);
  $smarty->assign("phone2",$phone2);
  $smarty->assign("email",$email);
  $smarty->assign("marital",$marital);
  $smarty->assign("spouseName",$spouseName);
  $smarty->assign("spousePhone",$spousePhone);
  $smarty->assign("pfNo",$pfNo);
  $smarty->assign("category",$category);
  $smarty->assign("designation",$designation);
  $smarty->assign("panNo",$panNo);
  $smarty->assign("accountNo",$accountNo);
  $smarty->assign("qualification",$qualification);
  $smarty->assign("experience",$experience);
  $smarty->assign("bloodGroup",$bloodGroup);
  $smarty->assign("empImage",$empImage);
  $smarty->assign("employeeMasterId",$employeeMasterId);
  $smarty->assign("loginId",$loginId);
  $smarty->assign("password",$password);
  $smarty->display("employeeEntry.tpl");
}
?>
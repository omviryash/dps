<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$class      = '';
        $count      = 0;
        $today      = date('Y-m-d');
	$stdArray   = array();
	$section    = '';
	$examMarksPerTryId = 0;
	$reportDate1 = '';
	$reportDate2 = '';
	
	if(isset($_REQUEST['startYear']))
  {
  	$academicStartYear = $_REQUEST['startYear']."-04-01";
  	$nextYear          = $_REQUEST['startYear'] + 1;
  	$academicEndYear   = $nextYear."-03-31";
  	$academicStartYearSelected = $_REQUEST['startYear'];
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y')."-04-01";
	  	$nextYear          = date('Y') + 1;
	  	$academicEndYear   = $nextYear."-03-31";
	  	$academicStartYearSelected = date('Y');
		}
		else
		{
			$prevYear          = date('Y') - 1;
			$academicStartYear = $prevYear."-04-01";
	  	$academicEndYear   = date('Y')."-03-31";
	  	$academicStartYearSelected = $prevYear;
		}
	}
	
	$class   = isset($_GET['class']) && $_GET['class'] != '' ? $_GET['class'] : 0;
	$section = isset($_GET['section']) && $_GET['section'] != '' ? $_GET['section'] : 0;
	
	$selectId = "SELECT employeeMasterId
                 FROM employeemaster
                WHERE loginId = '".$_SESSION['s_activName']."'";
  $selectIdRes = mysql_query($selectId);
  if($idRow = mysql_fetch_array($selectIdRes))
  {
  	$selectClass = "SELECT class,section
	                    FROM classteacherallotment
	                   WHERE employeeMasterId = '".$idRow['employeeMasterId']."'
	                     AND academicStartYear = '".$academicStartYear."'
                       AND academicEndYear = '".$academicEndYear."'";
	  $selectClassRes = mysql_query($selectClass);
	  if($classRow = mysql_fetch_array($selectClassRes))
	  {
	  	if($class == 0)
	  	{
		  	$class   = $classRow['class'];
		  	$section = $classRow['section'];
		  }
		}
		
		$dtlArr = array();
		$g = 0;
	  $selectStdIn = "SELECT exammarkspertry.examMarksPerTryId,exammarkspertry.grNo,studentmaster.gender,studentmaster.activated,
	                         exammarkspertry.academicStartYear,exammarkspertry.academicEndYear,studentmaster.studentName,exammarkspertry.goals,
	                         exammarkspertry.strengths,exammarkspertry.intHobbies,exammarkspertry.responsibilities,
	                         exammarkspertry.visionL,exammarkspertry.visionR,exammarkspertry.reportDate1,exammarkspertry.reportDate2,
	                         exammarkspertry.height,exammarkspertry.weight,exammarkspertry.dentalHygiene,
	                         exammarkspertry.resultNote1,exammarkspertry.resultNote2
	                    FROM exammarkspertry
	               LEFT JOIN studentmaster ON studentmaster.grNo = exammarkspertry.grNo
	                   WHERE exammarkspertry.class = '".$class."'
	                     AND exammarkspertry.section = '".$section."'
	                     AND exammarkspertry.academicStartYear = '".$academicStartYear."'
                       AND exammarkspertry.academicEndYear = '".$academicEndYear."'
                       AND studentmaster.activated = 'Y'";
	  $selectStdInRes = mysql_query($selectStdIn);
	  $count = mysql_num_rows($selectStdInRes);
	  while($stdInRow = mysql_fetch_array($selectStdInRes))
	  {
	  	
	  	$selectRoll = "SELECT rollNo
	                     FROM nominalroll
	                    WHERE grNo = '".$stdInRow['grNo']."'";
	    $selectRollRes = mysql_query($selectRoll);
	    while($rollRow = mysql_fetch_array($selectRollRes))
	    {
	    	$dtlArr[$g]['rollNo'] = $rollRow['rollNo'];
	    }
	    
	  	$dtlArr[$g]['examMarksPerTryId'] = $stdInRow['examMarksPerTryId'];
	  	//$dtlArr[$g]['rollNo']            = $stdInRow['rollNo'];
	  	$dtlArr[$g]['grNo']          = $stdInRow['grNo'];
	  	$dtlArr[$g]['studentName']   = $stdInRow['studentName'];
	  	
	  	$dtlArr[$g]['goals']             = $stdInRow['goals'];
	  	$dtlArr[$g]['strengths']         = $stdInRow['strengths'];
	  	$dtlArr[$g]['intHobbies']        = $stdInRow['intHobbies'];
	  	$dtlArr[$g]['responsibilities']  = $stdInRow['responsibilities'];

	  	$dtlArr[$g]['height']            = $stdInRow['height'];
	  	$dtlArr[$g]['weight']            = $stdInRow['weight'];
	  	
	  	$dtlArr[$g]['visionL']           = $stdInRow['visionL'];
	  	$dtlArr[$g]['visionR']           = $stdInRow['visionR'];
	  	
	  	if($stdInRow['reportDate1'] == '')
	  	{
	  	  $reportDate1 = date('Y-m-d');
	  	}
	  	else
	  	{
	  		$reportDate1 = $stdInRow['reportDate1'];
	  	}
	  	
	  	if($stdInRow['reportDate2'] == '')
	  	{
	  	  $reportDate2 = date('Y-m-d');
	  	}
	  	else
	  	{
	  		$reportDate2 = $stdInRow['reportDate2'];
	  	}
	  	
	  	$dtlArr[$g]['dentalHygiene']     = $stdInRow['dentalHygiene'];
	  	$dtlArr[$g]['resultNote1']       = $stdInRow['resultNote1'];
	  	$dtlArr[$g]['resultNote2']       = $stdInRow['resultNote2'];
	  	
	  	$g++;
	  }
  }
  
  if(isset($_POST['submitTaken']))
  {
  	$loopCount = 0;
  	while($loopCount < count($_POST['examMarksPerTryId']))
  	{
  		$examMarksPerTryId = ($_POST['examMarksPerTryId'][$loopCount] != '') ? $_POST['examMarksPerTryId'][$loopCount] : 0;
  		$goals            = ($_POST['goals'][$loopCount] != '') ? $_POST['goals'][$loopCount] : '';
  		$strengths        = ($_POST['strengths'][$loopCount] != '') ? $_POST['strengths'][$loopCount] : '';
  		$intHobbies       = ($_POST['intHobbies'][$loopCount] != '') ? $_POST['intHobbies'][$loopCount] : '';
  		$responsibilities = ($_POST['responsibilities'][$loopCount] != '') ? $_POST['responsibilities'][$loopCount] : '';
  		$height           = ($_POST['height'][$loopCount] != '') ? $_POST['height'][$loopCount] : 0;
  		$weight           = ($_POST['weight'][$loopCount] != '') ? $_POST['weight'][$loopCount] : 0;
  		$dentalHygiene    = ($_POST['dentalHygiene'][$loopCount] != '') ? $_POST['dentalHygiene'][$loopCount] : '';
  		$resultNote1      = ($_POST['resultNote1'][$loopCount] != '') ? $_POST['resultNote1'][$loopCount] : '';
  		$resultNote2      = ($_POST['resultNote2'][$loopCount] != '') ? $_POST['resultNote2'][$loopCount] : '';
  		$visionL          = ($_POST['visionL'][$loopCount] != '') ? $_POST['visionL'][$loopCount] : '';
  		$visionR          = ($_POST['visionR'][$loopCount] != '') ? $_POST['visionR'][$loopCount] : '';
  		$reportDate1      = $_POST['reportDate1Year'].'-'.$_POST['reportDate1Month'].'-'.$_POST['reportDate1Day'];
  		$reportDate2      = $_POST['reportDate2Year'].'-'.$_POST['reportDate2Month'].'-'.$_POST['reportDate2Day'];
  		
  		if($_POST['examMarksPerTryId'][$loopCount] != '' && $_POST['examMarksPerTryId'][$loopCount] > 0)
  		{
				$updateClass = "UPDATE exammarkspertry
		  	                   SET goals = '".$goals."',
		  	                       strengths = '".$strengths."',
		  	                       intHobbies = '".$intHobbies."',
		  	                       responsibilities = '".$responsibilities."',
		  	                       height = ".$height.",
		  	                       weight = ".$weight.",
		  	                       dentalHygiene = '".$dentalHygiene."',
		  	                       resultNote1 = '".$resultNote1."',
		  	                       resultNote2 = '".$resultNote2."',
		  	                       visionL = '".$visionL."',
		  	                       visionR = '".$visionR."',
		  	                       reportDate1 = '".$reportDate1."',
		  	                       reportDate2 = '".$reportDate2."'
		  	                 WHERE examMarksPerTryId = ".$examMarksPerTryId."";
			  $updateClassRes = om_query($updateClass);
			  if(!$updateClassRes)
			  {
			  	echo "Update Fail";
			  }
			  else
			  {
			  	header("Location:generateMarksDetailEntryTry.php?startYear=".$academicStartYearSelected."&go=go");
			  }
			}
			$loopCount++;
		}
  }
  
  $i = 0;
  $selectStdIn = "SELECT exammarkspertry.examMarksPerTryId,exammarkspertry.grNo,studentmaster.gender,studentmaster.activated,
                         exammarkspertry.academicStartYear,exammarkspertry.academicEndYear,studentmaster.studentName
                    FROM exammarkspertry
               LEFT JOIN studentmaster ON studentmaster.grNo = exammarkspertry.grNo
                   WHERE exammarkspertry.class = '".$class."'
                     AND exammarkspertry.section = '".$section."'
                     AND exammarkspertry.academicStartYear = '".$academicStartYear."'
                     AND exammarkspertry.academicEndYear = '".$academicEndYear."'
                     AND studentmaster.activated = 'Y'";
  $selectStdInRes = mysql_query($selectStdIn);
  while($stdInRow = mysql_fetch_array($selectStdInRes))
  {
  	$stdArray['examMarksPerTryId'][$i] = $stdInRow['examMarksPerTryId'];
  	$stdArray['studentName'][$i]   = $stdInRow['studentName'];
  	$i++;
  }
	  
  $c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster
                   WHERE className > 5
                     AND className <= 10";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  $secArrOut[0] = 'A';
  $secArrOut[1] = 'B';
  $secArrOut[2] = 'C';
  $secArrOut[3] = 'D';
  
  
	
  include("./bottom.php");
  $smarty->assign('academicStartYearSelected',$academicStartYearSelected);
  $smarty->assign('dtlArr',$dtlArr);
  $smarty->assign('secArrOut',$secArrOut);
  $smarty->assign('class',$class);
  $smarty->assign('section',$section);
  $smarty->assign('stdArray',$stdArray);
  $smarty->assign('count',$count);
  $smarty->assign('cArray',$cArray);
  $smarty->assign('examMarksPerTryId',$examMarksPerTryId);
  $smarty->assign('reportDate1',$reportDate1);
  $smarty->assign('reportDate2',$reportDate2);
  $smarty->display('generateMarksDetailEntryTry.tpl');  
}
?>
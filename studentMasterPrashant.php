<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$filePath = '';
	$fileName = '';
	
	$studentId           = '';
	$grNo                = '';
	$userType            = '';
	$studentLoginId      = '';
	$studentPassword     = '';
	$parentLoginId       = '';
	$parentPassword      = '';
	$activated           = '';
	$joinedInClass       = '';
	$joiningDate         = '';
	$studentName         = '';
	$dateOfBirth         = '';
	$gender              = '';
	$bloodGroup          = '';
	$houseName           = '';
	$currentAddress      = '';
	$currentState        = '';
	$currentAddressPin   = '';
	$residencePhone1     = '';
	$residencePhone2     = '';
	$smsMobile           = '';
	$studentEmail        = '';
	$fatherName          = '';
	$fatherPhone         = '';
	$fatherEmail         = '';
	$fatherMobile        = '';
	$fatherOccupation    = '';
	$fathersDesignation  = '';
	$fathersOrganization = '';
	$mothersName         = '';
	$mothersPhone        = '';
	$mothersEmailid      = '';
	$mothersMobile       = '';
	$motherOccupation    = '';
	$mothersDesignation  = '';
	$mothersOrganization = '';
	$permanentAddress    = '';
	$permanentState      = '';
	$permanentPIN        = '';
	$urban               = '';
	$religion            = '';
	$createdOn           = '';
	$previousSchool      = '';
	
	
	if(isset($_POST['submitBtn']))
	{
	  $uploaddir = dirname($_POST['filePath']);
	
	  $uploadfile = $uploaddir . basename($_FILES['fileName']['name']);
	  
	  $target_path = './uploadXls';
	  $target_path = $target_path ."/". basename( $_FILES['fileName']['name']);
	  $_FILES['fileName']['tmp_name']; // temp file
	  
	  $oldfile =  basename($_FILES['fileName']['name']);
	
	  // getting the extention
	
	  $pos = strpos($oldfile,".",0);
	  $ext = trim(substr($oldfile,$pos+1,strlen($oldfile))," ");
	  
	  if(move_uploaded_file($_FILES['fileName']['tmp_name'], $target_path)) 
	  {
	  	$row = 0;
	    $handle = fopen($target_path, "r");
	    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
	    {
	      if($row > 0)
	      {
	        $studentId           = $data[0];
					$grNo                = $data[11];
					if($grNo != "")
					{
						if($grNo <= 9)
						{
							//IFS00001
					    $grNo = "000".$grNo;
					  }
					  elseif($grNo > 9 && $grNo <= 99)
					  {
					  	//IFS00010
					  	$grNo = "00".$grNo;
					  }
					  elseif($grNo > 99 && $grNo <= 999)
					  {
					  	//IFS00100
					  	$grNo = "0".$grNo;
					  }
					  else
					  {
					  	//IFS00100
					  	$grNo = $grNo;
					  }
					}
					$userType            = $data[3];
					$studentLoginId      = $data[4];
					$studentPassword     = $data[5];
					$parentLoginId       = $data[6];
					$parentPassword      = $data[7];
	        if($data[10] == 'Yes')
	        {
					  $activated           = 'Y';
					}
					else
					{
					  $activated           = 'N';
					}
					
					if(!isset($data[14])){$joinedInClass = '';} else {$joinedInClass = $data[14]; }
					$joiningDate         = substr($data[15], 6, 4)."-".substr($data[15], 3, 2)."-".substr($data[15], 0, 2);
					$studentName         = $data[18];
					$dateOfBirth         = substr($data[19], 6, 4)."-".substr($data[19], 3, 2)."-".substr($data[19], 0, 2);
					if(!isset($data[20])){$gender = '';} else {$gender = $data[20]; }
					if(!isset($data[21])){$bloodGroup = '';} else {$bloodGroup = $data[21]; }
					if(!isset($data[23])){$houseName = '';} else {$houseName = $data[23]; }
					if(!isset($data[24])){$currentAddress = '';} else {$currentAddress = (addslashes($data[24])); }
					if(!isset($data[25])){$currentState = '';} else {$currentState = $data[25]; }
					if(!isset($data[26])){$currentAddressPin = '';} else {$currentAddressPin = $data[26]; }
					if(!isset($data[27])){$residencePhone1 = '';} else {$residencePhone1 = $data[27]; }
					if(!isset($data[28])){$residencePhone2 = '';} else {$residencePhone2 = $data[28]; }
					if(!isset($data[29])){$smsMobile = '';} else {$smsMobile = $data[29]; }
					if(!isset($data[30])){$studentEmail = '';} else {$studentEmail = $data[30]; }
					if(!isset($data[31])){$fatherName = '';} else {$fatherName = $data[31]; }
					if(!isset($data[32])){$fatherPhone = '';} else {$fatherPhone = $data[32]; }
					if(!isset($data[33])){$fatherEmail = '';} else {$fatherEmail = $data[33]; }
					if(!isset($data[34])){$fatherMobile = '';} else {$fatherMobile = $data[34]; }
					if(!isset($data[35])){$fatherOccupation = '';} else {$fatherOccupation = (addslashes($data[35])); }
					if(!isset($data[36])){$fathersDesignation = '';} else {$fathersDesignation = (addslashes($data[36])); }
					if(!isset($data[37])){$fathersOrganization = '';} else {$fathersOrganization = (addslashes($data[37])); }
					if(!isset($data[38])){$mothersName = '';} else {$mothersName = $data[38]; }
					if(!isset($data[39])){$mothersPhone = '';} else {$mothersPhone = $data[39]; }
					if(!isset($data[40])){$mothersEmailid = '';} else {$mothersEmailid = $data[40]; }
					if(!isset($data[41])){$mothersMobile = '';} else {$mothersMobile = $data[41]; }
					if(!isset($data[42])){$motherOccupation = '';} else {$motherOccupation = (addslashes($data[42])); }
					if(!isset($data[43])){$mothersDesignation = '';} else {$mothersDesignation = (addslashes($data[43])); }
					if(!isset($data[44])){$mothersOrganization = '';} else {$mothersOrganization = (addslashes($data[44])); }
					if(!isset($data[49])){$permanentAddress = '';} else {$permanentAddress = (addslashes($data[49])); }
					if(!isset($data[50])){$permanentState = '';} else {$permanentState = $data[50]; }
					if(!isset($data[51])){$permanentPIN = '';} else {$permanentPIN = $data[51]; }
					if(!isset($data[53])){$urban = '';} else {$urban = $data[53]; }
					if(!isset($data[54])){$religion = '';} else {$religion = $data[54]; }
					$createdOn           = substr($data[56], 6, 4)."-".substr($data[56], 3, 2)."-".substr($data[56], 0, 2);
					$studentImage        = '<img width="90" height="110" src="studentImage/'.$grNo.'_P.jpg" alt="'.$grNo.'_P.jpg" class="studentImage"><p id="large"></p>';
					$fatherImage         = '<img width="90" height="110" src="studentImage/'.$grNo.'_F.jpg" alt="'.$grNo.'_F.jpg" class="studentImage"><p id="large"></p>';
					$motherImage         = '<img width="90" height="110" src="studentImage/'.$grNo.'_M.jpg" alt="'.$grNo.'_M.jpg" class="studentImage"><p id="large"></p>';
					if (!isset($data[58]))
					{
					  $previousSchool    = '';
				  }
				  else
				  {
				  	$previousSchool    = $data[58];
				  }
	       
	        $uploadStudentMaster = "INSERT INTO studentmaster 
	                                       (studentId,
																					grNo,
																					userType,
																					studentLoginId,
																					studentPassword,
																					parentLoginId,
																					parentPassword,
																					activated,
																					joinedInClass,
																					joiningDate,
																					studentName,
																					dateOfBirth,
																					gender,
																					bloodGroup,
																					houseName,
																					currentAddress,
																					currentState,
																					currentAddressPin,
																					residencePhone1,
																					residencePhone2,
																					smsMobile,
																					studentEmail,
																					fatherName,
																					fatherPhone,
																					fatherEmail,
																					fatherMobile,
																					fatherOccupation,
																					fathersDesignation,
																					fathersOrganization,
																					mothersName,
																					mothersPhone,
																					mothersEmailid,
																					mothersMobile,
																					motherOccupation,
																					mothersDesignation,
																					mothersOrganization,
																					permanentAddress,
																					permanentState,
																					permanentPIN,
																					urban,
																					religion,
																					createdOn,
																					previousSchool,
																					studentImage,
																					fatherImage,
																					motherImage)
	      	                        VALUES ('".$studentId."',
	      	                                '".$grNo."',
	      	                                '".$userType."',
	      	                                '".$studentLoginId."',
	      	                                '".$studentPassword."',
	      	                                '".$parentLoginId."',
	      	                                '".$parentPassword."',
	      	                                '".$activated."',
	      	                                '".$joinedInClass."',
	      	                                '".$joiningDate."',
	      	                                '".$studentName."',
	      	                                '".$dateOfBirth."',
	      	                                '".$gender."',
	      	                                '".$bloodGroup."',
	      	                                '".$houseName."',
	      	                                '".$currentAddress."',
	      	                                '".$currentState."',
	      	                                '".$currentAddressPin."',
	      	                                '".$residencePhone1."',
	      	                                '".$residencePhone2."',
	      	                                '".$smsMobile."',
	      	                                '".$studentEmail."',
	      	                                '".$fatherName."',
	      	                                '".$fatherPhone."',
	      	                                '".$fatherEmail."',
	      	                                '".$fatherMobile."',
	      	                                '".$fatherOccupation."',
	      	                                '".$fathersDesignation."',
	      	                                '".$fathersOrganization."',
	      	                                '".$mothersName."',
	      	                                '".$mothersPhone."',
	      	                                '".$mothersEmailid."',
	      	                                '".$mothersMobile."',
	      	                                '".$motherOccupation."',
	      	                                '".$mothersDesignation."',
	      	                                '".$mothersOrganization."',
	      	                                '".$permanentAddress."',
	      	                                '".$permanentState."',
	      	                                '".$permanentPIN."',
	      	                                '".$urban."',
	      	                                '".$religion."',
	      	                                '".$createdOn."',
	      	                                '".$previousSchool."',
	      	                                '".$studentImage."',
	      	                                '".$fatherImage."',
	      	                                '".$motherImage."')";
	      	om_query($uploadStudentMaster);
		    }
	      $row++;
	    }
	  } 
	  else
	  {
	    echo "There was an error uploading the file, please try again!";
	  }
	}
}
?>
<body>
<H1><CENTER><FONT color="red">Student Profile</FONT></CENTER></H1><HR>
<CENTER>
  <FORM enctype="multipart/form-data" action="" name="studentMaster" method="POST"> 
	<a href="index.php">Home</a>
	<br /><br />
  <INPUT size="60" type="hidden" name="filePath"><BR>
  File Name: 
  <INPUT name="fileName" type="file" onChange="document.studentMaster.filePath.value=document.studentMaster.fileName.value;">
  <INPUT type="submit" name="submitBtn" value="Submit File"> 
</FORM>
</CENTER>
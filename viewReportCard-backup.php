<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$class      = '';
  $count      = 0;
  $today      = date('Y-m-d');
	$section    = '';
	$nominalRollId = 0;
	
	if(isset($_REQUEST['startYear']))
  {
  	$academicStartYear = $_REQUEST['startYear']."-04-01";
  	$nextYear          = $_REQUEST['startYear'] + 1;
  	$academicEndYear   = $nextYear."-03-31";
  	$academicStartYearSelected = $_REQUEST['startYear'];
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y')."-04-01";
	  	$nextYear          = date('Y') + 1;
	  	$academicEndYear   = $nextYear."-03-31";
	  	$academicStartYearSelected = date('Y');
		}
		else
		{
			$prevYear          = date('Y') - 1;
			$academicStartYear = $prevYear."-04-01";
	  	$academicEndYear   = date('Y')."-03-31";
	  	$academicStartYearSelected = $prevYear;
		}
	}
	
	$class   = isset($_GET['class']) && $_GET['class'] != '' ? $_GET['class'] : 0;
	$section = isset($_GET['section']) && $_GET['section'] != '' ? $_GET['section'] : 0;
	
	$selectId = "SELECT employeeMasterId
                 FROM employeemaster
                WHERE loginId = '".$_SESSION['s_activName']."'";
  $selectIdRes = mysql_query($selectId);
  if($idRow = mysql_fetch_array($selectIdRes))
  {
  	$selectClass = "SELECT class,section
	                    FROM classteacherallotment
	                   WHERE employeeMasterId = '".$idRow['employeeMasterId']."'
	                     AND academicStartYear = '".$academicStartYear."'
                       AND academicEndYear = '".$academicEndYear."'";
	  $selectClassRes = mysql_query($selectClass);
	  if($classRow = mysql_fetch_array($selectClassRes))
	  {
	  	if($class == 0)
	  	{
		  	$class   = $classRow['class'];
		  	$section = $classRow['section'];
		  }
		}
		
		$dtlArr = array();
		$g = 0;
		//change
		if($class == 0){
			$selectStdIn = "SELECT nominalroll.nominalRollId,nominalroll.grNo,nominalroll.rollNo,studentmaster.gender,studentmaster.activated,
	                         nominalroll.academicStartYear,nominalroll.academicEndYear,studentmaster.studentName,
	                         nominalroll.reportCardView1,nominalroll.reportCardView2
	                    FROM nominalroll
	               LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
	                   WHERE nominalroll.academicStartYear = '".$academicStartYear."'
                       AND nominalroll.academicEndYear = '".$academicEndYear."'
                       AND studentmaster.activated = 'Y'
                  ORDER BY nominalroll.rollNo";		
		} else {
			$selectStdIn = "SELECT nominalroll.nominalRollId,nominalroll.grNo,nominalroll.rollNo,studentmaster.gender,studentmaster.activated,
	                         nominalroll.academicStartYear,nominalroll.academicEndYear,studentmaster.studentName,
	                         nominalroll.reportCardView1,nominalroll.reportCardView2
	                    FROM nominalroll
	               LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
	                   WHERE nominalroll.class = '".$class."'
	                     AND nominalroll.section = '".$section."'
	                     AND nominalroll.academicStartYear = '".$academicStartYear."'
                       AND nominalroll.academicEndYear = '".$academicEndYear."'
                       AND studentmaster.activated = 'Y'
                  ORDER BY nominalroll.rollNo";
		}

	  $selectStdInRes = mysql_query($selectStdIn);
	  $count = mysql_num_rows($selectStdInRes);
	  while($stdInRow = mysql_fetch_array($selectStdInRes))
	  {
	  	$dtlArr[$g]['nominalRollId'] = $stdInRow['nominalRollId'];
	  	$dtlArr[$g]['rollNo']        = $stdInRow['rollNo'];
	  	$dtlArr[$g]['grNo']          = $stdInRow['grNo'];
	  	$dtlArr[$g]['studentName']   = $stdInRow['studentName'];
	  	
	  	$dtlArr[$g]['reportCardView1'] = $stdInRow['reportCardView1'];
	  	$dtlArr[$g]['reportCardView2'] = $stdInRow['reportCardView2'];
	  	$g++;
	  }
  }
  
  if(isset($_POST['submitTaken']))
  {
    // echo var_dump($_POST); exit;
  	$loopCount = 0;
  	while($loopCount < count($_POST['nominalRollId']))
  	{
  		$nominalRollId    = ($_POST['nominalRollId'][$loopCount] != '') ? $_POST['nominalRollId'][$loopCount] : 0;
  		
  		
      // $reportCardView1  = isset($_POST['reportCardView1'][$loopCount]) ? 'Y' : 'N';
      // $reportCardView2  = isset($_POST['reportCardView2'][$loopCount]) ? 'Y' : 'N';

      $reportCardView1  = array_search($nominalRollId, $_POST['reportCardView1']) === false ? 'N' : 'Y';
      $reportCardView2  = array_search($nominalRollId, $_POST['reportCardView2']) === false ? 'N' : 'Y';
  		
  		if($_POST['nominalRollId'][$loopCount] != '' && $_POST['nominalRollId'][$loopCount] > 0)
  		{
				$updateClass = "UPDATE nominalroll
		  	                   SET reportCardView1 = '".$reportCardView1."',
		  	                       reportCardView2 = '".$reportCardView2."'
		  	                 WHERE nominalRollId = ".$nominalRollId."";
			  $updateClassRes = om_query($updateClass);
			  if(!$updateClassRes)
			  {
			  	echo "Update Fail";
			  }
			  else
			  {
			  	header("Location:viewReportCard.php?startYear=".$academicStartYearSelected."&go=go");
			  }
			}
			$loopCount++;
		}
  }
  
  $c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster
                   WHERE className > 0
                     AND className <= 12";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  $secArrOut[0] = 'A';
  $secArrOut[1] = 'B';
  $secArrOut[2] = 'C';
  $secArrOut[3] = 'D';
  
  include("./bottom.php");
  $smarty->assign('academicStartYearSelected',$academicStartYearSelected);
  $smarty->assign('dtlArr',$dtlArr);
  $smarty->assign('secArrOut',$secArrOut);
  $smarty->assign('class',$class);
  $smarty->assign('section',$section);
  $smarty->assign('count',$count);
  $smarty->assign('cArray',$cArray);
  $smarty->assign('nominalRollId',$nominalRollId);
  $smarty->display('viewReportCard.tpl');  
}
?>
<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$feeStructureArray  = array();
	$todayAcademic = date('m-d');
	if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
	{
  	$academicStartYear = date('Y');
	}
	else
	{
		$academicStartYear = date('Y') - 1;
	}
	
	if(isset($_REQUEST['startYear']))
  {
	  $academicStartYear = $_REQUEST['startYear'];
	  $academicEndYear   = $_REQUEST['startYear'] + 1;
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y');
	  	$nextYear            = date('Y') + 1;
	  	$academicEndYear   = $nextYear;
		}
		else
		{
			$prevYear            = date('Y') - 1;
			$academicStartYear = $prevYear;
	  	$academicEndYear   = date('Y');
		}
	}
	
  $i = 0;
  $selectFeeStructure = "SELECT feestructure.feeStructureId,feestructure.academicStartYear,feestructure.academicEndYear,
                                feestructure.amount,feestructure.remarks,feetype.feeType
                           FROM feestructure
                      LEFT JOIN feetype ON feetype.feeTypeId = feestructure.feeTypeId
                          WHERE feestructure.academicStartYear = '".$academicStartYear."-04-01'
                            AND feestructure.academicEndYear = '".$academicEndYear."-03-31'";
  $selectFeeStructureRes = mysql_query($selectFeeStructure);
  while($feeStructureRow = mysql_fetch_array($selectFeeStructureRes))
  {
  	$feeStructureArray[$i]['feeStructureId']    = $feeStructureRow['feeStructureId'];
  	$feeStructureArray[$i]['feeType']           = $feeStructureRow['feeType'];
  	$feeStructureArray[$i]['academicStartYear'] = substr($feeStructureRow['academicStartYear'],0,4);
	  $feeStructureArray[$i]['academicEndYear']   = substr($feeStructureRow['academicEndYear'],0,4);
	  $feeStructureArray[$i]['amount']            = $feeStructureRow['amount'];
	  $feeStructureArray[$i]['remarks']           = $feeStructureRow['remarks'];
  	$i++;
  }
  
  include("./bottom.php");
  $smarty->assign('feeStructureArray',$feeStructureArray);
  $smarty->assign('academicStartYear',$academicStartYear);
  $smarty->display('feeStructureList.tpl');  
}
?>
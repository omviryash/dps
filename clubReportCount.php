<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$countTotalP = '';
	$countTotalA = '';
	$attendenceArr = array();
  $i = 0;
  
  if(isset($_REQUEST['startYear']))
  {
  	$academicStartYearCombo = $_REQUEST['startYear']."-04-01";
  	$nextYearCombo          = $_REQUEST['startYear'] + 1;
  	$academicEndYearCobmo   = $nextYearCombo."-03-31";
  	$academicStartYearSelected = $_REQUEST['startYear'];
	}
	else
	{
	  $todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYearSelected = date('Y');
	  	$academicStartYearCombo = date('Y')."-04-01";
	  	$nextYearCombo          = date('Y') + 1;
	  	$academicEndYearCobmo   = $nextYearCombo."-03-31";
		}
		else
		{
			$prevYearCobmo          = date('Y') - 1;
			$academicStartYearSelected = $prevYearCobmo;
			$academicStartYearCombo = $prevYearCobmo."-04-01";
	  	$academicEndYearCobmo   = date('Y')."-03-31";
		}
	}
	
  $selectAttendM = "SELECT subjectMasterId,subjectName
                      FROM subjectmaster
                     WHERE subjectType = 'Club'";
  $selectAttendMRes = mysql_query($selectAttendM);
  while($maRow = mysql_fetch_array($selectAttendMRes))
  {
  	$attendenceArr[$i]['subjectMasterId'] = $maRow['subjectMasterId'];
  	$attendenceArr[$i]['subjectName']     = $maRow['subjectName'];
  	
  	$selectAtnP = "SELECT nominalroll.nominalRollId
                     FROM nominalroll
                LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
                    WHERE clubTerm1 = '".$maRow['subjectName']."'
                      AND nominalroll.academicStartYear = '".$academicStartYearCombo."'
                      AND nominalroll.academicEndYear = '".$academicEndYearCobmo."'
                      AND activated = 'Y'";
    $selectAtnPRes = mysql_query($selectAtnP);
    $countP = mysql_num_rows($selectAtnPRes);
    $attendenceArr[$i]['countP'] = $countP;
    
    $selectAtnA = "SELECT nominalRollId
                     FROM nominalroll
                LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
                    WHERE clubTerm2 = '".$maRow['subjectName']."'
                      AND nominalroll.academicStartYear = '".$academicStartYearCombo."'
                      AND nominalroll.academicEndYear = '".$academicEndYearCobmo."'
                      AND activated = 'Y'";
    $selectAtnARes = mysql_query($selectAtnA);
    $countA = mysql_num_rows($selectAtnARes);
    $attendenceArr[$i]['countA'] = $countA;
    
    $countTotalP += $attendenceArr[$i]['countP'];
    $countTotalA += $attendenceArr[$i]['countA'];
  	$i++;
  }
  
  include("./bottom.php");
  $smarty->assign('attendenceArr',$attendenceArr);
  $smarty->assign('countTotalP',$countTotalP);
  $smarty->assign('countTotalA',$countTotalA);
  $smarty->assign('academicStartYearSelected',$academicStartYearSelected);
  $smarty->display('clubReportCount.tpl');
}
?>
<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] != 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	if(isset($_REQUEST['startYear']))
  {
	  $academicStartYear = $_REQUEST['startYear'];
	  $academicEndYear   = $_REQUEST['startYear'] + 1;
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y');
	  	$nextYear            = date('Y') + 1;
	  	$academicEndYear   = $nextYear;
		}
		else
		{
			$prevYear            = date('Y') - 1;
			$academicStartYear = $prevYear;
	  	$academicEndYear   = date('Y');
		}
	}
	
	$nominalArr = array();
	$allotArr   = array();
  $i = 0;
  $j = 0;
  $selectNominal = "SELECT nominalRollId,nominalroll.grNo,nominalroll.academicStartYear,nominalroll.academicEndYear,class,
                           section,rollNo,feeGroup,libraryMemberType,libraryMemberType,studentmaster.studentName,
                           busRoute,busStop,subjectGroup,coScholasticGroup,clubTerm1,clubTerm2,
                           boardRegistrionId,boardRollNo,activated,studentmaster.studentLoginId,studentmaster.parentLoginId
                      FROM nominalroll
                 LEFT JOIN studentmaster ON studentmaster.grNo = nominalroll.grNo
                     WHERE 1 = 1
                       AND academicStartYear = '".$academicStartYear."-04-01'
                       AND academicEndYear = '".$academicEndYear."-03-31'
                       AND studentmaster.studentLoginId = '".$_SESSION['s_activName']."' 
                           OR studentmaster.parentLoginId = '".$_SESSION['s_activName']."'
                       AND studentmaster.activated = 'Y'
                  ORDER BY nominalroll.rollNo";
  $selectNominalRes = mysql_query($selectNominal);
  while($nominalRow = mysql_fetch_array($selectNominalRes))
  {
    $nominalArr[$i]['academicYear']       = substr($nominalRow['academicStartYear'],0,4).'-'.substr($nominalRow['academicEndYear'],2,2);
    $nominalArr[$i]['class']              = $nominalRow['class'];
    $nominalArr[$i]['section']            = $nominalRow['section'];
    
    $selectAllot = "SELECT subjectteacherallotment.academicStartYear,subjectteacherallotment.academicEndYear,
                           subjectteacherallotment.class,subjectteacherallotment.section,subjectmaster.subjectName,employeemaster.name
	                    FROM subjectteacherallotment
	               LEFT JOIN employeemaster ON employeemaster.employeeMasterId = subjectteacherallotment.employeeMasterId
	               LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = subjectteacherallotment.subjectMasterId
	                   WHERE subjectteacherallotment.academicStartYear = '".$academicStartYear."-04-01'
	                     AND subjectteacherallotment.academicEndYear = '".$academicEndYear."-03-31'
	                     AND subjectteacherallotment.class = '".$nominalRow['class']."'
	                     AND subjectteacherallotment.section = '".$nominalRow['section']."'
	                ORDER BY subjectmaster.subjectName";
	  $selectAllotRes = mysql_query($selectAllot);
	  while($allotRow = mysql_fetch_array($selectAllotRes))
	  {
	  	$allotArr[$j]['academicYear']       = substr($allotRow['academicStartYear'],0,4).'-'.substr($allotRow['academicEndYear'],2,2);
	    $allotArr[$j]['class']              = $allotRow['class'];
	    $allotArr[$j]['section']            = $allotRow['section'];
	    $allotArr[$j]['subjectName']        = $allotRow['subjectName'];
	    $allotArr[$j]['teacherName']        = $allotRow['name'];
	    $j++;
	  }
    $i++;
  }
  
  include("./bottom.php");
  $smarty->assign('nominalArr',$nominalArr);
  $smarty->assign('allotArr',$allotArr);
  $smarty->assign('academicStartYear',$academicStartYear);
  $smarty->assign('academicEndYear',$academicEndYear);
  $smarty->display('mySubjectTeacher.tpl');  
}
?>
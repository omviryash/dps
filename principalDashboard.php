<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
  $today = date('Y-m-d');
  //For Teachers	
  $totalpresentTeacher='0';
  $presentTeacher = "SELECT count( employeeattendId ) as total FROM employeeattend WHERE employeeCode IN (SELECT employeeCode FROM employeemaster WHERE userType = 'Teacher' AND activated = 'Yes') AND attendences = 'P'
				AND date = CURDATE( )";
  $presentTeacherResult = mysql_query($presentTeacher);
  if($ptRow = mysql_fetch_array($presentTeacherResult))
  {
	  $totalpresentTeacher =$ptRow["total"];	
  }
  
  $totalTeacherq = "SELECT count( employeeMasterId ) as total FROM employeemaster WHERE userType = 'Teacher' AND activated = 'Yes'";
  $totalTeacherResult = mysql_query($totalTeacherq);
  if($abRow = mysql_fetch_array($totalTeacherResult))
  {
	  $totalTeacher =$abRow["total"];	
  }
  
  //For Admin Staff
  $totalpresentAdminstaff='0';
  $presentAdminstaff = "SELECT count( employeeattendId ) as total FROM employeeattend WHERE employeeCode IN (SELECT employeeCode FROM employeemaster WHERE userType = 'Administrator' AND activated = 'Yes') AND attendences = 'P'
				AND date = CURDATE( )";
  $presentAdminResult = mysql_query($presentAdminstaff);
  if($paRow = mysql_fetch_array($presentAdminResult))
  {
	  $totalpresentAdminstaff =$paRow["total"];	
  }
  
  $totalAdminq = "SELECT count( employeeMasterId ) as total FROM employeemaster WHERE userType = 'Administrator' AND activated = 'Yes'";
  $totalAdminResult = mysql_query($totalAdminq);
  if($taRow = mysql_fetch_array($totalAdminResult))
  {
	  $totalAdminstaff =$taRow["total"];	
  }
  
  //For Students
  $totalpresentStudent='0';
  $presentStudent = "SELECT count( attendenceId ) as total FROM attendence WHERE grNo IN (SELECT grNo FROM studentmaster WHERE userType = 'Student' AND activated = 'Y') AND attendences = 'P'
				AND date = CURDATE( )";
  $presentStudentResult = mysql_query($presentStudent);
  if($psRow = mysql_fetch_array($presentStudentResult))
  {
	  $totalpresentStudent =$psRow["total"];	
  }
  
  $totalStudentq = "SELECT count( studentMasterId ) as total FROM studentmaster WHERE userType = 'Student' AND activated = 'Y'";
  $totalStudentResult = mysql_query($totalStudentq);
  if($tsRow = mysql_fetch_array($totalStudentResult))
  {
	  $totalStudent =$tsRow["total"];	
  }
  
  //For Class IV
  $totalpresentStudentIV='0';
  $presentStudentIV = "SELECT count( employeeattendId ) as total FROM employeeattend WHERE employeeCode IN (SELECT employeeCode FROM employeemaster WHERE (userType = 'Class IV' OR userType = 'Worker' OR userType = 'Cleaner') AND activated = 'Yes') AND attendences = 'P'
				AND date = CURDATE( )";
  $presentStudentResultIV = mysql_query($presentStudentIV);
  if($psRowIV = mysql_fetch_array($presentStudentResultIV))
  {
	  $totalpresentStudentIV =$psRowIV["total"];	
  }
  
  $totalStudentIVq = "SELECT count( employeeMasterId ) as total FROM employeemaster WHERE (userType = 'Class IV' OR userType = 'Worker' OR userType = 'Cleaner') AND activated = 'Yes' ";
  $totalStudentResultIV = mysql_query($totalStudentIVq);
  if($tsRowIV = mysql_fetch_array($totalStudentResultIV))
  {
	  $totalStudentIV =$tsRowIV["total"];	
  }
  
  include("./bottom.php");
  $smarty->assign('presentTeacher',$totalpresentTeacher);
  $smarty->assign('totalTeacher',$totalTeacher);
  $smarty->assign('presentAdmin',$totalpresentAdminstaff);
  $smarty->assign('totalAdmin',$totalAdminstaff);
  $smarty->assign('presentStudent',$totalpresentStudent);
  $smarty->assign('totalStudent',$totalStudent);
  $smarty->assign('presentStudentIV',$totalpresentStudentIV);
  $smarty->assign('totalStudentIV',$totalStudentIV);
  $smarty->assign('today',$today);
  $smarty->display('principalDashboard.tpl');  
}
?>
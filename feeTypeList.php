<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$feeType      = "";
	$feeTypeId    = 0;
	
  $i = 0;
  $feeTypeArray = array();
  $selectFeeType = "SELECT feeTypeId,feeType
                      FROM feetype";
  $selectFeeTypeRes = mysql_query($selectFeeType);
  while($feeTypeRow = mysql_fetch_array($selectFeeTypeRes))
  {
  	$feeTypeArray[$i]['feeTypeId'] = $feeTypeRow['feeTypeId'];
  	$feeTypeArray[$i]['feeType']   = $feeTypeRow['feeType'];
  	$i++;
  }
  
  if(isset($_REQUEST['feeTypeId']) > 0)
  {
    $selectFeeType = "SELECT feeTypeId,feeType
                        FROM feetype
                       WHERE feeTypeId = ".$_REQUEST['feeTypeId'];
    $selectFeeTypeRes = mysql_query($selectFeeType);
    if($feeTypeRow = mysql_fetch_array($selectFeeTypeRes))
    {
    	$feeTypeId         = $feeTypeRow['feeTypeId'];
    	$feeType           = $feeTypeRow['feeType'];
    }
  }
  include("./bottom.php");
  $smarty->assign('feeType',$feeType);
  $smarty->assign('feeTypeId',$feeTypeId);
  $smarty->assign('feeTypeArray',$feeTypeArray);
  $smarty->display('feeTypeList.tpl');  
}
?>
<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$classTeacherAllotmentId = 0;
	$employeeMasterId        = 0;
	$todayAcademic = date('m-d');
	if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
	{
  	$academicStartYear = date('Y');
	}
	else
	{
		$academicStartYear = date('Y') - 1;
	}
	$academicEndYear         = '0000';
	$class   = "";
	$section = "";
	$period  = 0;
	$subjectMasterId = "";
	$isEdit  = 0;
	$subjectTeacherAllotmentId  = 0;
	
	$classArray    = array();
	
  if(isset($_POST['Submit']))
  {
  	$nextYear = $_POST['startYear'] + 1;
  	
  	$academicStartYear = $_POST['startYear']."-04-01";
  	$academicEndYear   = $nextYear."-03-31";
  	$employeeMasterId  = isset($_POST['employeeMasterId']) ? $_POST['employeeMasterId'] : 0;
  	$subjectMasterId  = isset($_POST['subjectMasterId']) ? $_POST['subjectMasterId'] : 0;
  	
  	$class   = isset($_POST['class']) ? $_POST['class'] : "";
  	$section = isset($_POST['section']) ? $_POST['section'] : "";
  	$period = isset($_POST['period']) ? $_POST['period'] : 0;
  	$subjectTeacherAllotmentId = isset($_POST['subjectTeacherAllotmentId']) ? $_POST['subjectTeacherAllotmentId'] : 0;
  	
  	if($subjectTeacherAllotmentId == 0)
  	{
  	  $insertClass = "INSERT INTO subjectteacherallotment (employeeMasterId,academicStartYear,academicEndYear,class,section,subjectMasterId,period)
  	                  VALUES (".$employeeMasterId.",'".$academicStartYear."','".$academicEndYear."','".$class."','".$section."',".$subjectMasterId.",'".$period."')";
  	  $insertClassRes = om_query($insertClass);
  	  if(!$insertClassRes)
  	  {
  	  	echo "Insert Fail";
  	  }
  	  else
  	  {
  	  	header("Location:subjectTeacherAllotment.php?done=1");
  	  }
  	}
  	else
  	{
  	  $isEdit = 1;
  	  $updateClass = "UPDATE subjectteacherallotment
  	                     SET employeeMasterId  = ".$employeeMasterId.",
  	                         academicStartYear = '".$academicStartYear."',
  	                         academicEndYear   = '".$academicEndYear."',
  	                         class   = '".$class."',
  	                         section = '".$section."',
  	                         subjectMasterId = ".$subjectMasterId.",
  	                         period = ".$period."
  	                   WHERE subjectTeacherAllotmentId = ".$_REQUEST['subjectTeacherAllotmentId'];
      $updateClassRes = om_query($updateClass);
      if(!$updateClassRes)
      {
      	echo "Update Fail";
      }
      else
      {
      	header("Location:subjectTeacherAllotment.php?done=1");
      }
  	}
  }
  
  $i = 0;
  $selectClass = "SELECT DISTINCT subjectteacherallotment.subjectTeacherAllotmentId,
                         subjectteacherallotment.academicStartYear,subjectteacherallotment.academicEndYear,
                         subjectteacherallotment.class,subjectteacherallotment.section,
                         employeemaster.name,subjectmaster.subjectName,subjectteacherallotment.period,
                         classmaster.priority
                    FROM subjectteacherallotment
               LEFT JOIN employeemaster ON employeemaster.employeeMasterId = subjectteacherallotment.employeeMasterId
               LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = subjectteacherallotment.subjectMasterId
               LEFT JOIN classmaster ON classmaster.className = subjectteacherallotment.class
                ORDER BY academicStartYear DESC,classmaster.priority,subjectteacherallotment.section,subjectmaster.subjectName";
  $selectClassRes = mysql_query($selectClass);
  while($classRow = mysql_fetch_array($selectClassRes))
  {
  	$classArray[$i]['subjectTeacherAllotmentId'] = $classRow['subjectTeacherAllotmentId'];
  	$classArray[$i]['name']                      = $classRow['name'];
  	$classArray[$i]['academicStartYear']         = substr($classRow['academicStartYear'],0,4);
  	$classArray[$i]['academicEndYear']           = substr($classRow['academicEndYear'],0,4);
  	
  	$classArray[$i]['class']       = $classRow['class'];
  	$classArray[$i]['priority']    = $classRow['priority'];
  	$classArray[$i]['section']     = $classRow['section'];
  	$classArray[$i]['subjectName'] = $classRow['subjectName'];
  	$classArray[$i]['period']      = $classRow['period'];
  	$i++;
  }
  
  if(isset($_REQUEST['subjectTeacherAllotmentId']) > 0)
  {
    $selectClass = "SELECT subjectteacherallotment.subjectTeacherAllotmentId,
                           subjectteacherallotment.academicStartYear,subjectteacherallotment.academicEndYear,
                           subjectteacherallotment.class,subjectteacherallotment.section,subjectteacherallotment.period,
                           employeemaster.name,employeemaster.employeeMasterId,subjectteacherallotment.subjectMasterId
                      FROM subjectteacherallotment
                 LEFT JOIN employeemaster ON employeemaster.employeeMasterId = subjectteacherallotment.employeeMasterId
                 LEFT JOIN subjectmaster ON subjectmaster.subjectMasterId = subjectteacherallotment.subjectMasterId
                     WHERE subjectTeacherAllotmentId = ".$_REQUEST['subjectTeacherAllotmentId']."";
    $selectClassRes = mysql_query($selectClass);
    if($classRow = mysql_fetch_array($selectClassRes))
    {
    	$subjectTeacherAllotmentId = $classRow['subjectTeacherAllotmentId'];
    	$employeeMasterId          = $classRow['employeeMasterId'];
    	$academicStartYear         = substr($classRow['academicStartYear'],0,4);
    	$academicEndYear           = substr($classRow['academicEndYear'],0,4);
    	$class                     = $classRow['class'];
    	$section                   = $classRow['section'];
    	$subjectMasterId           = $classRow['subjectMasterId'];
    	$period                    = $classRow['period'];
    }
  }
  
  $p=0;
	$empArray = array();
	$selectEmp = "SELECT employeeMasterId,name
                  FROM employeemaster
	               WHERE activated='Yes' && (userType = 'Teacher' || userType = 'Administrator' )";
	$selectEmpRes = mysql_query($selectEmp);
	while($empRow = mysql_fetch_array($selectEmpRes))
	{
	  $empArray['employeeMasterId'][$p] = $empRow['employeeMasterId'];
	  $empArray['name'][$p]             = $empRow['name'];
	  $p++;
	}
	
	$c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  $k = 0;
  $selectClub = "SELECT subjectMasterId,subjectName
                   FROM subjectmaster
                  WHERE subjectType = 'Main' OR subjectType = 'Optional'";
  $selectClubRes = mysql_query($selectClub);
  while($clubRow = mysql_fetch_array($selectClubRes))
  {
  	$clubArr['subjectMasterId'][$k]   = $clubRow['subjectMasterId'];
  	$clubArr['subjectName'][$k]       = $clubRow['subjectName'];
  	$k++;
  }
	
  $secArrOut[0] = 'A';
  $secArrOut[1] = 'B';
  $secArrOut[2] = 'C';
  $secArrOut[3] = 'D';
  
  include("./bottom.php");
  $smarty->assign('subjectTeacherAllotmentId',$subjectTeacherAllotmentId);
  $smarty->assign('employeeMasterId',$employeeMasterId);
  $smarty->assign('academicStartYear',$academicStartYear);
  $smarty->assign('academicEndYear',$academicEndYear);
  $smarty->assign('class',$class);
  $smarty->assign('isEdit',$isEdit);
  $smarty->assign('section',$section);
  $smarty->assign('subjectMasterId',$subjectMasterId);
  $smarty->assign('cArray',$cArray);
  $smarty->assign('secArrOut',$secArrOut);
  $smarty->assign('classArray',$classArray);
  $smarty->assign('empArray',$empArray);
  $smarty->assign('clubArr',$clubArr);
  $smarty->assign('period',$period);
  $smarty->display('subjectTeacherAllotment.tpl');  
}
?>
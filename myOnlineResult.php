<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] != 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$totalMarks = 0;
  $scheduleMasterId = isset($_GET['scheduleMasterId']) && $_GET['scheduleMasterId'] > 0 ? $_GET['scheduleMasterId'] : 0;
  $grNo = isset($_GET['grNo']) && $_GET['grNo'] != '' ? $_GET['grNo'] : 0;
	$classArray = array();
  $i = 0;
  $selectClass = "SELECT myonlinetest.myOnlineTestId,onlinetest.question,onlinetest.option1,onlinetest.option2,onlinetest.class,
                         onlinetest.option3,onlinetest.option4,onlinetest.answer,myonlinetest.answer AS myAnswer
                    FROM myonlinetest
               LEFT JOIN onlinetest ON onlinetest.onlineTestId = myonlinetest.onlineTestId
                   WHERE myonlinetest.scheduleMasterId = ".$scheduleMasterId."
                     AND myonlinetest.grNo = ".$grNo."";
  $selectClassRes = mysql_query($selectClass);
  $totalQuestion = mysql_num_rows($selectClassRes);
  while($classRow = mysql_fetch_array($selectClassRes))
  {
  	$classArray[$i]['myOnlineTestId'] = $classRow['myOnlineTestId'];
  	$classArray[$i]['class']          = $classRow['class'];
  	$classArray[$i]['question']       = $classRow['question'];
  	$classArray[$i]['option1']        = $classRow['option1'];
  	$classArray[$i]['option2']        = $classRow['option2'];
  	$classArray[$i]['option3']        = $classRow['option3'];
  	$classArray[$i]['option4']        = $classRow['option4'];
  	$classArray[$i]['answer']         = $classRow['answer'];
  	$classArray[$i]['myAnswer']       = $classRow['myAnswer'];
  	if($classRow['myAnswer'] == $classRow['answer'])
  	{
  	  $classArray[$i]['marks']       = 1;
  	}
  	else
  	{
  		$classArray[$i]['marks']       = 0;
  	}
  	$totalMarks += $classArray[$i]['marks'];
  	$i++;
  }
  
  include("./bottom.php");
  $smarty->assign('classArray',$classArray);
  $smarty->assign('totalMarks',$totalMarks);
  $smarty->assign('totalQuestion',$totalQuestion);
  $smarty->display('myOnlineResult.tpl');  
}
?>
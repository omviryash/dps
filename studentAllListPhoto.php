<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$activated = isset($_GET['activated']) && $_GET['activated'] !='' ? $_GET['activated'] : 'Y';
	$stdArray = array();
  $i = 0;
  $selectStd = "SELECT studentName,fatherName,mothersName,grNo,gender,studentImage,fatherImage,motherImage
                  FROM studentmaster";
  $selectStdRes = mysql_query($selectStd);
  while($stdRow = mysql_fetch_array($selectStdRes))
  {
  	$stdArray[$i]['studentName']      = $stdRow['studentName'];
  	$stdArray[$i]['fatherName']       = $stdRow['fatherName'];
  	$stdArray[$i]['mothersName']      = $stdRow['mothersName'];
  	$stdArray[$i]['grNo']             = $stdRow['grNo'];
  	$stdArray[$i]['gender']           = $stdRow['gender'];
  	$stdArray[$i]['studentImage']     = $stdRow['studentImage'];
  	$stdArray[$i]['fatherImage']      = $stdRow['fatherImage'];
  	$stdArray[$i]['motherImage']      = $stdRow['motherImage'];
  	$i++;
  }
  
  $activeValues[0] = 'Y';
  $activeValues[1] = 'N';
  $activeValues[2] = 'All';
  $activeOutPut[0] = 'Yes';
  $activeOutPut[1] = 'No';
  $activeOutPut[2] = 'Both';
  
  include("./bottom.php");
  $smarty->assign('stdArray',$stdArray);
  $smarty->assign('activeValues',$activeValues);
	$smarty->assign('activeOutPut',$activeOutPut);
	$smarty->assign('activated',$activated);
  $smarty->display('studentAllListPhoto.tpl');  
}
?>
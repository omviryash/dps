<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$classArray = array();
	$notFound = 'Record Not Found';
	$i = 0;
	if(isset($_REQUEST['bookAccessionNo']))
	{
	  $selectClass = "SELECT bookmaster.bookMasterId,bookmaster.bookType,bookmaster.bookAccessionNo,bookmaster.ISBN,bookmaster.bookTitle,
	                         bookmaster.author1,bookmaster.author2,bookmaster.location,bookmaster.price,bookmaster.language,
	                         publisherId
	                    FROM bookmaster
	                   WHERE bookAccessionNo like '%".$_REQUEST['bookAccessionNo']."%'";
	                   
	  $selectClassRes = mysql_query($selectClass);
	  while($classRow = mysql_fetch_array($selectClassRes))
	  {
	  	$classArray[$i]['bookMasterId']    = $classRow['bookMasterId'];
	  	$classArray[$i]['bookType']        = $classRow['bookType'];
	  	$classArray[$i]['bookAccessionNo'] = $classRow['bookAccessionNo'];
	  	$classArray[$i]['ISBN']		   = $classRow['ISBN'];
	  	$classArray[$i]['bookTitle']       = $classRow['bookTitle'];
	  	$classArray[$i]['author1']         = $classRow['author1'];
	  	$classArray[$i]['author2']         = $classRow['author2'];
	  	$classArray[$i]['location']        = $classRow['location'];
	  	$classArray[$i]['price']           = $classRow['price'];
	  	$classArray[$i]['publisherId']     = $classRow['publisherId'];
	  	$classArray[$i]['language']        = $classRow['language'];
	  	$i++;
	  }
	}
	else
	{
		$notFound = 'Record Not Found';
	}
	include("./bottom.php");
	$smarty->assign('classArray',$classArray);
	$smarty->assign('notFound',$notFound);
  $smarty->display('bookMasterListEdit.tpl');
}
?>
<?php
include "include/config.inc.php";
include("paginationStudent.php"); //pagination for this file
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$stdArray = array();
	$i = 0;
  $selectStd = "SELECT studentMasterId,studentName,fatherName,mothersName,grNo,dateOfBirth,joinedInClass,joiningDate,gender,bloodGroup,house.houseName,
                       currentAddress,currentState,currentAddressPin,residencePhone1,residencePhone2,studentEmail,
                       permanentAddress,permanentState,permanentPIN,religion,previousSchool,activated
                  FROM studentmaster
             LEFT JOIN house ON house.houseId = studentmaster.houseId
              ORDER BY studentMasterId DESC LIMIT $start, $limit";
  $selectStdRes = mysql_query($selectStd);
  while($stdRow = mysql_fetch_array($selectStdRes))
  {
  	$stdArray[$i]['studentMasterId']   = $stdRow['studentMasterId'];
  	$stdArray[$i]['studentName']       = $stdRow['studentName'];
  	$stdArray[$i]['fatherName']        = $stdRow['fatherName'];
  	$stdArray[$i]['mothersName']      = $stdRow['mothersName'];
  	$stdArray[$i]['grNo']              = $stdRow['grNo'];
  	$stdArray[$i]['dateOfBirth']       = $stdRow['dateOfBirth'];
  	$stdArray[$i]['joinedInClass']     = $stdRow['joinedInClass'];
  	$stdArray[$i]['joiningDate']       = $stdRow['joiningDate'];
  	$stdArray[$i]['gender']            = $stdRow['gender'];
  	$stdArray[$i]['bloodGroup']        = $stdRow['bloodGroup'];
  	$stdArray[$i]['houseName']         = $stdRow['houseName'];
  	$stdArray[$i]['currentAddress']    = $stdRow['currentAddress'];
    $stdArray[$i]['currentState']      = $stdRow['currentState'];
    $stdArray[$i]['currentAddressPin'] = $stdRow['currentAddressPin'];
  	$stdArray[$i]['residencePhone1']   = $stdRow['residencePhone1'];
  	$stdArray[$i]['residencePhone2']   = $stdRow['residencePhone2'];
  	$stdArray[$i]['studentEmail']      = $stdRow['studentEmail'];
  	$stdArray[$i]['permanentAddress']  = $stdRow['permanentAddress'];
  	$stdArray[$i]['permanentState']    = $stdRow['permanentState'];
    $stdArray[$i]['permanentPIN']      = $stdRow['permanentPIN'];
  	$stdArray[$i]['religion']          = $stdRow['religion'];
  	$stdArray[$i]['previousSchool']    = $stdRow['previousSchool'];
  	$stdArray[$i]['activated']         = $stdRow['activated'];
  	$i++;
  }
	
	$filterArr[0] = 100;
	$filterArr[1] = 200;
	$filterArr[2] = 500;
	$filterArr[3] = 1000;
	$filterArr[4] = 'All';
	
	include("./bottom.php");
	$smarty->assign('stdArray',$stdArray);
	$smarty->assign('pagination',$pagination);
	$smarty->assign('filterArr',$filterArr);
	$smarty->assign('limit',$limit);
  $smarty->display('studentAllListNew.tpl');
}
?>
ALTER TABLE  `librarytransaction` ADD  `checktype` CHAR( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER  `libraryTransactionId` ,
ADD  `employeeCode` INT NULL AFTER  `checktype`;

CREATE TABLE  `busstopmaster` (
`busStopMasterId` INT NOT NULL AUTO_INCREMENT ,
`vehicleMasterId` INT NULL ,
`routeMasterId` INT NULL ,
`busTime` INT NULL ,
`localArea` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`busStop` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`distance` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
PRIMARY KEY (  `busStopMasterId` )
) ENGINE = INNODB;

ALTER TABLE  `busstopmaster` CHANGE  `busTime`  `busTime` TIME NULL DEFAULT NULL;

ALTER TABLE  `nominalroll` ADD  `busArrival` INT NULL AFTER  `wefDate` ,
ADD  `busDeparture` INT NULL AFTER  `busArrival` ,
ADD  `routeArrival` INT NULL AFTER  `busDeparture` ,
ADD  `routeDeparture` INT NULL AFTER  `routeArrival` ,
ADD  `busStopMasterId` INT NULL AFTER  `routeDeparture`;

ALTER TABLE  `bookmaster` ADD UNIQUE (
`bookAccessionNo`
)

ALTER TABLE  `librarytransaction` ADD  `fine` DOUBLE NULL AFTER  `returnableDate`;

ALTER TABLE  `librarytransaction` ADD  `finePay` VARCHAR( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER  `fine`;

ALTER TABLE  `feecollection` ADD  `feeName` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER  `feeTypeId`;


CREATE TABLE `timetable` (`timeTableId` INT NOT NULL AUTO_INCREMENT, 
`academicStartYear` DATE NULL, 
`academicEndYear` DATE NULL, 
`employeeMasterId` INT NULL, 
`subjectMasterId` INT NULL, 
`weekDay` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, 
`periodNo` INT NULL, PRIMARY KEY (`timeTableId`)) ENGINE = InnoDB;


CREATE TABLE  `feestructure` (
`feeStructureId` INT NOT NULL AUTO_INCREMENT ,
`academicStartYear` DATE NULL ,
`academicEndYear` DATE NULL ,
`feeTypeId` INT NULL ,
`amount` DOUBLE NULL ,
`remarks` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
PRIMARY KEY (  `feeStructureId` )
) ENGINE = INNODB;

ALTER TABLE  `feetype` DROP  `academicStartYear` ,
DROP  `academicEndYear` ,
DROP  `amount` ;

ALTER TABLE  `feecollection` ADD  `discount` DOUBLE NULL AFTER  `chNo`;

ALTER TABLE  `timetable` ADD  `class` VARCHAR( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER  `employeeMasterId` ,
ADD  `section` CHAR( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER  `class`;

ALTER TABLE  `employeemaster` ADD  `busArrival` INT NULL AFTER  `bloodGroup` ,
ADD  `busDeparture` INT NULL AFTER  `busArrival` ,
ADD  `routeArrival` INT NULL AFTER  `busDeparture` ,
ADD  `routeDeparture` INT NULL AFTER  `routeArrival` ,
ADD  `busStopMasterId` INT NULL AFTER  `routeDeparture`;

ALTER TABLE  `examschedule` ADD  `updated` CHAR( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT  'N' AFTER  `studentView`;


CREATE TABLE `onlinetest` (`onlineTestId` INT NOT NULL AUTO_INCREMENT, `subjectMasterId` INT NULL, `class` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, `question` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL, `option1` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, `option2` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, `option3` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, `option4` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, `answer` CHAR(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, PRIMARY KEY (`onlineTestId`)) ENGINE = InnoDB;

ALTER TABLE  `onlinetest` ADD  `qNo` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER  `onlineTestId`;

ALTER TABLE  `feestructure` ADD  `mainStatus` CHAR( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT  '0' AFTER  `remarks`;

CREATE TABLE  `onlineschedule` (
`scheduleMasterId` INT NOT NULL AUTO_INCREMENT ,
`scheduleDate` DATE NULL ,
`class` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`subjectMasterId` INT NULL ,
PRIMARY KEY (  `scheduleMasterId` )
) ENGINE = INNODB;

CREATE TABLE  `onlinescheduledtl` (
`onlineScheduleStlId` INT NOT NULL AUTO_INCREMENT ,
`scheduleMasterId` INT NULL ,
`onlineTestId` INT NULL ,
PRIMARY KEY (  `onlineScheduleStlId` )
) ENGINE = INNODB;

CREATE TABLE  `dps`.`myonlinetest` (
`myOnlineTestId` INT NOT NULL AUTO_INCREMENT ,
`grNo` INT NULL ,
`scheduleMasterId` INT NULL ,
`onlineTestId` INT NULL ,
PRIMARY KEY (  `myOnlineTestId` )
) ENGINE = INNODB;

ALTER TABLE  `myonlinetest` ADD  `answer` CHAR( 1 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER  `onlineTestId`;
<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] != 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$studentName = '';
	$previous = 0;
	$nextPage = 0;
  $scheduleMasterId = '';
	$stdArray = array();
	
	if(isset($_REQUEST['goDateYear']))
	{
		if(isset($_REQUEST['goDateMonth']) && isset($_REQUEST['goDateDay']))
		{
			$todayAcademic = $_REQUEST['goDateMonth']."-".$_REQUEST['goDateDay'];
		}
		else
		{
		  $todayAcademic = date('m-d');
		}
		
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = $_REQUEST['goDateYear']."-04-01";
	  	$nextYear          = $_REQUEST['goDateYear'] + 1;
	  	$academicEndYear   = $nextYear."-03-31";
		}
		else
		{
			$prevYear          = $_REQUEST['goDateYear'] - 1;
			$academicStartYear = $prevYear."-04-01";
	  	$academicEndYear   = $_REQUEST['goDateYear']."-03-31";
		}
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y')."-04-01";
	  	$nextYear            = date('Y') + 1;
	  	$academicEndYear   = $nextYear."-03-31";
		}
		else
		{
			$prevYear          = date('Y') - 1;
			$academicStartYear = $prevYear."-04-01";
	  	$academicEndYear   = date('Y')."-03-31";
		}
	}
	
	$scheduleMasterId = isset($_GET['scheduleMasterId']) && $_GET['scheduleMasterId'] != '' ? $_GET['scheduleMasterId'] : 0;
	
	$selectId = "SELECT studentMasterId,grNo
                 FROM studentmaster
                WHERE studentLoginId = '".$_SESSION['s_activName']."'";
  $selectIdRes = mysql_query($selectId);
  if($idRow = mysql_fetch_array($selectIdRes))
  {
  	$grNo = $idRow['grNo'];
  	$selectClass = "SELECT class,section
	                    FROM nominalroll
	                   WHERE grNo = '".$idRow['grNo']."'
	                     AND academicStartYear = '".$academicStartYear."'
	                     AND academicEndYear = '".$academicEndYear."'";
	  $selectClassRes = mysql_query($selectClass);
	  if($classRow = mysql_fetch_array($selectClassRes))
	  {
      $class   = $classRow['class'];
      $section = $classRow['section'];
		}
  	if(isset($_GET['submit']))
    {
		  $selectStdIn = "SELECT scheduleMasterId,onlineTestId
		                    FROM onlinescheduledtl
		                   WHERE scheduleMasterId = ".$scheduleMasterId."";
		  $selectStdInRes = mysql_query($selectStdIn);
		  while($stdInRow = mysql_fetch_array($selectStdInRes))
		  {
		  	$scheduleMasterId = $stdInRow['scheduleMasterId'];
		  	$onlineTestId     = $stdInRow['onlineTestId'];
		  	
		  	$insertClass = "INSERT INTO myonlinetest (grNo,scheduleMasterId,onlineTestId)
	  	                  VALUES('".$grNo."','".$scheduleMasterId."','".$onlineTestId."')";
	  	  $insertClassRes = om_query($insertClass);
	  	  if(!$insertClassRes)
	  	  {
	  	  	echo "Insert Fail";
	  	  }
	  	  else
	  	  {
	  	  	header("Location:myOnlineTestGenerate.php?scheduleMasterId=".$scheduleMasterId."");
	  	  }
		  }
		}
		if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
		$start_from = ($page-1) * 1;
		
		$i = 0;
	  $selectStd = "SELECT myonlinetest.myOnlineTestId,onlinetest.qNo,onlinetest.question,onlinetest.option1,onlinetest.option2,onlinetest.option3,onlinetest.option4,myonlinetest.answer
	                  FROM myonlinetest
	             LEFT JOIN onlinetest ON onlinetest.onlineTestId = myonlinetest.onlineTestId
	                 WHERE myonlinetest.scheduleMasterId = ".$scheduleMasterId."
	                   AND grNo = '".$grNo."'
	                 LIMIT ".$start_from.", 1";
	  $selectStdRes = mysql_query($selectStd);
	  $count = mysql_num_rows($selectStdRes);
	  while($stdRow = mysql_fetch_array($selectStdRes))
	  {
	  	$stdArray[$i]['myOnlineTestId'] = $stdRow['myOnlineTestId'];
	  	$stdArray[$i]['qNo']      = $stdRow['qNo'];
	  	$stdArray[$i]['question'] = $stdRow['question'];
	  	$stdArray[$i]['option1']  = $stdRow['option1'];
	  	$stdArray[$i]['option2']  = $stdRow['option2'];
	  	$stdArray[$i]['option3']  = $stdRow['option3'];
	  	$stdArray[$i]['option4']  = $stdRow['option4'];
	  	$stdArray[$i]['answer']   = $stdRow['answer'];
	  	$i++;
	  }
	}
	
	$b = 0;
	$pageArray = array();
  $selectPag = "SELECT myonlinetest.myOnlineTestId,onlinetest.qNo,myonlinetest.answer
                  FROM myonlinetest
             LEFT JOIN onlinetest ON onlinetest.onlineTestId = myonlinetest.onlineTestId
                 WHERE myonlinetest.scheduleMasterId = ".$scheduleMasterId."
                   AND grNo = '".$grNo."'";
  $selectPagRes = mysql_query($selectPag);
  $count = mysql_num_rows($selectStdRes);
  while($pageRow = mysql_fetch_array($selectPagRes))
  {
  	$pageArray[$b]['qNo']    = $pageRow['qNo'];
  	$pageArray[$b]['answer'] = $pageRow['answer'];
  	$b++;
  }

	$selectStdRow = "SELECT myonlinetest.myOnlineTestId,onlinetest.qNo,onlinetest.question,onlinetest.option1,onlinetest.option2,onlinetest.option3,onlinetest.option4,myonlinetest.answer
                  FROM myonlinetest
             LEFT JOIN onlinetest ON onlinetest.onlineTestId = myonlinetest.onlineTestId
                 WHERE myonlinetest.scheduleMasterId = ".$scheduleMasterId."
                   AND grNo = '".$grNo."'";
  $selectStdRowRes = mysql_query($selectStdRow);
  $count = mysql_num_rows($selectStdRowRes);
  $countTime = $count;
	if(isset($_GET['d2']))
	{
		$d23 = explode(':',$_GET['d2']);
		
		$d2 = $d23[0];
	  $d3 = $d23[1];
	}
	else
	{
	  $d3 = 0;
	  $d2 = $countTime;
	}
	
	$d23New = $d3*1000 + $d2*60000;
	
	//$sqlName = "SELECT studentName FROM studentMaster WHERE grNo = '".$grNo."'";
	$sqlNameResult = mysql_query("SELECT studentName FROM studentMaster WHERE grNo = '".$grNo."'");
	$nameRow = mysql_fetch_array($sqlNameResult);
	$studentName = $nameRow['studentName'];
	
	$sql = "SELECT COUNT(myOnlineTestId) FROM myonlinetest WHERE myonlinetest.scheduleMasterId = ".$scheduleMasterId." AND grNo = '".$grNo."'";
	$rs_result = mysql_query($sql);
	$row = mysql_fetch_row($rs_result);
	$total_records = $row[0];
	$total_pages = ceil($total_records / 1);
	/// Using Inter
  //	for ($o=1; $o<=$total_pages; $o++) {
  //    echo $pagination =  "<table border='1'>"."<tr>"."<td>"."<a href='myOnlineTestGenerate.php?scheduleMasterId=".$scheduleMasterId."&page=".$o."'>".$o."</a> "."</td>"."</tr>"."</table>";
  //	};
	/// Using Inter
	if(($start_from + 2) >= 0)
	{
	    $next = $start_from + 2;
	}
	if($start_from != 0)
	{
	  $previous = $start_from;
	}
	if($next <= $total_pages)
	{
	  $nextPage = $next;
	}
	if($previous != '')
	{
	  $questonNo = $previous + 1;
  }
  else
  {
    $questonNo = 1;
  }
	
	if(isset($_GET['page']))
  {
  	$myOnlineTestId = isset($_GET['myOnlineTestId']) && $_GET['myOnlineTestId'] != '' ? $_GET['myOnlineTestId'] : 0;
	  $answerMain = isset($_GET['answerMain']) && $_GET['answerMain'] != '' ? $_GET['answerMain'] : '';
	  
  	$updateClass = "UPDATE myonlinetest
  	                   SET answer = '".$answerMain."'
  	                 WHERE myOnlineTestId = '".$myOnlineTestId."'";
	  $updateClassRes = om_query($updateClass);
	  if(!$updateClassRes)
	  {
	  	echo "Update Fail";
	  }
	  if($_GET['page'] == 0)
	  {
	  	header("Location:myOnlineResult.php?grNo=".$grNo."&scheduleMasterId=".$scheduleMasterId."");
	  }
  }
	
	$c=0;
	$cArray = array();
	$selectClass = "SELECT DISTINCT className
                    FROM classmaster";
	$selectClassRes = mysql_query($selectClass);
	while($classRow = mysql_fetch_array($selectClassRes))
	{
	  $cArray['className'][$c]    = $classRow['className'];
	  $c++;
	}
	
  $secArrOut[0] = 'A';
  $secArrOut[1] = 'B';
  $secArrOut[2] = 'C';
  $secArrOut[3] = 'D';
  
  
  include("./bottom.php");
  $smarty->assign('cArray',$cArray);
  $smarty->assign('secArrOut',$secArrOut);
  $smarty->assign('class',$class);
  $smarty->assign('section',$section);
  $smarty->assign('stdArray',$stdArray);
  $smarty->assign('count',$count);
  $smarty->assign('previous',$previous);
  $smarty->assign('nextPage',$nextPage);
  $smarty->assign('scheduleMasterId',$scheduleMasterId);
  $smarty->assign('d2',$d2);
  $smarty->assign('d3',$d3);
  $smarty->assign('studentName',$studentName);
  $smarty->assign('grNo',$grNo);
  $smarty->assign('d23New',$d23New);
  $smarty->assign('pageArray',$pageArray);
  $smarty->assign('questonNo',$questonNo);
  $smarty->display('myOnlineTestGenerate.tpl');  
}
?>
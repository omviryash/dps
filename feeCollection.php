<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$isEdit          = 0;
	$grNo            = '';
	$studentName     = '';
	$feeTypeId       = '';
	$feeName         = '';
	$amount          = '';
	$rNo             = '';
	$modeOfPay       = '';
	$chNo            = '';
	$discount        = 0;
	$feeCollectionId = 0;
	$feeDate         = date('Y-m-d');
	
	$classArray      = array();
	
  if(isset($_POST['Submit']))
  {
  	$loopCount1 = 0;
	  while($loopCount1 < count($_POST['amount']))
	  {
	  	$feeDate         = isset($_POST['feeDate']) ? $_POST['feeDate'] : '0000-00-00';
	  	$grNo            = isset($_POST['grNo']) ? $_POST['grNo'] : 0;
	  	$feeTypeId       = isset($_POST['feeTypeId'][$loopCount1]) ? $_POST['feeTypeId'][$loopCount1] : 0;
	  	$feeName         = isset($_POST['feeName'][$loopCount1]) ? $_POST['feeName'][$loopCount1] : '';
	  	$amount          = isset($_POST['amount'][$loopCount1]) ? $_POST['amount'][$loopCount1] : 0;
	  	$rNo             = isset($_POST['rNo']) ? $_POST['rNo'] : 0;
	  	$modeOfPay       = isset($_POST['modeOfPay'][$loopCount1]) ? $_POST['modeOfPay'][$loopCount1] : 0;
	  	$chNo            = isset($_POST['chNo'][$loopCount1]) ? $_POST['chNo'][$loopCount1] : 0;
	  	$discount        = isset($_POST['discount'][$loopCount1]) ? $_POST['discount'][$loopCount1] : 0;
	  	$feeCollectionId = isset($_POST['feeCollectionId'][$loopCount1]) ? $_POST['feeCollectionId'][$loopCount1] : 0;
	  	
	  	if($feeCollectionId == 0)
	  	{
	  		if($_POST['amount'][$loopCount1] > 0)
	  		{
		  	  $insertClass = "INSERT INTO feecollection (grNo,feeTypeId,feeName,amount,rNo,modeOfPay,chNo,discount,feeDate)
		  	                  VALUES (".$grNo.",".$feeTypeId.",'".$feeName."',".$amount.",'".$rNo."','".$modeOfPay."','".$chNo."',".$discount.",'".$feeDate."')";
		  	  $insertClassRes = om_query($insertClass);
		  	  if(!$insertClassRes)
		  	  {
		  	  	if($grNo == '' || $grNo == 0)
		  	  	{
		  	  	  echo "Please add GR-No. Properly";
		  	  	}
		  	  	else
		  	  	{
			  	  echo "Insert Fail";
			  	}
		  	  }
		  	  else
		  	  {
		  	  	header("Location:feeCollection.php?done=1");
		  	  }
	  	  }
	  	  $loopCount1++;
	  	}
	  	else
	  	{
	  	  if($_POST['amount'][$loopCount1] > 0)
	  	  {
		  	  $updateClass = "UPDATE feecollection
		  	                     SET grNo   = ".$grNo.",
		  	                         feeTypeId = ".$feeTypeId.",
		  	                         feeName = '".$feeName."',
		  	                         amount = ".$amount.",
		  	                         rNo    = '".$rNo."',
		  	                         modeOfPay = '".$modeOfPay."',
		  	                         chNo = '".$chNo."',
		  	                         discount = ".$discount.",
		  	                         feeDate = '".$feeDate."'
		  	                   WHERE feeCollectionId = ".$feeCollectionId;
		      $updateClassRes = om_query($updateClass);
		      if(!$updateClassRes)
		      {
		      	echo "Update Fail";
		      }
		      else
		      {
		      	header("Location:feeCollection.php?done=1");
		      }
		    }
	      $loopCount1++;
	  	}
	  }
  }
  
  $i = 0;
  $selectClass = "SELECT feecollection.feeCollectionId,feecollection.grNo,feecollection.feeTypeId,feecollection.feeName,feecollection.amount,feecollection.rNo,
                         feecollection.modeOfPay,.feecollection.chNo,feecollection.discount,feecollection.feeDate
                    FROM feecollection
                ORDER BY feecollection.feeDate DESC";
  $selectClassRes = mysql_query($selectClass);
  while($classRow = mysql_fetch_array($selectClassRes))
  {
  	$classArray[$i]['feeCollectionId'] = $classRow['feeCollectionId'];
  	$classArray[$i]['grNo']            = $classRow['grNo'];
  	$classArray[$i]['feeTypeId']       = $classRow['feeTypeId'];
  	$classArray[$i]['feeName']         = $classRow['feeName'];
  	$classArray[$i]['amount']          = $classRow['amount'];
  	$classArray[$i]['rNo']             = $classRow['rNo'];
  	$classArray[$i]['modeOfPay']       = $classRow['modeOfPay'];
  	$classArray[$i]['chNo']            = $classRow['chNo'];
  	$classArray[$i]['discount']        = $classRow['discount'];
  	$classArray[$i]['feeDate']         = $classRow['feeDate'];
  	$i++;
  }
  
  $editArry = array();
  if(isset($_REQUEST['feeCollectionId']) > 0)
  {
  	$isEdit = 1;
  	$e = 0;
    $selectClass = "SELECT feecollection.feeCollectionId,feecollection.grNo,feecollection.feeTypeId,
                           feecollection.feeName,feecollection.amount,feecollection.rNo,feecollection.modeOfPay,
                           feecollection.feeDate,feecollection.chNo,feecollection.discount,studentmaster.studentName
	                    FROM feecollection
	               LEFT JOIN studentmaster ON studentmaster.grNo = feecollection.grNo
                     WHERE feecollection.rNo = '".$_REQUEST['rNo']."'
                       AND feecollection.feeDate = '".$_REQUEST['feeDate']."'";
    $selectClassRes = mysql_query($selectClass);
    while($classRow = mysql_fetch_array($selectClassRes))
    {
    	$editArry[$e]['feeCollectionId'] = $classRow['feeCollectionId'];
    	$editArry[$e]['feeTypeId']       = $classRow['feeTypeId'];
    	$editArry[$e]['feeName']         = $classRow['feeName'];
    	$editArry[$e]['amount']          = $classRow['amount'];
    	$editArry[$e]['modeOfPay']       = $classRow['modeOfPay'];
    	$editArry[$e]['chNo']            = $classRow['chNo'];
    	$editArry[$e]['discount']        = $classRow['discount'];
    	$grNo    = $classRow['grNo'];
    	$rNo     = $classRow['rNo'];
    	$feeDate = $classRow['feeDate'];
    	$e++;
    }
  }
  
  $k = 0;
  $selectClub = "SELECT feeTypeId,feeType
                   FROM feetype";
  $selectClubRes = mysql_query($selectClub);
  while($clubRow = mysql_fetch_array($selectClubRes))
  {
  	$typeArr['feeTypeId'][$k]   = $clubRow['feeTypeId'];
  	$typeArr['feeType'][$k]     = $clubRow['feeType'];
  	$k++;
  }
  
  $feeNameOut[0] = 'Annual';
  $feeNameOut[1] = '1st Quarter';
  $feeNameOut[2] = '2nd Quarter';
  $feeNameOut[3] = '3rd Quarter';
  $feeNameOut[4] = '4th Quarter';
  $feeNameOut[5] = 'One Time';
  $feeNameOut[6] = 'Other';
  
  $modeOfPayOut[0] = 'Cash';
  $modeOfPayOut[1] = 'Cheque';
  $modeOfPayOut[2] = 'DD';
  $modeOfPayOut[3] = 'Online';
  $modeOfPayOut[4] = 'Other';
  
  include("./bottom.php");
  $smarty->assign('feeNameOut',$feeNameOut);
  $smarty->assign('modeOfPayOut',$modeOfPayOut);
  $smarty->assign('feeCollectionId',$feeCollectionId);
  $smarty->assign('isEdit',$isEdit);
  $smarty->assign('editArry',$editArry);
  $smarty->assign('classArray',$classArray);
  $smarty->assign('feeDate',$feeDate);
  $smarty->assign('grNo',$grNo);
  $smarty->assign('studentName',$studentName);
  $smarty->assign('feeTypeId',$feeTypeId);
  $smarty->assign('feeName',$feeName);
  $smarty->assign('amount',$amount);
  $smarty->assign('rNo',$rNo);
  $smarty->assign('modeOfPay',$modeOfPay);
  $smarty->assign('chNo',$chNo);
  $smarty->assign('discount',$discount);
  $smarty->assign('typeArr',$typeArr);
  $smarty->display('feeCollection.tpl'); 
}
?>
<?php
define('FPDF_FONTPATH', 'font/');
require('./font/fpdf.php');
include "include/config.inc.php";
include("amountToWords.php");
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
  $pdf=new FPDF('P','mm','A4');   //Create new pdf file
  $pdf->Open();     //Open file
  $pdf->SetAutoPageBreak(false);  //Disable automatic page break
  $pdf->AddPage();  //Add first page


  //header part start
  $pdf->Image('./images/logoCertificate.png',5,2,175,30);
  //header part end
  //table header part start  
  $pdf->SetFillColor(232, 232, 232);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->SetXY(5,25);
  $pdf->Cell(200, 5, 'Haripar, Survey No.12, B/H N.R.I Bungalows, Kalawad  Road ,  Rajkot - 360 005.', 0, 0, 'C', 0);

  $pdf->SetFont('Arial', '', 10);
  $pdf->SetXY(5,35);
  $pdf->Cell(20, 5, 'Phone No.', 0,0,'L');
  $pdf->Cell(5,  5, ':', 0,0,'L');
  $pdf->Cell(45, 5, '+91 9375070921/22', 0,0,'L');
  
  $pdf->SetXY(5,40);
  $pdf->Cell(20, 5, 'Fax No.', 0,0,'L');
  $pdf->Cell(5,  5, ':', 0,0,'L');
  $pdf->Cell(45, 5, '+91 9375070923', 0,0,'L');
  
  $pdf->SetXY(5,50);
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(100, 5, 'CBSE Affilition No. 430054', 0,0,'L');
  $pdf->Cell(100, 5, 'School Code: 13015', 0,0,'R');
  
  $pdf->SetXY(55,60);
  $pdf->SetFont('Arial', 'B', 20);
  $pdf->Cell(100, 10, 'School Leaving Certificate', 1,0,'C');
  
  $pdf->SetXY(5,275);
  $pdf->SetFont('Arial', '', 10);
  $pdf->Cell(66, 10, '........................................', 0,0,'L');
  $pdf->Cell(67, 10, '........................................', 0,0,'C');
  $pdf->Cell(66, 10, '........................................', 0,0,'R');
  
  $pdf->SetXY(5,282);
  $pdf->SetFont('Arial', '', 12);
  $pdf->Cell(66, 5, 'Signature Class teacher', 0,0,'L');
  $pdf->Cell(67, 5, 'Checked by', 0,0,'C');
  $pdf->Cell(66, 5, 'Signature of Principal', 0,0,'R');
  
  $pdf->SetXY(5,287);
  $pdf->SetFont('Arial', '', 12);
  $pdf->Cell(66, 5, '', 0,0,'L');
  $pdf->Cell(67, 5, '(Vallabh Vadhiya)', 0,0,'C');
  $pdf->Cell(58, 5, 'with SEAL', 0,0,'R');
  
  $pdf->SetXY(5,292);
  $pdf->SetFont('Arial', '', 12);
  $pdf->Cell(66, 5, '', 0,0,'L');
  $pdf->Cell(67, 5, 'Office Assistant', 0,0,'C');
  $pdf->Cell(66, 5, '', 0,0,'R');
  
  $pdf->Line(5,55,205,55);
  
  $i         = 0; 
  $rowHeight = 5;
  $yAxis     = 25;

  $yAxis = $yAxis + $rowHeight;
  $studentMasterId = isset($_REQUEST['studentMasterId']) ? $_REQUEST['studentMasterId'] : 0;
  $studentName     = "";
  $fatherName      = "";
  $grNo            = "";
  $dateOfBirth     = "";
  $joinedInClass   = "";
  $joiningDate     = "";
  $bloodGroup      = "";
  $mothersName     = "";
  $houseId         = "";
  $activated       = "";
  $genConduct      = "";
	$reasons         = "";
	$remarks         = "";
	$appCer          = "";
	$issueCer        = "";
	$classRoma       = "";
	$classWord       = "";
	$lastStudRoma    = "";
	$lastStudWord    = "";
	$subjectStudies  = "";
  //table header part end 
  $selectStd = "SELECT studentMasterId,studentName,fatherName,grNo,DATE_FORMAT(dateOfBirth, '%d-%m-%Y') AS dateOfBirth,joinedInClass,DATE_FORMAT(joiningDate, '%d-%m-%Y') AS joiningDate,gender,bloodGroup,
                       mothersName,genConduct,studentmaster.houseId,activated,DATE_FORMAT(appCer, '%d-%m-%Y') AS appCer,
                       DATE_FORMAT(issueCer, '%d-%m-%Y') AS issueCer,reasons,remarks,
                       tribe,tcSrNo,lastStud,takenResult,sameClass,higherClassYesNo,higherClass,schoolDues,workingDays,presentDays,nccCadet,gamePlayed,subjectStudies
                  FROM studentmaster
             LEFT JOIN house ON house.houseId = studentmaster.houseId
                 WHERE studentMasterId = ".$studentMasterId."";
  $selectStdRes = mysql_query($selectStd);
  while($stdRow = mysql_fetch_array($selectStdRes))
  {
  	$studentMasterId  = $stdRow['studentMasterId'];
  	$studentName      = $stdRow['studentName'];
  	$fatherName       = $stdRow['fatherName'];
  	$grNo             = $stdRow['grNo'];
  	$dateOfBirth      = $stdRow['dateOfBirth'];
  	$joinedInClass    = $stdRow['joinedInClass'];
  	$joiningDate      = $stdRow['joiningDate'];
  	$mothersName      = $stdRow['mothersName'];
  	$houseId          = $stdRow['houseId'];
  	$nationality      = 'Indian';
  	$activated        = $stdRow['activated'];
  	$genConduct       = $stdRow['genConduct'];
  	$reasons          = $stdRow['reasons'];
  	$remarks          = $stdRow['remarks'];
  	$appCer           = $stdRow['appCer'];
  	$issueCer         = $stdRow['issueCer'];
  	
  	$tribe            = $stdRow['tribe'];
  	$tcSrNo           = $stdRow['tcSrNo'];
  	$lastStud         = $stdRow['lastStud'];
  	$takenResult      = $stdRow['takenResult'];
  	$sameClass        = $stdRow['sameClass'];
  	$higherClassYesNo = $stdRow['higherClassYesNo'];
  	$higherClass      = $stdRow['higherClass'];
  	$schoolDues       = $stdRow['schoolDues'];
  	$workingDays      = $stdRow['workingDays'];
  	$presentDays      = $stdRow['presentDays'];
  	$nccCadet         = $stdRow['nccCadet'];
  	$gamePlayed       = $stdRow['gamePlayed'];
  	$subjectStudies   = $stdRow['subjectStudies'];
  	
  	if($joinedInClass == 1)
	  {
	  	$classWordJoin = 'First';
	  }
	  if($joinedInClass == 2)
	  {
	  	$classWordJoin = 'Second';
	  }
	  if($joinedInClass == 3)
	  {
	  	$classWordJoin = 'Third';
	  }
	  if($joinedInClass == 4)
	  {
	  	$classWordJoin = 'Fourth';
	  }
	  if($joinedInClass == 5)
	  {
	  	$classWordJoin = 'Fifth';
	  }
	  if($joinedInClass == 6)
	  {
	  	$classWordJoin = 'Sixth';
	  }
	  if($joinedInClass == 7)
	  {
	  	$classWordJoin = 'Seventh';
	  }
	  if($joinedInClass == 8)
	  {
	  	$classWordJoin = 'Eightth';
	  }
	  if($joinedInClass == 9)
	  {
	  	$classWordJoin = 'Ninth';
	  }
	  if($joinedInClass == 10)
	  {
	  	$classWordJoin = 'Tenth';
	  }
	  if($joinedInClass == '11 SCI.' || $joinedInClass == '11 COM.')
	  {
	  	$classWordJoin = 'Eleventh';
	  }
	  if($joinedInClass == '12 SCI.' || $joinedInClass == '12 COM.')
	  {
	  	$classWordJoin = 'Twelveth';
	  }
	  if($joinedInClass == 'Pre-Nursery')
	  {
	  	$classWordJoin = 'Pre-Nursery';
	  }
	  if($joinedInClass == 'Nursery')
	  {
	  	$classWordJoin = 'Nursery';
	  }
	  if($joinedInClass == 'Prep')
	  {
	  	$classWordJoin = 'Prep';
	  }
  	
  	if($higherClass == 1)
	  {
	  	$classRoma = 'I';
	  	$classWord = 'First';
	  }
	  if($higherClass == 2)
	  {
	  	$classRoma = 'II';
	  	$classWord = 'Second';
	  }
	  if($higherClass == 3)
	  {
	  	$classRoma = 'III';
	  	$classWord = 'Third';
	  }
	  if($higherClass == 4)
	  {
	  	$classRoma = 'IV';
	  	$classWord = 'Fourth';
	  }
	  if($higherClass == 5)
	  {
	  	$classRoma = 'V';
	  	$classWord = 'Fifth';
	  }
	  if($higherClass == 6)
	  {
	  	$classRoma = 'VI';
	  	$classWord = 'Sixth';
	  }
	  if($higherClass == 7)
	  {
	  	$classRoma = 'VII';
	  	$classWord = 'Seventh';
	  }
	  if($higherClass == 8)
	  {
	  	$classRoma = 'VIII';
	  	$classWord = 'Eightth';
	  }
	  if($higherClass == 9)
	  {
	  	$classRoma = 'IX';
	  	$classWord = 'Ninth';
	  }
	  if($higherClass == 10)
	  {
	  	$classRoma = 'X';
	  	$classWord = 'Tenth';
	  }
	  if($higherClass == 11)
	  {
	  	$classRoma = 'XI';
	  	$classWord = 'Eleventh';
	  }
	  if($higherClass == 12)
	  {
	  	$classRoma = 'XII';
	  	$classWord = 'Twelveth';
	  }
	  if($higherClass == 'Pre-Nursery')
	  {
	  	$classRoma = 'Pre-Nursery';
	  	$classWord = 'Pre-Nursery';
	  }
	  if($higherClass == 'Nursery')
	  {
	  	$classRoma = 'Nursery';
	  	$classWord = 'Nursery';
	  }
	  if($higherClass == 'Prep')
	  {
	  	$classRoma = 'Prep';
	  	$classWord = 'Prep';
	  }
	  else
	  {
	  	$classRoma = '-';
	  	$classWord = '-';
	  }
  
  	if($lastStud == 1)
	  {
	  	$lastStudRoma = 'I';
	  	$lastStudWord = 'First';
	  }
	  if($lastStud == 2)
	  {
	  	$lastStudRoma = 'II';
	  	$lastStudWord = 'Second';
	  }
	  if($lastStud == 3)
	  {
	  	$lastStudRoma = 'III';
	  	$lastStudWord = 'Third';
	  }
	  if($lastStud == 4)
	  {
	  	$lastStudRoma = 'IV';
	  	$lastStudWord = 'Fourth';
	  }
	  if($lastStud == 5)
	  {
	  	$lastStudRoma = 'V';
	  	$lastStudWord = 'Fifth';
	  }
	  if($lastStud == 6)
	  {
	  	$lastStudRoma = 'VI';
	  	$lastStudWord = 'Sixth';
	  }
	  if($lastStud == 7)
	  {
	  	$lastStudRoma = 'VII';
	  	$lastStudWord = 'Seventh';
	  }
	  if($lastStud == 8)
	  {
	  	$lastStudRoma = 'VIII';
	  	$lastStudWord = 'Eightth';
	  }
	  if($lastStud == 9)
	  {
	  	$lastStudRoma = 'IX';
	  	$lastStudWord = 'Ninth';
	  }
	  if($lastStud == 10)
	  {
	  	$lastStudRoma = 'X';
	  	$lastStudWord = 'Tenth';
	  }
	  if($lastStud == 11)
	  {
	  	$lastStudRoma = 'XI';
	  	$lastStudWord = 'Eleventh';
	  }
	  if($lastStud == 12)
	  {
	  	$lastStudRoma = 'XII';
	  	$lastStudWord = 'Twelveth';
	  }
	  if($lastStud == 'Pre-Nursery')
	  {
	  	$lastStudRoma = 'Pre-Nursery';
	  	$lastStudWord = 'Pre-Nursery';
	  }
	  if($lastStud == 'Nursery')
	  {
	  	$lastStudRoma = 'Nursery';
	  	$lastStudWord = 'Nursery';
	  }
	  if($lastStud == 'Prep')
	  {
	  	$lastStudRoma = 'Prep';
	  	$lastStudWord = 'Prep';
	  }
          
          // changes by mitesh
          //$mydate = strtoTime($dateOfBirth);
          $d = date_parse_from_format("d-m-YY", $dateOfBirth);
          $dfirsttwo = words(substr($d['year'], 0, 2));
          $dlasttwo = words(substr($d['year'], 2, 2));
          
          if($d['month'] == 1)
	  {
	  	$dmonth = 'January';
	  }
	  if($d['month'] == 2)
	  {
	  	$classWordJoin = 'February';
	  }
	  if($d['month'] == 3)
	  {
	  	$dmonth = 'March';
	  }
	  if($d['month'] == 4)
	  {
	  	$dmonth = 'April';
	  }
	  if($d['month'] == 5)
	  {
	  	$dmonth = 'May';
	  }
	  if($d['month'] == 6)
	  {
	  	$dmonth = 'June';
	  }
	  if($d['month'] == 7)
	  {
	  	$dmonth = 'July';
	  }
	  if($d['month'] == 8)
	  {
	  	$dmonth = 'August';
	  }
	  if($d['month'] == 9)
	  {
	  	$dmonth = 'September';
	  }
	  if($d['month'] == 10)
	  {
	  	$dmonth = 'October';
	  }
          if($d['month'] == 11)
	  {
	  	$dmonth = 'November';
	  }
	  if($d['month'] == 12)
	  {
	  	$dmonth = 'December';
	  }
          
          if($d['day'] == 1)
	  {
	  	$dday = 'First';
	  }
	  if($d['day'] == 2)
	  {
	  	$dday = 'Second';
	  }
	  if($d['day'] == 3)
	  {
	  	$dday = 'Third';
	  }
	  if($d['day'] == 4)
	  {
	  	$dday = 'Fourth';
	  }
	  if($d['day'] == 5)
	  {
	  	$dday = 'Fifth';
	  }
	  if($d['day'] == 6)
	  {
	  	$dday = 'Sixth';
	  }
	  if($d['day'] == 7)
	  {
	  	$dday = 'Seventh';
	  }
	  if($d['day'] == 8)
	  {
	  	$dday = 'Eightth';
	  }
	  if($d['day'] == 9)
	  {
	  	$dday = 'Ninth';
	  }
	  if($d['day'] == 10)
	  {
	  	$dday = 'Tenth';
	  }
          if($d['day'] == 11)
	  {
	  	$dday = 'Eleventh';
	  }
	  if($d['day'] == 12)
	  {
	  	$dday = 'Twelveth';
	  }
          if($d['day'] == 13)
	  {
	  	$dday = 'Thirteenth';
	  }
          if($d['day'] == 14)
	  {
	  	$dday = 'Fourteenth';
	  }
	  if($d['day'] == 15)
	  {
	  	$dday = 'Fifteenth';
	  }
          if($d['day'] == 16)
	  {
	  	$dday = 'Sixteenth';
	  }
          if($d['day'] == 17)
	  {
	  	$dday = 'Seventeenth';
	  }
	  if($d['day'] == 18)
	  {
	  	$dday = 'Eighteenth';
	  }
          if($d['day'] == 19)
	  {
	  	$dday = 'Nineteenth';
	  }
          if($d['day'] == 20)
	  {
	  	$dday = 'Twenteenth';
	  }
	  if($d['day'] == 21)
	  {
	  	$dday = 'Twentyfirst';
	  }
          if($d['day'] == 22)
	  {
	  	$dday = 'Twentysecond';
	  }
	  if($d['day'] == 23)
	  {
	  	$dday = 'Twentythird';
	  }
          if($d['day'] == 24)
	  {
	  	$dday = 'Twentyfourth';
	  }
          if($d['day'] == 25)
	  {
	  	$dday = 'Twentyfifth';
	  }
	  if($d['day'] == 26)
	  {
	  	$dday = 'Twentysixth';
	  }
          if($d['day'] == 27)
	  {
	  	$dday = 'Twentyseventh';
	  }
          if($d['day'] == 28)
	  {
	  	$dday = 'Twentyeighth';
	  }
	  if($d['day'] == 29)
	  {
	  	$dday = 'Twentyninth';
	  }
          if($d['day'] == 30)
	  {
	  	$dday = 'Thirtyth';
	  }
          if($d['day'] == 31)
	  {
	  	$dday = 'Thirtyfirst';
	  }
	  if($d['day'] == 32)
	  {
	  	$dday = 'Thirtysecond';
	  }
          
  	//$mydate = strtoTime($dateOfBirth);
		//$dateOfBirthWord = ucwords(strtolower(words(substr($dateOfBirth, 0, 2)))).''.date('F', $mydate).', '.ucwords(strtolower(words(substr($dateOfBirth, 6, 4))));
	//echo $dday;	
    $pdf->SetFillColor(232, 232, 232);
    $pdf->SetFont('Arial', 'B', 12);
    $pdf->SetXY(5,72);
    $pdf->Cell(20, 7,  'Sr No.:  ',0, 0,'L');
    $pdf->Cell(30, 7,  $tcSrNo,1, 0,'C');
    $pdf->Cell(90, 7,  '',0, 0,'L');
    $pdf->Cell(30, 7,  'Admission No.:    ',0, 0,'R');
    $pdf->Cell(30, 7,  $grNo,1, 0,'C');
    $pdf->SetFont('Arial', '', 12);
    
    $pdf->Line(5,55,205,55);
    
    $pdf->SetXY(5,80);
    $pdf->Cell(200, 6,  '1. Name of the Pupil: ',0, 0,'L');
    $pdf->SetXY(5,86);
    $pdf->Cell(200, 6,  '2. Father�s Name: ',0, 0,'L');
    $pdf->SetXY(5,92);
    $pdf->Cell(200, 6,  '3. Mother�s Name: ',0, 0,'L');
    $pdf->SetXY(5,99);
    $pdf->Cell(200, 6,  '4. Nationality: ',0, 0,'L');
    $pdf->SetXY(5,105);
    $pdf->Cell(200, 6,  '5. Whether the candidate belongs to Schedule Caste or Schedule Tribe: ',0, 0,'L');
    $pdf->SetXY(5,111);
    $pdf->Cell(200, 6,  '6. Date of first admission in the school with class: ',0, 0,'L');
    $pdf->SetXY(5,117);
    $pdf->Cell(200, 6,  '7. Date of birth (in Christian Era) according to Admission Register (in figures): ',0, 0,'L');
    $pdf->SetXY(5,123);
    $pdf->Cell(200, 6,  '  (in words): ',0, 0,'L');
    $pdf->SetXY(5,129);
    $pdf->Cell(200, 6,  '8. Class in which the pupil last studied (in figures): ',0, 0,'L');
    $pdf->SetXY(5,135);
    $pdf->Cell(200, 6,  '9. School / Board Annual examination last taken with result: ',0, 0,'L');
    $pdf->SetXY(5,141);
    $pdf->Cell(200, 6,  '10. Whether failed, if so once / twice, in the same class: ',0, 0,'L');
    $pdf->SetXY(5,147);
    $pdf->MultiCell(200, 6, '11. Subjects studied: ',0,'L');
    $pdf->SetXY(5,159);
    $pdf->Cell(200, 6,  '12. Whether qualified for promotion to the higher class: ',0, 0,'L');
    $pdf->SetXY(5,165);
    $pdf->Cell(200, 6,  '      if so, which class (in Fig.):- ',0, 0,'L');
    $pdf->SetXY(5,171);
    $pdf->Cell(200, 6,  '13. Month up to which the pupil has paid school dues: ' ,0, 0,'L');
    $pdf->SetXY(5,177);
    $pdf->Cell(200, 6,  '14. Any fee concession availed of: if so, the nature of such concession: ',0, 0,'L');
    $pdf->SetXY(5,183);
    $pdf->Cell(200, 6,  '15. Total No. of working days: ',0, 0,'L');
    $pdf->SetXY(5,189);
    $pdf->Cell(200, 6,  '16. Total No. of working days present: ',0, 0,'L');
    $pdf->SetXY(5,195);
    $pdf->Cell(200, 6,  '17. Whether NCC Cadet / Boy Scout / Girl Guide (details may be given): ',0, 0,'L');
    $pdf->SetXY(5,201);
    $pdf->MultiCell(200, 6,  '18 Games played or extra � curricular activities in which the pupil usually took part (mention achievement        level therein): ',0,'L');
    $pdf->SetXY(5,217);
    $pdf->Cell(200, 6,  '19. General conduct',0, 0,'L');
    $pdf->SetXY(5,223);
    $pdf->Cell(200, 6,  '20. Date of application for certificate',0, 0,'L');
    $pdf->SetXY(5,229);
    $pdf->Cell(200, 6,  '21. Date of issue of certificate',0, 0,'L');
    $pdf->SetXY(5,235);
    $pdf->Cell(200, 6,  '22. Reasons for leaving the school',0, 0,'L');
    $pdf->SetXY(5,241);
    $pdf->Cell(200, 6,  '23. Any other remarks',0, 0,'L');
    
    $pdf->SetFont('Arial', 'B', 12);
    $pdf->SetXY(5,80);
    $pdf->Cell(200, 6,  '                                  '.$studentName,0, 0,'L');
    $pdf->SetXY(5,86);
    $pdf->Cell(200, 6,  '                              '.$fatherName,0, 0,'L');
    $pdf->SetXY(5,92);
    $pdf->Cell(200, 6,  '                              '.$mothersName,0, 0,'L');
    $pdf->SetXY(5,99);
    $pdf->Cell(200, 6,  '                       '.$nationality,0, 0,'L');
    $pdf->SetXY(5,105);
    $pdf->Cell(200, 6,  '                                                                                                                   '.$tribe,0, 0,'L');
    $pdf->SetXY(5,111);
    $pdf->Cell(200, 6,  '                                                                               '.$joiningDate.'  ('.$classWordJoin.')',0, 0,'L');
    $pdf->SetXY(5,117);
    $pdf->Cell(200, 6,  '                                                                                                                             '.$dateOfBirth,0, 0,'L');
    $pdf->SetXY(5,123);
    $pdf->Cell(200, 6,  '                    '.$dday.', '.$dmonth.', '.ucfirst($dfirsttwo).$dlasttwo,0, 0,'L');
    $pdf->SetXY(5,129);
    $pdf->Cell(200, 6,  '                                                                                 '.$lastStudRoma.' (in words): '.$lastStudWord,0, 0,'L');
    $pdf->SetXY(5,135);
    $pdf->Cell(200, 6,  '                                                                                                '.$takenResult,0, 0,'L');
    $pdf->SetXY(5,141);
    $pdf->Cell(200, 6,  '                                                                                          '.$sameClass,0, 0,'L');
    $pdf->SetXY(12,147);
    $pdf->MultiCell(200, 6, '                             '.$subjectStudies,0,'L');
    $pdf->SetXY(5,159);
    $pdf->Cell(200, 6,  '                                                                                        '.$higherClassYesNo,0, 0,'L');
    $pdf->SetXY(5,165);
    if($classRoma == '-')
    {
      $pdf->Cell(200, 6,  '                                                    '.$classRoma.'    (in words):-    '.$classWord,0, 0,'L');
    }
    else
    {
    	$pdf->Cell(200, 6,  '                                                 '.$classRoma.' (in words):- '.$classWord,0, 0,'L');
    }
    $pdf->SetXY(5,171);
    $pdf->Cell(200, 6,  '                                                                                        '.$schoolDues,0, 0,'L');
    $pdf->SetXY(5,177);
    $pdf->Cell(200, 6,  '                                                                                                                   N/A',0, 0,'L');
    $pdf->SetXY(5,183);
    $pdf->Cell(200, 6,  '                                                 '.$workingDays,0, 0,'L');
    $pdf->SetXY(5,189);
    $pdf->Cell(200, 6,  '                                                              '.$presentDays,0, 0,'L');
    $pdf->SetXY(5,195);
    $pdf->Cell(200, 6,  '                                                                                                                    '.$nccCadet,0, 0,'L');
    $pdf->SetXY(12,201);
    $pdf->MultiCell(200, 6,  '                                                                                                                                                                                                '.$gamePlayed,0,'L');
    $pdf->SetXY(100,217);
    $pdf->Cell(100, 6,  ': '.$genConduct,0, 0,'L');
    $pdf->SetXY(100,223);
    $pdf->Cell(100, 6,  ': '.$appCer,0, 0,'L');
    $pdf->SetXY(100,229);
    $pdf->Cell(100, 6,  ': '.$issueCer,0, 0,'L');
    $pdf->SetXY(100,235);
    $pdf->Cell(100, 6,  ': '.$reasons,0, 0,'L');
    $pdf->SetXY(100,241);
    $pdf->Cell(100, 6,  ': '.$remarks,0, 0,'L');
  }
  
  $pdf->Output();
}
?>
<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$totalPresent   = 0;
	$totalAbsent    = 0;
	$attendences    = '';
	$attendenceArr  = array();
        $today = date('Y-m-d');
	
	if(isset($_REQUEST['attendenceYear']))
	{
	  $attendenceDate = $_REQUEST['attendenceYear']."-".$_REQUEST['attendenceMonth']."-".$_REQUEST['attendenceDay'];
	}
  else
  {
    $attendenceDate = $today;
  }
  
  if(isset($_REQUEST['attendenceYear']))
  {
	  $todayAcademic = $_REQUEST['attendenceMonth']."-".$_REQUEST['attendenceDay'];
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = $_REQUEST['attendenceYear']."-04-01";
	  	$nextYear          = $_REQUEST['attendenceYear'] + 1;
	  	$academicEndYear   = $nextYear."-03-31";
		}
		else
		{
			$prevYear          = $_REQUEST['attendenceYear'] - 1;
			$academicStartYear = $prevYear."-04-01";
	  	$academicEndYear   = $_REQUEST['attendenceYear']."-03-31";
		}
	}
	else
	{
		$todayAcademic = date('m-d');
		if($todayAcademic >= '04-01' && $todayAcademic <= '12-31')
		{
	  	$academicStartYear = date('Y')."-04-01";
	  	$nextYear          = date('Y') + 1;
	  	$academicEndYear   = $nextYear."-03-31";
		}
		else
		{
			$prevYear          = date('Y') - 1;
			$academicStartYear = $prevYear."-04-01";
	  	$academicEndYear   = date('Y')."-03-31";
		}
	}
	
  $i = 0;
  $selectEmp = "SELECT DISTINCT classteacherallotment.class,classteacherallotment.section,employeemaster.name,classmaster.priority
                  FROM classteacherallotment
             LEFT JOIN employeemaster ON employeemaster.employeeMasterId = classteacherallotment.employeeMasterId
             LEFT JOIN classmaster ON classmaster.className = classteacherallotment.class
                 WHERE classteacherallotment.academicStartYear = '".$academicStartYear."'
                   AND classteacherallotment.academicEndYear = '".$academicEndYear."'
              ORDER BY classmaster.priority,classteacherallotment.section";
  $selectEmpRes = mysql_query($selectEmp);
  while($empRow = mysql_fetch_array($selectEmpRes))
  {
  	$attendenceArr[$i]['class']    = $empRow['class'];
  	$attendenceArr[$i]['section']  = $empRow['section'];
  	$attendenceArr[$i]['name']     = $empRow['name'];
  	$selectAbsent = "SELECT attendences,date AS attendenceDate,class,section,taken
     	                 FROM attendence
  	                  WHERE class = '".$empRow['class']."'
  	                    AND section = '".$empRow['section']."'
  	                    AND attendences = 'A'
  	                    AND date = '".$attendenceDate."'
  	                    AND taken = 'AT'";
    $selectAbsentRes = mysql_query($selectAbsent);
    $countAbsent = mysql_num_rows($selectAbsentRes);
    $attendenceArr[$i]['countAbsent'] = $countAbsent;
    $selectPresent = "SELECT attendences,date AS attendenceDate,taken
                        FROM attendence 
  	                   WHERE class = '".$empRow['class']."'
  	                     AND section = '".$empRow['section']."'
  	                     AND attendences = 'P'
  	                     AND date = '".$attendenceDate."'
  	                     AND taken = 'AT'";
    $selectPresentRes = mysql_query($selectPresent);
    $countPresent     = mysql_num_rows($selectPresentRes);
    $attendenceArr[$i]['countPresent'] = $countPresent;
    $attendenceArr[$i]['countAll']     = $countPresent + $countAbsent;
  	
  	$taken = '';
  	$selectStdIn = "SELECT taken
	                    FROM attendence
	                   WHERE class = '".$empRow['class']."'
	                     AND section = '".$empRow['section']."'
	                     AND date = '".$attendenceDate."'";
	  $selectStdInRes = mysql_query($selectStdIn);
	  $countTaken = mysql_num_rows($selectPresentRes);
	  if($row = mysql_fetch_array($selectStdInRes))
	  $taken = $row['taken'];
    if($countTaken == '')
    {
      $attendenceArr[$i]['taken'] = 'NT';
    }
    else
    {
    	$attendenceArr[$i]['taken'] = $taken;
    }
    $totalPresent += $countPresent;
    $totalAbsent  += $countAbsent;
    
    $i++;
  }
  $totalStudent  = $totalPresent+$totalAbsent;
  include("./bottom.php");
  $smarty->assign('totalAbsent',$totalAbsent);
  $smarty->assign('totalPresent',$totalPresent);
  $smarty->assign('totalStudent',$totalStudent);
  $smarty->assign('attendences',$attendences);
  $smarty->assign('attendenceArr',$attendenceArr);
  $smarty->assign('attendenceDate',$attendenceDate);
  $smarty->display('attendenceReport.tpl');
}
?>
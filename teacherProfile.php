<?php
include "include/config.inc.php";
if(!isset($_SESSION['s_activName']) && !isset($_SESSION['s_userType']) || isset($_SESSION['s_userType']) && $_SESSION['s_userType'] == 'Student')
{
  $_SESSION['s_urlRedirectDir'] = $_SERVER['REQUEST_URI'];
	header("Location:checkLogin.php");
}
else
{
	$empArray = array();
  $selectEmp = "SELECT name,loginId,dateOfBirth,joiningDate,gender,currentAddress,currentAddressPin,residenceTelephone,
                       permanentAddress,mobile,phone1,phone2,email,marital,accountNo,panNo,qualification,experience,bloodGroup
                  FROM employeemaster
                 WHERE loginId = '".$_SESSION['s_activName']."'";
  $selectEmpRes = mysql_query($selectEmp);
  while($empRow = mysql_fetch_array($selectEmpRes))
  {
  	$empArray['name']               = $empRow['name'];
  	$empArray['dateOfBirth']        = $empRow['dateOfBirth'];
  	$empArray['joiningDate']        = $empRow['joiningDate'];
  	$empArray['gender']             = $empRow['gender'];
  	$empArray['currentAddress']     = $empRow['currentAddress'].'  '.$empRow['currentAddressPin'];
  	$empArray['residenceTelephone'] = $empRow['residenceTelephone'];
  	$empArray['permanentAddress']   = $empRow['permanentAddress'];
  	$empArray['mobile']             = $empRow['mobile'];
  	$empArray['phone1']             = $empRow['phone1'];
  	$empArray['phone2']             = $empRow['phone2'];
  	$empArray['email']              = $empRow['email'];
  	$empArray['marital']            = $empRow['marital'];
  	$empArray['accountNo']          = $empRow['accountNo'];
  	$empArray['panNo']              = $empRow['panNo'];
  	$empArray['qualification']      = $empRow['qualification'];
  	$empArray['experience']         = $empRow['experience'];
  	$empArray['bloodGroup']         = $empRow['bloodGroup'];
  	
  }
  include("./bottom.php");
  $smarty->assign('empArray',$empArray);
  $smarty->display('teacherProfile.tpl');  
}
?>